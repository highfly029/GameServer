package com.highfly029.parser.pb;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.googlecode.protobuf.format.JsonFormat;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.common.protocol.packet.PbQuest;

/**
 * generated by tools, don't modify!
 */
public class S2CLoginPushPlayerQuestDataParser implements IPacketParser {

    @Override
    public int getPtCode() {
        return 1600;
    }

    @Override
    public String getName() {
        return "S2CLoginPushPlayerQuestData";
    }

    @Override
    public boolean isParserHex() {
        return true;
    }

    @Override
    public String parser(ByteString byteString) throws InvalidProtocolBufferException {
        try {
            PbQuest.S2CLoginPushPlayerQuestData req = PbQuest.S2CLoginPushPlayerQuestData.parseFrom(byteString.toByteArray());
            JsonFormat jsonFormat = new JsonFormat();
            return jsonFormat.printToString(req);
        } catch (InvalidProtocolBufferException e) {
            throw e;
        }
    }
}
