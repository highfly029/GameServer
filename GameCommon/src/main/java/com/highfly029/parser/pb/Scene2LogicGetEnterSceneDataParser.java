package com.highfly029.parser.pb;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.googlecode.protobuf.format.JsonFormat;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.common.protocol.packet.PbScene;

/**
 * generated by tools, don't modify!
 */
public class Scene2LogicGetEnterSceneDataParser implements IPacketParser {

    @Override
    public int getPtCode() {
        return 509;
    }

    @Override
    public String getName() {
        return "Scene2LogicGetEnterSceneData";
    }

    @Override
    public boolean isParserHex() {
        return true;
    }

    @Override
    public String parser(ByteString byteString) throws InvalidProtocolBufferException {
        try {
            PbScene.Scene2LogicGetEnterSceneData req = PbScene.Scene2LogicGetEnterSceneData.parseFrom(byteString.toByteArray());
            JsonFormat jsonFormat = new JsonFormat();
            return jsonFormat.printToString(req);
        } catch (InvalidProtocolBufferException e) {
            throw e;
        }
    }
}
