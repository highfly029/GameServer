package com.highfly029.parser.custom;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.core.interfaces.IPacketParser;

/**
 * generated by tools, don't modify!
 */
public class Logic2LogicMessageParser implements IPacketParser {

    @Override
    public int getPtCode() {
        return 20003;
    }

    @Override
    public String getName() {
        return "Logic2LogicMessage";
    }

    @Override
    public boolean isParserHex() {
        return true;
    }

    @Override
    public String parser(ByteString byteString) throws InvalidProtocolBufferException {
        try {
            PbLoadBalance.Logic2LogicMessage req = PbLoadBalance.Logic2LogicMessage.parseFrom(byteString.toByteArray());
            return BaseMessageParser.parser(req.getPlayerID(), req.getFrom(), req.getFromIndex(),
                    req.getTo(), req.getToIndex(), req.getPtCode(), req.getData());
        } catch (InvalidProtocolBufferException e) {
            throw e;
        }
    }
}
