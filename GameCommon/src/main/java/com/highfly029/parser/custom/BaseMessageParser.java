package com.highfly029.parser.custom;

import java.util.HashMap;
import java.util.Map;

import com.google.protobuf.ByteString;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.core.tool.PacketParserTool;
import com.highfly029.utils.JsonUtils;

/**
 * @ClassName BrokerMessageParser
 * @Description BrokerMessageParser
 * @Author liyunpeng
 **/
public class BaseMessageParser {

    /**
     * 解析broker
     *
     * @param playerID
     * @param fromServerID
     * @param fromIndex
     * @param toServerID
     * @param toIndex
     * @param ptCode
     * @param data
     */
    public static String parser(long playerID, int fromServerID, int fromIndex, int toServerID, int toIndex,
                                int ptCode, ByteString data) {
        IPacketParser packetParser = PacketParserTool.getPacketParser(ptCode);
        String ret = PacketParserTool.getPacketDump(packetParser, data, true);
        Map<String, String> map = new HashMap<>();
        map.put("playerID", String.valueOf(playerID));
        map.put("from", String.valueOf(fromServerID));
        map.put("fromIndex", String.valueOf(fromIndex));
        map.put("to", String.valueOf(toServerID));
        map.put("toIndex", String.valueOf(toIndex));
        map.put("ptCode", String.valueOf(ptCode));
        map.put("data", ret != null ? ret : "null");
        return JsonUtils.toJson(map);
    }
}
