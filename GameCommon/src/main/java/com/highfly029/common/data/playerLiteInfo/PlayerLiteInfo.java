package com.highfly029.common.data.playerLiteInfo;

/**
 * @ClassName PlayerLiteInfo
 * @Description 玩家精简信息
 * @Author liyunpeng
 **/
public class PlayerLiteInfo {
    /**
     * 玩家ID
     */
    private long playerID;
    /**
     * 玩家名字
     */
    private String name;
    /**
     * 玩家等级
     */
    private int level;
    /**
     * 玩家联盟ID
     */
    private long unionID;
    /**
     * 玩家联盟名字
     */
    private String unionName;

    public long getPlayerID() {
        return playerID;
    }

    public void setPlayerID(long playerID) {
        this.playerID = playerID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getUnionID() {
        return unionID;
    }

    public void setUnionID(long unionID) {
        this.unionID = unionID;
    }

    public String getUnionName() {
        return unionName;
    }

    public void setUnionName(String unionName) {
        this.unionName = unionName;
    }

    @Override
    public String toString() {
        return "PlayerLiteInfo{" +
                "playerID=" + playerID +
                ", name='" + name + '\'' +
                ", level=" + level +
                ", unionID=" + unionID +
                ", unionName='" + unionName + '\'' +
                '}';
    }
}
