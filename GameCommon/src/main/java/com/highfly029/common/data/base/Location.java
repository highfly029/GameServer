package com.highfly029.common.data.base;

/**
 * @ClassName Location
 * @Description 定位信息
 * @Author liyunpeng
 **/
public class Location {
    /**
     * 未登录或者已经登出
     */
    public static final int LOGOUT = 0;
    /**
     * 登陆中
     */
    public static final int LOGIN_ING = 1;
    /**
     * 登陆成功
     */
    public static final int LOGIN_SUCCESS = 2;
    /**
     * 离线中(等待断线重连)
     */
    public static final int OFFLINE_ING = 3;

    /**
     * 所在的sql库
     */
    private int gameDbID;
    /**
     * 所在的逻辑服id
     */
    private int logicServerID;
    /**
     * 所在逻辑服线程索引
     */
    private int logicWorldIndex;
    /**
     * 所在的场景服id
     */
    private int sceneServerID;
    /**
     * 所在场景服线程索引
     */
    private int sceneWorldIndex;
    /**
     * 场景ID
     */
    private int sceneID;
    /**
     * 分线ID
     */
    private int lineID;
    /**
     * 场景在线程内的唯一实例ID
     */
    private int instanceID;
    /**
     * 坐标
     */
    private Vector3 curPos;
    /**
     * 登陆状态
     */
    private int state;

    /**
     * 玩家ID
     */
    private long playerID;

    /**
     * 是否在线
     *
     * @return
     */
    public boolean isOnline() {
        return state == LOGIN_SUCCESS;
    }

    /**
     * 设置在线
     */
    public void setOnline() {
        state = LOGIN_SUCCESS;
    }

    public static Location create() {
        return new Location();
    }

    public int getGameDbID() {
        return gameDbID;
    }

    public void setGameDbID(int gameDbID) {
        this.gameDbID = gameDbID;
    }

    public int getLogicServerID() {
        return logicServerID;
    }

    public void setLogicServerID(int logicServerID) {
        this.logicServerID = logicServerID;
    }

    public int getLogicWorldIndex() {
        return logicWorldIndex;
    }

    public void setLogicWorldIndex(int logicWorldIndex) {
        this.logicWorldIndex = logicWorldIndex;
    }

    public int getSceneServerID() {
        return sceneServerID;
    }

    public void setSceneServerID(int sceneServerID) {
        this.sceneServerID = sceneServerID;
    }

    public int getSceneWorldIndex() {
        return sceneWorldIndex;
    }

    public void setSceneWorldIndex(int sceneWorldIndex) {
        this.sceneWorldIndex = sceneWorldIndex;
    }

    public int getSceneID() {
        return sceneID;
    }

    public void setSceneID(int sceneID) {
        this.sceneID = sceneID;
    }

    public int getLineID() {
        return lineID;
    }

    public void setLineID(int lineID) {
        this.lineID = lineID;
    }

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public Vector3 getCurPos() {
        return curPos;
    }

    public void setCurPos(Vector3 curPos) {
        this.curPos = curPos;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public long getPlayerID() {
        return playerID;
    }

    public void setPlayerID(long playerID) {
        this.playerID = playerID;
    }

    @Override
    public String toString() {
        return "Location{" +
                "gameDbID=" + gameDbID +
                ", gameServerID=" + logicServerID +
                ", worldIndex=" + logicWorldIndex +
                ", sceneID=" + sceneID +
                ", lineID=" + lineID +
                ", instanceID=" + instanceID +
                ", curPos=" + curPos +
                ", state=" + state +
                ", playerID=" + playerID +
                '}';
    }
}
