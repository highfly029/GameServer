package com.highfly029.common.data.auction;

import com.google.protobuf.ByteString;

/**
 * @ClassName CenterAuctionData
 * @Description 拍卖物品数据
 * @Author liyunpeng
 **/
public class AuctionItemData {
    /**
     * 唯一id
     */
    private long id;
    /**
     * 上架玩家id
     */
    private long playerID;
    /**
     * 物品id
     */
    private int itemID;
    /**
     * 物品数量
     */
    private int itemNum;
    /**
     * 价格
     */
    private int price;
    /**
     * 上架时间
     */
    private long shelfTime;
    /**
     * 物品数据
     */
    private ByteString itemData;
    /**
     * 竞标数据
     */
    private ByteString bidData;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPlayerID() {
        return playerID;
    }

    public void setPlayerID(long playerID) {
        this.playerID = playerID;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public int getItemNum() {
        return itemNum;
    }

    public void setItemNum(int itemNum) {
        this.itemNum = itemNum;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public long getShelfTime() {
        return shelfTime;
    }

    public void setShelfTime(long shelfTime) {
        this.shelfTime = shelfTime;
    }

    public ByteString getItemData() {
        return itemData;
    }

    public void setItemData(ByteString itemData) {
        this.itemData = itemData;
    }

    public ByteString getBidData() {
        return bidData;
    }

    public void setBidData(ByteString bidData) {
        this.bidData = bidData;
    }

    @Override
    public String toString() {
        return "CenterAuctionItem{" +
                "id=" + id +
                ", playerID=" + playerID +
                ", itemID=" + itemID +
                ", itemNum=" + itemNum +
                ", price=" + price +
                ", shelfTime=" + shelfTime +
                '}';
    }
}
