package com.highfly029.common.data;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;

/**
 * @ClassName DataBaseAccess
 * @Description DataBaseAccess
 * @Author liyunpeng
 **/
public abstract class DataBaseAccess {
    private DataBaseAccess parent;

    private boolean isChange;

    public DataBaseAccess() {

    }

    public DataBaseAccess(DataBaseAccess parent) {
        this.parent = parent;
    }

    public boolean isChange() {
        return isChange;
    }

    public void setChange(boolean change) {
        if (parent != null) {
            parent.setChange(change);
        } else {
            isChange = change;
        }
    }

    public abstract GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException;

    public abstract GeneratedMessageV3 save();

    public abstract String getName();
}
