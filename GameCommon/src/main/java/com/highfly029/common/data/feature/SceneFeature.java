package com.highfly029.common.data.feature;

/**
 * @ClassName SceneFeature
 * @Description 场景服支持的功能
 * @Author liyunpeng
 **/
public class SceneFeature {
    /**
     * 新手服
     */
    public static final int NEW_PLAYER = 1;
    /**
     * 支持普通场景
     */
    public static final int NORMAL_SCENE = 2;
    /**
     * 支持多人副本场景(大规模GVG活动前临时申请机器使用 eg:每周每月)
     */
    public static final int MULTI_PLAYER_DUNGEON_SCENE = 3;
}
