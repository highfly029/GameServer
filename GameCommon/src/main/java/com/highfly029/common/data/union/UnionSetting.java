package com.highfly029.common.data.union;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.DataBaseAccess;
import com.highfly029.common.protocol.packet.PbUnion;

/**
 * @ClassName UnionSetting
 * @Description UnionSetting
 * @Author liyunpeng
 **/
public class UnionSetting extends DataBaseAccess {
    /**
     * 联盟公告
     */
    private String notice;

    /**
     * 联盟申请限制等级
     */
    private int joinLimitLv;

    /**
     * 是否需要申请加入
     */
    private boolean joinNeedApply;

    UnionSetting(DataBaseAccess parent) {
        super(parent);
    }

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbUnion.PbUnionSettingData pbUnionSettingData = PbUnion.PbUnionSettingData.parseFrom(data);
        notice = pbUnionSettingData.getNotice();
        joinLimitLv = pbUnionSettingData.getJoinLimitLv();
        joinNeedApply = pbUnionSettingData.getJoinNeedApply();
        return pbUnionSettingData;
    }

    @Override
    public PbUnion.PbUnionSettingData save() {
        PbUnion.PbUnionSettingData.Builder builder = PbUnion.PbUnionSettingData.newBuilder();
        if (notice != null) {
            builder.setNotice(notice);
        }
        builder.setJoinLimitLv(joinLimitLv);
        builder.setJoinNeedApply(joinNeedApply);
        return builder.build();
    }

    @Override
    public String getName() {
        return null;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        super.setChange(true);
        this.notice = notice;
    }

    public int getJoinLimitLv() {
        return joinLimitLv;
    }

    public void setJoinLimitLv(int joinLimitLv) {
        super.setChange(true);
        this.joinLimitLv = joinLimitLv;
    }

    public boolean isJoinNeedApply() {
        return joinNeedApply;
    }

    public void setJoinNeedApply(boolean joinNeedApply) {
        super.setChange(true);
        this.joinNeedApply = joinNeedApply;
    }
}
