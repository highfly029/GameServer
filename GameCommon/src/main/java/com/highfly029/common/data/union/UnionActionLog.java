package com.highfly029.common.data.union;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.DataBaseAccess;
import com.highfly029.common.protocol.packet.PbUnion;

/**
 * @ClassName UnionActionLog
 * @Description 联盟行为日志
 * @Author liyunpeng
 **/
public class UnionActionLog extends DataBaseAccess {
    /**
     * 时间戳
     */
    private long timestamp;
    /**
     * 联盟行为日志类型
     */
    private int actionLogType;
    /**
     * 额外参数
     */
    private String[] args;

    public static UnionActionLog create() {
        return new UnionActionLog();
    }

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbUnion.PbUnionActionLogData pbUnionActionLogData = PbUnion.PbUnionActionLogData.parseFrom(data);
        timestamp = pbUnionActionLogData.getTimestamp();
        actionLogType = pbUnionActionLogData.getActionLogType();
        int num = pbUnionActionLogData.getArgsCount();
        if (num > 0) {
            args = new String[num];
            for (int i = 0; i < num; i++) {
                args[i] = pbUnionActionLogData.getArgs(i);
            }
        }
        return pbUnionActionLogData;
    }

    @Override
    public PbUnion.PbUnionActionLogData save() {
        PbUnion.PbUnionActionLogData.Builder builder = PbUnion.PbUnionActionLogData.newBuilder();
        builder.setTimestamp(timestamp);
        builder.setActionLogType(actionLogType);
        int num;
        if (args != null && (num = args.length) > 0) {
            for (int i = 0; i < num; i++) {
                builder.addArgs(args[i]);
            }
        }
        return builder.build();
    }

    @Override
    public String getName() {
        return null;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getActionLogType() {
        return actionLogType;
    }

    public void setActionLogType(int actionLogType) {
        this.actionLogType = actionLogType;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }
}
