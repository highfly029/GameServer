package com.highfly029.common.data.activity;

import com.highfly029.common.template.activity.ActivityTemplate;
import com.highfly029.common.template.activity.ActivityTimeTypeConst;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.utils.CronExpression;
import com.highfly029.utils.CronUtils;
import com.highfly029.utils.DateUtils;
import com.highfly029.utils.StringUtils;

/**
 * @ClassName ActivityData
 * @Description 活动时间数据
 * @Author liyunpeng
 **/
public class ActivityTimeData {
    /**
     * 活动开启时间
     */
    private long startTime;

    /**
     * 活动关闭时间
     */
    private long endTime;

    /**
     * 相对时间参数
     */
    private int[] relativeArgs;

    /**
     * 开启时间cron表达式
     */
    private CronExpression startTimeCronExpression;

    /**
     * 结束时间cron表达式
     */
    private CronExpression endTimeCronExpression;

    /**
     * 活动强制关闭时间
     */
    private long forceCloseTime;

    public ActivityTimeData(ActivityTemplate activityTemplate) {
        switch (activityTemplate.getActivityTimeType()) {
            case ActivityTimeTypeConst.AbsoluteTime -> {
                startTime = DateUtils.dateStr2Long(activityTemplate.getStartTimeStr());
                endTime = DateUtils.dateStr2Long(activityTemplate.getEndTimeStr());
            }
            case ActivityTimeTypeConst.RelativeTime -> {
                relativeArgs = StringUtils.toIntArray(activityTemplate.getStartTimeStr(), ":");
            }
            case ActivityTimeTypeConst.CycleTime -> {
                startTimeCronExpression = CronUtils.createCronExpression(activityTemplate.getStartTimeStr());
                endTimeCronExpression = CronUtils.createCronExpression(activityTemplate.getEndTimeStr());
                refreshCronExpressionTime();
            }
        }
        if (activityTemplate.getForceCloseTimeStr() != null) {
            forceCloseTime = DateUtils.dateStr2Long(activityTemplate.getForceCloseTimeStr());
        }
    }

    /**
     * 刷新下一个活动的正则表达式时间
     */
    public void refreshCronExpressionTime() {
        startTime = CronUtils.getNextCronTime(startTimeCronExpression, SystemTimeTool.getMillTime());
        endTime = CronUtils.getNextCronTime(endTimeCronExpression, SystemTimeTool.getMillTime());
        //矫正
        if (startTime > endTime) {
            long nextEndTime = CronUtils.getNextCronTime(endTimeCronExpression, startTime);
            long duration = nextEndTime - startTime;
            startTime = endTime - duration;
        }
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int[] getRelativeArgs() {
        return relativeArgs;
    }

    public long getForceCloseTime() {
        return forceCloseTime;
    }
}
