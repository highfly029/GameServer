package com.highfly029.common.data.scene;

/**
 * @ClassName SceneStrategyEnum
 * @Description SceneStrategyEnum
 * @Author liyunpeng
 **/
public enum SceneStrategyEnum {
    /**
     * 家园场景
     */
    HOME,
    /**
     * 工会场景
     */
    UNION,
    /**
     * 分线场景
     */
    LINE,
    /**
     * 副本场景
     */
    DUNGEON,
}
