package com.highfly029.common.data.union;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.DataBaseAccess;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.utils.collection.IntLongMap;
import com.highfly029.utils.collection.LongIntMap;
import com.highfly029.utils.collection.LongLongMap;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.ObjectQueue;

/**
 * @ClassName Union
 * @Description Union
 * @Author liyunpeng
 **/
public abstract class Union extends DataBaseAccess {
    /**
     * 联盟ID
     */
    private long unionID;
    /**
     * 联盟名字
     */
    private String unionName;
    /**
     * 联盟等级
     */
    private int level;
    /**
     * 联盟成员数据
     */
    private final LongObjectMap<UnionMember> unionMemberMap = new LongObjectMap<>(UnionMember[]::new);
    /**
     * 联盟设置
     */
    private final UnionSetting setting = new UnionSetting(this);
    /**
     * 联盟权限 key:position -> value permission
     */
    private final IntLongMap permissionMap = new IntLongMap();
    /**
     * 联盟资产
     */
    private final IntLongMap assetsMap = new IntLongMap();
    /**
     * 申请加入列表
     */
    private final LongIntMap applyJoinMap = new LongIntMap();
    /**
     * 邀请加入列表
     */
    private final LongIntMap inviteJoinMap = new LongIntMap();
    /**
     * 行为日志
     */
    private final ObjectQueue<UnionActionLog> actionLogQueue = new ObjectQueue<>(UnionActionLog[]::new);

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbUnion.PbUnionData pbUnionData = PbUnion.PbUnionData.parseFrom(data);
        int num = pbUnionData.getMembersCount();

        level = pbUnionData.getLevel();
        for (int i = 0; i < num; i++) {
            PbUnion.PbUnionMemberData pbUnionMemberData = pbUnionData.getMembers(i);
            UnionMember member = UnionMember.create(this);
            member.load(pbUnionMemberData.toByteArray());
            unionMemberMap.put(member.getMemberID(), member);
        }

        setting.load(pbUnionData.getSetting().toByteArray());

        num = pbUnionData.getPermissionsCount();
        for (int i = 0; i < num; i++) {
            PbCommon.PbKeyValue keyValue = pbUnionData.getPermissions(i);
            permissionMap.put(keyValue.getIntKey(), keyValue.getLongValue());
        }

        num = pbUnionData.getAssetsCount();
        for (int i = 0; i < num; i++) {
            PbCommon.PbKeyValue keyValue = pbUnionData.getAssets(i);
            assetsMap.put(keyValue.getIntKey(), keyValue.getLongValue());
        }

        num = pbUnionData.getApplyListCount();
        for (int i = 0; i < num; i++) {
            PbCommon.PbKeyValue keyValue = pbUnionData.getApplyList(i);
            applyJoinMap.put(keyValue.getLongKey(), keyValue.getIntValue());
        }
        num = pbUnionData.getInviteListCount();
        for (int i = 0; i < num; i++) {
            PbCommon.PbKeyValue keyValue = pbUnionData.getInviteList(i);
            inviteJoinMap.put(keyValue.getLongKey(), keyValue.getIntValue());
        }

        num = pbUnionData.getActionLogCount();
        for (int i = 0; i < num; i++) {
            PbUnion.PbUnionActionLogData pbUnionActionLogData = pbUnionData.getActionLog(i);
            UnionActionLog actionLog = UnionActionLog.create();
            actionLog.load(pbUnionActionLogData.toByteArray());
            actionLogQueue.offer(actionLog);
        }

        return pbUnionData;
    }

    @Override
    public PbUnion.PbUnionData save() {
        PbUnion.PbUnionData.Builder builder = PbUnion.PbUnionData.newBuilder();
        builder.setLevel(level);
        unionMemberMap.foreachImmutable((k, v) -> {
            PbUnion.PbUnionMemberData pbUnionMemberData = v.save();
            builder.addMembers(pbUnionMemberData);
        });
        builder.setSetting(setting.save());
        permissionMap.foreachImmutable((k, v) -> {
            PbCommon.PbKeyValue.Builder kvBuilder = PbCommon.PbKeyValue.newBuilder();
            kvBuilder.setIntKey(k);
            kvBuilder.setLongValue(v);
            builder.addPermissions(kvBuilder.build());
        });
        assetsMap.foreachImmutable((k, v) -> {
            PbCommon.PbKeyValue.Builder kvBuilder = PbCommon.PbKeyValue.newBuilder();
            kvBuilder.setIntKey(k);
            kvBuilder.setLongValue(v);
            builder.addAssets(kvBuilder.build());
        });
        applyJoinMap.foreachImmutable((k, v) -> {
            PbCommon.PbKeyValue.Builder kvBuilder = PbCommon.PbKeyValue.newBuilder();
            kvBuilder.setLongValue(k);
            kvBuilder.setIntValue(v);
            builder.addApplyList(kvBuilder);
        });
        inviteJoinMap.foreachImmutable((k, v) -> {
            PbCommon.PbKeyValue.Builder kvBuilder = PbCommon.PbKeyValue.newBuilder();
            kvBuilder.setLongValue(k);
            kvBuilder.setIntKey(v);
            builder.addInviteList(kvBuilder);
        });
        actionLogQueue.foreachImmutable(v -> {
            PbUnion.PbUnionActionLogData pbUnionActionLogData = v.save();
            builder.addActionLog(pbUnionActionLogData);
        });
        return builder.build();
    }

    @Override
    public String getName() {
        return "unionData";
    }

    public long getUnionID() {
        return unionID;
    }

    public void setUnionID(long unionID) {
        super.setChange(true);
        this.unionID = unionID;
    }

    public String getUnionName() {
        return unionName;
    }

    public void setUnionName(String unionName) {
        super.setChange(true);
        this.unionName = unionName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        super.setChange(true);
        this.level = level;
    }

    /**
     * 添加成员
     *
     * @param memberID
     * @param unionMember
     */
    public void addUnionMember(long memberID, UnionMember unionMember) {
        super.setChange(true);
        unionMemberMap.put(memberID, unionMember);
    }

    /**
     * 删除成员
     *
     * @param memberID
     */
    public void removeUnionMember(long memberID) {
        super.setChange(true);
        unionMemberMap.remove(memberID);
    }

    /**
     * 获取成员
     *
     * @param memberID
     * @return
     */
    public UnionMember getUnionMember(long memberID) {
        return unionMemberMap.get(memberID);
    }

    public LongObjectMap<UnionMember> getUnionMemberMap() {
        return unionMemberMap;
    }

    /**
     * 获取成员数量
     *
     * @return
     */
    public int getUnionMemberSize() {
        return unionMemberMap.size();
    }

    /**
     * 获取权限
     *
     * @param position
     * @return
     */
    public long getPermission(int position) {
        return permissionMap.get(position);
    }

    /**
     * 设置权限
     *
     * @param position
     * @param permission
     */
    public void setPermission(int position, long permission) {
        super.setChange(true);
        permissionMap.put(position, permission);
    }

    /**
     * 增加资产
     *
     * @param type
     * @param value
     * @return
     */
    public long addAsset(int type, long value) {
        super.setChange(true);
        return assetsMap.addValue(type, value);
    }

    /**
     * 移除资产
     *
     * @param type
     * @param value
     * @return
     */
    public long removeAsset(int type, long value) {
        super.setChange(true);
        return assetsMap.addValue(type, -value);
    }

    /**
     * 是否已经在邀请列表中
     *
     * @param playerID
     * @return
     */
    public boolean isInInviteJoinList(long playerID) {
        return inviteJoinMap.contains(playerID);
    }

    public LongIntMap getInviteJoinMap() {
        return inviteJoinMap;
    }

    /**
     * 邀请列表大小
     *
     * @return
     */
    public int getInviteJoinSize() {
        return inviteJoinMap.size();
    }

    /**
     * 加入邀请列表
     *
     * @param playerID
     * @param logicServerID
     */
    public void addInviteJoinList(long playerID, int logicServerID) {
        super.setChange(true);
        inviteJoinMap.put(playerID, logicServerID);
    }

    /**
     * 删除邀请列表
     *
     * @param playerID
     */
    public void removeInviteJoinList(long playerID) {
        super.setChange(true);
        inviteJoinMap.remove(playerID);
    }

    /**
     * 是否已经在申请列表中
     *
     * @param playerID
     * @return
     */
    public boolean isInApplyJoinList(long playerID) {
        return applyJoinMap.contains(playerID);
    }

    public LongIntMap getApplyJoinMap() {
        return applyJoinMap;
    }

    /**
     * 获取申请列表大小
     *
     * @return
     */
    public int getApplyJoinListSize() {
        return applyJoinMap.size();
    }

    /**
     * 加入申请列表
     *
     * @param playerID
     * @param logicServerID
     */
    public void addApplyJoinList(long playerID, int logicServerID) {
        super.setChange(true);
        applyJoinMap.put(playerID, logicServerID);
    }

    /**
     * 删除申请列表
     *
     * @param playerID
     */
    public void removeApplyJoinList(long playerID) {
        super.setChange(true);
        applyJoinMap.remove(playerID);
    }

    public UnionSetting getSetting() {
        return setting;
    }

    /**
     * 增加行为日志
     *
     * @param unionActionLog
     * @param maxNum
     */
    protected void addActionLogQueue(UnionActionLog unionActionLog, int maxNum) {
        super.setChange(true);
        if (actionLogQueue.size() >= maxNum) {
            actionLogQueue.poll();
        }
        actionLogQueue.offer(unionActionLog);
    }
}
