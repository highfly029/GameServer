package com.highfly029.common.data.feature;

/**
 * @ClassName CenterFeature
 * @Description 中心服支持的功能
 * @Author liyunpeng
 **/
public class CenterFeature {
    /**
     * 联盟
     */
    public static final int UNION = 1;
    /**
     * 队伍
     */
    public static final int TEAM = 2;
    /**
     * 拍卖
     */
    public static final int AUCTION = 3;
    /**
     * 聊天
     */
    public static final int CHAT = 4;
    /**
     * 全服活动
     */
    public static final int ACTIVITY = 5;
}
