package com.highfly029.common.data.playerLiteInfo;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.highfly029.common.redis.PlayerLiteInfoKeyEnum;
import com.highfly029.common.redis.RedisKey;
import com.highfly029.core.tool.ITool;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.RedisClientTool;

import io.vertx.redis.client.Command;
import io.vertx.redis.client.Request;
import io.vertx.redis.client.Response;

/**
 * @ClassName PlayerLiteInfoCacheTool
 * @Description PlayerLiteInfoCacheTool
 * @Author liyunpeng
 **/
public class PlayerLiteInfoCacheTool implements ITool {
    private Cache<Long, PlayerLiteInfo> cache;

    private PlayerLiteInfoCacheTool() {

    }

    public void init() {
        cache = Caffeine.newBuilder()
                .maximumSize(2)
                .expireAfterWrite(1, TimeUnit.MINUTES)
                .removalListener((key, value, cause) -> {
                    LoggerTool.systemLogger.warn("PlayerLiteInfoCacheTool remove key={}, value={}, cause={}", key, value, cause);
                })
                .weakKeys().weakValues()
                .build();
    }

    private static class LazyHolder {
        private static final PlayerLiteInfoCacheTool INSTANCE = new PlayerLiteInfoCacheTool();

        static {
            INSTANCE.init();
        }
    }

    public static PlayerLiteInfoCacheTool getInstance() {
        return LazyHolder.INSTANCE;
    }

    /**
     * 获取玩家精简信息
     *
     * @param worldIndex
     * @param playerID
     * @param consumer   回调数据
     */
    public void getPlayerLiteInfo(int worldIndex, long playerID, Consumer<PlayerLiteInfo> consumer) {
        //这里不用get因为会堵塞 并且异步redis没办法直接拿到返回值
        PlayerLiteInfo playerLiteInfo = cache.getIfPresent(playerID);
        if (playerLiteInfo != null) {
            consumer.accept(playerLiteInfo);
        } else {
            Request request = Request.cmd(Command.HGETALL).arg(RedisKey.PLAYER_LITE_INFO.value() + playerID);
            RedisClientTool.send(worldIndex, request, (isSuccess, response, errorMsg) -> {
                if (isSuccess) {
                    if (response != null && response.size() > 0) {
                        PlayerLiteInfo info = new PlayerLiteInfo();
                        info.setPlayerID(response.get(PlayerLiteInfoKeyEnum.PLAYER_ID.value()).toLong());
                        info.setName(response.get(PlayerLiteInfoKeyEnum.PLAYER_NAME.value()).toString());
                        info.setLevel(response.get(PlayerLiteInfoKeyEnum.PLAYER_LEVEL.value()).toInteger());
                        Response unionIDResp = response.get(PlayerLiteInfoKeyEnum.PLAYER_UNION_ID.value());
                        if (unionIDResp != null) {
                            info.setUnionID(unionIDResp.toLong());
                        }
                        Response unionNameResp = response.get(PlayerLiteInfoKeyEnum.PLAYER_UNION_NAME.value());
                        if (unionNameResp != null) {
                            info.setUnionName(unionNameResp.toString());
                        }
                        cache.put(playerID, info);
                        consumer.accept(info);
                    } else {
                        LoggerTool.systemLogger.error("getPlayerLiteInfo redis not exist playerID={}, response={}", playerID, response);
                        consumer.accept(null);
                    }
                } else {
                    LoggerTool.systemLogger.error("getPlayerLiteInfo redis fail playerID={}, msg={}", playerID, errorMsg);
                    consumer.accept(null);
                }
            });
        }
    }

    /**
     * 更新玩家精简信息
     *
     * @param worldIndex
     * @param playerID
     * @param keyEnum
     * @param value
     */
    public void updatePlayerLiteInfo(int worldIndex, long playerID, PlayerLiteInfoKeyEnum keyEnum, String value) {
        Request request = Request.cmd(Command.HMSET).arg(RedisKey.PLAYER_LITE_INFO.value() + playerID)
                .arg(keyEnum.value()).arg(value);
        RedisClientTool.send(worldIndex, request, (isSuccess, response, errorMsg) -> {
            if (!isSuccess) {
                LoggerTool.systemLogger.error("updatePlayerLiteInfo redis fail playerID={}, key={}, msg={}", playerID, keyEnum.value(), errorMsg);
            }
        });
    }
}
