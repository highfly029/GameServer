package com.highfly029.common.data.union;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.DataBaseAccess;
import com.highfly029.common.protocol.packet.PbUnion;

/**
 * @ClassName UnionLite
 * @Description 联盟轻量级数据 一般用于联盟列表 查看联盟信息
 * @Author liyunpeng
 **/
public class UnionLite extends DataBaseAccess {
    /**
     * 联盟id
     */
    private long unionID;
    /**
     * 联盟等级
     */
    private int level;
    /**
     * name
     */
    private String name;
    /**
     * 联盟总人数
     */
    private int memberNum;
    /**
     * setting
     */
    private UnionSetting unionSetting = new UnionSetting(this);

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbUnion.PbUnionLiteData PbUnionLiteData = PbUnion.PbUnionLiteData.parseFrom(data);
        unionID = PbUnionLiteData.getUnionID();
        level = PbUnionLiteData.getLevel();
        name = PbUnionLiteData.getName();
        memberNum = PbUnionLiteData.getMemberNum();
        unionSetting.load(PbUnionLiteData.getSetting().toByteArray());
        return PbUnionLiteData;
    }

    @Override
    public PbUnion.PbUnionLiteData save() {
        PbUnion.PbUnionLiteData.Builder builder = PbUnion.PbUnionLiteData.newBuilder();
        builder.setUnionID(unionID);
        builder.setLevel(level);
        builder.setName(name);
        builder.setMemberNum(memberNum);
        builder.setSetting(unionSetting.save());
        return builder.build();
    }

    public long getUnionID() {
        return unionID;
    }

    public void setUnionID(long unionID) {
        this.unionID = unionID;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMemberNum() {
        return memberNum;
    }

    public void setMemberNum(int memberNum) {
        this.memberNum = memberNum;
    }

    public UnionSetting getUnionSetting() {
        return unionSetting;
    }

    public void setUnionSetting(UnionSetting unionSetting) {
        this.unionSetting = unionSetting;
    }

    @Override
    public String toString() {
        return "UnionLite{" +
                "unionID=" + unionID +
                ", level=" + level +
                ", name='" + name + '\'' +
                ", memberNum=" + memberNum +
                ", unionSetting=" + unionSetting +
                '}';
    }
}
