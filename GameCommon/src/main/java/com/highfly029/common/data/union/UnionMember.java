package com.highfly029.common.data.union;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.DataBaseAccess;
import com.highfly029.common.data.base.Location;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.common.utils.PbCommonUtils;

/**
 * @ClassName UnionMember
 * @Description 联盟成员数据结构
 * @Author liyunpeng
 **/
public class UnionMember extends DataBaseAccess {
    /**
     * 成员playerID
     */
    private long memberID;
    /**
     * 联盟职位
     */
    private int position;
    /**
     * 成员定位
     */
    private Location location;

    public UnionMember(Union union) {
        super(union);
    }

    public static UnionMember create(Union union) {
        UnionMember unionMember = new UnionMember(union);
        return unionMember;
    }

    public static void back(UnionMember unionMember) {

    }

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbUnion.PbUnionMemberData pbUnionMemberData = PbUnion.PbUnionMemberData.parseFrom(data);
        memberID = pbUnionMemberData.getMemberID();
        position = pbUnionMemberData.getPosition();
        location = PbCommonUtils.locationPb2Obj(pbUnionMemberData.getLocation());
        return pbUnionMemberData;
    }

    @Override
    public PbUnion.PbUnionMemberData save() {
        PbUnion.PbUnionMemberData.Builder builder = PbUnion.PbUnionMemberData.newBuilder();
        builder.setMemberID(memberID);
        builder.setPosition(position);
        if (location != null) {
            builder.setLocation(PbCommonUtils.locationObj2Pb(location));
        }
        return builder.build();
    }

    @Override
    public String getName() {
        return null;
    }

    public void setPosition(int position) {
        super.setChange(true);
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setMemberID(long playerID) {
        super.setChange(true);
        this.memberID = playerID;
    }

    public long getMemberID() {
        return memberID;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        //TODO 成员定位信息更新频繁 不定时存库 如果UnionMember落地数据频繁变化的话、考虑拆分单独的表存储

//        super.setChange(true);
        this.location = location;
    }

    @Override
    public String toString() {
        return "UnionMember{" +
                "memberID=" + memberID +
                ", position=" + position +
                ", location=" + location +
                '}';
    }
}
