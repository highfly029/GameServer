package com.highfly029.common.data.feature;

/**
 * @ClassName LoadBalanceFeature
 * @Description 负载均衡服支持的功能
 * @Author liyunpeng
 **/
public class LoadBalanceFeature {
    /**
     * 代理
     */
    public static final int BROKER = 1;
    /**
     * 选择逻辑服策略
     */
    public static final int LOGIC = 2;
    /**
     * 选择场景服策略
     */
    public static final int SCENE = 3;
}
