package com.highfly029.common.data.base;

import com.highfly029.utils.RandomUtils;

/**
 * @ClassName Vector3
 * @Description 向量工具
 * @Author liyunpeng
 **/
public class Vector3 {
    public static final int FLOAT2INT_SCALE = 1000;
    public static final float INT2FLOAT_SCALE = 1.0f / FLOAT2INT_SCALE;

    public static float epsilon = 1e-06f;
    public static final float TWO_PI = (float) (Math.PI * 2);
    /**
     * 度数转弧度换算单位
     */
    public static final float degreesToRadians = (float) (Math.PI / 180);
    /**
     * 弧度转度数换算单位
     */
    public static final float radiansToDegrees = (float) (180f / Math.PI);
    public static final Vector3 X = Vector3.create().set(1, 0, 0);
    public static final Vector3 Y = Vector3.create().set(0, 1, 0);
    public static final Vector3 Z = Vector3.create().set(0, 0, 1);
    public static final Vector3 ZERO = Vector3.create().set(0, 0, 0);

    public float x;
    public float y;
    public float z;

    public static Vector3 create() {
        Vector3 vector3 = new Vector3();
        return vector3;
    }

    public Vector3() {
    }

    public Vector3(Vector3 old) {
        this.x = old.x;
        this.y = old.y;
        this.z = old.z;
    }

    public Vector3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * 判断两个向量是否相等 浮点数差值小于0.000001就表示相等
     *
     * @param other
     * @return
     */
    public boolean equal(Vector3 other) {
        if (other != null) {
            return Math.abs(this.x - other.x) < epsilon && Math.abs(this.y - other.y) < epsilon && Math.abs(this.z - other.z) < epsilon;
        }
        return false;
    }


    public float[] getFloats() {
        float[] floats = new float[3];
        floats[0] = this.x;
        floats[1] = this.y;
        floats[2] = this.z;
        return floats;
    }


    public static Vector3 createWithFloats(float[] floats) {
        Vector3 vector3 = Vector3.create();
        vector3.x = floats[0];
        vector3.y = floats[1];
        vector3.z = floats[2];
        return vector3;
    }

    //取单位向量
    public Vector3 normalise() {
        float f = (float) Math.sqrt(x * x + y * y + z * z);
        // Will also work for zero-sized vectors, but will change nothing
        if (f > epsilon) {
            f = 1.0f / f;
            x *= f;
            y *= f;
            z *= f;
        }
        return this;
    }

    //取随机方向的单位向量
    public Vector3 randomNormalise() {
        x = RandomUtils.randomFloat() * (RandomUtils.randomBoolean() ? 1 : -1);
        y = RandomUtils.randomFloat() * (RandomUtils.randomBoolean() ? 1 : -1);
        z = RandomUtils.randomFloat() * (RandomUtils.randomBoolean() ? 1 : -1);
        this.normalise();
        return this;
    }

    /**
     * 设置
     *
     * @param x
     * @param y
     * @param z
     */
    public Vector3 set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }

    /**
     * 乘以dis
     *
     * @param dis
     */
    public void multiply(float dis) {
        this.x *= dis;
        this.y *= dis;
        this.z *= dis;
    }

    /**
     * 复制
     *
     * @param vector3
     */
    public void copy(Vector3 vector3) {
        this.x = vector3.x;
        this.y = vector3.y;
        this.z = vector3.z;
    }

    /**
     * 判断是否为单位向量
     *
     * @return
     */
    public boolean isNormalized() {
        return this.x * this.x + this.y * this.y + this.z * this.z == 1.0f;
    }

    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }
}
