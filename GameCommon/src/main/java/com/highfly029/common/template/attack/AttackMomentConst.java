package com.highfly029.common.template.attack;

/**
 * generated by tools, don't modify!
 */

public class AttackMomentConst {
    //在攻击开始时攻击源触发,每次attack触发一次
	public static final byte SourceAttackBegin = 1;
    //每个目标在检测命中前,每个目标触发
	public static final byte EachTargetBeforeCheckHit = 2;
    //命中第一个目标后在出伤害之前攻击源触发
	public static final byte SourceHitFirstTargetBeforeDamage = 3;
    //命中主目标在出伤害之前攻击源触发
	public static final byte SourceHitMainTargetBeforeDamage = 4;
    //命中每一个目标出伤害前攻击源触发
	public static final byte SourceHitEachTargetBeforeDamage = 5;
    //每个目标命中后出伤害前,每个目标触发
	public static final byte EachTargetBeHitBeforeDamage = 6;
    //命中第一个目标后在出伤害之后攻击源触发
	public static final byte SourceHitFirstTargetAfterDamage = 7;
    //命中主目标在出伤害之后攻击源触发
	public static final byte SourceHitMainTargetAfterDamage = 8;
    //命中每一个目标出伤害后攻击源触发
	public static final byte SourceHitEachTargetAfterDamage = 9;
    //每个目标命中后出伤害后,每个目标触发
	public static final byte EachTargetBeHitAfterDamage = 10;
    //攻击源击杀目标时攻击源触发
	public static final byte SourceKill = 11;
    //目标被击杀时目标触发
	public static final byte TargetBeKill = 12;
    //攻击结束时攻击源触发,每次attack触发一次
	public static final byte SourceAttackEnd = 13;

    //所有常量数量
    public static final int count = 13;

}
