package com.highfly029.common.template.model;

import com.highfly029.common.templateBase.BaseTemplate;

/**
 * generated by tools, don't modify!
 */

public class AvatarPartTemplate implements BaseTemplate {
    /**
     * #部件表
     */
    private short modelID;
    /**
     * 
     */
    private short avatarPartType;
    /**
     * 部件id
     */
    private int partID;


    public final short getModelID() {
        return modelID;
    }

    public final short getAvatarPartType() {
        return avatarPartType;
    }

    public final int getPartID() {
        return partID;
    }


}
