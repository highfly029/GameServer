package com.highfly029.common.template.race;

/**
 * generated by tools, don't modify!
 */

public class RaceConst {
    //战士
	public static final byte Warrior = 1;
    //法师
	public static final byte Mage = 2;

    //所有常量数量
    public static final int count = 2;

}
