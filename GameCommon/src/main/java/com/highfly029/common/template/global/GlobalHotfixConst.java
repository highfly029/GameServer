package com.highfly029.common.template.global;

import com.highfly029.common.templateBase.BaseConst;

/**
 * generated by tools, don't modify!
 */

public class GlobalHotfixConst implements BaseConst {
    //desc1
	private String f1;
    //desc2
	private String[] f2;
    //desc3
	private boolean f3;
    //desc4
	private boolean[] f4;
    //desc5
	private byte f5;
    //desc6
	private byte[] f6;
    //desc7
	private short f7;
    //desc8
	private short[] f8;
    //desc9
	private int[] f9;
    //desc10
	private long f10;
    //desc11
	private long[] f11;

    //所有常量数量
    public static final int count = 11;

    public final String getF1() {
        return f1;
    }

    public final String[] getF2() {
        return f2;
    }

    public final boolean isF3() {
        return f3;
    }

    public final boolean[] getF4() {
        return f4;
    }

    public final byte getF5() {
        return f5;
    }

    public final byte[] getF6() {
        return f6;
    }

    public final short getF7() {
        return f7;
    }

    public final short[] getF8() {
        return f8;
    }

    public final int[] getF9() {
        return f9;
    }

    public final long getF10() {
        return f10;
    }

    public final long[] getF11() {
        return f11;
    }

}
