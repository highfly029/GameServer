package com.highfly029.common.template.dungeonRoom;

import com.highfly029.common.templateBase.BaseTemplate;

/**
 * generated by tools, don't modify!
 */

public class DungeonRoomTemplate implements BaseTemplate {
    /**
     * 副本ID
     */
    private int dungeonID;
    /**
     * 副本房间id
     */
    private int dungeonRoomID;
    /**
     * 场景id
     */
    private int sceneID;
    /**
     * 是否是最后一个副本场景
     */
    private boolean isLastScene;
    /**
     * 进入此场景时刷新怪物
     */
    private int[] enterSceneRefreshMonster;
    /**
     * 根据时间刷新怪物
     */
    private int[][] timeRefreshMonster;
    /**
     * 根据怪物数量小于等于num刷新怪物
     */
    private int[][] lessEqualNumRefreshMonster;


    public final int getDungeonID() {
        return dungeonID;
    }

    public final int getDungeonRoomID() {
        return dungeonRoomID;
    }

    public final int getSceneID() {
        return sceneID;
    }

    public final boolean isLastScene() {
        return isLastScene;
    }

    public final int[] getEnterSceneRefreshMonster() {
        return enterSceneRefreshMonster;
    }

    public final int[][] getTimeRefreshMonster() {
        return timeRefreshMonster;
    }

    public final int[][] getLessEqualNumRefreshMonster() {
        return lessEqualNumRefreshMonster;
    }


}
