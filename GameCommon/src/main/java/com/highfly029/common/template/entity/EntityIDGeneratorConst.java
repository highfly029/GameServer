package com.highfly029.common.template.entity;

/**
 * generated by tools, don't modify!
 */

public class EntityIDGeneratorConst {
    //配表entityID最小值,最大值小于editorEntityIDMin
	public static final int ExcelEntityIDMin = 1;
    //编辑器entityID最小值,最大值小于playerEntityIDMin
	public static final int EditorEntityIDMin = 10000;
    //玩家entityID最小值,小于otherEntityIDMin
	public static final int PlayerEntityIDMin = 100000;
    //非玩家entityID最小值,小于Inter.Max
	public static final int OtherEntityIDMin = 1000000;

    //所有常量数量
    public static final int count = 4;

}
