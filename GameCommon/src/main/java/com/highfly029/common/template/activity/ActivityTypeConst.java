package com.highfly029.common.template.activity;

/**
 * generated by tools, don't modify!
 */

public class ActivityTypeConst {
    //test1
	public static final int ActivityTest1 = 1;
    //test2
	public static final int ActivityTest2 = 2;
    //test3
	public static final int ActivityTest3 = 3;

    //所有常量数量
    public static final int count = 3;

}
