package com.highfly029.common.template.playerAction;

/**
 * generated by tools, don't modify!
 */

public class PlayerActionConst {
    //arg1:属性id,arg2:属性值
	public static final int AddAttribute = 1;
    //更改avatar部件 avatarType, partID
	public static final int AvatarChangePart = 2;

    //所有常量数量
    public static final int count = 2;

}
