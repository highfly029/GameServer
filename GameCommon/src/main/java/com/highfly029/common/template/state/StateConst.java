package com.highfly029.common.template.state;

/**
 * generated by tools, don't modify!
 */

public class StateConst {
    //死亡
	public static final short Death = 0;
    //处于战斗状态
	public static final short OnFight = 1;
    //不可被选择(蝗虫群)
	public static final short CantBeTarget = 2;
    //不可移动
	public static final short CantMove = 3;
    //不可转身
	public static final short CantTurn = 4;
    //不可攻击
	public static final short CantAttack = 5;
    //不可施法
	public static final short CantCast = 6;
    //眩晕(等价于以上4个)
	public static final short Vertigo = 7;
    //控制免疫
	public static final short ControlImmune = 8;
    //伤害免疫
	public static final short DamageImmune = 9;
    //敌对不可选(无敌)
	public static final short CantBeEnemyTarget = 10;
    //完整无敌(等价于以上3个加不可被伤害击杀)
	public static final short Invincible = 11;
    //绝对命中(克制绝对闪避)
	public static final short AbsHit = 12;
    //绝对闪避
	public static final short AbsDodge = 13;
    //绝对暴击
	public static final short AbsCritical = 14;
    //绝对韧性(克制绝对暴击)
	public static final short AbsTenacity = 15;
    //隐身
	public static final short Hide = 16;
    //安全区(mmo)
	public static final short SafeRegion = 17;
    //吸收子弹(盾墙)
	public static final short AbsorbBullet = 18;
    //不可被攻击(attack跳过)
	public static final short CantBeAttackTarget = 19;
    //不可被伤害击杀(暗牧-薄葬)
	public static final short CantBeKillByDamage = 20;
    //不可被治疗
	public static final short CantBeHeal = 21;
    //物理免疫
	public static final short PhysicalImmune = 22;
    //魔法免疫
	public static final short MagicImmune = 23;

    //所有常量数量
    public static final int count = 24;

}
