package com.highfly029.common.template.skill;

/**
 * generated by tools, don't modify!
 */

public class SkillTargetTypeConst {
    //目标实体对象ID,单体攻击
	public static final byte TargetEntityID = 1;
    //目标位置为中心点的范围,方向为连线方向
	public static final byte TargetPosition = 2;
    //目标方向
	public static final byte TargetDirection = 3;
    //目标实体对象的位置为中心的范围,方向为连线方向
	public static final byte TargetEntityIDPos = 4;
    //目标实体对象的位置为中心的范围,方向为目标对象方向
	public static final byte TargetEntityIDPosDir = 5;

    //所有常量数量
    public static final int count = 5;

}
