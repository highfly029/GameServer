package com.highfly029.common.template.commonType;

/**
 * generated by tools, don't modify!
 */

public class PushTypeConst {
    //不推送
	public static final byte Off = 0;
    //只推送自己
	public static final byte Self = 1;
    //只推送其他玩家
	public static final byte Other = 2;
    //全都推送
	public static final byte All = 3;
    //只推送给主人
	public static final byte Master = 4;

    //所有常量数量
    public static final int count = 5;

}
