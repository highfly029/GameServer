package com.highfly029.common.template.attack;

/**
 * generated by tools, don't modify!
 */

public class SelectTargetTypeConst {
    //随机选择
	public static final byte SelectRandom = 1;
    //选择距离中心点最近的
	public static final byte SelectNearest = 2;
    //选择距离中心点最近的一个
	public static final byte SelectNearestOne = 3;
    //随机一个
	public static final byte SelectRandomOne = 4;

    //所有常量数量
    public static final int count = 4;

}
