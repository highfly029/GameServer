package com.highfly029.common.redis;

/**
 * @ClassName PlayerHashKey
 * @Description 玩家在redis内的Hash key
 * @Author liyunpeng
 **/
public abstract class PlayerHashKey {
    /**
     * 玩家所在服id
     */
    public static final String gameServerID = "gameServerID";
    /**
     * 玩家状态
     */
    public static final String state = "state";
}
