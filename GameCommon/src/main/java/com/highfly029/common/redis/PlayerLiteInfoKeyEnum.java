package com.highfly029.common.redis;

/**
 * @ClassName PlayerLiteInfoKeyEnum
 * @Description PlayerLiteInfoKeyEnum
 * @Author liyunpeng
 **/
public enum PlayerLiteInfoKeyEnum {
    /**
     * 玩家ID
     */
    PLAYER_ID("playerID"),
    /**
     * 玩家名字
     */
    PLAYER_NAME("name"),
    /**
     * 玩家等级
     */
    PLAYER_LEVEL("level"),
    /**
     * 玩家联盟ID
     */
    PLAYER_UNION_ID("unionID"),
    /**
     * 玩家联盟名字
     */
    PLAYER_UNION_NAME("unionName"),

    ;


    private final String value;

    PlayerLiteInfoKeyEnum(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
