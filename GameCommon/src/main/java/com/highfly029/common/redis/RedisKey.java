package com.highfly029.common.redis;

/**
 * redis主键
 */
public enum RedisKey {

    /**
     * 排行榜key前缀
     */
    REDIS_PREFIX_KEY("r:"),

    /**
     * 玩家简版信息
     */
    PLAYER_LITE_INFO("pli:"),

    /**
     * 玩家缓存数据
     */
    PLAYER_CACHE_DATA("pd:"),

    /**
     * 玩家离线消息队列
     */
    PLAYER_OFFLINE_MSG("pom:"),

    ;

    private final String value;

    RedisKey(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
