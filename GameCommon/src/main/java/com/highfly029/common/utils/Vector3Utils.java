package com.highfly029.common.utils;


import com.highfly029.common.data.base.Vector3;

/**
 * @ClassName Vector3Utils
 * @Description 向量计算工具
 * @Author liyunpeng
 **/
public final class Vector3Utils {

    /**
     * pos 延dir朝向移动 distance米
     *
     * @param retVector     返回值
     * @param pos
     * @param dir
     * @param needNormalise 是否需要单位化
     * @param distance
     * @return
     */
    public static void moveTowardsPos(Vector3 retVector, Vector3 pos, Vector3 dir, boolean needNormalise, float distance) {
        retVector.copy(dir);
        if (needNormalise) {
            retVector.normalise();
        }
        retVector.multiply(distance);
        add3D(retVector, pos, retVector);
    }

    /**
     * 向量取反
     *
     * @param retVector 返回值
     * @param vector3
     */
    public static void negative(Vector3 retVector, Vector3 vector3) {
        retVector.x = -vector3.x;
        retVector.y = -vector3.y;
        retVector.z = -vector3.z;
    }

    /**
     * 加
     *
     * @param a
     * @param b
     * @return
     */
    public static void add3D(Vector3 retVector, Vector3 a, Vector3 b) {
        retVector.x = a.x + b.x;
        retVector.y = a.y + b.y;
        retVector.z = a.z + b.z;
    }

    /**
     * 减
     *
     * @param retVector 返回值
     * @param a
     * @param b
     * @return
     */
    public static void sub3D(Vector3 retVector, Vector3 a, Vector3 b) {
        retVector.x = a.x - b.x;
        retVector.y = a.y - b.y;
        retVector.z = a.z - b.z;
    }

    /**
     * 乘
     *
     * @param a
     * @param dis
     * @return
     */
    public static Vector3 multiply3D(Vector3 a, float dis) {
        return Vector3.create().set(a.x * dis, a.y * dis, a.z * dis);
    }

    /**
     * 差乘
     *
     * @param retVector 返回值
     * @param a
     * @param b
     * @return
     */
    public static void cross3D(Vector3 retVector, Vector3 a, Vector3 b) {
        retVector.set(a.y * b.z - a.z * b.y,
                a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
    }

    /**
     * 叉乘
     *
     * @param a
     * @param b
     * @return
     */
    public static float cross3D(Vector3 a, Vector3 b) {
        return a.y * b.z - a.z * b.y + a.z * b.x - a.x * b.z + a.x * b.y - a.y * b.x;
    }

    /**
     * 点乘 点积
     *
     * @param a
     * @param b
     * @return
     */
    public static float dot(Vector3 a, Vector3 b) {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    /**
     * 在2D平面内b是否在a的顺时针方向
     *
     * @param a
     * @param b
     * @return
     */
    public static boolean isClockwise2D(Vector3 a, Vector3 b) {
        //也可以直接调用cross3D
        return -a.x * b.z + a.z * b.x > 0;
    }

    /**
     * 距离平方
     *
     * @param a
     * @param b
     * @return
     */
    public static float distance3DSquare(Vector3 a, Vector3 b) {
        float tmp;
        return (tmp = b.x - a.x) * tmp + (tmp = b.y - a.y) * tmp + (tmp = b.z - a.z) * tmp;
    }

    /**
     * 距离
     *
     * @param a
     * @param b
     * @return
     */
    public static float distance3D(Vector3 a, Vector3 b) {
        return (float) Math.sqrt(distance3DSquare(a, b));
    }

    /**
     * 返回a到b的单位方向
     *
     * @param retVector
     * @param a
     * @param b
     */
    public static void getDir(Vector3 retVector, Vector3 a, Vector3 b) {
        Vector3Utils.sub3D(retVector, b, a);
        retVector.normalise();
    }

    public static void main(String[] args) {
        Vector3 pos = Vector3.create();
        pos.set(10, 0, 10);
        Vector3 targetPos = Vector3.create();
        targetPos.set(100, 0, 100);
        Vector3 dir = Vector3.create();
        Vector3Utils.getDir(dir, pos, targetPos);
        Vector3 tmp = Vector3.create();
        float dis = 10f;
        for (int i = 0; i < 100; i++) {
            Vector3Utils.moveTowardsPos(tmp, pos, dir, false, dis);
            pos.copy(tmp);
        }
        System.out.println(pos);
    }

}
