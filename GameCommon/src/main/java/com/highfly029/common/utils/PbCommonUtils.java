package com.highfly029.common.utils;

import java.util.ArrayList;
import java.util.List;

import com.highfly029.common.data.auction.AuctionItemData;
import com.highfly029.common.data.base.Location;
import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.data.playerLiteInfo.PlayerLiteInfo;
import com.highfly029.common.protocol.packet.PbAuction;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.utils.collection.IntIntMap;

/**
 * @ClassName PbUtils
 * @Description Pb转换工具
 * @Author liyunpeng
 **/
public class PbCommonUtils {

    //定位信息
    public static PbCommon.PbLocation locationObj2Pb(Location location) {
        PbCommon.PbLocation.Builder builder = PbCommon.PbLocation.newBuilder();
        builder.setGameDbID(location.getGameDbID());
        builder.setLogicServerID(location.getLogicServerID());
        builder.setLogicWorldIndex(location.getLogicWorldIndex());
        builder.setSceneServerID(location.getSceneServerID());
        builder.setSceneWorldIndex(location.getSceneWorldIndex());
        builder.setSceneID(location.getSceneID());
        builder.setLineID(location.getLineID());
        builder.setInstanceID(location.getInstanceID());
        builder.setState(location.getState());
        builder.setPlayerID(location.getPlayerID());
        if (location.getCurPos() != null) {
            builder.setCurPos(vector3Obj2Pb(location.getCurPos()));
        }
        return builder.build();
    }

    public static Location locationPb2Obj(PbCommon.PbLocation pbLocation) {
        Location location = Location.create();
        location.setGameDbID(pbLocation.getGameDbID());
        location.setLogicServerID(pbLocation.getLogicServerID());
        location.setLogicWorldIndex(pbLocation.getLogicWorldIndex());
        location.setSceneServerID(pbLocation.getSceneServerID());
        location.setSceneWorldIndex(pbLocation.getSceneWorldIndex());
        location.setSceneID(pbLocation.getSceneID());
        location.setLineID(pbLocation.getLineID());
        location.setInstanceID(pbLocation.getInstanceID());
        location.setState(pbLocation.getState());
        location.setPlayerID(pbLocation.getPlayerID());
        if (pbLocation.hasCurPos()) {
            location.setCurPos(vector3Pb2Obj(pbLocation.getCurPos()));
        }
        return location;
    }

    //向量信息
    public static PbCommon.PbVector vector3Obj2Pb(Vector3 vector3) {
        PbCommon.PbVector.Builder builder = PbCommon.PbVector.newBuilder();
        builder.setX((int) (vector3.x * Vector3.FLOAT2INT_SCALE));
        builder.setY((int) (vector3.y * Vector3.FLOAT2INT_SCALE));
        builder.setZ((int) (vector3.z * Vector3.FLOAT2INT_SCALE));
        return builder.build();
    }

    public static Vector3 vector3Pb2Obj(PbCommon.PbVector pbVector) {
        Vector3 vector3 = Vector3.create();
        vector3.x = pbVector.getX() * Vector3.INT2FLOAT_SCALE;
        vector3.y = pbVector.getY() * Vector3.INT2FLOAT_SCALE;
        vector3.z = pbVector.getZ() * Vector3.INT2FLOAT_SCALE;
        return vector3;
    }

    public static IntIntMap keyValueList2IntIntMap(List<PbCommon.PbKeyValue> list) {
        IntIntMap map = new IntIntMap();
        for (int i = 0; i < list.size(); i++) {
            PbCommon.PbKeyValue pbKeyValue = list.get(i);
            map.put(pbKeyValue.getIntKey(), pbKeyValue.getIntValue());
        }
        return map;
    }

    public static List<PbCommon.PbKeyValue> keyValueIntIntMap2List(IntIntMap map) {
        List<PbCommon.PbKeyValue> list = new ArrayList<>();
        map.foreachImmutable((k, v) -> {
            PbCommon.PbKeyValue.Builder kvBuilder = PbCommon.PbKeyValue.newBuilder();
            kvBuilder.setIntKey(k);
            kvBuilder.setIntValue(v);
            list.add(kvBuilder.build());
        });
        return list;
    }

    //玩家精简信息
    public static PbCommon.PbPlayerLiteInfoData playerLiteInfoObj2Pb(PlayerLiteInfo playerLiteInfo) {
        PbCommon.PbPlayerLiteInfoData.Builder builder = PbCommon.PbPlayerLiteInfoData.newBuilder();
        builder.setPlayerID(playerLiteInfo.getPlayerID());
        builder.setLevel(playerLiteInfo.getLevel());
        builder.setName(playerLiteInfo.getName());
        builder.setUnionID(playerLiteInfo.getUnionID());
        if (playerLiteInfo.getUnionName() != null) {
            builder.setUnionName(playerLiteInfo.getUnionName());
        }
        return builder.build();
    }

    public static PlayerLiteInfo playerLiteInfoPb2Obj(PbCommon.PbPlayerLiteInfoData pbPlayerLiteInfoData) {
        PlayerLiteInfo playerLiteInfo = new PlayerLiteInfo();
        playerLiteInfo.setPlayerID(pbPlayerLiteInfoData.getPlayerID());
        playerLiteInfo.setLevel(pbPlayerLiteInfoData.getLevel());
        playerLiteInfo.setName(pbPlayerLiteInfoData.getName());
        playerLiteInfo.setUnionID(pbPlayerLiteInfoData.getUnionID());
        playerLiteInfo.setUnionName(pbPlayerLiteInfoData.getUnionName());
        return playerLiteInfo;
    }

    //拍卖行物品数据
    public static AuctionItemData auctionItemDataPb2Obj(PbAuction.PbAuctionItemData pbAuctionItemData) {
        AuctionItemData auctionItemData = new AuctionItemData();
        auctionItemData.setId(pbAuctionItemData.getId());
        auctionItemData.setItemID(pbAuctionItemData.getItemID());
        auctionItemData.setItemNum(pbAuctionItemData.getItemNum());
        auctionItemData.setPrice(pbAuctionItemData.getPrice());
        auctionItemData.setShelfTime(pbAuctionItemData.getShelfTime());
        auctionItemData.setItemData(pbAuctionItemData.getItemByteData());
        return auctionItemData;
    }

    public static PbAuction.PbAuctionItemData auctionItemDataObj2Pb(AuctionItemData auctionItemData) {
        PbAuction.PbAuctionItemData.Builder builder = PbAuction.PbAuctionItemData.newBuilder();
        builder.setId(auctionItemData.getId());
        builder.setItemID(auctionItemData.getItemID());
        builder.setItemNum(auctionItemData.getItemNum());
        builder.setPrice(auctionItemData.getPrice());
        builder.setShelfTime(auctionItemData.getShelfTime());
        if (auctionItemData.getItemData() != null) {
            builder.setItemByteData(auctionItemData.getItemData());
        }
        return builder.build();
    }

}
