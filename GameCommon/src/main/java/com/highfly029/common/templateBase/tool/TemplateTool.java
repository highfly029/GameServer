package com.highfly029.common.templateBase.tool;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.highfly029.common.templateBase.BaseConst;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.basetype.Attr;
import com.highfly029.common.templateBase.basetype.Reward;
import com.highfly029.utils.CompressUtils;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.JsonUtils;
import com.highfly029.utils.ReflectionUtils;
import com.highfly029.utils.StringUtils;
import com.highfly029.utils.SysUtils;
import com.highfly029.utils.collection.ObjObjMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName TemplateTool
 * @Description 模板工具
 * @Author liyunpeng
 **/
public class TemplateTool {
    public static final ObjObjMap<Class, ObjectList<BaseTemplate>> dataTemplates = new ObjObjMap<>(Class[]::new, ObjectList[]::new);
    //直接使用
    public static final ObjObjMap<Class, BaseConst> constDataMap = new ObjObjMap<>(Class[]::new, BaseConst[]::new);

    private static final String CSV_SEP = ",";
    private static final String COMMA = "\\:";
    private static final String VERTICAL = "\\|";

    //非贪婪匹配
    private static final Pattern csvPattern = Pattern.compile("\\^(.*?)\\$");

    private static final String packageName = "com.highfly029.common.template";

    /**
     * 加载所有配置
     *
     * @param dirName 目录名字
     */
    public static void load(String dirName, boolean useBinDataFile) {
        loadCSV(dirName, useBinDataFile);
    }

    @Deprecated
    private static void loadJson() {
        if (ConfigPropUtils.getBoolValue("useBinDataFile")) {
            initWithJsonBinFile();
        } else {
            initWithJsonFile();
        }
    }

    private static void loadCSV(String dirName, boolean useBinDataFile) {
        if (useBinDataFile) {
            initWithCSVBinFile(dirName);
        } else {
            initDefaultCSVFile(dirName);
        }
    }

    /**
     * 清理所有配置
     */
    public static void clear() {
        dataTemplates.clear();
    }

    private static void initWithJsonBinFile() {
        ObjectList<Class> classList = ReflectionUtils.getAllClassByInterface(BaseTemplate.class, packageName);
        String classPathStr = ConfigPropUtils.getConfigPath();
        System.out.println("classPath=" + classPathStr + ", subClassSize=" + classList.size());
        String binFileName = classPathStr + "/json/serverJson.bin";
        File binFile = new File(binFileName);
        long length = binFile.length();
        byte[] allByteData = new byte[(int) length];
        byte[] buffer = new byte[1024];
        int pos = 0;
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(binFileName))) {
            int bytesRead;
            while ((bytesRead = bufferedInputStream.read(buffer)) != -1) {
                System.arraycopy(buffer, 0, allByteData, pos, bytesRead);
                pos += bytesRead;
            }
        } catch (Exception e) {
            e.printStackTrace();
            SysUtils.exit();
        }
        ObjObjMap<String, ObjObjMap<String, String>> binDataMap = new ObjObjMap<>(String[]::new, ObjObjMap[]::new);

        try {
            byte[] unCompressBinBytes = CompressUtils.snappyUncompress(allByteData);
            String binDataStr = new String(unCompressBinBytes);
            int startIndex;
            int endIndex;
            int index = 0;
            while (((startIndex = binDataStr.indexOf("^", index)) != -1) && ((endIndex = binDataStr.indexOf("$", index)) != -1)) {
                String header = binDataStr.substring(startIndex + 1, endIndex);
                int contentIndex = binDataStr.indexOf("^", endIndex);
                String content;
                if (contentIndex == -1) {
                    content = binDataStr.substring(endIndex + 1);
                } else {
                    content = binDataStr.substring(endIndex + 1, contentIndex);
                }
                String[] array = header.split("/");
                ObjObjMap<String, String> subBinDataMap = binDataMap.get(array[0]);
                if (subBinDataMap == null) {
                    subBinDataMap = new ObjObjMap<>(String[]::new, String[]::new);
                    binDataMap.put(array[0], subBinDataMap);
                }
                subBinDataMap.put(array[1], content);
                index = contentIndex;
                if (contentIndex == -1) {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            SysUtils.exit();
        }

        Class[] values = classList.getValues();
        Class cls;
        for (int i = 0, size = classList.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                String[] array = cls.getName().split("\\.");
                String dir = array[array.length - 2];
                String name = array[array.length - 1];
                name = name.substring(0, name.lastIndexOf("Template"));
//                System.out.println("dir="+ dir + " name=" + name);
                ObjObjMap<String, String> subBinDataMap = binDataMap.get(dir);
                if (subBinDataMap == null) {
                    continue;
                }
                String content = subBinDataMap.get(name);
                if (content == null) {
                    continue;
                }
                try {
                    JsonParser parser = new JsonParser();
                    JsonArray jsonArray = parser.parse(content).getAsJsonArray();
                    ObjectList<BaseTemplate> list = new ObjectList<>(jsonArray.size(), BaseTemplate[]::new);
                    for (JsonElement element : jsonArray) {
                        BaseTemplate template = (BaseTemplate) ReflectionUtils.getNewInstance(cls);
                        setJsonDataByField(cls, template, element);
                        list.add(template);
                    }
                    dataTemplates.put(cls, list);
                } catch (Exception ee) {
                    ee.printStackTrace();
                    SysUtils.exit();
                }
            }
        }
        System.out.println("load binFile data success dataSize=" + dataTemplates.size());
    }

    private static void initWithJsonFile() {
        ObjectList<Class> classList = ReflectionUtils.getAllClassByInterface(BaseTemplate.class, packageName);
        String classPathStr = ConfigPropUtils.getConfigPath();
        System.out.println("classPath=" + classPathStr + ", subClassSize=" + classList.size());
        Class[] values = classList.getValues();
        Class cls;
        for (int i = 0, size = classList.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                String[] array = cls.getName().split("\\.");
                String dir = array[array.length - 2];
                String name = array[array.length - 1];
                name = name.substring(0, name.lastIndexOf("Template"));
                String jsonFileName = classPathStr + "/json/" + dir + "/" + name + ".json";
//                System.out.println("cls=" + cls.getName() + ", clsType=" + cls.getTypeName() + ", path=" + jsonFileName);
                File jsonFile = new File(jsonFileName);
                if (!jsonFile.exists()) {
                    continue;
                }
                try {
                    try (FileReader fileReader = new FileReader(jsonFileName)) {
                        JsonReader reader = new JsonReader(fileReader);
                        JsonParser parser = new JsonParser();
                        JsonArray jsonArray = parser.parse(reader).getAsJsonArray();

                        ObjectList<BaseTemplate> list = new ObjectList<>(jsonArray.size(), BaseTemplate[]::new);
                        for (JsonElement element : jsonArray) {
                            BaseTemplate template = (BaseTemplate) ReflectionUtils.getNewInstance(cls);
                            setJsonDataByField(cls, template, element);
                            list.add(template);
                        }
                        dataTemplates.put(cls, list);
                        reader.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        SysUtils.exit();
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();
                    SysUtils.exit();
                }
            }
        }
        System.out.println("load jsonFile data success dataSize=" + dataTemplates.size());
    }

    /**
     * 初始化csv文件
     */
    private static void initDefaultCSVFile(String dirName) {
        String classPathStr = ConfigPropUtils.getConfigPath();
        initTemplateWithCSVFile(dirName, classPathStr);
        initConstWithCSVFile(dirName, classPathStr);
    }

    private static void initTemplateWithCSVFile(String dirName, String classPathStr) {
        ObjectList<Class> classList = ReflectionUtils.getAllClassByInterface(BaseTemplate.class, packageName);
        System.out.println("classPath=" + classPathStr + ", subClassSize=" + classList.size());
        Class[] values = classList.getValues();
        Class cls;
        for (int i = 0, size = classList.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                String[] array = cls.getName().split("\\.");
                String name = getArrayPath(array);
                name = name.substring(0, name.lastIndexOf("Template"));
                String csvFileName = classPathStr + "/" + dirName + "/" + name + ".csv";
//                System.out.println("cls=" + cls.getName() + ", clsType=" + cls.getTypeName() + ", path=" + csvFileName);
                File csvFile = new File(csvFileName);
                if (!csvFile.exists()) {
                    continue;
                }
//                System.out.println("cls=" + cls.getName() + ", clsType=" + cls.getTypeName() + ", path=" + csvFileName);
                try (BufferedReader bufferedReader = new BufferedReader(new FileReader(csvFile));) {
                    String lineData;

                    int lineNum = 0;
                    String[] headers = null;
                    ObjectList<BaseTemplate> list = new ObjectList<>(BaseTemplate[]::new);
                    while ((lineData = bufferedReader.readLine()) != null) {
                        lineNum++;
                        if (lineNum == 1) {
                            headers = lineData.split(CSV_SEP);
                        } else {
                            String[] rows = lineData.split(CSV_SEP);
                            HashMap<String, String> dataMap = new HashMap<>();
                            for (int j = 0, len = rows.length; j < len; j++) {
                                dataMap.put(headers[j], rows[j]);
                            }
                            BaseTemplate template = (BaseTemplate) ReflectionUtils.getNewInstance(cls);
                            setDataByField(cls, template, dataMap);
                            list.add(template);
                        }
                    }
                    dataTemplates.put(cls, list);
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }
        }

    }

    private static void initConstWithCSVFile(String dirName, String classPathStr) {
        ObjectList<Class> classList = ReflectionUtils.getAllClassByInterface(BaseConst.class, packageName);
        Class[] values = classList.getValues();
        Class cls;
        for (int i = 0, size = classList.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                String[] array = cls.getName().split("\\.");
                String name = getArrayPath(array);
                String csvFileName = classPathStr + "/" + dirName + "/" + name + ".csv";
//                System.out.println("cls=" + cls.getName() + ", clsType=" + cls.getTypeName() + ", path=" + csvFileName);
                File csvFile = new File(csvFileName);
                if (!csvFile.exists()) {
                    continue;
                }
                try (BufferedReader bufferedReader = new BufferedReader(new FileReader(csvFile));) {
                    String lineData;

                    int lineNum = 0;
                    BaseConst baseConst = (BaseConst) ReflectionUtils.getNewInstance(cls);
                    HashMap<String, String> dataMap = new HashMap<>();

                    while ((lineData = bufferedReader.readLine()) != null) {
                        lineNum++;
                        if (lineNum > 1) {
                            String[] rows = lineData.split(CSV_SEP);
                            //filedName, value
                            dataMap.put(rows[0], rows[3]);
                        }
                    }
                    setDataByField(cls, baseConst, dataMap);
                    constDataMap.put(cls, baseConst);
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }
        }
    }

    private static void initWithCSVBinFile(String dirName) {
        ObjectList<Class> classList = ReflectionUtils.getAllClassByInterface(BaseTemplate.class, packageName);
        String classPathStr = ConfigPropUtils.getConfigPath();
        System.out.println("classPath=" + classPathStr + ", subClassSize=" + classList.size());
        String binFileName = classPathStr + "/" + dirName + "/serverCSV.bin";
        File binFile = new File(binFileName);
        if (!binFile.exists()) {
            System.out.println("initWithCSVBinFile not exist " + binFileName);
            return;
        }
        long length = binFile.length();
        byte[] allByteData = new byte[(int) length];
        byte[] buffer = new byte[1024];
        int pos = 0;
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(binFileName))) {
            int bytesRead;
            while ((bytesRead = bufferedInputStream.read(buffer)) != -1) {
                System.arraycopy(buffer, 0, allByteData, pos, bytesRead);
                pos += bytesRead;
            }
        } catch (Exception e) {
            e.printStackTrace();
            SysUtils.exit();
        }

        ObjObjMap<String, String> contentMap = new ObjObjMap<>(String[]::new, String[]::new);
        try {
            byte[] unCompressBinBytes = CompressUtils.snappyUncompress(allByteData);
            String binDataStr = new String(unCompressBinBytes);
            Matcher matcher = csvPattern.matcher(binDataStr);
            int groupCount = matcher.groupCount();
            int contentStartIndex = 0;
            int contentEndIndex;
            String name = null;
            while (matcher.find()) {
                for (int i = 1; i <= groupCount; i++) {
                    //上一个匹配结尾就是内容的开始
                    if (contentStartIndex > 0) {
                        contentEndIndex = matcher.start(i);
                        String content = binDataStr.substring(contentStartIndex + 1, contentEndIndex - 1);
                        contentMap.put(name, content);
                    }
                    name = matcher.group(i);
                    contentStartIndex = matcher.end(i);
                }
            }
            //最后一个数据
            String content = binDataStr.substring(contentStartIndex - 1);
            contentMap.put(name, content);
        } catch (IOException e) {
            e.printStackTrace();
            SysUtils.exit();
        }

        Class[] values = classList.getValues();
        Class cls;
        for (int i = 0, size = classList.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                String[] array = cls.getName().split("\\.");
                String key = getArrayKey(array);
                key = key.substring(0, key.lastIndexOf("Template"));
                String content = contentMap.get(key);
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(content.getBytes(Charset.forName("utf8"))), Charset.forName("utf8")))) {
                    String lineData;
                    int lineNum = 0;
                    String[] headers = null;
                    ObjectList<BaseTemplate> list = new ObjectList<>(BaseTemplate[]::new);
                    while ((lineData = bufferedReader.readLine()) != null) {
                        lineNum++;
                        if (lineNum == 1) {
                            headers = lineData.split(CSV_SEP);
                        } else {
                            String[] rows = lineData.split(CSV_SEP);
                            HashMap<String, String> dataMap = new HashMap<>();
                            for (int j = 0, len = rows.length; j < len; j++) {
                                dataMap.put(headers[j], rows[j]);
                            }
                            BaseTemplate template = (BaseTemplate) ReflectionUtils.getNewInstance(cls);
                            setDataByField(cls, template, dataMap);
                            list.add(template);
                        }
                    }
                    dataTemplates.put(cls, list);
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }
        }

        classList = ReflectionUtils.getAllClassByInterface(BaseConst.class, packageName);
        values = classList.getValues();
        for (int i = 0, size = classList.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                String[] array = cls.getName().split("\\.");
                String content = contentMap.get(getArrayKey(array));
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(content.getBytes(Charset.forName("utf8"))), Charset.forName("utf8")))) {
                    String lineData;

                    int lineNum = 0;
                    BaseConst baseConst = (BaseConst) ReflectionUtils.getNewInstance(cls);
                    HashMap<String, String> dataMap = new HashMap<>();

                    while ((lineData = bufferedReader.readLine()) != null) {
                        lineNum++;
                        if (lineNum > 1) {
                            String[] rows = lineData.split(CSV_SEP);
                            dataMap.put(rows[0], rows[3]);
                        }
                    }
                    setDataByField(cls, baseConst, dataMap);
                    constDataMap.put(cls, baseConst);
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }
        }
    }

    private static String getArrayPath(String[] array) {
        StringBuilder keyBuilder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (i > 3) {
                keyBuilder.append(array[i]).append("/");
            }
        }
        keyBuilder.deleteCharAt(keyBuilder.length() - 1);
        return keyBuilder.toString();
    }

    /**
     * 通过数据获取key
     *
     * @param array
     * @return
     */
    private static String getArrayKey(String[] array) {
        StringBuilder keyBuilder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (i > 3) {
                keyBuilder.append(array[i]).append("_");
            }
        }
        keyBuilder.deleteCharAt(keyBuilder.length() - 1);
        return keyBuilder.toString();
    }

    private static void setJsonDataByField(Class cls, Object object, JsonElement element) throws IllegalAccessException {
        HashMap<String, String> dataMap = JsonUtils.fromJson(element.toString(), HashMap.class);
        setDataByField(cls, object, dataMap);
    }

    private static void setDataByField(Class cls, Object object, HashMap<String, String> dataMap) throws IllegalAccessException {
        for (Field field : cls.getDeclaredFields()) {
            String fieldName = field.getName();
            Class fieldClass = field.getType();
            String value = dataMap.get(fieldName);
            fieldClass.getCanonicalName();
            field.setAccessible(true);
//            System.out.println("field="+fieldName+", fieldType="+fieldClass+", fieldGetTypeName="+fieldClass.getTypeName());
            if (value == null || value.equals("")) {
                //允许空值，为默认值
                continue;
            }
            switch (fieldClass.getTypeName()) {
                case "boolean" -> {
                    if (StringUtils.isInteger(value)) {
                        //用数字表示boolean
                        int valueNum = Integer.parseInt(value);
                        field.setBoolean(object, valueNum != 0);
                    } else {
                        field.setBoolean(object, Boolean.parseBoolean(value));
                    }
                }
                case "boolean[]" -> {
                    String[] valueArray = value.split(COMMA);
                    boolean[] array = new boolean[valueArray.length];
                    for (int i = 0; i < valueArray.length; i++) {
                        if (StringUtils.isInteger(valueArray[i])) {
                            //用数字表示boolean
                            int valueNum = Integer.parseInt(valueArray[i]);
                            array[i] = valueNum != 0;
                        } else {
                            array[i] = Boolean.parseBoolean(valueArray[i]);
                        }
                    }
                    field.set(object, array);
                }
                case "byte" -> {
                    field.setByte(object, Byte.parseByte(value));
                }
                case "byte[]" -> {
                    String[] valueArray = value.split(COMMA);
                    byte[] array = new byte[valueArray.length];
                    for (int i = 0; i < valueArray.length; i++) {
                        array[i] = Byte.parseByte(valueArray[i]);
                    }
                    field.set(object, array);
                }
                case "short" -> {
                    field.setShort(object, Short.parseShort(value));
                }
                case "short[]" -> {
                    String[] valueArray = value.split(COMMA);
                    short[] array = new short[valueArray.length];
                    for (int i = 0; i < valueArray.length; i++) {
                        array[i] = Short.parseShort(valueArray[i]);
                    }
                    field.set(object, array);
                }
                case "int" -> {
                    field.setInt(object, Integer.parseInt(value));
                }
                case "int[]" -> {
                    String[] valueArray = value.split(COMMA);
                    int[] array = new int[valueArray.length];
                    for (int i = 0; i < valueArray.length; i++) {
                        array[i] = Integer.parseInt(valueArray[i]);
                    }
                    field.set(object, array);
                }
                case "int[][]" -> {
                    String[] valueArray = value.split(COMMA);
                    int[][] arrays = new int[valueArray.length][];
                    for (int i = 0; i < valueArray.length; i++) {
                        String subValue = valueArray[i];
                        String[] subValueArray = subValue.split(VERTICAL);
                        arrays[i] = new int[subValueArray.length];
                        for (int j = 0; j < subValueArray.length; j++) {
                            arrays[i][j] = Integer.parseInt(subValueArray[j]);
                        }
                    }
                    field.set(object, arrays);
                }
                case "long" -> {
                    field.setLong(object, Long.parseLong(value));
                }
                case "long[]" -> {
                    String[] valueArray = value.split(COMMA);
                    long[] array = new long[valueArray.length];
                    for (int i = 0; i < valueArray.length; i++) {
                        array[i] = Long.parseLong(valueArray[i]);
                    }
                    field.set(object, array);
                }
                case "float" -> {
                    field.setFloat(object, Float.parseFloat(value));
                }
                case "float[]" -> {
                    String[] valueArray = value.split(COMMA);
                    float[] array = new float[valueArray.length];
                    for (int i = 0; i < valueArray.length; i++) {
                        array[i] = Float.parseFloat(valueArray[i]);
                    }
                    field.set(object, array);
                }
                case "java.lang.String" -> {
                    field.set(object, value);
                }
                case "java.lang.String[]" -> {
                    String[] valueArray = value.split(COMMA);
                    field.set(object, valueArray);
                }
                case "com.highfly029.common.templateBase.basetype.Reward" -> {
                    Reward reward = new Reward(value);
                    field.set(object, reward);
                }
                case "com.highfly029.common.templateBase.basetype.Reward[]" -> {
                    String[] valueArray = value.split(COMMA);
                    Reward[] array = new Reward[valueArray.length];
                    for (int i = 0; i < valueArray.length; i++) {
                        array[i] = new Reward(valueArray[i]);
                    }
                    field.set(object, array);
                }
                case "com.highfly029.common.templateBase.basetype.Attr" -> {
                    Attr attr = new Attr(value);
                    field.set(object, attr);
                }
                case "com.highfly029.common.templateBase.basetype.Attr[]" -> {
                    String[] valueArray = value.split(COMMA);
                    Attr[] array = new Attr[valueArray.length];
                    for (int i = 0; i < valueArray.length; i++) {
                        array[i] = new Attr(valueArray[i]);
                    }
                    field.set(object, array);
                }
                default -> {
                    System.out.println("error cls=" + cls.getName() + ", object=" + object + ", field=" + field.getName() + ", value=" + value);
                    SysUtils.exit();
                }
            }
        }
    }
}
