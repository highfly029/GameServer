package com.highfly029.common.templateBase.basetype;

/**
 * @ClassName Attr
 * @Description 属性
 * @Author liyunpeng
 **/
public class Attr {
    public int id;
    public int value;

    public Attr(String attrStr) {
        String[] arr = attrStr.split("\\|");
        if (arr.length == 2) {
            id = Integer.valueOf(arr[0]);
            value = Integer.valueOf(arr[1]);
        }
    }

    @Override
    public String toString() {
        return "Attr{" +
                "id=" + id +
                ", value=" + value +
                '}';
    }
}
