package com.highfly029.common.templateBase.basetype;

/**
 * @ClassName Reward
 * @Description 奖励
 * @Author liyunpeng
 **/
public class Reward {
    private int type;
    private int id;
    private int num;

    public Reward(String rewardStr) {
        String[] arr = rewardStr.split("\\|");
        if (arr.length == 3) {
            type = Integer.valueOf(arr[0]);
            id = Integer.valueOf(arr[1]);
            num = Integer.valueOf(arr[2]);
        }
    }

    public int getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    public int getNum() {
        return num;
    }

    @Override
    public String toString() {
        return "Reward{" +
                "type=" + type +
                ", id=" + id +
                ", num=" + num +
                '}';
    }
}
