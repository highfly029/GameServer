package com.highfly029.common.redisRank.maker;

import com.highfly029.common.redisRank.RedisRankData;
import com.highfly029.common.redisRank.provider.StandardProvider;

/**
 * @ClassName StandardMaker
 * @Description StandardMaker
 * @Author liyunpeng
 **/
public class StandardMaker<D extends RedisRankData> implements IRankScoreMaker<StandardProvider, D> {
    @Override
    public long encode(StandardProvider provider) {
        return provider.getScore();
    }

    @Override
    public void decode(D data, double score) {
        data.setScore((long) score);
    }
}
