package com.highfly029.common.redisRank;

import com.highfly029.common.redis.RedisKey;

/**
 * @ClassName RedisRankKey
 * @Description RedisRankKey
 * @Author liyunpeng
 **/
public class RedisRankKey {
    /**
     * redis key
     */
    private String key;

    public RedisRankKey(int rankType) {
        key = RedisKey.REDIS_PREFIX_KEY.value() + rankType;
    }

    public RedisRankKey(int rankType, int rankSubType) {
        key = RedisKey.REDIS_PREFIX_KEY.value() + rankType + ":" + rankSubType;
    }

    public RedisRankKey(int... rankArgs) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int arg : rankArgs) {
            stringBuilder.append(arg).append(":");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        key = RedisKey.REDIS_PREFIX_KEY.value() + stringBuilder;
    }

    public String getKey() {
        return key;
    }
}
