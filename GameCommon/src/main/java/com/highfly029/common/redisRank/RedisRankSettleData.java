package com.highfly029.common.redisRank;

/**
 * @ClassName RedisRankSettleData
 * @Description 排行榜结算数据
 * @Author liyunpeng
 **/
public class RedisRankSettleData {
    /**
     * 成员
     */
    private long memberID;
    /**
     * 排名
     */
    private int rank;

    public RedisRankSettleData(long memberID, int rank) {
        this.memberID = memberID;
        this.rank = rank;
    }

    public long getMemberID() {
        return memberID;
    }

    public int getRank() {
        return rank;
    }
}
