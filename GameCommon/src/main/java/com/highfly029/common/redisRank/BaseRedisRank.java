package com.highfly029.common.redisRank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import com.highfly029.common.redisRank.maker.IRankScoreMaker;
import com.highfly029.common.redisRank.provider.IRankScoreProvider;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.RedisClientTool;
import com.highfly029.core.tool.SystemTimeTool;

import io.vertx.redis.client.Command;
import io.vertx.redis.client.Request;
import io.vertx.redis.client.Response;

/**
 * @ClassName BaseRedisRank
 * @Description BaseRedisRank
 * @Author liyunpeng
 **/
public abstract class BaseRedisRank<P extends IRankScoreProvider, D extends RedisRankData> {

    /**
     * 11位整形,秒数时间上限
     * 10000000000 => 2286-11-20 17:46:40
     */
    private static final long SECONDS_CEIL = 10000000000L;

    private final RedisRankKey redisRankKey;

    private final IRankScoreMaker<P, D> rankScoreMaker;

    /**
     * 缓存持续时间
     */
    private static final int CACHED_TIME = 1000 * 60 * 5;

    /**
     * 排行榜数据上限
     */
    private static final int MAX_RANK_NUM = 2;

    /**
     * 缓存过期时间
     */
    private volatile long expireTime = 0L;

    /**
     * 缓存的排行榜数据
     */
    private volatile List<D> cachedRankList = new ArrayList<>();
    /**
     * 所有排行榜集合(不可变集合)
     */
    private static final HashMap<String, BaseRedisRank> map = new HashMap<>();

    public BaseRedisRank(RedisRankKey redisRankKey, IRankScoreMaker<P, D> rankScoreMaker) {
        this.redisRankKey = redisRankKey;
        this.rankScoreMaker = rankScoreMaker;

        map.put(redisRankKey.getKey(), this);
    }

    public static BaseRedisRank getRedisRank(String key) {
        return map.get(key);
    }

    public void getRankList(int worldIndex, Consumer<List<D>> consumer) {
        long now = SystemTimeTool.getMillTime();
        if (now > expireTime) {
            expireTime = now + CACHED_TIME;
            Request request = Request.cmd(Command.ZREVRANGE).arg(redisRankKey.getKey()).arg(0).arg(MAX_RANK_NUM - 1).arg("WITHSCORES");
            RedisClientTool.send(worldIndex, request, (isSuccess, response, errorMsg) -> {
                if (isSuccess) {
                    int size = response.size();
                    List<D> list = new ArrayList<>(size);
                    if (size == 0) {
                        cachedRankList = list;
                        consumer.accept(list);
                        return;
                    }
                    Set<Long> set = new HashSet<>(size);
                    for (int i = 0; i < size; i++) {
                        Response resp = response.get(i);
                        long memberID = resp.get(0).toLong();
                        double score = resp.get(1).toDouble();
                        D data = createRedisRankData();
                        rankScoreMaker.decode(data, score);
                        data.setMemberID(memberID);
                        data.setRedisScore(score);
                        list.add(data);
                        set.add(memberID);

                        LoggerTool.systemLogger.info("getRankList key={}, rank={}, memberID={}, score={}", redisRankKey.getKey(), i, memberID, score);
                    }

                    for (D data : list) {
                        initRedisRankData(worldIndex, data, memberID -> {
                            set.remove(memberID);
                            //全部初始化完成
                            if (set.isEmpty()) {
                                cachedRankList = list;
                                consumer.accept(list);
                            }
                        });
                    }

                } else {
                    LoggerTool.systemLogger.error("getRankList key={} msg={}", redisRankKey.getKey(), errorMsg);
                }
            });
        } else {
            consumer.accept(cachedRankList);
        }

    }

    /**
     * 创建排行数据
     *
     * @return
     */
    protected abstract D createRedisRankData();

    /**
     * 构造排行数据
     */
    protected abstract void initRedisRankData(int worldIndex, D data, Consumer<Long> consumer);

    public void commitScore(int worldIndex, long memberID, double score) {
        Consumer<List<D>> consumer = rankList -> {
            if (cachedRankList.size() >= MAX_RANK_NUM) {
                //分数不足以上榜的直接return
                D lastData = cachedRankList.get(MAX_RANK_NUM - 1);
                if (lastData != null && lastData.getRedisScore() > score) {
                    LoggerTool.systemLogger.warn("commitScore ignore key={} memberID={} score={}", redisRankKey.getKey(), memberID, score);
                    return;
                }
            }

            Request request = Request.cmd(Command.ZADD).arg(redisRankKey.getKey()).arg(score).arg(memberID);

            RedisClientTool.send(worldIndex, request, (isSuccess, response, errorMsg) -> {
                if (!isSuccess) {
                    LoggerTool.systemLogger.error("commitScore key={},memberID={},score={},msg={}", redisRankKey.getKey(), memberID, score, errorMsg);
                }
            });
        };

        //没有获取过排行榜数据，先拉取数据再提交数据
        if (expireTime == 0) {
            getRankList(worldIndex, consumer);
        } else {
            consumer.accept(null);
        }
    }

    public void commitScore(int index, long memberID, P provider) {

        long integerPart = rankScoreMaker.encode(provider);

        //小数部分=秒数上限-当前秒数(200年内都是10位整形),达到相同分数时间先到的排在前面的效果
        long decimalPart = SECONDS_CEIL - SystemTimeTool.getSecondTime();

        if (decimalPart < 0) {
            decimalPart = 0;
        }
        double score = Double.parseDouble(integerPart + "." + decimalPart);
        commitScore(index, memberID, score);
    }

    /**
     * 排行榜结算
     *
     * @param worldIndex
     * @param isClear    是否清除排行榜数据
     */
    public void settlement(int worldIndex, boolean isClear, Consumer<List<RedisRankSettleData>> consumer) {
        Request request = Request.cmd(Command.ZREVRANGE).arg(redisRankKey.getKey()).arg(0).arg(MAX_RANK_NUM - 1).arg("WITHSCORES");
        RedisClientTool.send(worldIndex, request, (isSuccess, response, errorMsg) -> {
            if (isSuccess) {
                int size = response.size();
                List<RedisRankSettleData> list = new ArrayList<>(size);
                if (size > 0) {
                    for (int i = 0; i < size; i++) {
                        Response resp = response.get(i);
                        long memberID = resp.get(0).toLong();
                        RedisRankSettleData settleData = new RedisRankSettleData(memberID, i + 1);
                        list.add(settleData);
                    }
                }
                consumer.accept(list);
                if (isClear) {
                    clearRank(worldIndex);
                }
            } else {
                LoggerTool.systemLogger.error("settlement key={} msg={}", redisRankKey.getKey(), errorMsg);
            }
        });
    }

    /**
     * 清除排行榜数据
     *
     * @param worldIndex
     */
    public void clearRank(int worldIndex) {
        Request request = Request.cmd(Command.DEL).arg(redisRankKey.getKey());
        RedisClientTool.send(worldIndex, request, (isSuccess, response, errorMsg) -> {
            if (isSuccess) {
                //redis清除成功后再清除内存数据
                cachedRankList = new ArrayList<>();
            } else {
                LoggerTool.systemLogger.error("clearRank key={} msg={}", redisRankKey.getKey(), errorMsg);
            }
        });
    }

}
