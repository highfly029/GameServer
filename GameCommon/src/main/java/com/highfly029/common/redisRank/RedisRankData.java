package com.highfly029.common.redisRank;

import com.highfly029.common.protocol.packet.PbActivity;
import com.highfly029.common.protocol.packet.PbRank;

/**
 * @ClassName RedisRankData
 * @Description RedisRankData
 * @Author liyunpeng
 **/
public abstract class RedisRankData {
    /**
     * 排行数据单条记录key
     */
    private long memberID;

    /**
     * redis存储的分数
     */
    private double redisScore;

    /**
     * 排行分数
     */
    private long score;

    /**
     * obj转pb
     *
     * @return
     */
    public abstract PbRank.RankData toPbRankData();

    public long getMemberID() {
        return memberID;
    }

    public void setMemberID(long memberID) {
        this.memberID = memberID;
    }

    public double getRedisScore() {
        return redisScore;
    }

    public void setRedisScore(double redisScore) {
        this.redisScore = redisScore;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }
}
