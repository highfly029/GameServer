package com.highfly029.common.redisRank.maker;

import com.highfly029.common.redisRank.RedisRankData;
import com.highfly029.common.redisRank.provider.IRankScoreProvider;

/**
 * @ClassName IRankScoreMaker
 * @Description 积分构造器
 * @Author liyunpeng
 **/
public interface IRankScoreMaker<P extends IRankScoreProvider, D extends RedisRankData> {

    /**
     * 构造积分
     */
    long encode(P provider);

    /**
     * 解析积分
     */
    void decode(D data, double score);
}
