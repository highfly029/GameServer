package com.highfly029.common.redisRank;

import com.highfly029.common.data.playerLiteInfo.PlayerLiteInfo;
import com.highfly029.common.protocol.packet.PbRank;
import com.highfly029.common.utils.PbCommonUtils;

/**
 * @ClassName PlayerRedisRankData
 * @Description PlayerRedisRankData
 * @Author liyunpeng
 **/
public class PlayerRedisRankData extends RedisRankData {
    /**
     * 玩家精简信息
     */
    private PlayerLiteInfo playerLiteInfo;

    public PlayerLiteInfo getPlayerLiteInfo() {
        return playerLiteInfo;
    }

    public void setPlayerLiteInfo(PlayerLiteInfo playerLiteInfo) {
        this.playerLiteInfo = playerLiteInfo;
    }

    @Override
    public PbRank.RankData toPbRankData() {
        PbRank.RankData.Builder builder = PbRank.RankData.newBuilder();
        builder.setMemberID(getMemberID());
        builder.setScore(getScore());
        builder.setPlayerInfo(PbCommonUtils.playerLiteInfoObj2Pb(playerLiteInfo));
        return builder.build();
    }
}
