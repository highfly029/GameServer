package com.highfly029.common.redisRank.provider;

/**
 * @ClassName StandardProvider
 * @Description StandardProvider
 * @Author liyunpeng
 **/
public class StandardProvider implements IRankScoreProvider {
    private long score;

    private StandardProvider(long score) {
        this.score = score;
    }

    public long getScore() {
        return score;
    }

    public static StandardProvider of(long score) {
        return new StandardProvider(score);
    }
}
