package com.highfly029.common.redisRank;

import java.util.function.Consumer;

import com.highfly029.common.data.playerLiteInfo.PlayerLiteInfoCacheTool;
import com.highfly029.common.redisRank.maker.IRankScoreMaker;
import com.highfly029.common.redisRank.provider.IRankScoreProvider;

/**
 * @ClassName PlayerRedisRank
 * @Description PlayerRedisRank
 * @Author liyunpeng
 **/
public abstract class PlayerRedisRank<P extends IRankScoreProvider, D extends PlayerRedisRankData> extends BaseRedisRank<P, D> {

    public PlayerRedisRank(RedisRankKey redisRankKey, IRankScoreMaker<P, D> rankScoreMaker) {
        super(redisRankKey, rankScoreMaker);
    }

    @Override
    protected void initRedisRankData(int worldIndex, D data, Consumer<Long> consumer) {
        PlayerLiteInfoCacheTool.getInstance().getPlayerLiteInfo(worldIndex, data.getMemberID(), info -> {
            data.setPlayerLiteInfo(info);
            consumer.accept(data.getMemberID());
        });
    }
}
