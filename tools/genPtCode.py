#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import os
import shutil
import subprocess
import platform

def getSeparation():
    osName = platform.system()
    print("osName=" + osName)
    if osName == "Windows":
        return ";"
    else :
        return ":"

def main():
    separation = getSeparation()
    cwd = os.getcwd()
    parent = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

    srcDir = os.path.join(parent, "protobuf/")

    sceneCmd = "java -cp ." + separation +"tools-1.0.0.jar com.highfly029.tools.Main ptcode " + srcDir + " java scene"
    print(sceneCmd)
    ret = subprocess.call(sceneCmd, shell=True)

    clientCmd = "java -cp ." + separation +"tools-1.0.0.jar com.highfly029.tools.Main ptcode " + srcDir + " java client"
    print(clientCmd)
    ret = subprocess.call(clientCmd, shell=True)


    clientGameCmd = "java -cp ." + separation + "tools-1.0.0.jar com.highfly029.tools.Main ptcode " + srcDir + " cs game"
    print(clientGameCmd)
    ret = subprocess.call(clientGameCmd, shell=True)

    GameCommonDir = os.path.join(parent, "GameCommon/src/main/java/com/highfly029/common/protocol")
    shutil.copy("PtCode.java", GameCommonDir)

    PacketParserDir = os.path.join(parent, "PacketParser/src/main/java/com/highfly029/parser/pb")
    TmpParserDir = "tmpParser"
    if os.path.exists(PacketParserDir):
        shutil.rmtree(PacketParserDir)
    shutil.copytree(TmpParserDir, PacketParserDir)

    CommonParserDir = os.path.join(parent, "GameCommon/src/main/java/com/highfly029/parser/pb")
    TmpParserDir = "tmpParser"
    if os.path.exists(CommonParserDir):
        shutil.rmtree(CommonParserDir)
    shutil.copytree(TmpParserDir, CommonParserDir)

    #删除解析文件
    if os.path.exists(TmpParserDir):
        shutil.rmtree(TmpParserDir)

    #删除ptCode文件
    for file in os.listdir(cwd):
        if (file.endswith(".java") or file.endswith(".cs")):
            os.remove(file)

if __name__ == '__main__':
    main()
    os.system("pause")
