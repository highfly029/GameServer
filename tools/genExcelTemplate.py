#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import os
import shutil
import subprocess
import platform

#数据格式 json或者
# format="json"
format="csv"

def getSeparation():
    osName = platform.system()
    print("osName=" + osName)
    if osName == "Windows":
        return ";"
    else :
        return ":"


#拷贝json数据到服务器对应目录
def copyJsonToServerDir(dir):
    dir = dir + "/" + format
    if os.path.exists(dir):
        shutil.rmtree(dir)
    shutil.copytree("server/" + format, dir)
    if format == "json":
        shutil.copy("server/serverJson.bin", dir)
    elif format == "csv":
        shutil.copy("server/serverCSV.bin", dir)
#拷贝服务器模板
def copyTemplateToServer(dir):
    if os.path.exists(dir):
        shutil.rmtree(dir)
    shutil.copytree("server/template", dir)

def main():
    separation = getSeparation()
    cwd = os.getcwd()
    parent = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    if os.path.exists("client"):
        shutil.rmtree("client")

    if os.path.exists("server"):
        shutil.rmtree("server")


    command = "java -cp ." + separation + "tools-1.0.0.jar com.highfly029.tools.Main excel "+format
    print(command)
    result = subprocess.call(command, shell=True)
    print("generate result="  + str(result))
    
    # copy to GameScene
    copyJsonToServerDir(os.path.join(parent, "GameScene/src/main/resources"))

    # copy to GameLogic
    copyJsonToServerDir(os.path.join(parent, "GameLogic/src/main/resources"))

    # copy to GameCenter
    copyJsonToServerDir(os.path.join(parent, "GameCenter/src/main/resources"))

    # copy to GameCient
    copyJsonToServerDir(os.path.join(parent, "GameClient/src/main/resources"))


    copyTemplateToServer(os.path.join(parent, "GameCommon/src/main/java/com/highfly029/common/template"))


    if os.path.exists("client"):
        shutil.rmtree("client")

    if os.path.exists("server"):
        shutil.rmtree("server")	


if __name__ == '__main__':
    main()
    os.system("pause")
