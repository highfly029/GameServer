#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import os
import shutil
import subprocess
import platform

def getSeparation():
    osName = platform.system()
    print("osName=" + osName)
    if osName == "Windows":
        return ";"
    else :
        return ":"

def main():
    separation = getSeparation()
    cwd = os.getcwd()
    parent = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    if os.path.exists("desc"):
        shutil.rmtree("desc")

    os.mkdir("desc")

    pbExe = ""
    if platform.system() == "Windows":
        pbExe = os.path.join(parent, "protobuf/protoc-3.11.4-win64/bin/protoc.exe")
    else :
        pbExe = os.path.join(parent, "protobuf/protoc-3.11.4-osx-x86_64/bin/protoc")

    gameProtoPath = os.path.join(parent, "protobuf/gameProto")
    for file in os.listdir(gameProtoPath):
        (filename,extension) = os.path.splitext(file)
        command = pbExe + " --proto_path=" + gameProtoPath + " --descriptor_set_out=desc/" + filename + ".desc " + file
        print(command)
        ret = subprocess.call(command, shell=True)

    javaCommand = "java -cp ." + separation + "tools-1.0.0.jar com.highfly029.tools.Main protobuf com.highfly029.scene"
    ret = subprocess.call(javaCommand, shell=True)

    dataInDbDir = os.path.join(parent, "GameScene/src/main/java/com/highfly029/scene/dataInDb")
    if os.path.exists(dataInDbDir):
        shutil.rmtree(dataInDbDir)
    shutil.copytree("dataInDb", dataInDbDir)

    if os.path.exists("desc"):
        shutil.rmtree("desc")

    if os.path.exists("dataInDb"):
        shutil.rmtree("dataInDb")	


if __name__ == '__main__':
    main()
    os.system("pause")
