#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import os
import shutil
import subprocess
import platform

def getSeparation():
    osName = platform.system()
    print("osName=" + osName)
    if osName == "Windows":
        return ";"
    else :
        return ":"

def main():
    separation = getSeparation()
    cwd = os.getcwd()
    parent = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

    inputDir = os.path.join(cwd, "recastInput")
    outputDir = os.path.join(cwd, "recastOutput")


    cmd = "java -cp ." + separation +"tools-1.0.0.jar com.highfly029.tools.Main recast " + inputDir + " " + outputDir
    print(cmd)
    ret = subprocess.call(cmd, shell=True)

if __name__ == '__main__':
    main()
    os.system("pause")
