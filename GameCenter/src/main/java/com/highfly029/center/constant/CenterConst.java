package com.highfly029.center.constant;

import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.IntList;

/**
 * @ClassName CenterConst
 * @Description CenterConst
 * @Author liyunpeng
 **/
public class CenterConst {
    /**
     * 联盟子世界索引集合
     */
    private static IntList unionWorldList = new IntList();
    /**
     * 队伍子世界索引集合
     */
    private static IntList teamWorldList = new IntList();
    /**
     * 拍卖行每个拍卖类型对应的子世界索引
     */
    private static IntIntMap auctionWorldIndexMap = new IntIntMap();
    /**
     * 活动子世界索引
     */
    private static int activityWorldIndex = 0;

    public static void addUnionWorldIndex(int unionWorldIndex) {
        unionWorldList.add(unionWorldIndex);
    }

    public static IntList getUnionWorldList() {
        return unionWorldList;
    }

    public static void addTeamWorldIndex(int teamWorldIndex) {
        teamWorldList.add(teamWorldIndex);
    }

    public static IntList getTeamWorldList() {
        return teamWorldList;
    }

    public static void addAuctionWorldIndex(int auctionType, int worldIndex) {
        auctionWorldIndexMap.put(auctionType, worldIndex);
    }

    public static IntIntMap getAuctionWorldIndexMap() {
        return auctionWorldIndexMap;
    }

    public static int getAuctionWorldIndex(int auctionType) {
        return auctionWorldIndexMap.get(auctionType);
    }

    public static int getActivityWorldIndex() {
        return activityWorldIndex;
    }

    public static void setActivityWorldIndex(int activityWorldIndex) {
        CenterConst.activityWorldIndex = activityWorldIndex;
    }
}
