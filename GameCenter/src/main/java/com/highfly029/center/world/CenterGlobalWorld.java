package com.highfly029.center.world;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.center.constant.CenterConst;
import com.highfly029.center.module.centerUnion.CenterUnionGlobalManager;
import com.highfly029.center.module.centerUnion.CenterUnionLite;
import com.highfly029.center.tool.CenterConfigTool;
import com.highfly029.center.tool.CenterLoggerTool;
import com.highfly029.common.data.feature.CenterFeature;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.interfaces.IServerGlobalProtocolHandler;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.protocol.BasePtCode;
import com.highfly029.core.session.Session;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.LongList;
import com.highfly029.utils.collection.ObjObjMap;

import io.netty.channel.Channel;
import io.vertx.mysqlclient.MySQLPool;

/**
 * @ClassName CenterGlobalWorld
 * @Description CenterGlobalWorld
 * @Author liyunpeng
 **/
public class CenterGlobalWorld extends GlobalWorld {
    private CenterUnionGlobalManager centerUnionGlobalManager;

    /**
     * 中心服数据库连接池
     */
    private MySQLPool centerDbConnectionPool;

    public CenterGlobalWorld(int childWorldNum) {
        super(true, childWorldNum, 200);
    }

    public CenterUnionGlobalManager getCenterUnionGlobalManager() {
        return centerUnionGlobalManager;
    }

    @Override
    public void onStartUpSuccess() {
        super.onStartUpSuccess();
        String dbName = "centerDb";
        //设置数据库
        centerDbConnectionPool = getMysqlTool().getConnection(dbName);

        postEvent2AllChild(Global2ChildEvent.INIT_DB_CONNECTION, dbName, centerDbConnectionPool);

        centerUnionGlobalManager.warmUp();
    }

    public MySQLPool getCenterDbConnectionPool() {
        return centerDbConnectionPool;
    }

    @Override
    protected void loadTemplateConfig(boolean isHotFix) {
        super.loadTemplateConfig(isHotFix);
        TemplateTool.load("csv", ConfigPropUtils.getBoolValue("useBinDataFile"));
        CenterLoggerTool.centerLogger.info("加载excel配置 size={}", TemplateTool.dataTemplates.size());
    }

    @Override
    protected void clearTemplateConfig() {
        super.clearTemplateConfig();
        //清理excel数据
        TemplateTool.clear();
        CenterLoggerTool.centerLogger.info("清理excel配置");
    }

    @Override
    protected void onHandleConfigData(ObjObjMap<Class, BaseGlobalConfig> configMap) {
        CenterConfigTool.setConfigData(configMap);
    }

    @Override
    protected void onHandlerConfigOver() {
        CenterConfigTool.updateHotfixFlag();
    }

    @Override
    protected void registerAllPacketParser() throws Exception {
        registerPacketParser("com.highfly029.parser.pb");
        registerPacketParser("com.highfly029.parser.custom");
    }

    @Override
    public void onTcpServerEvent(LogicEvent event) {
        byte action = event.getLogicEventAction();
        if (action == LogicEventAction.CHANNEL_READ) {
            Channel channel = (Channel) event.getData2();
            PbPacket.TcpPacket packet = (PbPacket.TcpPacket) event.getData1();
            tcpServerRead(channel, packet);
        } else {
            switch (action) {
                case LogicEventAction.CHANNEL_INACTIVE -> {
                    Channel channel = (Channel) event.getData1();
                    Session session = sessionTool.getSession(channel);
                    if (session != null) {
                        CenterLoggerTool.centerLogger.warn("inactive disconnect type={}, id={}", session.getType(), session.getIdentifier());
                        sessionTool.removeSession(session);
                    } else {
                        CenterLoggerTool.centerLogger.warn("inactive disconnect session is null channel={}", channel.id().asShortText());
                    }
                }
                default -> super.onTcpServerEvent(event);
            }
        }
    }

    /**
     * tcpServer读
     *
     * @param channel
     * @param packet
     */
    private void tcpServerRead(Channel channel, PbPacket.TcpPacket packet) {
        int ptCode = packet.getPtCode();
        int type = 0;
        int identifier = 0;
        try {
            if (ptCode == BasePtCode.HEART_REQ) {
                channel.writeAndFlush(serverHeartResp);
            } else if (ptCode != BasePtCode.CONNECT_TYPE_REQ) {
                Session session = sessionTool.getSession(channel);
                type = session.getType();
                identifier = session.getIdentifier();
                IServerGlobalProtocolHandler handler = serverGlobalHandlerMaps.get(ptCode);
                handler.onServerProtocolHandler(session, ptCode, packet);
            } else {
                PbCommon.ConnectTypeReq req = PbCommon.ConnectTypeReq.parseFrom(packet.getData());
                type = req.getType();
                identifier = req.getIdentifier();
                boolean isReconnect = req.getIsReConnect();

                PbCommon.ConnectTypeResp.Builder resp = PbCommon.ConnectTypeResp.newBuilder();
                resp.setType(SessionType.Center);
                resp.setIdentifier(ConfigConst.identifier);
                resp.setIsReConnect(isReconnect);

                if (CenterConst.getTeamWorldList().size() > 0) {
                    resp.addFeatures(CenterFeature.TEAM);
                }
                if (CenterConst.getUnionWorldList().size() > 0) {
                    resp.addFeatures(CenterFeature.UNION);
                }

                if (CenterConst.getAuctionWorldIndexMap().size() > 0) {
                    resp.addFeatures(CenterFeature.AUCTION);
                }

                if (CenterConst.getActivityWorldIndex() > 0) {
                    resp.addFeatures(CenterFeature.ACTIVITY);
                }

                Session session = sessionTool.getSession(type, identifier);
                if (session != null) {
                    //重复的连接 直接断开
                    CenterLoggerTool.centerLogger.error("connect duplicate type={} id={}", type, identifier);
                    channel.close();
                    return;
                }
                session = sessionTool.create();
                session.init(type, identifier, channel);
                sessionTool.addSession(session);
                PbPacket.TcpPacket tcpPacket = createTcpPacket(BasePtCode.CONNECT_TYPE_RESP, resp.build().toByteString());
                channel.writeAndFlush(tcpPacket);
                CenterLoggerTool.centerLogger.info("received connect type={}, id={}", type, identifier);
            }
        } catch (Exception e) {
            CenterLoggerTool.centerLogger.error("errorMsg: ptCode={}, type={}, id={}, msg={}", ptCode, type, identifier, e.getMessage());
            CenterLoggerTool.centerLogger.error("tcpServerRead", e);
        }
    }

    @Override
    protected void loadAllUpdate() {
        centerUnionGlobalManager = new CenterUnionGlobalManager(this);
        updateMaps.put(CenterUnionGlobalManager.class, centerUnionGlobalManager);
    }

    @Override
    protected void onWorldInnerEvent(LogicEvent logicEvent) {
        byte action = logicEvent.getLogicEventAction();
        switch (action) {
            case CenterWorldInnerEventType.Child2Global -> {
                Child2GlobalEvent event = (Child2GlobalEvent) logicEvent.getData1();
                onChild2GlobalEvent(event, logicEvent.getData2(), logicEvent.getData3());
            }
        }
    }

    protected void onChild2GlobalEvent(Child2GlobalEvent event, Object data1, Object data2) {
        switch (event) {
            case LOAD_UNION -> {
                CenterUnionLite centerUnionLite = (CenterUnionLite) data1;
                boolean isAdd = (boolean) data2;
                centerUnionGlobalManager.loadUnion(centerUnionLite, isAdd);
//                CenterLoggerTool.centerLogger.warn("loadUnion:{}", centerUnionLite);
            }
            case UNLOAD_UNION -> {
                long unionID = (long) data1;
                boolean isDisband = (boolean) data2;
                centerUnionGlobalManager.unloadUnion(unionID, isDisband);
//                CenterLoggerTool.centerLogger.warn("unloadUnion:{}", unionID);
            }
            case UPDATE_UNION_DATA -> {
                IntObjectMap<LongList> updateMap = (IntObjectMap<LongList>) data1;
                PbUnion.PbUpdateUnionData updateUnionData = (PbUnion.PbUpdateUnionData) data2;
                updateUnionData2LogicServer(updateMap, updateUnionData);
            }
            case SEND_MSG_TO_LOGIC_SERVER -> {
                long mixedValue = (long) data1;
                int logicServerID = MathUtils.getCompositeArg1(mixedValue);
                int ptCode = MathUtils.getCompositeArg2(mixedValue);
                GeneratedMessageV3 messageV3 = (GeneratedMessageV3) data2;
                sendMsg2LogicServer(logicServerID, ptCode, messageV3);
            }
            case SEND_MSG_TO_ALL_LOGIC_SERVER -> {
                int ptCode = (int) data1;
                GeneratedMessageV3 messageV3 = (GeneratedMessageV3) data2;
                sendMsg2AllLogicServer(ptCode, messageV3);
            }
        }
    }

    /**
     * 向所有子世界投递事件
     *
     * @param event
     * @param obj1
     * @param obj2
     */
    public void postEvent2AllChild(Global2ChildEvent event, Object obj1, Object obj2) {
        WorldTool.addAllChildWorldLogicEvent(LogicEventType.WORLD_INNER_EVENT_TYPE, CenterWorldInnerEventType.Global2Child, event, obj1, obj2);
    }

    /**
     * 向指定子世界投递事件
     *
     * @param worldIndex
     * @param event
     * @param obj1
     * @param obj2
     */
    public void postEvent2Child(int worldIndex, Global2ChildEvent event, Object obj1, Object obj2) {
        WorldTool.addChildWorldLogicEvent(worldIndex, LogicEventType.WORLD_INNER_EVENT_TYPE, CenterWorldInnerEventType.Global2Child, event, obj1, obj2);
    }

    /**
     * 更新联盟数据到对应的LogicServer
     *
     * @param updateMap
     * @param updateUnionData
     */
    private void updateUnionData2LogicServer(IntObjectMap<LongList> updateMap, PbUnion.PbUpdateUnionData updateUnionData) {
        updateMap.foreachImmutable((serverID, list) -> {
            Session session = sessionTool.getSession(SessionType.Logic, serverID);
            if (session != null) {
                PbUnion.Center2LogicUpdateUnion.Builder builder = PbUnion.Center2LogicUpdateUnion.newBuilder();
                builder.setUpdateUnionData(updateUnionData);
                for (int i = 0, len = list.size(); i < len; i++) {
                    builder.addPlayers(list.get(i));
                }
                session.sendMsgS2S(PtCode.Center2LogicUpdateUnion, builder.build());
            } else {
                CenterLoggerTool.centerLogger.error("updateUnionData2Members session is null serverID={}", serverID);
            }
        });
    }

    /**
     * 发送给指定逻辑服务器消息
     *
     * @param gameLogicID
     * @param ptCode
     * @param messageV3
     */
    private void sendMsg2LogicServer(int gameLogicID, int ptCode, GeneratedMessageV3 messageV3) {
        //TODO 合服后gameLogicID需要转换成当前合并后的serverID
        Session session = sessionTool.getSession(SessionType.Logic, gameLogicID);
        if (session == null) {
            CenterLoggerTool.centerLogger.error("sendMsg2LogicServer session is null gameLogicID={}, ptCode={}", gameLogicID, ptCode);
            return;
        }
        session.sendMsgS2S(ptCode, messageV3);
    }

    /**
     * 发送给所有逻辑服消息
     * @param ptCode
     * @param messageV3
     */
    private void sendMsg2AllLogicServer(int ptCode, GeneratedMessageV3 messageV3) {
        IntObjectMap<Session> map = sessionTool.getSessionMap(SessionType.Logic);
        if (map == null) {
            CenterLoggerTool.centerLogger.warn("sendMsg2AllLogicServer map is null ptCode={}", ptCode);
            return;
        }
        map.foreachImmutable((id, session) -> {
            session.sendMsgS2S(ptCode, messageV3);
        });
    }
}
