package com.highfly029.center.world;

import com.highfly029.center.module.centerUnion.CenterUnionManager;
import com.highfly029.center.tool.CenterLoggerTool;
import com.highfly029.utils.collection.LongList;

/**
 * @ClassName CenterChildWorld
 * @Description 处理联盟逻辑
 * @Author liyunpeng
 **/
public class CenterUnionWorld extends CenterChildWorld {
    private CenterUnionManager centerUnionManager;

    public CenterUnionWorld(int index) {
        super(index);
    }

    @Override
    protected void loadAllUpdate() {
        centerUnionManager = new CenterUnionManager(this);
        updateMaps.put(CenterUnionManager.class, centerUnionManager);
    }

    public CenterUnionManager getCenterUnionManager() {
        return centerUnionManager;
    }

    @Override
    protected void onGlobal2ChildEvent(Global2ChildEvent event, Object data1, Object data2) {
        switch (event) {
            case UNION_WARM_UP -> {
                LongList list = (LongList) data1;
                warnUp(list);
            }
            default -> super.onGlobal2ChildEvent(event, data1, data2);
        }
    }

    /**
     * 预热
     *
     * @param list
     */
    private void warnUp(LongList list) {
        CenterLoggerTool.centerLogger.info("warnUp list={}", list);
        for (int i = 0, size = list.size(); i < size; i++) {
            long unionID = list.get(i);
            centerUnionManager.loadUnionFromDB(unionID);
        }
    }
}
