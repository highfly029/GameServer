package com.highfly029.center.world;

/**
 * @ClassName CenterWorldInnerEventType
 * @Description CenterWorldInnerEventType
 * @Author liyunpeng
 **/
public class CenterWorldInnerEventType {

    public static final byte Global2Child = 1;

    public static final byte Child2Global = 2;
}
