package com.highfly029.center.world;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.ChildWorld;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.utils.MathUtils;

import io.vertx.mysqlclient.MySQLPool;

/**
 * @ClassName CenterChildWorld
 * @Description CenterChildWorld
 * @Author liyunpeng
 **/
public abstract class CenterChildWorld extends ChildWorld {
    /**
     * 中心服数据库连接池
     */
    private MySQLPool centerDbConnectionPool;

    public CenterChildWorld(int index) {
        super(index, 100);
    }


    @Override
    protected void onWorldInnerEvent(LogicEvent logicEvent) {
        byte action = logicEvent.getLogicEventAction();
        switch (action) {
            case CenterWorldInnerEventType.Global2Child -> {
                Global2ChildEvent event = (Global2ChildEvent) logicEvent.getData1();
                onGlobal2ChildEvent(event, logicEvent.getData2(), logicEvent.getData3());
            }
            default -> super.onWorldInnerEvent(logicEvent);
        }
    }


    protected void onGlobal2ChildEvent(Global2ChildEvent event, Object data1, Object data2) {
        switch (event) {
            case INIT_DB_CONNECTION -> {
                String dbName = (String) data1;
                MySQLPool connectionPool = (MySQLPool) data2;
                centerDbConnectionPool = connectionPool;
            }
        }
    }

    /**
     * 获取连接
     *
     * @return
     */
    public MySQLPool getCenterDbConnectionPool() {
        return centerDbConnectionPool;
    }

    /**
     * 向指定子世界投递事件
     *
     * @param event
     * @param obj1
     * @param obj2
     */
    public void postEvent2Global(Child2GlobalEvent event, Object obj1, Object obj2) {
        WorldTool.addLogicEvent(LogicEventType.WORLD_INNER_EVENT_TYPE, CenterWorldInnerEventType.Child2Global, event, obj1, obj2);
    }


    /**
     * 发送给指定逻辑服务器消息
     *
     * @param logicServerID
     * @param ptCode
     * @param msg
     */
    public void sendMsg2LogicServer(int logicServerID, int ptCode, GeneratedMessageV3 msg) {
        long mixedValue = MathUtils.getCompositeIndex(logicServerID, ptCode);
        postEvent2Global(Child2GlobalEvent.SEND_MSG_TO_LOGIC_SERVER, mixedValue, msg);
    }

    /**
     * 发送给所有逻辑服消息
     * @param ptCode
     * @param msg
     */
    public void sendMsg2AllLogicServer(int ptCode, GeneratedMessageV3 msg) {
        postEvent2Global(Child2GlobalEvent.SEND_MSG_TO_ALL_LOGIC_SERVER, ptCode, msg);
    }
}
