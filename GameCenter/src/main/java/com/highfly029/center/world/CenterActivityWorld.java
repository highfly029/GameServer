package com.highfly029.center.world;

import com.highfly029.center.module.centerActivity.CenterActivityManager;

/**
 * @ClassName CenterActivityWorld
 * @Description CenterActivityWorld
 * @Author liyunpeng
 **/
public class CenterActivityWorld extends CenterChildWorld {
    private CenterActivityManager centerActivityManager;

    public CenterActivityWorld(int index) {
        super(index);
    }

    @Override
    protected void loadAllUpdate() {
        super.loadAllUpdate();
        centerActivityManager = new CenterActivityManager(this);
        updateMaps.put(CenterActivityManager.class, centerActivityManager);
    }

    public CenterActivityManager getCenterActivityManager() {
        return centerActivityManager;
    }

    @Override
    protected void onStartUpSuccess() {
        super.onStartUpSuccess();
        centerActivityManager.init();
    }
}
