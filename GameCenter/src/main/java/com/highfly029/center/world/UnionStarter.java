package com.highfly029.center.world;

import com.highfly029.center.constant.CenterConst;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.DisruptorBuilder;
import com.highfly029.core.world.SleepingWaitExtendStrategy;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

/**
 * @ClassName UnionStarter
 * @Description 联盟独立进程启动器
 * @Author liyunpeng
 **/
public class UnionStarter {
    public static void main(String[] args) {
        int unionChildWorldNum = 4;
        int index = 0;
        int childNum = unionChildWorldNum;

        CenterGlobalWorld centerGlobalWorld = new CenterGlobalWorld(childNum);
        WaitStrategy waitStrategy = new SleepingWaitExtendStrategy(centerGlobalWorld);
        Disruptor disruptor = DisruptorBuilder.builder()
                .setPoolName("CenterGlobal")
                .setProducerType(ProducerType.MULTI)
                .setWorldThread(centerGlobalWorld)
                .setWaitStrategy(waitStrategy)
                .build();

        WorldTool worldTool = WorldTool.getInstance();
        worldTool.setDisruptor(disruptor);
        worldTool.setGlobalWorld(centerGlobalWorld);


        for (int i = 1; i <= unionChildWorldNum; i++) {
            index++;
            CenterUnionWorld centerUnionWorld = new CenterUnionWorld(index);
            WaitStrategy childWait = new SleepingWaitExtendStrategy(centerUnionWorld);
            Disruptor childDisruptor = DisruptorBuilder.builder()
                    .setPoolName("CenterUnion")
                    .setProducerType(ProducerType.SINGLE)
                    .setWorldThread(centerUnionWorld)
                    .setWaitStrategy(childWait)
                    .setIndex(index)
                    .build();
            worldTool.addChildWorld(index, childDisruptor);
            CenterConst.addUnionWorldIndex(index);
        }

        worldTool.start();
    }
}


