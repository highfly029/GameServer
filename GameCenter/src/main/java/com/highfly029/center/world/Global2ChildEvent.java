package com.highfly029.center.world;

/**
 * @ClassName Global2ChildEvent
 * @Description Global2ChildEvent
 * @Author liyunpeng
 **/
public enum Global2ChildEvent {
    /**
     * 初始化数据库连接
     */
    INIT_DB_CONNECTION,
    /**
     * 联盟预加载
     */
    UNION_WARM_UP,
}
