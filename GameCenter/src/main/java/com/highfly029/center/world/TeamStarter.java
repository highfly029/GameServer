package com.highfly029.center.world;

import com.highfly029.center.constant.CenterConst;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.DisruptorBuilder;
import com.highfly029.core.world.SleepingWaitExtendStrategy;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

/**
 * @ClassName TeamStarter
 * @Description 队伍独立进程启动器
 * @Author liyunpeng
 **/
public class TeamStarter {
    public static void main(String[] args) {
        int teamChildWorldNum = 4;
        int index = 0;
        int childNum = teamChildWorldNum;

        CenterGlobalWorld centerGlobalWorld = new CenterGlobalWorld(childNum);
        WaitStrategy waitStrategy = new SleepingWaitExtendStrategy(centerGlobalWorld);
        Disruptor disruptor = DisruptorBuilder.builder()
                .setPoolName("CenterGlobal")
                .setProducerType(ProducerType.MULTI)
                .setWorldThread(centerGlobalWorld)
                .setWaitStrategy(waitStrategy)
                .build();

        WorldTool worldTool = WorldTool.getInstance();
        worldTool.setDisruptor(disruptor);
        worldTool.setGlobalWorld(centerGlobalWorld);

        for (int i = 1; i <= teamChildWorldNum; i++) {
            index++;
            CenterTeamWorld centerTeamWorld = new CenterTeamWorld(index);
            WaitStrategy childWait = new SleepingWaitExtendStrategy(centerTeamWorld);
            Disruptor childDisruptor = DisruptorBuilder.builder()
                    .setPoolName("CenterTeam")
                    .setProducerType(ProducerType.SINGLE)
                    .setWorldThread(centerTeamWorld)
                    .setWaitStrategy(childWait)
                    .setIndex(index)
                    .build();
            worldTool.addChildWorld(index, childDisruptor);
            CenterConst.addTeamWorldIndex(index);
        }

        worldTool.start();
    }
}
