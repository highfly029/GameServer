package com.highfly029.center.world;

/**
 * @ClassName Child2GlobalEvent
 * @Description Child2GlobalEvent
 * @Author liyunpeng
 **/
public enum Child2GlobalEvent {
    /**
     * 加载联盟(包括创建)
     */
    LOAD_UNION,
    /**
     * 卸载联盟(包括删除)
     */
    UNLOAD_UNION,
    /**
     * 更新联盟数据
     */
    UPDATE_UNION_DATA,
    /**
     * 发送给指定逻辑服务器消息
     */
    SEND_MSG_TO_LOGIC_SERVER,
    /**
     * 发送给所有逻辑服消息
     */
    SEND_MSG_TO_ALL_LOGIC_SERVER,

    ;
}