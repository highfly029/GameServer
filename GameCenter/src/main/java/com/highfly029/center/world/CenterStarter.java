package com.highfly029.center.world;

import com.highfly029.center.constant.CenterConst;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.DisruptorBuilder;
import com.highfly029.core.world.SleepingWaitExtendStrategy;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

/**
 * @ClassName CenterStarter
 * @Description CenterStarter
 * @Author liyunpeng
 **/
public class CenterStarter {
    public static void main(String[] args) {
        int auctionChildWorldNum = 2;
        int unionChildWorldNum = 3;
        int teamChildWorldNum = 4;
        int index = 0;
        int childNum = auctionChildWorldNum + unionChildWorldNum + teamChildWorldNum;

        CenterGlobalWorld centerGlobalWorld = new CenterGlobalWorld(childNum);
        WaitStrategy waitStrategy = new SleepingWaitExtendStrategy(centerGlobalWorld);
        Disruptor disruptor = DisruptorBuilder.builder()
                .setPoolName("CenterGlobal")
                .setProducerType(ProducerType.MULTI)
                .setWorldThread(centerGlobalWorld)
                .setWaitStrategy(waitStrategy)
                .build();

        WorldTool worldTool = WorldTool.getInstance();
        worldTool.setDisruptor(disruptor);
        worldTool.setGlobalWorld(centerGlobalWorld);


        for (int i = 1; i <= unionChildWorldNum; i++) {
            index++;
            CenterUnionWorld centerUnionWorld = new CenterUnionWorld(index);
            WaitStrategy childWait = new SleepingWaitExtendStrategy(centerUnionWorld);
            Disruptor childDisruptor = DisruptorBuilder.builder()
                    .setPoolName("CenterUnion")
                    .setProducerType(ProducerType.SINGLE)
                    .setWorldThread(centerUnionWorld)
                    .setWaitStrategy(childWait)
                    .setIndex(index)
                    .build();
            worldTool.addChildWorld(index, childDisruptor);
            CenterConst.addUnionWorldIndex(index);
        }

        for (int i = 1; i <= teamChildWorldNum; i++) {
            index++;
            CenterTeamWorld centerTeamWorld = new CenterTeamWorld(index);
            WaitStrategy childWait = new SleepingWaitExtendStrategy(centerTeamWorld);
            Disruptor childDisruptor = DisruptorBuilder.builder()
                    .setPoolName("CenterTeam")
                    .setProducerType(ProducerType.SINGLE)
                    .setWorldThread(centerTeamWorld)
                    .setWaitStrategy(childWait)
                    .setIndex(index)
                    .build();
            worldTool.addChildWorld(index, childDisruptor);
            CenterConst.addTeamWorldIndex(index);
        }

        for (int i = 1; i <= auctionChildWorldNum; i++) {
            index++;
            CenterAuctionWorld centerAuctionWorld = new CenterAuctionWorld(index);
            WaitStrategy childWait = new SleepingWaitExtendStrategy(centerAuctionWorld);
            Disruptor childDisruptor = DisruptorBuilder.builder()
                    .setPoolName("CenterAuction")
                    .setProducerType(ProducerType.SINGLE)
                    .setWorldThread(centerAuctionWorld)
                    .setWaitStrategy(childWait)
                    .setIndex(index)
                    .build();
            worldTool.addChildWorld(index, childDisruptor);
            CenterConst.addAuctionWorldIndex(i - 1, index);
        }

        index++;
        CenterActivityWorld centerActivityWorld = new CenterActivityWorld(index);
        WaitStrategy childWait = new SleepingWaitExtendStrategy(centerActivityWorld);
        Disruptor childDisruptor = DisruptorBuilder.builder()
                .setPoolName("CenterActivity")
                .setProducerType(ProducerType.SINGLE)
                .setWorldThread(centerActivityWorld)
                .setWaitStrategy(childWait)
                .setIndex(index)
                .build();
        worldTool.addChildWorld(index, childDisruptor);
        CenterConst.setActivityWorldIndex(index);

        worldTool.start();
    }
}
