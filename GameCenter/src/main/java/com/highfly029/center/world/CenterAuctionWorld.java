package com.highfly029.center.world;

import java.util.ArrayList;
import java.util.List;

import com.highfly029.center.constant.CenterConst;
import com.highfly029.center.module.centerAuction.CenterAuctionManager;

/**
 * @ClassName CenterAuctionWorld
 * @Description 拍卖行子世界线程
 * @Author liyunpeng
 **/
public class CenterAuctionWorld extends CenterChildWorld {
    private CenterAuctionManager centerAuctionManager;

    public CenterAuctionWorld(int index) {
        super(index);
    }

    @Override
    protected void loadAllUpdate() {
        super.loadAllUpdate();
        centerAuctionManager = new CenterAuctionManager(this);
        updateMaps.put(CenterAuctionManager.class, centerAuctionManager);
    }

    public CenterAuctionManager getCenterAuctionManager() {
        return centerAuctionManager;
    }

    @Override
    protected void onStartUpSuccess() {
        super.onStartUpSuccess();
        List<Integer> list = new ArrayList<>();
        CenterConst.getAuctionWorldIndexMap().foreachImmutable((auctionType, worldIndex) -> {
            if (worldIndex == getIndex()) {
                list.add(auctionType);
            }
        });
        centerAuctionManager.loadAllAuctionItemFromDb(list);
    }
}
