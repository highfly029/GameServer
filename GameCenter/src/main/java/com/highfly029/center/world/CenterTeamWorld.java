package com.highfly029.center.world;

import com.highfly029.center.module.centerTeam.CenterTeamManager;

/**
 * @ClassName CenterTeamUnionWorld
 * @Description 处理队伍逻辑
 * @Author liyunpeng
 **/
public class CenterTeamWorld extends CenterChildWorld {
    private CenterTeamManager centerTeamManager;

    public CenterTeamWorld(int index) {
        super(index);
    }

    @Override
    protected void loadAllUpdate() {
        centerTeamManager = new CenterTeamManager(this);
        updateMaps.put(CenterTeamManager.class, centerTeamManager);
    }
}
