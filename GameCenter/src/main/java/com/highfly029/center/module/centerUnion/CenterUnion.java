package com.highfly029.center.module.centerUnion;

import java.util.function.Consumer;
import java.util.function.Predicate;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.center.tool.CenterConfigTool;
import com.highfly029.center.tool.CenterLoggerTool;
import com.highfly029.center.world.CenterUnionWorld;
import com.highfly029.center.world.Child2GlobalEvent;
import com.highfly029.common.data.base.Location;
import com.highfly029.common.data.union.Union;
import com.highfly029.common.data.union.UnionActionLog;
import com.highfly029.common.data.union.UnionMember;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.common.template.union.UnionActionLogTypeConst;
import com.highfly029.common.template.union.UnionHandleJoinResultConst;
import com.highfly029.common.template.union.UnionHandleJoinTypeConst;
import com.highfly029.common.template.union.UnionLevelTemplate;
import com.highfly029.common.template.union.UnionPermissionConst;
import com.highfly029.common.template.union.UnionPositionConst;
import com.highfly029.common.template.union.UnionPositionTemplate;
import com.highfly029.common.template.union.UnionTemplate;
import com.highfly029.common.template.union.UnionUpdateTypeConst;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.utils.ArrayUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.LongList;

import io.vertx.core.buffer.Buffer;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName CenterUnion
 * @Description 中心服联盟数据结构
 * @Author liyunpeng
 **/
public class CenterUnion extends Union {

    private CenterUnionWorld centerUnionWorld;
    /**
     * 数据过期时间 默认0永不过期、当联盟里所有玩家都不在线时、超过一定时间后设置expireTime、需要存库并从内存中删除
     */
    private long expireTime;

    public long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(long expireTime) {
        this.expireTime = expireTime;
    }

    public CenterUnionWorld getCenterUnionWorld() {
        return centerUnionWorld;
    }

    public void setCenterUnionWorld(CenterUnionWorld centerUnionWorld) {
        this.centerUnionWorld = centerUnionWorld;
    }

    /**
     * 初始化新的联盟数据
     */
    public void initNewUnion(long unionID, String unionName, long createPlayerID, Location location) {
        setUnionID(unionID);
        setUnionName(unionName);

        setLevel(1);

        CenterUnionConfig centerUnionConfig = CenterConfigTool.getCenterUnionConfig();
        //初始化默认权限
        for (int position = 1; position <= UnionPositionConst.count; position++) {
            UnionPositionTemplate unionPositionTemplate = centerUnionConfig.getUnionPositionTemplate(position);
            if (unionPositionTemplate.getDefaultPermission() != null) {
                for (int permission : unionPositionTemplate.getDefaultPermission()) {
                    addPermission(position, (byte) permission);
                }
            }
        }

        UnionMember unionMember = UnionMember.create(this);
        unionMember.setPosition(UnionPositionConst.Leader);
        unionMember.setMemberID(createPlayerID);
        unionMember.setLocation(location);
        location.setOnline();

        addUnionMember(createPlayerID, unionMember);
    }

    /**
     * 加载到内存时通知
     */
    public void onLoad() {

    }

    /**
     * 主动退出
     *
     * @param memberID
     * @return
     */
    public boolean quitUnion(long memberID) {
        UnionMember unionMember = getUnionMember(memberID);
        if (unionMember == null) {
            CenterLoggerTool.centerLogger.warn("quit unionID={} unionMember={} is null", getUnionID(), memberID);
            return false;
        }
        if (unionMember.getPosition() == UnionPositionConst.Leader) {
            CenterLoggerTool.centerLogger.warn("quit unionID={} unionMember={} is leader", getUnionID(), memberID);
            return false;
        }
        boolean ret = removeUnionMember(unionMember, true);
        if (ret) {
            addActionLog(UnionActionLogTypeConst.QuitUnion);
        }
        return ret;
    }

    /**
     * 提出成员
     */
    public boolean kickOut(long memberID, long targetPlayerID) {
        UnionMember unionMember = getUnionMember(memberID);
        if (unionMember == null) {
            CenterLoggerTool.centerLogger.error("kickOutMember not exist member playerID={}, unionID={}", memberID, getUnionID());
            return false;
        }
        UnionMember targetMember = getUnionMember(targetPlayerID);
        if (targetMember == null) {
            CenterLoggerTool.centerLogger.error("kickOutMember not exist target member targetPlayerID={}, unionID={}", targetPlayerID, getUnionID());
            return false;
        }
        if (!checkPermission(unionMember.getPosition(), UnionPermissionConst.KickOutMember)) {
            CenterLoggerTool.centerLogger.error("kickOutMember no permission playerID={}, unionID={}", memberID, getUnionID());
            return false;
        }
        boolean ret = removeUnionMember(targetMember, false);
        if (ret) {
            addActionLog(UnionActionLogTypeConst.KickOutMember);
        }
        return ret;
    }

    /**
     * 删除成员
     *
     * @param unionMember
     * @param initiative  是否是主动退出
     * @return
     */
    private boolean removeUnionMember(UnionMember unionMember, boolean initiative) {
        //成员数量只有一个人的时候不能删除成员 只能解散
        if (getUnionMemberSize() == 1) {
            CenterLoggerTool.centerLogger.error("removeUnionMember invalid unionID={}, memberID={}", getUnionID(), unionMember.getMemberID());
            return false;
        }
        //不能删除盟主
        if (unionMember.getPosition() == UnionPositionConst.Leader) {
            CenterLoggerTool.centerLogger.error("removeUnionMember invalid unionID={}, memberID={}", getUnionID(), unionMember.getMemberID());
            return false;
        }


        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.RemoveMember);
        builder.setLongValue1(unionMember.getMemberID());
        builder.setBoolValue1(initiative);
        updateUnionData2Members((member) -> true, builder.build());
        removeUnionMember(unionMember.getMemberID());

        onRemoveUnionMember(unionMember);
        UnionMember.back(unionMember);
        return true;
    }

    /**
     * 增加权限
     *
     * @param position   职位
     * @param permission 权限
     */
    public void addPermission(int position, byte permission) {
        long bit = 1L << (permission - 1);
        long tmpPermission = getPermission(position);
        if ((tmpPermission & bit) > 0) {
            return;
        }
        tmpPermission |= bit;
        setPermission(position, tmpPermission);
    }

    /**
     * 删除权限
     *
     * @param position   职位
     * @param permission 权限
     */
    public void removePermission(int position, byte permission) {
        long bit = 1L << (permission - 1);
        long tmpPermission = getPermission(position);
        if ((tmpPermission & bit) == 0) {
            return;
        }
        tmpPermission = tmpPermission & ~bit;
        setPermission(position, tmpPermission);
    }

    /**
     * 是否拥有权限
     *
     * @param position   职位
     * @param permission 权限
     * @return
     */
    public boolean checkPermission(int position, byte permission) {
        long bit = 1L << (permission - 1);
        long tmpPermission = getPermission(position);
        return (tmpPermission & bit) > 0;
    }

    /**
     * 增加资产
     *
     * @param type
     * @param value
     * @param behaviorSource
     */
    public void addAsset(int type, long value, int behaviorSource) {
        if (value <= 0) {
            return;
        }
        long now = addAsset(type, value);
        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.UpdateAsset);
        builder.setIntValue1(type);
        builder.setLongValue1(now);
        builder.setLongValue2(behaviorSource);
        builder.setBoolValue1(true);
        updateUnionData2Members((member) -> true, builder.build());

        onAssetChange(type, value, true);
    }

    /**
     * 删除资产
     *
     * @param type
     * @param value
     * @param behaviorSource
     */
    public void removeAsset(int type, long value, int behaviorSource) {
        if (value <= 0) {
            return;
        }
        long now = removeAsset(type, value);
        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.UpdateAsset);
        builder.setIntValue1(type);
        builder.setLongValue1(now);
        builder.setLongValue2(behaviorSource);
        builder.setBoolValue1(false);
        updateUnionData2Members((member) -> true, builder.build());

        onAssetChange(type, value, false);
    }


    /**
     * 邀请加入联盟
     *
     * @param playerID
     * @param logicServerID
     * @param targetPlayerID
     */
    public boolean inviteJoinUnion(long playerID, int logicServerID, long targetPlayerID) {
        UnionMember unionMember = getUnionMember(playerID);
        if (unionMember == null) {
            CenterLoggerTool.centerLogger.error("inviteJoinUnion not exist playerID={}, unionID={}, targetID={}", playerID, getUnionID(), targetPlayerID);
            return false;
        }

        if (!checkPermission(unionMember.getPosition(), UnionPermissionConst.InviteMember)) {
            CenterLoggerTool.centerLogger.error("inviteJoinUnion playerID={} dont have permission invite targetID={} in unionID={}", playerID, targetPlayerID, getUnionID());
            return false;
        }

        if (getUnionMember(targetPlayerID) != null) {
            CenterLoggerTool.centerLogger.error("inviteJoinUnion exist targetID={}, playerID={}, unoinID={}", targetPlayerID, playerID, getUnionID());
            return false;
        }

        if (isInInviteJoinList(targetPlayerID)) {
            CenterLoggerTool.centerLogger.error("unionID={} inviteJoinUnion inviteList already have the targetPlayerID={}, playerID={}", getUnionID(), targetPlayerID, playerID);
            return false;
        }
        UnionTemplate unionTemplate = CenterConfigTool.getCenterUnionConfig().getUnionTemplate();
        if (getInviteJoinSize() >= unionTemplate.getInviteListMaxNum()) {
            CenterLoggerTool.centerLogger.error("inviteJoinUnion inviteList unionID={}  maxNum targetPlayerID={}, playerID={}", getUnionID(), targetPlayerID, playerID);
            return false;
        }

        addInviteJoinList(targetPlayerID, logicServerID);
        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.AddInviteJoinList);
        builder.setLongValue1(targetPlayerID);
        builder.setLongValue2(expireTime);
        updateUnionData2Members((member) -> checkPermission(member.getPosition(), UnionPermissionConst.HandleInvite), builder.build());

        PbUnion.Center2LogicInviteJoinUnion.Builder c2sBuilder = PbUnion.Center2LogicInviteJoinUnion.newBuilder();
        c2sBuilder.setOperatorPlayerID(playerID);
        c2sBuilder.setTargetPlayerID(targetPlayerID);
        c2sBuilder.setUnionID(getUnionID());
        c2sBuilder.setName(getName());
        centerUnionWorld.sendMsg2LogicServer(logicServerID, PtCode.Center2LogicInviteJoinUnion, c2sBuilder.build());
        return true;
    }

    /**
     * 申请加入联盟
     *
     * @param playerID
     * @param level
     */
    public boolean applyJoinUnion(int logicServerID, long playerID, int level) {
        UnionTemplate unionTemplate = CenterConfigTool.getCenterUnionConfig().getUnionTemplate();
        if (getApplyJoinListSize() >= unionTemplate.getApplyListMaxNum()) {
            CenterLoggerTool.centerLogger.error("applyJoinUnion applyJoinMap unionID={} maxNum playerID={}", getUnionID(), playerID);
            return false;
        }

        if (getSetting().getJoinLimitLv() > level) {
            CenterLoggerTool.centerLogger.error("applyJoinUnion limit level unionID={},playerID={}", getUnionID(), playerID);
            return false;
        }

        if (!getSetting().isJoinNeedApply()) {
            if (!checkCanJoinUnion(playerID)) {
                CenterLoggerTool.centerLogger.error("applyJoinUnion check fail unionID={},playerID={}", getUnionID(), playerID);
                return false;
            }
            //直接尝试加入
            joinUnionDoubleCheck2(logicServerID, playerID, playerID, 0);
        } else {
            if (isInApplyJoinList(playerID)) {
                CenterLoggerTool.centerLogger.error("applyJoinUnion applyJoinMap unionID={} already have the playerID={}", getUnionID(), playerID);
                return false;
            }

            addApplyJoinList(playerID, logicServerID);
            PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
            builder.setUpdateType(UnionUpdateTypeConst.AddApplyJoinList);
            builder.setLongValue1(playerID);
            builder.setIntValue1(logicServerID);
            updateUnionData2Members((member) -> checkPermission(member.getPosition(), UnionPermissionConst.HandleApply), builder.build());
        }
        return true;
    }

    /**
     * 处理申请列表
     *
     * @param playerID
     * @param targetPlayerID
     * @param handleResult
     */
    public boolean handleApplyJoinList(long playerID, long targetPlayerID, int handleResult, int handleType) {
        UnionMember unionMember = getUnionMember(playerID);
        if (unionMember == null) {
            CenterLoggerTool.centerLogger.error("handleApplyJoinList not exist playerID={}, unionID={}", playerID, getUnionID());
            return false;
        }

        if (!checkPermission(unionMember.getPosition(), UnionPermissionConst.HandleApply)) {
            CenterLoggerTool.centerLogger.error("handleApplyJoinList no permission result={}, playerID={}, unionID={}", handleResult, playerID, getUnionID());
            return false;
        } else if (!isInApplyJoinList(targetPlayerID)) {
            CenterLoggerTool.centerLogger.error("handleApplyJoinList targetPlayerID={} not in applyList, playerID={}, unionID={}", targetPlayerID, playerID, getUnionID());
            return false;
        }

        switch (handleResult) {
            case UnionHandleJoinResultConst.Agree -> {
                joinUnionDoubleCheck2(getApplyJoinMap().get(targetPlayerID), playerID, targetPlayerID, handleType);
            }
            case UnionHandleJoinResultConst.Refuse -> {
                removeApplyJoinList(playerID, targetPlayerID);
                //TODO 是否需要通知玩家
            }
            case UnionHandleJoinResultConst.Ignore -> {
                removeApplyJoinList(playerID, targetPlayerID);
            }
        }
        return true;
    }

    /**
     * 删除申请列表里的目标玩家
     *
     * @param operationID
     * @param targetPlayerID
     */
    private void removeApplyJoinList(long operationID, long targetPlayerID) {
        removeApplyJoinList(targetPlayerID);
        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.RemoveApplyJoinList);
        builder.setLongValue1(operationID);
        builder.setLongValue2(targetPlayerID);
        updateUnionData2Members((member) -> checkPermission(member.getPosition(), UnionPermissionConst.HandleApply), builder.build());
    }

    /**
     * 加入联盟double check
     * 发送消息去对应的GameLogic check玩家是否确实没有加入联盟
     * 然后再在玩家身上记录prepareJoinUnionIDMark和expireTime防止并发操作、返回到此服务器后再真正的添加成员
     *
     * @param logicServerID
     * @param operatorPlayerID
     * @param targetPlayerID
     * @param handleType
     */
    private void joinUnionDoubleCheck2(int logicServerID, long operatorPlayerID, long targetPlayerID, int handleType) {
        PbUnion.Center2LogicJoinUnionDoubleCheck.Builder builder = PbUnion.Center2LogicJoinUnionDoubleCheck.newBuilder();
        builder.setOperatorPlayerID(operatorPlayerID);
        builder.setTargetPlayerID(targetPlayerID);
        builder.setUnionID(getUnionID());
        builder.setType(handleType);
        centerUnionWorld.sendMsg2LogicServer(logicServerID, PtCode.Center2LogicJoinUnionDoubleCheck, builder.build());
    }

    /**
     * 检测玩家能否加入联盟
     *
     * @param playerID
     * @return
     */
    private boolean checkCanJoinUnion(long playerID) {
        if (getUnionMember(playerID) != null) {
            CenterLoggerTool.centerLogger.error("checkCanJoinUnion already exist unionID={}, playerID={}", getUnionID(), playerID);
            return false;
        }

        if (isMaxMemberNum()) {
            CenterLoggerTool.centerLogger.error("checkCanJoinUnion already maxNum unionID={}, playerID={}", getUnionID(), playerID);
            return false;
        }
        return true;
    }

    /**
     * 检测目标玩家能成功加入联盟
     *
     * @param operatorID
     * @param targetPlayerID
     * @param handleType
     * @param location
     */
    public void onJoinUnionDoubleCheckSuccess(long operatorID, long targetPlayerID, int handleType, Location location) {
        if (!checkCanJoinUnion(targetPlayerID)) {
            CenterLoggerTool.centerLogger.error("onJoinUnionDoubleCheckSuccess check fail unionID={}, operatorID={}, targetPlayerID={}", getUnionID(), operatorID, targetPlayerID);
            return;
        }

        if (handleType == UnionHandleJoinTypeConst.Apply) {
            UnionMember unionMember = getUnionMember(operatorID);
            if (unionMember == null) {
                CenterLoggerTool.centerLogger.error("onJoinUnionDoubleCheckSuccess not exist operatorID={} unionID={}, targetPlayerID={}", operatorID, getUnionID(), targetPlayerID);
                return;
            }
            removeApplyJoinList(operatorID, targetPlayerID);
        }

        addNewMember(targetPlayerID, location);
    }

    /**
     * 玩家响应邀请加入联盟
     *
     * @param playerID
     * @param handleResultType
     */
    public boolean handleBeInviteJoinUnion(long playerID, int handleResultType, Location location) {
        if (!isInInviteJoinList(playerID)) {
            CenterLoggerTool.centerLogger.error("handleBeInviteJoinUnion not exist in inviteJoinList targetPlayerID={} unionID={}", playerID, getUnionID());
            return false;
        }
        removeInviteJoinList(playerID);
        if (getUnionMember(playerID) != null) {
            CenterLoggerTool.centerLogger.error("handleBeInviteJoinUnion already exist targetPlayerID={} unionID={}", playerID, getUnionID());
            return false;
        }

        if (isMaxMemberNum()) {
            CenterLoggerTool.centerLogger.error("handleBeInviteJoinUnion already maxNum unionID={}, targetPlayerID={}", getUnionID(), playerID);
            return false;
        }
        if (handleResultType == UnionHandleJoinResultConst.Agree) {
            PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
            builder.setUpdateType(UnionUpdateTypeConst.RemoveInviteJoinList);
            builder.setLongValue1(playerID);
            updateUnionData2Members((member) -> checkPermission(member.getPosition(), UnionPermissionConst.HandleInvite), builder.build());

            addNewMember(playerID, location);
            return true;
        }
        return false;
    }

    /**
     * 添加新玩家
     *
     * @param targetPlayerID
     * @param location
     */
    private void addNewMember(long targetPlayerID, Location location) {
        //发送给被添加的玩家联盟信息
        PbUnion.Center2LogicSyncPlayerUnionInfo.Builder unionInfoBuilder = PbUnion.Center2LogicSyncPlayerUnionInfo.newBuilder();
        unionInfoBuilder.setPlayerID(targetPlayerID);
        unionInfoBuilder.setUnionID(getUnionID());
        unionInfoBuilder.setName(getUnionName());
        unionInfoBuilder.setUnionData(save());
        centerUnionWorld.sendMsg2LogicServer(location.getLogicServerID(), PtCode.Center2LogicSyncPlayerUnionInfo, unionInfoBuilder.build());

        UnionMember targetMember = UnionMember.create(this);
        targetMember.setPosition(UnionPositionConst.Member);
        targetMember.setMemberID(targetPlayerID);
        targetMember.setLocation(location);

        addUnionMember(targetMember.getMemberID(), targetMember);
        onAddUnionMember(targetMember);

        //广播所有玩家
        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.AddMember);
        builder.setMember(targetMember.save());
        updateUnionData2Members((member) -> true, builder.build());
    }

    /**
     * 更换盟主
     *
     * @param playerID
     * @param targetPlayerID
     */
    public boolean changeLeader(long playerID, long targetPlayerID) {
        UnionMember unionMember = getUnionMember(playerID);
        if (unionMember == null) {
            CenterLoggerTool.centerLogger.error("changeLeader not exist member playerID={}, unionID={}", playerID, getUnionID());
            return false;
        }
        if (unionMember.getPosition() != UnionPositionConst.Leader) {
            CenterLoggerTool.centerLogger.error("changeLeader not leader playerID={}, unionID={}", playerID, getUnionID());
            return false;
        }
        UnionMember targetMember = getUnionMember(targetPlayerID);
        if (targetMember == null) {
            CenterLoggerTool.centerLogger.error("changeLeader not exist target member playerID={}, unionID={}, targetPlayerID={}", playerID, getUnionID(), targetPlayerID);
            return false;
        }
        unionMember.setPosition(UnionPositionConst.Member);
        targetMember.setPosition(UnionPositionConst.Leader);
        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.ChangeLeader);
        builder.setLongValue1(unionMember.getMemberID());
        builder.setLongValue2(targetMember.getMemberID());
        updateUnionData2Members((member) -> true, builder.build());
        return true;
    }

    /**
     * 解散联盟
     */
    void disband(UnionMember unionMember) {
        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.Disband);
        builder.setLongValue1(unionMember.getMemberID());
        updateUnionData2Members((member) -> true, builder.build());

        //最后删除回收
        centerUnionWorld.getCenterUnionManager().removeUnion(getUnionID(), true);
    }

    /**
     * 更新联盟数据到所有符合条件的在线玩家
     *
     * @param predicate
     * @param updateUnionData
     */
    private void updateUnionData2Members(Predicate<UnionMember> predicate, PbUnion.PbUpdateUnionData updateUnionData) {
        IntObjectMap<LongList> updateMap = new IntObjectMap<>(LongList[]::new);
        getUnionMemberMap().foreachImmutable((k, v) -> {
            if (v.getLocation().isOnline() && predicate.test(v)) {
                LongList list = updateMap.get(v.getLocation().getLogicServerID());
                if (list == null) {
                    list = new LongList();
                    updateMap.put(v.getLocation().getLogicServerID(), list);
                }
                list.add(v.getMemberID());
            }
        });

        centerUnionWorld.postEvent2Global(Child2GlobalEvent.UPDATE_UNION_DATA, updateMap, updateUnionData);
    }

    /**
     * 是否达到了最大成员数量
     *
     * @return
     */
    public boolean isMaxMemberNum() {
        UnionLevelTemplate unionLevelTemplate = CenterConfigTool.getCenterUnionConfig().getUnionLevelTemplate(getLevel());
        return getUnionMemberSize() >= unionLevelTemplate.getMaxNum();
    }

    /**
     * 修改联盟名字
     *
     * @param name
     * @param consumer 结果回调
     */
    public void modifyUnionName(long playerID, String name, int index, Consumer<Boolean> consumer) {
        UnionMember unionMember = getUnionMember(playerID);
        if (unionMember == null) {
            CenterLoggerTool.centerLogger.error("modifyUnionName not exist member playerID={}, unionID={}", playerID, getUnionID());
            return;
        }
        if (!checkPermission(unionMember.getPosition(), UnionPermissionConst.ModifyName)) {
            CenterLoggerTool.centerLogger.error("modifyUnionName no permission playerID={}, unionID={}", playerID, getUnionID());
            return;
        }
        Tuple tuple = Tuple.of(name, getUnionID());
        CenterLoggerTool.centerLogger.warn("modifyUnionName unionID={}", getUnionID());

        MySQLUtils.sendChild(centerUnionWorld.getCenterDbConnectionPool(), "UPDATE `union` SET `unionName`=? WHERE unionID=?;", tuple, index, (isSuccess, rows, errorMsg) -> {
            if (isSuccess) {
                setUnionName(name);
                addActionLog(UnionActionLogTypeConst.ModifyUnionName);
                CenterLoggerTool.centerLogger.info("modifyUnionName success unionID={} name={}", getUnionID(), name);
                PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
                builder.setUpdateType(UnionUpdateTypeConst.ModifyUnionName);
                builder.setStrValue1(name);
                updateUnionData2Members((member) -> true, builder.build());
                consumer.accept(true);
            } else {
                CenterLoggerTool.centerLogger.error("modifyUnionName unionID={}, name={}, errorMsg={}", getUnionID(), name, errorMsg);
                consumer.accept(false);
            }
        });
    }

    /**
     * 修改设置公告
     *
     * @param notice
     */
    public void modifyUnionSettingNotice(String notice) {
        getSetting().setNotice(notice);
        addActionLog(UnionActionLogTypeConst.ModifyUnionNotice);
        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.ModifyNotice);
        builder.setStrValue1(notice);
        updateUnionData2Members((member) -> true, builder.build());
    }

    /**
     * 修改设置加入等级
     *
     * @param joinLimitLevel
     */
    public void modifyUnionSettingJoinLimitLevel(int joinLimitLevel) {
        getSetting().setJoinLimitLv(joinLimitLevel);
        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.ModifyJoinLimitLevel);
        builder.setIntValue1(joinLimitLevel);
        updateUnionData2Members((member) -> true, builder.build());
    }

    /**
     * 修改设置是否自动加入
     *
     * @param joinNeedApply
     */
    public void modifyUnionSettingJoinNeedApply(boolean joinNeedApply) {
        getSetting().setJoinNeedApply(joinNeedApply);
        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.ModifyJoinNeedApply);
        builder.setBoolValue1(joinNeedApply);
        updateUnionData2Members((member) -> true, builder.build());
    }

    /**
     * 修改成员职位
     *
     * @param playerID
     * @param targetPlayerID
     * @param position
     */
    public boolean modifyUnionMemberPosition(long playerID, long targetPlayerID, int position) {
        UnionMember unionMember = getUnionMember(playerID);
        if (unionMember == null) {
            CenterLoggerTool.centerLogger.error("modifyUnionMemberPosition not exist member playerID={}, unionID={}", playerID, getUnionID());
            return false;
        }
        if (!checkPermission(unionMember.getPosition(), UnionPermissionConst.ModifyPosition)) {
            CenterLoggerTool.centerLogger.error("modifyUnionMemberPosition no permission playerID={}, unionID={}", playerID, getUnionID());
            return false;
        }
        UnionMember targetMember = getUnionMember(targetPlayerID);
        if (targetMember == null) {
            CenterLoggerTool.centerLogger.error("modifyUnionMemberPosition not exist targetPlayerID={}, playerID={}, unionID={}", targetPlayerID, playerID, getUnionID());
            return false;
        }
        if (position <= 0 || position > UnionPositionConst.count) {
            CenterLoggerTool.centerLogger.error("modifyUnionMemberPosition invalid position={} targetPlayerID={}, playerID={}, unionID={}", position, targetPlayerID, playerID, getUnionID());
            return false;
        }
        unionMember.setPosition(position);
        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.ModifyMemberPosition);
        builder.setLongValue1(unionMember.getMemberID());
        builder.setIntValue1(position);
        updateUnionData2Members((member) -> true, builder.build());
        return true;
    }

    /**
     * 修改权限
     *
     * @param position
     * @param permission
     */
    public boolean changePermission(long playerID, int position, long permission) {
        UnionMember unionMember = getUnionMember(playerID);
        if (unionMember == null) {
            CenterLoggerTool.centerLogger.error("changePermission not exist member playerID={}, unionID={}", playerID, getUnionID());
            return false;
        }
        if (!checkPermission(unionMember.getPosition(), UnionPermissionConst.ChangePermission)) {
            CenterLoggerTool.centerLogger.error("changePermission no permission playerID={}, unionID={}", playerID, getUnionID());
            return false;
        }
        if (position <= 0 || position > UnionPositionConst.count) {
            CenterLoggerTool.centerLogger.error("changePermission invalid position={}, playerID={}, unionID={}", position, playerID, getUnionID());
            return false;
        }
        UnionPositionTemplate unionPositionTemplate = CenterConfigTool.getCenterUnionConfig().getUnionPositionTemplate(position);
        if (unionPositionTemplate == null) {
            CenterLoggerTool.centerLogger.error("changePermission not exist unionPositionTemplate position={} playerID={}, unionID={}", position, playerID, getUnionID());
            return false;
        }

        int[] forbidden = unionPositionTemplate.getForbiddenPermission();

        for (int i = 0; i < 32; i++) {
            long bit = 1L << i;
            //存在某个权限
            if ((bit & permission) > 0) {
                if (i + 1 > UnionPermissionConst.count) {
                    CenterLoggerTool.centerLogger.error("changePermission exceed max position={} playerID={}, unionID={}", position, playerID, getUnionID());
                    return false;
                }
                if (forbidden != null) {
                    if (ArrayUtils.contain(forbidden, i + 1)) {
                        CenterLoggerTool.centerLogger.error("changePermission forbidden={} max position={} playerID={}, unionID={}", i + 1, position, playerID, getUnionID());
                        return false;
                    }
                }
            }
        }

        setPermission(position, permission);

        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.ChangePermission);
        builder.setIntValue1(position);
        builder.setLongValue1(permission);
        updateUnionData2Members((member) -> true, builder.build());
        return true;
    }

    /**
     * 增加行为日志
     *
     * @param actionLogType
     * @param args
     */
    private void addActionLog(int actionLogType, String... args) {
        UnionTemplate unionTemplate = CenterConfigTool.getCenterUnionConfig().getUnionTemplate();

        UnionActionLog unionActionLog = UnionActionLog.create();
        unionActionLog.setTimestamp(SystemTimeTool.getMillTime());
        unionActionLog.setActionLogType(actionLogType);

        if (args.length > 0) {
            unionActionLog.setArgs(args);
        }
        addActionLogQueue(unionActionLog, unionTemplate.getLogMaxNum());
        PbUnion.PbUpdateUnionData.Builder builder = PbUnion.PbUpdateUnionData.newBuilder();
        builder.setUpdateType(UnionUpdateTypeConst.AddActionLog);
        builder.setActionLog(unionActionLog.save());
        updateUnionData2Members((member) -> true, builder.build());
    }

    /**
     * 创建联盟通知
     */
    public void onCreateUnion() {
        addActionLog(UnionActionLogTypeConst.CreateUnion);
    }

    /**
     * 增加成员通知
     *
     * @param unionMember
     */
    public void onAddUnionMember(UnionMember unionMember) {
        addActionLog(UnionActionLogTypeConst.JoinUnion);
    }

    /**
     * 删除成员通知
     *
     * @param unionMember
     */
    public void onRemoveUnionMember(UnionMember unionMember) {

    }

    /**
     * 资产变化通知
     *
     * @param type
     * @param changeValue
     * @param isAdd
     */
    public void onAssetChange(int type, long changeValue, boolean isAdd) {

    }

    /**
     * 获取存库数据
     */
    public Tuple getSaveData() {
        GeneratedMessageV3 generatedMessageV3 = this.save();
        Buffer buffer = Buffer.buffer(generatedMessageV3.toByteArray());
        Tuple tuple = Tuple.of(buffer, SystemTimeTool.getMillTime(), getUnionID());
        return tuple;
    }

    /**
     * 转换到轻量级数据
     *
     * @return
     */
    public CenterUnionLite convert() {
        CenterUnionLite centerUnionLite = new CenterUnionLite();
        centerUnionLite.setChildWorldIndex(centerUnionWorld.getIndex());
        centerUnionLite.setUnionID(getUnionID());
        centerUnionLite.setName(getUnionName());
        centerUnionLite.setLevel(getLevel());
        centerUnionLite.setMemberNum(getUnionMemberSize());
        //通过序列化 反序列化实现深克隆 发送到global线程使用
        PbUnion.PbUnionSettingData pbUnionSettingData = getSetting().save();
        try {
            centerUnionLite.getUnionSetting().load(pbUnionSettingData.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return centerUnionLite;
    }
}
