package com.highfly029.center.module.centerUnion;

import com.highfly029.center.constant.CenterConst;
import com.highfly029.center.module.centerUnion.CenterUnionLite;
import com.highfly029.center.tool.CenterLoggerTool;
import com.highfly029.center.world.CenterGlobalWorld;
import com.highfly029.center.world.Global2ChildEvent;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.manager.BaseManager;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.LongIntMap;
import com.highfly029.utils.collection.LongList;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.ObjObjMap;
import com.highfly029.utils.collection.ObjectList;

import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowIterator;

/**
 * @ClassName CenterUnionGlobalManager
 * @Description 联盟全局管理类
 * @Author liyunpeng
 **/
public class CenterUnionGlobalManager extends BaseManager {
    private final CenterGlobalWorld centerGlobalWorld;
    /**
     * 内存中所有union集合
     * key=unionID
     * value=CenterUnionLite
     */
    private final LongObjectMap<CenterUnionLite> allUnionMap = new LongObjectMap<>(CenterUnionLite[]::new);

    /**
     * key=name
     * value=CenterUnionLite
     * 按照名字缓存的联盟集合 方便查找
     */
    private final ObjObjMap<String, CenterUnionLite> nameCachedMap = new ObjObjMap<>(String[]::new, CenterUnionLite[]::new);

    /**
     * 有序的联盟列表 方便查找列表
     */
    private final ObjectList<CenterUnionLite> sortedUnionList = new ObjectList<>(CenterUnionLite[]::new);

    /**
     * 是否是有序列表 数据变化时设置true 查询前再真正的排序(惰性)
     */
    private boolean isSortedList;
    /**
     * 排序过期事件
     */
    private long sortExpireTime;
    /**
     * 排序缓存事件
     */
    private static final long SORT_CACHE_TIME = 60 * 1000;

    /**
     * 预加载缓存记录 防止多个childWorld同时加载union导致的并发问题
     * 只有union记录找不到时才会使用预加载记录 所以即使预加载失败也没问题
     */
    private final LongIntMap prepareUnionCache = new LongIntMap();

    public CenterUnionGlobalManager(CenterGlobalWorld world) {
        super(world);
        centerGlobalWorld = world;
    }

    /**
     * 增加联盟记录
     *
     * @param centerUnionLite
     * @param isAdd
     */
    public void loadUnion(CenterUnionLite centerUnionLite, boolean isAdd) {
        long unionID = centerUnionLite.getUnionID();
        allUnionMap.put(unionID, centerUnionLite);
        prepareUnionCache.remove(unionID);
        nameCachedMap.put(centerUnionLite.getName(), centerUnionLite);
        sortedUnionList.add(centerUnionLite);
        setSortedList(false);
    }

    /**
     * 移除联盟记录
     *
     * @param unionID
     * @param isDisband
     */
    public void unloadUnion(long unionID, boolean isDisband) {
        CenterUnionLite centerUnionLite = allUnionMap.remove(unionID);
        prepareUnionCache.remove(unionID);
        //只有解散的联盟才会删除缓存数据
        if (isDisband) {
            nameCachedMap.remove(centerUnionLite.getName());
            sortedUnionList.remove(centerUnionLite);
        }
    }

    /**
     * 获取联盟对应的索引
     *
     * @param unionID
     * @return
     */
    public int getChildWorldIndex(long unionID) {
        CenterUnionLite centerUnionLite = allUnionMap.get(unionID);
        if (centerUnionLite != null && centerUnionLite.getChildWorldIndex() > 0) {
            return centerUnionLite.getChildWorldIndex();
        } else {
            return prepareUnionCache.get(unionID);
        }
    }

    /**
     * 增加联盟预加载记录
     *
     * @param childWorldIndex
     * @param unionID
     */
    public void addPrepareUnionID(int childWorldIndex, long unionID) {
        prepareUnionCache.put(unionID, childWorldIndex);
    }

    /**
     * 按照名字查找
     *
     * @param name
     * @return
     */
    public CenterUnionLite getUnionLiteByName(String name) {
        return nameCachedMap.get(name);
    }

    /**
     * 获取有序联盟列表
     *
     * @return
     */
    public ObjectList<CenterUnionLite> getSortedList() {
        long now = SystemTimeTool.getMillTime();
        if (!isSortedList() && now > sortExpireTime) {
            sortedUnionList.sort(this::compareUnionList);
            setSortedList(true);
            sortExpireTime = now + SORT_CACHE_TIME;
        }
        return sortedUnionList;
    }

    /**
     * 排序方式
     *
     * @param unionLite1
     * @param unionLite2
     * @return
     */
    private int compareUnionList(CenterUnionLite unionLite1, CenterUnionLite unionLite2) {
        return Long.compare(unionLite1.getUnionID(), unionLite2.getUnionID());
    }

    public boolean isSortedList() {
        return isSortedList;
    }

    public void setSortedList(boolean sortedList) {
        isSortedList = sortedList;
    }

    /**
     * 预热
     */
    public void warmUp() {
        int childWorldSize = CenterConst.getUnionWorldList().size();
        int everyChildNum = 2;
        int limitNum = everyChildNum * childWorldSize;
        MySQLUtils.sendGlobal(centerGlobalWorld.getCenterDbConnectionPool(), "SELECT `unionID` FROM `union` ORDER BY `saveTime` DESC LIMIT " + limitNum, null, ((isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                IntObjectMap<LongList> map = new IntObjectMap<>();

                int mask = CenterConst.getUnionWorldList().size();
                int pos = 0;
                RowIterator<Row> iterator = successRows.iterator();
                while (iterator.hasNext()) {
                    Row row = iterator.next();
                    long unionID = row.getLong("unionID");
                    pos = pos % mask;
                    int childWorldIndex = CenterConst.getUnionWorldList().get(pos);
                    LongList list = map.get(childWorldIndex);
                    if (list == null) {
                        list = new LongList();
                        map.put(childWorldIndex, list);
                    }
                    list.add(unionID);
                    pos++;
                }
                map.foreachImmutable((worldIndex, list) ->
                        centerGlobalWorld.postEvent2Child(worldIndex, Global2ChildEvent.UNION_WARM_UP, list, null));
            } else {
                CenterLoggerTool.centerLogger.error("union warmUp error={}", errorMsg);
            }
        }));
    }


    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {

    }

    @Override
    public void onMinute() {

    }

    @Override
    public void onShutDown() {

    }
}
