package com.highfly029.center.module.centerActivity;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.center.constant.CenterConst;
import com.highfly029.center.world.CenterGlobalWorld;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbActivity;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.world.GlobalWorld;

/**
 * @ClassName CenterActivityGlobalHandler
 * @Description CenterActivityGlobalHandler
 * @Author liyunpeng
 **/
public class CenterActivityGlobalHandler implements IRegisterServerGlobalProtocol {
    private CenterGlobalWorld centerGlobalWorld;

    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {
        this.centerGlobalWorld = (CenterGlobalWorld) globalWorld;

        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterGetOpenedActivity, this::getOpenedActivity);
    }

    /**
     * 获取开启的活动
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void getOpenedActivity(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbActivity.Logic2CenterGetOpenedActivity req = PbActivity.Logic2CenterGetOpenedActivity.parseFrom(packet.getData());
        int worldIndex = CenterConst.getActivityWorldIndex();
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }
}
