package com.highfly029.center.module;

import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.core.interfaces.IRegisterServerChildProtocol;
import com.highfly029.core.session.Session;

/**
 * @ClassName BaseChildHandler
 * @Description BaseChildHandler
 * @Author liyunpeng
 **/
public abstract class BaseServerChildHandler implements IRegisterServerChildProtocol {

    /**
     * 发送给玩家@{infoCode}
     *
     * @param session
     * @param playerID
     * @param infoCode
     */
    public void sendInfoCode2Player(Session session, long playerID, int infoCode) {
        PbCommon.Center2SceneInfoCode.Builder builder = PbCommon.Center2SceneInfoCode.newBuilder();
        builder.setInfoCode(infoCode);
        builder.setPlayerID(playerID);
        session.sendMsgS2S(PtCode.Center2SceneInfoCode, builder.build());
    }

    /**
     * 发送给玩家@{infoCode}
     *
     * @param session
     * @param playerID
     * @param infoCode
     * @param args
     */
    public void sendInfoCode2Player(Session session, long playerID, int infoCode, String... args) {
        PbCommon.Center2SceneInfoCode.Builder builder = PbCommon.Center2SceneInfoCode.newBuilder();
        builder.setInfoCode(infoCode);
        builder.setPlayerID(playerID);
        for (String arg : args) {
            builder.addArgs(arg);
        }
        session.sendMsgS2S(PtCode.Center2SceneInfoCode, builder.build());
    }
}
