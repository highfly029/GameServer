package com.highfly029.center.module.centerAuction;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.center.module.BaseServerChildHandler;
import com.highfly029.center.world.CenterAuctionWorld;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbAuction;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.session.Session;
import com.highfly029.core.world.ChildWorld;

/**
 * @ClassName CenterAuctionChildHandler
 * @Description CenterAuctionChildHandler
 * @Author liyunpeng
 **/
public class CenterAuctionChildHandler extends BaseServerChildHandler {
    private CenterAuctionWorld centerAuctionWorld;

    @Override
    public void registerChildProtocol(ChildWorld childWorld) throws Exception {
        if (!(childWorld instanceof CenterAuctionWorld)) {
            return;
        }
        centerAuctionWorld = (CenterAuctionWorld) childWorld;

        childWorld.registerServerChildProtocol(PtCode.Logic2CenterAuctionSearch, this::auctionSearch);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterAuctionShelfItem, this::auctionShelfItem);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterAuctionBuy, this::auctionBuy);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterAuctionCancelShelfItem, this::auctionCancelShelfItem);
    }

    /**
     * 拍卖行查询
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void auctionSearch(Session session, int ptCode, GeneratedMessageV3 message) {
        PbAuction.Logic2CenterAuctionSearch req = (PbAuction.Logic2CenterAuctionSearch) message;
        centerAuctionWorld.getCenterAuctionManager().auctionSearch(session, req.getAuctionType(), req.getPlayerID());
    }

    /**
     * 拍卖行上架物品
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void auctionShelfItem(Session session, int ptCode, GeneratedMessageV3 message) {
        PbAuction.Logic2CenterAuctionShelfItem req = (PbAuction.Logic2CenterAuctionShelfItem) message;
        centerAuctionWorld.getCenterAuctionManager().auctionShelfItem(session, req.getAuctionType(), req.getPlayerID(),
                PbCommonUtils.auctionItemDataPb2Obj(req.getAuctionItemData()));
    }

    /**
     * 拍卖行购买物品
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void auctionBuy(Session session, int ptCode, GeneratedMessageV3 message) {
        PbAuction.Logic2CenterAuctionBuy req = (PbAuction.Logic2CenterAuctionBuy) message;
        centerAuctionWorld.getCenterAuctionManager().auctionBuy(session, req.getAuctionType(), req.getPlayerID(),
                PbCommonUtils.auctionItemDataPb2Obj(req.getAuctionItemData()));
    }

    /**
     * 拍卖行取消上架
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void auctionCancelShelfItem(Session session, int ptCode, GeneratedMessageV3 message) {
        PbAuction.Logic2CenterAuctionCancelShelfItem req = (PbAuction.Logic2CenterAuctionCancelShelfItem) message;
        centerAuctionWorld.getCenterAuctionManager().auctionCancelShelfItem(session, req.getAuctionType(), req.getId(), req.getPlayerID());
    }
}
