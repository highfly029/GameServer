package com.highfly029.center.module.centerUnion;

import com.highfly029.common.data.union.UnionLite;

/**
 * @ClassName CenterUnionLite
 * @Description CenterUnionLite
 * @Author liyunpeng
 **/
public class CenterUnionLite extends UnionLite {
    /**
     * 联盟所在索引
     */
    private int childWorldIndex;

    public int getChildWorldIndex() {
        return childWorldIndex;
    }

    public void setChildWorldIndex(int childWorldIndex) {
        this.childWorldIndex = childWorldIndex;
    }


    @Override
    public String toString() {
        return "CenterUnionLite{" +
                "childWorldIndex=" + childWorldIndex +
                "} " + super.toString();
    }
}
