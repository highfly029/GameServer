package com.highfly029.center.module.centerUnion;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.center.constant.CenterConst;
import com.highfly029.center.tool.CenterLoggerTool;
import com.highfly029.center.world.CenterGlobalWorld;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName CenterUnionGlobalHandler
 * @Description 中心服联盟global协议处理
 * @Author liyunpeng
 **/
public class CenterUnionGlobalHandler implements IRegisterServerGlobalProtocol {
    private CenterGlobalWorld centerGlobalWorld;
    /**
     * 新增联盟的自增索引
     */
    private int randomUnionIndex;

    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {
        centerGlobalWorld = (CenterGlobalWorld) globalWorld;

        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterGetUnionList, this::getUnionList);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterCreateUnion, this::createUnion);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterPullPlayerUnionInfo, this::pullPlayerUnionInfo);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterMemberLogout, this::memberLogout);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterQuitUnion, this::quitUnion);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterDisbandUnion, this::disbandUnion);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterApplyJoinUnion, this::applyJoinUnion);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterInviteJoinUnion, this::inviteJoinUnion);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterHandleJoinUnion, this::handleJoinUnion);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterJoinUnionDoubleCheckResult, this::onJoinUnionDoubleCheckResult);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterHandleInviteJoinUnion, this::handleBeInviteJoinUnion);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterKickOutUnionMember, this::kickOutMember);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterChangeLeader, this::changeLeader);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterModifyUnionName, this::modifyUnionName);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterModifyUnionSetting, this::modifyUnionSetting);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterModifyUnionMemberPosition, this::modifyUnionMemberPosition);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterChangePermission, this::changePermission);
    }

    /**
     * 随机找到一个worldIndex去添加联盟
     *
     * @return
     */
    private int getRandomWorldIndex() {
        IntList unionIndexList = CenterConst.getUnionWorldList();
        int pos = randomUnionIndex++ % unionIndexList.size();
        int worldIndex = unionIndexList.get(pos);
        return worldIndex;
    }

    /**
     * 获取联盟列表
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void getUnionList(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterGetUnionList req = PbUnion.Logic2CenterGetUnionList.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        int pageIndex = req.getPageIndex();
        String unionName = req.getUnionName();

        int pageMaxNum = 10;

        PbUnion.Center2LogicGetUnionList.Builder builder = PbUnion.Center2LogicGetUnionList.newBuilder();
        builder.setPlayerID(playerID);

        CenterUnionGlobalManager centerUnionManager = centerGlobalWorld.getCenterUnionGlobalManager();
        //如果保证内存里有全部的联盟数据则需要开服时全部加载进来或者直接查数据库
        if (pageIndex > 0) {
            //按页查找
            ObjectList<CenterUnionLite> list = centerUnionManager.getSortedList();
            int startPos = (pageIndex - 1) * pageMaxNum;
            int endPos = startPos + pageMaxNum;
            int size = list.size();
            if (startPos >= size) {
                CenterLoggerTool.centerLogger.error("getUnionList invalid pageIndex playerID={}", playerID);
                return;
            }
            endPos = Math.min(endPos, size);

            for (int i = startPos; i < endPos; i++) {
                CenterUnionLite centerUnionLite = list.get(i);
                builder.addUnionList(centerUnionLite.save());
            }
            session.sendMsgS2S(PtCode.Center2LogicGetUnionList, builder.build());

        } else if (unionName != null && unionName.length() > 0) {
            //按名字查找
            CenterUnionLite centerUnionLite = centerUnionManager.getUnionLiteByName(unionName);
            if (centerUnionLite != null) {
                //内存中找到直接使用
                builder.addUnionList(centerUnionLite.save());
                session.sendMsgS2S(PtCode.Center2LogicGetUnionList, builder.build());
            } else {
                //TODO 找不到返回客户端
            }

        } else {
            CenterLoggerTool.centerLogger.error("getUnionList invalid parameter");
        }
    }

    /**
     * 创建联盟
     */
    private void createUnion(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        int worldIndex = getRandomWorldIndex();
        PbUnion.Logic2CenterCreateUnion req = PbUnion.Logic2CenterCreateUnion.parseFrom(packet.getData());
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 拉取玩家联盟信息
     */
    private void pullPlayerUnionInfo(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterPullPlayerUnionInfo req = PbUnion.Logic2CenterPullPlayerUnionInfo.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        if (worldIndex <= 0) {
            //如果没找到联盟所在子世界 则随机找一个
            worldIndex = getRandomWorldIndex();
            centerGlobalWorld.getCenterUnionGlobalManager().addPrepareUnionID(worldIndex, unionID);
        }
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 玩家登出
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void memberLogout(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterMemberLogout req = PbUnion.Logic2CenterMemberLogout.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 主动退出联盟
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void quitUnion(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterQuitUnion req = PbUnion.Logic2CenterQuitUnion.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 解散联盟
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void disbandUnion(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterDisbandUnion req = PbUnion.Logic2CenterDisbandUnion.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 申请加入联盟
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void applyJoinUnion(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterApplyJoinUnion req = PbUnion.Logic2CenterApplyJoinUnion.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        if (worldIndex <= 0) {
            //如果没找到联盟所在子世界 则随机找一个
            worldIndex = getRandomWorldIndex();
            centerGlobalWorld.getCenterUnionGlobalManager().addPrepareUnionID(worldIndex, unionID);
        }
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 邀请加入联盟
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void inviteJoinUnion(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterInviteJoinUnion req = PbUnion.Logic2CenterInviteJoinUnion.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 处理入盟消息
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void handleJoinUnion(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterHandleJoinUnion req = PbUnion.Logic2CenterHandleJoinUnion.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 加入联盟检测结果
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void onJoinUnionDoubleCheckResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterJoinUnionDoubleCheckResult req = PbUnion.Logic2CenterJoinUnionDoubleCheckResult.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 玩家响应邀请加入联盟
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void handleBeInviteJoinUnion(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterHandleInviteJoinUnion req = PbUnion.Logic2CenterHandleInviteJoinUnion.parseFrom(packet.getData());
        long targetUnionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(targetUnionID);
        if (worldIndex <= 0) {
            //如果没找到联盟所在子世界 则随机找一个
            worldIndex = getRandomWorldIndex();
            centerGlobalWorld.getCenterUnionGlobalManager().addPrepareUnionID(worldIndex, targetUnionID);
        }
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 踢出成员
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void kickOutMember(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterKickOutUnionMember req = PbUnion.Logic2CenterKickOutUnionMember.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 更换盟主
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void changeLeader(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterChangeLeader req = PbUnion.Logic2CenterChangeLeader.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 修改联盟名字
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void modifyUnionName(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterModifyUnionName req = PbUnion.Logic2CenterModifyUnionName.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 修改联盟设置
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void modifyUnionSetting(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterModifyUnionSetting req = PbUnion.Logic2CenterModifyUnionSetting.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 修改联盟成员职位
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void modifyUnionMemberPosition(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterModifyUnionMemberPosition req = PbUnion.Logic2CenterModifyUnionMemberPosition.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 修改权限
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void changePermission(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Logic2CenterChangePermission req = PbUnion.Logic2CenterChangePermission.parseFrom(packet.getData());
        long unionID = req.getUnionID();
        int worldIndex = centerGlobalWorld.getCenterUnionGlobalManager().getChildWorldIndex(unionID);
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

}
