package com.highfly029.center.module.centerActivity;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.center.module.BaseServerChildHandler;
import com.highfly029.center.world.CenterActivityWorld;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbActivity;
import com.highfly029.core.session.Session;
import com.highfly029.core.world.ChildWorld;

/**
 * @ClassName CenterActivityChildHandler
 * @Description CenterActivityChildHandler
 * @Author liyunpeng
 **/
public class CenterActivityChildHandler extends BaseServerChildHandler {
    private CenterActivityWorld centerActivityWorld;

    @Override
    public void registerChildProtocol(ChildWorld childWorld) throws Exception {
        if (!(childWorld instanceof CenterActivityWorld)) {
            return;
        }
        centerActivityWorld = (CenterActivityWorld) childWorld;

        childWorld.registerServerChildProtocol(PtCode.Logic2CenterGetOpenedActivity, this::getOpenedActivity);
    }

    /**
     * 获取开启的活动
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void getOpenedActivity(Session session, int ptCode, GeneratedMessageV3 message) {
        PbActivity.Logic2CenterGetOpenedActivity req = (PbActivity.Logic2CenterGetOpenedActivity) message;
        centerActivityWorld.getCenterActivityManager().getCenterOpenedActivity(session);
    }
}
