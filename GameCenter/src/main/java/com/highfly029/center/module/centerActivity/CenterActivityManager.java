package com.highfly029.center.module.centerActivity;

import com.highfly029.center.tool.CenterConfigTool;
import com.highfly029.center.tool.CenterLoggerTool;
import com.highfly029.center.world.CenterActivityWorld;
import com.highfly029.common.data.activity.ActivityTimeData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbActivity;
import com.highfly029.common.template.activity.ActivityDriveTypeConst;
import com.highfly029.common.template.activity.ActivityTemplate;
import com.highfly029.common.template.activity.ActivityTimeTypeConst;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.manager.BaseManager;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;

import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName CenterActivityManager
 * @Description CenterActivityManager
 * @Author liyunpeng
 **/
public class CenterActivityManager extends BaseManager {
    private CenterActivityWorld centerActivityWorld;
    /**
     * 开启中的中心服活动集合
     */
    private final IntSet openedCenterActivitySet = new IntSet();

    public CenterActivityManager(CenterActivityWorld world) {
        super(world);
        this.centerActivityWorld = world;
    }

    @Override
    public void init() {
        CenterActivityConfig centerActivityConfig = CenterConfigTool.getCenterActivityConfig();
        IntObjectMap<ActivityTemplate> activityTemplateMap = centerActivityConfig.getActivityTemplateMap();
        IntObjectMap<ActivityTimeData> centerActivityTimeDataMap = centerActivityConfig.getCenterActivityTemplateDataMap();

        MySQLUtils.sendChild(centerActivityWorld.getCenterDbConnectionPool(), "SELECT * FROM activity;", null, centerActivityWorld.getIndex(), (isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                for (Row row : successRows) {
                    int id = row.getInteger("id");
                    long startTime = row.getLong("startTime");
                    long endTime = row.getLong("endTime");
                    ActivityTemplate activityTemplate = activityTemplateMap.get(id);
                    //设置开启中的循环活动的时间
                    if (activityTemplate.getActivityTimeType() == ActivityTimeTypeConst.CycleTime) {
                        ActivityTimeData activityTimeData = centerActivityTimeDataMap.get(id);
                        activityTimeData.setStartTime(startTime);
                        activityTimeData.setEndTime(endTime);
                    }
                    //逻辑服启动时初始化已经在开启中的活动
                    openedCenterActivitySet.add(id);
                }
            } else {
                CenterLoggerTool.centerLogger.error("ActivityGlobalManager init select fail {}", errorMsg);
            }
        });
    }

    /**
     * logic获取中心服开启的活动
     */
    public void getCenterOpenedActivity(Session session) {
        PbActivity.Center2LogicReturnOpenedActivity.Builder builder = PbActivity.Center2LogicReturnOpenedActivity.newBuilder();
        openedCenterActivitySet.foreachImmutable(builder::addOpenedIDList);
        session.sendMsgS2S(PtCode.Center2LogicReturnOpenedActivity, builder.build());
    }

    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {
        CenterActivityConfig centerActivityConfig = CenterConfigTool.getCenterActivityConfig();
        IntObjectMap<ActivityTemplate> activityTemplateMap = centerActivityConfig.getActivityTemplateMap();
        IntObjectMap<ActivityTimeData> centerActivityTimeDataMap = centerActivityConfig.getCenterActivityTemplateDataMap();
        long now = SystemTimeTool.getMillTime();
        activityTemplateMap.foreachImmutable((id, activityTemplate) -> {
            if (activityTemplate.getActivityDriveType() == ActivityDriveTypeConst.Center) {
                ActivityTimeData activityTimeData = centerActivityTimeDataMap.get(id);
                //强制关闭活动
                if (activityTimeData.getForceCloseTime() > 0 && now >= activityTimeData.getForceCloseTime()) {
                    if (openedCenterActivitySet.contains(activityTemplate.getId())) {
                        closeActivity(activityTemplate, activityTimeData, true);
                    }
                    return;
                }

                if (now >= activityTimeData.getStartTime() && now <= activityTimeData.getEndTime()) {
                    if (!openedCenterActivitySet.contains(activityTemplate.getId())) {
                        openActivity(activityTemplate, activityTimeData);
                    }
                }
                if (now > activityTimeData.getEndTime() && openedCenterActivitySet.contains(activityTemplate.getId())) {
                    closeActivity(activityTemplate, activityTimeData, false);
                    //重新计算循环活动的下次时间
                    if (activityTemplate.getActivityTimeType() == ActivityTimeTypeConst.CycleTime) {
                        activityTimeData.refreshCronExpressionTime();
                    }
                }
            }
        });
    }

    /**
     * 开启新的活动
     *
     * @param activityTemplate
     * @param activityTimeData
     */
    private void openActivity(ActivityTemplate activityTemplate, ActivityTimeData activityTimeData) {
        openedCenterActivitySet.add(activityTemplate.getId());
        CenterLoggerTool.centerLogger.info("开启活动 {}", activityTemplate.getId());
        Tuple tuple = Tuple.of(activityTemplate.getId(), activityTimeData.getStartTime(), activityTimeData.getEndTime());
        MySQLUtils.sendChild(centerActivityWorld.getCenterDbConnectionPool(), "INSERT INTO activity VALUES(?, ?, ?);", tuple, centerActivityWorld.getIndex(), (isSuccess, successRows, errorMsg) -> {
            if (!isSuccess) {
                CenterLoggerTool.centerLogger.error("openActivity insert fail {} {}", activityTemplate.getId(), errorMsg);
            }
        });

        PbActivity.Center2LogicOpenActivity.Builder builder = PbActivity.Center2LogicOpenActivity.newBuilder();
        builder.setId(activityTemplate.getId());
        centerActivityWorld.sendMsg2AllLogicServer(PtCode.Center2LogicOpenActivity, builder.build());
    }

    /**
     * 关闭活动
     *
     * @param activityTemplate
     * @param activityTimeData
     */
    private void closeActivity(ActivityTemplate activityTemplate, ActivityTimeData activityTimeData, boolean forceClose) {
        openedCenterActivitySet.remove(activityTemplate.getId());
        if (forceClose) {
            CenterLoggerTool.centerLogger.info("强制关闭活动 {}", activityTemplate.getId());
        } else {
            CenterLoggerTool.centerLogger.info("关闭活动 {}", activityTemplate.getId());
        }
        MySQLUtils.sendChild(centerActivityWorld.getCenterDbConnectionPool(), "DELETE FROM activity WHERE id=" + activityTemplate.getId(), null, centerActivityWorld.getIndex(), (isSuccess, successRows, errorMsg) -> {
            if (!isSuccess) {
                CenterLoggerTool.centerLogger.error("closeActivity delete fail {} {}", activityTemplate.getId(), errorMsg);
            }
        });

        PbActivity.Center2LogicCloseActivity.Builder builder = PbActivity.Center2LogicCloseActivity.newBuilder();
        builder.setId(activityTemplate.getId());
        builder.setForceClose(forceClose);
        centerActivityWorld.sendMsg2AllLogicServer(PtCode.Center2LogicCloseActivity, builder.build());
    }

    @Override
    public void onMinute() {

    }

    @Override
    public void onShutDown() {

    }
}
