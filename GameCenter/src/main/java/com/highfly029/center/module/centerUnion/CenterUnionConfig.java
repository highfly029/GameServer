package com.highfly029.center.module.centerUnion;

import com.highfly029.common.template.union.UnionAssetTemplate;
import com.highfly029.common.template.union.UnionLevelTemplate;
import com.highfly029.common.template.union.UnionPositionTemplate;
import com.highfly029.common.template.union.UnionTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName CenterConfig
 * @Description CenterConfig
 * @Author liyunpeng
 **/
public class CenterUnionConfig extends BaseGlobalConfig {
    /**
     * 联盟配置
     */
    private UnionTemplate unionTemplate;
    /**
     * 联盟等级配置
     */
    private final IntObjectMap<UnionLevelTemplate> unionLevelMap = new IntObjectMap<>(UnionLevelTemplate[]::new);
    /**
     * 联盟职位配置
     */
    private final IntObjectMap<UnionPositionTemplate> unionPositionMap = new IntObjectMap<>(UnionPositionTemplate[]::new);
    /**
     * 联盟资产配置
     */
    private final IntObjectMap<UnionAssetTemplate> unionAssetMap = new IntObjectMap<>(UnionAssetTemplate[]::new);

    @Override
    public void check(boolean isHotfix) {
        //check 默认拥有的权限和禁用的权限互斥
    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(UnionTemplate.class);
        unionTemplate = (UnionTemplate) list.get(0);

        list = TemplateTool.dataTemplates.get(UnionLevelTemplate.class);
        UnionLevelTemplate unionLevelTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            unionLevelTemplate = (UnionLevelTemplate) list.get(i);
            unionLevelMap.put(unionLevelTemplate.getLevel(), unionLevelTemplate);
        }

        list = TemplateTool.dataTemplates.get(UnionPositionTemplate.class);
        UnionPositionTemplate positionTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            positionTemplate = (UnionPositionTemplate) list.get(i);
            unionPositionMap.put(positionTemplate.getPosition(), positionTemplate);
        }

        list = TemplateTool.dataTemplates.get(UnionAssetTemplate.class);
        UnionAssetTemplate unionAssetTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            unionAssetTemplate = (UnionAssetTemplate) list.get(i);
            unionAssetMap.put(unionAssetTemplate.getAssetID(), unionAssetTemplate);
        }
    }

    /**
     * 获取联盟模版
     *
     * @return
     */
    public UnionTemplate getUnionTemplate() {
        return unionTemplate;
    }

    /**
     * 获取联盟等级模版
     *
     * @param level
     * @return
     */
    public UnionLevelTemplate getUnionLevelTemplate(int level) {
        return unionLevelMap.get(level);
    }

    /**
     * 获取联盟职位模版
     *
     * @param position
     * @return
     */
    public UnionPositionTemplate getUnionPositionTemplate(int position) {
        return unionPositionMap.get(position);
    }

    /**
     * 获取联盟资产模版
     *
     * @param assetID
     * @return
     */
    public UnionAssetTemplate getUnionAssetTemplate(int assetID) {
        return unionAssetMap.get(assetID);
    }

}
