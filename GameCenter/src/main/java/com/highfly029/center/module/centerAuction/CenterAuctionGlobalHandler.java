package com.highfly029.center.module.centerAuction;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.center.constant.CenterConst;
import com.highfly029.center.world.CenterGlobalWorld;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbAuction;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.world.GlobalWorld;

/**
 * @ClassName CenterAuctionGlobalHandler
 * @Description CenterAuctionGlobalHandler
 * @Author liyunpeng
 **/
public class CenterAuctionGlobalHandler implements IRegisterServerGlobalProtocol {
    private CenterGlobalWorld centerGlobalWorld;

    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {
        centerGlobalWorld = (CenterGlobalWorld) globalWorld;

        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterAuctionSearch, this::auctionSearch);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterAuctionShelfItem, this::auctionShelfItem);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterAuctionBuy, this::auctionBuy);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2CenterAuctionCancelShelfItem, this::auctionCancelShelfItem);
    }

    /**
     * 根据拍卖类型获取子世界索引
     *
     * @param auctionType
     * @return
     */
    private int getWorldIndexByAuctionType(int auctionType) {
        return CenterConst.getAuctionWorldIndex(auctionType);
    }

    /**
     * 拍卖行查询
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void auctionSearch(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbAuction.Logic2CenterAuctionSearch req = PbAuction.Logic2CenterAuctionSearch.parseFrom(packet.getData());
        int worldIndex = getWorldIndexByAuctionType(req.getAuctionType());
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 拍卖行上架物品
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void auctionShelfItem(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbAuction.Logic2CenterAuctionShelfItem req = PbAuction.Logic2CenterAuctionShelfItem.parseFrom(packet.getData());
        int worldIndex = getWorldIndexByAuctionType(req.getAuctionType());
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 拍卖行购买物品
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void auctionBuy(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbAuction.Logic2CenterAuctionBuy req = PbAuction.Logic2CenterAuctionBuy.parseFrom(packet.getData());
        int worldIndex = getWorldIndexByAuctionType(req.getAuctionType());
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

    /**
     * 拍卖行取消上架
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void auctionCancelShelfItem(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbAuction.Logic2CenterAuctionCancelShelfItem req = PbAuction.Logic2CenterAuctionCancelShelfItem.parseFrom(packet.getData());
        int worldIndex = getWorldIndexByAuctionType(req.getAuctionType());
        centerGlobalWorld.addServerMessage2ChildWorld(worldIndex, ptCode, session, req);
    }

}
