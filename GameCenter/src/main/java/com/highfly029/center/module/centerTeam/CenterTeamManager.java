package com.highfly029.center.module.centerTeam;

import com.highfly029.center.world.CenterTeamWorld;
import com.highfly029.core.manager.BaseManager;

/**
 * @ClassName CenterTeamManager
 * @Description 队伍管理类
 * @Author liyunpeng
 **/
public class CenterTeamManager extends BaseManager {

    public CenterTeamManager(CenterTeamWorld centerTeamWorld) {
        super(centerTeamWorld);
    }

    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {

    }

    @Override
    public void onMinute() {

    }

    @Override
    public void onShutDown() {

    }
}
