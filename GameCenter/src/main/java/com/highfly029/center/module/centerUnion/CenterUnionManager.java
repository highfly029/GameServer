package com.highfly029.center.module.centerUnion;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.highfly029.center.module.centerUnion.CenterUnion;
import com.highfly029.center.tool.CenterLoggerTool;
import com.highfly029.center.world.CenterUnionWorld;
import com.highfly029.center.world.Child2GlobalEvent;
import com.highfly029.common.data.union.Union;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.manager.BaseManager;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.utils.SnowFlake;
import com.highfly029.utils.collection.LongList;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.LongSet;
import com.highfly029.utils.collection.ObjectList;

import io.vertx.core.buffer.Buffer;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName CenterUnionManager
 * @Description 联盟管理类
 * @Author liyunpeng
 **/
public class CenterUnionManager extends BaseManager {
    private CenterUnionWorld centerUnionWorld;
    /**
     * 正在从数据库中加载的联盟集合 防止多个玩家同时异步从数据库读取联盟数据
     */
    private final LongSet unionLoadingMarkSet = new LongSet();
    /**
     * 联盟加载后的回调集合
     */
    private final LongObjectMap<ObjectList<Consumer>> unionLoadCallbackMap = new LongObjectMap<>(ObjectList[]::new);

    /**
     * 联盟数据集合
     */
    private final LongObjectMap<CenterUnion> unionMap = new LongObjectMap<>(CenterUnion[]::new);

    /**
     * 联盟id生成器
     */
    private static final SnowFlake snowFlake = new SnowFlake(ConfigConst.identifier);


    public CenterUnionManager(CenterUnionWorld centerUnionWorld) {
        super(centerUnionWorld);
        this.centerUnionWorld = centerUnionWorld;
    }

    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {

    }

    @Override
    public void onMinute() {
        saveAllUnions();
    }

    /**
     * 生成联盟id
     *
     * @return
     */
    public long generateUnionID() {
        return snowFlake.nextID();
    }

    /**
     * 创建联盟数据
     *
     * @return
     */
    public CenterUnion create() {
        CenterUnion union = new CenterUnion();
        union.setCenterUnionWorld(centerUnionWorld);
        return union;
    }

    /**
     * 释放联盟数据
     *
     * @param union
     */
    public void free(CenterUnion union) {

    }

    /**
     * 增加联盟
     *
     * @param union
     */
    public void addUnion(CenterUnion union, boolean isAdd) {
        unionMap.put(union.getUnionID(), union);
        centerUnionWorld.postEvent2Global(Child2GlobalEvent.LOAD_UNION, union.convert(), isAdd);
    }

    /**
     * 删除联盟
     *
     * @param unionID
     */
    public void removeUnion(long unionID, boolean isDisband) {
        CenterUnion union = unionMap.remove(unionID);
        centerUnionWorld.postEvent2Global(Child2GlobalEvent.UNLOAD_UNION, unionID, isDisband);
        free(union);
    }

    /**
     * 查找内存中的联盟数据
     *
     * @param unionID
     * @return
     */
    public CenterUnion getUnion(long unionID) {
        return unionMap.get(unionID);
    }

    /**
     * 是否存在联盟
     *
     * @param unionID
     * @return
     */
    public boolean exist(long unionID) {
        return unionMap.contains(unionID);
    }

    /**
     * 从数据库中加载联盟
     * 成功后会自动自行回调
     *
     * @param unionID
     */
    public void loadUnionFromDB(long unionID) {
        if (exist(unionID)) {
            CenterLoggerTool.centerLogger.warn("loadUnionFromDB UnionID={} already exist", unionID);
            return;
        }
        if (unionLoadingMarkSet.contains(unionID)) {
            CenterLoggerTool.centerLogger.warn("loadUnionFromDB unionID={} duplicate req, wait a moment!", unionID);
            return;
        }
        unionLoadingMarkSet.add(unionID);
        Tuple tuple = Tuple.of(unionID);
        MySQLUtils.sendChild(centerUnionWorld.getCenterDbConnectionPool(), "SELECT unionID, unionName, unionData FROM `union` WHERE unionID=?;", tuple, centerUnionWorld.getIndex(), (isSuccess, rows, errorMsg) -> {
            //只要数据库执行完毕 不管成功失败都删除
            unionLoadingMarkSet.remove(unionID);

            if (isSuccess) {
                if (rows.size() == 0) {
                    onLoadUnionFromDBFail(unionID, "loadUnionFromDB not exist union");
                } else {
                    try {
                        Row row = rows.iterator().next();
                        Buffer buffer = row.getBuffer("unionData");
                        byte[] data = buffer.getBytes();
                        CenterUnion union = create();
                        union.load(data);
                        union.setUnionName(row.getString("unionName"));
                        union.setUnionID(unionID);
                        //目前为止没有修改过union数据
                        union.setChange(false);

                        union.onLoad();

                        addUnion(union, false);

                        onLoadUnionFromDBSuccess(union);
                    } catch (Exception e) {
                        CenterLoggerTool.centerLogger.error("loadUnion parse fail unionID=" + unionID, e);
                        onLoadUnionFromDBFail(unionID, e.getMessage());
                    }
                }
            } else {
                onLoadUnionFromDBFail(unionID, errorMsg);
            }
        });
    }

    /**
     * 增加加载联盟后的回调事件
     *
     * @param unionID
     * @param callback
     */
    public void addLoadUnionCallback(long unionID, Consumer<CenterUnion> callback) {
        ObjectList<Consumer> list = unionLoadCallbackMap.get(unionID);
        if (list == null) {
            list = new ObjectList<>(Consumer[]::new);
            unionLoadCallbackMap.put(unionID, list);
        }
        list.add(callback);
    }

    /**
     * 从数据库加载联盟成功后的回调
     *
     * @param union
     */
    private void onLoadUnionFromDBSuccess(Union union) {
        CenterLoggerTool.centerLogger.info("loadUnion from db success unionID={}", union.getUnionID());
        //加载联盟成功后处理之前增加的回调逻辑
        ObjectList<Consumer> list = unionLoadCallbackMap.get(union.getUnionID());
        if (list != null) {
            for (int i = 0, len = list.size(); i < len; i++) {
                list.get(i).accept(union);
            }
        }
        unionLoadCallbackMap.remove(union.getUnionID());
    }

    /**
     * 数据库中没有找到联盟后处理之前增加的回调逻辑
     *
     * @param unionID
     * @param errorMsg
     */
    private void onLoadUnionFromDBFail(long unionID, String errorMsg) {
        CenterLoggerTool.centerLogger.error("onLoadUnionFromDBFail unionID={}, errorMsg={}", unionID, errorMsg);
        //没有找到联盟后处理之前增加的回调逻辑
        ObjectList<Consumer> list = unionLoadCallbackMap.get(unionID);
        if (list != null) {
            for (int i = 0, len = list.size(); i < len; i++) {
                list.get(i).accept(null);
            }
            unionLoadCallbackMap.remove(unionID);
        }
    }

    @Override
    public void onShutDown() {
        saveAllUnions();
    }

    /**
     * 存储所有的联盟
     */
    private void saveAllUnions() {
        if (unionMap.isEmpty()) {
            return;
        }
        long now = SystemTimeTool.getMillTime();
        List<Tuple> list = new ArrayList<>();

        LongList unionIDList = new LongList();

        unionMap.foreachMutable((k, v) -> {
            if (v.isChange()) {
                list.add(v.getSaveData());
                unionIDList.add(v.getUnionID());
            }

            //只有没有修改才会触发过期移除
            if (!v.isChange() && v.getExpireTime() > 0 && now > v.getExpireTime()) {
                removeUnion(v.getUnionID(), false);
            }
        });

        //批量落地
        if (list.size() > 0) {

            MySQLUtils.sendBatch(centerUnionWorld.getCenterDbConnectionPool(), "UPDATE `union` SET `unionData`=?, `saveTime`=? WHERE unionID=?;", list, centerUnionWorld.getIndex(), (isSuccess, rows, errorMsg) -> {
                if (isSuccess) {
                    CenterLoggerTool.centerLogger.info("mysql saveAllUnions success unionIDList={}", unionIDList);
                    for (int i = 0; i < unionIDList.size(); i++) {
                        CenterUnion centerUnion = unionMap.get(unionIDList.get(i));
                        centerUnion.setChange(false);
                    }
                } else {
                    CenterLoggerTool.centerLogger.error("mysql saveAllUnions fail unionIDList={} errorMsg={}", unionIDList, errorMsg);
                }
            });
        }
    }
}
