package com.highfly029.center.module.centerActivity;

import com.highfly029.common.data.activity.ActivityTimeData;
import com.highfly029.common.template.activity.ActivityDriveTypeConst;
import com.highfly029.common.template.activity.ActivityTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName CenterActivityConfig
 * @Description CenterActivityConfig
 * @Author liyunpeng
 **/
public class CenterActivityConfig extends BaseGlobalConfig {
    private final IntObjectMap<ActivityTemplate> activityTemplateMap = new IntObjectMap<>(ActivityTemplate[]::new);

    private final IntObjectMap<ActivityTimeData> centerActivityTemplateDataMap = new IntObjectMap<>(ActivityTimeData[]::new);

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(ActivityTemplate.class);
        ActivityTemplate activityTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            activityTemplate = (ActivityTemplate) list.get(i);
            if (activityTemplate.getActivityDriveType() == ActivityDriveTypeConst.Center) {
                ActivityTimeData activityTimeData = new ActivityTimeData(activityTemplate);
                activityTemplateMap.put(activityTemplate.getId(), activityTemplate);
                centerActivityTemplateDataMap.put(activityTemplate.getId(), activityTimeData);
            }
        }
    }

    /**
     * 获取活动模板类
     *
     * @param id
     * @return
     */
    public ActivityTemplate getTemplate(int id) {
        return activityTemplateMap.get(id);
    }

    public IntObjectMap<ActivityTemplate> getActivityTemplateMap() {
        return activityTemplateMap;
    }

    public IntObjectMap<ActivityTimeData> getCenterActivityTemplateDataMap() {
        return centerActivityTemplateDataMap;
    }

    @Override
    public void check(boolean isHotfix) {

    }
}
