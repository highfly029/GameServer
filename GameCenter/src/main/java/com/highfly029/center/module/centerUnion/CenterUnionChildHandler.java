package com.highfly029.center.module.centerUnion;

import java.util.function.Consumer;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.center.module.BaseServerChildHandler;
import com.highfly029.center.tool.CenterLoggerTool;
import com.highfly029.center.world.CenterUnionWorld;
import com.highfly029.common.data.base.Location;
import com.highfly029.common.data.union.UnionMember;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.common.template.infoCode.InfoCodeConst;
import com.highfly029.common.template.union.UnionHandleJoinResultConst;
import com.highfly029.common.template.union.UnionHandleJoinTypeConst;
import com.highfly029.common.template.union.UnionPermissionConst;
import com.highfly029.common.template.union.UnionPositionConst;
import com.highfly029.common.template.union.UnionUpdateTypeConst;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.session.Session;
import com.highfly029.core.world.ChildWorld;

import io.vertx.core.buffer.Buffer;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName CenterUnionChildHandler
 * @Description 中心服联盟child协议处理
 * @Author liyunpeng
 **/
public class CenterUnionChildHandler extends BaseServerChildHandler {
    private CenterUnionWorld centerUnionWorld;

    @Override
    public void registerChildProtocol(ChildWorld childWorld) throws Exception {
        if (!(childWorld instanceof CenterUnionWorld)) {
            return;
        }

        centerUnionWorld = (CenterUnionWorld) childWorld;

        childWorld.registerServerChildProtocol(PtCode.Logic2CenterCreateUnion, this::createUnion);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterPullPlayerUnionInfo, this::pullPlayerUnionInfo);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterMemberLogout, this::memberLogout);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterQuitUnion, this::quitUnion);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterDisbandUnion, this::disbandUnion);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterApplyJoinUnion, this::applyJoinUnion);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterInviteJoinUnion, this::inviteJoinUnion);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterHandleJoinUnion, this::handleJoinUnion);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterKickOutUnionMember, this::kickOutMember);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterJoinUnionDoubleCheckResult, this::onJoinUnionDoubleCheckResult);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterHandleInviteJoinUnion, this::handleBeInviteJoinUnion);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterChangeLeader, this::changeLeader);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterModifyUnionName, this::modifyUnionName);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterModifyUnionSetting, this::modifyUnionSetting);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterModifyUnionMemberPosition, this::modifyUnionMemberPosition);
        childWorld.registerServerChildProtocol(PtCode.Logic2CenterChangePermission, this::changePermission);
    }

    /**
     * 处理联盟逻辑，优先查找内存中的联盟数据
     * 如果内存中没有数据，则从数据库中加载后再执行逻辑
     * 如果数据库中没有数据，则回调中的union=null 注意处理
     *
     * @param unionID
     * @param callback
     */
    private void handleUnionWithCallback(long unionID, Consumer<CenterUnion> callback) {
        CenterUnionManager centerUnionManager = centerUnionWorld.getCenterUnionManager();
        CenterUnion union = centerUnionManager.getUnion(unionID);
        if (union != null) {
            callback.accept(union);
        } else {
            centerUnionManager.addLoadUnionCallback(unionID, callback);

            //从数据库里加载联盟数据
            centerUnionManager.loadUnionFromDB(unionID);
        }
    }

    /**
     * 创建联盟
     */
    private void createUnion(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterCreateUnion req = (PbUnion.Logic2CenterCreateUnion) message;
        long createPlayerID = req.getCreatePlayerID();
        String name = req.getName();
        CenterLoggerTool.centerLogger.info("createUnion createPlayerID={}, name={}", createPlayerID, name);
        CenterUnionManager centerUnionManager = centerUnionWorld.getCenterUnionManager();
        long unionID = centerUnionManager.generateUnionID();

        //先提前创建数据
        CenterUnion union = centerUnionManager.create();
        union.initNewUnion(unionID, name, createPlayerID, PbCommonUtils.locationPb2Obj(req.getLocation()));

        Buffer buffer = Buffer.buffer(union.save().toByteArray());
        Tuple tuple = Tuple.of(unionID, name, createPlayerID, buffer);
        MySQLUtils.sendChild(centerUnionWorld.getCenterDbConnectionPool(), "INSERT INTO `union`(unionID, unionName, createPlayerID, unionData, createTime) VALUE(?, ?, ?, ?, now());", tuple, centerUnionWorld.getIndex(), (isSuccess, rows, errorMsg) -> {
            if (isSuccess) {
                CenterLoggerTool.centerLogger.info("createUnion success createPlayerID={}, name={}, unionID={}", createPlayerID, name, unionID);

                //创建成功后加载到内存
                centerUnionManager.addUnion(union, true);
                //通知
                union.onCreateUnion();
                UnionMember unionMember = union.getUnionMember(createPlayerID);
                union.onAddUnionMember(unionMember);

                PbUnion.Center2LogicCreateUnion.Builder builder = PbUnion.Center2LogicCreateUnion.newBuilder();
                builder.setResult(true);
                builder.setCreatePlayerID(createPlayerID);
                builder.setUnionID(unionID);
                builder.setName(name);
                builder.setUnionData(union.save());
                session.sendMsgS2S(PtCode.Center2LogicCreateUnion, builder.build());
            } else {
                CenterLoggerTool.centerLogger.info("createUnion fail createPlayerID={}, name={}, unionID={}, msg={}", createPlayerID, name, unionID, errorMsg);
                PbUnion.Center2LogicCreateUnion.Builder builder = PbUnion.Center2LogicCreateUnion.newBuilder();
                builder.setResult(false);
                builder.setCreatePlayerID(createPlayerID);
                session.sendMsgS2S(PtCode.Center2LogicCreateUnion, builder.build());
            }
        });
    }

    /**
     * 拉取玩家联盟信息
     */
    private void pullPlayerUnionInfo(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterPullPlayerUnionInfo req = (PbUnion.Logic2CenterPullPlayerUnionInfo) message;
        long playerID = req.getPlayerID();
        long unionID = req.getUnionID();

        handleUnionWithCallback(unionID, union -> {
            PbUnion.Center2LogicSyncPlayerUnionInfo.Builder builder = PbUnion.Center2LogicSyncPlayerUnionInfo.newBuilder();
            builder.setPlayerID(playerID);
            builder.setIsPull(true);
            if (union != null) {
                //玩家获取联盟信息时发现该联盟没有此玩家成员、可能是玩家离线期间被踢
                UnionMember unionMember = union.getUnionMember(playerID);
                if (unionMember == null) {
                    builder.setUnionID(0);
                    CenterLoggerTool.centerLogger.warn("pullPlayerUnionInfo unionID={} unionMember not exist playerID={}", unionID, playerID);
                } else {
                    Location location = PbCommonUtils.locationPb2Obj(req.getLocation());
                    location.setOnline();
                    unionMember.setLocation(location);
                    builder.setUnionID(union.getUnionID());
                    builder.setName(union.getUnionName());
                    builder.setUnionData(union.save());
                    CenterLoggerTool.centerLogger.info("pullPlayerUnionInfo ok playerID={}, unionID={}", playerID, union.getUnionID());
                }
            } else {
                builder.setUnionID(0);
                //可能是玩家离线期间联盟解散
                CenterLoggerTool.centerLogger.error("pullPlayerUnionInfo unionID={} is null", unionID);
            }
            session.sendMsgS2S(PtCode.Center2LogicSyncPlayerUnionInfo, builder.build());
        });
    }

    /**
     * 玩家登出
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void memberLogout(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterMemberLogout req = (PbUnion.Logic2CenterMemberLogout) message;
        long unionID = req.getUnionID();
        long playerID = req.getPlayerID();
        CenterUnion union = centerUnionWorld.getCenterUnionManager().getUnion(unionID);
        boolean result = false;
        do {
            if (union == null) {
                CenterLoggerTool.centerLogger.error("memberLogout not exist union unionID={}, playerID={}", unionID, playerID);
                break;
            }
            UnionMember unionMember = union.getUnionMember(playerID);
            if (unionMember == null) {
                CenterLoggerTool.centerLogger.error("memberLogout not exist memberID unionID={}, memberID={}", unionID, playerID);
                break;
            }
            unionMember.getLocation().setState(Location.LOGOUT);
            result = true;
        } while (false);
        if (result == false) {
            sendInfoCode2Player(session, playerID, InfoCodeConst.UnknownError);
        }
    }

    /**
     * 主动退出联盟
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void quitUnion(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterQuitUnion req = (PbUnion.Logic2CenterQuitUnion) message;
        long playerID = req.getPlayerID();
        long unionID = req.getUnionID();
        CenterUnion union = centerUnionWorld.getCenterUnionManager().getUnion(unionID);

        if (union == null) {
            CenterLoggerTool.centerLogger.error("quitUnion not exist union unionID={}, playerID={}", unionID, playerID);
            return;
        }
        boolean result = union.quitUnion(playerID);
        if (!result) {
            sendInfoCode2Player(session, playerID, InfoCodeConst.UnknownError);
        }
    }

    /**
     * 解散联盟
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void disbandUnion(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterDisbandUnion req = (PbUnion.Logic2CenterDisbandUnion) message;
        long playerID = req.getPlayerID();
        long unionID = req.getUnionID();
        CenterUnion union = centerUnionWorld.getCenterUnionManager().getUnion(unionID);
        boolean result = false;
        do {
            if (union == null) {
                CenterLoggerTool.centerLogger.error("disbandUnion not exist union unionID={}, playerID={}", unionID, playerID);
                break;
            }
            UnionMember unionMember = union.getUnionMember(playerID);
            if (unionMember == null) {
                CenterLoggerTool.centerLogger.error("disbandUnion not exist member unionID={}, playerID={}", unionID, playerID);
                break;
            }
            if (unionMember.getPosition() != UnionPositionConst.Leader) {
                CenterLoggerTool.centerLogger.error("disbandUnion only leader can do it unionID={}, playerID={}", unionID, playerID);
                break;
            }
            Tuple tuple = Tuple.of(unionID);
            MySQLUtils.sendChild(centerUnionWorld.getCenterDbConnectionPool(), "DELETE FROM `union` WHERE `unionID`=?;", tuple, centerUnionWorld.getIndex(), (isSuccess, rows, errorMsg) -> {
                if (isSuccess) {
                    union.disband(unionMember);
                    CenterLoggerTool.centerLogger.info("disband delete union success playerID={}, unionID={}", playerID, unionID);
                } else {
                    CenterLoggerTool.centerLogger.error("disband delete union fail playerID={}, unionID={}, errorMsg={}", playerID, unionID, errorMsg);
                }
            });
            result = true;
        } while (false);
        //失败了返回错误信息，成功了走统一推送，不需要单独发送
        if (result == false) {
            sendInfoCode2Player(session, playerID, InfoCodeConst.UnknownError);
        }
    }

    /**
     * 申请加入联盟
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void applyJoinUnion(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterApplyJoinUnion req = (PbUnion.Logic2CenterApplyJoinUnion) message;
        long unionID = req.getUnionID();

        Consumer<CenterUnion> callback = (union) -> {
            long playerID = req.getPlayerID();
            if (union == null) {
                CenterLoggerTool.centerLogger.warn("applyJoinUnion cant find unionID={}, playerID={}", unionID, playerID);
                return;
            }
            boolean result = union.applyJoinUnion(req.getLogicServerID(), playerID, req.getLevel());
            if (!result) {
                sendInfoCode2Player(session, playerID, InfoCodeConst.UnknownError, String.valueOf(unionID));
            }
        };

        handleUnionWithCallback(unionID, callback);
    }

    /**
     * 邀请加入联盟
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void inviteJoinUnion(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterInviteJoinUnion req = (PbUnion.Logic2CenterInviteJoinUnion) message;
        long unionID = req.getUnionID();
        long playerID = req.getPlayerID();
        long targetPlayerID = req.getTargetPlayerID();
        boolean result = false;
        do {
            CenterUnion union = centerUnionWorld.getCenterUnionManager().getUnion(unionID);
            if (union == null) {
                CenterLoggerTool.centerLogger.error("inviteJoinUnion playerID={} cant find unionID={}, targetID={}", playerID, unionID, targetPlayerID);
                break;
            }
            result = union.inviteJoinUnion(playerID, req.getLogicServerID(), targetPlayerID);
        } while (false);
        if (result == false) {
            sendInfoCode2Player(session, playerID, InfoCodeConst.UnknownError);
        }
    }

    /**
     * 处理入盟消息 被处理的玩家必须在线
     * <p>
     * ========================
     * 另外一种方案 处理申请的玩家时、先去找到玩家对应所在的服务器、然后发送消息去check玩家是否确实没有加入联盟
     * 然后再在玩家身上记录prepareJoinUnionIDMark和expireTime防止并发操作、返回到此服务器后再真正的添加成员
     * or 如果是申请的时候能自动加入 也需要去check然后加入
     * or 玩家处理邀请时也需要先去check
     *
     * @param session
     * @param ptCode
     * @throws InvalidProtocolBufferException
     */
    private void handleJoinUnion(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterHandleJoinUnion req = (PbUnion.Logic2CenterHandleJoinUnion) message;
        long unionID = req.getUnionID();
        long playerID = req.getPlayerID();
        boolean result = false;
        do {
            CenterUnion union = centerUnionWorld.getCenterUnionManager().getUnion(unionID);
            if (union == null) {
                CenterLoggerTool.centerLogger.error("handleJoinUnion not existUnionID={}, playerID={}", unionID, playerID);
                break;
            }

            if (union.isMaxMemberNum()) {
                CenterLoggerTool.centerLogger.error("handleJoinUnion maxMemberNum UnionID={}, playerID={}", unionID, playerID);
                break;
            }

            int handleType = req.getHandleType();
            int handleResult = req.getHandleResult();
            if (handleResult <= 0 || handleResult > UnionHandleJoinResultConst.count) {
                CenterLoggerTool.centerLogger.error("handleJoinUnion invalid handleResult={}, playerID={}, unionID={}", handleResult, playerID, unionID);
                break;
            }
            long targetPlayerID = req.getTargetPlayerID();
            if (handleType == UnionHandleJoinTypeConst.Apply) {
                result = union.handleApplyJoinList(playerID, targetPlayerID, handleResult, handleType);
            } else {
                CenterLoggerTool.centerLogger.error("handleJoinUnion invalid handleType={}, playerID={}, unionID={}", handleType, playerID, unionID);
                break;
            }
        } while (false);
        if (result == false) {
            sendInfoCode2Player(session, playerID, InfoCodeConst.UnknownError);
        }
    }

    /**
     * 加入联盟检测结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void onJoinUnionDoubleCheckResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterJoinUnionDoubleCheckResult req = (PbUnion.Logic2CenterJoinUnionDoubleCheckResult) message;
        long unionID = req.getUnionID();
        long operatorPlayerID = req.getOperatorPlayerID();
        long targetPlayerID = req.getTargetPlayerID();
        int type = req.getType();
        boolean result = req.getResult();

        if (result == false) {
            CenterLoggerTool.centerLogger.error("onJoinUnionDoubleCheckResult fail operatorPlayerID={}, targetPlayerID={}, union={}, type={}",
                    operatorPlayerID, targetPlayerID, unionID, type);
            return;
        }

        if (!req.hasLocation()) {
            CenterLoggerTool.centerLogger.error("onJoinUnionDoubleCheckResult not exist location UnionID={}, operatorPlayerID={}, targetPlayerID={} ", unionID, operatorPlayerID, targetPlayerID);
            return;
        }
        handleUnionWithCallback(unionID, union -> {
            if (union == null) {
                CenterLoggerTool.centerLogger.error("onJoinUnionDoubleCheckResult not exist UnionID={}, operatorPlayerID={}, targetPlayerID={} ", unionID, operatorPlayerID, targetPlayerID);
                return;
            }
            union.onJoinUnionDoubleCheckSuccess(operatorPlayerID, targetPlayerID, type, PbCommonUtils.locationPb2Obj(req.getLocation()));
        });
    }

    /**
     * 玩家响应邀请加入联盟
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void handleBeInviteJoinUnion(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterHandleInviteJoinUnion req = (PbUnion.Logic2CenterHandleInviteJoinUnion) message;
        long targetUnionID = req.getUnionID();

        Consumer<CenterUnion> callback = union -> {
            long playerID = req.getPlayerID();
            if (union == null) {
                CenterLoggerTool.centerLogger.warn("handleBeInviteJoinUnion cant find unionID={}, playerID={}", targetUnionID, playerID);
                return;
            }
            boolean result = union.handleBeInviteJoinUnion(playerID, req.getHandleResultType(), PbCommonUtils.locationPb2Obj(req.getLocation()));

            PbUnion.Center2LogicHandleInviteJoinUnion.Builder builder = PbUnion.Center2LogicHandleInviteJoinUnion.newBuilder();
            builder.setPlayerID(playerID);
            builder.setUnionID(targetUnionID);
            builder.setResult(result);
            builder.setHandleResultType(req.getHandleResultType());
            session.sendMsgS2S(PtCode.Center2LogicHandleInviteJoinUnion, builder.build());
        };

        handleUnionWithCallback(targetUnionID, callback);
    }

    /**
     * 踢出成员
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void kickOutMember(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterKickOutUnionMember req = (PbUnion.Logic2CenterKickOutUnionMember) message;
        long playerID = req.getPlayerID();
        long unionID = req.getUnionID();
        long targetPlayerID = req.getTargetPlayerID();
        boolean result = false;
        do {
            CenterUnion union = centerUnionWorld.getCenterUnionManager().getUnion(unionID);
            if (union == null) {
                CenterLoggerTool.centerLogger.error("kickOutMember not exist union playerID={}, unionID={}", playerID, unionID);
                break;
            }

            result = union.kickOut(playerID, targetPlayerID);
        } while (false);
        if (result == false) {
            sendInfoCode2Player(session, playerID, InfoCodeConst.UnknownError);
        }
    }

    /**
     * 更换盟主
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void changeLeader(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterChangeLeader req = (PbUnion.Logic2CenterChangeLeader) message;
        long playerID = req.getPlayerID();
        long unionID = req.getUnionID();
        long targetPlayerID = req.getTargetPlayerID();
        boolean result = false;
        do {
            CenterUnion union = centerUnionWorld.getCenterUnionManager().getUnion(unionID);
            if (union == null) {
                CenterLoggerTool.centerLogger.error("changeLeader not exist union playerID={}, unionID={}", playerID, unionID);
                break;
            }
            result = union.changeLeader(playerID, targetPlayerID);
        } while (false);
        if (result == false) {
            sendInfoCode2Player(session, playerID, InfoCodeConst.UnknownError);
        }
    }

    /**
     * 修改联盟名字
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void modifyUnionName(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterModifyUnionName req = (PbUnion.Logic2CenterModifyUnionName) message;
        long unionID = req.getUnionID();
        long playerID = req.getPlayerID();
        String name = req.getName();

        CenterUnion union = centerUnionWorld.getCenterUnionManager().getUnion(unionID);
        if (union == null) {
            CenterLoggerTool.centerLogger.error("modifyUnionName not exist union playerID={}, unionID={}", playerID, unionID);
            return;
        }
        if (name.equals(union.getUnionName())) {
            CenterLoggerTool.centerLogger.error("modifyUnionName same name playerID={}, unionID={}", playerID, unionID);
            return;
        }
        //TODO check name valid

        union.modifyUnionName(playerID, name, centerUnionWorld.getIndex(), (ret) -> {
            PbUnion.Center2LogicModifyUnionName.Builder builder = PbUnion.Center2LogicModifyUnionName.newBuilder();
            builder.setResult(ret);
            builder.setPlayerID(playerID);
            session.sendMsgS2S(PtCode.Center2LogicModifyUnionName, builder.build());
        });

    }

    /**
     * 修改联盟设置
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void modifyUnionSetting(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterModifyUnionSetting req = (PbUnion.Logic2CenterModifyUnionSetting) message;
        long unionID = req.getUnionID();
        long playerID = req.getPlayerID();
        boolean result = false;
        do {
            CenterUnion union = centerUnionWorld.getCenterUnionManager().getUnion(unionID);
            if (union == null) {
                CenterLoggerTool.centerLogger.error("modifyUnionSetting not exist union playerID={}, unionID={}", playerID, unionID);
                break;
            }
            UnionMember unionMember = union.getUnionMember(playerID);
            if (unionMember == null) {
                CenterLoggerTool.centerLogger.error("modifyUnionSetting not exist member playerID={}, unionID={}", playerID, unionID);
                break;
            }

            if (!union.checkPermission(unionMember.getPosition(), UnionPermissionConst.ModifySetting)) {
                CenterLoggerTool.centerLogger.error("modifyUnionSetting no permission playerID={}, unionID={}", playerID, unionID);
                break;
            }
            PbUnion.PbUnionSettingData settingData = req.getSetting();
            switch (req.getSettingType()) {
                case UnionUpdateTypeConst.ModifyNotice -> {
                    union.modifyUnionSettingNotice(settingData.getNotice());
                }
                case UnionUpdateTypeConst.ModifyJoinLimitLevel -> {
                    union.modifyUnionSettingJoinLimitLevel(settingData.getJoinLimitLv());
                }
                case UnionUpdateTypeConst.ModifyJoinNeedApply -> {
                    union.modifyUnionSettingJoinNeedApply(settingData.getJoinNeedApply());
                }
                default -> {
                    CenterLoggerTool.centerLogger.error("modifyUnionSetting invalid type={}, playerID={}, unionID={}", req.getSettingType(), playerID, unionID);
                }
            }
            result = true;
        } while (false);
        if (!result) {
            sendInfoCode2Player(session, playerID, InfoCodeConst.UnknownError);
        }
    }

    /**
     * 修改联盟成员职位
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void modifyUnionMemberPosition(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterModifyUnionMemberPosition req = (PbUnion.Logic2CenterModifyUnionMemberPosition) message;
        long unionID = req.getUnionID();
        long playerID = req.getPlayerID();
        boolean result = false;
        do {
            CenterUnion union = centerUnionWorld.getCenterUnionManager().getUnion(unionID);
            if (union == null) {
                CenterLoggerTool.centerLogger.error("modifyUnionMemberPosition not exist union playerID={}, unionID={}", playerID, unionID);
                break;
            }
            result = union.modifyUnionMemberPosition(playerID, req.getTargetPlayerID(), req.getPosition());
        } while (false);
        if (result == false) {
            sendInfoCode2Player(session, playerID, InfoCodeConst.UnknownError);
        }
    }

    /**
     * 修改权限
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void changePermission(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Logic2CenterChangePermission req = (PbUnion.Logic2CenterChangePermission) message;
        long playerID = req.getPlayerID();
        long unionID = req.getUnionID();
        boolean result = false;
        do {
            CenterUnion union = centerUnionWorld.getCenterUnionManager().getUnion(unionID);
            if (union == null) {
                CenterLoggerTool.centerLogger.error("changePermission not exist union playerID={}, unionID={}", playerID, unionID);
                break;
            }
            result = union.changePermission(playerID, req.getPosition(), req.getPermission());
        } while (false);

        if (result == false) {
            sendInfoCode2Player(session, playerID, InfoCodeConst.UnknownError);
        }
    }
}
