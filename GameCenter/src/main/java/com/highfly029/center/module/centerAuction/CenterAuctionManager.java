package com.highfly029.center.module.centerAuction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.google.protobuf.ByteString;
import com.highfly029.center.tool.CenterLoggerTool;
import com.highfly029.center.world.CenterAuctionWorld;
import com.highfly029.common.data.auction.AuctionItemData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbAuction;
import com.highfly029.common.template.auction.AuctionConst;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.manager.BaseManager;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.utils.SnowFlake;

import io.vertx.core.buffer.Buffer;
import io.vertx.mysqlclient.MySQLClient;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName CenterAuctionManager
 * @Description 拍卖行管理类
 * @Author liyunpeng
 **/
public class CenterAuctionManager extends BaseManager {
    private CenterAuctionWorld centerAuctionWorld;

    /**
     * 拍卖行数据 key=auctionType
     */
    private HashMap<Integer, HashMap<Long, AuctionItemData>> auctionAllMap = new HashMap<>();

    public CenterAuctionManager(CenterAuctionWorld world) {
        super(world);
        this.centerAuctionWorld = world;
    }

    /**
     * 获取指定拍卖类型的所有拍卖数据
     *
     * @param auctionType
     * @return
     */
    private HashMap<Long, AuctionItemData> getAuctionItemMap(int auctionType) {
        return auctionAllMap.get(auctionType);
    }

    /**
     * 从数据库加载所有拍卖行上架物品
     *
     * @param list 拍卖类型
     */
    public void loadAllAuctionItemFromDb(List<Integer> list) {
        for (Integer auctionType : list) {
            HashMap<Long, AuctionItemData> auctionItemMap = new HashMap<>();
            auctionAllMap.put(auctionType, auctionItemMap);
            String sql = "SELECT * FROM auction WHERE auction_type=?;";
            Tuple tuple = Tuple.of(auctionType);
            long startTime = SystemTimeTool.getMillTime();
            MySQLUtils.sendChild(centerAuctionWorld.getCenterDbConnectionPool(), sql, tuple, centerAuctionWorld.getIndex(), (isSuccess, successRows, errorMsg) -> {
                if (isSuccess) {
                    for (Row row : successRows) {
                        AuctionItemData auctionItemData = new AuctionItemData();
                        long id = row.getLong("id");
                        auctionItemData.setId(id);
                        long playerID = row.getLong("player_id");
                        auctionItemData.setPlayerID(playerID);
                        int itemID = row.getInteger("item_id");
                        auctionItemData.setItemID(itemID);
                        int itemNum = row.getInteger("item_num");
                        auctionItemData.setItemNum(itemNum);
                        int price = row.getInteger("price");
                        auctionItemData.setPrice(price);
                        long shelfTime = row.getLong("shelf_time");
                        auctionItemData.setShelfTime(shelfTime);
                        Buffer itemDataBuffer = row.getBuffer("item_data");
                        if (itemDataBuffer != null && itemDataBuffer.length() > 0) {
                            ByteString itemData = ByteString.copyFrom(itemDataBuffer.getBytes());
                            auctionItemData.setItemData(itemData);
                        }
                        auctionItemMap.put(id, auctionItemData);
                    }
                    CenterLoggerTool.centerLogger.info("loadAllAuctionItemFromDb success auctionType={}, size={}, costTime={}",
                            auctionType, successRows.size(), SystemTimeTool.getMillTime() - startTime);
                } else {
                    CenterLoggerTool.centerLogger.error("loadAllAuctionItemFromDb fail errorMsg={}", errorMsg);
                }
            });
        }

    }

    /**
     * 拍卖行查询
     *
     * @param session
     * @param auctionType
     * @param playerID
     */
    public void auctionSearch(Session session, int auctionType, long playerID) {
        HashMap<Long, AuctionItemData> auctionItemMap = getAuctionItemMap(auctionType);
        PbAuction.Center2LogicAuctionSearch.Builder builder = PbAuction.Center2LogicAuctionSearch.newBuilder();
        builder.setAuctionType(auctionType);
        builder.setPlayerID(playerID);
        auctionItemMap.forEach((id, data) -> {
            PbAuction.PbAuctionItemData.Builder auctionItemBuilder = PbAuction.PbAuctionItemData.newBuilder();
            auctionItemBuilder.setId(id);
            auctionItemBuilder.setItemID(data.getItemID());
            auctionItemBuilder.setItemNum(data.getItemNum());
            auctionItemBuilder.setPrice(data.getPrice());
            auctionItemBuilder.setShelfTime(data.getShelfTime());
            if (data.getItemData() != null) {
                auctionItemBuilder.setItemByteData(data.getItemData());
            }
            builder.addAuctionItemList(auctionItemBuilder.build());
        });
        session.sendMsgS2S(PtCode.Center2LogicAuctionSearch, builder.build());
    }

    /**
     * 拍卖行上架物品
     *
     * @param auctionType
     * @param playerID
     * @param auctionItemData
     */
    public void auctionShelfItem(Session session, int auctionType, long playerID, AuctionItemData auctionItemData) {
        String sql = "INSERT INTO `auction` (`player_id`, `auction_type`, `item_id`, `item_num`, `price`, `item_data`, `bid_data`, `shelf_time`) \n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?);";
        List<Object> dataList = new ArrayList<>();
        dataList.add(playerID);
        dataList.add(auctionType);
        dataList.add(auctionItemData.getItemID());
        dataList.add(auctionItemData.getItemNum());
        dataList.add(auctionItemData.getPrice());
        Buffer itemDataBuff = Buffer.buffer(auctionItemData.getItemData().toByteArray());
        dataList.add(itemDataBuff);
        dataList.add(null);
        dataList.add(auctionItemData.getShelfTime());
        Tuple tuple = Tuple.wrap(dataList);

        PbAuction.Center2LogicAuctionShelfItem.Builder builder = PbAuction.Center2LogicAuctionShelfItem.newBuilder();
        builder.setPlayerID(playerID);
        builder.setAuctionType(auctionType);

        MySQLUtils.sendChild(centerAuctionWorld.getCenterDbConnectionPool(), sql, tuple, centerAuctionWorld.getIndex(), (isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                long id = successRows.property(MySQLClient.LAST_INSERTED_ID);
                auctionItemData.setId(id);
                auctionItemData.setPlayerID(playerID);
                HashMap<Long, AuctionItemData> auctionItemMap = getAuctionItemMap(auctionType);
                auctionItemMap.put(id, auctionItemData);

                CenterLoggerTool.centerLogger.info("auctionShelfItem insert success {}", auctionItemData);
            } else {
                CenterLoggerTool.centerLogger.error("auctionShelfItem insert fail auctionType={},playerID={},itemID={},itemNum={},price={}, msg={}",
                        auctionType, playerID, auctionItemData.getItemID(), auctionItemData.getItemNum(), auctionItemData.getPrice(), errorMsg);
            }
            builder.setAuctionItemData(PbCommonUtils.auctionItemDataObj2Pb(auctionItemData));
            builder.setResult(isSuccess);
            session.sendMsgS2S(PtCode.Center2LogicAuctionShelfItem, builder.build());
        });
    }

    /**
     * 拍卖行购买物品
     *
     * @param session
     * @param auctionType
     * @param playerID
     */
    public void auctionBuy(Session session, int auctionType, long playerID, AuctionItemData auctionItemData) {
        HashMap<Long, AuctionItemData> auctionItemMap = getAuctionItemMap(auctionType);
        AuctionItemData sellAuction = auctionItemMap.get(auctionItemData.getId());
        if (sellAuction == null) {
            CenterLoggerTool.centerLogger.warn("auctionBuy not exist auctionType={}, playerID={}, data={}", auctionType, playerID, auctionItemData);
            return;
        }
        String sql = "DELETE FROM auction WHERE id=" + auctionItemData.getId();
        MySQLUtils.sendChild(centerAuctionWorld.getCenterDbConnectionPool(), sql, null, centerAuctionWorld.getIndex(), (isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                auctionItemMap.remove(auctionItemData.getId());
                long sellPlayerID = sellAuction.getPlayerID();
                int logicSourceServerID = SnowFlake.getMixID(sellPlayerID);
                PbAuction.Center2LogicAuctionBuyToSeller.Builder builder = PbAuction.Center2LogicAuctionBuyToSeller.newBuilder();
                builder.setAuctionItemData(PbCommonUtils.auctionItemDataObj2Pb(auctionItemData));
                builder.setBuyPlayerID(playerID);
                builder.setSellerPlayerID(sellPlayerID);
                centerAuctionWorld.sendMsg2LogicServer(logicSourceServerID, PtCode.Center2LogicAuctionBuyToSeller, builder.build());
            } else {
                CenterLoggerTool.centerLogger.error("auctionBuy delete fail auctionType={},playerID={},data={}, msg={}",
                        auctionType, playerID, auctionItemData, errorMsg);
            }
            PbAuction.Center2LogicAuctionBuy.Builder builder = PbAuction.Center2LogicAuctionBuy.newBuilder();
            builder.setAuctionItemData(PbCommonUtils.auctionItemDataObj2Pb(auctionItemData));
            builder.setAuctionType(auctionType);
            builder.setPlayerID(playerID);
            builder.setResult(isSuccess);
            CenterLoggerTool.centerLogger.info("auctionBuy result={}", isSuccess);
            session.sendMsgS2S(PtCode.Center2LogicAuctionBuy, builder.build());
        });

    }

    /**
     * 拍卖行取消上架
     *
     * @param session
     * @param auctionType
     * @param auctionID
     * @param playerID
     */
    public void auctionCancelShelfItem(Session session, int auctionType, long auctionID, long playerID) {
        HashMap<Long, AuctionItemData> auctionItemMap = getAuctionItemMap(auctionType);
        AuctionItemData sellAuction = auctionItemMap.get(auctionID);
        if (sellAuction == null) {
            CenterLoggerTool.centerLogger.warn("auctionCancelShelfItem not exist auctionType={}, playerID={}, auctionID={}", auctionType, playerID, auctionID);
            return;
        }
        String sql = "DELETE FROM auction WHERE id=" + auctionID;
        MySQLUtils.sendChild(centerAuctionWorld.getCenterDbConnectionPool(), sql, null, centerAuctionWorld.getIndex(), (isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                auctionItemMap.remove(auctionID);
            } else {
                CenterLoggerTool.centerLogger.warn("auctionCancelShelfItem delete fail auctionType={},playerID={},auctionID={}, msg={}",
                        auctionType, playerID, auctionID, errorMsg);
            }
            PbAuction.Center2LogicAuctionCancelShelfItem.Builder builder = PbAuction.Center2LogicAuctionCancelShelfItem.newBuilder();
            builder.setAuctionType(auctionType);
            builder.setAuctionItemData(PbCommonUtils.auctionItemDataObj2Pb(sellAuction));
            builder.setPlayerID(playerID);
            builder.setResult(isSuccess);
            CenterLoggerTool.centerLogger.info("auctionCancelShelfItem result={}", isSuccess);
            session.sendMsgS2S(PtCode.Center2LogicAuctionCancelShelfItem, builder.build());
        });
    }

    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {

    }

    @Override
    public void onMinute() {
        checkExpireTime();
    }

    /**
     * 检测上架物品过期时间
     */
    private void checkExpireTime() {
        long now = SystemTimeTool.getMillTime();
        auctionAllMap.forEach((auctionType, map) -> {
            for (Iterator<Map.Entry<Long, AuctionItemData>> it = map.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<Long, AuctionItemData> entry = it.next();
                AuctionItemData auctionItemData = entry.getValue();
                if (now >= auctionItemData.getShelfTime() + TimeUnit.MINUTES.toMillis(AuctionConst.ShelfMaxTime)) {
                    it.remove();
                    String sql = "DELETE FROM auction WHERE id=" + auctionItemData.getId();
                    MySQLUtils.sendChild(centerAuctionWorld.getCenterDbConnectionPool(), sql, null, centerAuctionWorld.getIndex(), (isSuccess, successRows, errorMsg) -> {
                        if (isSuccess) {
                            long playerID = auctionItemData.getPlayerID();
                            int logicSourceServerID = SnowFlake.getMixID(playerID);
                            PbAuction.Center2LogicAuctionExpireCancel.Builder builder = PbAuction.Center2LogicAuctionExpireCancel.newBuilder();
                            builder.setAuctionItemData(PbCommonUtils.auctionItemDataObj2Pb(auctionItemData));
                            builder.setSellerPlayerID(playerID);
                            centerAuctionWorld.sendMsg2LogicServer(logicSourceServerID, PtCode.Center2LogicAuctionExpireCancel, builder.build());
                        } else {
                            CenterLoggerTool.centerLogger.warn("auction checkExpireTime delete fail auctionType={},auctionData={}, msg={}",
                                    auctionType, auctionItemData, errorMsg);
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onShutDown() {

    }

}
