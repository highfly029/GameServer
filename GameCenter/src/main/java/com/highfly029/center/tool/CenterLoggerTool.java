package com.highfly029.center.tool;

import com.highfly029.core.tool.LoggerTool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @ClassName CenterLoggerTool
 * @Description CenterLoggerTool
 * @Author liyunpeng
 **/
public class CenterLoggerTool extends LoggerTool {
    /**
     * game日志
     */
    public static final Logger centerLogger = LoggerFactory.getLogger("center");
}
