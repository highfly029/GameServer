package com.highfly029.center.tool;

import java.util.concurrent.atomic.AtomicInteger;

import com.highfly029.center.module.centerActivity.CenterActivityConfig;
import com.highfly029.center.module.centerUnion.CenterUnionConfig;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.ObjObjMap;

/**
 * @ClassName CenterConfigTool
 * @Description CenterConfigTool
 * @Author liyunpeng
 **/
public class CenterConfigTool {
    /**
     * 热更新标记
     */
    private static AtomicInteger hotfixFlag = new AtomicInteger(-1);

    /**
     * 配置数据集合
     */
    private static ObjObjMap<Class, BaseGlobalConfig>[] configDataMaps = new ObjObjMap[2];

    private static int readIndex() {
        return hotfixFlag.get() % 2;
    }

    private static int writeIndex() {
        return (hotfixFlag.get() + 1) % 2;
    }

    /**
     * 热更新标记
     */
    public static void updateHotfixFlag() {
        hotfixFlag.incrementAndGet();
    }

    /**
     * 设置数据
     *
     * @param map
     */
    public static void setConfigData(ObjObjMap<Class, BaseGlobalConfig> map) {
        configDataMaps[writeIndex()] = map;
    }

    /***************************** 业务配置快捷方式 *****************************/
    public static CenterUnionConfig getCenterUnionConfig() {
        return (CenterUnionConfig) configDataMaps[readIndex()].get(CenterUnionConfig.class);
    }

    public static CenterActivityConfig getCenterActivityConfig() {
        return (CenterActivityConfig) configDataMaps[readIndex()].get(CenterActivityConfig.class);
    }
}
