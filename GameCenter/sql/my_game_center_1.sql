/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100803
 Source Host           : localhost:3306
 Source Schema         : my_game_center_1

 Target Server Type    : MySQL
 Target Server Version : 100803
 File Encoding         : 65001

 Date: 13/09/2022 18:41:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for union
-- ----------------------------
DROP TABLE IF EXISTS `union`;
CREATE TABLE `union` (
  `unionID` bigint(20) NOT NULL,
  `unionName` varchar(255) NOT NULL,
  `createPlayerID` bigint(20) DEFAULT NULL,
  `createTime` timestamp NULL DEFAULT NULL,
  `unionData` longblob DEFAULT NULL,
  `saveTime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`unionID`),
  UNIQUE KEY `uniqueName` (`unionName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

SET FOREIGN_KEY_CHECKS = 1;
