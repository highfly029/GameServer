package com.highfly029.parser.pb;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.googlecode.protobuf.format.JsonFormat;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.common.protocol.packet.PbUnion;

/**
 * generated by tools, don't modify!
 */
public class C2SChangePermissionParser implements IPacketParser {

    @Override
    public int getPtCode() {
        return 2059;
    }

    @Override
    public String getName() {
        return "C2SChangePermission";
    }

    @Override
    public boolean isParserHex() {
        return true;
    }

    @Override
    public String parser(ByteString byteString) throws InvalidProtocolBufferException {
        try {
            PbUnion.C2SChangePermission req = PbUnion.C2SChangePermission.parseFrom(byteString.toByteArray());
            JsonFormat jsonFormat = new JsonFormat();
            return jsonFormat.printToString(req);
        } catch (InvalidProtocolBufferException e) {
            throw e;
        }
    }
}
