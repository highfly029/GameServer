package com.highfly029.parser.pb;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.googlecode.protobuf.format.JsonFormat;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.common.protocol.packet.PbUnion;

/**
 * generated by tools, don't modify!
 */
public class S2CHandleJoinUnionParser implements IPacketParser {

    @Override
    public int getPtCode() {
        return 2036;
    }

    @Override
    public String getName() {
        return "S2CHandleJoinUnion";
    }

    @Override
    public boolean isParserHex() {
        return true;
    }

    @Override
    public String parser(ByteString byteString) throws InvalidProtocolBufferException {
        try {
            PbUnion.S2CHandleJoinUnion req = PbUnion.S2CHandleJoinUnion.parseFrom(byteString.toByteArray());
            JsonFormat jsonFormat = new JsonFormat();
            return jsonFormat.printToString(req);
        } catch (InvalidProtocolBufferException e) {
            throw e;
        }
    }
}
