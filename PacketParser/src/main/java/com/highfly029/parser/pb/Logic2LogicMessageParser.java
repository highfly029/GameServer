package com.highfly029.parser.pb;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.googlecode.protobuf.format.JsonFormat;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.common.protocol.packet.PbLoadBalance;

/**
 * generated by tools, don't modify!
 */
public class Logic2LogicMessageParser implements IPacketParser {

    @Override
    public int getPtCode() {
        return 20003;
    }

    @Override
    public String getName() {
        return "Logic2LogicMessage";
    }

    @Override
    public boolean isParserHex() {
        return true;
    }

    @Override
    public String parser(ByteString byteString) throws InvalidProtocolBufferException {
        try {
            PbLoadBalance.Logic2LogicMessage req = PbLoadBalance.Logic2LogicMessage.parseFrom(byteString.toByteArray());
            JsonFormat jsonFormat = new JsonFormat();
            return jsonFormat.printToString(req);
        } catch (InvalidProtocolBufferException e) {
            throw e;
        }
    }
}
