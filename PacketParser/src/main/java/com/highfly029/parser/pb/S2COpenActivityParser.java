package com.highfly029.parser.pb;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.googlecode.protobuf.format.JsonFormat;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.common.protocol.packet.PbActivity;

/**
 * generated by tools, don't modify!
 */
public class S2COpenActivityParser implements IPacketParser {

    @Override
    public int getPtCode() {
        return 3001;
    }

    @Override
    public String getName() {
        return "S2COpenActivity";
    }

    @Override
    public boolean isParserHex() {
        return true;
    }

    @Override
    public String parser(ByteString byteString) throws InvalidProtocolBufferException {
        try {
            PbActivity.S2COpenActivity req = PbActivity.S2COpenActivity.parseFrom(byteString.toByteArray());
            JsonFormat jsonFormat = new JsonFormat();
            return jsonFormat.printToString(req);
        } catch (InvalidProtocolBufferException e) {
            throw e;
        }
    }
}
