package com.highfly029.parser.pb;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.googlecode.protobuf.format.JsonFormat;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.common.protocol.packet.PbQuest;

/**
 * generated by tools, don't modify!
 */
public class C2SCommitQuestParser implements IPacketParser {

    @Override
    public int getPtCode() {
        return 1603;
    }

    @Override
    public String getName() {
        return "C2SCommitQuest";
    }

    @Override
    public boolean isParserHex() {
        return true;
    }

    @Override
    public String parser(ByteString byteString) throws InvalidProtocolBufferException {
        try {
            PbQuest.C2SCommitQuest req = PbQuest.C2SCommitQuest.parseFrom(byteString.toByteArray());
            JsonFormat jsonFormat = new JsonFormat();
            return jsonFormat.printToString(req);
        } catch (InvalidProtocolBufferException e) {
            throw e;
        }
    }
}
