package com.highfly029.parser;

import com.google.protobuf.ByteString;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.core.tool.PacketParserTool;
import com.highfly029.utils.ByteUtils;
import com.highfly029.utils.ReflectionUtils;
import com.highfly029.utils.collection.ObjectList;


/**
 * @ClassName PbPacketParser
 * @Description PbPacketParser
 * @Author liyunpeng
 **/
public class PbPacketParser {
    public static void register() throws Exception {
        ObjectList<Class> clzList = ReflectionUtils.getAllClassByInterface(IPacketParser.class, PbPacketParser.class.getPackageName());
        for (int i = 0, len = clzList.size(); i < len; i++) {
            Class cls = clzList.get(i);
            IPacketParser packetParser = (IPacketParser) cls.getDeclaredConstructor().newInstance();
            PacketParserTool.registerPacketParser(packetParser.getPtCode(), packetParser);
        }
    }

    /**
     * cat packet.log |awk -F '\0' '{if($2>=100) print $2,$5}' |xargs -L1 java -jar PacketParser-1.0.0.jar
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            register();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (args.length >= 3) {
            int ptCode = Integer.valueOf(args[0]);
            String msg = args[1];

            parse(ptCode, msg);
        } else {
            int ptCode = 20006;
            String msg = "0802100118808080804020808080880428b0e5b6c20332093132372e302e302e3138924e42030203014a020103";

            parse(ptCode, msg);
        }

    }


    private static void parse(int ptCode, String msg) {
        if (msg == null || msg.equals("null")) {
            System.out.println("ptCode=" + ptCode + ":null");
            return;
        }

        String out;
        try {
            byte[] array = ByteUtils.hexStringToByteArray(msg);
            IPacketParser packetParser = PacketParserTool.getPacketParser(ptCode);
            if (packetParser == null) {
                System.out.println("ptCode=" + ptCode + ":packetParser is null");
                return;
            }
            //如果解析出现异常、返回null
            out = packetParser.parser(ByteString.copyFrom(array));
            if (out == null) {
                System.out.println("ptCode=" + ptCode + ":out is null");
                return;
            }
        } catch (Exception e) {
            System.out.println("exception ptCode=" + ptCode + " :" + msg);
            return;
        }

        System.out.println("ptCode=" + ptCode + ":" + out);
    }
}
