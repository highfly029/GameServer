package com.highfly029.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import com.google.protobuf.ByteString;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.core.tool.PacketParserTool;
import com.highfly029.utils.ReflectionUtils;
import com.highfly029.utils.collection.ObjectList;

import org.apache.commons.text.StringEscapeUtils;

/**
 * @ClassName PbPacketParserString
 * @Description PbPacketParserString
 * @Author liyunpeng
 **/
public class PbPacketParserString {
    public static void register() throws Exception {
        ObjectList<Class> clzList = ReflectionUtils.getAllClassByInterface(IPacketParser.class, PbPacketParser.class.getPackageName());
        for (int i = 0, len = clzList.size(); i < len; i++) {
            Class cls = clzList.get(i);
            IPacketParser packetParser = (IPacketParser) cls.getDeclaredConstructor().newInstance();
            PacketParserTool.registerPacketParser(packetParser.getPtCode(), packetParser);
        }
    }

    public static void main(String[] args) {
        try {
            register();
        } catch (Exception e) {
            e.printStackTrace();
        }

        int ptCode = 505;
        String msg = "\b\u0001\u0010\u0001";
        parse(ptCode, msg);
    }

    /**
     * 有的不能用
     * @return
     */
    private static String getDataLogString() {
        try {
            String classPath = PbPacketParserString.class.getResource("/").getPath();
            File file = new File(classPath);
            classPath = file.getParentFile().getParentFile().getPath();
            classPath += "/src/main/resources/data.log";
            BufferedReader reader = new BufferedReader(new FileReader(classPath));
            String msg = reader.readLine();
            return StringEscapeUtils.unescapeJava(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void parse(int ptCode, String msg) {
        if (msg == null || msg.equals("null")) {
            System.out.println("ptCode=" + ptCode + ":null");
            return;
        }

        String out;
        try {
            IPacketParser packetParser = PacketParserTool.getPacketParser(ptCode);
            if (packetParser == null) {
                System.out.println("ptCode=" + ptCode + ":packetParser is null");
                return;
            }
            //如果解析出现异常、返回null
            out = packetParser.parser(ByteString.copyFromUtf8(msg));
            if (out == null) {
                System.out.println("ptCode=" + ptCode + ":out is null");
                return;
            }
        } catch (Exception e) {
            System.out.println("exception ptCode=" + ptCode + " :" + msg);
            return;
        }

        System.out.println("ptCode=" + ptCode + ":" + out);
    }
}
