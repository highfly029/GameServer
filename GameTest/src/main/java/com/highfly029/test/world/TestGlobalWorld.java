package com.highfly029.test.world;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.interfaces.IHttpPath;
import com.highfly029.core.interfaces.ITimerCallback;
import com.highfly029.core.net.udp.UdpClient;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.plugins.timer.Timer;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.utils.ConfigPropUtils;

import io.vertx.mysqlclient.MySQLPool;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName TestGlobalWorld
 * @Description TestGlobalWorld
 * @Author liyunpeng
 **/
public class TestGlobalWorld extends GlobalWorld {
    private static final AtomicInteger id = new AtomicInteger();
    public static int clientNum;
    private final AtomicLong sequenceNumber = new AtomicLong(1);
    private PbPacket.TcpPacket.Builder builder;
    private long httpClientSendNum;
    private long httpClientReceivedSuccessNum;
    private long httpClientReceivedFailNum;
    private boolean httpClientTestFlag;
    private boolean triggerRedisTest;
    private boolean triggerPressTest;
    private boolean triggerMysqlTest;
    private boolean tcpClientTest;
    private boolean udpClientTest;

    /**
     * mysql数据库连接
     */
    public static MySQLPool mySQLPool;

    public TestGlobalWorld(int childWorldNum) {
        super(false, childWorldNum, 100);
    }


    private UdpClient udpClient;

    @Override
    public void onStartUpSuccess() {
        super.onStartUpSuccess();
        udpClient = (UdpClient) netTool.clientMaps.get("udpwork1");
        mySQLPool = mysqlTool.getConnection("connectTest");
    }

    @Override
    public void onTick(int escapedMillTime) {
        /**
         * 模拟业务压力 10000000L cpu=100%, cost 140ms
         */
        if (triggerPressTest == true) {
            long num = 0;
            int a = 7;
            int b = 12;
            for (long i = 0; i < 10000000L; i++) {
                num += i * 0.5 * 2 / 0.5 / 2 * a / (b - 2);
            }
        }
    }

    @Override
    public void onSecond() {
//        LoggerTool.systemLogger.info("=====triger onSecond======={}", System.currentTimeMillis());
//        sendTcpClientTest();
//        sendHttpClientTest();
//        sendHttpsClientTest();
//        redisTest();
//        mysqlTest();
//        sendUdpClientTest();
    }

    private void sendUdpClientTest() {
//        Channel channel = udpClient.getChannel();
//        String name = udpClient.getName();
//        PbCommon.Request.Builder req = PbCommon.Request.newBuilder();
//        req.setId(7777777);
//        StringBuilder stringBuilder = new StringBuilder();                stringBuilder.append(name);
//        stringBuilder.append(" udp测试请求");
//        req.setReq(stringBuilder.toString());
//
//        PbPacket.UdpPacket.Builder udpPacketBuilder = PbPacket.UdpPacket.newBuilder();
//        udpPacketBuilder.setPtCode(1001);
//        udpPacketBuilder.setData(req.build().toByteString());
//        udpPacketBuilder.setSequenceNum(sequenceNumber.getAndIncrement());
//        PbPacket.Udp.Builder udpBuilder = PbPacket.Udp.newBuilder();
//        udpBuilder.setPacket(udpPacketBuilder.build().toByteString());
//        channel.writeAndFlush(udpBuilder);
    }

    @Override
    public void onUdpServerEvent(LogicEvent event) {
        super.onUdpServerEvent(event);
        switch (event.getLogicEventAction()) {
            case LogicEventAction.CHANNEL_READ -> {
                try {
//                    PbPacket.Udp udpPacket = (PbPacket.Udp) event.data;
//                    PbPacket.UdpPacket packet = PbPacket.UdpPacket.parseFrom(udpPacket.getPacket());
//                    PbCommon.Request req = PbCommon.Request.parseFrom(packet.getData());
//                    int id = req.getId();
//                    String data = req.getReq();
//                    LoggerTool.systemLogger.info("UDP Server Received PtCode={},sequenceNum={},resp.id={},resp.data={}",
//                            packet.getPtCode(), 0, id, data);
//
//                    PbCommon.Response.Builder resp = PbCommon.Response.newBuilder();
//                    resp.setId(id);
//                    resp.setResp(data + " 这是响应");
//                    PbPacket.UdpPacket.Builder builder = PbPacket.UdpPacket.newBuilder();
//                    builder.setPtCode(1002);
//                    builder.setData(resp.build().toByteString());
//            builder.setSequenceNum(packet.getSequenceNum());
//                    PbPacket.Udp.Builder udpBuilder = PbPacket.Udp.newBuilder();
//                    udpBuilder.setPacket(builder.build().toByteString());
//                    udpBuilder.setUdpHost(udpPacket.getUdpHost());
//                    udpBuilder.setUdpPort(udpPacket.getUdpPort());
//                    event.channel.writeAndFlush(udpBuilder);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onUdpClientEvent(LogicEvent event) {
        super.onUdpClientEvent(event);
        switch (event.getLogicEventAction()) {
            case LogicEventAction.CHANNEL_READ -> {
                try {
//                    PbPacket.Udp udpPacket = (PbPacket.Udp) event.data;
//                    PbPacket.UdpPacket packet = PbPacket.UdpPacket.parseFrom(udpPacket.getPacket());
//                    PbCommon.Request req = PbCommon.Request.parseFrom(packet.getData());
//                    int id = req.getId();
//                    String data = req.getReq();
//                    LoggerTool.systemLogger.info("UDP Client Received PtCode={},sequenceNum={},resp.id={},resp.data={}",
//                            packet.getPtCode(), 0, id, data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void mysqlTest() {
        if (triggerMysqlTest == true) {
            return;
        }
        triggerMysqlTest = true;


        for (int i = 0; i < 20; i++) {
            int num = i + 1;
            Tuple tuple = Tuple.of(num);

            MySQLUtils.sendGlobal(mySQLPool, "SELECT * FROM t1 where id = ?;", tuple, (isSuccess, rows, errorMsg) -> {
                if (isSuccess) {
                    rows.forEach(data -> {
                        LoggerTool.systemLogger.info("connectTest myid={}, id={}, name={}, text={}",
                                num,
                                data.getInteger("id"),
                                data.getString("name"),
                                data.getString("text"));
                    });
                } else {
                    LoggerTool.systemLogger.info("onMySQLCallbackFail id={}, {}", num, errorMsg);
                }
            });
        }


        for (int i = 0; i < 10; i++) {
            int num = i + 1;
            Tuple tuple = Tuple.of(num);
            MySQLUtils.sendChild(mySQLPool, "SELECT * FROM t1 where id = ?;", tuple, 0, (isSuccess, rows, errorMsg) -> {
                if (isSuccess) {
                    rows.forEach(data -> {
                        LoggerTool.systemLogger.info("localTest myid={}, id={}, name={}, text={}",
                                num,
                                data.getInteger("id"),
                                data.getString("name"),
                                data.getString("text"));
                    });
                } else {
                    LoggerTool.systemLogger.info("onMySQLCallbackFail id={}, {}", num, errorMsg);
                }
            });
        }

    }

    private void redisTest() {


    }

    private void timerTest(int delay, int repeated, int interval) {
        ITimerCallback callback = (Timer timer) -> {
            LoggerTool.systemLogger.info("TimerCallback timer={}", timer);
        };
        Timer timer = Timer.create().buildDelay(delay).buildRepeated(repeated).buildInterval(interval).buildCallback(callback);
        timerPlugin.addTimer(timer);
    }

    private void sendHttpClientTest() {
//        HttpClient client = netTool.getHttpClient();
//        HttpClient client = netTool.getHttpSSLClient();
//        HttpClientRequestBuilder builder = new HttpClientRequestBuilder();
//        client.send(builder.setUri("https://www.baifubao.com/callback?cmd=1059&callback=phone&phone=18513866429").get().build(),
//                new HttpCallback() {
//                    @Override
//                    public void onHttpCallbackSuccess(String result) {
//                        LoggerTool.systemLogger.info("success result={}", result);
//                    }
//
//                    @Override
//                    public void onHttpCallbackFail(int errorCode) {
//                        LoggerTool.systemLogger.info("fail code={}", errorCode);
//                    }
//                });
        if (!ConfigPropUtils.getBoolValue("HttpClientTest")) {
            return;
        }
        LoggerTool.systemLogger.info("HttpClientTest sendNum={}, receivedSuccessNum={} receivedFailNum={}",
                this.httpClientSendNum, this.httpClientReceivedSuccessNum, this.httpClientReceivedFailNum);
        if (!httpClientTestFlag) {
            return;
        }
    }

    private void sendHttpsClientTest() {

    }

    @Override
    protected IHttpPath getHttpPath(String path) {
        return super.getHttpPath(path);
    }

    //    @Override
//    public String onHttpResponse(String path, HttpMethod method, Map<String, String> params, String content, HttpHeaders headers) {
//        String ret;
//        switch (path){
//            case HttpPathConst.httpClientTest: {
//                if (params.get("flag").equals("true")){
//                    httpClientTestFlag = true;
//                } else {
//                    httpClientTestFlag = false;
//                }
//                ret = "success";
//                break;
//            }
//            case HttpPathConst.timerTest : {
//                int delay = Integer.valueOf(params.get("delay"));
//                int repeated = Integer.valueOf(params.get("repeated"));
//                int interval = Integer.valueOf(params.get("interval"));
//                timerTest(delay, repeated, interval);
//                ret = "success";
//                break;
//            }
//            case HttpPathConst.press: {
//                if (params.get("flag").equals("true")){
//                    triggerPressTest = true;
//                } else {
//                    triggerPressTest = false;
//                }
//                ret = "success";
//                break;
//            }
//            default:{
//                ret = "fail";
//            }
//        }
//        return ret;
//    }

    @Override
    public void onTcpClientEvent(LogicEvent event) {
//        LoggerTool.systemLogger.info("onTcpClientEvent");
        try {
//            PbPacket.TcpPacket packet = (PbPacket.TcpPacket) event.data;
//            PbCommon.Response resp = PbCommon.Response.parseFrom(packet.getData());
//            int id = resp.getId();
//            String data = resp.getResp();
//            Channel channel = (Channel) event.channel;
//            Attribute<String> value = channel.attr(TcpClientHandler.CLIENT_NAME);
//            String name = value.get();
//            LoggerTool.systemLogger.info("name = {},id={}, data={}", name, id, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
