package com.highfly029.test.world;

import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.DisruptorBuilder;
import com.highfly029.core.world.SleepingWaitExtendStrategy;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

/**
 * @ClassName TestStarter
 * @Description TestStarter
 * @Author liyunpeng
 **/
public class TestStarter {
    public static void main(String[] args) {
        int childNum = 4;
        TestGlobalWorld testGlobalWorld = new TestGlobalWorld(childNum);
        WaitStrategy waitStrategy = new SleepingWaitExtendStrategy(testGlobalWorld);
        Disruptor disruptor = DisruptorBuilder.builder()
                .setPoolName("TestGlobal")
                .setProducerType(ProducerType.MULTI)
                .setWorldThread(testGlobalWorld)
                .setWaitStrategy(waitStrategy)
                .build();

        WorldTool worldTool = WorldTool.getInstance();
        worldTool.setDisruptor(disruptor);
        worldTool.setGlobalWorld(testGlobalWorld);

        for (int i = 1; i <= childNum; i++) {
            TestChildWorld testChildWorld = new TestChildWorld(i);
            WaitStrategy childWait = new SleepingWaitExtendStrategy(testChildWorld);
            Disruptor childDisruptor = DisruptorBuilder.builder()
                    .setPoolName("TestChild")
                    .setProducerType(ProducerType.SINGLE)
                    .setWorldThread(testChildWorld)
                    .setWaitStrategy(childWait)
                    .setIndex(i)
                    .build();
            worldTool.addChildWorld(i, childDisruptor);
        }
        worldTool.start();
    }
}
