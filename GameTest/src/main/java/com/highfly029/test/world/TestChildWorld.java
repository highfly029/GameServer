package com.highfly029.test.world;

import com.highfly029.core.world.ChildWorld;

/**
 * @ClassName TestChildWorld
 * @Description TestChildWorld
 * @Author liyunpeng
 **/
public class TestChildWorld extends ChildWorld {

    public TestChildWorld(int index) {
        super(index, 100);
    }
}
