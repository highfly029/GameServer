package com.highfly029.test;

/**
 * @ClassName HttpPathConst
 * @Description HttpPathConst
 * @Author liyunpeng
 **/
public class HttpPathConst {
    public static final String press = "press";
    public static final String timeChange = "timeChange";
    public static final String timeReset = "timeReset";
    public static final String httpClientTest = "httpClientTest";
    public static final String timerTest = "timerTest";
}
