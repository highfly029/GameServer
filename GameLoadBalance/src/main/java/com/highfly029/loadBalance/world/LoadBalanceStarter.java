package com.highfly029.loadBalance.world;

import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.DisruptorBuilder;
import com.highfly029.core.world.LogicEvent;
import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

/**
 * @ClassName LoadBalanceStarter
 * @Description LoadBalanceStarter
 * @Author liyunpeng
 **/
public class LoadBalanceStarter {
    public static void main(String[] args) {

        LoadBalanceGlobalWorld loadBalanceGlobalWorld = new LoadBalanceGlobalWorld(0);
        Disruptor<LogicEvent> disruptor = DisruptorBuilder.builder()
                .setPoolName("LoadBalance")
                .setProducerType(ProducerType.MULTI)
                .setWorldThread(loadBalanceGlobalWorld)
                .setWaitStrategy(new BlockingWaitStrategy())
                .build();

        WorldTool worldTool = WorldTool.getInstance();
        worldTool.setDisruptor(disruptor);
        worldTool.setGlobalWorld(loadBalanceGlobalWorld);

        worldTool.start();
    }

}
