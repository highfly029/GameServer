package com.highfly029.loadBalance.world;

import java.util.ArrayList;

import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.interfaces.IHttpPath;
import com.highfly029.core.interfaces.IServerGlobalProtocolHandler;
import com.highfly029.core.net.NetUtils;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.protocol.BasePtCode;
import com.highfly029.core.session.Session;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.loadBalance.enums.LoadBalancePathEnum;
import com.highfly029.loadBalance.manager.LoadBalanceLogicManager;
import com.highfly029.loadBalance.manager.LoadBalanceSceneManager;
import com.highfly029.loadBalance.manager.logic.MemoryLogicStrategy;
import com.highfly029.loadBalance.manager.scene.MemorySceneStrategy;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.StringUtils;

import io.netty.channel.Channel;

/**
 * @ClassName LoadBalanceGlobalWorld
 * @Description LoadBalanceGlobalWorld
 * @Author liyunpeng
 **/
public class LoadBalanceGlobalWorld extends GlobalWorld {
    private LoadBalanceSceneManager loadBalanceSceneManager;

    private LoadBalanceLogicManager loadBalanceLogicManager;

    public static ArrayList<Object> list = new ArrayList<>();

    public LoadBalanceGlobalWorld(int childWorldNum) {
        super(true, childWorldNum, 500);
    }

    @Override
    protected void registerAllPacketParser() throws Exception {
        registerPacketParser("com.highfly029.parser.pb");
        registerPacketParser("com.highfly029.parser.custom");
    }

    @Override
    protected IHttpPath getHttpPath(String path) {
        LoadBalancePathEnum pathEnum = null;
        try {
            pathEnum = LoadBalancePathEnum.valueOf(path);
        } catch (Exception exception) {
            LoggerTool.httpLogger.error("not exist path", exception);
        }
        return pathEnum;
    }

    @Override
    public void onTcpServerEvent(LogicEvent event) {
        byte action = event.getLogicEventAction();
        if (action == LogicEventAction.CHANNEL_READ) {
            Channel channel = (Channel) event.getData2();
            PbPacket.TcpPacket packet = (PbPacket.TcpPacket) event.getData1();
            tcpServerRead(channel, packet);
        } else {
            switch (action) {
                case LogicEventAction.CHANNEL_INACTIVE -> {
                    Channel channel = (Channel) event.getData1();
                    Session session = sessionTool.getSession(channel);
                    if (session != null) {
                        if (session.getType() == SessionType.Scene) {
                            loadBalanceSceneManager.getSceneStrategy().onServerInactive(session.getIdentifier());
                        } else if (session.getType() == SessionType.Logic) {
                            loadBalanceLogicManager.getLogicStrategy().onServerInactive(session.getIdentifier());
                        }
                        LoggerTool.systemLogger.warn("inactive disconnect {} type={}, id={}", session.getChannel(), session.getType(), session.getIdentifier());
                        sessionTool.removeSession(session);
                    } else {
                        LoggerTool.systemLogger.error("inactive disconnect invalid session is null channel={}, text={}", channel, channel.id().asShortText());
                    }
                }
                default -> super.onTcpServerEvent(event);
            }
        }
    }

    /**
     * tcpServer读
     *
     * @param channel
     * @param packet
     */
    private void tcpServerRead(Channel channel, PbPacket.TcpPacket packet) {
        int ptCode = packet.getPtCode();
        int type = 0;
        int identifier = 0;
        try {
            if (ptCode == BasePtCode.HEART_REQ) {
                channel.writeAndFlush(serverHeartResp);
            } else if (ptCode != BasePtCode.CONNECT_TYPE_REQ) {
                Session session = sessionTool.getSession(channel);
                type = session.getType();
                identifier = session.getIdentifier();
                IServerGlobalProtocolHandler handler = serverGlobalHandlerMaps.get(ptCode);
                handler.onServerProtocolHandler(session, ptCode, packet);
            } else {
                PbCommon.ConnectTypeReq req = PbCommon.ConnectTypeReq.parseFrom(packet.getData());
                type = req.getType();
                identifier = req.getIdentifier();
                boolean isReconnect = req.getIsReConnect();

                PbCommon.ConnectTypeResp.Builder resp = PbCommon.ConnectTypeResp.newBuilder();
                resp.setType(SessionType.LoadBalance);
                resp.setIdentifier(ConfigConst.identifier);
                resp.setIsReConnect(isReconnect);

                String features = ConfigPropUtils.getValue("features");
                int[] featureArray = StringUtils.toIntArray(features, ",");
                if (featureArray != null) {
                    for (int feature : featureArray) {
                        resp.addFeatures(feature);
                    }
                }
                Session session = sessionTool.getSession(type, identifier);
                if (session != null) {
                    //重复的连接 直接断开
                    LoggerTool.systemLogger.error("connect duplicate type={}, id={}", type, identifier);
                    channel.close();
                    return;
                }
                session = sessionTool.create();
                session.init(type, identifier, channel);
                sessionTool.addSession(session);
                PbPacket.TcpPacket tcpPacket = createTcpPacket(BasePtCode.CONNECT_TYPE_RESP, resp.build().toByteString());
                channel.writeAndFlush(tcpPacket);
                LoggerTool.systemLogger.info("接收到连接 session type={}, id={}, ip={}", type, identifier, NetUtils.getIP(channel));

                if (session.getType() == SessionType.Scene) {
                    loadBalanceSceneManager.getSceneStrategy().onServerActive(session.getIdentifier());
                } else if (session.getType() == SessionType.Logic) {
                    loadBalanceLogicManager.getLogicStrategy().onServerActive(session.getIdentifier());
                }
            }
        } catch (Exception e) {
            LoggerTool.systemLogger.error("errorMsg: ptCode={}, type={}, id={}, msg={}", ptCode, type, identifier, e.getMessage());
            LoggerTool.systemLogger.error("tcpServerRead", e);
        }
    }

    @Override
    protected void loadAllUpdate() {
        loadBalanceSceneManager = new LoadBalanceSceneManager(this, new MemorySceneStrategy(this));
        updateMaps.put(LoadBalanceSceneManager.class, loadBalanceSceneManager);

        loadBalanceLogicManager = new LoadBalanceLogicManager(this, new MemoryLogicStrategy(this));
        updateMaps.put(LoadBalanceLogicManager.class, loadBalanceLogicManager);
    }

    public LoadBalanceSceneManager getLoadBalanceSceneManager() {
        return loadBalanceSceneManager;
    }

    public LoadBalanceLogicManager getLoadBalanceLogicManager() {
        return loadBalanceLogicManager;
    }
}
