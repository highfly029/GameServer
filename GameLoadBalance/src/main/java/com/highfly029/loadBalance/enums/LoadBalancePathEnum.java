package com.highfly029.loadBalance.enums;

import com.highfly029.core.interfaces.IHttpPath;

/**
 * @ClassName LoadBalancePathEnum
 * @Description LoadBalancePathEnum
 * @Author liyunpeng
 **/
public enum LoadBalancePathEnum implements IHttpPath {
    /**
     * 选择登陆服信息
     */
    selectLogicServerInfo("selectLogicServerInfo"),

    oom("oom"),


    ;

    private final String path;

    public String path() {
        return path;
    }

    LoadBalancePathEnum(String path) {
        this.path = path;
    }

    @Override
    public boolean isOneChildHandleOnly() {
        return false;
    }

    @Override
    public boolean isAllChildHandle() {
        return false;
    }
}
