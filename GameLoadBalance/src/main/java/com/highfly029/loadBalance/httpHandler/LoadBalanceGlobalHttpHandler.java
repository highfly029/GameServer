package com.highfly029.loadBalance.httpHandler;

import com.highfly029.core.net.http.BaseGlobalHttpHandler;
import com.highfly029.core.world.WorldThread;
import com.highfly029.loadBalance.world.LoadBalanceGlobalWorld;

/**
 * @ClassName LoadBalanceGlobalHttpHandler
 * @Description LoadBalanceGlobalHttpHandler
 * @Author liyunpeng
 **/
public abstract class LoadBalanceGlobalHttpHandler extends BaseGlobalHttpHandler {
    protected LoadBalanceGlobalWorld globalWorld;

    @Override
    public void init(WorldThread worldThread) {
        globalWorld = (LoadBalanceGlobalWorld) worldThread;
    }
}
