package com.highfly029.loadBalance.httpHandler;

import com.highfly029.core.tool.LoggerTool;
import com.highfly029.loadBalance.enums.LoadBalancePathEnum;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @ClassName TestOOMHandler
 * @Description TestOOMHandler
 * @Author liyunpeng
 **/
public class TestOOMHandler extends LoadBalanceGlobalHttpHandler {

    @Override
    public String register() {
        return LoadBalancePathEnum.oom.path();
    }

    @Override
    public void handler(Channel channel, FullHttpRequest request) {
        LoggerTool.httpLogger.info("test oom!");
        while (true) {
            globalWorld.list.add(new int[1024 * 128]);
        }
    }
}
