package com.highfly029.loadBalance.httpHandler;

import com.highfly029.core.net.http.HttpConst;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.loadBalance.enums.LoadBalancePathEnum;
import com.highfly029.loadBalance.manager.LoadBalanceLogicManager;
import com.highfly029.loadBalance.manager.logic.LogicServerInfo;
import com.highfly029.utils.JsonUtils;
import com.highfly029.utils.collection.ObjObjMap;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @ClassName SelectLogicServerInfoHandler
 * @Description SelectLogicServerInfoHandler
 * @Author liyunpeng
 **/
public class SelectLogicServerInfoHandler extends LoadBalanceGlobalHttpHandler {
    @Override
    public String register() {
        return LoadBalancePathEnum.selectLogicServerInfo.path();
    }

    /**
     * demo
     * http://127.0.0.1/LoadBalance/selectLogicServerInfo?accountID=1&sourceLogicID=1
     *
     * @param channel
     * @param request
     */
    @Override
    public void handler(Channel channel, FullHttpRequest request) {
        ObjObjMap<String, String> params = HttpUtils.getRequestParams(request);
        String accountID = params.get("accountID");
        int sourceLogicID = Integer.parseInt(params.get("sourceLogicID"));
        ObjObjMap<String, String> result = new ObjObjMap<>(String[]::new, String[]::new);
        LoadBalanceLogicManager manager = globalWorld.getLoadBalanceLogicManager();
        LogicServerInfo logicServerInfo = manager.getLogicStrategy().selectLogicServer(Long.parseLong(accountID), sourceLogicID);
        if (logicServerInfo != null) {
            result.put("result", HttpConst.RESP_SUCCESS);
            result.put("host", logicServerInfo.getClientHost());
            result.put("port", String.valueOf(logicServerInfo.getClientPort()));
        } else {
            result.put("result", HttpConst.RESP_FAIL);
            result.put("msg", "not exist logic server"+sourceLogicID);
        }
        HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
    }
}
