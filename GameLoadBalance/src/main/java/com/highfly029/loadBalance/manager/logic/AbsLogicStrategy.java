package com.highfly029.loadBalance.manager.logic;

import java.util.HashMap;
import java.util.Map;

import com.highfly029.common.data.base.Location;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.loadBalance.world.LoadBalanceGlobalWorld;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName AbsLogicStrategy
 * @Description AbsLogicStrategy
 * @Author liyunpeng
 **/
public abstract class AbsLogicStrategy {
    /**
     * 玩家的定位状态
     * key=accountID
     */
    protected Map<Long, Location> accountLocationMap = new HashMap<>();

    /**
     * 玩家的定位状态
     * key=playerID
     */
    protected Map<Long, Location> playerLocationMap = new HashMap<>();

    protected LoadBalanceGlobalWorld globalWorld;

    //TODO 排队系统

    /**
     * 逻辑服服务器信息集合
     */
    protected IntObjectMap<LogicServerInfo> serverInfoMap = new IntObjectMap<>(LogicServerInfo[]::new);


    public AbsLogicStrategy(LoadBalanceGlobalWorld globalWorld) {
        this.globalWorld = globalWorld;
    }

    /**
     * 获取逻辑服信息
     *
     * @param id
     * @return
     */
    public LogicServerInfo getLogicServerInfo(int id) {
        return serverInfoMap.get(id);
    }

    /**
     * 获取玩家定位信息
     *
     * @param accountID
     * @return
     */
    public Location getAccountLocation(long accountID) {
        return accountLocationMap.get(accountID);
    }

    /**
     * 获取玩家定位信息
     * @param playerID
     * @return
     */
    public Location getPlayerLocation(long playerID) {
        return playerLocationMap.get(playerID);
    }


    /**
     * 增加逻辑服务器信息
     *
     * @param id
     * @param clientHost
     * @param clientPort
     * @param playerNum
     */
    public void addLogicServerInfo(int id, String clientHost, int clientPort, int playerNum) {
        LogicServerInfo logicServerInfo = new LogicServerInfo();
        logicServerInfo.setServerID(id);
        logicServerInfo.setClientHost(clientHost);
        logicServerInfo.setClientPort(clientPort);
        logicServerInfo.setPlayerNum(playerNum);
        logicServerInfo.setActive(true);
        serverInfoMap.put(id, logicServerInfo);
        LoggerTool.systemLogger.info("addLogicServerInfo id={}, playerNum={}", id, playerNum);
    }

    /**
     * 逻辑服务器断开连接
     *
     * @param id
     */
    public void onServerInactive(int id) {
        LogicServerInfo logicServerInfo = serverInfoMap.get(id);
        if (logicServerInfo == null) {
            LoggerTool.systemLogger.error("onServerInactive logicServerInfo is null id={}", id);
            return;
        }
        logicServerInfo.setActive(false);
        LoggerTool.systemLogger.info("onServerInactive id={}", id);
    }

    /**
     * 逻辑服务器重新连接
     * @param id
     */
    public void onServerActive(int id) {
        LogicServerInfo logicServerInfo = serverInfoMap.get(id);
        if (logicServerInfo != null) {
            logicServerInfo.setActive(true);
            LoggerTool.systemLogger.info("onServerActive id={}", id);
        }
    }

    /**
     * 推送玩家的定位状态
     *
     * @param playerID
     * @param location
     * @param isNewPlayer 是否是新号玩家
     */
    public void onPushPlayerLocationState(long accountID, long playerID, Location location, boolean isNewPlayer) {
        LoggerTool.systemLogger.info("onPushPlayerLocationState accountID={}, playerID={}, state={}", accountID, playerID, location.getState());
        LogicServerInfo logicServerInfo = serverInfoMap.get(location.getLogicServerID());
        if (logicServerInfo != null) {
            if (location.getState() == Location.LOGIN_SUCCESS) {
                logicServerInfo.setPlayerNum(logicServerInfo.getPlayerNum() + 1);
            } else if (location.getState() == Location.LOGOUT) {
                logicServerInfo.setPlayerNum(logicServerInfo.getPlayerNum() - 1);
            }
        }
    }

    /**
     * 选择登录的逻辑服务器
     *
     * @return
     */
    public LogicServerInfo selectLogicServer(long accountID, int sourceLogicID) {
        return selectMinPlayerServer();
    }

    /**
     * 踢出账号
     *
     * @param accountID
     */
    public void kickOut(long accountID) {

    }

    /**
     * 选择一个人最少的登陆服务器
     *
     * @return 服务器信息
     */
    protected LogicServerInfo selectMinPlayerServer() {
        var ref = new Object() {
            int minNum = Integer.MAX_VALUE;
            LogicServerInfo serverInfo;
        };
        serverInfoMap.foreachImmutable((id, serverInfo) -> {
            if (!serverInfo.isActive()) {
                return;
            }
            int num;
            if ((num = serverInfo.getPlayerNum()) < ref.minNum) {
                ref.minNum = num;
                ref.serverInfo = serverInfo;
            }
        });
        return ref.serverInfo;
    }
}
