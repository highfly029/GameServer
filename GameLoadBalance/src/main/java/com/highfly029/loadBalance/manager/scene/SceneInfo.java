package com.highfly029.loadBalance.manager.scene;

import com.highfly029.core.session.Session;
import com.highfly029.utils.collection.LongSet;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName SceneInfo
 * @Description 场景信息
 * @Author liyunpeng
 **/
public class SceneInfo {
    /**
     * 场景ID
     */
    private int sceneID;
    /**
     * 场景实例id
     * =0表示还没有创建成功
     */
    private int instanceID;
    /**
     * 场景玩家
     */
    private LongSet players = new LongSet();
    /**
     * 场景限制人数
     */
    private int limitNum;
    /**
     * 等待进入预创建场景的玩家队列
     */
    private ObjectList<PreCreateWaitingInfo> preCreateList = new ObjectList<>(PreCreateWaitingInfo[]::new);

    /**
     * 场景已经创建完成
     *
     * @return
     */
    public boolean isCreationComplete() {
        return instanceID > 0;
    }

    /**
     * 获取当前场景的玩家数量
     *
     * @return
     */
    public int getPlayerNum() {
        return players.size() + preCreateList.size();
    }

    /**
     * 是否能够进入场景
     *
     * @return
     */
    public boolean isCanEnterScene() {
        return limitNum <= 0 || getPlayerNum() < limitNum;
    }

    /**
     * 增加预创建场景等待的玩家
     *
     * @param session
     * @param playerID
     * @param logicWorldIndex
     */
    public void addPreCreateInfo(Session session, long playerID, int logicWorldIndex) {
        PreCreateWaitingInfo preCreateWaitingInfo = new PreCreateWaitingInfo(session, playerID, logicWorldIndex);
        preCreateList.add(preCreateWaitingInfo);
    }

    public ObjectList<PreCreateWaitingInfo> getPreCreateList() {
        return preCreateList;
    }

    /**
     * 清理预创建场景等待队列
     */
    public void clearPreCreateInfo() {
        preCreateList.clear();
    }

    /**
     * 预创建场景临时等待类
     */
    public static class PreCreateWaitingInfo {
        private Session session;
        private long playerID;
        private int logicWorldIndex;

        public PreCreateWaitingInfo(Session session, long playerID, int logicWorldIndex) {
            this.session = session;
            this.playerID = playerID;
            this.logicWorldIndex = logicWorldIndex;
        }

        public Session getSession() {
            return session;
        }

        public long getPlayerID() {
            return playerID;
        }

        public int getLogicWorldIndex() {
            return logicWorldIndex;
        }
    }

    public int getSceneID() {
        return sceneID;
    }

    public void setSceneID(int sceneID) {
        this.sceneID = sceneID;
    }

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public void setLimitNum(int limitNum) {
        this.limitNum = limitNum;
    }

    public LongSet getPlayers() {
        return players;
    }

    /**
     * 删除预创建玩家
     *
     * @param playerID
     */
    public void removePreCreateInfo(long playerID) {
        int removeIndex = -1;
        for (int i = 0; i < preCreateList.size(); i++) {
            if (preCreateList.get(i).playerID == playerID) {
                removeIndex = i;
            }
        }
        if (removeIndex > -1) {
            preCreateList.remove(removeIndex);
        }
    }
}
