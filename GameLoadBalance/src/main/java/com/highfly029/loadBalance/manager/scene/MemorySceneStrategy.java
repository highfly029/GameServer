package com.highfly029.loadBalance.manager.scene;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import com.highfly029.core.plugins.counter.IntegerCounter;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.loadBalance.world.LoadBalanceGlobalWorld;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.LongObjectMap;

/**
 * @ClassName MemorySceneStrategy
 * @Description 内存型策略，只有一个LoadBalance服时可用
 * @Author liyunpeng
 **/
public class MemorySceneStrategy extends AbsSceneStrategy {

    /**
     * 每个分线场景对应的分线计数器
     */
    private IntObjectMap<IntegerCounter> lineCounter = new IntObjectMap<>(IntegerCounter[]::new);


    public MemorySceneStrategy(LoadBalanceGlobalWorld globalWorld) {
        super(globalWorld);
    }

    /**
     * 获取工会场景
     *
     * @param result
     * @param unionID
     */
    protected void getUnionScene(FindSceneInfoResult result, long unionID) {
        getSceneByKey(result, unionID);

        if (result.isFind()) {
            if (!result.isPreCreate()) {
                //找到创建完成的场景，直接返回
                sendSceneInfo(result);
            }
        } else {
            //没找到去预创建
            preCreateSceneByKey(result, unionID);
        }
    }

    /**
     * 获取家园场景
     *
     * @param result
     * @param homePlayerID homePlayerID
     */
    protected void getHomeScene(FindSceneInfoResult result, long homePlayerID) {
        getSceneByKey(result, homePlayerID);

        if (result.isFind()) {
            if (!result.isPreCreate()) {
                //找到创建完成的场景，直接返回
                sendSceneInfo(result);
            }
        } else {
            //没找到去预创建
            preCreateSceneByKey(result, homePlayerID);
        }
    }

    /**
     * 根据key来查找场景
     *
     * @param result
     * @param key
     */
    private void getSceneByKey(FindSceneInfoResult result, long key) {
        serverInfoMap.foreachImmutable((serverID, info) -> {
            if (!isAvailableServer(info, result.getSceneFeature())) {
                return;
            }
            if (result.isFind()) {
                return;
            }
            info.getChildWorldInfos().foreachImmutable((worldIndex, worldInfo) -> {
                if (result.isFind()) {
                    return;
                }
                LongObjectMap<SceneInfo> sceneMap = worldInfo.getSceneStrategyMap(result.getSceneStrategy());
                if (sceneMap != null) {
                    SceneInfo sceneInfo = sceneMap.get(key);
                    if (sceneInfo != null && sceneInfo.isCanEnterScene()) {
                        if (sceneInfo.isCreationComplete()) {
                            //找到存在的场景
                            result.setFind(true);
                            result.setSceneServerInfo(info);
                            result.setSceneWorldIndex(worldIndex);
                            result.setSceneInfo(sceneInfo);
                        } else {
                            //找到预创建的场景
                            result.setFind(true);
                            result.setPreCreate(true);

                            sceneInfo.addPreCreateInfo(result.getSession(), result.getPlayerID(), result.getLogicWorldIndex());
                        }
                    }
                }
            });
        });
    }

    /**
     * 获取分线场景
     *
     * @param sceneID
     * @param lineID
     * @return
     */
    protected void getLineScene(FindSceneInfoResult result, int sceneID, int lineID) {
        long key = MathUtils.getCompositeIndex(sceneID, lineID);
        getSceneByKey(result, key);
        if (result.isFind()) {
            if (!result.isPreCreate()) {
                //找到创建完成的场景，直接返回
                sendSceneInfo(result);
            }
        } else {
            //没找到
            LoggerTool.systemLogger.error("getLineScene cant find scene playerID={} sceneID={}, lineID={}", result.getPlayerID(), sceneID, lineID);
        }
    }

    /**
     * 获取分线场景
     *
     * @param sceneID
     * @return
     */
    protected void getLineScene(FindSceneInfoResult result, int sceneID) {
        Map<Long, SceneInfo> filterMap = new HashMap<>();
        serverInfoMap.foreachImmutable((serverID, info) -> {
            if (!isAvailableServer(info, result.getSceneFeature())) {
                return;
            }
            info.getChildWorldInfos().foreachImmutable((worldIndex, worldInfo) -> {
                LongObjectMap<SceneInfo> sceneMap = worldInfo.getSceneStrategyMap(result.getSceneStrategy());
                if (sceneMap != null) {
                    sceneMap.foreachImmutable((mixID, sceneInfo) -> {
                        //找到所有能进入的场景
                        if (MathUtils.getCompositeArg1(mixID) == sceneID && sceneInfo.isCanEnterScene()) {
                            long compositeIndex = MathUtils.getCompositeIndex(serverID, worldIndex);
                            filterMap.put(compositeIndex, sceneInfo);
                        }
                    });
                }
            });
        });
        Map.Entry<Long, SceneInfo> resultEntry = filterMap.entrySet().stream().sorted((entry1, entry2) -> {
                    //优先有队友的
                    return 1;
                })
                //优先人数少的
                .sorted(Comparator.comparingInt(entry -> entry.getValue().getPlayerNum()))
                .findFirst().orElse(null);
        if (resultEntry != null) {
            long mixID = resultEntry.getKey();
            SceneInfo sceneInfo = resultEntry.getValue();
            int serverID = MathUtils.getCompositeArg1(mixID);
            int worldIndex = MathUtils.getCompositeArg2(mixID);
            SceneServerInfo sceneServerInfo = serverInfoMap.get(serverID);
            result.setFind(true);
            result.setSceneInfo(sceneInfo);
            result.setSceneServerInfo(sceneServerInfo);
            result.setSceneWorldIndex(worldIndex);
            sendSceneInfo(result);
        }

        if (!result.isFind()) {
            //没找到去预创建
            //生成lineID
            IntegerCounter integerCounter = lineCounter.get(sceneID);
            if (integerCounter == null) {
                integerCounter = new IntegerCounter();
                lineCounter.put(sceneID, integerCounter);
            }
            int newLineID = integerCounter.getNextValue();
            long key = MathUtils.getCompositeIndex(sceneID, newLineID);
            preCreateSceneByKey(result, key);
        }
    }

    /**
     * 获取副本场景
     * 副本类场景只有一个人能创建，创建成功后再通知其他人进入
     *
     * @param dungeonInstanceID
     * @return
     */
    protected void getDungeonScene(FindSceneInfoResult result, int dungeonType, int dungeonInstanceID) {

    }
}
