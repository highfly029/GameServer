package com.highfly029.loadBalance.manager;

import com.highfly029.core.manager.BaseManager;
import com.highfly029.core.world.WorldThread;
import com.highfly029.loadBalance.manager.logic.AbsLogicStrategy;

/**
 * @ClassName LoadBalanceLogicManager
 * @Description LoadBalanceLogicManager
 * @Author liyunpeng
 **/
public class LoadBalanceLogicManager extends BaseManager {
    /**
     * 逻辑服策略
     */
    private AbsLogicStrategy logicStrategy;

    public LoadBalanceLogicManager(WorldThread world, AbsLogicStrategy logicStrategy) {
        super(world);
        this.logicStrategy = logicStrategy;
    }

    public AbsLogicStrategy getLogicStrategy() {
        return logicStrategy;
    }

    /************ 使用BlockingWaitStrategy策略不会触发update相关更新 ************/

    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {

    }

    @Override
    public void onMinute() {

    }

    @Override
    public void onShutDown() {

    }
}
