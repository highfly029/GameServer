package com.highfly029.loadBalance.manager.scene;

import com.highfly029.common.data.scene.SceneStrategyEnum;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.session.Session;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.loadBalance.world.LoadBalanceGlobalWorld;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName AbsSceneStrategy
 * @Description AbsSceneStrategy
 * @Author liyunpeng
 **/
public abstract class AbsSceneStrategy {
    protected LoadBalanceGlobalWorld globalWorld;

    /**
     * 服务器信息集合
     */
    protected IntObjectMap<SceneServerInfo> serverInfoMap = new IntObjectMap<>(SceneServerInfo[]::new);

    public AbsSceneStrategy(LoadBalanceGlobalWorld globalWorld) {
        this.globalWorld = globalWorld;
    }

    /**
     * 增加场景服务器信息
     *
     * @param id
     * @param clientHost
     * @param clientPort
     * @param sceneFeatures
     * @param worldIndexList
     */
    public void addSceneServerInfo(int id, String clientHost, int clientPort, IntSet sceneFeatures, IntList worldIndexList) {
        if (sceneFeatures.isEmpty()) {
            LoggerTool.systemLogger.error("id={} addSceneServerInfo sceneFeatures is null", id);
        }
        SceneServerInfo sceneServerInfo = new SceneServerInfo();
        sceneServerInfo.setServerID(id);
        sceneServerInfo.setSceneFeatures(sceneFeatures);
        sceneServerInfo.setActive(true);
        sceneServerInfo.setClientHost(clientHost);
        sceneServerInfo.setClientPort(clientPort);
        sceneServerInfo.initChildWorld(worldIndexList);
        serverInfoMap.put(id, sceneServerInfo);
        LoggerTool.systemLogger.info("addSceneServerInfo id={},host={},port={},worldIndexList={}", id, clientHost, clientPort, worldIndexList);
    }

    /**
     * 场景服务器断开连接
     *
     * @param id
     */
    public void onServerInactive(int id) {
        SceneServerInfo sceneServerInfo = serverInfoMap.get(id);
        sceneServerInfo.setActive(false);
        LoggerTool.systemLogger.info("onServerInactive id={}", id);
    }

    /**
     * 场景服务器重新连接
     * @param id
     */
    public void onServerActive(int id) {
        SceneServerInfo sceneServerInfo = serverInfoMap.get(id);
        if (sceneServerInfo != null) {
            sceneServerInfo.setActive(true);
            LoggerTool.systemLogger.info("onServerActive id={}", id);
        }
    }

    /**
     * 增加场景子世界信息
     */
    public void addSceneChildWorldInfo() {
        LoggerTool.systemLogger.info("addSceneChildWorldInfo");
    }


    /**
     * 玩家向LoadBalance请求决策
     *
     * @param req 请求数据
     */
    public void getSceneServerInfo(Session session, PbScene.S2LGetSceneServerInfo req) {
        long playerID = req.getPlayerID();
        int logicWorldIndex = req.getLogicWorldIndex();
        int sceneID = req.getSceneID();
        int sceneFeature = req.getSceneFeature();
        int sceneStrategy = req.getSceneStrategy();
        LoggerTool.systemLogger.info("getSceneServerInfo serverID={}, playerID={}, sceneID={}, sceneFeature={}, sceneStrategy={}", session.getIdentifier(), playerID, sceneID, sceneFeature, sceneStrategy);
        SceneStrategyEnum strategyEnum = SceneStrategyEnum.values()[sceneStrategy];
        FindSceneInfoResult findSceneInfoResult = new FindSceneInfoResult(sceneFeature, sceneStrategy, sceneID, session, playerID, logicWorldIndex);
        switch (strategyEnum) {
            case HOME -> getHomeScene(findSceneInfoResult, req.getHomePlayerID());
            case UNION -> getUnionScene(findSceneInfoResult, req.getUnionID());
            case LINE -> {
                int lineID = req.getLineID();
                if (lineID > 0) {
                    getLineScene(findSceneInfoResult, sceneID, lineID);
                } else {
                    getLineScene(findSceneInfoResult, sceneID);
                }
            }
            case DUNGEON -> {
                getDungeonScene(findSceneInfoResult, req.getDungeonType(), req.getDungeonInstaneID());
            }
        }
    }

    /**
     * 返回逻辑服找到的场景信息
     *
     * @param result
     */
    protected void sendSceneInfo(FindSceneInfoResult result) {
        sendSceneInfo(result.getSession(), result.getPlayerID(), result.getLogicWorldIndex(), result.getSceneServerInfo(), result.getSceneWorldIndex(), result.getSceneInfo(), false);
    }

    protected void sendSceneInfo(Session session, long playerID, int logicWorldIndex, SceneServerInfo sceneServerInfo, int sceneWorldIndex, SceneInfo sceneInfo, boolean isCreateScene) {
        PbScene.L2SGetSceneServerInfo.Builder builder = PbScene.L2SGetSceneServerInfo.newBuilder();
        builder.setPlayerID(playerID);
        builder.setLogicWorldIndex(logicWorldIndex);
        builder.setClientHost(sceneServerInfo.getClientHost());
        builder.setClientPort(sceneServerInfo.getClientPort());
        builder.setSceneServerID(sceneServerInfo.getServerID());
        builder.setSceneWorldIndex(sceneWorldIndex);
        builder.setSceneID(sceneInfo.getSceneID());
        builder.setInstanceID(sceneInfo.getInstanceID());
        builder.setIsCreateScene(isCreateScene);
        session.sendMsgS2S(PtCode.L2SGetSceneServerInfo, builder.build());
    }

    /**
     * 根据key预创建场景
     *
     * @param result
     * @param key
     */
    protected void preCreateSceneByKey(FindSceneInfoResult result, long key) {
        //找一个人数最少的服的人数最少的子世界
        SceneServerInfo sceneServerInfo = getLeastPlayerNumServer(result.getSceneFeature());
        int sceneWorldIndex = sceneServerInfo.getLeastPlayerNumWorldIndex();

        //预创建
        LongObjectMap<SceneInfo> sceneMap = getSceneStrategyMap(sceneServerInfo.getServerID(), sceneWorldIndex, result.getSceneStrategy());
        SceneInfo sceneInfo = new SceneInfo();
        sceneInfo.addPreCreateInfo(result.getSession(), result.getPlayerID(), result.getLogicWorldIndex());
        sceneInfo.setSceneID(result.getSceneID());
        sceneMap.put(key, sceneInfo);

        //发送到指定服务器去创建场景 返回成功信息后再通知所有的等待中的玩家
        Session session = globalWorld.getSessionTool().getSession(SessionType.Scene, sceneServerInfo.getServerID());
        PbScene.L2SCreateScene.Builder builder = PbScene.L2SCreateScene.newBuilder();
        builder.setLoadBalanceServerID(ConfigConst.identifier);
        builder.setSceneServerID(sceneServerInfo.getServerID());
        builder.setSceneWorldIndex(sceneWorldIndex);
        builder.setSceneID(result.getSceneID());
        builder.setSceneStrategy(result.getSceneStrategy());
        builder.setKey(key);
        session.sendMsgS2S(PtCode.L2SCreateScene, builder.build());

        LoggerTool.systemLogger.info("preCreateSceneByKey serverID={}, sceneWorldIndex={}, sceneID={}, sceneStrategy={}, key={}",
                sceneServerInfo.getServerID(), sceneWorldIndex, result.getSceneID(), result.getSceneStrategy(), key);
    }


    /**
     * 创建场景结果
     *
     * @param session
     * @param req
     */
    public void onCreateSceneResult(Session session, PbScene.S2LCreateSceneResult req) {
        int loadBalanceServerID = req.getLoadBalanceServerID();
        int sceneServerID = req.getSceneServerID();
        int sceneWorldIndex = req.getSceneWorldIndex();
        int sceneStrategy = req.getSceneStrategy();
        int sceneID = req.getSceneID();
        long key = req.getKey();
        boolean result = req.getResult();
        int limitNum = req.getLimitNum();

        LoggerTool.systemLogger.info("onCreateSceneResult loadBalanceServerID={} serverID={}, sceneWorldIndex={}, sceneStrategy={},sceneID={}, key={}, result={},limitNum={}",
                loadBalanceServerID, sceneServerID, sceneWorldIndex, sceneStrategy, sceneID, key, result, limitNum);
        if (ConfigConst.identifier == loadBalanceServerID) {
            LongObjectMap<SceneInfo> map = getSceneStrategyMap(sceneServerID, sceneWorldIndex, sceneStrategy);
            SceneInfo sceneInfo = map.get(key);
            if (sceneInfo == null) {
                LoggerTool.systemLogger.error("createSceneResult sceneInfo is null sceneWorldIndex={},sceneStrategy={},key={}",
                        sceneWorldIndex, sceneStrategy, key);
                return;
            }
            int instanceID = req.getInstanceID();
            if (result) {
                sceneInfo.setInstanceID(instanceID);
                sceneInfo.setLimitNum(limitNum);
                ObjectList<SceneInfo.PreCreateWaitingInfo> preCreateList = sceneInfo.getPreCreateList();
                for (int i = 0; i < preCreateList.size(); i++) {
                    SceneInfo.PreCreateWaitingInfo preCreateWaitingInfo = preCreateList.get(i);
                    sendSceneInfo(preCreateWaitingInfo.getSession(), preCreateWaitingInfo.getPlayerID(), preCreateWaitingInfo.getLogicWorldIndex(),
                            serverInfoMap.get(sceneServerID), sceneWorldIndex, sceneInfo, true);
                }
            } else {
                //创建场景失败
                LoggerTool.systemLogger.error("onCreateSceneResult fail sceneWorldIndex={},sceneStrategy={},sceneID={},key={}",
                        sceneWorldIndex, sceneStrategy, sceneID, key);

                map.remove(key);
            }
        } else if (result) {
            //别的负载均衡服创建的场景，直接增加
            LongObjectMap<SceneInfo> map = getSceneStrategyMap(sceneServerID, sceneWorldIndex, sceneStrategy);
            SceneInfo sceneInfo = new SceneInfo();
            sceneInfo.setSceneID(sceneID);
            sceneInfo.setInstanceID(req.getInstanceID());
            map.put(key, sceneInfo);
        }
    }

    /**
     * 删除场景通知
     *
     * @param session
     * @param req
     */
    public void onRemoveScene(Session session, PbScene.S2LRemoveScene req) {
        int sceneServerID = req.getSceneServerID();
        int sceneWorldIndex = req.getSceneWorldIndex();
        int sceneStrategy = req.getSceneStrategy();
        long key = req.getKey();
        LoggerTool.systemLogger.info("onRemoveScene serverID={}, sceneWorldIndex={}, sceneStrategy={}, key={}",
                sceneServerID, sceneWorldIndex, sceneStrategy, key);
        LongObjectMap<SceneInfo> map = getSceneStrategyMap(sceneServerID, sceneWorldIndex, sceneStrategy);
        if (map != null) {
            map.remove(key);
        }
    }

    /**
     * 刷新场景数据
     * @param session
     * @param req
     */
    public void onRefreshSceneInfo(Session session, PbScene.S2LRefreshSceneInfo req) {
        int sceneServerID = req.getSceneServerID();
        int sceneWorldIndex = req.getSceneWorldIndex();
        int sceneStrategy = req.getSceneStrategy();
        long key = req.getKey();
        boolean isAdd = req.getIsAdd();
        long playerID = req.getPlayerID();
        LoggerTool.systemLogger.info("onRefreshSceneInfo serverID={}, sceneWorldIndex={}, sceneStrategy={}, key={}, isAdd={}, playerID={}",
                sceneServerID, sceneWorldIndex, sceneStrategy, key, isAdd, playerID);
        LongObjectMap<SceneInfo> map = getSceneStrategyMap(sceneServerID, sceneWorldIndex, sceneStrategy);
        SceneInfo sceneInfo = map.get(key);
        if (isAdd) {
            sceneInfo.getPlayers().add(playerID);
            sceneInfo.removePreCreateInfo(playerID);
        } else {
            sceneInfo.getPlayers().remove(playerID);
        }

    }

    /**
     * 获取工会场景
     *
     * @param result
     * @param unionID
     */
    protected void getUnionScene(FindSceneInfoResult result, long unionID) {

    }

    /**
     * 获取家园场景
     *
     * @param result
     * @param homePlayerID homePlayerID
     */
    protected void getHomeScene(FindSceneInfoResult result, long homePlayerID) {

    }

    /**
     * 获取分线场景
     *
     * @param sceneID
     * @param lineID
     * @return
     */
    protected void getLineScene(FindSceneInfoResult result, int sceneID, int lineID) {

    }

    /**
     * 获取分线场景
     *
     * @param sceneID
     * @return
     */
    protected void getLineScene(FindSceneInfoResult result, int sceneID) {

    }

    /**
     * 获取副本场景
     * 副本类场景只有一个人能创建，创建成功后再通知其他人进入
     *
     * @param dungeonInstanceID
     * @return
     */
    protected void getDungeonScene(FindSceneInfoResult result, int dungeonType, int dungeonInstanceID) {

    }

    /**
     * 获取指定子世界策略的场景信息, 如果不存在则创建
     *
     * @param serverID
     * @param sceneWorldIndex
     * @param sceneStrategy
     * @return 必定非空
     */
    private LongObjectMap<SceneInfo> getSceneStrategyMap(int serverID, int sceneWorldIndex, int sceneStrategy) {
        SceneServerInfo sceneServerInfo = serverInfoMap.get(serverID);
        if (sceneServerInfo == null) {
            LoggerTool.systemLogger.error("getSceneStrategyMap sceneServerInfo is null serverID={}, sceneWorldIndex={}, sceneStrategy={}", serverID, sceneWorldIndex, sceneStrategy);
            return null;
        }
        SceneChildWorldInfo sceneChildWorldInfo = sceneServerInfo.getChildWorldInfos().get(sceneWorldIndex);
        if (sceneChildWorldInfo == null) {
            LoggerTool.systemLogger.error("getSceneStrategyMap sceneChildWorldInfo is null serverID={}, sceneWorldIndex={}, sceneStrategy={}", serverID, sceneWorldIndex, sceneStrategy);
            return null;
        }
        LongObjectMap<SceneInfo> map = sceneChildWorldInfo.getSceneStrategyMap(sceneStrategy);
        if (map == null) {
            sceneChildWorldInfo.initSceneStrategy(sceneStrategy);
            map = sceneChildWorldInfo.getSceneStrategyMap(sceneStrategy);
        }
        return map;
    }

    /**
     * 找一个玩家人数最少的服务器
     *
     * @param sceneFeature
     * @return
     */
    protected SceneServerInfo getLeastPlayerNumServer(int sceneFeature) {
        var ref = new Object() {
            private SceneServerInfo serverInfo = null;
            private int min = Integer.MAX_VALUE;
        };

        serverInfoMap.foreachImmutable((serverID, info) -> {
            if (!isAvailableServer(info, sceneFeature)) {
                return;
            }
            int num = info.getPlayerNum();
            if (num < ref.min) {
                ref.min = num;
                ref.serverInfo = info;
            }
        });
        return ref.serverInfo;
    }

    /**
     * 判断服务器是否可用
     *
     * @param sceneServerInfo
     * @param sceneFeature
     * @return
     */
    protected boolean isAvailableServer(SceneServerInfo sceneServerInfo, int sceneFeature) {
        if (!sceneServerInfo.isActive()) {
            return false;
        }
        if (!sceneServerInfo.getSceneFeatures().contains(sceneFeature)) {
            return false;
        }
        return true;
    }
}
