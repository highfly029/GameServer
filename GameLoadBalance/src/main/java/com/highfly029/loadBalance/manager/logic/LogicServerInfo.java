package com.highfly029.loadBalance.manager.logic;

/**
 * @ClassName LogicServerInfo
 * @Description LogicServerInfo
 * @Author liyunpeng
 **/
public class LogicServerInfo {
    /**
     * 状态 测试状态->正式状态->不可进入状态
     */
    private int state;
    /**
     * 游戏服务器ID
     */
    private int serverID;
    /**
     * 客户端连接host
     */
    private String clientHost;
    /**
     * 客户端连接端口
     */
    private int clientPort;
    /**
     * 服务器是否可用
     */
    private boolean active;
    /**
     * 玩家数量
     */
    private int playerNum;

    public int getServerID() {
        return serverID;
    }

    public void setServerID(int serverID) {
        this.serverID = serverID;
    }

    public String getClientHost() {
        return clientHost;
    }

    public void setClientHost(String clientHost) {
        this.clientHost = clientHost;
    }

    public int getClientPort() {
        return clientPort;
    }

    public void setClientPort(int clientPort) {
        this.clientPort = clientPort;
    }

    public int getPlayerNum() {
        return playerNum;
    }

    public void setPlayerNum(int playerNum) {
        this.playerNum = playerNum;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
