package com.highfly029.loadBalance.manager.scene;

import com.highfly029.loadBalance.manager.scene.SceneChildWorldInfo;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName ServerInfo
 * @Description 服务器信息
 * @Author liyunpeng
 **/
public class SceneServerInfo {
    /**
     * 状态 测试状态->正式状态->不可进入状态
     */
    private int state;
    /**
     * 游戏服务器ID
     */
    private int serverID;
    /**
     * 虚拟机最大内存
     */
    private long maxMemory;
    /**
     * 虚拟机全部内存
     */
    private long totalMemory;
    /**
     * 虚拟机空闲内存
     */
    private long freeMemory;
    /**
     * 客户端连接host
     */
    private String clientHost;
    /**
     * 客户端连接端口
     */
    private int clientPort;
    /**
     * 支持的场景功能
     */
    private IntSet sceneFeatures = new IntSet();
    /**
     * 每个子世界对应的信息
     * key=childIndex
     */
    private IntObjectMap<SceneChildWorldInfo> childWorldInfos = new IntObjectMap<>(SceneChildWorldInfo[]::new);
    /**
     * 服务器是否可用
     */
    private boolean active;

    /**
     * 获取服务器所有的玩家数量
     *
     * @return
     */
    public int getPlayerNum() {
        var ref = new Object() {
            private int num = 0;
        };
        childWorldInfos.foreachMutable((k, v) -> {
            ref.num += v.getPlayerNum();
        });
        return ref.num;
    }

    /**
     * 获取人数最少的子世界索引
     * @return
     */
    public int getLeastPlayerNumWorldIndex() {
        var ref = new Object() {
            private int worldIndex;
            private int min = Integer.MAX_VALUE;
        };
        childWorldInfos.foreachMutable((k, v) -> {
            int num = v.getPlayerNum();
            if (num < ref.min) {
                ref.min = num;
                ref.worldIndex = k;
            }
        });
        return ref.worldIndex;
    }

    /**
     * 初始化子世界
     *
     * @param worldIndexList
     */
    public void initChildWorld(IntList worldIndexList) {
        for (int i = 0; i < worldIndexList.size(); i++) {
            int worldIndex = worldIndexList.get(i);
            SceneChildWorldInfo sceneChildWorldInfo = new SceneChildWorldInfo();
            childWorldInfos.put(worldIndex, sceneChildWorldInfo);
        }
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getServerID() {
        return serverID;
    }

    public void setServerID(int serverID) {
        this.serverID = serverID;
    }

    public long getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(long maxMemory) {
        this.maxMemory = maxMemory;
    }

    public long getTotalMemory() {
        return totalMemory;
    }

    public void setTotalMemory(long totalMemory) {
        this.totalMemory = totalMemory;
    }

    public long getFreeMemory() {
        return freeMemory;
    }

    public void setFreeMemory(long freeMemory) {
        this.freeMemory = freeMemory;
    }

    public String getClientHost() {
        return clientHost;
    }

    public void setClientHost(String clientHost) {
        this.clientHost = clientHost;
    }

    public int getClientPort() {
        return clientPort;
    }

    public void setClientPort(int clientPort) {
        this.clientPort = clientPort;
    }

    public IntSet getSceneFeatures() {
        return sceneFeatures;
    }

    public void setSceneFeatures(IntSet sceneFeatures) {
        this.sceneFeatures = sceneFeatures;
    }

    public IntObjectMap<SceneChildWorldInfo> getChildWorldInfos() {
        return childWorldInfos;
    }

    public void setChildWorldInfos(IntObjectMap<SceneChildWorldInfo> childWorldInfos) {
        this.childWorldInfos = childWorldInfos;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}

