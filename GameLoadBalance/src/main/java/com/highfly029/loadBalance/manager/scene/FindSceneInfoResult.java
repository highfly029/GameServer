package com.highfly029.loadBalance.manager.scene;

import com.highfly029.core.session.Session;

/**
 * @ClassName FindSceneInfoResult
 * @Description 查找场景信息的结果
 * @Author liyunpeng
 **/
public class FindSceneInfoResult {
    /**
     * 场景功能
     */
    private int sceneFeature;
    /**
     * 场景分配策略
     */
    private int sceneStrategy;
    /**
     * 场景id
     */
    private int sceneID;

    /**
     * 是否找到
     */
    private boolean isFind;
    /**
     * 是否需要预创建
     */
    private boolean preCreate;
    /**
     * 服务器信息
     */
    private SceneServerInfo sceneServerInfo;
    /**
     * 场景子世界索引
     */
    private int sceneWorldIndex;
    /**
     * 场景信息
     */
    private SceneInfo sceneInfo;
    /**
     * 连接
     */
    private Session session;
    /**
     * 请求的玩家
     */
    private long playerID;
    /**
     * 玩家所在的逻辑服世界索引
     */
    private int logicWorldIndex;

    public FindSceneInfoResult(int sceneFeature, int sceneStrategy, int sceneID, Session session, long playerID, int logicWorldIndex) {
        this.sceneFeature = sceneFeature;
        this.sceneStrategy = sceneStrategy;
        this.sceneID = sceneID;
        this.session = session;
        this.playerID = playerID;
        this.logicWorldIndex = logicWorldIndex;
    }

    public int getSceneID() {
        return sceneID;
    }

    public int getSceneFeature() {
        return sceneFeature;
    }

    public int getSceneStrategy() {
        return sceneStrategy;
    }

    public boolean isFind() {
        return isFind;
    }

    public void setFind(boolean find) {
        isFind = find;
    }

    public boolean isPreCreate() {
        return preCreate;
    }

    public void setPreCreate(boolean preCreate) {
        this.preCreate = preCreate;
    }

    public SceneServerInfo getSceneServerInfo() {
        return sceneServerInfo;
    }

    public void setSceneServerInfo(SceneServerInfo sceneServerInfo) {
        this.sceneServerInfo = sceneServerInfo;
    }

    public int getSceneWorldIndex() {
        return sceneWorldIndex;
    }

    public void setSceneWorldIndex(int sceneWorldIndex) {
        this.sceneWorldIndex = sceneWorldIndex;
    }

    public SceneInfo getSceneInfo() {
        return sceneInfo;
    }

    public void setSceneInfo(SceneInfo sceneInfo) {
        this.sceneInfo = sceneInfo;
    }

    public Session getSession() {
        return session;
    }

    public long getPlayerID() {
        return playerID;
    }

    public int getLogicWorldIndex() {
        return logicWorldIndex;
    }
}
