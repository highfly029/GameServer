package com.highfly029.loadBalance.manager;

import com.highfly029.core.manager.BaseManager;
import com.highfly029.core.world.WorldThread;
import com.highfly029.loadBalance.manager.scene.AbsSceneStrategy;

/**
 * @ClassName LoadBalanceSceneManager
 * @Description LoadBalanceSceneManager
 * @Author liyunpeng
 **/
public class LoadBalanceSceneManager extends BaseManager {
    /**
     * 场景服策略
     */
    private AbsSceneStrategy sceneStrategy;

    public LoadBalanceSceneManager(WorldThread world, AbsSceneStrategy sceneStrategy) {
        super(world);
        this.sceneStrategy = sceneStrategy;
    }

    public AbsSceneStrategy getSceneStrategy() {
        return sceneStrategy;
    }

    /************ 使用BlockingWaitStrategy策略不会触发update相关更新 ************/

    @Override
    public void onTick(int escapedMillTime) {
    }

    @Override
    public void onSecond() {
    }

    @Override
    public void onMinute() {
    }

    @Override
    public void onShutDown() {

    }
}
