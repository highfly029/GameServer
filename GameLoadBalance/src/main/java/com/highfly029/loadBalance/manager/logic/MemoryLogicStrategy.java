package com.highfly029.loadBalance.manager.logic;

import com.highfly029.common.data.base.Location;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.core.session.Session;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.loadBalance.world.LoadBalanceGlobalWorld;
import com.highfly029.utils.ConfigPropUtils;

/**
 * @ClassName MemoryLogicStrategy
 * @Description MemoryLogicStrategy
 * @Author liyunpeng
 **/
public class MemoryLogicStrategy extends AbsLogicStrategy {

    public MemoryLogicStrategy(LoadBalanceGlobalWorld globalWorld) {
        super(globalWorld);
    }

    @Override
    public void onPushPlayerLocationState(long accountID, long playerID, Location location, boolean isNewPlayer) {
        super.onPushPlayerLocationState(accountID, playerID, location, isNewPlayer);
        //登出移除，其他都更新
        if (location.getState() == Location.LOGOUT) {
            accountLocationMap.remove(accountID);
            playerLocationMap.remove(playerID);
        } else {
            accountLocationMap.put(accountID, location);
            playerLocationMap.put(playerID, location);
        }
    }

    @Override
    public LogicServerInfo selectLogicServer(long accountID,int sourceLogicID) {
        Location preLocation = accountLocationMap.get(accountID);
        //顶号先踢出之前的账号
        if (preLocation != null) {
            kickOut(accountID);
        }
        if (sourceLogicID > 0) {
            //源logicID转换成当前logicID
            int logicID = ConfigPropUtils.getIntValue(String.valueOf(sourceLogicID));
            return serverInfoMap.get(logicID);
        } else {
            //新账号
            return selectMinPlayerServer();
        }
    }

    @Override
    public void kickOut(long accountID) {
        Location preLocation = accountLocationMap.get(accountID);
        if (preLocation == null) {
            LoggerTool.systemLogger.warn("kickOut preLocation is null accountID={}", accountID);
            return;
        }
        LoggerTool.systemLogger.info("kickOut accountID={}, playerID={}, serverID={}, worldIndex={}",
                accountID, preLocation.getPlayerID(), preLocation.getLogicServerID(), preLocation.getLogicWorldIndex());

        Session session = globalWorld.getSessionTool().getSession(SessionType.Logic, preLocation.getLogicServerID());
        if (session != null) {
            PbLoadBalance.L2SKickOutPlayer.Builder builder = PbLoadBalance.L2SKickOutPlayer.newBuilder();
            builder.setLogicWorldIndex(preLocation.getLogicWorldIndex());
            builder.setPlayerID(preLocation.getPlayerID());
            session.sendMsgS2S(PtCode.L2SKickOutPlayer, builder.build());
        } else {
            LoggerTool.systemLogger.error("selectLogicServer session is null serverID={}", preLocation.getLogicServerID());
        }
    }
}
