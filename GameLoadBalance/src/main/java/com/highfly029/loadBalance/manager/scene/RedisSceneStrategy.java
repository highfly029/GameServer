package com.highfly029.loadBalance.manager.scene;

import com.highfly029.loadBalance.world.LoadBalanceGlobalWorld;

/**
 * @ClassName RedisSceneStrategy
 * @Description redis型策略，多个LoadBalance服时可用
 * @Author liyunpeng
 **/
public class RedisSceneStrategy extends AbsSceneStrategy {

    public RedisSceneStrategy(LoadBalanceGlobalWorld globalWorld) {
        super(globalWorld);
    }
}
