package com.highfly029.loadBalance.manager.scene;

import com.highfly029.common.data.scene.SceneStrategyEnum;
import com.highfly029.utils.collection.LongObjectMap;

/**
 * @ClassName SceneChildWorldInfo
 * @Description 场景子世界信息
 * @Author liyunpeng
 **/
public class SceneChildWorldInfo {
    /**
     * 子世界场景集合
     * 家园场景key=playerID
     * 工会场景key=unionID
     * 分线场景key=sceneID << 32 | lineID
     * 副本场景key=dungeonType << 32 | instance
     */
    private LongObjectMap<SceneInfo>[] sceneMapArray = new LongObjectMap[SceneStrategyEnum.values().length];


    /**
     * 获取分线场景策略集合
     * @param sceneStrategy
     * @return
     */
    public LongObjectMap<SceneInfo> getSceneStrategyMap(int sceneStrategy) {
        return sceneMapArray[sceneStrategy];
    }

    /**
     * 初始化
     * @param sceneStrategy
     */
    public void initSceneStrategy(int sceneStrategy) {
        if (sceneMapArray[sceneStrategy] == null) {
            sceneMapArray[sceneStrategy] = new LongObjectMap<>(SceneInfo[]::new);
        }
    }
    /**
     * 获取当前子世界所有玩家数量
     *
     * @return
     */
    public int getPlayerNum() {
        var ref = new Object() {
            private int num = 0;
        };
        for (int i = 0; i < sceneMapArray.length; i++) {
            LongObjectMap<SceneInfo> map = sceneMapArray[i];
            if (map != null) {
                map.foreachImmutable((k, v) -> {
                    ref.num += v.getPlayerNum();
                });
            }
        }
        return ref.num;
    }
}
