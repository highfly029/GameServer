package com.highfly029.loadBalance.tcpHandler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.loadBalance.world.LoadBalanceGlobalWorld;

/**
 * @ClassName BrokerGlobalHandler
 * @Description broker
 * @Author liyunpeng
 **/
public class BrokerGlobalHandler implements IRegisterServerGlobalProtocol {
    private LoadBalanceGlobalWorld loadBalanceGlobalWorld;

    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {
        loadBalanceGlobalWorld = (LoadBalanceGlobalWorld) globalWorld;
        globalWorld.registerServerGlobalProtocol(PtCode.Scene2LogicMessage, this::scene2LogicMessage);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2SceneMessage, this::logic2SceneMessage);
        globalWorld.registerServerGlobalProtocol(PtCode.Scene2SceneMessage, this::scene2SceneMessage);
        globalWorld.registerServerGlobalProtocol(PtCode.Logic2LogicMessage, this::logic2LogicMessage);
    }

    /**
     * 场景服发送到逻辑服消息
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void scene2LogicMessage(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbLoadBalance.Scene2LogicMessage message = PbLoadBalance.Scene2LogicMessage.parseFrom(packet.getData());
        int to = message.getTo();
        Session logicSession = loadBalanceGlobalWorld.getSessionTool().getSession(SessionType.Logic, to);
        if (logicSession != null) {
            logicSession.sendMsgS2S(PtCode.LoadBalance2LogicMessage, message);
        } else {
            //没有连接直接丢弃消息
            LoggerTool.systemLogger.error("scene2LogicMessage from={},to={},ptCode={},playerID={} session is null", message.getFrom(), to, ptCode, message.getPlayerID());
        }
    }

    /**
     * 逻辑服发送到场景服消息
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void logic2SceneMessage(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbLoadBalance.Logic2SceneMessage message = PbLoadBalance.Logic2SceneMessage.parseFrom(packet.getData());
        int to = message.getTo();
        Session sceneSession = loadBalanceGlobalWorld.getSessionTool().getSession(SessionType.Scene, to);
        if (sceneSession != null) {
            sceneSession.sendMsgS2S(PtCode.LoadBalance2SceneMessage, message);
        } else {
            //没有连接直接丢弃消息
            LoggerTool.systemLogger.error("logic2SceneMessage from={},to={},ptCode={},playerID={} session is null", message.getFrom(), to, ptCode, message.getPlayerID());
        }
    }

    /**
     * 逻辑服发送到逻辑服消息
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void logic2LogicMessage(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbLoadBalance.Logic2LogicMessage message = PbLoadBalance.Logic2LogicMessage.parseFrom(packet.getData());
        int to = message.getTo();
        Session logicSession = loadBalanceGlobalWorld.getSessionTool().getSession(SessionType.Logic, to);
        if (logicSession != null) {
            logicSession.sendMsgS2S(PtCode.LoadBalance2LogicMessage, message);
        } else {
            //没有连接直接丢弃消息
            LoggerTool.systemLogger.error("logic2LogicMessage from={},to={},ptCode={},playerID={} session is null", message.getFrom(), to, ptCode, message.getPlayerID());
        }
    }

    /**
     * 场景服发送到场景服消息
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void scene2SceneMessage(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbLoadBalance.Scene2SceneMessage message = PbLoadBalance.Scene2SceneMessage.parseFrom(packet.getData());
        int to = message.getTo();
        Session sceneSession = loadBalanceGlobalWorld.getSessionTool().getSession(SessionType.Scene, to);
        if (sceneSession != null) {
            sceneSession.sendMsgS2S(PtCode.LoadBalance2SceneMessage, message);
        } else {
            //没有连接直接丢弃消息
            LoggerTool.systemLogger.error("scene2SceneMessage from={},to={},ptCode={},playerID={} session is null", message.getFrom(), to, ptCode, message.getPlayerID());
        }
    }

}
