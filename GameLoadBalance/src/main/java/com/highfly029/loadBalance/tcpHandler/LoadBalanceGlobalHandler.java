package com.highfly029.loadBalance.tcpHandler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.base.Location;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.loadBalance.manager.LoadBalanceLogicManager;
import com.highfly029.loadBalance.manager.LoadBalanceSceneManager;
import com.highfly029.loadBalance.world.LoadBalanceGlobalWorld;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName LoadBalanceGlobalHandler
 * @Description 负载均衡
 * @Author liyunpeng
 **/
public class LoadBalanceGlobalHandler implements IRegisterServerGlobalProtocol {
    private LoadBalanceGlobalWorld loadBalanceGlobalWorld;

    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {
        loadBalanceGlobalWorld = (LoadBalanceGlobalWorld) globalWorld;
        globalWorld.registerServerGlobalProtocol(PtCode.S2LPushServerInfo, this::onPushServerInfo);
        globalWorld.registerServerGlobalProtocol(PtCode.S2LPushSceneChildWorldInfo, this::onPushSceneChildWorldInfo);
        globalWorld.registerServerGlobalProtocol(PtCode.S2LPushPlayerLocationState, this::onPushPlayerLocationState);
        globalWorld.registerServerGlobalProtocol(PtCode.S2LGetSceneServerInfo, this::getSceneServerInfo);
        globalWorld.registerServerGlobalProtocol(PtCode.S2LCreateSceneResult, this::createSceneResult);
        globalWorld.registerServerGlobalProtocol(PtCode.S2LRemoveScene, this::removeScene);
        globalWorld.registerServerGlobalProtocol(PtCode.S2LRefreshSceneInfo, this::refreshSceneInfo);
        globalWorld.registerServerGlobalProtocol(PtCode.L2LPushOfflineMsg, this::pushOfflineMsg);
    }

    /**
     * GameScene推送过来的服务器信息
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void onPushServerInfo(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbLoadBalance.S2LPushServerInfo req = PbLoadBalance.S2LPushServerInfo.parseFrom(packet.getData());
        if (req.getType() == SessionType.Scene) {
            IntSet sceneFeatures = new IntSet();
            int num;
            if ((num = req.getSceneFeaturesCount()) > 0) {
                for (int i = 0; i < num; i++) {
                    sceneFeatures.add(req.getSceneFeatures(i));
                }
            }
            IntList worldIndexList = new IntList();
            for (int i = 0; i < req.getWorldIndexCount(); i++) {
                worldIndexList.add(req.getWorldIndex(i));
            }
            LoadBalanceSceneManager manager = loadBalanceGlobalWorld.getLoadBalanceSceneManager();
            manager.getSceneStrategy().addSceneServerInfo(req.getIdentifier(), req.getClientHost(), req.getClientPort(), sceneFeatures, worldIndexList);
        } else if (req.getType() == SessionType.Logic) {
            LoadBalanceLogicManager manager = loadBalanceGlobalWorld.getLoadBalanceLogicManager();
            manager.getLogicStrategy().addLogicServerInfo(req.getIdentifier(), req.getClientHost(), req.getClientPort(), req.getPlayerNum());
        } else {
            LoggerTool.systemLogger.error("onPushServerInfo not support type={}", req.getType());
        }
    }

    /**
     * GameScene推送过来的场景子世界信息
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void onPushSceneChildWorldInfo(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        LoadBalanceSceneManager manager = loadBalanceGlobalWorld.getLoadBalanceSceneManager();
        manager.getSceneStrategy().addSceneChildWorldInfo();
    }

    /**
     * 玩家登陆
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void onPushPlayerLocationState(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        LoadBalanceLogicManager manager = loadBalanceGlobalWorld.getLoadBalanceLogicManager();
        PbLoadBalance.S2LPushPlayerLocationState req = PbLoadBalance.S2LPushPlayerLocationState.parseFrom(packet.getData());
        manager.getLogicStrategy().onPushPlayerLocationState(req.getAccountID(), req.getPlayerID(), PbCommonUtils.locationPb2Obj(req.getLocation()), req.getIsNewPlayer());
    }

    /**
     * 玩家向LoadBalance请求场景服信息
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void getSceneServerInfo(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        LoadBalanceSceneManager manager = loadBalanceGlobalWorld.getLoadBalanceSceneManager();
        PbScene.S2LGetSceneServerInfo req = PbScene.S2LGetSceneServerInfo.parseFrom(packet.getData());
        manager.getSceneStrategy().getSceneServerInfo(session, req);
    }

    /**
     * 场景服返回的创建场景结果
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void createSceneResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        LoadBalanceSceneManager manager = loadBalanceGlobalWorld.getLoadBalanceSceneManager();
        PbScene.S2LCreateSceneResult req = PbScene.S2LCreateSceneResult.parseFrom(packet.getData());
        manager.getSceneStrategy().onCreateSceneResult(session, req);
    }

    /**
     * 删除场景通知
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void removeScene(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        LoadBalanceSceneManager manager = loadBalanceGlobalWorld.getLoadBalanceSceneManager();
        PbScene.S2LRemoveScene req = PbScene.S2LRemoveScene.parseFrom(packet.getData());
        manager.getSceneStrategy().onRemoveScene(session, req);
    }

    /**
     * 更新场景消息
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void refreshSceneInfo(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        LoadBalanceSceneManager manager = loadBalanceGlobalWorld.getLoadBalanceSceneManager();
        PbScene.S2LRefreshSceneInfo req = PbScene.S2LRefreshSceneInfo.parseFrom(packet.getData());
        manager.getSceneStrategy().onRefreshSceneInfo(session, req);
    }

    /**
     * 推送离线消息
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void pushOfflineMsg(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbLoadBalance.L2LPushOfflineMsg req = PbLoadBalance.L2LPushOfflineMsg.parseFrom(packet.getData());
        LoadBalanceLogicManager manager = loadBalanceGlobalWorld.getLoadBalanceLogicManager();
        Location location = manager.getLogicStrategy().getPlayerLocation(req.getPlayerID());
        if (location != null) {
            Session targetLogicSession = loadBalanceGlobalWorld.getSessionTool().getSession(SessionType.Logic, location.getLogicServerID());
            if (targetLogicSession != null) {
                targetLogicSession.sendMsgS2S(PtCode.L2LPushOfflineMsg, req);
            }
        }
    }
}
