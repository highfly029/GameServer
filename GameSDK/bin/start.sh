#!/bin/bash
basePath=`pwd`
mainClass="com.highfly029.sdk.world.SDKStarter"
vmoptionFile="jvm.vmoptions"
options=""
if [[ -f $vmoptionFile ]]; then
  for line in `cat $vmoptionFile`
  do
    if [[ ! $line =~ ^#.* ]]; then
      options="$options $line"
    fi
  done
fi
#netty 内存检测
nettyLeakDetection="-Dio.netty.leakDetectionLevel=paranoid"
logfile=stdout.log
infofile=server.info
if [[ -f $infofile && -f $logfile ]]; then
  name=`tail -n 1 $infofile`
  mv $logfile $logfile$name
fi

nohup java -server $options $nettyLeakDetection -cp "../conf/:../lib/*" $mainClass $basePath > $logfile 2>&1 &
echo $! >> $infofile
curDate=`date +%Y%m%d%H%M`
echo $curDate >> $infofile
