package com.highfly029.sdk.enums;

import com.highfly029.core.interfaces.IHttpPath;

/**
 * http路径
 */
public enum SDKPathEnum implements IHttpPath {
    /**
     * 注册
     */
    register("register"),

    /**
     * 登陆
     */
    login("login"),

    /**
     * 验证
     */
    verify("verify"),

    /**
     * 使缓存无效
     */
    invalidateCache("invalidateCache") {
        @Override
        public boolean isOneChildHandleOnly() {
            return false;
        }

        @Override
        public boolean isAllChildHandle() {
            return true;
        }
    },

    globalTest("globalTest") {
        @Override
        public boolean isOneChildHandleOnly() {
            return false;
        }

        @Override
        public boolean isAllChildHandle() {
            return false;
        }
    };

    private final String path;

    public String path() {
        return path;
    }

    SDKPathEnum(String path) {
        this.path = path;
    }

    /**
     * 是否只有一个child处理
     *
     * @return
     */
    @Override
    public boolean isOneChildHandleOnly() {
        return true;
    }

    /**
     * 是否所有child处理
     *
     * @return
     */
    @Override
    public boolean isAllChildHandle() {
        return false;
    }
}
