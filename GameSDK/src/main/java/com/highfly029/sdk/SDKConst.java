package com.highfly029.sdk;

import java.util.concurrent.TimeUnit;

import com.highfly029.core.world.GlobalWorld;

import io.vertx.mysqlclient.MySQLPool;

/**
 * @ClassName SDKConst
 * @Description SDKConst
 * @Author liyunpeng
 **/
public class SDKConst {
    /**
     * mySQLPool数据库连接
     */
    private static MySQLPool mySQLPool;
    /**
     * 登陆验证有效时间
     */
    public static final long LOGIN_VALID_TIME = TimeUnit.MINUTES.toMillis(5);


    /**
     * afterOnStartUp后初始化
     */
    public static void init(GlobalWorld globalWorld) {
        mySQLPool = globalWorld.getMysqlTool().getConnection("sdk_db");
    }

    public static MySQLPool getMySQLPool() {
        return mySQLPool;
    }
}
