package com.highfly029.sdk.world;

import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.DisruptorBuilder;
import com.highfly029.core.world.SleepingWaitExtendStrategy;
import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

/**
 * @ClassName SDKStarter
 * @Description SDKStarter
 * @Author liyunpeng
 **/
public class SDKStarter {
    public static void main(String[] args) {
        int childNum = 4;
        SDKGlobalWorld sdkGlobalWorld = new SDKGlobalWorld(childNum);
        WaitStrategy waitStrategy = new SleepingWaitExtendStrategy(sdkGlobalWorld);
        Disruptor disruptor = DisruptorBuilder.builder()
                .setPoolName("SDKGlobal")
                .setProducerType(ProducerType.MULTI)
                .setWorldThread(sdkGlobalWorld)
                .setWaitStrategy(waitStrategy)
                .build();

        WorldTool worldTool = WorldTool.getInstance();
        worldTool.setDisruptor(disruptor);
        worldTool.setGlobalWorld(sdkGlobalWorld);

        for (int i = 1; i <= childNum; i++) {
            SDKChildWorld sdkChildWorld = new SDKChildWorld(i);
            Disruptor childDisruptor = DisruptorBuilder.builder()
                    .setPoolName("SDKChild")
                    .setProducerType(ProducerType.SINGLE)
                    .setWorldThread(sdkChildWorld)
                    .setWaitStrategy(new BlockingWaitStrategy())
                    .setIndex(i)
                    .build();
            worldTool.addChildWorld(i, childDisruptor);
        }
        worldTool.start();
    }
}
