package com.highfly029.sdk.world;

import com.highfly029.core.interfaces.IHttpPath;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.sdk.SDKConst;
import com.highfly029.sdk.enums.SDKPathEnum;

/**
 * @ClassName SDKGlobalWorld
 * @Description SDKGlobalWorld
 * @Author liyunpeng
 **/
public class SDKGlobalWorld extends GlobalWorld {

    public SDKGlobalWorld(int childWorldNum) {
        super(true, childWorldNum, 500);
    }

    @Override
    protected void onStartUpSuccess() {
        super.onStartUpSuccess();
        SDKConst.init(this);
    }

    @Override
    protected IHttpPath getHttpPath(String path) {
        SDKPathEnum pathEnum = null;
        try {
            pathEnum = SDKPathEnum.valueOf(path);
        } catch (Exception exception) {
            LoggerTool.httpLogger.error("not exist path", exception);
        }
        return pathEnum;
    }
}
