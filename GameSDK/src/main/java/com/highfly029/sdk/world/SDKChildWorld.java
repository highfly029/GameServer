package com.highfly029.sdk.world;

import com.highfly029.core.world.ChildWorld;
import com.highfly029.utils.collection.ObjObjMap;

/**
 * @ClassName SDKChildWorld
 * @Description SDKChildWorld
 * @Author liyunpeng
 **/
public class SDKChildWorld extends ChildWorld {
    /**
     * 登陆key缓存
     */
    private final ObjObjMap<String, String> loginKeyCache = new ObjObjMap<>(String[]::new, String[]::new);

    public SDKChildWorld(int index) {
        super(index, 100);
    }

    public ObjObjMap<String, String> getLoginKeyCache() {
        return loginKeyCache;
    }
}
