package com.highfly029.sdk.httpHandler;

import java.util.function.BiConsumer;

import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.net.http.HttpConst;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.sdk.SDKConst;
import com.highfly029.sdk.enums.SDKPathEnum;
import com.highfly029.utils.JsonUtils;
import com.highfly029.utils.MD5Utils;
import com.highfly029.utils.collection.ObjObjMap;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.util.internal.StringUtil;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName VerifyHandler
 * @Description VerifyHandler
 * @Author liyunpeng
 **/
public class VerifyHandler extends SDKChildHttpHandler {

    @Override
    public String register() {
        return SDKPathEnum.verify.path();
    }

    /**
     * demo
     * http://localhost:8000/sdk/verify?userId=test4001&game_id=pwd4001&time=1234&token=abcd
     */
    @Override
    public void handler(Channel channel, FullHttpRequest request) {
        ObjObjMap<String, String> params = HttpUtils.getRequestParams(request);
        String userId = params.get("userId");
        String gameId = params.get("gameId");
        String time = params.get("time");
        String token = params.get("token");

        ObjObjMap<String, String> result = new ObjObjMap<>(String[]::new, String[]::new);

        if (StringUtil.isNullOrEmpty(userId)) {
            LoggerTool.httpLogger.error("verify handler userId is null {}", params);
            result.put("result", HttpConst.RESP_FAIL);
            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            return;
        }
        if (StringUtil.isNullOrEmpty(gameId)) {
            LoggerTool.httpLogger.error("verify handler gameId is null {}", params);
            result.put("result", HttpConst.RESP_FAIL);
            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            return;
        }
        if (StringUtil.isNullOrEmpty(time)) {
            LoggerTool.httpLogger.error("verify handler time is null {}", params);
            result.put("result", HttpConst.RESP_FAIL);
            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            return;
        }
        if (StringUtil.isNullOrEmpty(token)) {
            LoggerTool.httpLogger.error("verify handler token is null {}", params);
            result.put("result", HttpConst.RESP_FAIL);
            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            return;
        }

        long now = System.currentTimeMillis();
        long timestamp = Long.valueOf(time);
        if (now > timestamp + SDKConst.LOGIN_VALID_TIME) {
            LoggerTool.httpLogger.error("verify handler time is invalid {}", params);
            result.put("result", HttpConst.RESP_FAIL);
            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            return;
        }

        BiConsumer<Boolean, String> consumer = (ret, msg) -> {
            if (ret) {
                String loginKey = sdkChildWorld.getLoginKeyCache().get(gameId);
                if (StringUtil.isNullOrEmpty(loginKey)) {
                    LoggerTool.httpLogger.error("verify handler loginKey is null");
                    result.put("result", HttpConst.RESP_FAIL);
                    result.put("msg", "verify handler loginKey is null");
                    HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
                    return;
                }
                String tokenStr = "user_id=" + userId + "&game_id=" + gameId + "&login_key=" + loginKey + "&time=" + time;
                String md5 = MD5Utils.md5(tokenStr);
                if (token.equals(md5)) {
                    LoggerTool.httpLogger.info("verify handler success {}", params);
                    result.put("result", HttpConst.RESP_SUCCESS);
                } else {
                    LoggerTool.httpLogger.error("verify handler md5 failed {}", params);
                    result.put("result", HttpConst.RESP_FAIL);
                    result.put("msg", "verify handler md5 failed");
                }
            } else {
                result.put("result", HttpConst.RESP_FAIL);
                result.put("msg", msg);
            }
            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
        };

        Tuple placeholders = Tuple.of(userId);
        int index = sdkChildWorld.getIndex();
        MySQLUtils.sendChild(SDKConst.getMySQLPool(), "select user_id from user where user_id=?", placeholders, index, (isSuccess, rows, errorMsg) -> {
            if (isSuccess) {
                int size = rows.size();
                if (size == 1) {
                    loadLoginKeyCache(consumer);
                } else {
                    LoggerTool.httpLogger.error("verify handler not exist user_id size={}, {}", size, params);
                    result.put("result", HttpConst.RESP_FAIL);
                    result.put("msg", "not exist user_id");
                    HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
                }
            } else {
                LoggerTool.httpLogger.error("verify handler mysql connect failed {}", params);
                result.put("result", HttpConst.RESP_FAIL);
                result.put("msg", errorMsg);
                HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            }
        });
    }
}
