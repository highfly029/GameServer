package com.highfly029.sdk.httpHandler;

import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.net.NetUtils;
import com.highfly029.core.net.http.HttpConst;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.sdk.SDKConst;
import com.highfly029.sdk.enums.SDKPathEnum;
import com.highfly029.utils.JsonUtils;
import com.highfly029.utils.collection.ObjObjMap;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.util.internal.StringUtil;
import io.vertx.mysqlclient.MySQLClient;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName RegisterHandler
 * @Description RegisterHandler
 * @Author liyunpeng
 **/
public class RegisterHandler extends SDKChildHttpHandler {

    @Override
    public String register() {
        return SDKPathEnum.register.path();
    }

    /**
     * demo:
     * http://localhost/sdk/register?userName=test1&pwd=112233
     */
    @Override
    public void handler(Channel channel, FullHttpRequest request) {
        ObjObjMap<String, String> params = HttpUtils.getRequestParams(request);
        String userName = params.get("userName");
        String pwd = params.get("pwd");

        ObjObjMap<String, String> result = new ObjObjMap<>(String[]::new, String[]::new);
        if (StringUtil.isNullOrEmpty(userName)) {
            LoggerTool.httpLogger.error("register handler failed userName is null");
            result.put("result", HttpConst.RESP_FAIL);
            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            return;
        }
        if (StringUtil.isNullOrEmpty(pwd)) {
            LoggerTool.httpLogger.error("register handler failed pwd is null");
            result.put("result", HttpConst.RESP_FAIL);
            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            return;
        }
        Tuple tuple = Tuple.of(userName, pwd);

        int index = sdkChildWorld.getIndex();

        MySQLUtils.sendChild(SDKConst.getMySQLPool(), "select user_id from user where user_name=? and pwd=?", tuple, index, (isSuccess, rows, errorMsg) -> {
            if (isSuccess) {
                int size = rows.size();
                if (size == 0) {

                    Tuple userInfo = Tuple.of(userName, pwd, NetUtils.getIP(channel));
                    MySQLUtils.sendChild(SDKConst.getMySQLPool(), "INSERT INTO user (user_name, pwd, ip) VALUE(?, ?, ?)", userInfo, index, (isSuccess2, rows2, errorMsg2) -> {
                        if (isSuccess2) {
                            result.put("userName", userName);
                            long userId = rows2.property(MySQLClient.LAST_INSERTED_ID);
                            result.put("userId", String.valueOf(userId));
                            LoggerTool.httpLogger.info("register handler success userName={}, userId={}", userName, userId);
                            result.put("result", HttpConst.RESP_SUCCESS);
                            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
                        } else {
                            LoggerTool.httpLogger.error("register handler failed insert msg={}", errorMsg2);
                            result.put("result", HttpConst.RESP_FAIL);
                            result.put("msg", errorMsg2);
                            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
                        }
                    });
                } else if (size == 1) {
                    result.put("userName", userName);
                    long userId = rows.iterator().next().getLong(0);
                    result.put("userId", String.valueOf(userId));
                    result.put("registered", String.valueOf(true));
                    result.put("result", HttpConst.RESP_SUCCESS);
                    LoggerTool.httpLogger.error("register handler failed registered userName={}, userId={}", userName, userId);
                    HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
                } else {
                    LoggerTool.httpLogger.error("register handler failed more size msg={}", HttpConst.RESP_FAIL);
                    result.put("result", HttpConst.RESP_FAIL);
                    result.put("msg", "register handler failed more size msg={}");
                    HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
                }
            } else {
                LoggerTool.httpLogger.error("register handler failed select msg={}", errorMsg);
                result.put("result", HttpConst.RESP_FAIL);
                result.put("msg", errorMsg);
                HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            }
        });
    }
}
