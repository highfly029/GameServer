package com.highfly029.sdk.httpHandler;

/**
 * @ClassName TestConst
 * @Description TestConst
 * @Author liyunpeng
 **/
public class TestConst {
    //基础命中率
    public static final int BaseHitRate = 8888;
    //基础暴击率
    public static final int BaseCritical = 88;
}
