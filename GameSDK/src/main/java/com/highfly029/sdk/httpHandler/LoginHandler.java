package com.highfly029.sdk.httpHandler;

import java.util.function.BiConsumer;

import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.net.http.HttpConst;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.sdk.SDKConst;
import com.highfly029.sdk.enums.SDKPathEnum;
import com.highfly029.utils.JsonUtils;
import com.highfly029.utils.MD5Utils;
import com.highfly029.utils.collection.ObjObjMap;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.util.internal.StringUtil;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName LoginHandler
 * @Description LoginHandler
 * @Author liyunpeng
 **/
public class LoginHandler extends SDKChildHttpHandler {

    @Override
    public String register() {
        return SDKPathEnum.login.path();
    }

    /**
     * demo
     * http://localhost/sdk/login?userName=test10&pwd=112233&gameId=1001
     */
    @Override
    public void handler(Channel channel, FullHttpRequest request) {
        ObjObjMap<String, String> params = HttpUtils.getRequestParams(request);
        String userName = params.get("userName");
        String pwd = params.get("pwd");
        String gameId = params.get("gameId");
        ObjObjMap<String, String> result = new ObjObjMap<>(String[]::new, String[]::new);
        if (StringUtil.isNullOrEmpty(userName)) {
            LoggerTool.httpLogger.error("login handler userName is null {}", params);
            result.put("result", HttpConst.RESP_FAIL);
            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            return;
        }
        if (StringUtil.isNullOrEmpty(pwd)) {
            LoggerTool.httpLogger.error("login handler pwd is null {}", params);
            result.put("result", HttpConst.RESP_FAIL);
            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            return;
        }
        if (StringUtil.isNullOrEmpty(gameId)) {
            LoggerTool.httpLogger.error("login handler gameId is null {}", params);
            result.put("result", HttpConst.RESP_FAIL);
            HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            return;
        }

        Tuple placeholders = Tuple.of(userName, pwd);
        int index = sdkChildWorld.getIndex();
        MySQLUtils.sendChild(SDKConst.getMySQLPool(), "select user_id from user where user_name=? and pwd=?", placeholders, index, (isSuccess, rows, errorMsg) -> {
            if (isSuccess) {
                int size = rows.size();
                if (size == 1) {
                    int userId = rows.iterator().next().getInteger(0);
                    BiConsumer<Boolean, String> consumer = (ret, msg) -> {
                        if (ret) {
                            String loginKey = sdkChildWorld.getLoginKeyCache().get(gameId);
                            if (loginKey == null) {
                                result.put("result", HttpConst.RESP_FAIL);
                                result.put("msg", "login handler cant find loginKey");
                                HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
                                return;
                            }
                            long time = System.currentTimeMillis();
                            String tokenStr = "user_id=" + userId + "&game_id=" + gameId + "&login_key=" + loginKey + "&time=" + time;
                            String token = MD5Utils.md5(tokenStr);
                            LoggerTool.httpLogger.info("login handler success userId={}, gameId={}, tokenStr={}, token={}", userId, gameId, tokenStr, token);
                            result.put("userName", userName);
                            result.put("userId", String.valueOf(userId));
                            result.put("token", token);
                            result.put("time", String.valueOf(time));
                            result.put("result", HttpConst.RESP_SUCCESS);
                        } else {
                            result.put("result", HttpConst.RESP_FAIL);
                            result.put("msg", msg);
                        }
                        HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
                    };

                    loadLoginKeyCache(consumer);

                } else {
                    LoggerTool.httpLogger.error("login handler cant find user or pwd is error {}", params);
                    result.put("result", HttpConst.RESP_FAIL);
                    result.put("msg", "login handler cant find user or pwd is error {}");
                    HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
                }
            } else {
                LoggerTool.httpLogger.error("login handler failed select {}", params);
                result.put("result", HttpConst.RESP_FAIL);
                result.put("msg", errorMsg);
                HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            }
        });
    }
}
