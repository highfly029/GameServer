package com.highfly029.sdk.httpHandler;

import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.sdk.enums.SDKPathEnum;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @ClassName GlobalTestHandler
 * @Description GlobalTestHandler
 * @Author liyunpeng
 **/
public class GlobalTestHandler extends SDKGlobalHttpHandler {
    @Override
    public String register() {
        return SDKPathEnum.globalTest.path();
    }

    @Override
    public void handler(Channel channel, FullHttpRequest request) {
        LoggerTool.httpLogger.info("hotfix test");
        LoggerTool.httpLogger.info("GlobalTestHandler 基础命中率={},基础暴击率{}", TestConst.BaseCritical, TestConst.BaseHitRate);
        HttpUtils.sendHttpResponse(channel, "GlobalTestHandler success");
    }
}
