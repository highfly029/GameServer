package com.highfly029.sdk.httpHandler;

import com.highfly029.core.net.http.HttpConst;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.sdk.enums.SDKPathEnum;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @ClassName InvalidateCacheHandler
 * @Description InvalidateCacheHandler
 * @Author liyunpeng
 **/
public class InvalidateCacheHandler extends SDKChildHttpHandler {
    @Override
    public String register() {
        return SDKPathEnum.invalidateCache.path();
    }

    @Override
    public void handler(Channel channel, FullHttpRequest request) {
        //清空loginKey缓存下次重新加载
        sdkChildWorld.getLoginKeyCache().clear();
//        LoggerTool.httpLogger.info("invalidateCache success size={}", sdkChildWorld.getLoginKeyCache().size());
        HttpUtils.sendHttpResponse(channel, HttpConst.RESP_SUCCESS);
    }
}
