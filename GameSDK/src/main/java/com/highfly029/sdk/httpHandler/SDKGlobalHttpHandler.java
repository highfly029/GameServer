package com.highfly029.sdk.httpHandler;

import com.highfly029.core.net.http.BaseGlobalHttpHandler;
import com.highfly029.core.world.WorldThread;
import com.highfly029.sdk.world.SDKGlobalWorld;

/**
 * @ClassName BaseGlobalHttpHandler
 * @Description BaseGlobalHttpHandler
 * @Author liyunpeng
 **/
public abstract class SDKGlobalHttpHandler extends BaseGlobalHttpHandler {
    private SDKGlobalWorld sdkGlobalWorld;

    @Override
    public void init(WorldThread worldThread) {
        this.sdkGlobalWorld = (SDKGlobalWorld) worldThread;
    }

}
