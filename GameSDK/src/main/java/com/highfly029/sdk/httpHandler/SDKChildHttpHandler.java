package com.highfly029.sdk.httpHandler;

import java.util.function.BiConsumer;

import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.net.http.BaseChildHttpHandler;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.world.WorldThread;
import com.highfly029.sdk.SDKConst;
import com.highfly029.sdk.world.SDKChildWorld;

import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowIterator;

/**
 * @ClassName BaseHttpHandler
 * @Description BaseHttpHandler
 * @Author liyunpeng
 **/
public abstract class SDKChildHttpHandler extends BaseChildHttpHandler {
    protected SDKChildWorld sdkChildWorld;

    @Override
    public void init(WorldThread worldThread) {
        this.sdkChildWorld = (SDKChildWorld) worldThread;
    }

    /**
     * 加载loginKey的缓存并回调 如果有缓存直接回调
     *
     * @param consumer
     */
    protected void loadLoginKeyCache(BiConsumer<Boolean, String> consumer) {
        if (sdkChildWorld.getLoginKeyCache().size() == 0) {
            MySQLUtils.sendChild(SDKConst.getMySQLPool(), "select game_id, login_key from const", null, sdkChildWorld.getIndex(), (isSuccess, rows, errorMsg) -> {
                if (isSuccess) {
                    if (rows.size() > 0) {
                        RowIterator<Row> iterator = rows.iterator();
                        while (iterator.hasNext()) {
                            Row row = iterator.next();
                            String game_id = String.valueOf(row.getInteger(0));
                            String login_key = row.getString(1);
                            sdkChildWorld.getLoginKeyCache().put(game_id, login_key);
                        }
                        LoggerTool.httpLogger.info("load loginKeyCache success!");
                        consumer.accept(true, null);
                    } else {
                        consumer.accept(false, "loadCache failed size=0");
                    }
                } else {
                    consumer.accept(false, errorMsg);
                }
            });
        } else {
            consumer.accept(true, null);
        }
    }
}
