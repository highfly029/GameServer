package com.highfly029.utils;

import com.highfly029.utils.collection.ObjectList;

import org.junit.Test;

public class ACAutomatonTest {

    @Test
    public void test1() {
        ACAutomaton filter = new ACAutomaton();
        ObjectList<String> list = new ObjectList<>(String[]::new);
//        list.add("我");
//        list.add("我是");
//        list.add("我是人");
//        list.add("我是好人");
//        list.add("我是地球人");
//        list.add("我是好人啊");

        list.add("he");
        list.add("hers");

        list.add("his");
        list.add("hisabcd");
        list.add("erase");

        filter.init(list);

        String t = "dhersxhisabxhersa";

        System.out.println(filter.contain(t));
        System.out.println(filter.replace(t));
        System.out.println(filter.replaceGreed(t));

//        String t2 = "我是好人啊哈哈我是好人呢";
//        System.out.println(filter.replace(t2));
//        System.out.println(filter.replaceGreed(t2));
//        filter.bfsPrint();
    }

    @Test
    public void caseSensitiveTest() {
        char[] ignore = new char[]{};
        char[] caseArray = new char[]{'C'};
        ACAutomaton filter = new ACAutomaton(ignore, caseArray, '*');
        ObjectList<String> list = new ObjectList<>(String[]::new);

        list.add("C");

        filter.init(list);

        String t = "card";
        System.out.println(filter.contain(t));
        System.out.println(filter.replace(t));
    }

    @Test
    public void ignoreTest() {
        char[] ignore = new char[]{'-', ' ', '='};
        char[] caseArray = new char[]{};
        ACAutomaton filter = new ACAutomaton(ignore, caseArray, '*');
        ObjectList<String> list = new ObjectList<>(String[]::new);

        list.add("马");
        list.add("马克思");
        list.add("马克思主义");
        list.add("哲学");

        filter.init(list);

        String t = "我的 马- 克 =思 主-义哲学";
        System.out.println(filter.contain(t));
        System.out.println(filter.replace(t));
        System.out.println(filter.replaceGreed(t));
    }
}