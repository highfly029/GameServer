package com.highfly029.utils.collection;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IntIntMapTest {
    IntIntMap map = new IntIntMap();

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("map.size=" + map.size());
        System.out.println("capacity=" + map.getCapacity());
        System.out.println("freeValue=" + map.getFreeValue());
    }

    @Test
    public void testLarge() {
        for (int i = 0; i < 1000000; i++) {
            map.put(i, i);
        }
        for (int i = 0; i < 10000; i++) {
            map.remove(i);
        }
        System.out.println(map.get(99999));
    }

    @Test
    public void test() {
        System.out.println("put...");
        map.put(Integer.MAX_VALUE, Integer.MAX_VALUE);
        map.put(Integer.MAX_VALUE - 1, Integer.MAX_VALUE - 1);
        map.put(Integer.MAX_VALUE - 2, Integer.MAX_VALUE - 2);
        map.put(Integer.MAX_VALUE - 3, Integer.MAX_VALUE - 3);
        map.put(Integer.MAX_VALUE - 4, Integer.MAX_VALUE - 4);
        map.put(Integer.MIN_VALUE, Integer.MIN_VALUE);
        print();
        System.out.println("remove...");
        map.remove(Integer.MAX_VALUE - 2);
        map.remove(Integer.MAX_VALUE - 3);
        map.remove(Integer.MAX_VALUE - 4);
        print();
        System.out.println("putIfAbsent...");
        map.putIfAbsent(Integer.MAX_VALUE, Integer.MAX_VALUE);
        map.putIfAbsent(Integer.MAX_VALUE - 1, Integer.MAX_VALUE - 1);
        map.putIfAbsent(Integer.MAX_VALUE - 2, Integer.MAX_VALUE - 2);
        map.putIfAbsent(Integer.MAX_VALUE - 3, Integer.MAX_VALUE - 3);
        map.putIfAbsent(Integer.MAX_VALUE - 4, Integer.MAX_VALUE - 4);
        print();
        System.out.println("clear");
        map.clear();
        print();

    }

    private void print() {
        System.out.println(map);
//        System.out.println("==============print start================");
//        int freeValue = map.getFreeValue();
//        long[] table = map.getTable();
//        long entry;
//        int key;
//        int value;
//        for (int i = 0, len = table.length; i < len; i++) {
//            if ((key = (int) (entry = table[i])) != freeValue) {
//                value = (int) (entry >>> 32);
//                System.out.println("key="+key+" value="+value);
//            }
//        }
//        System.out.println("==============print end================");
    }

    @Test
    public void getCapacity() {
        test();
        test();
    }

    @Test
    public void getFreeValue() {
    }

    @Test
    public void getTable() {
    }

    @Test
    public void put() {
        map.put(1, 1);
        map.put(2, 2);
        map.put(3, 3);
        map.put(4, 4);
        System.out.println(map);
    }

    @Test
    public void addValue() {
        System.out.println(map.addValue(1, 1));
        System.out.println(map.addValue(1, 1));
        System.out.println(map.addValue(1, 1));
        System.out.println(map.addValue(1, 1));
        System.out.println(map.addValue(1, 1));
        print();
    }

    @Test
    public void contains() {
        System.out.println(map.contains(1));
        map.put(1, 10);
        print();
        System.out.println(map.contains(1));
    }

    @Test
    public void get() {
    }

    @Test
    public void getOrDefault() {
        System.out.println(map.getOrDefault(1, 2));
    }

    @Test
    public void remove() {
    }

    @Test
    public void clear() {
    }

    @Test
    public void reSizeCapacity() {
        map.reSizeCapacity(20);
    }

    @Test
    public void putIfAbsent() {

    }

    @Test
    public void foreachImmutable() {

    }

    @Test
    public void foreach() {
        for (int i = 0; i < 100; i++) {
            foreachMutable();
        }
    }

    @Test
    public void foreachMutable() {
        IntIntMap map = new IntIntMap();
        Random random = new Random();
        for (int i = 0; i < 100000; i++) {
            int key = random.nextInt();
            map.put(key, key);
        }
        int beforeSize = map.size();
        long beginTime = System.currentTimeMillis();
        IntIntMap intIntMap = new IntIntMap();
        LongSet removeSet = new LongSet();
        LongList list = new LongList();
        map.foreachMutable((k, v) -> {
            intIntMap.addValue(1, 1);
            list.add(1);
            if (k % 10 == 0) {
                map.remove(k);
                removeSet.add(k);
            }
        });

        LongSet keySet = new LongSet(map.size());
        map.foreachImmutable((k, v) -> keySet.add(k));
        if (removeSet.size() + map.size() != beforeSize) {
            System.out.println("----------");
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("map.size=" + map.size());
            System.out.println("before map size=" + beforeSize);
        }

        if (beforeSize != intIntMap.get(1)) {
            System.out.println("=================");
            System.out.println("before map size=" + beforeSize);
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("keySet size=" + keySet.size());
            System.out.println("map.size=" + map.size());
            System.out.println("list size=" + list.size());
            System.out.println("intIntMap value=" + intIntMap.get(1));
            long restTime = System.currentTimeMillis() - beginTime;
            System.out.println("time=" + restTime);
        }
    }

    @Test
    public void delete() {
        map.put(1, 100);
        map.put(2, 200);
        map.put(3, 300);
        map.put(4, 400);
        System.out.println("1,");
    }
}