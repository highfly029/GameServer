package com.highfly029.utils.collection;

import java.lang.reflect.Array;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ObjectListTest {
    ObjectList<String> list = new ObjectList<>(String[]::new);
    ObjectList<Array> arrayList = new ObjectList<>(Array[]::new);

    @Before
    public void setUp() throws Exception {
        for (int i = 0; i < 10; i++) {
            list.add("str" + i);
        }
        list.add(null);
        list.add(null);
        list.add(null);
    }

    @After
    public void tearDown() throws Exception {
        System.out.println(list);
    }

    @Test
    public void test() {

    }

    @Test
    public void sort() {
        ObjectList<Obj> list = new ObjectList<>(Obj[]::new);
        for (int i = 100; i > 80; i--) {
            Obj obj = new Obj();
            obj.a = i;
            obj.b = i;
            list.add(obj);
        }
        System.out.println(list);
        System.out.println("---sort---");
        list.sort(this::objCompare);
        System.out.println(list);
        System.out.println(list.get(0));
    }

    public int objCompare(Obj obj1, Obj obj2) {
        if (obj1.a > obj2.a) {
            return 1;
        } else if (obj1.a < obj2.a) {
            return -1;
        }
        return 0;
    }

    class Obj {
        int a;
        int b;

        @Override
        public String toString() {
            return "[" + a + "," + b + "]";
        }
    }


}