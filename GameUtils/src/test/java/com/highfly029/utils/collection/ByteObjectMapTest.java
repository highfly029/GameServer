package com.highfly029.utils.collection;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ByteObjectMapTest {
    class Vector3 {
        public float x;
        public float y;
        public float z;

        public Vector3 set(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
            return this;
        }
    }

    ByteObjectMap<String> map = new ByteObjectMap<>(String[]::new);
    ByteObjectMap<Vector3> vectorMap = new ByteObjectMap<>(Vector3[]::new);

    @Test
    public void testIntegerCache() {
        Integer int1 = 123;
        Integer int2 = 123;
        if (int1 == int2) {
            System.out.println("123 is same");
        }
        Integer int3 = 456;
        Integer int4 = 456;
        if (int3 == int4) {
            System.out.println("456 is same");
        }
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("size=" + map.size());
        System.out.println("capacity=" + map.getCapacity());
        System.out.println("freeValue=" + map.getFreeValue());
    }

    @Test
    public void testLarge() {
//        HashByteObjMap<Object> hashByteObjMaps = HashByteObjMaps.newMutableMap();
//        for (byte i = Byte.MIN_VALUE; i <= Byte.MAX_VALUE; i++) {
//            hashByteObjMaps.put(i, "aaa");
//        }
//        System.out.println("size="+hashByteObjMaps.size());
        for (byte i = Byte.MIN_VALUE; i < Byte.MAX_VALUE; i++) {
            map.put(i, "aaa");
        }
        for (byte i = Byte.MIN_VALUE; i < Byte.MAX_VALUE; i++) {
            map.put(i, "aaa");
        }
        for (byte i = Byte.MIN_VALUE; i < Byte.MAX_VALUE; i++) {
            map.put(i, "aaa");
        }
        for (byte i = Byte.MIN_VALUE; i < Byte.MAX_VALUE; i++) {
            map.put(i, "ccc");
        }
        System.out.println("aaa");
//        map.put(Byte.MAX_VALUE, "aaa");
//        System.out.println("bbb");
    }

    @Test
    public void test() {
        Vector3 v1 = new Vector3().set(1, 1, 1);
        Vector3 v2 = new Vector3().set(2, 1, 1);
        Vector3 v3 = new Vector3().set(3, 1, 1);
        vectorMap.put((byte) 1, v1);
        vectorMap.put((byte) 1, v1);
        vectorMap.put((byte) 1, v1);
        vectorMap.put((byte) 1, v1);
        System.out.println(vectorMap.get((byte) 1));
    }

    @Test
    public void getCapacity() {
    }

    @Test
    public void getFreeValue() {
    }

    @Test
    public void getKeys() {
    }

    @Test
    public void getValues() {
    }

    @Test
    public void put() {
    }

    @Test
    public void contains() {
    }

    @Test
    public void get() {
    }

    @Test
    public void getOrDefault() {
    }

    @Test
    public void remove() {
    }

    @Test
    public void clear() {
    }

    @Test
    public void reSizeCapacity() {
    }

    @Test
    public void putIfAbsent() {
    }

    @Test
    public void foreachImmutable() {

    }

    @Test
    public void foreach() {
        for (int i = 0; i < 100; i++) {
            foreachMutable();
        }
    }

    @Test
    public void foreachMutable() {
        ByteObjectMap<Vector3> map = new ByteObjectMap<>(Vector3[]::new);
        Random random = new Random();
        for (int i = 0; i < 500; i++) {
            byte key = (byte) random.nextInt();
            map.put(key, new Vector3().set(i, i, i));
        }
        int beforeSize = map.size();
        long beginTime = System.currentTimeMillis();
        IntIntMap intIntMap = new IntIntMap();
        LongSet removeSet = new LongSet();
        LongList list = new LongList();
        map.foreachMutable((k, v) -> {
            intIntMap.addValue(1, 1);
            list.add(1);
            v.x += 1;
            if (k % 10 == 0) {
                map.remove(k);
                removeSet.add(k);
            }
        });

        LongSet keySet = new LongSet(map.size());
        map.foreachImmutable((k, v) -> {
            keySet.add(k);
            int x = (int) v.x;
            int y = (int) v.y;
            if (x != y + 1) {
//                System.out.println("k="+k+" v="+v);
            }
        });
        if (removeSet.size() + map.size() != beforeSize) {
            System.out.println("----------");
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("map.size=" + map.size());
            System.out.println("before map size=" + beforeSize);
        }

        if (beforeSize != intIntMap.get(1)) {
            System.out.println("=================");
            System.out.println("before map size=" + beforeSize);
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("keySet size=" + keySet.size());
            System.out.println("map.size=" + map.size());
            System.out.println("list size=" + list.size());
            System.out.println("intIntMap value=" + intIntMap.get(1));
            long restTime = System.currentTimeMillis() - beginTime;
            System.out.println("time=" + restTime);
        }
    }
}