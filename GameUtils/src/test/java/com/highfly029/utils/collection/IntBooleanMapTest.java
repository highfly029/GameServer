package com.highfly029.utils.collection;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IntBooleanMapTest {
    IntBooleanMap map = new IntBooleanMap();

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("map.size=" + map.size());
        System.out.println("capacity=" + map.getCapacity());
        System.out.println("freeValue=" + map.getFreeValue());
    }

    @Test
    public void testLarge() {
        for (int i = 0; i < 1000000; i++) {
            map.put(i, true);
        }
        for (int i = 0; i < 10000; i++) {
            map.remove(i);
        }
        System.out.println(map.get(99999));
        System.out.println(map.get(999999999));
    }

    @Test
    public void test() {
        System.out.println("put...");
        map.put(Integer.MAX_VALUE, true);
        map.put(Integer.MAX_VALUE - 1, false);
        map.put(Integer.MAX_VALUE - 2, true);
        map.put(Integer.MAX_VALUE - 3, false);
        map.put(Integer.MAX_VALUE - 4, true);
        print();
        System.out.println("remove...");
        map.remove(Integer.MAX_VALUE - 2);
        map.remove(Integer.MAX_VALUE - 3);
        map.remove(Integer.MAX_VALUE - 4);
        print();
        System.out.println("putIfAbsent...");
        map.putIfAbsent(Integer.MAX_VALUE, true);
        map.putIfAbsent(Integer.MAX_VALUE - 1, false);
        map.putIfAbsent(Integer.MAX_VALUE - 2, true);
        map.putIfAbsent(Integer.MAX_VALUE - 3, false);
        map.putIfAbsent(Integer.MAX_VALUE - 4, true);
        print();
        System.out.println("clear");
        map.clear();
        print();

    }

    private void print() {
        System.out.println(map);
//        System.out.println("==============print start================");
//        int freeValue = map.getFreeValue();
//        int[] keys = map.getKeys();
//        boolean[] values = map.getValues();
//        int key;
//        boolean value;
//        for (int i = 0, len = keys.length; i < len; i++) {
//            if ((key = keys[i]) != freeValue) {
//                value = values[i];
//                System.out.println("key="+key+" value="+value);
//            }
//        }
//        System.out.println("==============print end================");
    }

    @Test
    public void getCapacity() {
        test();
        test();
    }

    @Test
    public void getFreeValue() {
    }

    @Test
    public void getKeys() {
    }

    @Test
    public void getValues() {
    }

    @Test
    public void put() {
    }

    @Test
    public void contains() {
        System.out.println(map.contains(1));
        map.put(1, false);
        print();
        System.out.println(map.contains(1));
    }

    @Test
    public void get() {
    }

    @Test
    public void getOrDefault() {
    }

    @Test
    public void remove() {
    }

    @Test
    public void clear() {
    }

    @Test
    public void reSizeCapacity() {
        map.reSizeCapacity(20);
    }

    @Test
    public void putIfAbsent() {
    }

    @Test
    public void foreachImmutable() {

    }

    @Test
    public void foreach() {
        for (int i = 0; i < 100; i++) {
            foreachMutable();
        }
    }

    @Test
    public void foreachMutable() {
        IntBooleanMap map = new IntBooleanMap();
        Random random = new Random();
        for (int i = 0; i < 500; i++) {
            int key = random.nextInt();
            map.put(key, true);
        }
        int beforeSize = map.size();
        long beginTime = System.currentTimeMillis();
        IntIntMap intIntMap = new IntIntMap();
        LongSet removeSet = new LongSet();
        LongList list = new LongList();
        map.foreachMutable((k, v) -> {
            intIntMap.addValue(1, 1);
            list.add(1);
            if (k % 10 == 0) {
                map.remove(k);
                removeSet.add(k);
            }
        });

        LongSet keySet = new LongSet(map.size());
        map.foreachImmutable((k, v) -> keySet.add(k));
        if (removeSet.size() + map.size() != beforeSize) {
            System.out.println("----------");
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("map.size=" + map.size());
            System.out.println("before map size=" + beforeSize);
        }

        if (beforeSize != intIntMap.get(1)) {
            System.out.println("=================");
            System.out.println("before map size=" + beforeSize);
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("keySet size=" + keySet.size());
            System.out.println("map.size=" + map.size());
            System.out.println("list size=" + list.size());
            System.out.println("intIntMap value=" + intIntMap.get(1));
            long restTime = System.currentTimeMillis() - beginTime;
            System.out.println("time=" + restTime);
        }
    }
}