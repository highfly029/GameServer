package com.highfly029.utils.collection;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LongObjectMapTest {
    class Vector3 {
        public float x;
        public float y;
        public float z;

        public Vector3 set(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
            return this;
        }
    }

    private long t1;
    private long t2;
    LongObjectMap<Vector3> map = new LongObjectMap<>(Vector3[]::new);

    @Before
    public void setUp() throws Exception {
        t1 = System.currentTimeMillis();
    }

    @After
    public void tearDown() throws Exception {
        t2 = System.currentTimeMillis();
        System.out.println("size=" + map.size());
        System.out.println("capacity=" + map.getCapacity());
        System.out.println("freeValue=" + map.getFreeValue());
        System.out.println("t2-t1=" + (t2 - t1));
    }

    @Test
    public void testLarge() {
        for (int i = 0; i < 1000000; i++) {
            Vector3 vector3 = new Vector3();
            map.put(i, vector3.set(i, i, i));
        }
        for (int i = 0; i < 10000; i++) {
            map.remove(i);
        }
        System.out.println(map.get(99999));
    }

    @Test
    public void test() {
        System.out.println("put...");
        map.put(Long.MAX_VALUE, new Vector3().set(7, 0, 0));
        map.put(Long.MAX_VALUE - 1, new Vector3().set(6, 0, 0));
        map.put(Long.MAX_VALUE - 2, new Vector3().set(5, 0, 0));
        map.put(Long.MAX_VALUE - 3, new Vector3().set(4, 0, 0));
        map.put(Long.MAX_VALUE - 4, new Vector3().set(3, 0, 0));
        print();
        System.out.println("remove...");
        map.remove(Long.MAX_VALUE - 2);
        map.remove(Long.MAX_VALUE - 3);
        map.remove(Long.MAX_VALUE - 4);
        print();
        System.out.println("putIfAbsent...");
        map.putIfAbsent(Long.MAX_VALUE, new Vector3().set(7, 0, 0));
        map.putIfAbsent(Long.MAX_VALUE - 1, new Vector3().set(6, 0, 0));
        map.putIfAbsent(Long.MAX_VALUE - 2, new Vector3().set(5, 0, 0));
        map.putIfAbsent(Long.MAX_VALUE - 3, new Vector3().set(4, 0, 0));
        map.putIfAbsent(Long.MAX_VALUE - 4, new Vector3().set(3, 0, 0));
        print();
        System.out.println("clear");
        map.clear();
        print();

    }

    private void print() {
        long freeValue = map.getFreeValue();
        long[] keys = map.getKeys();
        Vector3[] values = map.getValues();
        long key;
        Vector3 value;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                value = values[i];
                System.out.println("key=" + key + " value=" + value);
            }
        }
    }

    @Test
    public void getCapacity() {
        test();
        test();
    }

    @Test
    public void getFreeValue() {
    }

    @Test
    public void getKeys() {
    }

    @Test
    public void getValues() {
    }

    @Test
    public void put() {
        Vector3 vector1 = new Vector3().set(1, 2, 3);
        Vector3 vector2 = new Vector3().set(2, 3, 4);
        map.put(1, vector1);
        map.put(1, vector1);
        map.put(1, vector1);
        map.put(1, vector1);
        map.putIfAbsent(1, vector1);
        map.putIfAbsent(1, vector1);
        map.putIfAbsent(1, vector1);
        map.put(1, vector2);
        print();
    }

    @Test
    public void foreach() {
//        long[] array = new long[]{
//                1134519773, -1920979392, -1675473001, -65412612, 290931865, -777451392, 1908978791, 51279594, 126258594, 794226676, -1359328590, 866073350, -184860528, 1056206629, -936223443, 1921765941, 114270080, -2070793110, -932927155, -271144739, -1571263721, 267532190, 1401693416, -296717638, 1928092359, 1731397394, 28178971, 542917362, -1586211504, 1403940805, -860496530, 89428326, 480783754, 840824018, -1438703050, -1867857590, 2094262112, 1095420457, 1571828026, -480248585, 289261855, -323598719, 1027884441, -888177921, 939900144, 1174601869, 1475974122, 749699202, 937084579, 1203112813, -786939995, 766489791, 1701548461, 1231750759, -1301757248, 1500551661, -1455203469, 1745679318, 511093661, -998686901, 1041546697, -414119070, 178705698, 932020341, 1065885108, -1949303952, 1001815337, 1008231929, -423902601, 569200092, -2002068197, -830244054, -402663219, 1283711973, -1366349609, -347263693, -1657215374, -95830680, -1395058729, -566392978, -1778945560, -1588822191, -2060821355, -2126913290, -503586185, 1879797070, -1012421077, -1758638214, 1355406452, -892679484, -1115404231, 1619325780, -782328378, -1526212858, 1944782090, -149131047, -1135468891, 28563652, 1616672249, 275262194
//        };

        LongObjectMap<Vector3> map = new LongObjectMap<>(Vector3[]::new);
        Random random = new Random();
        IntList list = new IntList();
        for (int i = 0; i < 500; i++) {
            int randomInt = random.nextInt();
//            int randomInt = i;
//            list.add(randomInt);
            map.put(randomInt, new Vector3().set(i, i, i));
        }
//        for (int i = 0; i < array.length; i++) {
//            map.put(array[i], Vector3.create().set(i, i, i));
//        }
        int beforeSize = map.size();
        long beginTime = System.currentTimeMillis();
        IntIntMap intIntMap = new IntIntMap();
        LongSet keySetTmp = new LongSet(map.size());
//        map.foreachImmutable((k, v) -> {
//            keySetTmp.add(k);
//            intIntMap.addValue(1, 1);
//            v.x += 1;
//        });

        LongSet removeSet = new LongSet();
        map.foreachMutable((k, v) -> {
            keySetTmp.add(k);
            intIntMap.addValue(1, 1);
            v.x += 1;
            if (k % 10 == 0) {
                map.remove(k);
                removeSet.add(k);
            }
        });

        LongSet keySet = new LongSet(map.size());
        map.foreachImmutable((k, v) -> {
            keySet.add(k);
            int x = (int) v.x;
            int y = (int) v.y;
            if (x != y + 1) {
//                System.out.println("k="+k+" v="+v);
            }
        });

        if (removeSet.size() + map.size() != beforeSize) {
            System.out.println("----------");
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("map.size=" + map.size());
            System.out.println("before map size=" + beforeSize);
        }
        long restTime = System.currentTimeMillis() - beginTime;
        if (beforeSize != intIntMap.get(1)) {
            System.out.println("=================");
            System.out.println(list);
            System.out.println("before map size=" + beforeSize);
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("keySetTmp size=" + keySetTmp.size());
            System.out.println("keySet size=" + keySet.size());
            System.out.println("intIntMap value=" + intIntMap.get(1));

            System.out.println("time=" + restTime);
        }
    }

    @Test
    public void contains() {
        System.out.println(map.contains(1));
        map.put(1, new Vector3());
        print();
        System.out.println(map.contains(1));
    }

    @Test
    public void get() {
        for (int i = 0; i < 10000; i++) {
            map.clear();
            foreach();
        }
    }

    @Test
    public void getOrDefault() {
    }

    @Test
    public void remove() {
    }

    @Test
    public void clear() {
    }

    @Test
    public void reSizeCapacity() {
    }

    @Test
    public void putIfAbsent() {
    }
}