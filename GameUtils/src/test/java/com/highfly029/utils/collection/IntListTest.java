package com.highfly029.utils.collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IntListTest {
    IntList list1 = new IntList();
    IntList list2 = new IntList();
    IntList retList = new IntList();

    @Before
    public void setUp() throws Exception {
        for (int i = 0; i < 31; i++) {
            list1.add(i);
        }
        for (int i = 5; i < 21; i++) {
            list2.add(i);
        }
    }

    @After
    public void tearDown() throws Exception {
        System.out.println(list1);
        System.out.println(list2);
        System.out.println(retList);
    }

    @Test
    public void accumulate() {
        System.out.println(list1.accumulate(1, 5));
    }

    @Test
    public void remove() {
        for (int i = 10; i < 20; i++) {
            list1.remove(i);
        }
    }


    @Test
    public void intersect() {
        IntList.intersect(list1, list2, retList);
    }

    @Test
    public void union() {
        IntList.union(list1, list2, retList);
    }
}