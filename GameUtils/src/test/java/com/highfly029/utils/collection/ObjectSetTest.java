package com.highfly029.utils.collection;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ObjectSetTest {
    class Vector3 {
        public float x;
        public float y;
        public float z;

        public Vector3 set(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
            return this;
        }
    }

    ObjectSet<String> set = new ObjectSet<>(String[]::new);
    ObjectSet<Vector3> setVector = new ObjectSet<>(Vector3[]::new);

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("size=" + set.size());
        System.out.println("capacity=" + set.getCapacity());
    }

    @Test
    public void testLarge() {
//        for (int i = 0; i < 1000000; i++) {
//            set.add(String.valueOf(i));
//        }
//        for (int i = 0; i < 10000; i++) {
//            set.remove(String.valueOf(i));
//        }
//        System.out.println(set.contains(String.valueOf(99999)));
//        System.out.println(set.contains(String.valueOf(999999999)));
    }

    @Test
    public void test() {
        System.out.println("add...");
        for (int i = 0; i < 10; i++) {
            set.add(String.valueOf(i));
        }
        print();
        System.out.println("remove...");
        for (int i = 0; i < 5; i++) {
            set.remove(String.valueOf(i));
        }
        print();
        System.out.println("add...");
        for (int i = 0; i < 10; i++) {
            set.add(String.valueOf(i));
        }
        print();
        System.out.println("remove...");
        for (int i = 0; i < 10; i++) {
            set.remove(String.valueOf(i));
        }
        print();
        System.out.println("clear");
        set.clear();
        print();
    }

    private void print() {
        System.out.println(set);
//        System.out.println("===========print=============");
//        String[] keys = set.getKeys();
//        String key;
//        for (int i = 0, len = keys.length; i < len; i++) {
//            if ((key = keys[i]) != null) {
//                System.out.println("key="+key);
//            }
//        }
//        System.out.println("set.size="+set.size());
    }

    @Test
    public void getCapacity() {
    }

    @Test
    public void getKeys() {
    }

    @Test
    public void add() {
        Vector3 v1 = new Vector3().set(1, 1, 1);
        Vector3 v2 = new Vector3().set(2, 1, 1);
        Vector3 v3 = new Vector3().set(3, 1, 1);
        Vector3 v4 = new Vector3().set(4, 1, 1);
        Vector3 v5 = new Vector3().set(5, 1, 1);

        setVector.add(v1);
        setVector.add(v1);
        setVector.add(v1);
        setVector.add(v1);
        v1.x = 1111111;
        System.out.println("=============add v1========");
    }

    @Test
    public void contains() {
    }

    @Test
    public void remove() {
        for (int i = 0; i < 100; i++) {
            set.add(String.valueOf(i));
        }
        System.out.println(set);
        System.out.println("size=" + set.size());
        String[] keys = set.getKeys();
        String key;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != null) {
                if (key.equals("10") || key.equals("20") || key.equals("30") || key.equals("40")) {
                    set.remove(key);
                }
            }
        }
        System.out.println(set);
        System.out.println("size=" + set.size());
    }

    @Test
    public void clear() {
    }

    @Test
    public void reSizeCapacity() {
    }

    @Test
    public void foreach() {

    }

    @Test
    public void foreachMutable() {
        ObjectSet<String> set = new ObjectSet<>();
        Random random = new Random();
        for (int i = 0; i < 10000; i++) {
            long key = random.nextLong();
            set.add(String.valueOf(key));
        }

        int beforeSize = set.size();
        long beginTime = System.currentTimeMillis();
        IntIntMap intIntMap = new IntIntMap();
        LongSet removeSet = new LongSet();
        LongList list = new LongList();
        set.foreachMutable((k) -> {
            intIntMap.addValue(1, 1);
            list.add(1);
            if (Long.valueOf(k) % 10 == 0) {
                set.remove(k);
                removeSet.add(Long.valueOf(k));
            }
        });

        LongSet keySet = new LongSet(set.size());
        set.foreachImmutable((k) -> keySet.add(Long.valueOf(k)));
        if (beforeSize != intIntMap.get(1)) {
            System.out.println("=================");
            System.out.println("before map size=" + beforeSize);
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("keySet size=" + keySet.size());
            System.out.println("map.size=" + set.size());
            System.out.println("list size=" + list.size());
            System.out.println("intIntMap value=" + intIntMap.get(1));
            long restTime = System.currentTimeMillis() - beginTime;
            System.out.println("time=" + restTime);
        }
    }
}