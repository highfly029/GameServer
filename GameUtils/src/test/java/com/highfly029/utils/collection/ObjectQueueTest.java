package com.highfly029.utils.collection;

import com.highfly029.utils.RandomUtils;

import org.junit.Test;

public class ObjectQueueTest {

    @Test
    public void test() {
        ObjectQueue<String> queue = new ObjectQueue<>(String[]::new);

        queue.offer("1");
        queue.offer("2");
        queue.offer("3");
        queue.offer("4");
        queue.poll();
        queue.poll();
        queue.offer("a");
        queue.offer("b");
        queue.poll();
        queue.poll();
        queue.poll();
        queue.poll();
        queue.poll();

        queue.foreachImmutable(v -> {
            System.out.println(v);
        });
    }

    @Test
    public void offer() {
        ObjectQueue<String> queue = new ObjectQueue<>(String[]::new);

        for (int i = 0; i < 20; i++) {
            queue.offer(String.valueOf(i + 1));
        }

        queue.foreachImmutable(v -> {
            System.out.println(v);
        });
    }

    @Test
    public void random() {
        ObjectQueue<String> queue = new ObjectQueue<>(String[]::new);
        int num = 9999;
        for (int i = 0; i < num; i++) {

            boolean isAdd = RandomUtils.randomBoolean();
            if (isAdd) {
                queue.offer(String.valueOf(i));
            } else {
                queue.poll();
            }

//            queue.offer(String.valueOf(i));
//            if (!isAdd) {
//                queue.poll();
//            }
        }
        queue.foreachImmutable(v -> {
            System.out.println(v);
        });

        System.out.println("queue.size=" + queue.size());
    }
}