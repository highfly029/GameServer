package com.highfly029.utils.collection;

import org.junit.Test;

public class IntQueueTest {

    @Test
    public void test() {
        IntQueue queue = new IntQueue();
        queue.offer(1);
        queue.offer(1);
        queue.offer(1);
        queue.offer(1);
        queue.offer(1);

        int num = 4;
        for (int i = 0; i < 4; i++) {
            queue.offer(i);
        }

        for (int i = 0; i < 4; i++) {
            queue.poll();
        }

        queue.foreachImmutable(v -> {
            System.out.println(v);
        });
    }
}