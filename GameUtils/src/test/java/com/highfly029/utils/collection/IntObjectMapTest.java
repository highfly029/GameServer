package com.highfly029.utils.collection;

import java.util.Random;

import com.highfly029.utils.RandomUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class IntObjectMapTest {
    IntObjectMap<MyVector> map = new IntObjectMap<>(MyVector[]::new);

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("size=" + map.size());
        System.out.println("capacity=" + map.getCapacity());
        System.out.println("freeValue=" + map.getFreeValue());
    }

    @Test
    public void testLarge() {
        for (int i = 0; i < 1000000; i++) {
            map.put(i, MyVector.create().set(i, i, i));
        }
        for (int i = 0; i < 10000; i++) {
            map.remove(i);
        }
        System.out.println(map.get(99999));
    }

    @Test
    public void test() {
        System.out.println("put...");
        map.put(Integer.MAX_VALUE, MyVector.create().set(7, 0, 0));
        map.put(Integer.MAX_VALUE - 1, MyVector.create().set(6, 0, 0));
        map.put(Integer.MAX_VALUE - 2, MyVector.create().set(5, 0, 0));
        map.put(Integer.MAX_VALUE - 3, MyVector.create().set(4, 0, 0));
        map.put(Integer.MAX_VALUE - 4, MyVector.create().set(3, 0, 0));
        print();
        System.out.println("remove...");
        map.remove(Integer.MAX_VALUE - 2);
        map.remove(Integer.MAX_VALUE - 3);
        map.remove(Integer.MAX_VALUE - 4);
        print();
        System.out.println("putIfAbsent...");
        map.putIfAbsent(Integer.MAX_VALUE, MyVector.create().set(7, 0, 0));
        map.putIfAbsent(Integer.MAX_VALUE - 1, MyVector.create().set(6, 0, 0));
        map.putIfAbsent(Integer.MAX_VALUE - 2, MyVector.create().set(5, 0, 0));
        map.putIfAbsent(Integer.MAX_VALUE - 3, MyVector.create().set(4, 0, 0));
        map.putIfAbsent(Integer.MAX_VALUE - 4, MyVector.create().set(3, 0, 0));
        print();
        System.out.println("clear");
        map.clear();
        print();

    }

    private void print() {
        int freeValue = map.getFreeValue();
        int[] keys = map.getKeys();
        MyVector[] values = map.getValues();
        int key;
        MyVector value;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                value = values[i];
                System.out.println("key=" + key + " value=" + value);
            }
        }
    }

    @Test
    public void getCapacity() {
        test();
        test();
    }

    @Test
    public void getFreeValue() {
    }

    @Test
    public void getKeys() {
    }

    @Test
    public void getValues() {
    }

    @Test
    public void put() {
        MyVector vector1 = MyVector.create().set(1, 2, 3);
        MyVector vector2 = MyVector.create().set(2, 3, 4);
        map.put(1, vector1);
        map.put(1, vector1);
        map.put(1, vector1);
        map.put(1, vector1);
        map.putIfAbsent(1, vector1);
        map.putIfAbsent(1, vector1);
        map.putIfAbsent(1, vector1);
        map.put(1, vector2);
        print();
    }

    @Test
    public void contains() {
        System.out.println(map.contains(1));
        map.put(1, MyVector.create());
        print();
        System.out.println(map.contains(1));
    }

    @Test
    public void get() {
    }

    @Test
    public void getOrDefault() {
        for (int i = 0; i < 10; i++) {
            randomRemove();
        }
    }

    @Test
    public void randomRemove() {
        IntObjectMap<String> map = new IntObjectMap<>(String[]::new);
        for (int i = 0; i < 10000000; i++) {
            int randomInt = RandomUtils.randomInt(0, Integer.MAX_VALUE);
            map.put(randomInt, "str" + i);
        }
        int beforeSize = map.size;
//        System.out.println("before map size=" + beforeSize);
        int freeValue = map.getFreeValue();
        int[] keys = map.getKeys();
        String[] values = map.getValues();
        int key;
        String value;
        IntList list = new IntList();
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                if (key % 10 == 0) {
                    list.add(key);
                    map.remove(key);
                    if (key != keys[i]) {
                        i--;
                    }
                }
            }
        }
        list.sort();
        int afterSize = map.size;
//        System.out.println("after map size=" +afterSize);
        int listSize = list.size;
//        System.out.println("after list size=" + listSize);
        if (beforeSize != (afterSize + listSize)) {
            System.out.println("error !!!!!!!!!!!!!!!!!!!!!!!!!");
        }

//        System.out.println(list);
    }

    @Test
    public void remove() {
        IntObjectMap<String> strMap = new IntObjectMap<>(String[]::new);
        for (int i = 0; i < 50; i++) {
            strMap.put(i, "str" + i);
        }

        int freeValue = strMap.getFreeValue();
        int[] keys = strMap.getKeys();
        String[] values = strMap.getValues();
        int key;
        String value;
        IntList list = new IntList();
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                list.add(key);
                strMap.remove(key);
                if (key != keys[i]) {
                    i--;
                }
//                if (key == 42 ) {
//                    strMap.remove(key);
//                    if (key != keys[i]) {
//                        i--;
//                    }
//                }
            }
        }
        list.sort();
        System.out.println("list size=" + list.size);
        System.out.println(list);
        System.out.println("map size=" + strMap.size);
//        strMap.remove(42);

//        strMap.put(111, null);
//        strMap.put(112, null);
//        strMap.put(113, null);
//        System.out.println(strMap);
//        System.out.println("size="+strMap.size());
//        int freeValue = strMap.getFreeValue();
//        int[] keys = strMap.getKeys();
//        String[] values = strMap.getValues();
//        int key;
//        String value;
//        for (int i = 0, len = keys.length; i < len; i++) {
//            if ((key = keys[i]) != freeValue) {
//                if (key == 5 || key == 15 || key == 25 || key == 35 || key == 45) {
//                    strMap.remove(key);
//                }
//            }
//        }
//        System.out.println(strMap);
//        System.out.println("size="+strMap.size());
    }

    @Test
    public void clear() {
        IntObjectMap<String> intObjectMap = new IntObjectMap<>(7, String[]::new);
        for (int i = 0; i < 20; i++) {
            intObjectMap.put(i, String.valueOf(i * 1000));
        }
        intObjectMap.clear();
        print();
    }

    @Test
    public void reSizeCapacity() {
    }

    @Test
    public void putIfAbsent() {
    }

    @Test
    public void foreachImmutable() {
        IntObjectMap<MyVector> map = new IntObjectMap<>(MyVector[]::new);
        Random random = new Random();
        for (int i = 0; i < 1000000; i++) {
            int key = random.nextInt();
            MyVector value = MyVector.create();
            map.put(key, value);
        }
        int beforeSize = map.size();
        IntIntMap intIntMap = new IntIntMap();
        IntList intList = new IntList();
        map.foreachImmutable((k, v) -> {
            changeMyVector(v);
            intIntMap.addValue(1, 1);
            if (k % 10 == 0) {
                intList.add(k);
            }
        });

        map.foreachImmutable((k, v) -> {
            if (v.x != 1) {
                System.out.println(v);
            }
        });

        for (int i = 0, size = intList.size(); i < size; i++) {
            int key = intList.get(i);
            map.remove(key);
        }

        map.foreachImmutable((k, v) -> {
            if (k % 10 == 0) {
                System.out.println("invalid v=" + v);
            }
        });

        System.out.println("=================");
        System.out.println("before map size=" + beforeSize);
        System.out.println("map.size=" + map.size());
        System.out.println("intIntMap value=" + intIntMap.get(1));
        System.out.println("intList  size=" + intList.size());
    }

    @Test
    public void foreach() {
        for (int i = 0; i < 100; i++) {
            foreachMutable();
        }
    }

    @Test
    public void foreachMutable() {
        IntObjectMap<MyVector> map = new IntObjectMap<>(MyVector[]::new);
        Random random = new Random();
        for (int i = 0; i < 1000000; i++) {
            int key = random.nextInt();
            MyVector value = MyVector.create();
            map.put(key, value);
        }
//        System.out.println("map="+ map);
        int beforeSize = map.size();
        long beginTime = System.currentTimeMillis();
        IntIntMap intIntMap = new IntIntMap();
        IntIntMap removeCntMap = new IntIntMap();
        LongSet removeSet = new LongSet();
        LongList list = new LongList();
        map.foreachMutable((k, v) -> {
            changeMyVector(v);
            intIntMap.addValue(1, 1);
            list.add(1);
            if (k % 10 == 0) {
                map.remove(k);
                removeSet.add(k);
                removeCntMap.addValue(1, 1);
            }
        });

        LongSet keySet = new LongSet(map.size());
        IntIntMap invalidMap = new IntIntMap();
        IntList invalidList = new IntList();
        map.foreachImmutable((k, v) -> {
            if (v.x != 1) {
                if (v.x == 0) {
                    System.out.println(v);
                }
                invalidMap.addValue(1, 1);
                invalidList.add((int) v.x);
            }
        });
        if (removeSet.size() + map.size() != beforeSize) {
            System.out.println("----------");
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("map.size=" + map.size());
            System.out.println("before map size=" + beforeSize);
        }

        if (beforeSize != intIntMap.get(1)) {
            System.out.println("=================");
            System.out.println("before map size=" + beforeSize);
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("removeCntMap size=" + removeCntMap.get(1));
            System.out.println("keySet size=" + keySet.size());
            System.out.println("map.size=" + map.size());
            System.out.println("list size=" + list.size());
            System.out.println("intIntMap value=" + intIntMap.get(1));
            System.out.println("invalidMap size=" + invalidMap.get(1));
//            System.out.println("invalid list=" + invalidList);
            long restTime = System.currentTimeMillis() - beginTime;
            System.out.println("time=" + restTime);
        }
    }

    private void changeMyVector(MyVector MyVector) {
        MyVector.x++;
    }
}