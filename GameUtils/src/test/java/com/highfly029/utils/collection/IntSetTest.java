package com.highfly029.utils.collection;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IntSetTest {
    IntSet set = new IntSet();

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("size=" + set.size());
        System.out.println("capacity=" + set.getCapacity());
        System.out.println("freeValue=" + set.getFreeValue());
    }

    @Test
    public void testLarge() {
        for (int i = 0; i < 1000000; i++) {
            set.add(i);
        }
        for (int i = 0; i < 10000; i++) {
            set.remove(i);
        }
        System.out.println(set.contains(99999));
        System.out.println(set.contains(999999999));
    }

    @Test
    public void test() {
        System.out.println("add...");
        for (int i = 0; i < 10; i++) {
            set.add(Integer.MAX_VALUE - i);
        }
        print();
        System.out.println("remove...");
        for (int i = 0; i < 5; i++) {
            set.remove(Integer.MAX_VALUE - i);
        }
        print();
        System.out.println("add...");
        for (int i = 0; i < 10; i++) {
            set.add(Integer.MAX_VALUE);
        }
        print();
        System.out.println("remove...");
        for (int i = 0; i < 10; i++) {
            set.remove(Integer.MAX_VALUE);
        }
        print();
        System.out.println("clear");
        set.clear();
        print();
    }

    private void print() {
        System.out.println(set);
//        System.out.println("===========print=============");
//        int freeValue = set.getFreeValue();
//        int[] keys = set.getKeys();
//        int key;
//        for (int i = 0, len = keys.length; i < len; i++) {
//            if ((key = keys[i]) != freeValue) {
//                System.out.println("key="+key);
//            }
//        }
//        System.out.println("set.size="+set.size());
    }

    @Test
    public void getCapacity() {
        test();
        test();
    }

    @Test
    public void getFreeValue() {
    }

    @Test
    public void getKeys() {
    }

    @Test
    public void add() {
    }

    @Test
    public void contains() {
    }

    @Test
    public void remove() {
    }

    @Test
    public void clear() {
    }

    @Test
    public void reSizeCapacity() {
        set.reSizeCapacity(20);
    }

    @Test
    public void foreach() {

    }

    @Test
    public void foreachMutable() {
        IntSet set = new IntSet();
        Random random = new Random();
        for (int i = 0; i < 10000; i++) {
            int key = random.nextInt();
            set.add(key);
        }

        int beforeSize = set.size();
        long beginTime = System.currentTimeMillis();
        IntIntMap intIntMap = new IntIntMap();
        LongSet removeSet = new LongSet();
        LongList list = new LongList();
        set.foreachMutable((k) -> {
            intIntMap.addValue(1, 1);
            list.add(1);
            if (k % 10 == 0) {
                set.remove(k);
                removeSet.add(k);
            }
        });

        LongSet keySet = new LongSet(set.size());
        set.foreachImmutable((k) -> keySet.add(k));
        if (beforeSize != intIntMap.get(1)) {
            System.out.println("=================");
            System.out.println("before map size=" + beforeSize);
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("keySet size=" + keySet.size());
            System.out.println("map.size=" + set.size());
            System.out.println("list size=" + list.size());
            System.out.println("intIntMap value=" + intIntMap.get(1));
            long restTime = System.currentTimeMillis() - beginTime;
            System.out.println("time=" + restTime);
        }
    }
}