package com.highfly029.utils.collection;

class MyVector {
    public int x;
    public int y;
    public int z;

    public static MyVector create() {
        MyVector myVector = new MyVector();
        return myVector;
    }

    public MyVector set(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("x=").append(x)
                .append(",y=").append(y)
                .append(",z=").append(z);
        return stringBuilder.toString();
    }
}