package com.highfly029.utils.collection;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ObjObjMapTest {
    class Vector3 {
        public float x;
        public float y;
        public float z;

        public Vector3 set(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
            return this;
        }
    }

    ObjObjMap<String, Vector3> map = new ObjObjMap<>(String[]::new, Vector3[]::new);

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("size=" + map.size());
        System.out.println("capacity=" + map.getCapacity());
    }

    @Test
    public void testLarge() {
        for (int i = 0; i < 100; i++) {
            map.put(String.valueOf(i), new Vector3().set(i, i, i));
        }
        System.out.println("size=" + map.size());
        System.out.println("capacity=" + map.getCapacity());
        for (int i = 80; i < 120; i++) {
            map.remove(String.valueOf(i));
        }
        System.out.println(map.get("99999"));
    }

    @Test
    public void test() {
        System.out.println("put...");
        map.put(null, null);
        map.put("2", null);
        map.put("7", new Vector3().set(7, 0, 0));
        map.put("6", new Vector3().set(6, 0, 0));
        map.put("5", new Vector3().set(5, 0, 0));
        map.put("4", new Vector3().set(4, 0, 0));
        map.put("3", new Vector3().set(3, 0, 0));
        print();
        System.out.println("remove...");
        map.remove("5");
        map.remove("4");
        map.remove("3");
        print();
        System.out.println("putIfAbsent...");
        map.putIfAbsent("7", new Vector3().set(7, 0, 0));
        map.putIfAbsent("6", new Vector3().set(6, 0, 0));
        map.putIfAbsent("5", new Vector3().set(5, 0, 0));
        map.putIfAbsent("4", new Vector3().set(4, 0, 0));
        map.putIfAbsent("3", new Vector3().set(3, 0, 0));
        print();
        System.out.println("clear");
        map.clear();
        print();
    }

    private void print() {
        System.out.println(map);
//        String[] keys = map.getKeys();
//        Vector3[] values = map.getValues();
//        String key;
//        Vector3 value;
//        for (int i = 0, len = keys.length; i < len; i++) {
//            if ((key = keys[i]) != null) {
//                value = values[i];
//                System.out.println("key="+key+" value="+value);
//            }
//        }
    }

    @Test
    public void getCapacity() {
        test();
        test();
    }

    @Test
    public void getKeys() {
    }

    @Test
    public void getValues() {
    }

    @Test
    public void put() {
        Vector3 vector1 = new Vector3().set(1, 2, 3);
        Vector3 vector2 = new Vector3().set(2, 3, 4);
        map.put("1", vector1);
        map.put("1", vector1);
        map.put("1", vector1);
        map.put("1", vector1);
        map.putIfAbsent("1", vector1);
        map.putIfAbsent("1", vector1);
        map.putIfAbsent("1", vector1);
        map.put("1", vector2);
        print();
    }

    @Test
    public void contains() {
        System.out.println(map.contains(String.valueOf(1)));
        map.put(String.valueOf(1), new Vector3());
        print();
        System.out.println(map.contains(String.valueOf(1)));
    }

    @Test
    public void get() {
    }

    @Test
    public void getOrDefault() {
    }

    @Test
    public void remove() {
    }

    @Test
    public void clear() {
    }

    @Test
    public void reSizeCapacity() {
    }

    @Test
    public void putIfAbsent() {
    }

    @Test
    public void foreach() {
        for (int i = 0; i < 100; i++) {
            foreachMutable();
        }
    }

    @Test
    public void foreachMutable() {
        ObjObjMap<String, Vector3> map = new ObjObjMap<>(String[]::new, Vector3[]::new);
        Random random = new Random();
        for (int i = 0; i < 500; i++) {
            long key = random.nextLong();
            Vector3 value = new Vector3();
            map.put("str" + key, value);
        }
        int beforeSize = map.size();
        long beginTime = System.currentTimeMillis();
        IntIntMap intIntMap = new IntIntMap();
        LongSet removeSet = new LongSet();
        LongList list = new LongList();
        map.foreachMutable((k, v) -> {
            intIntMap.addValue(1, 1);
            list.add(1);
            if (k.hashCode() % 10 == 0) {
                map.remove(k);
                removeSet.add(k.hashCode());
            }
        });

        LongSet keySet = new LongSet(map.size());
        map.foreachImmutable((k, v) -> keySet.add(k.hashCode()));
        if (removeSet.size() + map.size() != beforeSize) {
            System.out.println("----------");
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("map.size=" + map.size());
            System.out.println("before map size=" + beforeSize);
        }

        if (beforeSize != intIntMap.get(1)) {
            System.out.println("=================");
            System.out.println("before map size=" + beforeSize);
            System.out.println("removeSet size=" + removeSet.size());
            System.out.println("keySet size=" + keySet.size());
            System.out.println("map.size=" + map.size());
            System.out.println("list size=" + list.size());
            System.out.println("intIntMap value=" + intIntMap.get(1));
            long restTime = System.currentTimeMillis() - beginTime;
            System.out.println("time=" + restTime);
        }
    }
}