package com.highfly029.utils;

import java.lang.reflect.Type;

import com.google.gson.reflect.TypeToken;
import com.highfly029.utils.collection.ObjObjMap;
import com.highfly029.utils.collection.ObjectSet;

import org.junit.Test;

public class JsonUtilsTest {

    public class Student {
        private String name;
        private int score;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", score=" + score +
                    '}';
        }
    }

    @Test
    public void testObjObjMap1() {
        ObjObjMap<String, String> map = new ObjObjMap<>(String[]::new, String[]::new);
        map.put("success", "true");
        map.put("fail", "false");
        map.put("a", null);

        System.out.println(map);

        String json = JsonUtils.toJson(map);
        System.out.println(json);
        ObjObjMap<String, String> map2 = JsonUtils.fromJson(json, ObjObjMap.class);
        System.out.println(map2);
    }

    @Test
    public void testObjObjMap2() {
        ObjObjMap<String, Student> map = new ObjObjMap<>(String[]::new, Student[]::new);
        Student s1 = new Student();
        s1.setName("name1");
        s1.setScore(1111);
        map.put(s1.getName(), s1);

        Student s2 = new Student();
        s2.setName("name2");
        s2.setScore(2222);
        map.put(s2.getName(), s2);

        System.out.println(map);

        String json = JsonUtils.toJson(map);
        System.out.println(json);
        Type type = new TypeToken<ObjObjMap<String, Student>>() {
        }.getType();
        ObjObjMap<String, Student> map2 = JsonUtils.fromJson(json, type);
        System.out.println(map2);
    }

    @Test
    public void testObjectSet() {
        ObjectSet<String> set = new ObjectSet<>(String[]::new);
        for (int i = 0; i < 5; i++) {
            set.add("aaa" + i);
        }
        System.out.println(set);

        String json = JsonUtils.toJson(set);
        System.out.println(json);
        ObjectSet<String> set2 = JsonUtils.fromJson(json, ObjectSet.class);
        System.out.println(set2);
    }

    @Test
    public void testObjectSet2() {
        ObjectSet<Student> set = new ObjectSet<>(Student[]::new);
        for (int i = 0; i < 3; i++) {
            Student student = new Student();
            student.setName("name" + i);
            student.setScore(i * 1000);
            set.add(student);
        }
        System.out.println(set);

        String json = JsonUtils.toJson(set);
        System.out.println(json);
        Type type = new TypeToken<ObjectSet<Student>>() {
        }.getType();
        ObjectSet<Student> set2 = JsonUtils.fromJson(json, type);
        System.out.println(set2);
    }


}