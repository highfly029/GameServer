package com.highfly029.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @ClassName DateUtils
 * @Description 日期类工具
 * @Author liyunpeng
 **/
public final class DateUtils {
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final int MILLIS_EVERY_SECOND = 1000;

    /**
     * 判断两个时间戳是否是同一天
     *
     * @param time1
     * @param time2
     * @return
     */
    public static boolean isSameDay(long time1, long time2) {
        long secondMillsPerDay = 86400000L;
        long interval = time1 - time2;
        TimeZone timeZone = TimeZone.getDefault();
        if (interval > secondMillsPerDay || interval < -secondMillsPerDay) {
            return false;
        }
        return ((long) timeZone.getOffset(time1) + time1) / secondMillsPerDay == ((long) timeZone.getOffset(time2) + time2) / secondMillsPerDay;
    }

    /**
     * 判断两个时间戳是否同一个月
     *
     * @param time1
     * @param time2
     * @return
     */
    public static boolean isSameMonth(long time1, long time2) {
        return false;
    }

    /**
     * 判断两个时间戳是否同一年
     *
     * @param time1
     * @param time2
     * @return
     */
    public static boolean isSameYear(long time1, long time2) {
        return false;
    }

    /**
     * 是否是闰年
     *
     * @param year
     * @return
     */
    public static boolean isLeapYear(int year) {
        return ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0));
    }

    /**
     * 日期字符串转时间戳
     * @param str
     * @return
     */
    public static long dateStr2Long(String str) {
        try {
            Date date = SIMPLE_DATE_FORMAT.parse(str);
            if (date != null) {
                return date.getTime();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0L;
    }

    public static void main(String[] args) throws ParseException {
        long time1 = 1603353613000L;
        long time2 = 1603382300000L;
        System.out.println(isSameDay(time1, time2));
    }
}
