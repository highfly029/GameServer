package com.highfly029.utils.gsonAdapter;

import java.io.IOException;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.highfly029.utils.collection.ObjObjMap;

/**
 * @ClassName ObjObjJsonAdapter
 * @Description ObjObjJsonAdapter
 * @Author liyunpeng
 **/
public class ObjObjJsonAdapter extends TypeAdapter<ObjObjMap> {
    @Override
    public void write(JsonWriter jsonWriter, ObjObjMap objObjMap) throws IOException {
        jsonWriter.beginObject();
        objObjMap.foreachImmutable((k, v) -> {
            try {
                jsonWriter.name(k.toString());
                jsonWriter.value(v.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        jsonWriter.endObject();
    }

    @Override
    public ObjObjMap read(JsonReader jsonReader) throws IOException {
        final ObjObjMap map = new ObjObjMap();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String key = jsonReader.nextName();
            String value = jsonReader.nextString();
            map.put(key, value);
        }
        jsonReader.endObject();
        return map;
    }
}
