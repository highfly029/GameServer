package com.highfly029.utils.gsonDeserializer;

import java.lang.reflect.Field;
import java.lang.reflect.Type;

/**
 * @ClassName AbstractDeserializer
 * @Description AbstractDeserializer
 * @Author liyunpeng
 **/
public abstract class AbstractDeserializer {
    public Type[] getTypeArray(Type type) {
        Class clz = type.getClass();
        Type[] array = null;
        try {
            Field field = clz.getDeclaredField("typeArguments");
            field.setAccessible(true);
            array = (Type[]) field.get(type);
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return array;
    }
}
