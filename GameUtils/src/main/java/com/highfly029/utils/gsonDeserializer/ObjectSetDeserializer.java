package com.highfly029.utils.gsonDeserializer;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.highfly029.utils.collection.ObjectSet;

/**
 * @ClassName ObjectSetDeserializer
 * @Description ObjectSetDeserializer
 * @Author liyunpeng
 **/
public class ObjectSetDeserializer extends AbstractDeserializer implements JsonDeserializer<ObjectSet> {
    @Override
    public ObjectSet deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        ObjectSet set = new ObjectSet();
        if (jsonElement.isJsonArray()) {
            JsonArray jsonArray = jsonElement.getAsJsonArray();
            for (JsonElement element : jsonArray) {
                Type[] array = getTypeArray(type);
                if (array == null) {
                    set.add(element.getAsString());
                } else {
                    set.add(jsonDeserializationContext.deserialize(element, array[0]));
                }
            }
        }
        return set;
    }
}
