package com.highfly029.utils.gsonDeserializer;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Map;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.highfly029.utils.collection.ObjObjMap;

/**
 * @ClassName ObjObjMapDeserializer
 * @Description ObjObjMapDeserializer
 * @Author liyunpeng
 **/
public class ObjObjMapDeserializer extends AbstractDeserializer implements JsonDeserializer<ObjObjMap> {
    @Override
    public ObjObjMap deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        ObjObjMap map = new ObjObjMap();
        if (jsonElement.isJsonObject()) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            Iterator<Map.Entry<String, JsonElement>> iterator = jsonObject.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, JsonElement> entry = iterator.next();

                Type[] array = getTypeArray(type);
                if (array == null) {
                    map.put(entry.getKey(), entry.getValue().getAsString());
                } else {
                    map.put(entry.getKey(), jsonDeserializationContext.deserialize(entry.getValue(), array[1]));
                }
            }
        }
        return map;
    }
}
