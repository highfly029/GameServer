package com.highfly029.utils;

/**
 * @ClassName MathUtils
 * @Description 数学计算
 * @Author liyunpeng
 **/
public final class MathUtils {
    /**
     * 判断一个数是不是2的幂
     *
     * @param num
     * @return
     */
    public static final boolean isPowerOfTwo(int num) {
        return (num & -num) == num;
    }

    /**
     * 获取距离num最近的是2的次幂的数字
     *
     * @param num
     * @return 永远 >= num
     */
    public static int getNearlyPowerOfTwo(int num) {
        if ((num & -num) == num) {
            return num;
        }
        return Integer.MIN_VALUE >>> (Integer.numberOfLeadingZeros(num) - 1);
    }

    /**
     * 获取复合索引
     * @param arg1
     * @param arg2
     * @return
     */
    public static long getCompositeIndex(int arg1, int arg2) {
        return ((long) arg1) << 32 | (long) arg2;
    }

    /**
     * 获取复合索引第一个参数
     * @param index
     * @return
     */
    public static int getCompositeArg1(long index) {
        return (int) (index >> 32);
    }

    /**
     * 获取复合索引第二个参数
     * @param index
     * @return
     */
    public static int getCompositeArg2(long index) {
        return (int) index;
    }

    public static void main(String[] args) {
        long a = 4299262263297L;
        System.out.println(getCompositeArg1(a));
        System.out.println(getCompositeArg2(a));
        System.out.println(getCompositeIndex(1001, 1));
    }
}
