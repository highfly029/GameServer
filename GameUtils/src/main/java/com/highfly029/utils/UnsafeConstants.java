/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.highfly029.utils;

import java.lang.reflect.Field;

import sun.misc.Unsafe;

import static java.nio.ByteOrder.LITTLE_ENDIAN;
import static java.nio.ByteOrder.nativeOrder;


public interface UnsafeConstants {
    Unsafe U = Inner.U;

    class Inner {
        private static final Unsafe U;

        static {
            try {
                Field f = Unsafe.class.getDeclaredField("theUnsafe");
                f.setAccessible(true);
                U = (Unsafe) f.get(null);
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
    }

    long BYTE_SCALE = 1L;
    int BYTE_SCALE_SHIFT = 0;

    long CHAR_SCALE = 2L;
    int CHAR_SCALE_SHIFT = 1;

    long SHORT_SCALE = 2L;
    int SHORT_SCALE_SHIFT = 1;

    long INT_SCALE = 4L;
    int INT_SCALE_SHIFT = 2;

    long FLOAT_SCALE = 4L;
    int FLOAT_SCALE_SHIFT = 2;

    long LONG_SCALE = 8L;
    int LONG_SCALE_SHIFT = 3;

    long DOUBLE_SCALE = 8L;
    int DOUBLE_SCALE_SHIFT = 3;

    long BYTE_BASE = U.arrayBaseOffset(byte[].class);
    long CHAR_BASE = U.arrayBaseOffset(char[].class);
    long SHORT_BASE = U.arrayBaseOffset(short[].class);
    long INT_BASE = U.arrayBaseOffset(int[].class);
    long FLOAT_BASE = U.arrayBaseOffset(float[].class);
    long LONG_BASE = U.arrayBaseOffset(long[].class);
    long DOUBLE_BASE = U.arrayBaseOffset(double[].class);

    /**
     * In parallel key-value tables keys and values sometimes are read and written as a single
     * value of doubled size. Key is always in the lower half, value is in the higher.
     * Then the offset to keys and values depends on native byte order.
     */

    long BYTE_KEY_OFFSET = nativeOrder() == LITTLE_ENDIAN ? 0L : 1L;
    long BYTE_VALUE_OFFSET = 1L - BYTE_KEY_OFFSET;

    long CHAR_KEY_OFFSET = nativeOrder() == LITTLE_ENDIAN ? 0L : 2L;
    long CHAR_VALUE_OFFSET = 2L - CHAR_KEY_OFFSET;

    long SHORT_KEY_OFFSET = CHAR_KEY_OFFSET;
    long SHORT_VALUE_OFFSET = CHAR_VALUE_OFFSET;

    long INT_KEY_OFFSET = nativeOrder() == LITTLE_ENDIAN ? 0L : 4L;
    long INT_VALUE_OFFSET = 4L - INT_KEY_OFFSET;

    long FLOAT_KEY_OFFSET = INT_KEY_OFFSET;
    long FLOAT_VALUE_OFFSET = INT_VALUE_OFFSET;
}
