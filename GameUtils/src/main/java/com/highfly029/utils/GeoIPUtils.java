package com.highfly029.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.AddressNotFoundException;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.model.CountryResponse;
import com.maxmind.geoip2.record.City;
import com.maxmind.geoip2.record.Continent;
import com.maxmind.geoip2.record.Country;
import com.maxmind.geoip2.record.Subdivision;

/**
 * @ClassName GeoIPUtils
 * @Description GeoIPUtils
 * @Author liyunpeng
 **/
public class GeoIPUtils {
    private static final String UNKNOWN = "unknown";
    private static FileInputStream database = null;
    private static DatabaseReader reader = null;

    static {
        try {
            String fileName = ConfigPropUtils.getConfigPath() + "/GeoIP2-Country.mmdb";
            database = new FileInputStream(fileName);
            reader = new DatabaseReader.Builder(database).build();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (database != null) {
                try {
                    database.close();
                } catch (IOException e) {
                    System.out.println(e);
                }
            }
        }
    }

    /**
     * 获取国家代码
     *
     * @param ip
     * @return
     */
    public static String getCountryCode(String ip) {
        String countryCode = UNKNOWN;
        if (reader != null) {
            try {
                InetAddress ipAddress = InetAddress.getByName(ip);
                CountryResponse response = reader.country(ipAddress);

                Country country = response.getCountry();
                countryCode = country.getIsoCode();
                if (countryCode == null) {
                    countryCode = UNKNOWN;
                }
            } catch (AddressNotFoundException e) {
                //此处不处理
            } catch (Exception e) {
                System.out.println(e);
            }
        } else {
            System.out.println("GeoIP DatabaseReader is null");
        }
        return countryCode;
    }

    /**
     * 获取洲代码
     * AF : Africa 非洲
     * AS : Asia 亚洲
     * EU : Europe 欧洲
     * NA : North America 北美洲
     * OC : Oceania 大洋洲
     * SA : South America 南美洲
     * AN : Antarctica 南极洲
     *
     * @param ip
     * @return
     */
    public static String getContinentCode(String ip) {
        String continentCode = UNKNOWN;
        if (reader != null) {
            try {
                InetAddress ipAddress = InetAddress.getByName(ip);
                CountryResponse response = reader.country(ipAddress);

                Continent continent = response.getContinent();
                continentCode = continent.getCode();
                if (continentCode == null) {
                    continentCode = UNKNOWN;
                }
            } catch (AddressNotFoundException e) {
                //此处不处理
            } catch (Exception e) {
                System.out.println(e);
            }
        } else {
            System.out.println("GeoIP DatabaseReader is null");
        }
        return continentCode;
    }

    /**
     * 获取城市代码
     *
     * @param ip
     * @return
     */
    public static String getCityCode(String ip) {
        String cityCode = UNKNOWN;
        if (reader != null) {
            try {
                InetAddress ipAddress = InetAddress.getByName(ip);
                CityResponse response = reader.city(ipAddress);
                City city = response.getCity();
                cityCode = city.getName();
                if (cityCode == null) {
                    cityCode = UNKNOWN;
                }
            } catch (AddressNotFoundException e) {
                //此处不处理
            } catch (Exception e) {
                System.out.println(e);
            }
        } else {
            System.out.println("GeoIP DatabaseReader is null");
        }
        return cityCode;
    }

    /**
     * 获取省代码
     *
     * @param ip
     * @return
     */
    public static String getSubdivisionCode(String ip) {
        String cityCode = UNKNOWN;
        if (reader != null) {
            try {
                InetAddress ipAddress = InetAddress.getByName(ip);
                CityResponse response = reader.city(ipAddress);
                Subdivision subdivision = response.getMostSpecificSubdivision();
                cityCode = subdivision.getName();
                if (cityCode == null) {
                    cityCode = UNKNOWN;
                }
            } catch (AddressNotFoundException e) {
                //此处不处理
            } catch (Exception e) {
                System.out.println(e);
            }
        } else {
            System.out.println("GeoIP DatabaseReader is null");
        }
        return cityCode;
    }
}
