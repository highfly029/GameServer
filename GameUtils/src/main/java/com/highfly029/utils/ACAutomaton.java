package com.highfly029.utils;

import com.highfly029.utils.collection.CharObjectMap;
import com.highfly029.utils.collection.CharSet;
import com.highfly029.utils.collection.ObjectList;
import com.highfly029.utils.collection.ObjectQueue;

/**
 * @ClassName ACAutomaton
 * @Description AC自动机 多模式匹配
 * @Author liyunpeng
 **/
public class ACAutomaton {
    /**
     * 根结点
     */
    private final ACNode rootNode;
    /**
     * 要忽略的字符集
     */
    private final CharSet ignoreCharSet;
    /**
     * 要被替换的字符
     */
    private final char replaceChar;
    /**
     * 大小写敏感的字符集
     */
    private final CharSet caseSensitiveSet;

    public ACAutomaton() {
        this(null, null, '*');
    }


    public ACAutomaton(char[] ignoreArray, char[] caseSensitiveArray, char replaceChar) {
        rootNode = new ACNode();
        ignoreCharSet = new CharSet();
        if (ignoreArray != null && ignoreArray.length > 0) {
            for (int i = 0, len = ignoreArray.length; i < len; i++) {
                ignoreCharSet.add(ignoreArray[i]);
            }
        }

        this.replaceChar = replaceChar;

        caseSensitiveSet = new CharSet();
        if (caseSensitiveArray != null && caseSensitiveArray.length > 0) {
            for (int i = 0, len = caseSensitiveArray.length; i < len; i++) {
                caseSensitiveSet.add(caseSensitiveArray[i]);
            }
        }
    }


    /**
     * 转换成小写字符
     *
     * @param c
     * @return
     */
    private char toLowerCase(char c) {
        return caseSensitiveSet.contains(c) ? c : Character.toLowerCase(c);
    }

    /**
     * 是否是忽略的字符
     *
     * @param c
     * @return
     */
    private boolean isIgnore(char c) {
        return ignoreCharSet.contains(c);
    }

    /**
     * 初始化
     *
     * @param list
     */
    public void init(ObjectList<String> list) {
        if (list != null && list.size() > 0) {
            String[] values = list.getValues();
            String value;
            for (int i = 0, len = list.size(); i < len; i++) {
                value = values[i];
                if (value == null || (value = value.trim()).length() == 0) {
                    continue;
                }

                char[] charArray = value.toCharArray();
                ACNode currentNode = this.rootNode;
                for (int j = 0, jLen = charArray.length; j < jLen; j++) {
                    char c = toLowerCase(charArray[j]);
                    ACNode nextNode = currentNode.subNodes.get(c);
                    if (nextNode == null) {
                        nextNode = new ACNode();
                        currentNode.subNodes.put(c, nextNode);
                    }
                    currentNode = nextNode;
                }

                if (currentNode != rootNode) {
                    currentNode.terminal = true;
                }
            }
        }
        buildFailedNode();
    }

    /**
     * 构造失效节点
     */
    private final void buildFailedNode() {
        ObjectQueue<ACNode> queue = new ObjectQueue<>(ACNode[]::new);
        rootNode.failNode = rootNode;
        rootNode.subNodes.foreachImmutable((k, v) -> {
            queue.offer(v);
            v.level = 1;
            v.failNode = rootNode;
        });


        ACNode currentNode;
        ACNode failNode;

        while (!queue.isEmpty()) {
            currentNode = queue.poll();
            failNode = currentNode.failNode;

            if (currentNode.subNodes.isEmpty()) {
                continue;
            }

            char[] keys = currentNode.subNodes.getKeys();
            ACNode[] values = currentNode.subNodes.getValues();
            char key;
            ACNode value;
            char freeValue = currentNode.subNodes.getFreeValue();

            for (int i = 0, len = keys.length; i < len; i++) {
                if ((key = keys[i]) != freeValue) {
                    if ((value = values[i]) != null) {

                        while (failNode != rootNode && !failNode.subNodes.contains(key)) {
                            failNode = failNode.failNode;
                        }
                        value.failNode = failNode.subNodes.get(key);

                        if (value.failNode == null) {
                            value.failNode = rootNode;
                        }
                        value.level = currentNode.level + 1;
                        queue.offer(value);
                    }
                }
            }

        }
    }

    public final String replace(final String word) {
        char[] array = word.toCharArray();
        char[] result = word.toCharArray();
        ACNode current = rootNode;
        ACNode next;
        boolean filter = false;
        int replaceFrom = 0;
        int ignoreLength = 0;
        for (int i = 0, len = array.length; i < len; i++) {
            char c = toLowerCase(array[i]);
            next = current.subNodes.get(c);
            if (!isIgnore(c)) {
                while (next == null && current != rootNode) {
                    current = current.failNode;
                    next = current.subNodes.get(c);
                }
            }
            if (next != null) {
                current = next;
            }

            if (current != rootNode && isIgnore(c)) {
                ignoreLength++;
            }
            if (current.terminal) {
                int j = i - (current.level - 1) - ignoreLength;
                if (j < replaceFrom) {
                    j = replaceFrom;
                }
                replaceFrom = i + 1;
                for (; j <= i; j++) {
                    result[j] = replaceChar;
                    filter = true;
                }
                current = rootNode;
                ignoreLength = 0;
            }
        }

        if (filter) {
            return String.valueOf(result);
        } else {
            return word;
        }
    }

    /**
     * 贪婪模式
     * 优先替换最长串
     *
     * @param word
     * @return
     */
    public final String replaceGreed(final String word) {
        char[] array = word.toCharArray();
        char[] result = word.toCharArray();
        ACNode current = rootNode;
        ACNode next;
        boolean filter = false;
        int replaceFrom = 0;
        int ignoreLength = 0;
        boolean isAlreadyFind = false;
        int alreadyi = 0;
        int alreadyj = 0;

        for (int i = 0, len = array.length; i < len; i++) {
            char c = toLowerCase(array[i]);
            next = current.subNodes.get(c);
            if (!isIgnore(c)) {
                //贪婪
                if (isAlreadyFind && next == null) {
                    isAlreadyFind = false;
                    int j = alreadyj;
                    if (j < replaceFrom) {
                        j = replaceFrom;
                    }
                    replaceFrom = alreadyi;
                    for (; j <= alreadyi; j++) {
                        result[j] = replaceChar;
                        filter = true;
                    }
                    current = rootNode;
                    next = current.subNodes.get(c);
                    ignoreLength = 0;
                }
                while (next == null && current != rootNode) {
                    current = current.failNode;
                    next = current.subNodes.get(c);
                }
            }
            if (next != null) {
                current = next;
            }

            if (current != rootNode && isIgnore(c)) {
                ignoreLength++;
            }

            if (current.terminal) {
                isAlreadyFind = true;
                alreadyi = i;
                alreadyj = i - (current.level - 1) - ignoreLength;
                //最后一个匹配成功特殊处理
                if (i == len - 1) {
                    int j = i - (current.level - 1) - ignoreLength;
                    if (j < replaceFrom) {
                        j = replaceFrom;
                    }
                    replaceFrom = i + 1;
                    for (; j <= i; j++) {
                        result[j] = replaceChar;
                        filter = true;
                    }
                    current = rootNode;
                    ignoreLength = 0;
                }
            }
        }

        if (filter) {
            return String.valueOf(result);
        } else {
            return word;
        }
    }

    /**
     * 是否包含敏感词
     *
     * @param word
     * @return
     */
    public final boolean contain(final String word) {
        char[] array = word.toCharArray();
        ACNode current = rootNode;
        ACNode next = null;
        for (int i = 0, len = array.length; i < len; i++) {
            char c = toLowerCase(array[i]);
            if (!isIgnore(c)) {
                next = current.subNodes.get(c);
                while (next == null && current != rootNode) {
                    current = current.failNode;
                    next = current.subNodes.get(c);
                }
            }
            if (next != null) {
                current = next;
            }
            if (current.terminal) {
                return true;
            }
        }
        return false;
    }


    private static class ACNode {
        //当前结点的层级
        public int level;
        //当前结果匹配失败时的跳转结点
        public ACNode failNode;
        //当前结点是否是终结结点
        public boolean terminal;
        //当前结点后子结点，Key为小写字母
        public CharObjectMap<ACNode> subNodes = new CharObjectMap<>(ACNode[]::new);

        @Override
        public String toString() {
            return "@" + hashCode();
        }
    }

    /**
     * 广度优先搜索算法按行打印 Trie树
     */
    public void bfsPrint() {
        ObjectQueue<ACNode> queue = new ObjectQueue<>(ACNode[]::new);
        queue.offer(rootNode);
        System.out.println("0:null-rootNode=" + rootNode);
        int curLevel = 0;

        while (!queue.isEmpty()) {
            ACNode node = queue.poll();
            if (node.subNodes != null) {
                char[] keys = node.subNodes.getKeys();
                ACNode[] values = node.subNodes.getValues();
                char key;
                ACNode value;
                char freeValue = node.subNodes.getFreeValue();

                for (int i = 0, len = keys.length; i < len; i++) {
                    if ((key = keys[i]) != freeValue) {
                        if ((value = values[i]) != null) {
                            if (value.level != curLevel) {
                                if (curLevel > 0) {
                                    System.out.println();
                                }
                                System.out.print(value.level + ":");
                            }
                            System.out.print(key);
                            System.out.print("-cur=" + value);
                            System.out.print("-fail=" + value.failNode);
                            if (value.terminal) {
                                System.out.print("#");
                            }
                            System.out.print(" ");
                            queue.offer(value);
                            curLevel = value.level;
                        }
                    }
                }
            }
        }
        System.out.println();
    }
}
