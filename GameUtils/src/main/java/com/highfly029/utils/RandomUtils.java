package com.highfly029.utils;

import java.util.Collection;
import java.util.Random;

import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName RandomUtils
 * @Description 随机生成工具
 * @Author liyunpeng
 **/
public final class RandomUtils {
    //概率万分比
    public static final float RatioPercent = 0.0001f;
    //概率单位
    public static final int RatioUnit = 10000;

    private static final Random random = new Random();

    /**
     * 检测概率是否触发
     *
     * @param probabilityValue
     * @return 触发成功
     */
    public static boolean checkProbability(int probabilityValue) {
        if (probabilityValue <= 0) {
            return false;
        } else if (probabilityValue >= RandomUtils.RatioUnit) {
            return true;
        } else {
            int randomValue = RandomUtils.randomInt(RandomUtils.RatioUnit);
            return randomValue < probabilityValue;
        }
    }

    /**
     * 随机一个boolean值
     */
    public static boolean randomBoolean() {
        return random.nextBoolean();
    }

    /**
     * 随机一个int值 0 <= ret < bound
     */
    public static int randomInt(final int bound) {
        return random.nextInt(bound);
    }

    /**
     * 随机值
     *
     * @return
     */
    public static int nextInt() {
        return random.nextInt();
    }

    /**
     * 范围内随机一个int值
     *
     * @param start
     * @param end
     * @return
     */
    public static int randomInt(final int start, final int end) {
        if (start > end) {
            return 0;
        } else if (start == end) {
            return start;
        } else {
            return start + random.nextInt(end - start);
        }
    }

    /**
     * 随机一个long值
     */
    public static long randomLong(final long bound) {
        return (long) randomDouble(bound);
    }

    /**
     * 随机一个0到指定数值bound之间（不包含bound）的float值
     */
    public static float randomFloat(final float bound) {
        return randomFloat() * bound;
    }

    /**
     * 随机一个介于0到1之间（不包含1）的float值
     */
    public static float randomFloat() {
        return random.nextFloat();
    }

    /**
     * 随机一个0到指定数值bound之间（不包含bound）的double值
     */
    public static double randomDouble(final double bound) {
        return randomDouble() * bound;
    }

    /**
     * 随机一个介于0到1之间（不包含1）的double值
     */
    public static double randomDouble() {
        return random.nextDouble();
    }


    /**
     * 从指定对象集合中随机一个对象，如果集合为null或空，则返回null
     */
    public static <E> E random(Collection<E> collection) {
        if (collection == null || collection.isEmpty()) {
            return null;
        }
        int size = collection.size();
        //集合中只有一个对象，直接返回
        if (size == 1) {
            for (E e : collection) {
                return e;
            }
        }

        //多个对象，随机
        int index = randomInt(size);
        int count = 0;
        for (E e : collection) {
            if (index == count) {
                return e;
            }
            count++;
        }
        return null;
    }

    /**
     * 从指定对象集合中随机一个对象
     *
     * @param map
     * @param <V>
     * @return
     */
    public static <V> V random(IntObjectMap<V> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }
        int size = map.size();
        int random = 0;
        if (size > 0) {
            random = randomInt(size);
        }
        int count = 0;
        int freeValue = map.getFreeValue();
        int[] keys = map.getKeys();
        V[] values = map.getValues();

        for (int i = 0, len = keys.length; i < len; i++) {
            if (keys[i] != freeValue) {
                if (random == count) {
                    return values[i];
                }
                count++;
            }
        }
        return null;
    }

    /**
     * 从list中随机一个对象
     * @param list
     * @param <V>
     * @return
     */
    public static <V> V random(ObjectList<V> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        int random = randomInt(list.size());
        return list.get(random);
    }
}
