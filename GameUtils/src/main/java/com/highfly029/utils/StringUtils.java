package com.highfly029.utils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName StringUtils
 * @Description 字符串工具
 * @Author liyunpeng
 **/
public final class StringUtils {
    public static final String VERTICAL_SYMBOL = "\\|";
    public static final String COMMA_SYMBOL = "\\,";

    private static final Pattern emoji = Pattern.compile("[^\u0000-\uFFFF]");

    /**
     * 是否包含emoji
     *
     * @param str
     * @return
     */
    public static boolean isContainEmoji(String str) {
        return emoji.matcher(str).find();
    }

    /**
     * 字符串转int[]
     *
     * @param str
     * @return
     */
    public static int[] toIntArray(String str) {
        return toIntArray(str, VERTICAL_SYMBOL);
    }

    public static int[] toIntArrayByComma(String str) {
        return toIntArray(str, COMMA_SYMBOL);
    }

    /**
     * 字符串转int[]
     *
     * @param str
     * @param sep
     * @return
     */
    public static int[] toIntArray(String str, String sep) {
        String[] strArr = str.split(sep);
        if (strArr == null || strArr.length == 0) {
            return null;
        } else {
            int[] array = new int[strArr.length];
            for (int i = 0, size = strArr.length; i < size; i++) {
                array[i] = Integer.parseInt(strArr[i]);
            }
            return array;
        }
    }

    /**
     * 字符串转换 去掉空字符
     *
     * @param str
     * @param sep
     * @return
     */
    public static String[] toStringArray(String str, String sep) {
        String[] strArr = str.split(sep);
        if (strArr != null) {
            for (int i = 0, size = strArr.length; i < size; i++) {
                strArr[i] = strArr[i].trim();
            }
        }
        return strArr;
    }

    /**
     * 判断是否为整数
     *
     * @param str 传入的字符串
     * @return 是整数返回true, 否则返回false
     */
    public static boolean isInteger(String str) {
        if (str == null || str.equals("")) {
            return false;
        } else {
            Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
            return pattern.matcher(str).matches();
        }
    }

    /**
     * 移除字符串中的空格符
     *
     * @param str
     * @return
     */
    public static String removeSpace(String str) {
        return str.replaceAll("\\s+", "");
    }

    /**
     * 将mysql的latin1字符集转换成utf8
     *
     * @param str
     * @return
     */
    public static String convertLatin1ToUTF8(String str) {
        if (str != null) {
            int length = str.length();
            byte[] buffer = new byte[length];
            return new String(str.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        }
        return null;
    }

    /**
     * 含有unicode 的字符串转一般字符串
     *
     * @param unicodeStr 混有 Unicode 的字符串
     * @return
     */
    public static String unicodeStr2String(String unicodeStr) {
        int length = unicodeStr.length();
        int count = 0;
        //正则匹配条件，可匹配“\\u”1到4位，一般是4位可直接使用 String regex = "\\\\u[a-f0-9A-F]{4}";
        String regex = "\\\\u[a-f0-9A-F]{1,4}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(unicodeStr);
        StringBuffer sb = new StringBuffer();

        while (matcher.find()) {
            String oldChar = matcher.group();//原本的Unicode字符
            String newChar = unicode2String(oldChar);//转换为普通字符
            // int index = unicodeStr.indexOf(oldChar);
            // 在遇见重复出现的unicode代码的时候会造成从源字符串获取非unicode编码字符的时候截取索引越界等
            int index = matcher.start();

            sb.append(unicodeStr.substring(count, index));//添加前面不是unicode的字符
            sb.append(newChar);//添加转换后的字符
            count = index + oldChar.length();//统计下标移动的位置
        }
        sb.append(unicodeStr.substring(count, length));//添加末尾不是Unicode的字符
        return sb.toString();
    }

    /**
     * 字符串转换unicode
     *
     * @param string
     * @return
     */
    public static String string2Unicode(String string) {
        StringBuffer unicode = new StringBuffer();
        for (int i = 0; i < string.length(); i++) {
            // 取出每一个字符
            char c = string.charAt(i);
            // 转换为unicode
            unicode.append("\\u" + Integer.toHexString(c));
        }

        return unicode.toString();
    }

    /**
     * unicode 转字符串
     *
     * @param unicode 全为 Unicode 的字符串
     * @return
     */
    public static String unicode2String(String unicode) {
        StringBuffer string = new StringBuffer();
        String[] hex = unicode.split("\\\\u");

        for (int i = 1; i < hex.length; i++) {
            // 转换出每一个代码点
            int data = Integer.parseInt(hex[i], 16);
            // 追加成string
            string.append((char) data);
        }

        return string.toString();
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
//        String a = "营地测试3";
//        String a1 = new String(a.getBytes("UTF-8"), "ISO-8859-1");
//        System.out.println(convertLatin1ToUTF8(a1));
        String b = "🧍‍♀️";
        System.out.println(isContainEmoji(b));
    }
}
