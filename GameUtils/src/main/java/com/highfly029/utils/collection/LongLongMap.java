package com.highfly029.utils.collection;

import java.util.Arrays;

import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.collection.consumer.LongLongConsumer;

/**
 * @ClassName LongLongMap
 * @Description LongLongMap
 * @Author liyunpeng
 **/
public class LongLongMap extends BaseHash {
    public static final LongLongMap empty = new LongLongMap();
    private long freeValue;
    private long[] keys;
    private long[] values;

    public LongLongMap() {
        init(reCountCapacity(0));
    }

    public LongLongMap(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    private void init(int capacity) {
        keys = new long[capacity];
        values = new long[capacity];
        if (freeValue != 0) {
            Arrays.fill(keys, freeValue);
        }
    }

    /**
     * 获取map的最大容量
     *
     * @return
     */
    public int getCapacity() {
        return keys.length;
    }

    public final long getFreeValue() {
        return freeValue;
    }

    public final long[] getKeys() {
        return keys;
    }

    public final long[] getValues() {
        return values;
    }

    /**
     * 增加
     *
     * @param key
     * @param value
     */
    public void put(long key, long value) {
        int index = insert(key, value);
        if (index < 0) {
            return;
        } else {
            values[index] = value;
            return;
        }
    }

    /**
     * 叠加
     *
     * @param key
     * @param value
     * @return
     */
    public long addValue(long key, long value) {
        int index = insert(key, value);
        if (index < 0) {
            return value;
        } else {
            return values[index] += value;
        }
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean contains(long key) {
        if (size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 获取
     *
     * @param key
     * @return
     */
    public long get(long key) {
        if (size == 0) {
            return 0L;
        }
        int index = index(key);
        if (index >= 0) {
            return values[index];
        } else {
            return 0L;
        }
    }

    /**
     * 获取
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public long getOrDefault(long key, long defaultValue) {
        if (size == 0) {
            return defaultValue;
        }
        int index = index(key);
        if (index >= 0) {
            return values[index];
        } else {
            return defaultValue;
        }
    }

    /**
     * 删除
     *
     * @param key
     * @return
     */
    public long remove(long key) {
        if (size == 0) {
            return 0L;
        }
        long free;
        if (key != (free = freeValue)) {
            long[] tmpKeys = keys;
            int capacityMask = tmpKeys.length - 1;
            int index;
            long cur;
            if ((cur = tmpKeys[index = mixLongKey(key) & capacityMask]) != key) {
                if (cur == free) {
                    return 0L;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            break;
                        } else if (cur == free) {
                            return 0L;
                        }
                    }
                }
            }

            long[] tmpValues = values;
            long value = tmpValues[index];
            int indexToRemove = index;
            int indexToShift = indexToRemove;
            int shiftDistance = 1;
            long keyToShift;
            while (true) {
                indexToShift = (indexToShift - 1) & capacityMask;
                if ((keyToShift = tmpKeys[indexToShift]) == free) {
                    break;
                }
                if (((mixLongKey(keyToShift) - indexToShift) & capacityMask) >= shiftDistance) {
                    tmpKeys[indexToRemove] = keyToShift;
                    tmpValues[indexToRemove] = tmpValues[indexToShift];
                    indexToRemove = indexToShift;
                    shiftDistance = 1;
                } else {
                    shiftDistance++;
                }
            }

            tmpKeys[indexToRemove] = free;
            size--;
            return value;
        } else {
            return 0;
        }
    }

    @Override
    public void clear() {
        this.freeValue = 0;
        if (keys.length > clearMaxSize) {
            init(clearMaxSize);
        } else {
            Arrays.fill(keys, freeValue);
        }
        size = 0;
    }

    /**
     * 扩容
     *
     * @param capacity
     */
    public void reSizeCapacity(int capacity) {
        if (capacity > keys.length) {
            rehash(reCountCapacity(capacity));
        }
    }

    /**
     * 如果不存在才添加
     *
     * @param key
     * @param value
     * @return
     */
    public long putIfAbsent(long key, long value) {
        int index = insert(key, value);
        if (index < 0) {
            return value;
        } else {
            return values[index];
        }
    }

    /**
     * 插入
     *
     * @param key
     * @param value
     * @return 索引
     */
    private int insert(long key, long value) {
        long free;
        if (key == (free = freeValue)) {
            free = changeFree();
        }
        long[] tmpKeys = keys;
        int capacityMask;
        int index;
        long cur;
        if ((cur = tmpKeys[index = mixLongKey(key) & (capacityMask = tmpKeys.length - 1)]) != free) {
            if (cur == key) {
                return index;
            } else {
                while (true) {
                    if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == free) {
                        break;
                    } else if (cur == key) {
                        return index;
                    }
                }
            }
        }
        tmpKeys[index] = key;
        values[index] = value;
        int curSize = ++size;
        if (isCanGrow(curSize / keys.length)) {
            rehash(size << 1);
        }
        return -1;
    }

    private long changeFree() {
        long newFreeValue = findNewFreeOrRemoved();
        replaceAll(keys, freeValue, newFreeValue);
        freeValue = newFreeValue;
        return newFreeValue;
    }

    private long findNewFreeOrRemoved() {
        long tmpFreeValue = freeValue;
        long newFreeValue;
        do {
            newFreeValue = RandomUtils.nextInt();
        } while ((newFreeValue == tmpFreeValue) || index(newFreeValue) >= 0);
        return newFreeValue;
    }

    /**
     * 查找key的索引
     *
     * @param key
     * @return -1表示不存在这个key
     */
    private int index(long key) {
        long free;
        if (key != (free = freeValue)) {
            long[] tmpKeys = keys;
            int capacityMask;
            int index;
            long cur;
            if ((cur = tmpKeys[index = mixLongKey(key) & (capacityMask = tmpKeys.length - 1)]) == key) {
                return index;
            } else {
                if (cur == free) {
                    return -1;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            return index;
                        } else if (cur == free) {
                            return -1;
                        }
                    }
                }
            }
        } else {
            return -1;
        }
    }

    private void rehash(int newCapacity) {
        long free = freeValue;
        long[] tmpKeys = keys;
        long[] tmpValues = values;
        init(newCapacity);
        long[] newKeys = keys;
        long[] newValues = values;
        int capacityMask = newKeys.length - 1;

        long key;
        int index;
        for (int i = tmpKeys.length - 1; i >= 0; i--) {
            if ((key = tmpKeys[i]) != free) {
                if (newKeys[index = mixLongKey(key) & capacityMask] != free) {
                    while (true) {
                        if (newKeys[(index = (index - 1) & capacityMask)] == free) {
                            break;
                        }
                    }
                }
                newKeys[index] = key;
                newValues[index] = tmpValues[i];
            }
        }
    }

    @Override
    public String toString() {
        long freeValue = this.freeValue;
        long[] keys = this.keys;
        long[] values = this.values;
        long key;
        long value;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                value = values[i];
                stringBuilder.append(key).append(":").append(value).append(", ");
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    /**
     * 不可变的遍历
     *
     * @param consumer
     */
    public void foreachImmutable(LongLongConsumer consumer) {
        if (size == 0) {
            return;
        }
        long freeValue = this.freeValue;
        long[] keys = this.keys;
        long[] values = this.values;
        long key;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key, values[i]);
            }
        }
    }

    /**
     * 可变的遍历(需要注意在遍历时删除元素，有可能会重复遍历某一元素)
     *
     * @param consumer
     */
    public void foreachMutable(LongLongConsumer consumer) {
        if (size == 0) {
            return;
        }
        long freeValue = this.freeValue;
        long[] keys = this.keys;
        long[] values = this.values;
        long key;

        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key, values[i]);
                if (key != keys[i]) {
                    i--;
                }
            }
        }
    }
}
