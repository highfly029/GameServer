package com.highfly029.utils.collection;

import com.highfly029.utils.collection.consumer.ObjectConsumer;
import com.highfly029.utils.collection.creater.ObjectArrayCreateInterface;

/**
 * @ClassName ObjectQueue
 * @Description ObjectQueue
 * @Author liyunpeng
 **/
public class ObjectQueue<E> extends BaseQueue {
    public static final ObjectQueue empty = new ObjectQueue();
    private E[] values;
    private ObjectArrayCreateInterface<E> create;

    public ObjectQueue() {
        init(reCountCapacity(0));
    }

    public ObjectQueue(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
    }

    public ObjectQueue(ObjectArrayCreateInterface<E> create) {
        this.create = create;
        init(reCountCapacity(0));
    }

    public ObjectQueue(int capacity, ObjectArrayCreateInterface<E> create) {
        this.create = create;
        int size = reCountCapacity(capacity);
        init(size);
    }

    private E[] createObjectArray(int length) {
        if (create != null) {
            return create.create(length);
        }
        return (E[]) new Object[length];
    }

    /**
     * 初始化
     *
     * @param capacity
     */
    private void init(int capacity) {
        values = createObjectArray(capacity);
        size = 0;
    }

    /**
     * 增加数组长度
     *
     * @param capacity
     */
    private void grow(int capacity) {
        E[] array = createObjectArray(capacity);
        if (size != 0) {
            if (startIndex < endIndex) {
                System.arraycopy(values, 0, array, 0, size);
            } else {
                int len = values.length - startIndex;
                System.arraycopy(values, startIndex, array, 0, len);
                System.arraycopy(values, 0, array, len, endIndex);
            }
        }
        startIndex = 0;
        endIndex = size;
        values = array;
    }

    /**
     * 尾部增加元素
     *
     * @param e
     * @return
     */
    public boolean offer(E e) {
        if (size == values.length) {
            grow(size << 1);
        }

        int end = endIndex;
        if (end >= values.length) {
            end = 0;
        }
        values[end++] = e;

        endIndex = end;
        size++;
        return true;
    }

    /**
     * 删除头部元素并返回
     *
     * @return
     */
    public E poll() {
        if (size == 0) {
            return null;
        }
        int start = startIndex;
        E e = values[start];
        values[start] = null;
        if (++start == values.length) {
            start = 0;
        }
        startIndex = start;
        size--;
        return e;
    }

    /**
     * 查看头部元素
     *
     * @return
     */
    public E peek() {
        if (size == 0) {
            return null;
        }
        return values[startIndex];
    }

    /**
     * 不可变的遍历
     *
     * @param consumer
     */
    public void foreachImmutable(ObjectConsumer<? super E> consumer) {
        if (size == 0) {
            return;
        }

        if (endIndex > startIndex) {
            for (int i = startIndex; i < endIndex; i++) {
                consumer.accept(values[i]);
            }
        } else {
            for (int i = startIndex, len = values.length; i < len; i++) {
                consumer.accept(values[i]);
            }
            for (int i = 0; i < endIndex; i++) {
                consumer.accept(values[i]);
            }
        }
    }

    public void clear() {
        size = 0;
        startIndex = 0;
        endIndex = 0;
        init(defaultSize);
    }
}
