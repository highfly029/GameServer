package com.highfly029.utils.collection;

import java.util.Arrays;

import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.collection.consumer.CharObjectConsumer;
import com.highfly029.utils.collection.creater.ObjectArrayCreateInterface;

/**
 * @ClassName CharObjectMap
 * @Description CharObjectMap
 * @Author liyunpeng
 **/
public class CharObjectMap<V> extends BaseHash {
    public static final CharObjectMap empty = new CharObjectMap();
    private char freeValue;
    private char[] keys;
    private V[] values;
    private ObjectArrayCreateInterface<V> create;

    public CharObjectMap() {
        init(reCountCapacity(0));
    }

    public CharObjectMap(ObjectArrayCreateInterface<V> create) {
        this.create = create;
        init(reCountCapacity(0));
    }

    public CharObjectMap(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    public CharObjectMap(int capacity, ObjectArrayCreateInterface<V> create) {
        this.create = create;
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    private void init(int capacity) {
        keys = new char[capacity];
        if (freeValue != 0) {
            Arrays.fill(keys, freeValue);
        }
        values = createObjectArray(capacity);
    }

    private V[] createObjectArray(int length) {
        if (create != null) {
            return create.create(length);
        }
        return (V[]) new Object[length];
    }

    /**
     * 获取map的最大容量
     *
     * @return
     */
    public int getCapacity() {
        return keys.length;
    }

    public final char getFreeValue() {
        return freeValue;
    }

    public final char[] getKeys() {
        return keys;
    }

    public final V[] getValues() {
        return values;
    }

    /**
     * 增加
     *
     * @param key
     * @param value
     */
    public void put(char key, V value) {
        int index = insert(key, value);
        if (index < 0) {
            return;
        } else {
            values[index] = value;
            return;
        }
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean contains(char key) {
        if (size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean containsKey(char key) {
        if (size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 获取
     *
     * @param key
     * @return
     */
    public V get(char key) {
        if (size == 0) {
            return null;
        }
        int index = index(key);
        if (index >= 0) {
            return values[index];
        } else {
            return null;
        }
    }

    /**
     * 获取
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public V getOrDefault(char key, V defaultValue) {
        if (size == 0) {
            return defaultValue;
        }
        int index = index(key);
        if (index >= 0) {
            return values[index];
        } else {
            return defaultValue;
        }
    }

    /**
     * 删除
     *
     * @param key
     * @return
     */
    public V remove(char key) {
        if (size == 0) {
            return null;
        }
        char free;
        if (key != (free = freeValue)) {
            char[] tmpKeys = keys;
            int capacityMask = tmpKeys.length - 1;
            int index;
            char cur;
            if ((cur = tmpKeys[index = mixCharKey(key) & capacityMask]) != key) {
                if (cur == free) {
                    return null;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            break;
                        } else if (cur == free) {
                            return null;
                        }
                    }
                }
            }

            V[] tmpValues = values;
            V value = tmpValues[index];
            int indexToRemove = index;
            int indexToShift = indexToRemove;
            int shiftDistance = 1;
            char keyToShift;
            while (true) {
                indexToShift = (indexToShift - 1) & capacityMask;
                if ((keyToShift = tmpKeys[indexToShift]) == free) {
                    break;
                }
                if (((mixCharKey(keyToShift) - indexToShift) & capacityMask) >= shiftDistance) {
                    tmpKeys[indexToRemove] = keyToShift;
                    tmpValues[indexToRemove] = tmpValues[indexToShift];
                    indexToRemove = indexToShift;
                    shiftDistance = 1;
                } else {
                    shiftDistance++;
                }
            }

            tmpKeys[indexToRemove] = free;
            tmpValues[indexToRemove] = null;
            size--;
            return value;
        } else {
            return null;
        }
    }

    @Override
    public void clear() {
        this.freeValue = 0;
        char[] tmpKeys = this.keys;
        if (tmpKeys.length > clearMaxSize) {
            init(clearMaxSize);
        } else {
            char tmpFreeValue = this.freeValue;
            V[] tmpValues = this.values;
            for (int i = 0, len = tmpKeys.length; i < len; i++) {
                tmpKeys[i] = tmpFreeValue;
                tmpValues[i] = null;
            }
        }
        size = 0;
    }

    /**
     * 扩容
     *
     * @param capacity
     */
    public void reSizeCapacity(int capacity) {
        if (capacity > keys.length) {
            rehash(reCountCapacity(capacity));
        }
    }

    /**
     * 如果不存在才添加
     *
     * @param key
     * @param value
     * @return
     */
    public V putIfAbsent(char key, V value) {
        int index = insert(key, value);
        if (index < 0) {
            return value;
        } else {
            return values[index];
        }
    }

    /**
     * 插入
     *
     * @param key
     * @param value
     * @return 索引
     */
    private int insert(char key, V value) {
        char free;
        if (key == (free = freeValue)) {
            free = changeFree();
        }
        char[] tmpKeys = keys;
        int capacityMask;
        int index;
        char cur;
        if ((cur = tmpKeys[index = mixCharKey(key) & (capacityMask = tmpKeys.length - 1)]) != free) {
            if (cur == key) {
                return index;
            } else {
                while (true) {
                    if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == free) {
                        break;
                    } else if (cur == key) {
                        return index;
                    }
                }
            }
        }
        tmpKeys[index] = key;
        values[index] = value;
        int curSize = ++size;
        if (isCanGrow(curSize / keys.length)) {
            rehash(size << 1);
        }
        return -1;
    }

    private char changeFree() {
        char newFreeValue = findNewFreeOrRemoved();
        replaceAll(keys, freeValue, newFreeValue);
        freeValue = newFreeValue;
        return newFreeValue;
    }

    private char findNewFreeOrRemoved() {
        char tmpFreeValue = freeValue;
        char newFreeValue;
        do {
            newFreeValue = (char) RandomUtils.nextInt();
        } while ((newFreeValue == tmpFreeValue) || index(newFreeValue) >= 0);
        return newFreeValue;
    }

    /**
     * 查找key的索引
     *
     * @param key
     * @return -1表示不存在这个key
     */
    private int index(char key) {
        char free;
        if (key != (free = freeValue)) {
            char[] tmpKeys = keys;
            int capacityMask;
            int index;
            char cur;
            if ((cur = tmpKeys[index = mixCharKey(key) & (capacityMask = tmpKeys.length - 1)]) == key) {
                return index;
            } else {
                if (cur == free) {
                    return -1;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            return index;
                        } else if (cur == free) {
                            return -1;
                        }
                    }
                }
            }
        } else {
            return -1;
        }
    }

    private void rehash(int newCapacity) {
        char free = freeValue;
        char[] tmpKeys = keys;
        V[] tmpValues = values;
        init(newCapacity);
        char[] newKeys = keys;
        V[] newValues = values;
        int capacityMask = newKeys.length - 1;

        char key;
        int index;
        for (int i = tmpKeys.length - 1; i >= 0; i--) {
            if ((key = tmpKeys[i]) != free) {
                if (newKeys[index = mixCharKey(key) & capacityMask] != free) {
                    while (true) {
                        if (newKeys[(index = (index - 1) & capacityMask)] == free) {
                            break;
                        }
                    }
                }
                newKeys[index] = key;
                newValues[index] = tmpValues[i];
            }
        }
    }

    @Override
    public String toString() {
        char freeValue = this.freeValue;
        char[] keys = this.keys;
        V[] values = this.values;
        char key;
        V value;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                value = values[i];
                stringBuilder.append(key).append(":").append(value).append(", ");
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    /**
     * 不可变的遍历
     *
     * @param consumer
     */
    public void foreachImmutable(CharObjectConsumer<? super V> consumer) {
        if (size == 0) {
            return;
        }
        char freeValue = this.freeValue;
        char[] keys = this.keys;
        V[] values = this.values;
        char key;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key, values[i]);
            }
        }
    }

    /**
     * 可变的遍历(需要注意在遍历时删除元素，有可能会重复遍历未删除的某一元素)
     *
     * @param consumer
     */
    public void foreachMutable(CharObjectConsumer<? super V> consumer) {
        if (size == 0) {
            return;
        }
        char freeValue = this.freeValue;
        char[] keys = this.keys;
        V[] values = this.values;
        char key;

        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key, values[i]);
                if (key != keys[i]) {
                    i--;
                }
            }
        }
    }
}
