package com.highfly029.utils.collection;

import com.highfly029.utils.MathUtils;

/**
 * @ClassName BaseQueue
 * @Description BaseQueue
 * @Author liyunpeng
 **/
public abstract class BaseQueue {
    protected static final int defaultSize = 4;
    protected int size = 0;
    protected int startIndex;
    protected int endIndex;

    /**
     * 重新计算容量
     *
     * @param capacity
     * @return
     */
    protected int reCountCapacity(int capacity) {
        capacity = MathUtils.getNearlyPowerOfTwo(capacity);
        if (capacity < defaultSize) {
            capacity = defaultSize;
        }
        return capacity;
    }


    /**
     * 获取队列的元素数量
     *
     * @return
     */
    public final int size() {
        return size;
    }

    /**
     * 判断队列是否为空
     *
     * @return
     */
    public final boolean isEmpty() {
        return size == 0;
    }

    /**
     * 清空队列
     */
    public void clear() {
        size = 0;
        startIndex = 0;
        endIndex = 0;
    }
}
