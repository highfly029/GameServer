package com.highfly029.utils.collection;

import java.util.Arrays;
import java.util.Comparator;

import com.highfly029.utils.collection.creater.ObjectArrayCreateInterface;

/**
 * @ClassName ObjectList
 * @Description ObjectList
 * @Author liyunpeng
 **/
public class ObjectList<E> extends BaseList {
    public static final ObjectList empty = new ObjectList();
    private E[] values;
    private ObjectArrayCreateInterface<E> create;

    public ObjectList() {
        init(reCountCapacity(0));
    }

    public ObjectList(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    public ObjectList(ObjectArrayCreateInterface<E> create) {
        this.create = create;
        init(reCountCapacity(0));
    }

    public ObjectList(int capacity, ObjectArrayCreateInterface<E> create) {
        this.create = create;
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    private E[] createObjectArray(int length) {
        if (create != null) {
            return create.create(length);
        }
        return (E[]) new Object[length];
    }

    /**
     * 初始化
     *
     * @param capacity
     */
    private void init(int capacity) {
        values = createObjectArray(capacity);
        size = 0;
    }

    /**
     * 获取队列的最大容量
     *
     * @return
     */
    public int getCapacity() {
        return values.length;
    }

    /**
     * 获取队列里的数组
     *
     * @return
     */
    public final E[] getValues() {
        return values;
    }

    /**
     * 增加数组长度
     *
     * @param capacity
     */
    private void grow(int capacity) {
        E[] array = createObjectArray(capacity);
        System.arraycopy(values, 0, array, 0, size);
        values = array;
    }

    /**
     * 根据索引设置元素
     *
     * @param index
     * @param value
     */
    public void set(int index, E value) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
        values[index] = value;
    }

    /**
     * 根据索引获取元素
     *
     * @param index
     * @return
     */
    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
        return values[index];
    }

    /**
     * 增加值
     *
     * @param value
     */
    public void add(E value) {
        if (size == values.length) {
            grow(size << 1);
        }
        values[size++] = value;
    }

    /**
     * 增加一组
     *
     * @param array
     */
    public void addArray(E[] array) {
        int newCapacity = size + array.length;
        if (newCapacity > values.length) {
            grow(reCountCapacity(newCapacity));
        }
        System.arraycopy(array, 0, values, size, array.length);
        size = newCapacity;
    }

    /**
     * 增加数组
     *
     * @param list
     */
    public void addAll(ObjectList<E> list) {
        if (list.isEmpty()) {
            return;
        }
        int newCapacity = size + list.size();
        if (newCapacity > values.length) {
            grow(reCountCapacity(newCapacity));
        }
        E[] array = list.getValues();
        System.arraycopy(array, 0, values, size, list.size());
        size = newCapacity;
    }

    /**
     * 在列表头部添加元素
     *
     * @param value
     */
    public void unshift(E value) {
        if (size == values.length) {
            grow(size << 1);
        }
        if (size > 0) {
            System.arraycopy(values, 0, values, 1, size);
        }
        values[0] = value;
        size++;
    }

    /**
     * 删除索引对应的元素
     *
     * @param index
     * @return
     */
    public E remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
        if (size == 0) {
            return null;
        }
        E value = values[index];
        int numMoved = size - index - 1;
        if (numMoved > 0) {
            System.arraycopy(values, index + 1, values, index, numMoved);
        }
        values[--size] = null;
        return value;
    }

    /**
     * 删除对象
     *
     * @param element
     * @return
     */
    public boolean remove(E element) {
        int index = indexOf(element);
        if (index != -1) {
            remove(index);
            return true;
        }
        return false;
    }

    /**
     * 查找第一个等于元素的索引
     *
     * @param value
     * @return
     */
    public int indexOf(E value) {
        return indexOf(0, value);
    }

    /**
     * 从指定偏移量开始查找第一个等于元素的索引
     *
     * @param offset
     * @param value
     * @return
     */
    public int indexOf(int offset, E value) {
        if (size == 0) {
            return -1;
        }
        if (offset < 0 || offset >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(offset));
        }
        E[] tmpValues = values;
        E tmp;
        for (int i = offset, len = size; i < len; i++) {
            if ((tmp = tmpValues[i]) == value || tmp.equals(value)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 从尾部开始,向前查找第一个等于元素的索引
     *
     * @param value
     * @return
     */
    public int lastIndexOf(E value) {
        return lastIndexOf(size - 1, value);
    }

    /**
     * 从尾部开始,从指定偏移量开始向前查找第一个等于元素的索引
     *
     * @param offset
     * @param value
     * @return
     */
    public int lastIndexOf(int offset, E value) {
        if (size == 0) {
            return -1;
        }
        if (offset < 0 || offset >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(offset));
        }
        E[] tmpValues = values;
        E tmp;
        for (int i = offset; i >= 0; i--) {
            if ((tmp = tmpValues[i]) == value || tmp.equals(value)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 是否包含元素
     *
     * @param element
     * @return
     */
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    /**
     * 在指定索引位置插入元素
     *
     * @param index
     * @param value
     */
    public void insert(int index, E value) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
        if (size == values.length) {
            E[] array = createObjectArray(size << 1);
            System.arraycopy(values, 0, array, 0, index);
            System.arraycopy(values, index, array, index + 1, size - index);
            array[index] = value;
            values = array;
        } else {
            System.arraycopy(values, index, values, index + 1, size - index);
            values[index] = value;
        }
        size++;
    }

    /**
     * 扩容
     *
     * @param capacity
     */
    public void reSizeCapacity(int capacity) {
        if (capacity > values.length) {
            grow(reCountCapacity(capacity));
        }
    }

    @Override
    public void clear() {
        E[] tmpValue = values;
        if (tmpValue.length > clearMaxSize) {
            init(clearMaxSize);
        } else {
            for (int i = 0, len = size; i < len; i++) {
                tmpValue[i] = null;
            }
            size = 0;
        }
    }

    /**
     * 转换成数组
     *
     * @return
     */
    public E[] toArray() {
        if (size == 0) {
            return null;
        }
        E[] array = createObjectArray(size);
        System.arraycopy(values, 0, array, 0, size);
        return array;
    }

    public void sort(Comparator<? super E> comparator) {
        if (size == 0) {
            return;
        }
        Arrays.sort(values, 0, size, comparator);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0; i < size; i++) {
            stringBuilder.append(values[i]);
            if (i == size - 1) {
                return stringBuilder.append("]").toString();
            }
            stringBuilder.append(", ");
        }
        return "[]";
    }
}
