package com.highfly029.utils.collection.consumer;

/**
 * @ClassName LongLongConsumer
 * @Description LongLongConsumer
 * @Author liyunpeng
 **/
public interface LongLongConsumer {
    void accept(long k, long v);
}
