package com.highfly029.utils.collection;

import java.util.Arrays;

import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.collection.consumer.CharConsumer;

/**
 * @ClassName CharSet
 * @Description CharSet
 * @Author liyunpeng
 **/
public class CharSet extends BaseHash {
    public static final CharSet empty = new CharSet();
    private char freeValue;
    private char[] keys;

    public CharSet() {
        init(reCountCapacity(0));
    }

    public CharSet(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    private void init(int capacity) {
        keys = new char[capacity];
        if (freeValue != 0) {
            Arrays.fill(keys, freeValue);
        }
    }

    /**
     * 获取map的最大容量
     *
     * @return
     */
    public int getCapacity() {
        return keys.length;
    }

    public final char getFreeValue() {
        return freeValue;
    }

    public final char[] getKeys() {
        return keys;
    }

    /**
     * 增加
     *
     * @param key
     * @return true增加成功 false增加失败
     */
    public boolean add(char key) {
        char free;
        if (key == (free = freeValue)) {
            free = changeFree();
        }
        char[] tmpKeys = keys;
        int capacityMask;
        int index;
        char cur;
        if ((cur = tmpKeys[index = mixCharKey(key) & (capacityMask = tmpKeys.length - 1)]) != free) {
            if (cur == key) {
                return false;
            } else {
                while (true) {
                    if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == free) {
                        break;
                    } else if (cur == key) {
                        return false;
                    }
                }
            }
        }
        tmpKeys[index] = key;
        int curSize = ++size;
        if (isCanGrow(curSize / keys.length)) {
            rehash(size << 1);
        }
        return true;
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean contains(char key) {
        if (size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 删除
     *
     * @param key
     * @return true删除成功 false删除失败
     */
    public boolean remove(char key) {
        if (size == 0) {
            return false;
        }
        char free;
        if (key != (free = freeValue)) {
            char[] tmpKeys = keys;
            int capacityMask = tmpKeys.length - 1;
            int index;
            char cur;
            if ((cur = tmpKeys[index = mixCharKey(key) & capacityMask]) != key) {
                if (cur == free) {
                    return false;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            break;
                        } else if (cur == free) {
                            return false;
                        }
                    }
                }
            }
            int indexToRemove = index;
            int indexToShift = indexToRemove;
            int shiftDistance = 1;
            char keyToShift;
            while (true) {
                indexToShift = (indexToShift - 1) & capacityMask;
                if ((keyToShift = tmpKeys[indexToShift]) == free) {
                    break;
                }
                if (((mixCharKey(keyToShift) - indexToShift) & capacityMask) >= shiftDistance) {
                    tmpKeys[indexToRemove] = keyToShift;
                    indexToRemove = indexToShift;
                    shiftDistance = 1;
                } else {
                    shiftDistance++;
                }
            }

            tmpKeys[indexToRemove] = free;
            size--;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void clear() {
        this.freeValue = 0;
        if (keys.length > clearMaxSize) {
            init(clearMaxSize);
        } else {
            Arrays.fill(keys, freeValue);
        }
        size = 0;
    }

    /**
     * 扩容
     *
     * @param capacity
     */
    public void reSizeCapacity(int capacity) {
        if (capacity > keys.length) {
            rehash(reCountCapacity(capacity));
        }
    }

    private char changeFree() {
        char newFreeValue = findNewFreeOrRemoved();
        replaceAll(keys, freeValue, newFreeValue);
        freeValue = newFreeValue;
        return newFreeValue;
    }

    private char findNewFreeOrRemoved() {
        char tmpFreeValue = freeValue;
        char newFreeValue;
        do {
            newFreeValue = (char) RandomUtils.nextInt();
        } while ((newFreeValue == tmpFreeValue) || index(newFreeValue) >= 0);
        return newFreeValue;
    }

    /**
     * 查找key的索引
     *
     * @param key
     * @return -1表示不存在这个key
     */
    private int index(char key) {
        char free;
        if (key != (free = freeValue)) {
            char[] tmpKeys = keys;
            int capacityMask;
            int index;
            char cur;
            if ((cur = tmpKeys[index = mixCharKey(key) & (capacityMask = tmpKeys.length - 1)]) == key) {
                return index;
            } else {
                if (cur == free) {
                    return -1;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            return index;
                        } else if (cur == free) {
                            return -1;
                        }
                    }
                }
            }
        } else {
            return -1;
        }
    }

    private void rehash(int newCapacity) {
        char free = freeValue;
        char[] tmpKeys = keys;
        init(newCapacity);
        char[] newKeys = keys;
        int capacityMask = newKeys.length - 1;
        char key;
        int index;
        for (int i = tmpKeys.length - 1; i >= 0; i--) {
            if ((key = tmpKeys[i]) != free) {
                if (newKeys[index = mixCharKey(key) & capacityMask] != free) {
                    while (true) {
                        if (newKeys[(index = (index - 1) & capacityMask)] == free) {
                            break;
                        }
                    }
                }
                newKeys[index] = key;
            }
        }
    }

    @Override
    public String toString() {
        char freeValue = this.freeValue;
        char[] keys = this.keys;
        char key;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                stringBuilder.append(key).append(", ");
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    /**
     * 不可变的遍历
     *
     * @param consumer
     */
    public void foreachImmutable(CharConsumer consumer) {
        if (size == 0) {
            return;
        }
        char freeValue = this.freeValue;
        char[] keys = this.keys;
        char key;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key);
            }
        }
    }

    /**
     * 可变的遍历(需要注意在遍历时删除元素，有可能会重复遍历某一元素)
     *
     * @param consumer
     */
    public void foreachMutable(CharConsumer consumer) {
        if (size == 0) {
            return;
        }
        char freeValue = this.freeValue;
        char[] keys = this.keys;
        char key;

        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key);
                if (key != keys[i]) {
                    i--;
                }
            }
        }
    }
}
