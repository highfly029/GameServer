package com.highfly029.utils.collection.consumer;

/**
 * @ClassName LongIntConsumer
 * @Description LongIntConsumer
 * @Author liyunpeng
 **/
public interface LongIntConsumer {
    void accept(long k, int v);
}
