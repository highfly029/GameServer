package com.highfly029.utils.collection.consumer;

/**
 * @ClassName LongObjectConsumer
 * @Description LongObjectConsumer
 * @Author liyunpeng
 **/
public interface LongObjectConsumer<V> {
    void accept(long k, V v);
}
