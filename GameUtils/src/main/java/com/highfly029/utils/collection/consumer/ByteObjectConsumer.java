package com.highfly029.utils.collection.consumer;

/**
 * @ClassName ByteObjectConsumer
 * @Description ByteObjectConsumer
 * @Author liyunpeng
 **/
public interface ByteObjectConsumer<V> {
    void accept(byte k, V v);
}
