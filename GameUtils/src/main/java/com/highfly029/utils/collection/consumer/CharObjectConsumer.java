package com.highfly029.utils.collection.consumer;

/**
 * @ClassName CharObjectConsumer
 * @Description CharObjectConsumer
 * @Author liyunpeng
 **/
public interface CharObjectConsumer<V> {
    void accept(char k, V v);
}
