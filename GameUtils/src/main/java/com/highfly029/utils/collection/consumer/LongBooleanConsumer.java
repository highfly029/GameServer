package com.highfly029.utils.collection.consumer;

/**
 * @ClassName LongBooleanConsumer
 * @Description LongBooleanConsumer
 * @Author liyunpeng
 **/
public interface LongBooleanConsumer {
    void accept(long k, boolean v);
}
