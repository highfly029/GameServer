package com.highfly029.utils.collection;

import com.highfly029.utils.collection.consumer.IntConsumer;

/**
 * @ClassName IntQueue
 * @Description IntQueue
 * @Author liyunpeng
 **/
public class IntQueue extends BaseQueue {
    public static final IntQueue empty = new IntQueue();
    private int[] values;

    public IntQueue() {
        init(reCountCapacity(0));
    }

    public IntQueue(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
    }

    /**
     * 初始化
     *
     * @param capacity
     */
    private void init(int capacity) {
        values = new int[capacity];
        size = 0;
    }

    /**
     * 增加数组长度
     *
     * @param capacity
     */
    private void grow(int capacity) {
        int[] array = new int[capacity];
        if (size != 0) {
            if (startIndex < endIndex) {
                System.arraycopy(values, 0, array, 0, size);
            } else {
                int len = values.length - startIndex;
                System.arraycopy(values, startIndex, array, 0, len);
                System.arraycopy(values, 0, array, len, endIndex);
            }
        }
        startIndex = 0;
        endIndex = size;
        values = array;
    }

    /**
     * 尾部增加元素
     *
     * @param e
     * @return
     */
    public boolean offer(int e) {
        if (size == values.length) {
            grow(size << 1);
        }

        int end = endIndex;
        if (end >= values.length) {
            end = 0;
        }
        values[end++] = e;

        endIndex = end;
        size++;
        return true;
    }

    /**
     * 删除头部元素并返回
     *
     * @return
     */
    public int poll() {
        if (size == 0) {
            return 0;
        }
        int start = startIndex;
        int e = values[start];
        values[start] = 0;
        if (++start == values.length) {
            start = 0;
        }
        startIndex = start;
        size--;
        return e;
    }

    /**
     * 查看头部元素
     *
     * @return
     */
    public int peek() {
        if (size == 0) {
            return 0;
        }
        return values[startIndex];
    }

    /**
     * 不可变的遍历
     *
     * @param consumer
     */
    public void foreachImmutable(IntConsumer consumer) {
        if (size == 0) {
            return;
        }

        if (endIndex > startIndex) {
            for (int i = startIndex; i < endIndex; i++) {
                consumer.accept(values[i]);
            }
        } else {
            for (int i = startIndex, len = values.length; i < len; i++) {
                consumer.accept(values[i]);
            }
            for (int i = 0; i < endIndex; i++) {
                consumer.accept(values[i]);
            }
        }
    }

    public void clear() {
        size = 0;
        startIndex = 0;
        endIndex = 0;
        init(defaultSize);
    }
}
