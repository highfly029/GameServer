package com.highfly029.utils.collection.creater;

/**
 * @ClassName ObjectArrayCreateInterface
 * @Description ObjectArrayCreateInterface
 * @Author liyunpeng
 **/
public interface ObjectArrayCreateInterface<E> {
    E[] create(int length);
}
