package com.highfly029.utils.collection;

import com.highfly029.utils.collection.consumer.ObjectConsumer;
import com.highfly029.utils.collection.creater.ObjectArrayCreateInterface;

/**
 * @ClassName ObjectSet
 * @Description ObjectSet
 * @Author liyunpeng
 **/
public class ObjectSet<K> extends BaseHash {
    public static final ObjectSet empty = new ObjectSet();
    private K[] keys;
    private ObjectArrayCreateInterface<K> keyCreate;

    public ObjectSet() {
        init(reCountCapacity(0));
    }

    public ObjectSet(ObjectArrayCreateInterface<K> keyCreate) {
        this.keyCreate = keyCreate;
        init(reCountCapacity(0));
    }

    public ObjectSet(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    public ObjectSet(int capacity, ObjectArrayCreateInterface<K> keyCreate) {
        this.keyCreate = keyCreate;
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    private void init(int capacity) {
        keys = createKeyObjectArray(capacity);
    }

    private K[] createKeyObjectArray(int length) {
        if (keyCreate != null) {
            return keyCreate.create(length);
        }
        return (K[]) new Object[length];
    }

    /**
     * 获取map的最大容量
     *
     * @return
     */
    public int getCapacity() {
        return keys.length;
    }

    public final K[] getKeys() {
        return keys;
    }

    /**
     * 增加
     *
     * @param key
     * @return true增加成功 false增加失败
     */
    public boolean add(K key) {
        if (key == null) {
            return false;
        }
        K[] tmpKeys = keys;
        int capacityMask;
        int index;
        K cur;
        if ((cur = tmpKeys[index = mixObjectKey(key.hashCode()) & (capacityMask = tmpKeys.length - 1)]) != null) {
            if (cur == key || cur.equals(key)) {
                return false;
            } else {
                while (true) {
                    if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == null) {
                        break;
                    } else if (cur == key || cur.equals(key)) {
                        return false;
                    }
                }
            }
        }
        tmpKeys[index] = key;
        int curSize = ++size;
        if (isCanGrow(curSize / keys.length)) {
            rehash(size << 1);
        }
        return true;
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean contains(K key) {
        if (key == null || size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 删除
     *
     * @param key
     * @return true删除成功 false删除失败
     */
    public boolean remove(K key) {
        if (key == null || size == 0) {
            return false;
        }
        K[] tmpKeys = keys;
        int capacityMask = tmpKeys.length - 1;
        int index;
        K cur;
        if ((cur = tmpKeys[index = mixObjectKey(key.hashCode()) & capacityMask]) == key || (cur != null && cur.equals(key))) {
        } else if (cur == null) {
            return false;
        } else {
            while (true) {
                if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key || (cur != null && cur.equals(key))) {
                    break;
                } else if (cur == null) {
                    return false;
                }
            }
        }


        int indexToRemove = index;
        int indexToShift = indexToRemove;
        int shiftDistance = 1;
        K keyToShift;
        while (true) {
            indexToShift = (indexToShift - 1) & capacityMask;
            if ((keyToShift = tmpKeys[indexToShift]) == null) {
                break;
            }
            if (((mixObjectKey(keyToShift.hashCode()) - indexToShift) & capacityMask) >= shiftDistance) {
                tmpKeys[indexToRemove] = keyToShift;
                indexToRemove = indexToShift;
                shiftDistance = 1;
            } else {
                shiftDistance++;
            }
        }

        tmpKeys[indexToRemove] = null;
        size--;
        return true;
    }

    @Override
    public void clear() {
        K[] tmpKeys = this.keys;
        if (tmpKeys.length > clearMaxSize) {
            init(clearMaxSize);
        } else {
            for (int i = 0, len = tmpKeys.length; i < len; i++) {
                tmpKeys[i] = null;
            }
        }
        size = 0;
    }

    /**
     * 扩容
     *
     * @param capacity
     */
    public void reSizeCapacity(int capacity) {
        if (capacity > keys.length) {
            rehash(reCountCapacity(capacity));
        }
    }

    /**
     * 查找key的索引
     *
     * @param key
     * @return -1表示不存在这个key
     */
    private int index(K key) {
        if (key != null) {
            K[] tmpKeys = keys;
            int capacityMask;
            int index;
            K cur;
            if ((cur = tmpKeys[index = mixObjectKey(key.hashCode()) & (capacityMask = tmpKeys.length - 1)]) == key || (cur != null && cur.equals(key))) {
                return index;
            } else {
                if (cur == null) {
                    return -1;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key || (cur != null && cur.equals(key))) {
                            return index;
                        } else if (cur == null) {
                            return -1;
                        }
                    }
                }
            }
        } else {
            return -1;
        }
    }

    private void rehash(int newCapacity) {
        K[] tmpKeys = keys;
        keys = createKeyObjectArray(newCapacity);
        K[] newKeys = keys;
        int capacityMask = newKeys.length - 1;
        K key;
        int index;
        for (int i = tmpKeys.length - 1; i >= 0; i--) {
            if ((key = tmpKeys[i]) != null) {
                if (newKeys[index = mixObjectKey(key.hashCode()) & capacityMask] != null) {
                    while (true) {
                        if (newKeys[(index = (index - 1) & capacityMask)] == null) {
                            break;
                        }
                    }
                }
                newKeys[index] = key;
            }
        }
    }

    @Override
    public String toString() {
        K[] keys = this.keys;
        K key;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != null) {
                stringBuilder.append(key).append(", ");
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    /**
     * 不可变的遍历
     *
     * @param consumer
     */
    public void foreachImmutable(ObjectConsumer<? super K> consumer) {
        if (size == 0) {
            return;
        }
        K[] keys = this.keys;
        K key;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != null) {
                consumer.accept(key);
            }
        }
    }

    /**
     * 可变的遍历(需要注意在遍历时删除元素，有可能会重复遍历某一元素)
     *
     * @param consumer
     */
    public void foreachMutable(ObjectConsumer<? super K> consumer) {
        if (size == 0) {
            return;
        }
        K[] keys = this.keys;
        K key;

        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != null) {
                consumer.accept(key);
                if (key != keys[i]) {
                    i--;
                }
            }
        }
    }
}
