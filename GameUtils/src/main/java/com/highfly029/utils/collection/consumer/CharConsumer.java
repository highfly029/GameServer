package com.highfly029.utils.collection.consumer;

/**
 * @ClassName CharConsumer
 * @Description CharConsumer
 * @Author liyunpeng
 **/
public interface CharConsumer {
    void accept(char k);
}
