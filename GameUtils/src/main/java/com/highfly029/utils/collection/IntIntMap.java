package com.highfly029.utils.collection;

import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.UnsafeConstants;
import com.highfly029.utils.collection.consumer.IntIntConsumer;

/**
 * @ClassName IntIntMap
 * @Description IntIntMap
 * @Author liyunpeng
 **/
public class IntIntMap extends BaseHash {
    public static final IntIntMap empty = new IntIntMap();
    private static final long INT_MASK = 0xFFFFFFFFL;
    private long[] table;
    private int freeValue;

    public IntIntMap() {
        init(reCountCapacity(0));
    }

    public IntIntMap(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    private void init(int capacity) {
        table = new long[capacity];
        if (freeValue != 0) {
            fillKeys(table, freeValue);
        }
    }

    /**
     * 获取map的最大容量
     *
     * @return
     */
    public int getCapacity() {
        return table.length;
    }

    public final int getFreeValue() {
        return freeValue;
    }

    public final long[] getTable() {
        return table;
    }

    /**
     * 增加
     *
     * @param key
     * @param value
     */
    public void put(int key, int value) {
        int free;
        if (key == (free = freeValue)) {
            free = changeFree();
        }
        long[] tab = table;
        int capacityMask, index;
        int cur;
        if ((cur = (int) (tab[index = mixIntKey(key) & (capacityMask = tab.length - 1)])) == free) {
            tab[index] = ((((long) key) & INT_MASK) | (((long) value) << 32));
            postInsertHook();
            return;
        } else {
            if (cur != key) {
                while (true) {
                    if ((cur = (int) (tab[(index = (index - 1) & capacityMask)])) == free) {
                        tab[index] = ((((long) key) & INT_MASK) | (((long) value) << 32));
                        postInsertHook();
                        return;
                    } else if (cur == key) {
                        break;
                    }
                }
            }
            UnsafeConstants.U.putInt(tab, UnsafeConstants.LONG_BASE + UnsafeConstants.INT_VALUE_OFFSET + (((long) (index)) << UnsafeConstants.LONG_SCALE_SHIFT), value);
            return;
        }
    }

    /**
     * 叠加
     *
     * @param key
     * @param value
     * @return
     */
    public int addValue(int key, int value) {
        int free;
        if (key == (free = freeValue)) {
            free = changeFree();
        }
        long[] tab = table;
        int capacityMask, index;
        int cur;
        long entry;
        keyPresent:
        if ((cur = (int) (entry = tab[index = mixIntKey(key) & (capacityMask = tab.length - 1)])) != key) {
            keyAbsent:
            if (cur != free) {
                while (true) {
                    if ((cur = (int) (entry = tab[(index = (index - 1) & capacityMask)])) == key) {
                        break keyPresent;
                    } else if (cur == free) {
                        break keyAbsent;
                    }
                }
            }
            int newValue = value;
            tab[index] = ((((long) key) & INT_MASK) | (((long) newValue) << 32));
            postInsertHook();
            return newValue;
        }
        int newValue = (int) (entry >>> 32) + value;
        UnsafeConstants.U.putInt(tab, UnsafeConstants.LONG_BASE + UnsafeConstants.INT_VALUE_OFFSET + (((long) (index)) << UnsafeConstants.LONG_SCALE_SHIFT), newValue);
        return newValue;
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean contains(int key) {
        if (size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 获取
     *
     * @param key
     * @return
     */
    public int get(int key) {
        if (size == 0) {
            return 0;
        }
        int free;
        if (key != (free = freeValue)) {
            long[] tab = table;
            int capacityMask, index;
            int cur;
            long entry;
            if ((cur = (int) (entry = tab[index = mixIntKey(key) & (capacityMask = tab.length - 1)])) == key) {
                return (int) (entry >>> 32);
            } else {
                if (cur == free) {
                    return 0;
                } else {
                    while (true) {
                        if ((cur = (int) (entry = tab[(index = (index - 1) & capacityMask)])) == key) {
                            return (int) (entry >>> 32);
                        } else if (cur == free) {
                            return 0;
                        }
                    }
                }
            }
        } else {
            return 0;
        }
    }

    /**
     * 获取
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public int getOrDefault(int key, int defaultValue) {
        if (size == 0) {
            return defaultValue;
        }
        int free;
        if (key != (free = freeValue)) {
            long[] tab = table;
            int capacityMask, index;
            int cur;
            long entry;
            if ((cur = (int) (entry = tab[index = mixIntKey(key) & (capacityMask = tab.length - 1)])) == key) {
                return (int) (entry >>> 32);
            } else {
                if (cur == free) {
                    return defaultValue;
                } else {
                    while (true) {
                        if ((cur = (int) (entry = tab[(index = (index - 1) & capacityMask)])) == key) {
                            return (int) (entry >>> 32);
                        } else if (cur == free) {
                            return defaultValue;
                        }
                    }
                }
            }
        } else {
            return defaultValue;
        }
    }

    /**
     * 删除
     *
     * @param key
     * @return
     */
    public int remove(int key) {
        if (size == 0) {
            return 0;
        }
        int free;
        if (key != (free = freeValue)) {
            long[] tab = table;
            int capacityMask = tab.length - 1;
            int index;
            int cur;
            long entry;
            if ((cur = (int) (entry = tab[index = mixIntKey(key) & capacityMask])) != key) {
                if (cur == free) {
                    return 0;
                } else {
                    while (true) {
                        if ((cur = (int) (entry = tab[(index = (index - 1) & capacityMask)])) == key) {
                            break;
                        } else if (cur == free) {
                            return 0;
                        }
                    }
                }
            }
            // key is present
            int val = (int) (entry >>> 32);
            int indexToRemove = index;
            int indexToShift = indexToRemove;
            int shiftDistance = 1;
            while (true) {
                indexToShift = (indexToShift - 1) & capacityMask;
                int keyToShift;
                if ((keyToShift = (int) (entry = tab[indexToShift])) == free) {
                    break;
                }
                if (((mixIntKey(keyToShift) - indexToShift) & capacityMask) >= shiftDistance) {
                    tab[indexToRemove] = entry;
                    indexToRemove = indexToShift;
                    shiftDistance = 1;
                } else {
                    shiftDistance++;
                }
            }
            UnsafeConstants.U.putInt(tab, UnsafeConstants.LONG_BASE + UnsafeConstants.INT_KEY_OFFSET + (((long) (indexToRemove)) << UnsafeConstants.LONG_SCALE_SHIFT), free);
            size--;
            return val;
        } else {
            return 0;
        }
    }

    @Override
    public void clear() {
        this.freeValue = 0;
        if (table.length > clearMaxSize) {
            init(clearMaxSize);
        } else {
            fillKeys(table, freeValue);
        }
        size = 0;
    }

    /**
     * 扩容
     *
     * @param capacity
     */
    public void reSizeCapacity(int capacity) {
        if (capacity > table.length) {
            rehash(reCountCapacity(capacity));
        }
    }

    /**
     * 如果不存在才添加
     *
     * @param key
     * @param value
     * @return
     */
    public int putIfAbsent(int key, int value) {
        int free;
        if (key == (free = freeValue)) {
            free = changeFree();
        }
        long[] tab = table;
        int capacityMask, index;
        int cur;
        long entry;
        if ((cur = (int) (entry = tab[index = mixIntKey(key) & (capacityMask = tab.length - 1)])) == free) {
            tab[index] = ((((long) key) & INT_MASK) | (((long) value) << 32));
            postInsertHook();
            return 0;
        } else {
            if (cur == key) {
                return (int) (entry >>> 32);
            } else {
                while (true) {
                    if ((cur = (int) (entry = tab[(index = (index - 1) & capacityMask)])) == free) {
                        tab[index] = ((((long) key) & INT_MASK) | (((long) value) << 32));
                        postInsertHook();
                        return 0;
                    } else if (cur == key) {
                        return (int) (entry >>> 32);
                    }
                }
            }
        }
    }

    private void postInsertHook() {
        int curSize = ++size;
        if (isCanGrow(curSize / table.length)) {
            rehash(size << 1);
        }
    }


    private int changeFree() {
        int newFreeValue = findNewFreeOrRemoved();
        replaceAllKeys(table, freeValue, newFreeValue);
        freeValue = newFreeValue;
        return newFreeValue;
    }

    private int findNewFreeOrRemoved() {
        int tmpFreeValue = freeValue;
        int newFreeValue;
        do {
            newFreeValue = RandomUtils.nextInt();
        } while ((newFreeValue == tmpFreeValue) || index(newFreeValue) >= 0);
        return newFreeValue;
    }

    /**
     * 查找key的索引
     *
     * @param key
     * @return -1表示不存在这个key
     */
    private int index(int key) {
        int free;
        if (key != (free = freeValue)) {
            long[] tab = table;
            int capacityMask, index;
            int cur;
            if ((cur = (int) (tab[index = mixIntKey(key) & (capacityMask = tab.length - 1)])) == key) {
                return index;
            } else {
                if (cur == free) {
                    return -1;
                } else {
                    while (true) {
                        if ((cur = (int) (tab[(index = (index - 1) & capacityMask)])) == key) {
                            return index;
                        } else if (cur == free) {
                            return -1;
                        }
                    }
                }
            }
        } else {
            return -1;
        }
    }

    private void rehash(int newCapacity) {
        int free = freeValue;
        long[] tab = table;
        long entry;
        init(newCapacity);
        long[] newTab = table;
        int capacityMask = newTab.length - 1;
        for (int i = tab.length - 1; i >= 0; i--) {
            int key;
            if ((key = (int) (entry = tab[i])) != free) {
                int index;
                if (UnsafeConstants.U.getInt(newTab, UnsafeConstants.LONG_BASE + UnsafeConstants.INT_KEY_OFFSET + (((long) (index = mixIntKey(key) & capacityMask)) << UnsafeConstants.LONG_SCALE_SHIFT)) != free) {
                    while (true) {
                        if (UnsafeConstants.U.getInt(newTab, UnsafeConstants.LONG_BASE + UnsafeConstants.INT_KEY_OFFSET + (((long) ((index = (index - 1) & capacityMask))) << UnsafeConstants.LONG_SCALE_SHIFT)) == free) {
                            break;
                        }
                    }
                }
                newTab[index] = entry;
            }
        }
    }

    @Override
    public String toString() {
        int freeValue = this.freeValue;
        long[] table = this.table;
        long entry;
        int key;
        int value;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0, len = table.length; i < len; i++) {
            entry = table[i];
            if ((key = (int) entry) != freeValue) {
                value = (int) (entry >>> 32);
                stringBuilder.append(key).append(":").append(value).append(", ");
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    /**
     * 不可变的遍历
     *
     * @param consumer
     */
    public void foreachImmutable(IntIntConsumer consumer) {
        if (size == 0) {
            return;
        }
        int freeValue = this.freeValue;
        long[] table = this.table;
        long entry;
        int key;
        int value;
        for (int i = 0, len = table.length; i < len; i++) {
            entry = table[i];
            if ((key = (int) entry) != freeValue) {
                value = (int) (entry >>> 32);
                consumer.accept(key, value);
            }
        }
    }

    /**
     * 可变的遍历(需要注意在遍历时删除元素，有可能会重复遍历某一元素)
     *
     * @param consumer
     */
    public void foreachMutable(IntIntConsumer consumer) {
        if (size == 0) {
            return;
        }
        int freeValue = this.freeValue;
        long[] table = this.table;
        long entry;
        int key;
        int value;
        for (int i = 0, len = table.length; i < len; i++) {
            entry = table[i];
            if ((key = (int) entry) != freeValue) {
                value = (int) (entry >>> 32);
                consumer.accept(key, value);
                if (key != (int) table[i]) {
                    i--;
                }
            }
        }
    }
}
