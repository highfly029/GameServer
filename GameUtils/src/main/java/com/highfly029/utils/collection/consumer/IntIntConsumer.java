package com.highfly029.utils.collection.consumer;

/**
 * @ClassName IntIntConsumer
 * @Description IntIntConsumer
 * @Author liyunpeng
 **/
public interface IntIntConsumer {
    void accept(int k, int v);
}
