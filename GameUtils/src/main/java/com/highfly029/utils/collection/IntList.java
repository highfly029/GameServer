package com.highfly029.utils.collection;

import java.util.Arrays;

/**
 * @ClassName IntList
 * @Description IntList
 * @Author liyunpeng
 **/
public class IntList extends BaseList {
    public static final IntList empty = new IntList();
    private int[] values;

    public IntList() {
        init(reCountCapacity(0));
    }

    public IntList(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    /**
     * 初始化
     *
     * @param capacity
     */
    private void init(int capacity) {
        values = new int[capacity];
        size = 0;
    }

    /**
     * 获取队列的最大容量
     *
     * @return
     */
    public int getCapacity() {
        return values.length;
    }

    /**
     * 获取队列里的数组
     *
     * @return
     */
    public final int[] getValues() {
        return values;
    }

    /**
     * 增加数组长度
     *
     * @param capacity
     */
    private void grow(int capacity) {
        int[] array = new int[capacity];
        System.arraycopy(values, 0, array, 0, size);
        values = array;
    }

    /**
     * 根据索引设置元素
     *
     * @param index
     * @param value
     */
    public void set(int index, int value) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
        values[index] = value;
    }

    /**
     * 根据索引获取元素
     *
     * @param index
     * @return
     */
    public int get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
        return values[index];
    }

    /**
     * 累加
     *
     * @param index
     * @param value
     * @return
     */
    public int accumulate(int index, int value) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
        return values[index] += value;
    }

    /**
     * 增加值
     *
     * @param value
     */
    public void add(int value) {
        if (size == values.length) {
            grow(size << 1);
        }
        values[size++] = value;
    }

    /**
     * 增加一组数组
     *
     * @param array
     */
    public void addArray(int[] array) {
        int newCapacity = size + array.length;
        if (newCapacity > values.length) {
            grow(reCountCapacity(newCapacity));
        }
        System.arraycopy(array, 0, values, size, array.length);
        size = newCapacity;
    }

    /**
     * 增加一组list
     *
     * @param list
     */
    public void addAll(IntList list) {
        if (list.isEmpty()) {
            return;
        }
        int newCapacity = size + list.size();
        if (newCapacity > values.length) {
            grow(reCountCapacity(newCapacity));
        }
        int[] array = list.getValues();
        System.arraycopy(array, 0, values, size, list.size());
        size = newCapacity;
    }

    /**
     * 在列表头部添加元素
     *
     * @param value
     */
    public void unshift(int value) {
        if (size == values.length) {
            grow(size << 1);
        }
        if (size > 0) {
            System.arraycopy(values, 0, values, 1, size);
        }
        values[0] = value;
        size++;
    }

    /**
     * 删除索引对应的元素
     *
     * @param index
     * @return
     */
    public int remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
        if (size == 0) {
            return 0;
        }
        int value = values[index];
        int numMoved = size - index - 1;
        if (numMoved > 0) {
            System.arraycopy(values, index + 1, values, index, numMoved);
        }
        size--;
        return value;
    }

    /**
     * 查找第一个等于元素的索引
     *
     * @param value
     * @return
     */
    public int indexOf(int value) {
        return indexOf(0, value);
    }

    /**
     * 是否包含
     *
     * @param value
     * @return
     */
    public boolean contains(int value) {
        return indexOf(value) >= 0;
    }

    /**
     * 从指定偏移量开始查找第一个等于元素的索引
     *
     * @param offset
     * @param value
     * @return
     */
    public int indexOf(int offset, int value) {
        if (size == 0) {
            return -1;
        }
        if (offset < 0 || offset >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(offset));
        }
        int[] tmpValues = values;
        for (int i = offset, len = size; i < len; i++) {
            if (tmpValues[i] == value) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 从尾部开始,向前查找第一个等于元素的索引
     *
     * @param value
     * @return
     */
    public int lastIndexOf(int value) {
        return lastIndexOf(size - 1, value);
    }

    /**
     * 从尾部开始,从指定偏移量开始向前查找第一个等于元素的索引
     *
     * @param offset
     * @param value
     * @return
     */
    public int lastIndexOf(int offset, int value) {
        if (size == 0) {
            return -1;
        }
        if (offset < 0 || offset >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(offset));
        }
        int[] tmpValues = values;
        for (int i = offset; i >= 0; i--) {
            if (tmpValues[i] == value) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 在指定索引位置插入元素
     *
     * @param index
     * @param value
     */
    public void insert(int index, int value) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
        if (size == values.length) {
            int[] array = new int[size << 1];
            System.arraycopy(values, 0, array, 0, index);
            System.arraycopy(values, index, array, index + 1, size - index);
            array[index] = value;
            values = array;
        } else {
            System.arraycopy(values, index, values, index + 1, size - index);
            values[index] = value;
        }
        size++;
    }

    /**
     * 扩容
     *
     * @param capacity
     */
    public void reSizeCapacity(int capacity) {
        if (capacity > values.length) {
            grow(reCountCapacity(capacity));
        }
    }

    /**
     * 转换成数组
     *
     * @return
     */
    public int[] toArray() {
        if (size == 0) {
            return new int[0];
        }
        int[] array = new int[size];
        System.arraycopy(values, 0, array, 0, size);
        return array;
    }

    /**
     * 排序
     */
    public void sort() {
        if (size == 0) {
            return;
        }
        Arrays.sort(values, 0, size);
    }

    /**
     * 反转
     */
    public void reverse() {
        if (size == 0) {
            return;
        }
        int halfSize = size >> 1;
        int maxIndex = size - 1;
        int[] tmpValues = values;
        int tmp;
        for (int i = 0; i < halfSize; i++) {
            tmp = tmpValues[i];
            tmpValues[i] = tmpValues[maxIndex - i];
            tmpValues[maxIndex - i] = tmp;
        }
    }

    @Override
    public void clear() {
        if (values.length > clearMaxSize) {
            init(clearMaxSize);
        } else {
            size = 0;
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0; i < size; i++) {
            stringBuilder.append(values[i]);
            if (i == size - 1) {
                return stringBuilder.append("]").toString();
            }
            stringBuilder.append(", ");
        }
        return "[]";
    }

    /**
     * 求两个list交集
     *
     * @param list1
     * @param list2
     * @param retList
     */
    public static void intersect(IntList list1, IntList list2, IntList retList) {
        int size1 = list1.size();
        int size2 = list2.size();
        if (size1 == 0 || size2 == 0) {
            return;
        }
        int[] values1 = list1.getValues();
        int[] values2 = list2.getValues();
        for (int i = 0; i < size1; i++) {
            for (int j = 0; j < size2; j++) {
                if (values1[i] == values2[j]) {
                    retList.add(values1[i]);
                    break;
                }
            }
        }

    }

    /**
     * 求两个list并集
     *
     * @param list1
     * @param list2
     * @param retList
     */
    public static void union(IntList list1, IntList list2, IntList retList) {
        int size1 = list1.size();
        int size2 = list2.size();
        int[] values1 = list1.getValues();
        int[] values2 = list2.getValues();
        retList.addAll(list2);
        for (int i = 0; i < size1; i++) {
            boolean isSame = false;
            for (int j = 0; j < size2; j++) {
                if (values1[i] == values2[j]) {
                    isSame = true;
                    break;
                }
            }
            if (isSame == false) {
                retList.add(values1[i]);
            }
        }
    }
}
