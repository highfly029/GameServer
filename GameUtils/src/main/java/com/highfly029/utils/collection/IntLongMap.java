package com.highfly029.utils.collection;

import java.util.Arrays;

import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.collection.consumer.IntLongConsumer;

/**
 * @ClassName IntLongMap
 * @Description IntLongMap
 * @Author liyunpeng
 **/
public class IntLongMap extends BaseHash {
    public static final IntLongMap empty = new IntLongMap();
    private int freeValue;
    private int[] keys;
    private long[] values;

    public IntLongMap() {
        init(reCountCapacity(0));
    }

    public IntLongMap(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    private void init(int capacity) {
        keys = new int[capacity];
        values = new long[capacity];
        if (freeValue != 0) {
            Arrays.fill(keys, freeValue);
        }
    }

    /**
     * 获取map的最大容量
     *
     * @return
     */
    public int getCapacity() {
        return keys.length;
    }

    public final int getFreeValue() {
        return freeValue;
    }

    public final int[] getKeys() {
        return keys;
    }

    public final long[] getValues() {
        return values;
    }

    /**
     * 增加
     *
     * @param key
     * @param value
     */
    public void put(int key, long value) {
        int index = insert(key, value);
        if (index < 0) {
            return;
        } else {
            values[index] = value;
            return;
        }
    }

    /**
     * 叠加
     *
     * @param key
     * @param value
     * @return
     */
    public long addValue(int key, long value) {
        int index = insert(key, value);
        if (index < 0) {
            return value;
        } else {
            return values[index] += value;
        }
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean contains(int key) {
        if (size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 获取
     *
     * @param key
     * @return
     */
    public long get(int key) {
        if (size == 0) {
            return 0L;
        }
        int index = index(key);
        if (index >= 0) {
            return values[index];
        } else {
            return 0L;
        }
    }

    /**
     * 获取
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public long getOrDefault(int key, long defaultValue) {
        if (size == 0) {
            return defaultValue;
        }
        int index = index(key);
        if (index >= 0) {
            return values[index];
        } else {
            return defaultValue;
        }
    }

    /**
     * 删除
     *
     * @param key
     * @return
     */
    public long remove(int key) {
        if (size == 0) {
            return 0L;
        }
        int free;
        if (key != (free = freeValue)) {
            int[] tmpKeys = keys;
            int capacityMask = tmpKeys.length - 1;
            int index;
            int cur;
            if ((cur = tmpKeys[index = mixIntKey(key) & capacityMask]) != key) {
                if (cur == free) {
                    return 0L;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            break;
                        } else if (cur == free) {
                            return 0L;
                        }
                    }
                }
            }

            long[] tmpValues = values;
            long value = tmpValues[index];
            int indexToRemove = index;
            int indexToShift = indexToRemove;
            int shiftDistance = 1;
            int keyToShift;
            while (true) {
                indexToShift = (indexToShift - 1) & capacityMask;
                if ((keyToShift = tmpKeys[indexToShift]) == free) {
                    break;
                }
                if (((mixIntKey(keyToShift) - indexToShift) & capacityMask) >= shiftDistance) {
                    tmpKeys[indexToRemove] = keyToShift;
                    tmpValues[indexToRemove] = tmpValues[indexToShift];
                    indexToRemove = indexToShift;
                    shiftDistance = 1;
                } else {
                    shiftDistance++;
                }
            }

            tmpKeys[indexToRemove] = free;
            size--;
            return value;
        } else {
            return 0;
        }
    }

    @Override
    public void clear() {
        this.freeValue = 0;
        if (keys.length > clearMaxSize) {
            init(clearMaxSize);
        } else {
            Arrays.fill(keys, freeValue);
        }
        size = 0;
    }

    /**
     * 扩容
     *
     * @param capacity
     */
    public void reSizeCapacity(int capacity) {
        if (capacity > keys.length) {
            rehash(reCountCapacity(capacity));
        }
    }

    /**
     * 如果不存在才添加
     *
     * @param key
     * @param value
     * @return
     */
    public long putIfAbsent(int key, long value) {
        int index = insert(key, value);
        if (index < 0) {
            return value;
        } else {
            return values[index];
        }
    }

    /**
     * 插入
     *
     * @param key
     * @param value
     * @return 索引
     */
    private int insert(int key, long value) {
        int free;
        if (key == (free = freeValue)) {
            free = changeFree();
        }
        int[] tmpKeys = keys;
        int capacityMask;
        int index;
        int cur;
        if ((cur = tmpKeys[index = mixIntKey(key) & (capacityMask = tmpKeys.length - 1)]) != free) {
            if (cur == key) {
                return index;
            } else {
                while (true) {
                    if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == free) {
                        break;
                    } else if (cur == key) {
                        return index;
                    }
                }
            }
        }
        tmpKeys[index] = key;
        values[index] = value;
        int curSize = ++size;
        if (isCanGrow(curSize / keys.length)) {
            rehash(size << 1);
        }
        return -1;
    }

    private int changeFree() {
        int newFreeValue = findNewFreeOrRemoved();
        replaceAll(keys, freeValue, newFreeValue);
        freeValue = newFreeValue;
        return newFreeValue;
    }

    private int findNewFreeOrRemoved() {
        int tmpFreeValue = freeValue;
        int newFreeValue;
        do {
            newFreeValue = RandomUtils.nextInt();
        } while ((newFreeValue == tmpFreeValue) || index(newFreeValue) >= 0);
        return newFreeValue;
    }

    /**
     * 查找key的索引
     *
     * @param key
     * @return -1表示不存在这个key
     */
    private int index(int key) {
        int free;
        if (key != (free = freeValue)) {
            int[] tmpKeys = keys;
            int capacityMask;
            int index;
            int cur;
            if ((cur = tmpKeys[index = mixIntKey(key) & (capacityMask = tmpKeys.length - 1)]) == key) {
                return index;
            } else {
                if (cur == free) {
                    return -1;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            return index;
                        } else if (cur == free) {
                            return -1;
                        }
                    }
                }
            }
        } else {
            return -1;
        }
    }

    private void rehash(int newCapacity) {
        int free = freeValue;
        int[] tmpKeys = keys;
        long[] tmpValues = values;
        init(newCapacity);
        int[] newKeys = keys;
        long[] newValues = values;
        int capacityMask = newKeys.length - 1;

        int key;
        int index;
        for (int i = tmpKeys.length - 1; i >= 0; i--) {
            if ((key = tmpKeys[i]) != free) {
                if (newKeys[index = mixIntKey(key) & capacityMask] != free) {
                    while (true) {
                        if (newKeys[(index = (index - 1) & capacityMask)] == free) {
                            break;
                        }
                    }
                }
                newKeys[index] = key;
                newValues[index] = tmpValues[i];
            }
        }
    }

    @Override
    public String toString() {
        int freeValue = this.freeValue;
        int[] keys = this.keys;
        long[] values = this.values;
        int key;
        long value;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                value = values[i];
                stringBuilder.append(key).append(":").append(value).append(", ");
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    /**
     * 不可变的遍历
     *
     * @param consumer
     */
    public void foreachImmutable(IntLongConsumer consumer) {
        if (size == 0) {
            return;
        }
        int freeValue = this.freeValue;
        int[] keys = this.keys;
        long[] values = this.values;
        int key;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key, values[i]);
            }
        }
    }

    /**
     * 可变的遍历(需要注意在遍历时删除元素，有可能会重复遍历某一元素)
     *
     * @param consumer
     */
    public void foreachMutable(IntLongConsumer consumer) {
        if (size == 0) {
            return;
        }
        int freeValue = this.freeValue;
        int[] keys = this.keys;
        long[] values = this.values;
        int key;

        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key, values[i]);
                if (key != keys[i]) {
                    i--;
                }
            }
        }
    }
}
