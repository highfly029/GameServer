package com.highfly029.utils.collection.consumer;

/**
 * @ClassName ObjectConsumer
 * @Description ObjectConsumer
 * @Author liyunpeng
 **/
public interface ObjectConsumer<K> {
    void accept(K k);
}
