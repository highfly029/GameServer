package com.highfly029.utils.collection;

import com.highfly029.utils.MathUtils;
import com.highfly029.utils.UnsafeConstants;


/**
 * @ClassName BaseHash
 * @Description BaseHash
 * @Author liyunpeng
 **/
public abstract class BaseHash {
    /**
     * = round(2 ^ 32 * (sqrt(5) - 1)), Java form of unsigned 2654435769
     */
    private static final int INT_PHI_MAGIC = -1640531527;
    /**
     * ~= round(2 ^ 64 * (sqrt(5) - 1)), Java form of 11400714819323198485
     */
    private static final long LONG_PHI_MAGIC = -7046029254386353131L;

    public static final int BYTE_CARDINALITY = 1 << 8;
    public static final int BYTE_MASK = 0xFF;
    /**
     * A prime slightly greater than <tt>{@link #BYTE_CARDINALITY} / 3</tt>.
     */
    public static final int BYTE_PERMUTATION_STEP = 97;

    protected static final int defaultSize = 4;
    protected static final int defaultClearMaxSize = 16;
    protected int clearMaxSize = defaultClearMaxSize;
    /**
     * 默认最小负载因子 1/3
     */
    private static final double DEFAULT_MIN_LOAD = 1.0 / 3.0;
    /**
     * 默认最大负载因子 2/3
     */
    private static final double DEFAULT_MAX_LOAD = 2.0 / 3.0;

    protected int size = 0;

    /**
     * 当前负载>2/3 扩容
     *
     * @param curLoad
     * @return
     */
    protected boolean isCanGrow(double curLoad) {
        return curLoad > DEFAULT_MAX_LOAD;
    }

    /**
     * 重新计算容量
     *
     * @param capacity
     * @return
     */
    protected int reCountCapacity(int capacity) {
        capacity = MathUtils.getNearlyPowerOfTwo(capacity);
        if (capacity < defaultSize) {
            capacity = defaultSize;
        }
        return capacity;
    }

    /**
     * 获取map元素数量
     *
     * @return
     */
    public final int size() {
        return size;
    }

    /**
     * 判断map是否为空
     *
     * @return
     */
    public final boolean isEmpty() {
        return size == 0;
    }

    /**
     * 清空map
     */
    public void clear() {
        size = 0;
    }

    public static void replaceAll(int[] array, int oldValue, int newValue) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == oldValue) {
                array[i] = newValue;
            }
        }
    }

    public static void replaceAll(char[] array, char oldValue, char newValue) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == oldValue) {
                array[i] = newValue;
            }
        }
    }

    public static void replaceAll(byte[] array, byte oldValue, byte newValue) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == oldValue) {
                array[i] = newValue;
            }
        }
    }

    public static void replaceAll(long[] array, long oldValue, long newValue) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == oldValue) {
                array[i] = newValue;
            }
        }
    }

    public static void replaceAllKeys(long[] table, int oldKey, int newKey) {
        long base = UnsafeConstants.LONG_BASE + UnsafeConstants.INT_KEY_OFFSET;
        for (long off = UnsafeConstants.LONG_SCALE * (long) table.length; (off -= UnsafeConstants.LONG_SCALE) >= 0L; ) {
            if (UnsafeConstants.U.getInt(table, base + off) == oldKey) {
                UnsafeConstants.U.putInt(table, base + off, newKey);
            }
        }
    }

    public static void fillKeys(long[] table, int key) {
        long base = UnsafeConstants.LONG_BASE + UnsafeConstants.INT_KEY_OFFSET;
        for (long off = UnsafeConstants.LONG_SCALE * (long) table.length; (off -= UnsafeConstants.LONG_SCALE) >= 0L; ) {
            UnsafeConstants.U.putInt(table, base + off, key);
        }
    }

    public static int mixObjectKey(int hash) {
        return hash ^ hash >> 16;
    }

    /**
     * long double通用
     *
     * @param key
     * @return
     */
    public static int mixLongKey(long key) {
        long h = key * LONG_PHI_MAGIC;
        h ^= h >> 32;
        return (int) (h ^ h >> 16);
    }

    /**
     * float int 通用
     *
     * @param key
     * @return
     */
    public static int mixIntKey(int key) {
        int h = key * INT_PHI_MAGIC;
        return h ^ h >> 16;
    }

    /**
     * short char通用
     *
     * @param key
     * @return
     */
    public static int mixShortKey(short key) {
        int h = key * INT_PHI_MAGIC;
        return h ^ h >> 10;
    }

    public static int mixCharKey(char key) {
        int h = key * INT_PHI_MAGIC;
        return h ^ h >> 10;
    }

    public static int mixByteKey(byte key) {
        int h = key * INT_PHI_MAGIC;
        return h ^ h >> 6;
    }
}
