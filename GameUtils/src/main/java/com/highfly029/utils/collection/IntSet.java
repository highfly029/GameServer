package com.highfly029.utils.collection;

import java.util.Arrays;

import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.collection.consumer.IntConsumer;

/**
 * @ClassName IntSet
 * @Description IntSet
 * @Author liyunpeng
 **/
public class IntSet extends BaseHash {
    public static final IntSet empty = new IntSet();
    private int freeValue;
    private int[] keys;

    public IntSet() {
        init(reCountCapacity(0));
    }

    public IntSet(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    private void init(int capacity) {
        keys = new int[capacity];
        if (freeValue != 0) {
            Arrays.fill(keys, freeValue);
        }
    }

    /**
     * 获取map的最大容量
     *
     * @return
     */
    public int getCapacity() {
        return keys.length;
    }

    public final int getFreeValue() {
        return freeValue;
    }

    public final int[] getKeys() {
        return keys;
    }

    /**
     * 增加
     *
     * @param key
     * @return true增加成功 false增加失败
     */
    public boolean add(int key) {
        int free;
        if (key == (free = freeValue)) {
            free = changeFree();
        }
        int[] tmpKeys = keys;
        int capacityMask;
        int index;
        int cur;
        if ((cur = tmpKeys[index = mixIntKey(key) & (capacityMask = tmpKeys.length - 1)]) != free) {
            if (cur == key) {
                return false;
            } else {
                while (true) {
                    if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == free) {
                        break;
                    } else if (cur == key) {
                        return false;
                    }
                }
            }
        }
        tmpKeys[index] = key;
        int curSize = ++size;
        if (isCanGrow(curSize / keys.length)) {
            rehash(size << 1);
        }
        return true;
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean contains(int key) {
        if (size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 删除
     *
     * @param key
     * @return true删除成功 false删除失败
     */
    public boolean remove(int key) {
        if (size == 0) {
            return false;
        }
        int free;
        if (key != (free = freeValue)) {
            int[] tmpKeys = keys;
            int capacityMask = tmpKeys.length - 1;
            int index;
            int cur;
            if ((cur = tmpKeys[index = mixIntKey(key) & capacityMask]) != key) {
                if (cur == free) {
                    return false;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            break;
                        } else if (cur == free) {
                            return false;
                        }
                    }
                }
            }
            int indexToRemove = index;
            int indexToShift = indexToRemove;
            int shiftDistance = 1;
            int keyToShift;
            while (true) {
                indexToShift = (indexToShift - 1) & capacityMask;
                if ((keyToShift = tmpKeys[indexToShift]) == free) {
                    break;
                }
                if (((mixIntKey(keyToShift) - indexToShift) & capacityMask) >= shiftDistance) {
                    tmpKeys[indexToRemove] = keyToShift;
                    indexToRemove = indexToShift;
                    shiftDistance = 1;
                } else {
                    shiftDistance++;
                }
            }

            tmpKeys[indexToRemove] = free;
            size--;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void clear() {
        this.freeValue = 0;
        if (keys.length > clearMaxSize) {
            init(clearMaxSize);
        } else {
            Arrays.fill(keys, freeValue);
        }
        size = 0;
    }

    /**
     * 扩容
     *
     * @param capacity
     */
    public void reSizeCapacity(int capacity) {
        if (capacity > keys.length) {
            rehash(reCountCapacity(capacity));
        }
    }

    private int changeFree() {
        int newFreeValue = findNewFreeOrRemoved();
        replaceAll(keys, freeValue, newFreeValue);
        freeValue = newFreeValue;
        return newFreeValue;
    }

    private int findNewFreeOrRemoved() {
        int tmpFreeValue = freeValue;
        int newFreeValue;
        do {
            newFreeValue = RandomUtils.nextInt();
        } while ((newFreeValue == tmpFreeValue) || index(newFreeValue) >= 0);
        return newFreeValue;
    }

    /**
     * 查找key的索引
     *
     * @param key
     * @return -1表示不存在这个key
     */
    private int index(int key) {
        int free;
        if (key != (free = freeValue)) {
            int[] tmpKeys = keys;
            int capacityMask;
            int index;
            int cur;
            if ((cur = tmpKeys[index = mixIntKey(key) & (capacityMask = tmpKeys.length - 1)]) == key) {
                return index;
            } else {
                if (cur == free) {
                    return -1;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            return index;
                        } else if (cur == free) {
                            return -1;
                        }
                    }
                }
            }
        } else {
            return -1;
        }
    }

    private void rehash(int newCapacity) {
        int free = freeValue;
        int[] tmpKeys = keys;
        init(newCapacity);
        int[] newKeys = keys;
        int capacityMask = newKeys.length - 1;
        int key;
        int index;
        for (int i = tmpKeys.length - 1; i >= 0; i--) {
            if ((key = tmpKeys[i]) != free) {
                if (newKeys[index = mixIntKey(key) & capacityMask] != free) {
                    while (true) {
                        if (newKeys[(index = (index - 1) & capacityMask)] == free) {
                            break;
                        }
                    }
                }
                newKeys[index] = key;
            }
        }
    }

    /**
     * 添加集合
     *
     * @param set
     */
    public void addAll(IntSet set) {
        int freeValue = set.getFreeValue();
        int[] keys = set.getKeys();
        int key;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                add(key);
            }
        }
    }

    @Override
    public String toString() {
        int freeValue = this.freeValue;
        int[] keys = this.keys;
        int key;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                stringBuilder.append(key).append(", ");
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    /**
     * 不可变的遍历
     *
     * @param consumer
     */
    public void foreachImmutable(IntConsumer consumer) {
        if (size == 0) {
            return;
        }
        int freeValue = this.freeValue;
        int[] keys = this.keys;
        int key;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key);
            }
        }
    }

    /**
     * 可变的遍历(需要注意在遍历时删除元素，有可能会重复遍历某一元素)
     *
     * @param consumer
     */
    public void foreachMutable(IntConsumer consumer) {
        if (size == 0) {
            return;
        }
        int freeValue = this.freeValue;
        int[] keys = this.keys;
        int key;

        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key);
                if (key != keys[i]) {
                    i--;
                }
            }
        }
    }
}
