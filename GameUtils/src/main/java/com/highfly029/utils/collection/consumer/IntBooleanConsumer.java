package com.highfly029.utils.collection.consumer;

/**
 * @ClassName IntBooleanConsumer
 * @Description IntBooleanConsumer
 * @Author liyunpeng
 **/
public interface IntBooleanConsumer {
    void accept(int k, boolean v);
}
