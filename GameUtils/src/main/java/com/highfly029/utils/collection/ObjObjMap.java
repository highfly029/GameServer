package com.highfly029.utils.collection;

import com.highfly029.utils.collection.consumer.ObjObjConsumer;
import com.highfly029.utils.collection.creater.ObjectArrayCreateInterface;

/**
 * @ClassName ObjObjMap
 * @Description ObjObjMap
 * @Author liyunpeng
 **/
public class ObjObjMap<K, V> extends BaseHash {
    public static final ObjObjMap empty = new ObjObjMap();
    private K[] keys;
    private V[] values;
    private ObjectArrayCreateInterface<K> keyCreate;
    private ObjectArrayCreateInterface<V> valueCreate;

    public ObjObjMap() {
        init(reCountCapacity(0));
    }

    public ObjObjMap(ObjectArrayCreateInterface<K> keyCreate, ObjectArrayCreateInterface<V> valueCreate) {
        this.keyCreate = keyCreate;
        this.valueCreate = valueCreate;
        init(reCountCapacity(0));
    }

    public ObjObjMap(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    public ObjObjMap(int capacity, ObjectArrayCreateInterface<K> keyCreate, ObjectArrayCreateInterface<V> valueCreate) {
        this.keyCreate = keyCreate;
        this.valueCreate = valueCreate;
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    private void init(int capacity) {
        keys = createKeyObjectArray(capacity);
        values = createValueObjectArray(capacity);
    }

    private K[] createKeyObjectArray(int length) {
        if (keyCreate != null) {
            return keyCreate.create(length);
        }
        return (K[]) new Object[length];
    }

    private V[] createValueObjectArray(int length) {
        if (valueCreate != null) {
            return valueCreate.create(length);
        }
        return (V[]) new Object[length];
    }

    /**
     * 获取map的最大容量
     *
     * @return
     */
    public int getCapacity() {
        return keys.length;
    }

    public final K[] getKeys() {
        return keys;
    }

    public final V[] getValues() {
        return values;
    }

    /**
     * 增加
     *
     * @param key
     * @param value
     */
    public void put(K key, V value) {
        if (key == null) {
            return;
        }
        int index = insert(key, value);
        if (index < 0) {
            return;
        } else {
            values[index] = value;
            return;
        }
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean contains(K key) {
        if (key == null || size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean containsKey(K key) {
        if (key == null || size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 获取
     *
     * @param key
     * @return
     */
    public V get(K key) {
        if (key == null || size == 0) {
            return null;
        }
        int index = index(key);
        if (index >= 0) {
            return values[index];
        } else {
            return null;
        }
    }

    /**
     * 获取
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public V getOrDefault(K key, V defaultValue) {
        if (key == null || size == 0) {
            return defaultValue;
        }
        int index = index(key);
        if (index >= 0) {
            return values[index];
        } else {
            return defaultValue;
        }
    }

    /**
     * 删除
     *
     * @param key
     * @return
     */
    public V remove(K key) {
        if (key == null || size == 0) {
            return null;
        }
        K[] tmpKeys = keys;
        int capacityMask = tmpKeys.length - 1;
        int index;
        K cur;
        if ((cur = tmpKeys[index = mixObjectKey(key.hashCode()) & capacityMask]) == key || (cur != null && cur.equals(key))) {
        } else if (cur == null) {
            return null;
        } else {
            while (true) {
                if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key || (cur != null && cur.equals(key))) {
                    break;
                } else if (cur == null) {
                    return null;
                }
            }
        }

        V[] tmpValues = values;
        V value = tmpValues[index];
        int indexToRemove = index;
        int indexToShift = indexToRemove;
        int shiftDistance = 1;
        K keyToShift;
        while (true) {
            indexToShift = (indexToShift - 1) & capacityMask;
            if ((keyToShift = tmpKeys[indexToShift]) == null) {
                break;
            }
            if (((mixObjectKey(keyToShift.hashCode()) - indexToShift) & capacityMask) >= shiftDistance) {
                tmpKeys[indexToRemove] = keyToShift;
                tmpValues[indexToRemove] = tmpValues[indexToShift];
                indexToRemove = indexToShift;
                shiftDistance = 1;
            } else {
                shiftDistance++;
            }
        }

        tmpKeys[indexToRemove] = null;
        tmpValues[indexToRemove] = null;
        size--;
        return value;
    }

    @Override
    public void clear() {
        K[] tmpKeys = this.keys;
        V[] tmpValues = this.values;
        if (tmpKeys.length > clearMaxSize) {
            init(clearMaxSize);
        } else {
            for (int i = 0, len = tmpKeys.length; i < len; i++) {
                tmpKeys[i] = null;
                tmpValues[i] = null;
            }
        }
        size = 0;
    }

    /**
     * 扩容
     *
     * @param capacity
     */
    public void reSizeCapacity(int capacity) {
        if (capacity > keys.length) {
            rehash(reCountCapacity(capacity));
        }
    }

    /**
     * 如果不存在才添加
     *
     * @param key
     * @param value
     * @return
     */
    public V putIfAbsent(K key, V value) {
        if (key == null) {
            return null;
        }
        int index = insert(key, value);
        if (index < 0) {
            return value;
        } else {
            return values[index];
        }
    }

    /**
     * 插入
     *
     * @param key
     * @param value
     * @return 索引
     */
    private int insert(K key, V value) {
        K[] tmpKeys = keys;
        int capacityMask;
        int index;
        K cur;
        if ((cur = tmpKeys[index = mixObjectKey(key.hashCode()) & (capacityMask = tmpKeys.length - 1)]) != null) {
            if (cur == key || cur.equals(key)) {
                return index;
            } else {
                while (true) {
                    if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == null) {
                        break;
                    } else if (cur == key || cur.equals(key)) {
                        return index;
                    }
                }
            }
        }
        tmpKeys[index] = key;
        values[index] = value;
        int curSize = ++size;
        if (isCanGrow(curSize / keys.length)) {
            rehash(size << 1);
        }
        return -1;
    }

    /**
     * 查找key的索引
     *
     * @param key
     * @return -1表示不存在这个key
     */
    private int index(K key) {
        if (key != null) {
            K[] tmpKeys = keys;
            int capacityMask;
            int index;
            K cur;
            if ((cur = tmpKeys[index = mixObjectKey(key.hashCode()) & (capacityMask = tmpKeys.length - 1)]) == key || (cur != null && cur.equals(key))) {
                return index;
            } else {
                if (cur == null) {
                    return -1;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key || (cur != null && cur.equals(key))) {
                            return index;
                        } else if (cur == null) {
                            return -1;
                        }
                    }
                }
            }
        } else {
            return -1;
        }
    }

    private void rehash(int newCapacity) {
        K[] tmpKeys = keys;
        V[] tmpValues = values;
        init(newCapacity);
        K[] newKeys = keys;
        V[] newValues = values;
        int capacityMask = newKeys.length - 1;

        K key;
        int index;
        for (int i = tmpKeys.length - 1; i >= 0; i--) {
            if ((key = tmpKeys[i]) != null) {
                if (newKeys[index = mixObjectKey(key.hashCode()) & capacityMask] != null) {
                    while (true) {
                        if (newKeys[(index = (index - 1) & capacityMask)] == null) {
                            break;
                        }
                    }
                }
                newKeys[index] = key;
                newValues[index] = tmpValues[i];
            }
        }
    }

    @Override
    public String toString() {
        K[] keys = this.keys;
        V[] values = this.values;
        K key;
        V value;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != null) {
                value = values[i];
                stringBuilder.append(key).append(":").append(value).append(", ");
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    /**
     * 不可变的遍历
     *
     * @param consumer
     */
    public void foreachImmutable(ObjObjConsumer<? super K, ? super V> consumer) {
        if (size == 0) {
            return;
        }
        K[] keys = this.keys;
        V[] values = this.values;
        K key;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != null) {
                consumer.accept(key, values[i]);
            }
        }
    }

    /**
     * 可变的遍历(需要注意在遍历时删除元素，有可能会重复遍历某一元素)
     *
     * @param consumer
     */
    public void foreachMutable(ObjObjConsumer<? super K, ? super V> consumer) {
        if (size == 0) {
            return;
        }
        K[] keys = this.keys;
        V[] values = this.values;
        K key;

        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != null) {
                consumer.accept(key, values[i]);
                if (key != keys[i]) {
                    i--;
                }
            }
        }
    }
}
