package com.highfly029.utils.collection.consumer;

/**
 * @ClassName IntConsumer
 * @Description IntConsumer
 * @Author liyunpeng
 **/
public interface IntConsumer {
    void accept(int k);
}
