package com.highfly029.utils.collection;

import java.util.Arrays;

import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.collection.consumer.LongObjectConsumer;
import com.highfly029.utils.collection.creater.ObjectArrayCreateInterface;

/**
 * @ClassName LongObjectMap
 * @Description LongObjectMap
 * @Author liyunpeng
 **/
public class LongObjectMap<V> extends BaseHash {
    public static final LongObjectMap empty = new LongObjectMap();
    private long freeValue;
    private long[] keys;
    private V[] values;
    private ObjectArrayCreateInterface<V> create;

    public LongObjectMap() {
        init(reCountCapacity(0));
    }

    public LongObjectMap(ObjectArrayCreateInterface<V> create) {
        this.create = create;
        init(reCountCapacity(0));
    }

    public LongObjectMap(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    public LongObjectMap(int capacity, ObjectArrayCreateInterface<V> create) {
        this.create = create;
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    private void init(int capacity) {
        keys = new long[capacity];
        if (freeValue != 0) {
            Arrays.fill(keys, freeValue);
        }
        values = createObjectArray(capacity);
    }

    private V[] createObjectArray(int length) {
        if (create != null) {
            return create.create(length);
        }
        return (V[]) new Object[length];
    }

    /**
     * 获取map的最大容量
     *
     * @return
     */
    public int getCapacity() {
        return keys.length;
    }

    public final long getFreeValue() {
        return freeValue;
    }

    public final long[] getKeys() {
        return keys;
    }

    public final V[] getValues() {
        return values;
    }

    /**
     * 增加
     *
     * @param key
     * @param value
     */
    public void put(long key, V value) {
        int index = insert(key, value);
        if (index < 0) {
            return;
        } else {
            values[index] = value;
            return;
        }
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean contains(long key) {
        if (size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean containsKey(long key) {
        if (size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 获取
     *
     * @param key
     * @return
     */
    public V get(long key) {
        if (size == 0) {
            return null;
        }
        int index = index(key);
        if (index >= 0) {
            return values[index];
        } else {
            return null;
        }
    }

    /**
     * 获取
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public V getOrDefault(long key, V defaultValue) {
        if (size == 0) {
            return defaultValue;
        }
        int index = index(key);
        if (index >= 0) {
            return values[index];
        } else {
            return defaultValue;
        }
    }

    /**
     * 删除
     *
     * @param key
     * @return
     */
    public V remove(long key) {
        if (size == 0) {
            return null;
        }
        long free;
        if (key != (free = freeValue)) {
            long[] tmpKeys = keys;
            int capacityMask = tmpKeys.length - 1;
            int index;
            long cur;
            if ((cur = tmpKeys[index = mixLongKey(key) & capacityMask]) != key) {
                if (cur == free) {
                    return null;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            break;
                        } else if (cur == free) {
                            return null;
                        }
                    }
                }
            }

            V[] tmpValues = values;
            V value = tmpValues[index];
            int indexToRemove = index;
            int indexToShift = indexToRemove;
            int shiftDistance = 1;
            long keyToShift;
            while (true) {
                indexToShift = (indexToShift - 1) & capacityMask;
                if ((keyToShift = tmpKeys[indexToShift]) == free) {
                    break;
                }
                if (((mixLongKey(keyToShift) - indexToShift) & capacityMask) >= shiftDistance) {
                    tmpKeys[indexToRemove] = keyToShift;
                    tmpValues[indexToRemove] = tmpValues[indexToShift];
                    indexToRemove = indexToShift;
                    shiftDistance = 1;
                } else {
                    shiftDistance++;
                }
            }

            tmpKeys[indexToRemove] = free;
            tmpValues[indexToRemove] = null;
            size--;
            return value;
        } else {
            return null;
        }
    }

    @Override
    public void clear() {
        this.freeValue = 0;
        long tmpFreeValue = this.freeValue;
        long[] tmpKeys = this.keys;
        if (tmpKeys.length > clearMaxSize) {
            init(clearMaxSize);
        } else {
            V[] tmpValues = this.values;
            for (int i = 0, len = tmpKeys.length; i < len; i++) {
                tmpKeys[i] = tmpFreeValue;
                tmpValues[i] = null;
            }
        }
        size = 0;
    }

    /**
     * 扩容
     *
     * @param capacity
     */
    public void reSizeCapacity(int capacity) {
        if (capacity > keys.length) {
            rehash(reCountCapacity(capacity));
        }
    }

    /**
     * 如果不存在才添加
     *
     * @param key
     * @param value
     * @return
     */
    public V putIfAbsent(long key, V value) {
        int index = insert(key, value);
        if (index < 0) {
            return value;
        } else {
            return values[index];
        }
    }

    /**
     * 插入
     *
     * @param key
     * @param value
     * @return 索引
     */
    private int insert(long key, V value) {
        long free;
        if (key == (free = freeValue)) {
            free = changeFree();
        }
        long[] tmpKeys = keys;
        int capacityMask;
        int index;
        long cur;
        if ((cur = tmpKeys[index = mixLongKey(key) & (capacityMask = tmpKeys.length - 1)]) != free) {
            if (cur == key) {
                return index;
            } else {
                while (true) {
                    if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == free) {
                        break;
                    } else if (cur == key) {
                        return index;
                    }
                }
            }
        }
        tmpKeys[index] = key;
        values[index] = value;
        int curSize = ++size;
        if (isCanGrow(curSize / keys.length)) {
            rehash(size << 1);
        }
        return -1;
    }

    private long changeFree() {
        long newFreeValue = findNewFreeOrRemoved();
        replaceAll(keys, freeValue, newFreeValue);
        freeValue = newFreeValue;
        return newFreeValue;
    }

    private long findNewFreeOrRemoved() {
        long tmpFreeValue = freeValue;
        long newFreeValue;
        do {
            newFreeValue = RandomUtils.nextInt();
        } while ((newFreeValue == tmpFreeValue) || index(newFreeValue) >= 0);
        return newFreeValue;
    }

    /**
     * 查找key的索引
     *
     * @param key
     * @return -1表示不存在这个key
     */
    private int index(long key) {
        long free;
        if (key != (free = freeValue)) {
            long[] tmpKeys = keys;
            int capacityMask;
            int index;
            long cur;
            if ((cur = tmpKeys[index = mixLongKey(key) & (capacityMask = tmpKeys.length - 1)]) == key) {
                return index;
            } else {
                if (cur == free) {
                    return -1;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            return index;
                        } else if (cur == free) {
                            return -1;
                        }
                    }
                }
            }
        } else {
            return -1;
        }
    }

    private void rehash(int newCapacity) {
        long free = freeValue;
        long[] tmpKeys = keys;
        V[] tmpValues = values;
        init(newCapacity);
        long[] newKeys = keys;
        V[] newValues = values;
        int capacityMask = newKeys.length - 1;

        long key;
        int index;
        for (int i = tmpKeys.length - 1; i >= 0; i--) {
            if ((key = tmpKeys[i]) != free) {
                if (newKeys[index = mixLongKey(key) & capacityMask] != free) {
                    while (true) {
                        if (newKeys[(index = (index - 1) & capacityMask)] == free) {
                            break;
                        }
                    }
                }
                newKeys[index] = key;
                newValues[index] = tmpValues[i];
            }
        }
    }

    @Override
    public String toString() {
        long freeValue = this.freeValue;
        long[] keys = this.keys;
        V[] values = this.values;
        long key;
        V value;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                value = values[i];
                stringBuilder.append(key).append(":").append(value).append(", ");
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    /**
     * 不可变的遍历
     *
     * @param consumer
     */
    public void foreachImmutable(LongObjectConsumer<? super V> consumer) {
        if (size == 0) {
            return;
        }
        long freeValue = this.freeValue;
        long[] keys = this.keys;
        V[] values = this.values;
        long key;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key, values[i]);
            }
        }
    }

    /**
     * 可变的遍历(需要注意在遍历时删除元素，有可能会重复遍历某一元素)
     *
     * @param consumer
     */
    public void foreachMutable(LongObjectConsumer<? super V> consumer) {
        if (size == 0) {
            return;
        }
        long freeValue = this.freeValue;
        long[] keys = this.keys;
        V[] values = this.values;
        long key;

        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key, values[i]);
                if (key != keys[i]) {
                    i--;
                }
            }
        }
    }
}
