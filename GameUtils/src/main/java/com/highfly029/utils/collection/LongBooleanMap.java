package com.highfly029.utils.collection;

import java.util.Arrays;

import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.collection.consumer.LongBooleanConsumer;

/**
 * @ClassName LongBooleanMap
 * @Description LongBooleanMap
 * @Author liyunpeng
 **/
public class LongBooleanMap extends BaseHash {
    public static final LongBooleanMap empty = new LongBooleanMap();
    private long freeValue;
    private long[] keys;
    private boolean[] values;

    public LongBooleanMap() {
        init(reCountCapacity(0));
    }

    public LongBooleanMap(int capacity) {
        int size = reCountCapacity(capacity);
        init(size);
        clearMaxSize = size;
    }

    private void init(int capacity) {
        keys = new long[capacity];
        values = new boolean[capacity];
        if (freeValue != 0) {
            Arrays.fill(keys, freeValue);
        }
    }

    /**
     * 获取map的最大容量
     *
     * @return
     */
    public int getCapacity() {
        return keys.length;
    }

    public final long getFreeValue() {
        return freeValue;
    }

    public final long[] getKeys() {
        return keys;
    }

    public final boolean[] getValues() {
        return values;
    }

    /**
     * 增加
     *
     * @param key
     * @param value
     */
    public void put(long key, boolean value) {
        int index = insert(key, value);
        if (index < 0) {
            return;
        } else {
            values[index] = value;
            return;
        }
    }

    /**
     * 是否存在key
     *
     * @param key
     * @return
     */
    public boolean contains(long key) {
        if (size == 0) {
            return false;
        }
        return index(key) >= 0;
    }

    /**
     * 获取
     *
     * @param key
     * @return
     */
    public boolean get(long key) {
        if (size == 0) {
            return false;
        }
        int index = index(key);
        if (index >= 0) {
            return values[index];
        } else {
            return false;
        }
    }

    /**
     * 获取
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public boolean getOrDefault(long key, boolean defaultValue) {
        if (size == 0) {
            return defaultValue;
        }
        int index = index(key);
        if (index >= 0) {
            return values[index];
        } else {
            return defaultValue;
        }
    }

    /**
     * 删除
     *
     * @param key
     * @return
     */
    public boolean remove(long key) {
        if (size == 0) {
            return false;
        }
        long free;
        if (key != (free = freeValue)) {
            long[] tmpKeys = keys;
            int capacityMask = tmpKeys.length - 1;
            int index;
            long cur;
            if ((cur = tmpKeys[index = mixLongKey(key) & capacityMask]) != key) {
                if (cur == free) {
                    return false;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            break;
                        } else if (cur == free) {
                            return false;
                        }
                    }
                }
            }

            boolean[] tmpValues = values;
            boolean value = tmpValues[index];
            int indexToRemove = index;
            int indexToShift = indexToRemove;
            int shiftDistance = 1;
            long keyToShift;
            while (true) {
                indexToShift = (indexToShift - 1) & capacityMask;
                if ((keyToShift = tmpKeys[indexToShift]) == free) {
                    break;
                }
                if (((mixLongKey(keyToShift) - indexToShift) & capacityMask) >= shiftDistance) {
                    tmpKeys[indexToRemove] = keyToShift;
                    tmpValues[indexToRemove] = tmpValues[indexToShift];
                    indexToRemove = indexToShift;
                    shiftDistance = 1;
                } else {
                    shiftDistance++;
                }
            }

            tmpKeys[indexToRemove] = free;
            size--;
            return value;
        } else {
            return false;
        }
    }

    @Override
    public void clear() {
        this.freeValue = 0;
        if (this.keys.length > clearMaxSize) {
            init(clearMaxSize);
        } else {
            Arrays.fill(keys, freeValue);
        }
        size = 0;
    }

    /**
     * 扩容
     *
     * @param capacity
     */
    public void reSizeCapacity(int capacity) {
        if (capacity > keys.length) {
            rehash(reCountCapacity(capacity));
        }
    }

    /**
     * 如果不存在才添加
     *
     * @param key
     * @param value
     * @return
     */
    public boolean putIfAbsent(long key, boolean value) {
        int index = insert(key, value);
        if (index < 0) {
            return value;
        } else {
            return values[index];
        }
    }

    /**
     * 插入
     *
     * @param key
     * @param value
     * @return 索引
     */
    private int insert(long key, boolean value) {
        long free;
        if (key == (free = freeValue)) {
            free = changeFree();
        }
        long[] tmpKeys = keys;
        int capacityMask;
        int index;
        long cur;
        if ((cur = tmpKeys[index = mixLongKey(key) & (capacityMask = tmpKeys.length - 1)]) != free) {
            if (cur == key) {
                return index;
            } else {
                while (true) {
                    if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == free) {
                        break;
                    } else if (cur == key) {
                        return index;
                    }
                }
            }
        }
        tmpKeys[index] = key;
        values[index] = value;
        int curSize = ++size;
        if (isCanGrow(curSize / keys.length)) {
            rehash(size << 1);
        }
        return -1;
    }

    private long changeFree() {
        long newFreeValue = findNewFreeOrRemoved();
        replaceAll(keys, freeValue, newFreeValue);
        freeValue = newFreeValue;
        return newFreeValue;
    }

    private long findNewFreeOrRemoved() {
        long tmpFreeValue = freeValue;
        long newFreeValue;
        do {
            newFreeValue = RandomUtils.nextInt();
        } while ((newFreeValue == tmpFreeValue) || index(newFreeValue) >= 0);
        return newFreeValue;
    }

    /**
     * 查找key的索引
     *
     * @param key
     * @return -1表示不存在这个key
     */
    private int index(long key) {
        long free;
        if (key != (free = freeValue)) {
            long[] tmpKeys = keys;
            int capacityMask;
            int index;
            long cur;
            if ((cur = tmpKeys[index = mixLongKey(key) & (capacityMask = tmpKeys.length - 1)]) == key) {
                return index;
            } else {
                if (cur == free) {
                    return -1;
                } else {
                    while (true) {
                        if ((cur = tmpKeys[(index = (index - 1) & capacityMask)]) == key) {
                            return index;
                        } else if (cur == free) {
                            return -1;
                        }
                    }
                }
            }
        } else {
            return -1;
        }
    }

    private void rehash(int newCapacity) {
        long free = freeValue;
        long[] tmpKeys = keys;
        boolean[] tmpValues = values;
        init(newCapacity);
        long[] newKeys = keys;
        boolean[] newValues = values;
        int capacityMask = newKeys.length - 1;

        long key;
        int index;
        for (int i = tmpKeys.length - 1; i >= 0; i--) {
            if ((key = tmpKeys[i]) != free) {
                if (newKeys[index = mixLongKey(key) & capacityMask] != free) {
                    while (true) {
                        if (newKeys[(index = (index - 1) & capacityMask)] == free) {
                            break;
                        }
                    }
                }
                newKeys[index] = key;
                newValues[index] = tmpValues[i];
            }
        }
    }

    @Override
    public String toString() {
        long freeValue = this.freeValue;
        long[] keys = this.keys;
        boolean[] values = this.values;
        long key;
        boolean value;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                value = values[i];
                stringBuilder.append(key).append(":").append(value).append(", ");
            }
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    /**
     * 不可变的遍历
     *
     * @param consumer
     */
    public void foreachImmutable(LongBooleanConsumer consumer) {
        if (size == 0) {
            return;
        }
        long freeValue = this.freeValue;
        long[] keys = this.keys;
        boolean[] values = this.values;
        long key;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key, values[i]);
            }
        }
    }

    /**
     * 可变的遍历(需要注意在遍历时删除元素，有可能会重复遍历某一元素)
     *
     * @param consumer
     */
    public void foreachMutable(LongBooleanConsumer consumer) {
        if (size == 0) {
            return;
        }
        long freeValue = this.freeValue;
        long[] keys = this.keys;
        boolean[] values = this.values;
        long key;

        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                consumer.accept(key, values[i]);
                if (key != keys[i]) {
                    i--;
                }
            }
        }
    }
}
