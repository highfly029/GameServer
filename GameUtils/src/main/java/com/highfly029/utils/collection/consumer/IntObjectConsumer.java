package com.highfly029.utils.collection.consumer;

/**
 * @ClassName IntObjectConsumer
 * @Description IntObjectConsumer
 * @Author liyunpeng
 **/
public interface IntObjectConsumer<V> {
    void accept(int k, V v);
}
