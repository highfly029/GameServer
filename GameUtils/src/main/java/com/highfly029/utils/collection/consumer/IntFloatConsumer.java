package com.highfly029.utils.collection.consumer;

/**
 * @ClassName IntFloatConsumer
 * @Description IntFloatConsumer
 * @Author liyunpeng
 **/
public interface IntFloatConsumer {
    void accept(int k, float v);
}
