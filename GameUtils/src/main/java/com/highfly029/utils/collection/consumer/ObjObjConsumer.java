package com.highfly029.utils.collection.consumer;

/**
 * @ClassName ObjObjConsumer
 * @Description ObjObjConsumer
 * @Author liyunpeng
 **/
public interface ObjObjConsumer<K, V> {
    void accept(K k, V v);
}
