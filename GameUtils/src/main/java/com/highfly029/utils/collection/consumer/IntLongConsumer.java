package com.highfly029.utils.collection.consumer;

/**
 * @ClassName IntLongConsumer
 * @Description IntLongConsumer
 * @Author liyunpeng
 **/
public interface IntLongConsumer {
    void accept(int k, long v);
}
