package com.highfly029.utils.collection;

import com.highfly029.utils.MathUtils;

/**
 * @ClassName BaseList
 * @Description 列表基类
 * @Author liyunpeng
 **/
public abstract class BaseList {
    protected static final int defaultSize = 4;
    protected int size = 0;
    protected static final int defaultClearMaxSize = 16;
    protected int clearMaxSize = defaultClearMaxSize;

    /**
     * 重新计算容量
     *
     * @param capacity
     * @return
     */
    protected int reCountCapacity(int capacity) {
        capacity = MathUtils.getNearlyPowerOfTwo(capacity);
        if (capacity < defaultSize) {
            capacity = defaultSize;
        }
        return capacity;
    }

    /**
     * 获取队列的元素数量
     *
     * @return
     */
    public final int size() {
        return size;
    }

    /**
     * 判断队列是否为空
     *
     * @return
     */
    public final boolean isEmpty() {
        return size == 0;
    }

    /**
     * 清空列表
     */
    public void clear() {
        size = 0;
    }

    protected String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }
}
