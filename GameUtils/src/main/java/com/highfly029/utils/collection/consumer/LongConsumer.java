package com.highfly029.utils.collection.consumer;

/**
 * @ClassName LongConsumer
 * @Description LongConsumer
 * @Author liyunpeng
 **/
public interface LongConsumer {
    void accept(long k);
}
