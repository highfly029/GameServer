package com.highfly029.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import net.jpountz.lz4.LZ4BlockInputStream;
import net.jpountz.lz4.LZ4BlockOutputStream;
import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;
import org.xerial.snappy.Snappy;

public final class CompressUtils {
    public static void main(String[] args) {
        final int MAX_CNT = 10000;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 1000; i++) {
            stringBuilder.append("\"abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+\"");
        }
        long t1 = System.nanoTime();
        for (int i = 0; i < MAX_CNT; i++) {
            try {
                byte[] comopressed = snappyCompress(stringBuilder.toString().getBytes(StandardCharsets.UTF_8));
                byte[] uncompressed = snappyUncompress(comopressed);
//                System.out.println("str="+new String(uncompressed));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        long t2 = System.nanoTime();
        System.out.println("snappy time=" + (t2 - t1));

        t1 = System.nanoTime();
        for (int i = 0; i < MAX_CNT; i++) {
            try {
                byte[] comopressed = lz4Compress(stringBuilder.toString().getBytes(StandardCharsets.UTF_8));
                byte[] uncompressed = lz4Uncompress(comopressed);
//                System.out.println("str="+new String(uncompressed));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        t2 = System.nanoTime();
        System.out.println("lz4 time=" + (t2 - t1));

    }

    public static byte[] snappyCompress(byte[] srcBytes) throws IOException {
        return Snappy.compress(srcBytes);
    }

    public static byte[] snappyUncompress(byte[] bytes) throws IOException {
        return Snappy.uncompress(bytes);
    }

    public static byte[] lz4Compress(byte[] srcBytes) throws IOException {
        LZ4Factory factory = LZ4Factory.fastestInstance();
        ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
        LZ4Compressor compressor = factory.fastCompressor();
        LZ4BlockOutputStream compressedOutput = new LZ4BlockOutputStream(
                byteOutput, 2048, compressor);
        compressedOutput.write(srcBytes);
        compressedOutput.close();
        return byteOutput.toByteArray();
    }

    public static byte[] lz4Uncompress(byte[] bytes) throws IOException {
        LZ4Factory factory = LZ4Factory.fastestInstance();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        LZ4FastDecompressor decompresser = factory.fastDecompressor();
        LZ4BlockInputStream lzis = new LZ4BlockInputStream(
                new ByteArrayInputStream(bytes), decompresser);
        int count;
        byte[] buffer = new byte[2048];
        while ((count = lzis.read(buffer)) != -1) {
            baos.write(buffer, 0, count);
        }
        lzis.close();
        return baos.toByteArray();
    }
}
