package com.highfly029.utils;

import java.text.ParseException;
import java.util.Date;
import java.util.TimeZone;

/**
 * @ClassName CronUtils
 * @Description cron工具
 * @Author liyunpeng
 **/
public final class CronUtils {

    /**
     * 创建cron表达式
     *
     * @return
     */
    public static CronExpression createCronExpression(String cron) {
        return createCronExpression(cron, TimeZone.getDefault());
    }

    /**
     * 创建cron表达式
     *
     * @return
     */
    public static CronExpression createCronExpression(String cron, TimeZone timeZone) {
        CronExpression cronExpression = null;
        try {
            cronExpression = new CronExpression(cron);
            cronExpression.setTimeZone(timeZone);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cronExpression;
    }

    /**
     * 获取下一个执行时间
     *
     * @param cronExpression
     * @param time
     * @return
     */
    public static long getNextCronTime(CronExpression cronExpression, long time) {
        Date date = cronExpression.getNextValidTimeAfter(new Date(time));
        if (date == null) {
            return 0L;
        }
        return date.getTime();
    }

    /**
     * 获取上一个执行时间
     *
     * @return
     */
    public static long getPreCronTime() {
        return 0L;
    }
}
