package com.highfly029.utils;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.highfly029.utils.collection.ObjObjMap;
import com.highfly029.utils.collection.ObjectSet;
import com.highfly029.utils.gsonAdapter.ObjObjJsonAdapter;
import com.highfly029.utils.gsonDeserializer.ObjObjMapDeserializer;
import com.highfly029.utils.gsonDeserializer.ObjectSetDeserializer;
import com.highfly029.utils.gsonSerializer.ObjObjMapSerializer;
import com.highfly029.utils.gsonSerializer.ObjectSetSerializer;

/**
 * @ClassName JsonUtils
 * @Description json
 * https://www.jianshu.com/p/fc5c9cdf3aab
 * @Author liyunpeng
 **/
public class JsonUtils {
    private static Gson gson = null;

    static {
        if (gson == null) {
            GsonBuilder gsonBuilder = new GsonBuilder();

            gsonBuilder.serializeNulls();
            registerAdapter(gsonBuilder);
//            registerSerializer(gsonBuilder);
//            registerDeserializer(gsonBuilder);
            gson = gsonBuilder.create();
        }
        if (gson == null) {
            System.out.println("JsonUtils error");
        }
    }

    /**
     * 性能更高、但不支持自定义多层结构
     *
     * @param gsonBuilder
     */
    private static void registerAdapter(GsonBuilder gsonBuilder) {
        gsonBuilder.registerTypeAdapter(ObjObjMap.class, new ObjObjJsonAdapter());
    }

    private static void registerSerializer(GsonBuilder gsonBuilder) {
        gsonBuilder.registerTypeAdapter(ObjObjMap.class, new ObjObjMapSerializer());
        gsonBuilder.registerTypeAdapter(ObjectSet.class, new ObjectSetSerializer());
    }

    private static void registerDeserializer(GsonBuilder gsonBuilder) {
        gsonBuilder.registerTypeAdapter(ObjObjMap.class, new ObjObjMapDeserializer());
        gsonBuilder.registerTypeAdapter(ObjectSet.class, new ObjectSetDeserializer());
    }


    public static String toJson(Object object) {
        return gson.toJson(object);
    }

    public static <T> T fromJson(String jsonStr, Class<T> cls) {
        return gson.fromJson(jsonStr, cls);
    }

    public static <T> T fromJson(String jsonStr, Type type) {
        return gson.fromJson(jsonStr, type);
    }

    public static Gson getGson() {
        return gson;
    }
}
