package com.highfly029.utils;

/**
 * @ClassName ByteUtils
 * @Description 字节转换工具
 * @Author liyunpeng
 **/
public class ByteUtils {

    /**
     * 16进制字符串转字节数组
     */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }
}
