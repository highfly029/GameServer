package com.highfly029.utils;

/**
 * @ClassName SysUtils
 * @Description 系统工具
 * @Author liyunpeng
 **/
public final class SysUtils {
    private static final Runtime runtime = Runtime.getRuntime();


    /**
     * 系统退出
     */
    public static void exit() {
        exit(0);
    }

    /**
     * 系统退出
     * 新开一个线程调用exit防止线程死循环
     *
     * @param code 退出码
     */
    public static void exit(int code) {
        new Thread(() -> System.exit(code)).start();
    }

    public static boolean isLinux() {
        return System.getProperty("os.name").startsWith("Linux");
    }

    public static boolean isWindows() {
        return System.getProperty("os.name").startsWith("Windows");
    }

    public static boolean isMacOS() {
        return System.getProperty("os.name").startsWith("Mac");
    }

    /**
     * 获取虚拟机空闲内存
     *
     * @return
     */
    public static long getFreeMemory() {
        return runtime.freeMemory();
    }

    /**
     * 获取虚拟机全部内存
     *
     * @return
     */
    public static long getTotalMemory() {
        return runtime.totalMemory();
    }

    /**
     * 获取虚拟机最大内存
     *
     * @return
     */
    public static long getMaxMemory() {
        return runtime.maxMemory();
    }
}
