package com.highfly029.utils.gsonSerializer;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.highfly029.utils.collection.ObjectSet;

/**
 * @ClassName ObjectSetSerializer
 * @Description ObjectSetSerializer
 * @Author liyunpeng
 **/
public class ObjectSetSerializer implements JsonSerializer<ObjectSet> {
    @Override
    public JsonElement serialize(ObjectSet objectSet, Type type, JsonSerializationContext jsonSerializationContext) {

        JsonArray jsonArray = new JsonArray();
        objectSet.foreachImmutable(k -> jsonArray.add(jsonSerializationContext.serialize(k)));
        return jsonArray;
    }
}
