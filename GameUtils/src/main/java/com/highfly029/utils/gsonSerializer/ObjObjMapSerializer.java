package com.highfly029.utils.gsonSerializer;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.highfly029.utils.collection.ObjObjMap;

/**
 * @ClassName ObjObjMapSerializer
 * @Description ObjObjMapSerializer
 * @Author liyunpeng
 **/
public class ObjObjMapSerializer implements JsonSerializer<ObjObjMap> {

    @Override
    public JsonElement serialize(ObjObjMap objObjMap, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject jsonObject = new JsonObject();
        objObjMap.foreachImmutable((k, v) -> jsonObject.add(k.toString(), jsonSerializationContext.serialize(v)));
        return jsonObject;
    }
}
