package com.highfly029.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;


/**
 * @ClassName ConfigPropUtils
 * @Description 配置文件
 * @Author liyunpeng
 **/
public final class ConfigPropUtils {
    private static volatile Properties prop;

    private static final String NULLSTR = "";
    //配置路径
    private static final String confPath;

    static {
        String classPath = ConfigPropUtils.class.getResource("/").getPath();
        File file = new File(classPath);
        String basePath = file.getParent();
        confPath = basePath + "/conf";

        load();
    }

    public static void load() {
        Properties properties = new Properties();
        try {
            File dir = new File(getConfigPath());
            if (dir.isDirectory()) {
                File[] files = dir.listFiles();
                for (File file : files) {
                    if (!file.isDirectory() && !file.getName().startsWith("log4j2") && file.getName().toLowerCase().endsWith("properties")) {
                        InputStream in = new BufferedInputStream(new FileInputStream(getConfigPath() + "/" + file.getName()));
                        properties.load(in);
                        in.close();
                    }
                }
            }

            if (properties.stringPropertyNames().size() != 0) {
                System.out.println("load config success");
                prop = properties;
            } else {
                System.out.println("load config fail");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 是否初始化成功
     * @return
     */
    public static boolean isInitComplete() {
        return prop != null;
    }

    public static String getConfigPath() {
        return confPath;
    }

    public static String getValue(String key) {
        return prop.getProperty(key, NULLSTR);
    }

    public static int getIntValue(String key) {
        return Integer.parseInt(prop.getProperty(key, "0"));
    }

    public static boolean getBoolValue(String key) {
        return Boolean.parseBoolean(prop.getProperty(key));
    }

    public static Set<String> getPropKeySet() {
        return prop.stringPropertyNames();
    }
}
