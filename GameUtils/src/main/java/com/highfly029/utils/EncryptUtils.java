package com.highfly029.utils;

public final class EncryptUtils {

    private static final String xorKey = "nNVxI2DFTT1yQneQQtfAFzf5edh9og4H";

    /**
     * 异或加密（解密）
     *
     * @param src
     * @param key
     * @return
     */
    public static byte[] xor(byte[] src, byte[] key) {
        byte[] result = new byte[src.length];
        for (int i = 0, j = 0; i < src.length; i++) {
            byte tmp = src[i];
            byte t = key[j];
            result[i] = (byte) (tmp ^ t);
            j++;
            if (j == key.length) {
                j = 0;
            }
        }
        return result;
    }

    public static byte[] xor(byte[] src) {
        return xor(src, xorKey.getBytes());
    }


    private static byte[] intToBytes(int value) {
        byte[] src = new byte[4];
        src[3] = (byte) ((value >> 24) & 0xFF);
        src[2] = (byte) ((value >> 16) & 0xFF);
        src[1] = (byte) ((value >> 8) & 0xFF);
        src[0] = (byte) (value & 0xFF);
        return src;
    }

    public static byte[] encrypt(byte[] srcs) {
        byte[] key = magic(xorKey.getBytes(), intToBytes(srcs.length), true);
        return magic(srcs, key, true);
    }

    public static byte[] decrypt(byte[] srcs) {
        byte[] key = magic(xorKey.getBytes(), intToBytes(srcs.length), true);
        return magic(srcs, key, false);
    }

    private static byte[] magic(byte[] srcs, byte[] key, boolean isEncrypt) {
        byte[] result = new byte[srcs.length];
        byte[] karray = key;
        for (int i = 0, j = 0; i < srcs.length; i++) {
            //先异或
            result[i] = (byte) (srcs[i] ^ karray[j]);
            //再移位
            byte b = 0;
            byte d = 0;
            if (isEncrypt) {
                b = (byte) ((result[i] & 0xff) >> 7);
                d = (byte) ((result[i] & 0xff) << 1);
            } else {
                b = (byte) ((result[i] & 0xff) << 7);
                d = (byte) ((result[i] & 0xff) >> 1);
            }
            result[i] = (byte) (b | d);
            //再异或
            result[i] = (byte) (result[i] ^ karray[j]);
            //key移位
            j++;
            if (j == karray.length) {
                j = 0;
            }
        }
        return result;
    }

    private static String bytesToString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(b + ", ");
        }
        return sb.toString();
    }

    public static void main(String[] args) {

//        String str = "Hello World! 你好，中国！";
//        long t1 = System.nanoTime();
//        for(int i = 0; i < 1; i++) {
//            byte[] encrypt_data = encrypt(str.getBytes());
//            byte[] decrypt_byte = decrypt(encrypt_data);
//            String data = new String(decrypt_byte);
//	        System.out.println(data);
//        }
//        long t2 = System.nanoTime();
//        System.out.println("time="+(t2-t1));

        String xorStr = "~!@#$%^&*()_+{}:>?<[];',./Li你好 1\t2";
        byte[] xorEncrypt = xor(xorStr.getBytes());
        System.out.println("xorEncrypt=" + new String(xorEncrypt));
        byte[] xorDecrypt = xor(xorEncrypt);
        System.out.println("xorDecrypt=" + new String(xorDecrypt));

    }

    private static boolean isEquals(byte[] b, byte[] c) {
        if (b.length != c.length) {
            return false;
        }
        System.out.println(bytesToString(b));
        System.out.println(bytesToString(c));
        for (int i = 0; i < c.length; i++) {
            if (b[i] != c[i]) {
                return false;
            }
        }
        return true;

    }

}

