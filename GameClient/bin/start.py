#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import os
import subprocess
import platform

def getSeparation():
    osName = platform.system()
    print("osName=" + osName)
    if osName == "Windows":
        return ";"
    else :
        return ":"

if __name__ == '__main__':
    separation = getSeparation()
    cmd = "java -server -cp ../conf/" + separation + "../lib/* com.highfly029.client.world.ClientStarter"
    print(cmd)
    ret = subprocess.call(cmd, shell=True)
    print("ret=" + str(ret))
    os.system("pause")