#!/bin/bash
#test enter scene

num=$1
baseDir=$(dirname "$PWD")
sourceDir=${baseDir}"/GameClient/target/GameClient-1.0.0-final"
if [ ! -d ${baseDir}"/GameClient/robot" ]; then mkdir robot; fi

kill -9 $(jcmd |grep ClientStarter|awk '{print $1}'|xargs)

for i in $(seq 1 $num)
do
  dir=${baseDir}"/GameClient/robot/scene"$i
  if [ -d $dir ]; then rm -rf $dir; fi
  echo $i $dir
  cp -r ${sourceDir} $dir
  
  gsed -i -e 's/userName.*/userName=scene'$i'/g'  ${dir}'/conf/config.properties'

  gsed -i -e 's/open_gui.*/open_gui=false/' ${dir}'/conf/config.properties'
  gsed -i -e 's/open_ai.*/open_ai=true/' ${dir}'/conf/config.properties'
  
  cd ${dir}'/bin'
  bash 'start.sh'
done

