package com.highfly029.client.ai;

import com.highfly029.client.data.ClientAgent;

/**
 * @ClassName BaseAI
 * @Description BaseAI
 * @Author liyunpeng
 **/
public interface BaseAI {

    void init(ClientAgent clientAgent);

    /**
     * 每次触发循环
     */
    void onLoop();
}
