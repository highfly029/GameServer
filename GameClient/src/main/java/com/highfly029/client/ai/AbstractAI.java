package com.highfly029.client.ai;

import com.highfly029.client.data.ClientAgent;

/**
 * @ClassName AbstractAI
 * @Description AbstractAI
 * @Author liyunpeng
 **/
public abstract class AbstractAI implements BaseAI {
    protected ClientAgent clientAgent;

    /************** 黑盒数据 **************/
    /**
     * 登录成功
     */
    protected boolean isLoginSuccess;

    /**
     * 进入场景成功
     */
    protected boolean isEnterSceneSuccess;

    @Override
    public void init(ClientAgent clientAgent) {
        this.clientAgent = clientAgent;
    }

    public void setLoginSuccess(boolean loginSuccess) {
        isLoginSuccess = loginSuccess;
    }

    public void setEnterSceneSuccess(boolean enterSceneSuccess) {
        isEnterSceneSuccess = enterSceneSuccess;
    }
}
