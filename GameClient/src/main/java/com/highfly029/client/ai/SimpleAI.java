package com.highfly029.client.ai;

import java.util.concurrent.TimeUnit;

import com.highfly029.client.tool.ClientConfigTool;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.utils.RandomUtils;


/**
 * @ClassName SimpleAI
 * @Description 简单的AI
 * @Author liyunpeng
 **/
public class SimpleAI extends AbstractAI {

    /**
     * 等待一段时间
     */
    private long waitEndTime = 0;

    @Override
    public void onLoop() {
        if (!isLoginSuccess) {
            return;
        }
        if (!isEnterSceneSuccess) {
            return;
        }
        if (checkWait()) {
            return;
        }

        //随机切换场景
        int randomSecondTime = RandomUtils.randomInt(5, 10);
        int randomSceneID = ClientConfigTool.getSceneConfig().getRandomCityScene();
        LoggerTool.systemLogger.info("SimpleAI random sceneID={}, second={}", randomSceneID, randomSecondTime);
        clientAgent.sendGmCommand("/gm switchScene " + randomSceneID + " 0");
        setWaitTime(randomSecondTime);
    }

    private boolean checkWait() {
        if (waitEndTime > 0) {
            if (SystemTimeTool.getMillTime() >= waitEndTime) {
                waitEndTime = 0;
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    private void setWaitTime(int secondTime) {
        waitEndTime = SystemTimeTool.getMillTime() + TimeUnit.SECONDS.toMillis(secondTime);
    }
}
