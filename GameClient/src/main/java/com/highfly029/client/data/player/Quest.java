package com.highfly029.client.data.player;

import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName Quest
 * @Description 任务
 * @Author liyunpeng
 **/
public class Quest {
    /**
     * questType -> questID -> qustData
     */
    public IntObjectMap<QuestTypeData> allQuest = new IntObjectMap<>(QuestTypeData[]::new);

    public void addAcceptQuest(int questType, QuestData questData) {
        QuestTypeData questTypeData = allQuest.get(questType);
        if (questTypeData == null) {
            questTypeData = new QuestTypeData();
            allQuest.put(questType, questTypeData);
        }
        questTypeData.acceptQuests.put(questData.questID, questData);
    }

    public void addCompleteQuest(int questType, int questID) {
        QuestTypeData questTypeData = allQuest.get(questType);
        questTypeData.acceptQuests.remove(questID);
        questTypeData.completeQuests.add(questID);
    }

    public void giveUpQuest(int questType, int questID) {
        QuestTypeData questTypeData = allQuest.get(questType);
        questTypeData.acceptQuests.remove(questID);
    }

    public void updateGoalData(int questType, int questID, int goalID, int progress) {
        QuestTypeData questTypeData = allQuest.get(questType);
        QuestData questData = questTypeData.acceptQuests.get(questID);
        GoalData goalData = questData.goals.get(goalID);
        goalData.progress = progress;
    }

    public void addGoalData(int questType, int questID, GoalData goalData) {
        QuestTypeData questTypeData = allQuest.get(questType);
        QuestData questData = questTypeData.acceptQuests.get(questID);
        questData.goals.put(goalData.goalID, goalData);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        int freeValue = allQuest.getFreeValue();
        int[] keys = allQuest.getKeys();
        QuestTypeData[] questTypeDatas = allQuest.getValues();
        int key;
        QuestTypeData questTypeData;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                stringBuilder.append("=========== questType=").append(key).append(" ==========\n");
                if ((questTypeData = questTypeDatas[i]) != null) {
                    stringBuilder.append("---completeQuests---\n");
                    stringBuilder.append(questTypeData.completeQuests);
                    stringBuilder.append("\n");
                    stringBuilder.append("---acceptQuests---\n");
                    questTypeData.acceptQuests.foreachImmutable((k, v) -> {
                        stringBuilder.append("acceptQuestID=").append(k).append(" questData=[").append(v).append("]\n");
                    });
                }
                stringBuilder.append("\n");
            }
        }
        return stringBuilder.toString();
    }
}
