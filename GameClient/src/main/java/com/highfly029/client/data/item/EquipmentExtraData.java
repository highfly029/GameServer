package com.highfly029.client.data.item;

import com.highfly029.common.template.equipment.EquipmentTemplate;
import com.highfly029.utils.collection.IntIntMap;

/**
 * @ClassName EquipmentExtraData
 * @Description 装备物品扩展数据
 * @Author liyunpeng
 **/
public class EquipmentExtraData {
    public EquipmentTemplate equipmentTemplate;
    //装备属性(一般是随机出来的)
    public IntIntMap attributes;

    public static EquipmentExtraData create() {
        EquipmentExtraData equipmentExtraData = new EquipmentExtraData();
        return equipmentExtraData;
    }

    public void init(int itemID) {
//        this.equipmentTemplate = EquipConfig.getEquipmentTemplate(itemID);

        //TODO 临时增加随机属性
        attributes = new IntIntMap();
        if (itemID < 5) {
            //增加100攻击
            attributes.addValue(65, 100);
        }
    }

    public EquipmentExtraData clone() {
        EquipmentExtraData equipmentExtraData = EquipmentExtraData.create();
        equipmentExtraData.equipmentTemplate = this.equipmentTemplate;

        return equipmentExtraData;
    }
}
