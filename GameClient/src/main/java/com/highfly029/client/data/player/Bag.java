package com.highfly029.client.data.player;


import com.highfly029.client.data.item.ItemData;

/**
 * @ClassName Bag
 * @Description Bag
 * @Author liyunpeng
 **/
public class Bag {
    public int gridNum;
    public ItemData[] items;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("gridNum=").append(gridNum).append("\n");
        ItemData itemData;
        for (int i = 0; i < items.length; i++) {
            if ((itemData = items[i]) != null) {
                stringBuilder.append("index:").append(i).append(itemData).append("\n");
            }
        }
        return stringBuilder.toString();
    }
}
