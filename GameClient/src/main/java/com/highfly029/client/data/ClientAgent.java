package com.highfly029.client.data;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.client.ai.AbstractAI;
import com.highfly029.client.data.entity.Entity;
import com.highfly029.client.data.player.Bag;
import com.highfly029.client.data.player.Equipment;
import com.highfly029.client.data.player.Player;
import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbEntity;
import com.highfly029.common.protocol.packet.PbEquipment;
import com.highfly029.common.protocol.packet.PbGm;
import com.highfly029.common.protocol.packet.PbQuest;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.common.template.bag.PlayerBagTypeConst;
import com.highfly029.common.template.equipment.EquipmentTypeConst;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.utils.collection.ObjObjMap;

import io.netty.channel.Channel;

/**
 * @ClassName ClientAgent
 * @Description 一个客户端的代理
 * @Author liyunpeng
 **/
public class ClientAgent {
    /**
     * 客户端连接
     */
    public TcpClient logicConnect;

    public TcpClient sceneConnect;

    /**
     * 连接集合
     */
    private ObjObjMap<Channel, TcpClient> connectMap = new ObjObjMap<>(Channel[]::new, TcpClient[]::new);

    public long accountID;
    //源逻辑服id
    public int sourceLogicID;

    /**
     * 客户端包序号
     */
    public long sequenceNum = 0;
    /**
     * 默认不是断线重连
     */
    public boolean isReconnect = false;

    /**
     * 逻辑世界索引
     */
    public int logicWorldIndex;

    /**
     * 服务器分发的网络唯一标示
     */
    public int socketID = 0;
    /**
     * 服务器生层的socket校验令牌
     */
    public int token;
    /**
     * 客户端玩家数据
     */
    public Player player;
    /**
     * 客户端实体单位数据 需要渲染线程使用、所以需要线程安全
     */
    public Map<Integer, Entity> entityMap = new ConcurrentHashMap<>();
    /**
     * 界面是否需要重绘
     */
    public boolean isGuiNeedRepaint;
    /**
     * 界面是否需要显示返回内容
     */
    private String respText;
    /**
     * 客户端接收序号
     */
    private long clientReceivedIndex;

    private PreEnterSceneData preEnterSceneData;

    /**
     * 客户端AI
     */
    private AbstractAI ai;

    public ClientAgent(AbstractAI ai) {
        this.ai = ai;
    }

    public AbstractAI getAi() {
        return ai;
    }

    public PreEnterSceneData getPreEnterSceneData() {
        return preEnterSceneData;
    }

    public void setPreEnterSceneData(PreEnterSceneData preEnterSceneData) {
        this.preEnterSceneData = preEnterSceneData;
    }

    public void setRespText(String respText) {
        this.respText = respText;
    }

    public String getRespText() {
        return respText;
    }

    private long getIncrementSequence() {
        return ++sequenceNum;
    }

    public long getClientReceivedIndex() {
        return clientReceivedIndex;
    }

    public void clientReceivedIndexIncrease() {
        clientReceivedIndex++;
    }

    public void clientReceivedIndexReset() {
        clientReceivedIndex = 0;
    }

    public TcpClient createConnect(String name, String host, int port) {
        TcpClient tcpClient = new TcpClient();
        tcpClient.setName(name);
        tcpClient.setWorkerThreadNum(2);
        tcpClient.setReaderIdleTime(60);
        tcpClient.setWriterIdleTime(30);
        tcpClient.setTimeOutMillis(60000);
        tcpClient.setHost(host);
        tcpClient.setPort(port);
        tcpClient.connect();
        LoggerTool.systemLogger.info("开始连接 {}", tcpClient);
        return tcpClient;
    }

    /** 增加连接 */
    public void addConnect(Channel channel, TcpClient tcpClient) {
        connectMap.put(channel, tcpClient);
    }

    /**
     * 根据连接获取client
     * @param channel
     * @return
     */
    public TcpClient getConnect(Channel channel) {
        return connectMap.get(channel);
    }

    /**
     * 发送消息
     *
     * @param protocolCode
     * @param message
     */
    private void send(TcpClient tcpClient, int protocolCode, GeneratedMessageV3 message) {
        PbPacket.TcpPacketClient.Builder builder = PbPacket.TcpPacketClient.newBuilder();
        builder.setPtCode(protocolCode);

        if (message != null) {
            builder.setData(message.toByteString());
        }
        builder.setSequenceNum(getIncrementSequence());
        tcpClient.getChannel().writeAndFlush(builder.build());
    }

    /**
     * 向逻辑服发送消息
     * @param protocolCode
     * @param message
     */
    public void sendLogic(int protocolCode, GeneratedMessageV3 message) {
        send(logicConnect, protocolCode, message);
    }

    /**
     * 向场景服发送消息
     * @param protocolCode
     * @param message
     */
    public void sendScene(int protocolCode, GeneratedMessageV3 message) {
        send(sceneConnect, protocolCode, message);
    }

    /**
     * 获取玩家实体数据
     *
     * @return
     */
    public Entity getPlayerEntity() {
        if (player != null && player.playerEntity != null) {
            return player.playerEntity;
        }
        return null;
    }

    /**
     * 发送指令
     *
     * @param command
     */
    public void sendGmCommand(String command) {
        if (command.startsWith("/gm")) {
            //服务器gm命令
            PbGm.C2SGmCommand.Builder req = PbGm.C2SGmCommand.newBuilder();
            req.setCommand(command);
            sendLogic(PtCode.C2SGmCommand, req.build());
        } else {
            //客户端gm命令
            String[] params = command.split("\\s+");
            switch (params[0]) {
                case "printBag" -> {
                    Bag bag = player.bagMap.get(PlayerBagTypeConst.Bag);
                    LoggerTool.systemLogger.info("printBag\n{}", bag != null ? bag : "");
                }
                case "printEquipment" -> {
                    Equipment equipment = player.equipmentMap.get(EquipmentTypeConst.Normal);
                    LoggerTool.systemLogger.info("printEquipment\n{}", equipment != null ? equipment : "");
                }
                case "putOn" -> {
                    int index = Integer.parseInt(params[1]);
                    putOnEquipment(PlayerBagTypeConst.Bag, index, EquipmentTypeConst.Normal);
                }
                case "putOff" -> {
                    int slot = Integer.parseInt(params[1]);
                    putOffEquipment(EquipmentTypeConst.Normal, slot, PlayerBagTypeConst.Bag);
                }
                case "printAsset" -> {
                    LoggerTool.systemLogger.info("printAsset\n{}", player.asset);
                }
                case "printRole" -> {
                    LoggerTool.systemLogger.info("printRole\n{}", player.role);
                }
                case "printFunction" -> {
                    LoggerTool.systemLogger.info("printFunction\n{}", player.function.openFunctions);
                }
                case "printQuest" -> {
                    LoggerTool.systemLogger.info("printQuest\n{}", player.quest);
                }
                case "acceptQuest" -> {
                    PbQuest.C2SAcceptQuest.Builder builder = PbQuest.C2SAcceptQuest.newBuilder();
                    int questType = Integer.parseInt(params[1]);
                    int questID = Integer.parseInt(params[2]);
                    builder.setQuestType(questType);
                    builder.setQuestID(questID);
                    sendLogic(PtCode.C2SAcceptQuest, builder.build());
                }
                case "commitQuest" -> {
                    PbQuest.C2SCommitQuest.Builder builder = PbQuest.C2SCommitQuest.newBuilder();
                    int questType = Integer.parseInt(params[1]);
                    int questID = Integer.parseInt(params[2]);
                    if (params.length == 4) {
                        int selectRewardIndex = Integer.parseInt(params[3]);
                        builder.setSelectRewardIndex(selectRewardIndex);
                    }
                    builder.setQuestType(questType);
                    builder.setQuestID(questID);
                    sendLogic(PtCode.C2SCommitQuest, builder.build());
                }
                case "giveUpQuest" -> {
                    PbQuest.C2SGiveUpQuest.Builder builder = PbQuest.C2SGiveUpQuest.newBuilder();
                    int questType = Integer.parseInt(params[1]);
                    int questID = Integer.parseInt(params[2]);
                    builder.setQuestType(questType);
                    builder.setQuestID(questID);
                    sendLogic(PtCode.C2SGiveUpQuest, builder.build());
                }
                case "pickUp" -> {
                    PbScene.C2SPickUpDropItems.Builder builder = PbScene.C2SPickUpDropItems.newBuilder();
                    int entityID = Integer.parseInt(params[1]);
                    builder.setEntityID(entityID);
                    sendLogic(PtCode.C2SPickUpDropItems, builder.build());
                }
                case "createUnion" -> {
                    PbUnion.C2SCreateUnion.Builder builder = PbUnion.C2SCreateUnion.newBuilder();
                    builder.setName(params[1]);
                    sendLogic(PtCode.C2SCreateUnion, builder.build());
                }
                default -> LoggerTool.systemLogger.error("invalid gm command {}", command);
            }
        }
    }

    /**
     * 穿装备
     */
    public void putOnEquipment(int bagType, int index, int equipmentType) {
        PbEquipment.C2SPutOnEquipment.Builder builder = PbEquipment.C2SPutOnEquipment.newBuilder();
        builder.setBagType(bagType);
        builder.setIndex(index);
        builder.setEquipmentType(equipmentType);
        sendLogic(PtCode.C2SPutOnEquipment, builder.build());
    }

    /**
     * 脱装备
     */
    public void putOffEquipment(int equipmentType, int slot, int bagType) {
        PbEquipment.C2SPutOffEquipment.Builder builder = PbEquipment.C2SPutOffEquipment.newBuilder();
        builder.setEquipmentType(equipmentType);
        builder.setSlot(slot);
        builder.setBagType(bagType);
        sendLogic(PtCode.C2SPutOffEquipment, builder.build());
    }

    /**
     * 开始移动
     *
     * @param dir
     */
    public void startMove(Vector3 dir) {
        if (dir.equal(Vector3.ZERO)) {
            return;
        }
        PbEntity.C2SEntityMoveStart.Builder req = PbEntity.C2SEntityMoveStart.newBuilder();
        req.setEntityID(getPlayerEntity().getId());
        req.setCurDir(PbCommonUtils.vector3Obj2Pb(getPlayerEntity().getCurPos()));
        req.setCurDir(PbCommonUtils.vector3Obj2Pb(dir));
        sendScene(PtCode.C2SEntityMoveStart, req.build());
    }

    /**
     * 停止移动
     */
    public void stopMove() {
        PbEntity.C2SEntityMoveStop.Builder req = PbEntity.C2SEntityMoveStop.newBuilder();
        req.setEntityID(getPlayerEntity().getId());
        sendScene(PtCode.C2SEntityMoveStop, req.build());
    }
}
