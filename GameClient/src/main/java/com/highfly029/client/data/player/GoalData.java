package com.highfly029.client.data.player;

import com.highfly029.common.template.goal.GoalTemplate;

/**
 * @ClassName GoalData
 * @Description 目标数据
 * @Author liyunpeng
 **/
public class GoalData {
    /**
     * 目标id
     */
    public int goalID;
    /**
     * 进度
     */
    public int progress;
    /**
     * 模板
     */
    public GoalTemplate template;
}
