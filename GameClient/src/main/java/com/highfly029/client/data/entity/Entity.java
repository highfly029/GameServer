package com.highfly029.client.data.entity;

import com.highfly029.common.data.base.Vector3;

/**
 * @ClassName Entity
 * @Description Entity
 * @Author liyunpeng
 **/
public class Entity {
    /**
     * 唯一id
     */
    private int id;
    /**
     * 类型
     */
    private byte type;
    /**
     * 名字
     */
    private String name;
    /**
     * 当前坐标
     */
    private Vector3 curPos = Vector3.create();
    /**
     * 当前角色朝向
     */
    private Vector3 direction = Vector3.create();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vector3 getCurPos() {
        return curPos;
    }

    public void setCurPos(Vector3 curPos) {
        this.curPos = curPos;
    }

    public Vector3 getDirection() {
        return direction;
    }

    public void setDirection(Vector3 direction) {
        this.direction = direction;
    }
}
