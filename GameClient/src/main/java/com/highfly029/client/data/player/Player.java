package com.highfly029.client.data.player;

import com.highfly029.client.data.entity.Entity;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName Player
 * @Description 玩家
 * @Author liyunpeng
 **/
public class Player {
    public int playerEntityID;

    public long playerID;

    /**
     * 玩家实体数据
     */
    public Entity playerEntity;
    /**
     * 背包
     */
    public IntObjectMap<Bag> bagMap = new IntObjectMap<>(Bag[]::new);
    /**
     * 装备
     */
    public IntObjectMap<Equipment> equipmentMap = new IntObjectMap<>(Equipment[]::new);
    /**
     * 资产
     */
    public Asset asset = new Asset();
    /**
     * 角色相关
     */
    public Role role = new Role();
    /**
     * 功能开启
     */
    public Function function = new Function();
    /**
     * 任务
     */
    public Quest quest = new Quest();

}
