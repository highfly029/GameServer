package com.highfly029.client.data.player;

import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName QuestTypeData
 * @Description 任务类型数据
 * @Author liyunpeng
 **/
public class QuestTypeData {
    /**
     * 已完成任务
     */
    public IntSet completeQuests = new IntSet();
    /**
     * 已接取的任务
     */
    public IntObjectMap<QuestData> acceptQuests = new IntObjectMap<>(QuestData[]::new);
}
