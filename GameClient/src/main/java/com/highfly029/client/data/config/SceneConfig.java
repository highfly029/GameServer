package com.highfly029.client.data.config;

import com.highfly029.common.template.scene.SceneTemplate;
import com.highfly029.common.template.scene.SceneTypeConst;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName SceneConfig
 * @Description SceneConfig
 * @Author liyunpeng
 **/
public class SceneConfig extends BaseGlobalConfig {

    //sceneID -> SceneTemplate
    private final IntObjectMap<SceneTemplate> sceneTemplates = new IntObjectMap<>(SceneTemplate[]::new);

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(SceneTemplate.class);
        SceneTemplate sceneTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            sceneTemplate = (SceneTemplate) list.get(i);
            sceneTemplates.put(sceneTemplate.getSceneID(), sceneTemplate);
        }
    }

    /**
     * 获取场景模板
     *
     * @param sceneID
     * @return
     */
    public SceneTemplate getSceneTemplate(int sceneID) {
        return sceneTemplates.get(sceneID);
    }

    @Override
    public void check(boolean isHotfix) {

    }

    /**
     * 获取随机主城id
     * @return
     */
    public int getRandomCityScene() {
        IntObjectMap<SceneTemplate> map = new IntObjectMap<>(SceneTemplate[]::new);
        sceneTemplates.foreachImmutable((k, v) -> {
            if (v.getSceneType() == SceneTypeConst.CityScene) {
                map.put(k, v);
            }
        });
        return RandomUtils.random(map).getSceneID();
    }
}
