package com.highfly029.client.data.player;

import com.highfly029.utils.collection.IntBooleanMap;

/**
 * @ClassName Function
 * @Description 功能开启
 * @Author liyunpeng
 **/
public class Function {
    public IntBooleanMap openFunctions = new IntBooleanMap();
}
