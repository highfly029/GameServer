package com.highfly029.client.data.player;

import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName Equipment
 * @Description Equipment
 * @Author liyunpeng
 **/
public class Equipment {
    /**
     * 装备槽数据 key=slotIndex value=EquipmentSLotData
     */
    public IntObjectMap<EquipmentSlotData> equipmentSlots = new IntObjectMap<>(EquipmentSlotData[]::new);

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        equipmentSlots.foreachImmutable((slot, data) -> {
            stringBuilder.append("slot:").append(slot).append(" EquipmentSlotData:").append(data).append("\n");
        });
        return stringBuilder.toString();
    }
}
