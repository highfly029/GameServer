package com.highfly029.client.data.player;

/**
 * @ClassName Role
 * @Description 角色数据
 * @Author liyunpeng
 **/
public class Role {
    /**
     * 等级
     */
    public int level;
    /**
     * 经验
     */
    public int exp;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("level=").append(level).append("\n");
        stringBuilder.append("exp=").append(exp).append("\n");
        return stringBuilder.toString();
    }
}
