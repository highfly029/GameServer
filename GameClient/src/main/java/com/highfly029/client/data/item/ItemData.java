package com.highfly029.client.data.item;

import com.highfly029.client.data.item.EquipmentExtraData;
import com.highfly029.common.template.item.ItemTemplate;
import com.highfly029.common.template.item.ItemTypeConst;

/**
 * @ClassName ItemData
 * @Description 物品数据
 * @Author liyunpeng
 **/
public class ItemData {
    /**
     * 物品产出者id,服务器自己用,可以用来观察非法物品的流向,仅可以流通的物品有此值,在物品流通的日志中打印此数值
     */
    private long createID;
    /**
     * 物品id
     */
    private int itemID;
    /**
     * 物品数量
     */
    private int itemNum;
    /**
     * 是否绑定
     */
    private boolean isBind;
    /**
     * 过期时间戳, 0表示永远不会过期
     */
    private long invalidTime = 0L;
    /**
     * 物品配置
     */
    private ItemTemplate itemTemplate;
    /**
     * 装备物品扩展数据
     */
    private EquipmentExtraData equipmentExtraData;

    /**
     * 物品是否有独立的扩展数据
     *
     * @return
     */
    public boolean isHaveExtraData() {
        return equipmentExtraData != null;
    }

    public void init(int itemID, int itemNum) {
        this.itemID = itemID;
        this.itemNum = itemNum;
        if (this.itemTemplate.getType() == ItemTypeConst.Equipment) {
            equipmentExtraData = EquipmentExtraData.create();
            equipmentExtraData.init(itemID);
        }
    }

    public void init(int itemID, int itemNum, boolean isBind) {
        this.itemID = itemID;
        this.itemNum = itemNum;
        this.isBind = isBind;
        if (this.itemTemplate.getType() == ItemTypeConst.Equipment) {
            equipmentExtraData = EquipmentExtraData.create();
            equipmentExtraData.init(itemID);
        }
    }

    public long getCreateID() {
        return createID;
    }

    public void setCreateID(long createID) {
        this.createID = createID;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public int getItemNum() {
        return itemNum;
    }

    public void setItemNum(int itemNum) {
        this.itemNum = itemNum;
    }

    public boolean isBind() {
        return isBind;
    }

    public void setBind(boolean bind) {
        isBind = bind;
    }

    public long getInvalidTime() {
        return invalidTime;
    }

    public void setInvalidTime(long invalidTime) {
        this.invalidTime = invalidTime;
    }

    public ItemTemplate getItemTemplate() {
        return itemTemplate;
    }

    public void setItemTemplate(ItemTemplate itemTemplate) {
        this.itemTemplate = itemTemplate;
    }

    public EquipmentExtraData getEquipmentExtraData() {
        return equipmentExtraData;
    }

    public void setEquipmentExtraData(EquipmentExtraData equipmentExtraData) {
        this.equipmentExtraData = equipmentExtraData;
    }

    /**
     * 复制
     *
     * @param itemData
     * @return
     */
    public ItemData copy(ItemData itemData) {
        itemData.itemID = this.itemID;
        itemData.itemNum = this.itemNum;
        itemData.isBind = this.isBind;
        itemData.invalidTime = this.invalidTime;
        itemData.itemTemplate = this.itemTemplate;
        if (this.itemTemplate.getType() == ItemTypeConst.Equipment) {
            itemData.equipmentExtraData = this.equipmentExtraData.clone();
        }
        return itemData;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("itemID=")
                .append(itemID)
                .append("_itemNum=")
                .append(itemNum)
                .append("_isBind=")
                .append(isBind);

        return stringBuilder.toString();
    }
}
