package com.highfly029.client.data.player;

import com.highfly029.client.data.item.ItemData;

/**
 * @ClassName EquipmentSlotData
 * @Description 装备槽数据
 * @Author liyunpeng
 **/
public class EquipmentSlotData {
    public boolean isOpen;
    public ItemData equipment;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" isOpen=").append(isOpen)
                .append(" equipment=").append(equipment);
        return stringBuilder.toString();
    }
}
