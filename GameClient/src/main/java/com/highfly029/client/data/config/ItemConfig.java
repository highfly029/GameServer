package com.highfly029.client.data.config;

import com.highfly029.common.template.item.ItemTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName ItemConfig
 * @Description ItemConfig
 * @Author liyunpeng
 **/
public class ItemConfig extends BaseGlobalConfig {
    //itemID -> ItemTemplate
    private final IntObjectMap<ItemTemplate> itemTemplates = new IntObjectMap<>(ItemTemplate[]::new);

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(ItemTemplate.class);
        if (list != null && !list.isEmpty()) {
            ItemTemplate template;
            for (int i = 0, size = list.size(); i < size; i++) {
                template = (ItemTemplate) list.get(i);
                itemTemplates.put(template.getItemID(), template);
            }
        }
    }

    @Override
    public void check(boolean isHotfix) {

    }

    /**
     * 获取物品模板
     *
     * @param itemID
     * @return
     */
    public ItemTemplate getItemTemplate(int itemID) {
        return itemTemplates.get(itemID);
    }
}
