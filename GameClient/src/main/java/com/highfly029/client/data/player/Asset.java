package com.highfly029.client.data.player;


import com.highfly029.common.template.asset.PlayerAssetConst;

/**
 * @ClassName Asset
 * @Description 资产
 * @Author liyunpeng
 **/
public class Asset {
    /**
     * 当前值
     */
    public long[] curAssets = new long[PlayerAssetConst.count];
    /**
     * 最大值
     */
    public long[] maxAssets = new long[PlayerAssetConst.count];

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("curAssets==========\n");
        for (int i = 0; i < curAssets.length; i++) {
            stringBuilder.append("type=").append(i).append(" value=").append(curAssets[i]).append("\n");
        }
        stringBuilder.append("maxAssets==========\n");
        for (int i = 0; i < maxAssets.length; i++) {
            stringBuilder.append("type=").append(i).append(" value=").append(maxAssets[i]).append("\n");
        }
        return stringBuilder.toString();
    }
}
