package com.highfly029.client.data.player;

import com.highfly029.common.template.quest.QuestTemplate;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName QuestData
 * @Description 任务数据
 * @Author liyunpeng
 **/
public class QuestData {
    /**
     * 任务id
     */
    public int questID;
    /**
     * 过期时间
     */
    public long invalidTime;
    /**
     * 目标组 goalID -> goalData
     */
    public IntObjectMap<GoalData> goals = new IntObjectMap<>(GoalData[]::new);
    /**
     * 模板
     */
    public QuestTemplate template;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("questID=").append(questID).append(": ");
        goals.foreachImmutable((k, v) -> {
            stringBuilder.append("goalID=").append(v.goalID).append(" progress=").append(v.progress).append(" ");
        });
        return stringBuilder.toString();
    }
}
