package com.highfly029.client.data;

/**
 * @ClassName PreEnterSceneData
 * @Description PreEnterSceneData
 * @Author liyunpeng
 **/
public class PreEnterSceneData {
    private int logicServerID;
    private int logicServerWorldIndex;
    private int sceneID;
    private int sceneWorldIndex;
    private int sceneInstanceID;

    public int getLogicServerID() {
        return logicServerID;
    }

    public void setLogicServerID(int logicServerID) {
        this.logicServerID = logicServerID;
    }

    public int getLogicServerWorldIndex() {
        return logicServerWorldIndex;
    }

    public void setLogicServerWorldIndex(int logicServerWorldIndex) {
        this.logicServerWorldIndex = logicServerWorldIndex;
    }

    public int getSceneID() {
        return sceneID;
    }

    public void setSceneID(int sceneID) {
        this.sceneID = sceneID;
    }

    public int getSceneWorldIndex() {
        return sceneWorldIndex;
    }

    public void setSceneWorldIndex(int sceneWorldIndex) {
        this.sceneWorldIndex = sceneWorldIndex;
    }

    public int getSceneInstanceID() {
        return sceneInstanceID;
    }

    public void setSceneInstanceID(int sceneInstanceID) {
        this.sceneInstanceID = sceneInstanceID;
    }
}
