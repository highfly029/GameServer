package com.highfly029.client;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.highfly029.client.data.entity.Entity;
import com.highfly029.client.world.ClientGlobalWorld;
import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbEntity;
import com.highfly029.common.template.entity.EntityTypeConst;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.utils.SysUtils;

/**
 * @ClassName ClientGUI
 * @Description ClientGUI
 * @Author liyunpeng
 **/
public class ClientGUI extends JFrame implements KeyListener, ActionListener {
    private final JTextField input;
    private final JLabel label;
    private final JLabel respLabel;
    private Graphics2D graphics2D;
    private final ClientGlobalWorld world;

    /**
     * 格子地图数据
     */
    private final int xOrigin = 200;
    private final int yOrigin = 1000;
    //格子大小
    private final int gridSize = 50;
    //地图大小
    private final int mapX = 500;
    private final int mapY = 500;
    private int gridSizeInX;
    private int gridSizeInY;

    /**
     * 坐标系以屏幕左上方为0,0坐标，分别向右和向下递增
     */
    public ClientGUI(ClientGlobalWorld world) {
        this.world = world;
        this.gridSizeInX = this.mapX / gridSize;
        if (this.mapX % gridSize > 0) {
            this.gridSizeInX++;
        }
        this.gridSizeInY = this.mapY / gridSize;
        if (this.mapY % gridSize > 0) {
            this.gridSizeInY++;
        }

        if (gridSizeInX <= 0 || gridSizeInY <= 0) {
            System.out.println("gridSize error");
            SysUtils.exit();
        }

        //设置键盘监听事件
        this.addKeyListener(this);

        //设置一个漂亮的外观
        JFrame.setDefaultLookAndFeelDecorated(true);
        //不使用布局管理器
        this.setLayout(null);
        //设置标题
        this.setTitle("TitleName");
        this.setBounds(0, 0, 1024, 1024);//设置窗口的显示位置和大小

//        this.setResizable(false);//禁用最大化按钮
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBackground(Color.BLACK);//设置背景颜色
        label = new JLabel("testLabel");
        //设置颜色
        label.setForeground(Color.RED);
        label.setBounds(512, 1, 1024, 20);
        this.add(label);

        JLabel desc = new JLabel("红色是自己，绿色是别的玩家，黑色是野怪,蓝色是其他entity");
        desc.setBounds(50, 20, 2014, 20);
        this.add(desc);

        input = new JTextField("/gm ");
        input.setBounds(50, 50, 300, 30);
        this.add(input);

        JButton button = new JButton("send");

        button.addActionListener(this);
        button.setBounds(50, 80, 100, 30);
        this.add(button);

        respLabel = new JLabel();
        respLabel.setBounds(50, 100, 512, 40);
        respLabel.setText("接收信息");
        this.add(respLabel);

        //获取焦点，使按钮和键盘监听同时生效
        this.setFocusable(true);
        this.setVisible(true);
    }

    /**
     * 画场景地图
     */
    public void drawSceneMapGrid() {
        Graphics2D g = graphics2D;
        //重新填充地图区域
        g.drawRect(xOrigin, yOrigin - gridSize * gridSizeInY, gridSizeInX * gridSize, gridSizeInY * gridSize);

        g.setColor(Color.BLUE);
        //横线
        for (int i = 0; i < gridSizeInY + 1; i++) {
            g.drawLine(xOrigin, yOrigin - gridSize * i, xOrigin + gridSizeInX * gridSize, yOrigin - gridSize * i);
        }
        //竖线
        for (int i = 0; i < gridSizeInX + 1; i++) {
            g.drawLine(xOrigin + gridSize * i, yOrigin, xOrigin + gridSize * i, yOrigin - gridSize * gridSizeInY);
        }
    }

    //画自己
    public void drawSelf() {
        if (world.getPlayerEntity() == null) {
            return;
        }
        Graphics2D g = graphics2D;
        //实心圆
        g.setColor(Color.RED);
        //圆心半径
        int radius = 5;
        //posX posY 为服务器真实坐标
        int posX = (int) world.getPlayerEntity().getCurPos().x;
        int posY = (int) world.getPlayerEntity().getCurPos().z;
        //java客户端显示坐标
        posX = xOrigin + posX;
        posY = yOrigin - posY;
        //-5为 优化坐标为圆圈中心
        g.fillOval(posX - radius, posY - radius, radius * 2, radius * 2);
//        Shape circle = new Ellipse2D.Double(posX - radius, posY - radius, radius * 2, radius * 2);
//        g.fill(circle);
    }

    //画其他Entity
    public void drawEntity() {
        Graphics2D g = graphics2D;
        for (Entity entity : world.getEntityMap().values()) {
            //实心圆
            if (entity.getType() == EntityTypeConst.MONSTER) {
                g.setColor(Color.BLACK);
            } else if (entity.getType() == EntityTypeConst.PLAYER) {
                g.setColor(Color.GREEN);
            } else {
                g.setColor(Color.BLUE);
            }

            //圆心半径
            int radius = 2;
            //posX posY 为服务器真实坐标
            int posX = (int) entity.getCurPos().x;
            int posY = (int) entity.getCurPos().z;
            //java客户端显示坐标
            posX = xOrigin + posX;
            posY = yOrigin - posY;
            g.fillOval(posX - radius, posY - radius, radius * 2, radius * 2);
//            Shape circle = new Ellipse2D.Double(posX - radius, posY - radius, radius * 2, radius * 2);
//            g.fill(circle);
        }
    }

    /**
     * 渲染
     */
    public void onFrame() {
        if (world.getPlayerEntity() != null) {
            Vector3 pos = world.getPlayerEntity().getCurPos();
            StringBuilder sb = new StringBuilder();
            sb.append("player pos{" + pos.x + "," + pos.y + "," + pos.z + "}");
            this.label.setText(sb.toString());
        }

        graphics2D = (Graphics2D) this.getGraphics();
        drawSceneMapGrid();
        drawSelf();
        drawEntity();
        graphics2D.dispose();
    }


    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        updatePlayerPos(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        world.stopMove();
    }

    private void updatePlayerPos(KeyEvent e) {
        if (world.getPlayerEntity() == null) {
            return;
        }
        Vector3 dir = Vector3.create();
        Vector3 skillPos = Vector3.create();
        Vector3 skillDir = Vector3.create();
        Entity playerEntity = world.getPlayerEntity();
        switch (e.getKeyCode()) {
            case KeyEvent.VK_Q -> {

//                useSkill(1033, 1, 1, 1000080, null, null);

//                useSkill(1036, 1, 1, world.getPlayerEntity().getId(), null, null);
                useSkill(1003, 1, 2, playerEntity.getId(), playerEntity.getCurPos(), playerEntity.getDirection());
//                useSkill(1038, 1, 1, world.getPlayerEntity().getId(), null, null);
//                useSkill(1039, 1, 1, playerEntity.getId(), null, null);
//                useSkill(1040, 1, 3, 0, null, playerEntity.getDirection());

            }
            case KeyEvent.VK_W -> {

            }
            case KeyEvent.VK_E -> {

            }
            case KeyEvent.VK_R -> {

            }
            case KeyEvent.VK_UP -> {
                dir.z = 1;
                world.startMove(dir);
            }
            case KeyEvent.VK_DOWN -> {
                dir.z = -1;
                world.startMove(dir);
            }
            case KeyEvent.VK_LEFT -> {
                dir.x = -1;
                world.startMove(dir);
            }
            case KeyEvent.VK_RIGHT -> {
                dir.x = 1;
                world.startMove(dir);
            }
        }
    }

    private void useSkill(int skillID, int skillLevel, int targetType, int targetEntityID, Vector3 pos, Vector3 dir) {
        PbEntity.C2SEntityCastSkill.Builder req = PbEntity.C2SEntityCastSkill.newBuilder();
        PbCommon.PbSkillData.Builder skillDataBuilder = PbCommon.PbSkillData.newBuilder();

        skillDataBuilder.setSkillID(skillID);
        skillDataBuilder.setSkillLevel(skillLevel);
        skillDataBuilder.setSkillTargetType(targetType);
        skillDataBuilder.setTargetEntityID(targetEntityID);
        if (pos != null) {
            skillDataBuilder.setTargetPos(PbCommonUtils.vector3Obj2Pb(pos));
        }
        if (dir != null) {
            skillDataBuilder.setTargetDir(PbCommonUtils.vector3Obj2Pb(dir));
        }

        req.setSkillData(skillDataBuilder.build());
        world.useSkill(req);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = input.getText();
        world.sendGmCommand(command);

        //键盘重新获取焦点
        this.requestFocus();
    }

    public void setRespText(String result) {
        this.respLabel.setText(result);
    }
}
