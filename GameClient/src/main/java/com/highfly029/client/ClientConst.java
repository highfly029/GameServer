package com.highfly029.client;

import com.highfly029.utils.ConfigPropUtils;

/**
 * @ClassName ClientConst
 * @Description ClientConst
 * @Author liyunpeng
 **/
public class ClientConst {
    /**
     * 是否开启GUI
     */
    public static final boolean OPEN_GUI = ConfigPropUtils.getBoolValue("open_gui");

    /**
     * 是否开启AI
     */
    public static final boolean OPEN_AI = ConfigPropUtils.getBoolValue("open_ai");

}
