package com.highfly029.client.handler;

import com.highfly029.client.data.ClientAgent;
import com.highfly029.client.data.entity.Entity;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbEntity;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.utils.StringUtils;

/**
 * @ClassName EntityHandler
 * @Description EntityHandler
 * @Author liyunpeng
 **/
public class EntityHandler implements PacketHandler {
    @Override
    public void onHandlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception {
        switch (packet.getPtCode()) {
            case PtCode.S2CEntityMoveInfo -> {
                PbEntity.S2CEntityMoveInfo moveInfoPush = PbEntity.S2CEntityMoveInfo.parseFrom(packet.getData());
                PbCommon.PbVector pos = moveInfoPush.getCurPos();
                PbCommon.PbVector dir = moveInfoPush.getCurDir();
                if (moveInfoPush.getEntityID() == clientAgent.getPlayerEntity().getId()) {
                    clientAgent.getPlayerEntity().setCurPos(PbCommonUtils.vector3Pb2Obj(pos));
                    clientAgent.getPlayerEntity().setDirection(PbCommonUtils.vector3Pb2Obj(dir));
                } else {
                    Entity entity = clientAgent.entityMap.get(moveInfoPush.getEntityID());
                    if (entity != null) {
                        entity.setCurPos(PbCommonUtils.vector3Pb2Obj(pos));
                        entity.setDirection(PbCommonUtils.vector3Pb2Obj(dir));
                    }
                }

                clientAgent.isGuiNeedRepaint = true;
            }
            case PtCode.S2CEnterSelfAOIList -> {

                PbEntity.S2CEnterSelfAOIList entityListPush = PbEntity.S2CEnterSelfAOIList.parseFrom(packet.getData());
                for (PbCommon.PbEntity pbEntity : entityListPush.getEnterListList()) {
                    Entity entity = new Entity();
                    entity.setId(pbEntity.getEntityID());
                    entity.setType((byte) pbEntity.getType());
                    PbCommon.PbVector pbPos = pbEntity.getCurPos();
                    entity.setCurPos(PbCommonUtils.vector3Pb2Obj(pbPos));
                    PbCommon.PbVector pbDir = pbEntity.getDirection();
                    entity.setDirection(PbCommonUtils.vector3Pb2Obj(pbDir));
                    clientAgent.entityMap.put(entity.getId(), entity);
                }
                clientAgent.isGuiNeedRepaint = true;
            }
            case PtCode.S2CLeaveSelfAOIList -> {
                PbEntity.S2CLeaveSelfAOIList entityListPush = PbEntity.S2CLeaveSelfAOIList.parseFrom(packet.getData());
                for (int entityId : entityListPush.getLeaveListList()) {
                    clientAgent.entityMap.remove(entityId);
                }
                clientAgent.isGuiNeedRepaint = true;
            }
            case PtCode.S2CAddFixedPosEntity -> {
                PbEntity.S2CAddFixedPosEntity addFixedPosEntity = PbEntity.S2CAddFixedPosEntity.parseFrom(packet.getData());
                PbCommon.PbEntity pbEntity = addFixedPosEntity.getEntity();
                Entity entity = new Entity();
                entity.setId(pbEntity.getEntityID());
                entity.setType((byte) pbEntity.getType());
                PbCommon.PbVector pbPos = pbEntity.getCurPos();
                entity.setCurPos(PbCommonUtils.vector3Pb2Obj(pbPos));
                PbCommon.PbVector pbDir = pbEntity.getDirection();
                entity.setDirection(PbCommonUtils.vector3Pb2Obj(pbDir));
                clientAgent.entityMap.put(entity.getId(), entity);
                clientAgent.isGuiNeedRepaint = true;
            }
            case PtCode.S2CDelFixedPosEntity -> {
                PbEntity.S2CDelFixedPosEntity delFixedPosEntity = PbEntity.S2CDelFixedPosEntity.parseFrom(packet.getData());
                int entityID = delFixedPosEntity.getEntityID();
                clientAgent.entityMap.remove(entityID);
                clientAgent.isGuiNeedRepaint = true;
            }
            case PtCode.S2CSetEntityPos -> {
                PbEntity.S2CSetEntityPos entityPos = PbEntity.S2CSetEntityPos.parseFrom(packet.getData());
                LoggerTool.systemLogger.info("entityPos={}", StringUtils.removeSpace(entityPos.toString()));
                int entityID = entityPos.getEntityID();
                Entity entity = null;
                if (clientAgent.getPlayerEntity().getId() == entityID) {
                    entity = clientAgent.getPlayerEntity();
                } else {
                    entity = clientAgent.entityMap.get(entityID);
                }
                entity.setCurPos(PbCommonUtils.vector3Pb2Obj(entityPos.getCurPos()));
                clientAgent.isGuiNeedRepaint = true;

            }
            case PtCode.S2CSetEntityDir -> {
                PbEntity.S2CSetEntityDir entityDir = PbEntity.S2CSetEntityDir.parseFrom(packet.getData());
                int entityID = entityDir.getEntityID();
                Entity entity = null;
                if (clientAgent.getPlayerEntity().getId() == entityID) {
                    entity = clientAgent.getPlayerEntity();
                } else {
                    entity = clientAgent.entityMap.get(entityID);
                }
                entity.setDirection(PbCommonUtils.vector3Pb2Obj(entityDir.getCurDir()));
                clientAgent.isGuiNeedRepaint = true;

            }
            case PtCode.S2CEntityState -> {
                PbEntity.S2CEntityState entityStatePush = PbEntity.S2CEntityState.parseFrom(packet.getData());
                int entityId = entityStatePush.getEntityID();
                for (PbCommon.PbKeyValue pbKeyValue : entityStatePush.getStateList()) {
                    //死亡状态
                    if (pbKeyValue.getIntKey() == 0) {
                        if (pbKeyValue.getBoolValue() == true) {
                            clientAgent.entityMap.remove(entityId);
                            clientAgent.isGuiNeedRepaint = true;
                            return;
                        }
                    }
                }
            }
            case PtCode.S2CEntitySpecialMoveStart -> {
            }
            case PtCode.S2CEntitySpecialMoveOver -> {
                PbEntity.S2CEntitySpecialMoveOver entitySpecialMoveOverPush = PbEntity.S2CEntitySpecialMoveOver.parseFrom(packet.getData());
                int entityId = entitySpecialMoveOverPush.getEntityID();
                Entity entity = null;
                if (clientAgent.getPlayerEntity().getId() == entityId) {
                    entity = clientAgent.getPlayerEntity();
                } else {
                    entity = clientAgent.entityMap.get(entityId);
                }
                if (entity != null) {
                    PbCommon.PbVector pos = entitySpecialMoveOverPush.getCurPos();
                    entity.setCurPos(PbCommonUtils.vector3Pb2Obj(pos));
                    clientAgent.isGuiNeedRepaint = true;
                }
            }
            case PtCode.S2CEntityCastSkill -> {
                PbEntity.S2CEntityCastSkill entityUseSkillPush = PbEntity.S2CEntityCastSkill.parseFrom(packet.getData());
//                System.out.println("entityUseSkillPush:"+ entityUseSkillPush);
            }
            case PtCode.S2CAttackDamage -> {
                PbEntity.S2CAttackDamage attackDamagePush = PbEntity.S2CAttackDamage.parseFrom(packet.getData());
//                System.out.println("attackDamage:"+ attackDamagePush);
            }
            case PtCode.S2CEntityAttribute -> {
                PbEntity.S2CEntityAttribute entityAttributePush = PbEntity.S2CEntityAttribute.parseFrom(packet.getData());
//                System.out.println("entityAttributePush:"+entityAttributePush);
            }
            case PtCode.S2CEntityAddBuff -> {
                PbEntity.S2CEntityAddBuff entityAddBuffPush = PbEntity.S2CEntityAddBuff.parseFrom(packet.getData());
//                System.out.println("entityAddBuffPush:"+entityAddBuffPush);
            }
            case PtCode.S2CEntityRemoveBuff -> {
                PbEntity.S2CEntityRemoveBuff entityRemoveBuffPush = PbEntity.S2CEntityRemoveBuff.parseFrom(packet.getData());
//                System.out.println("entityRemoveBuffPush:"+entityRemoveBuffPush);
            }
            case PtCode.S2CEntityRefreshBuff -> {
                PbEntity.S2CEntityRefreshBuff entityRefreshBuffPush = PbEntity.S2CEntityRefreshBuff.parseFrom(packet.getData());
//                System.out.println("entityRefreshBuffPush:"+entityRefreshBuffPush);
            }
        }
    }
}
