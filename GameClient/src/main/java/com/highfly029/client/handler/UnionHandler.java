package com.highfly029.client.handler;

import com.highfly029.client.data.ClientAgent;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.tool.LoggerTool;

/**
 * @ClassName UnionHandler
 * @Description UnionHandler
 * @Author liyunpeng
 **/
public class UnionHandler implements PacketHandler {
    @Override
    public void onHandlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception {
        switch (packet.getPtCode()) {
            case PtCode.S2CCreateUnion -> {
                PbUnion.S2CCreateUnion req = PbUnion.S2CCreateUnion.parseFrom(packet.getData());
                LoggerTool.systemLogger.info("createUnion 结果={}", req.getResult());
            }
            case PtCode.S2CUpdateUnion -> {
                PbUnion.S2CUpdateUnion req = PbUnion.S2CUpdateUnion.parseFrom(packet.getData());
                PbUnion.PbUpdateUnionData updateUnionData = req.getUpdateUnionData();
                int updateType = updateUnionData.getUpdateType();
                LoggerTool.systemLogger.info("updateUnion type={}, intValue={}, longValue={}", updateType, updateUnionData.getIntValue1(), updateUnionData.getLongValue1());
            }
        }
    }
}
