package com.highfly029.client.handler;

import com.highfly029.client.data.ClientAgent;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.tool.LoggerTool;

/**
 * @ClassName CommonHandler
 * @Description CommonHandler
 * @Author liyunpeng
 **/
public class CommonHandler implements PacketHandler {
    @Override
    public void onHandlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception {
        switch (packet.getPtCode()) {
            case PtCode.S2CReconnectResult -> {
                clientAgent.clientReceivedIndexReset();
                LoggerTool.systemLogger.info("clientReceivedIndex={}", clientAgent.getClientReceivedIndex());
            }
        }
    }
}
