package com.highfly029.client.handler;

import com.highfly029.client.data.ClientAgent;
import com.highfly029.client.data.player.Equipment;
import com.highfly029.client.data.player.EquipmentSlotData;
import com.highfly029.client.utils.PbClientUtils;
import com.highfly029.client.data.item.ItemData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbEquipment;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;

/**
 * @ClassName EquipmentHandler
 * @Description EquipmentHandler
 * @Author liyunpeng
 **/
public class EquipmentHandler implements PacketHandler {
    @Override
    public void onHandlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception {
        switch (packet.getPtCode()) {
            case PtCode.S2CLoginPushPlayerAllEquipments -> {
                PbEquipment.S2CLoginPushPlayerAllEquipments allEquipments = PbEquipment.S2CLoginPushPlayerAllEquipments.parseFrom(packet.getData());
//                System.out.println("allEquipments:" + allEquipments);
                if (allEquipments.hasPlayerEquipmentData()) {
                    PbEquipment.PbPlayerEquipmentData pbPlayerEquipmentData = allEquipments.getPlayerEquipmentData();
                    for (int i = 0; i < pbPlayerEquipmentData.getEquipmentDatasCount(); i++) {
                        PbCommon.PbEquipmentData pbEquipmentData = pbPlayerEquipmentData.getEquipmentDatas(i);
                        int type = pbEquipmentData.getType();
                        Equipment equipment = new Equipment();
                        for (int j = 0; j < pbEquipmentData.getEquipmentSlotsCount(); j++) {
                            PbCommon.PbEquipmentSlotData pbEquipmentSlotData = pbEquipmentData.getEquipmentSlots(j);
                            EquipmentSlotData equipmentSlotData = PbClientUtils.equipmentSlotPb2Obj(pbEquipmentSlotData);
                            int slotIndex = pbEquipmentSlotData.getSlotIndex();
                            equipment.equipmentSlots.put(slotIndex, equipmentSlotData);
                        }
                        clientAgent.player.equipmentMap.put(type, equipment);
                    }
                }
            }
            case PtCode.S2CPutOnEquipment -> {
                PbEquipment.S2CPutOnEquipment req = PbEquipment.S2CPutOnEquipment.parseFrom(packet.getData());
                int type = req.getEquipmentType();
                int slot = req.getSlot();
                ItemData itemData = PbClientUtils.itemPb2Obj(req.getItemData());
                Equipment equipment = clientAgent.player.equipmentMap.get(type);
                equipment.equipmentSlots.get(slot).equipment = itemData;
            }
            case PtCode.S2CPutOffEquipment -> {
                PbEquipment.S2CPutOffEquipment req = PbEquipment.S2CPutOffEquipment.parseFrom(packet.getData());
                int type = req.getEquipmentType();
                int slot = req.getSlot();
                Equipment equipment = clientAgent.player.equipmentMap.get(type);
                equipment.equipmentSlots.get(slot).equipment = null;
            }
        }
    }
}
