package com.highfly029.client.handler;

import com.highfly029.client.data.ClientAgent;
import com.highfly029.client.data.player.GoalData;
import com.highfly029.client.data.player.QuestData;
import com.highfly029.client.data.player.QuestTypeData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbQuest;
import com.highfly029.common.template.goal.GoalTemplate;
import com.highfly029.common.template.quest.QuestTemplate;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName QuestHandler
 * @Description 任务
 * @Author liyunpeng
 **/
public class QuestHandler implements PacketHandler {
    private static final IntObjectMap<QuestTemplate> questTemplates = new IntObjectMap<>(QuestTemplate[]::new);
    private static final IntObjectMap<GoalTemplate> goalTemplates = new IntObjectMap<>(GoalTemplate[]::new);

    public QuestHandler() {
//        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(QuestTemplate.class);
//        QuestTemplate template;
//        for (int i = 0, size = list.size(); i < size; i++) {
//            template = (QuestTemplate) list.get(i);
//            questTemplates.put(template.getQuestID(), template);
//        }
//        list = TemplateTool.dataTemplates.get(GoalTemplate.class);
//        GoalTemplate goalTemplate;
//        for (int i = 0, size = list.size(); i < size; i++) {
//            goalTemplate = (GoalTemplate) list.get(i);
//            goalTemplates.put(goalTemplate.getGoalID(), goalTemplate);
//        }
    }

    @Override
    public void onHandlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception {
        switch (packet.getPtCode()) {
            case PtCode.S2CLoginPushPlayerQuestData -> {
                PbQuest.S2CLoginPushPlayerQuestData resp = PbQuest.S2CLoginPushPlayerQuestData.parseFrom(packet.getData());
                if (resp.hasPlayerQuestData()) {
                    PbQuest.PbPlayerQuestData pbPlayerQuestData = resp.getPlayerQuestData();
                    for (int i = 0, len = pbPlayerQuestData.getQuestDatasCount(); i < len; i++) {
                        PbQuest.PbQuestTypeData pbQuestTypeData = pbPlayerQuestData.getQuestDatas(i);
                        QuestTypeData questTypeData = new QuestTypeData();
                        int questType = pbQuestTypeData.getQuestType();

                        for (int j = 0, jLen = pbQuestTypeData.getCompleteQuestsCount(); j < jLen; j++) {
                            int completeID = pbQuestTypeData.getCompleteQuests(j);
                            questTypeData.completeQuests.add(completeID);
                        }

                        for (int j = 0, jLen = pbQuestTypeData.getAcceptQuestsCount(); j < jLen; j++) {
                            PbQuest.PbQuestData pbQuestData = pbQuestTypeData.getAcceptQuests(j);
                            QuestData questData = genQuest(pbQuestData);
                            questTypeData.acceptQuests.put(questData.questID, questData);
                        }
                        clientAgent.player.quest.allQuest.put(questType, questTypeData);
                    }
                }
            }
            case PtCode.S2CAcceptQuest -> {
                PbQuest.S2CAcceptQuest resp = PbQuest.S2CAcceptQuest.parseFrom(packet.getData());
                int questType = resp.getQuestType();
                PbQuest.PbQuestData pbQuestData = resp.getQuestData();
                QuestData questData = genQuest(pbQuestData);
                clientAgent.player.quest.addAcceptQuest(questType, questData);
            }
            case PtCode.S2CCommitQuest -> {
                PbQuest.S2CCommitQuest resp = PbQuest.S2CCommitQuest.parseFrom(packet.getData());
                int questType = resp.getQuestType();
                int questID = resp.getQuestID();
                clientAgent.player.quest.addCompleteQuest(questType, questID);
            }
            case PtCode.S2CGiveUpQuest -> {
                PbQuest.S2CGiveUpQuest resp = PbQuest.S2CGiveUpQuest.parseFrom(packet.getData());
                int questType = resp.getQuestType();
                int questID = resp.getQuestID();
                clientAgent.player.quest.giveUpQuest(questType, questID);
            }
            case PtCode.S2CUpdateGoalData -> {
                PbQuest.S2CUpdateGoalData resp = PbQuest.S2CUpdateGoalData.parseFrom(packet.getData());
                int questType = resp.getQuestType();
                int questID = resp.getQuestID();
                int goalID = resp.getGoalID();
                int progress = resp.getProgress();
                clientAgent.player.quest.updateGoalData(questType, questID, goalID, progress);
            }
            case PtCode.S2CAddGoalData -> {
                PbQuest.S2CAddGoalData resp = PbQuest.S2CAddGoalData.parseFrom(packet.getData());
                int questType = resp.getQuestType();
                int questID = resp.getQuestID();
                PbCommon.PbGoalData pbGoalData = resp.getGoal();
                GoalData goalData = new GoalData();
                goalData.goalID = pbGoalData.getGoalID();
                goalData.progress = pbGoalData.getProgress();
                goalData.template = goalTemplates.get(goalData.goalID);
                clientAgent.player.quest.addGoalData(questType, questID, goalData);
            }
        }
    }

    private QuestData genQuest(PbQuest.PbQuestData pbQuestData) {
        QuestData questData = new QuestData();
        questData.questID = pbQuestData.getQuestID();
        questData.invalidTime = pbQuestData.getInvalidTime();
        questData.template = questTemplates.get(questData.questID);

        for (int k = 0, kLen = pbQuestData.getGoalsCount(); k < kLen; k++) {
            PbCommon.PbGoalData pbGoalData = pbQuestData.getGoals(k);
            GoalData goalData = new GoalData();
            goalData.goalID = pbGoalData.getGoalID();
            goalData.progress = pbGoalData.getProgress();
            goalData.template = goalTemplates.get(goalData.goalID);
            questData.goals.put(goalData.goalID, goalData);
        }
        return questData;
    }
}
