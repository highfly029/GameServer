package com.highfly029.client.handler;

import com.highfly029.client.data.ClientAgent;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbFunction;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;

/**
 * @ClassName FunctionHandler
 * @Description FunctionHandler
 * @Author liyunpeng
 **/
public class FunctionHandler implements PacketHandler {
    @Override
    public void onHandlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception {
        switch (packet.getPtCode()) {
            case PtCode.S2CLoginPushPlayerFunction -> {
                PbFunction.S2CLoginPushPlayerFunction resp = PbFunction.S2CLoginPushPlayerFunction.parseFrom(packet.getData());
                for (int i = 0, len = resp.getPlayerFunctionData().getOpenFunctionsCount(); i < len; i++) {
                    int functionID = resp.getPlayerFunctionData().getOpenFunctions(i);
                    clientAgent.player.function.openFunctions.put(functionID, true);
                }
            }
            case PtCode.S2COpenFunction -> {
                PbFunction.S2COpenFunction resp = PbFunction.S2COpenFunction.parseFrom(packet.getData());
                int functionID = resp.getFunctionID();
                clientAgent.player.function.openFunctions.put(functionID, true);
            }
        }
    }
}
