package com.highfly029.client.handler;

import com.highfly029.client.data.ClientAgent;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;

/**
 * @ClassName PacketHandler
 * @Description 协议处理接口
 * @Author liyunpeng
 **/
public interface PacketHandler {
    void onHandlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception;
}
