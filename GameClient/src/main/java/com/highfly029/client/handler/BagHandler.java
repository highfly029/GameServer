package com.highfly029.client.handler;

import com.highfly029.client.data.ClientAgent;
import com.highfly029.client.data.player.Bag;
import com.highfly029.client.utils.PbClientUtils;
import com.highfly029.client.data.item.ItemData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbBag;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.tool.LoggerTool;

/**
 * @ClassName BagHandler
 * @Description bag
 * @Author liyunpeng
 **/
public class BagHandler implements PacketHandler {
    @Override
    public void onHandlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception {
        switch (packet.getPtCode()) {
            case PtCode.S2CLoginPushPlayerAllBags -> {
                PbBag.S2CLoginPushPlayerAllBags allBags = PbBag.S2CLoginPushPlayerAllBags.parseFrom(packet.getData());
//                System.out.println("allBags:" + allBags);
                if (allBags.hasPlayerBagData()) {
                    PbBag.PbPlayerBagData pbPlayerBagsData = allBags.getPlayerBagData();
                    for (int i = 0, len = pbPlayerBagsData.getItemContainerDatasCount(); i < len; i++) {
                        PbCommon.PbItemContainerData pbItemContainerData = pbPlayerBagsData.getItemContainerDatas(i);
                        Bag bag = new Bag();
                        bag.gridNum = pbItemContainerData.getGridNum();
                        bag.items = new ItemData[bag.gridNum];
                        for (int j = 0, jLen = pbItemContainerData.getItemDatasCount(); j < jLen; j++) {
                            PbCommon.PbItemData pbItemData = pbItemContainerData.getItemDatas(j);
                            ItemData itemData = PbClientUtils.itemPb2Obj(pbItemData);
                            int index = pbItemData.getIndex();
                            bag.items[index] = itemData;
                        }
                        clientAgent.player.bagMap.put(pbItemContainerData.getType(), bag);
                    }
                }
            }
            case PtCode.S2CAddItems -> {
                PbBag.S2CAddItems resp = PbBag.S2CAddItems.parseFrom(packet.getData());
                Bag bag = clientAgent.player.bagMap.get(resp.getBagType());
                if (resp.getAddItemsCount() > 0) {
                    for (int i = 0; i < resp.getAddItemsCount(); i++) {
                        PbCommon.PbKeyValue pbKeyValue = resp.getAddItems(i);
                        int index = pbKeyValue.getIntKey();
                        int addNum = pbKeyValue.getIntValue();
                        bag.items[index].setItemNum(bag.items[index].getItemNum() + addNum);
                    }
                }
                if (resp.getNewItemsCount() > 0) {
                    for (int i = 0; i < resp.getNewItemsCount(); i++) {
                        PbCommon.PbItemData pbItemData = resp.getNewItems(i);
                        ItemData itemData = PbClientUtils.itemPb2Obj(pbItemData);
                        int index = pbItemData.getIndex();
                        if (bag.items[index] != null) {
                            LoggerTool.systemLogger.error("bag addItems error index={}, itemData=", index, bag.items[index]);
                        }
                        bag.items[index] = itemData;
                    }
                }
            }
            case PtCode.S2CRemoveItems -> {
                PbBag.S2CRemoveItems resp = PbBag.S2CRemoveItems.parseFrom(packet.getData());
                Bag bag = clientAgent.player.bagMap.get(resp.getBagType());
                if (resp.getRemoveItemsCount() > 0) {
                    for (int i = 0; i < resp.getRemoveItemsCount(); i++) {
                        PbCommon.PbKeyValue pbKeyValue = resp.getRemoveItems(i);
                        int index = pbKeyValue.getIntKey();
                        int removeNum = pbKeyValue.getIntValue();
                        bag.items[index].setItemNum(bag.items[index].getItemNum() - removeNum);
                        if (bag.items[index].getItemNum() == 0) {
                            bag.items[index] = null;
                        }
                    }
                }
            }
            case PtCode.S2CRemoveItemByIndex -> {
                PbBag.S2CRemoveItemByIndex resp = PbBag.S2CRemoveItemByIndex.parseFrom(packet.getData());
                Bag bag = clientAgent.player.bagMap.get(resp.getBagType());
                int index = resp.getIndex();
                int num = resp.getNum();
                bag.items[index].setItemNum(bag.items[index].getItemNum() - num);
                if (bag.items[index].getItemNum() == 0) {
                    bag.items[index] = null;
                }
            }
            case PtCode.S2CSwapItemByIndex -> {
                PbBag.S2CSwapItemByIndex resp = PbBag.S2CSwapItemByIndex.parseFrom(packet.getData());
                Bag bag = clientAgent.player.bagMap.get(resp.getBagType());
                int fromIndex = resp.getFromIndex();
                int toIndex = resp.getToIndex();
                ItemData tmpItemData = bag.items[fromIndex];
                bag.items[fromIndex] = bag.items[toIndex];
                bag.items[toIndex] = tmpItemData;
            }
            case PtCode.S2CSplitItem -> {
                PbBag.S2CSplitItem resp = PbBag.S2CSplitItem.parseFrom(packet.getData());
                Bag bag = clientAgent.player.bagMap.get(resp.getBagType());
                int index = resp.getIndex();
                int num = resp.getNum();
                PbCommon.PbItemData pbItemData = resp.getNewItemData();
                ItemData itemData = PbClientUtils.itemPb2Obj(pbItemData);
                int newIndex = pbItemData.getIndex();

                bag.items[index].setItemNum(bag.items[index].getItemNum() - num);
                bag.items[newIndex] = itemData;
            }
            case PtCode.S2CSort -> {
                PbBag.S2CSort resp = PbBag.S2CSort.parseFrom(packet.getData());
                Bag bag = clientAgent.player.bagMap.get(resp.getBagType());
                for (int i = 0; i < bag.gridNum; i++) {
                    bag.items[i] = null;
                }
                for (int i = 0; i < resp.getItemsCount(); i++) {
                    PbCommon.PbItemData pbItemData = resp.getItems(i);
                    ItemData itemData = PbClientUtils.itemPb2Obj(pbItemData);
                    int index = pbItemData.getIndex();
                    bag.items[index] = itemData;
                }
            }
            case PtCode.S2CResetGridNum -> {
                PbBag.S2CResetGridNum resp = PbBag.S2CResetGridNum.parseFrom(packet.getData());
                Bag bag = clientAgent.player.bagMap.get(resp.getBagType());
                int newGridNum = resp.getNewGridNum();
                if (newGridNum > bag.gridNum) {
                    ItemData[] newArray = new ItemData[newGridNum];
                    System.arraycopy(bag.items, 0, newArray, 0, bag.gridNum);
                    bag.items = newArray;
                } else if (newGridNum < bag.gridNum) {
                    ItemData[] newArray = new ItemData[newGridNum];
                    System.arraycopy(bag.items, 0, newArray, 0, newGridNum);
                    bag.items = newArray;
                }
                bag.gridNum = newGridNum;
            }
        }
    }
}
