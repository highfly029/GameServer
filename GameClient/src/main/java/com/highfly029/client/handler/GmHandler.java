package com.highfly029.client.handler;

import com.highfly029.client.data.ClientAgent;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbGm;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.tool.LoggerTool;

/**
 * @ClassName GmHandler
 * @Description GmHandler
 * @Author liyunpeng
 **/
public class GmHandler implements PacketHandler {
    @Override
    public void onHandlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception {
        switch (packet.getPtCode()) {
            case PtCode.S2CGmCommand -> {
                PbGm.S2CGmCommand resp = PbGm.S2CGmCommand.parseFrom(packet.getData());
                String result = resp.getResult();
                clientAgent.setRespText(result);
                LoggerTool.systemLogger.info("收到gm信息 result={}", result);
            }
        }
    }
}
