package com.highfly029.client.handler;

import com.highfly029.client.data.ClientAgent;
import com.highfly029.client.data.PreEnterSceneData;
import com.highfly029.client.data.entity.Entity;
import com.highfly029.client.data.player.Player;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbLogin;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.protocol.BasePtCode;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.utils.SysUtils;

/**
 * @ClassName LoginHandler
 * @Description 登录
 * @Author liyunpeng
 **/
public class LoginHandler implements PacketHandler {

    @Override
    public void onHandlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception {
        switch (packet.getPtCode()) {
            case BasePtCode.CONNECT_TYPE_RESP -> {
                if (tcpClient.getName().equals("logic")) {
                    PbCommon.ConnectTypeResp resp = PbCommon.ConnectTypeResp.parseFrom(packet.getData());
                    if (resp.getErrorCode() > 0) {
                        LoggerTool.systemLogger.info("连接错误 关闭客户端 请重新启动 errorCode={}", resp.getErrorCode());
                        SysUtils.exit();
                        return;
                    }

                    //默认false、第一次连接成功后改为true、后续走重连机制
                    clientAgent.isReconnect = true;
                    //每次连接都有一个新的socktID
                    clientAgent.socketID = resp.getSocketID();

                    clientAgent.logicWorldIndex = resp.getWorldIndex();

                    if (!resp.getIsReConnect()) {
                        PreEnterSceneData preEnterSceneData = clientAgent.getPreEnterSceneData();
                        if (preEnterSceneData == null) {
                            preEnterSceneData = new PreEnterSceneData();
                            clientAgent.setPreEnterSceneData(preEnterSceneData);
                        }
                        preEnterSceneData.setLogicServerID(resp.getIdentifier());
                        preEnterSceneData.setLogicServerWorldIndex(resp.getWorldIndex());
                        //只有第一次连接成功才设置token
                        clientAgent.token = resp.getToken();

                        //只有第一次连接成功才走登陆流程
                        PbLogin.C2SRoleList.Builder builder = PbLogin.C2SRoleList.newBuilder();
                        builder.setAccountID(clientAgent.accountID);
                        builder.setSourceLogicID(clientAgent.sourceLogicID);
                        clientAgent.sendLogic(PtCode.C2SRoleList, builder.build());
                        LoggerTool.systemLogger.info("获取对方类型 type={}, 请求角色列表", resp.getType());
                    }
                } else {
                    playerEnterScene(clientAgent);
                }
            }
            case PtCode.S2CRoleList -> {
                PbLogin.S2CRoleList resp = PbLogin.S2CRoleList.parseFrom(packet.getData());
                System.out.println(resp);
                if (resp.getPlayersCount() > 0) {
                    PbLogin.RoleListInfo info = resp.getPlayers(0);
                    PbLogin.C2SRoleLogin.Builder builder = PbLogin.C2SRoleLogin.newBuilder();
                    builder.setAccountID(resp.getAccountID());
                    builder.setPlayerID(info.getPlayerID());
                    builder.setSourceLogicID(clientAgent.sourceLogicID);
                    clientAgent.sendLogic(PtCode.C2SRoleLogin, builder.build());
                    LoggerTool.systemLogger.info("已经有角色 playerID={}, roleName={}，请求直接登录", info.getPlayerID(), info.getName());
                } else {
                    PbLogin.C2SRoleCreate.Builder builder = PbLogin.C2SRoleCreate.newBuilder();
                    builder.setAccountID(resp.getAccountID());
                    clientAgent.sendLogic(PtCode.C2SRoleCreate, builder.build());
                    LoggerTool.systemLogger.info("没有角色，请求创建角色");
                }
            }
            case PtCode.S2CRoleCreate -> {
                PbLogin.S2CRoleCreate resp = PbLogin.S2CRoleCreate.parseFrom(packet.getData());
                long accountID = resp.getAccountID();
                long playerID = resp.getPlayerID();
                clientAgent.player = new Player();
                clientAgent.player.playerID = playerID;
                LoggerTool.systemLogger.info("角色创建成功 accountID={}, playerID={}", accountID, playerID);
                clientAgent.getAi().setLoginSuccess(true);
            }
            case PtCode.S2CRoleLogin -> {
                clientAgent.player = new Player();
                PbLogin.S2CRoleLogin resp = PbLogin.S2CRoleLogin.parseFrom(packet.getData());
                long playerID = resp.getPlayerID();
                clientAgent.player.playerID = playerID;
                LoggerTool.systemLogger.info("角色登录成功 playerID={}", playerID);
                clientAgent.getAi().setLoginSuccess(true);
            }
            case PtCode.S2CPreEnterSceneInfo -> {
                PbScene.S2CPreEnterSceneInfo resp = PbScene.S2CPreEnterSceneInfo.parseFrom(packet.getData());
                LoggerTool.systemLogger.info("准备进入场景服 host={}, port={}, sceneWorldIndex={}", resp.getHost(), resp.getPort(), resp.getSceneWorldIndex());
                PreEnterSceneData preEnterSceneData = clientAgent.getPreEnterSceneData();
                if (preEnterSceneData == null) {
                    preEnterSceneData = new PreEnterSceneData();
                    clientAgent.setPreEnterSceneData(preEnterSceneData);
                }
                int oldSceneWorldIndex = preEnterSceneData.getSceneWorldIndex();

                preEnterSceneData.setSceneID(resp.getSceneID());
                preEnterSceneData.setSceneWorldIndex(resp.getSceneWorldIndex());
                preEnterSceneData.setSceneInstanceID(resp.getInstanceID());
                if (clientAgent.sceneConnect != null) {
                    if (clientAgent.sceneConnect.getHost().equals(resp.getHost()) && clientAgent.sceneConnect.getPort() == resp.getPort()) {
                        //同一个连接，并且线程一致直接进入
                        if (oldSceneWorldIndex == preEnterSceneData.getSceneWorldIndex()) {
                            LoggerTool.systemLogger.info("同一个连接，并且线程一致直接进入场景服");
                            playerEnterScene(clientAgent);
                        } else {
                            LoggerTool.systemLogger.info("同一个连接，但是需要切换线程再进入场景服");
                            PbScene.C2SPlayerSwitchSceneWorldIndex.Builder builder = PbScene.C2SPlayerSwitchSceneWorldIndex.newBuilder();
                            builder.setSceneWorldIndex(preEnterSceneData.getSceneWorldIndex());
                            clientAgent.sendScene(PtCode.C2SPlayerSwitchSceneWorldIndex, builder.build());
                        }

                    } else {
                        //不同的连接 先断开旧连接，重新连接
                        LoggerTool.systemLogger.info("不同的连接 先断开旧连接，重新连接场景服");
                        clientAgent.sceneConnect.getChannel().close();
                        clientAgent.sceneConnect = clientAgent.createConnect("scene", resp.getHost(), resp.getPort());
                    }
                } else {
                    //第一次连接
                    LoggerTool.systemLogger.info("第一次连接场景服");
                    clientAgent.sceneConnect = clientAgent.createConnect("scene", resp.getHost(), resp.getPort());
                }
            }
            case PtCode.S2CPlayerSwitchSceneWorldIndex -> {
                LoggerTool.systemLogger.info("场景服切换索引完成 直接进入场景");
                playerEnterScene(clientAgent);
            }
            case PtCode.S2CPlayerEnterScene -> {
                PbScene.S2CPlayerEnterScene resp = PbScene.S2CPlayerEnterScene.parseFrom(packet.getData());
                PbCommon.PbEntity pbEntity = resp.getPlayer();
                Entity playerEntity = new Entity();
                playerEntity.setId(pbEntity.getEntityID());
                playerEntity.setType((byte) pbEntity.getType());
                playerEntity.setCurPos(PbCommonUtils.vector3Pb2Obj(pbEntity.getCurPos()));
                playerEntity.setDirection(PbCommonUtils.vector3Pb2Obj(pbEntity.getDirection()));
                clientAgent.player.playerEntity = playerEntity;
                clientAgent.entityMap.clear();
                LoggerTool.systemLogger.info("实体进入地图成功 result={}, id={}, type={}, pos={}", resp.getResult(), playerEntity.getId(), playerEntity.getType(), playerEntity.getCurPos());
                clientAgent.getAi().setEnterSceneSuccess(true);
            }
            case PtCode.S2CKickOut -> {
                PbLogin.S2CKickOut req = PbLogin.S2CKickOut.parseFrom(packet.getData());
                LoggerTool.systemLogger.warn("玩家被提出，退出客户端 playerID={}", req.getPlayerID());
                SysUtils.exit();
            }
        }
    }

    /**
     * 客户端进入场景请求
     */
    private void playerEnterScene(ClientAgent clientAgent) {
        LoggerTool.systemLogger.info("客户端进入场景请求");
        PbScene.C2SPlayerEnterScene.Builder req = PbScene.C2SPlayerEnterScene.newBuilder();
        PreEnterSceneData preEnterSceneData = clientAgent.getPreEnterSceneData();
        req.setSceneID(preEnterSceneData.getSceneID());
        req.setLogicServerID(preEnterSceneData.getLogicServerID());
        req.setLogicServerWorldIndex(preEnterSceneData.getLogicServerWorldIndex());
        req.setInstanceID(preEnterSceneData.getSceneInstanceID());
        clientAgent.sendScene(PtCode.C2SPlayerEnterScene, req.build());
    }
}
