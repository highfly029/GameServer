package com.highfly029.client.handler;

import com.highfly029.client.data.ClientAgent;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbRole;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;

/**
 * @ClassName RoleHandler
 * @Description RoleHandler
 * @Author liyunpeng
 **/
public class RoleHandler implements PacketHandler {
    @Override
    public void onHandlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception {
        switch (packet.getPtCode()) {
            case PtCode.S2CLoginPushPlayerRole -> {
                PbRole.S2CLoginPushPlayerRole resp = PbRole.S2CLoginPushPlayerRole.parseFrom(packet.getData());
                clientAgent.player.role.exp = resp.getPlayerRoleData().getExp();
                clientAgent.player.role.level = resp.getPlayerRoleData().getLevel();
            }
            case PtCode.S2CAddExp -> {
                PbRole.S2CAddExp resp = PbRole.S2CAddExp.parseFrom(packet.getData());
                clientAgent.player.role.exp = resp.getExp();
            }
            case PtCode.S2CAddLevel -> {
                PbRole.S2CAddLevel resp = PbRole.S2CAddLevel.parseFrom(packet.getData());
                clientAgent.player.role.level = resp.getLevel();
            }
        }
    }
}
