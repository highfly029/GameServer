package com.highfly029.client.handler;

import com.highfly029.client.data.ClientAgent;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbAsset;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;

/**
 * @ClassName AssetHandler
 * @Description AssetHandler
 * @Author liyunpeng
 **/
public class AssetHandler implements PacketHandler {
    @Override
    public void onHandlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception {
        switch (packet.getPtCode()) {
            case PtCode.S2CLoginPushPlayerAllAsset -> {
                PbAsset.S2CLoginPushPlayerAllAsset allAsset = PbAsset.S2CLoginPushPlayerAllAsset.parseFrom(packet.getData());
//                System.out.println("allAsset:" + allAsset);
                if (allAsset.hasPlayerAssetData()) {
                    PbAsset.PbPlayerAssetData pbPlayerAssetData = allAsset.getPlayerAssetData();
                    for (int i = 0; i < pbPlayerAssetData.getCurAssetListCount(); i++) {
                        long curValue = pbPlayerAssetData.getCurAssetList(i);
                        clientAgent.player.asset.curAssets[i] = curValue;
                    }
                    for (int i = 0; i < pbPlayerAssetData.getMaxAssetListCount(); i++) {
                        long maxValue = pbPlayerAssetData.getMaxAssetList(i);
                        clientAgent.player.asset.maxAssets[i] = maxValue;
                    }
                }
            }
            case PtCode.S2CAddMaxAsset -> {
                PbAsset.S2CAddMaxAsset resp = PbAsset.S2CAddMaxAsset.parseFrom(packet.getData());
                int type = resp.getType();
                long addMaxValue = resp.getAddMaxValue();
                clientAgent.player.asset.maxAssets[type] += addMaxValue;
            }
            case PtCode.S2CRemoveMaxAsset -> {
                PbAsset.S2CRemoveMaxAsset resp = PbAsset.S2CRemoveMaxAsset.parseFrom(packet.getData());
                int type = resp.getType();
                long removeMaxValue = resp.getRemoveMaxValue();
                clientAgent.player.asset.maxAssets[type] -= removeMaxValue;
            }
            case PtCode.S2CAddCurAsset -> {
                PbAsset.S2CAddCurAsset resp = PbAsset.S2CAddCurAsset.parseFrom(packet.getData());
                int type = resp.getType();
                long addCurValue = resp.getAddCurValue();
                clientAgent.player.asset.curAssets[type] += addCurValue;
            }
            case PtCode.S2CRemoveCurAsset -> {
                PbAsset.S2CRemoveCurAsset resp = PbAsset.S2CRemoveCurAsset.parseFrom(packet.getData());
                int type = resp.getType();
                long removeCurValue = resp.getRemoveCurValue();
                clientAgent.player.asset.curAssets[type] -= removeCurValue;
            }
        }
    }
}
