package com.highfly029.client.world;

import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.DisruptorBuilder;
import com.highfly029.core.world.SleepingWaitExtendStrategy;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

/**
 * @ClassName ClientStarter
 * @Description 客户端启动器
 * @Author liyunpeng
 **/
public class ClientStarter {
    public static void main(String[] args) {
        ClientGlobalWorld clientGlobalWorld = new ClientGlobalWorld(0);
        WaitStrategy waitStrategy = new SleepingWaitExtendStrategy(clientGlobalWorld);
        Disruptor disruptor = DisruptorBuilder.builder()
                .setPoolName("ClientGlobal")
                .setProducerType(ProducerType.MULTI)
                .setWorldThread(clientGlobalWorld)
                .setWaitStrategy(waitStrategy)
                .build();

        WorldTool worldTool = WorldTool.getInstance();
        worldTool.setDisruptor(disruptor);
        worldTool.setGlobalWorld(clientGlobalWorld);

        worldTool.start();
    }
}
