package com.highfly029.client.world;

import java.util.Map;

import com.highfly029.client.ClientConst;
import com.highfly029.client.ClientGUI;
import com.highfly029.client.ai.SimpleAI;
import com.highfly029.client.data.ClientAgent;
import com.highfly029.client.data.entity.Entity;
import com.highfly029.client.handler.PacketHandler;
import com.highfly029.client.tool.ClientConfigTool;
import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbEntity;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.interfaces.ITimerCallback;
import com.highfly029.core.net.http.HttpConst;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.plugins.timer.Timer;
import com.highfly029.core.protocol.BasePtCode;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.HttpClientTool;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.JsonUtils;
import com.highfly029.utils.ReflectionUtils;
import com.highfly029.utils.SysUtils;
import com.highfly029.utils.collection.ObjObjMap;
import com.highfly029.utils.collection.ObjectList;

import io.netty.channel.Channel;

/**
 * @ClassName ClientGlobalWorld
 * @Description ClientGlobalWorld
 * @Author liyunpeng
 **/
public class ClientGlobalWorld extends GlobalWorld {

    /**
     * 客户端数据
     */
    public final ClientAgent clientAgent;

    private ClientGUI gui;
    /**
     * 协议处理
     */
    private final ObjectList<PacketHandler> packetHandlers = new ObjectList<>(PacketHandler[]::new);

    public ClientGlobalWorld(int childWorldNum) {
        super(true, childWorldNum, 200);
        SimpleAI simpleAI = new SimpleAI();
        clientAgent = new ClientAgent(simpleAI);
        simpleAI.init(clientAgent);
    }

    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {
        if (ClientConst.OPEN_AI) {
            clientAgent.getAi().onLoop();
        }
    }

    @Override
    protected void onMinute() {

    }

    @Override
    protected void loadTemplateConfig(boolean isHotFix) {
        super.loadTemplateConfig(isHotFix);
        TemplateTool.load("csv", ConfigPropUtils.getBoolValue("useBinDataFile"));
        //加载excel数据
        LoggerTool.systemLogger.info("client dataTemplates.size={}", TemplateTool.dataTemplates.size());
    }

    @Override
    protected void clearTemplateConfig() {
        super.clearTemplateConfig();
        //清理excel数据
        TemplateTool.clear();
    }

    @Override
    protected void onHandleConfigData(ObjObjMap<Class, BaseGlobalConfig> configMap) {
        ClientConfigTool.setConfigData(configMap);
    }

    @Override
    protected void onHandlerConfigOver() {
        ClientConfigTool.updateHotfixFlag();
    }


    @Override
    protected void onStartUpSuccess() {
        super.onStartUpSuccess();
        ObjectList<Class> handlerClassList = ReflectionUtils.getAllClassByInterface(PacketHandler.class, PacketHandler.class.getPackage().getName());
        for (int i = 0; i < handlerClassList.size(); i++) {
            PacketHandler packetHandler = null;
            try {
                packetHandler = (PacketHandler) handlerClassList.get(i).getDeclaredConstructor().newInstance();
            } catch (Exception exception) {
                LoggerTool.systemLogger.error("packetHandler", exception);
            }
            packetHandlers.add(packetHandler);
        }

        //是否快速登陆
        boolean quick = false;
        if (!quick) {
            sdkLogin();
        } else {
            //废弃
            initGUI();
            clientAgent.logicConnect = (TcpClient) netTool.clientMaps.get("game1");
            clientAgent.accountID = ConfigPropUtils.getIntValue("accountId");
        }
    }

    /**
     * 初始化GUI
     */
    private void initGUI() {
        gui = new ClientGUI(this);

        Thread gameLoopThread = new Thread("GameLoop") {
            public void run() {
                long lastUpdateNanos = System.nanoTime();
                while (true) {
                    long currentNanos = System.nanoTime();
                    float seconds = (currentNanos - lastUpdateNanos) / 1000000000f;
//                    processEvents();
//                    update(seconds);

                    try {
                        Thread.sleep(30);
                        gui.onFrame();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastUpdateNanos = currentNanos;
                }
            }
        };
        gameLoopThread.setDaemon(false);
        gameLoopThread.start();
    }

    /**
     * sdk注册
     */
    private void sdkRegister() {
        ObjObjMap<String, String> map = new ObjObjMap<>(String[]::new, String[]::new);
        map.put("userName", ConfigPropUtils.getValue("userName"));
        map.put("pwd", ConfigPropUtils.getValue("pwd"));
        String host = ConfigPropUtils.getValue("sdkHost");
        int port = ConfigPropUtils.getIntValue("sdkPort");

        HttpClientTool.get(0, port, host, "/sdk/register?" + HttpUtils.generateParams(map), ((isSuccess, body, errorMsg) -> {
            if (isSuccess) {
                ObjObjMap<String, String> resultMap = JsonUtils.fromJson(body, ObjObjMap.class);
                if (resultMap.containsKey("result") && resultMap.get("result").equals(HttpConst.RESP_SUCCESS)) {
                    long userId = Long.valueOf(resultMap.get("userId"));
                    boolean registered = Boolean.valueOf(resultMap.get("registered"));
                    LoggerTool.systemLogger.info("sdk注册成功 已注册={}, userId={}", registered, userId);

                    //注册成功后再次重新登陆
                    sdkLogin();
                } else {
                    LoggerTool.systemLogger.error("sdk服务器注册失败 退出!");
                    SysUtils.exit();
                }
            } else {
                LoggerTool.systemLogger.error("register无法访问sdk服务器 退出! {}", errorMsg);
                SysUtils.exit();
            }
        }));
    }

    /**
     * sdk登陆
     */
    private void sdkLogin() {
        ObjObjMap<String, String> map = new ObjObjMap<>(String[]::new, String[]::new);
        map.put("userName", ConfigPropUtils.getValue("userName"));
        map.put("pwd", ConfigPropUtils.getValue("pwd"));
        String host = ConfigPropUtils.getValue("sdkHost");
        int port = ConfigPropUtils.getIntValue("sdkPort");
        map.put("gameId", ConfigPropUtils.getValue("gameId"));
        LoggerTool.systemLogger.info("sdk开始登录 userName={}, pwd={}, gameId={}",
                ConfigPropUtils.getValue("userName"),
                ConfigPropUtils.getValue("pwd"),
                ConfigPropUtils.getValue("gameId"));
        HttpClientTool.get(0, port, host, "/sdk/login?" + HttpUtils.generateParams(map), ((isSuccess, body, errorMsg) -> {
            if (isSuccess) {
                ObjObjMap<String, String> resultMap = JsonUtils.fromJson(body, ObjObjMap.class);
                if (resultMap.containsKey("result") && resultMap.get("result").equals(HttpConst.RESP_SUCCESS)) {
                    String userId = resultMap.get("userId");
                    String time = resultMap.get("time");
                    String token = resultMap.get("token");
                    LoggerTool.systemLogger.info("sdk登陆成功 userId={}, time={}, token={}", userId, time, token);
                    //登陆account获取信息
                    accountGetInfo(userId, time, token);
                } else {
                    LoggerTool.systemLogger.error("sdk服务器登陆失败 先尝试注册!");
                    sdkRegister();
                }
            } else {
                LoggerTool.systemLogger.error("login无法访问sdk服务器 退出! {}", errorMsg);
                SysUtils.exit();
            }
        }));
    }

    /**
     * 获取帐号信息
     *
     * @param userId
     * @param time
     * @param token
     */
    private void accountGetInfo(String userId, String time, String token) {
        ObjObjMap<String, String> map = new ObjObjMap<>(String[]::new, String[]::new);
        map.put("gameId", ConfigPropUtils.getValue("gameId"));
        map.put("gameChannel", ConfigPropUtils.getValue("gameChannel"));
        map.put("os", ConfigPropUtils.getValue("os"));
        map.put("userId", userId);
        map.put("time", time);
        map.put("token", token);
        String host = ConfigPropUtils.getValue("accountHost");
        int port = ConfigPropUtils.getIntValue("accountPort");

        HttpClientTool.get(0, port, host, "/account/getAccount?" + HttpUtils.generateParams(map), ((isSuccess, body, errorMsg) -> {
            if (isSuccess) {
                ObjObjMap<String, String> resultMap = JsonUtils.fromJson(body, ObjObjMap.class);
                if (resultMap.containsKey("result") && resultMap.get("result").equals(HttpConst.RESP_SUCCESS)) {
                    String infoStr = resultMap.get("info");

                    ObjObjMap<String, String> infoMap = JsonUtils.fromJson(infoStr, ObjObjMap.class);
                    String accountID = infoMap.get("accountID");
                    String userID = infoMap.get("userID");
                    String channel = infoMap.get("channel");
                    String areaID = infoMap.get("areaID");
                    String sourceLogicID = infoMap.get("sourceLogicID");
                    String country = infoMap.get("country");
                    String loadBalanceHost = infoMap.get("loadBalanceIP");
                    //test
//                    loadBalanceHost = "127.0.0.1";
                    LoggerTool.systemLogger.info("account服务器返回成功 info={}", infoStr);
                    clientAgent.accountID = Long.parseLong(accountID);
                    clientAgent.sourceLogicID = Integer.parseInt(sourceLogicID);
                    getInfoFromLoadBalanceServer(loadBalanceHost, accountID, userID, sourceLogicID);
                } else {
                    LoggerTool.systemLogger.error("account服务器获取信息失败 退出!");
                    SysUtils.exit();
                }
            } else {
                LoggerTool.systemLogger.error("无法访问account服务器 退出! {}", errorMsg);
                SysUtils.exit();
            }
        }));
    }

    /**
     * 从负载均衡服务器获取信息
     *
     * @param host
     */
    private void getInfoFromLoadBalanceServer(String host, String accountID, String userID, String sourceLogicID) {
        HttpClientTool.get(0, host, "/LoadBalance/selectLogicServerInfo?accountID=" + accountID + "&sourceLogicID=" + sourceLogicID, ((isSuccess, body, errorMsg) -> {
            if (isSuccess) {
                ObjObjMap<String, String> resultMap = JsonUtils.fromJson(body, ObjObjMap.class);
                if (resultMap.containsKey("result") && resultMap.get("result").equals(HttpConst.RESP_SUCCESS)) {
                    String clientHost = resultMap.get("host");
                    int clientPort = Integer.valueOf(resultMap.get("port"));
                    LoggerTool.systemLogger.info("loadBalance服务器 登陆成功! host={}, port={}", clientHost, clientPort);

                    if (ClientConst.OPEN_GUI) {
                        initGUI();
                    }

                    connectTcpClient(clientHost, clientPort);
                } else {
                    LoggerTool.systemLogger.error("loadBalance服务器 返回错误! {}", body);
                    SysUtils.exit(1);
                }
            } else {
                LoggerTool.systemLogger.error("无法访问loadBalance服务器 退出! {}", errorMsg);
                SysUtils.exit(1);
            }
        }));
    }

    /**
     * 连接到tcp
     *
     * @param host
     * @param port
     */
    private void connectTcpClient(String host, int port) {
        clientAgent.logicConnect = clientAgent.createConnect("logic", host, port);
    }

    public Entity getPlayerEntity() {
        if (clientAgent != null) {
            return clientAgent.getPlayerEntity();
        }
        return null;
    }

    public Map<Integer, Entity> getEntityMap() {
        return clientAgent.entityMap;
    }

    @Override
    public void onTcpClientEvent(LogicEvent event) {
        super.onTcpClientEvent(event);
        switch (event.getLogicEventAction()) {
            case LogicEventAction.CHANNEL_ACTIVE -> {
                TcpClient tcpClient = (TcpClient) event.getData1();
                Channel channel = (Channel) event.getData2();
                clientAgent.addConnect(channel, tcpClient);
                if (tcpClient.getName().equals("logic")) {
                    ITimerCallback callback = (Timer timer) -> {
                        if (ClientConst.OPEN_GUI && gui == null) {
                            LoggerTool.systemLogger.info("wait for gui");
                        } else {
                            timer.cancel();
                            PbCommon.ConnectTypeReq.Builder req = PbCommon.ConnectTypeReq.newBuilder();
                            req.setType(SessionType.Client);
                            req.setIsReConnect(clientAgent.isReconnect);
                            req.setSocketID(clientAgent.socketID);
                            req.setToken(clientAgent.token);
                            if (clientAgent.isReconnect) {
                                req.setPlayerID(clientAgent.player.playerID);
                                req.setWorldIndex(clientAgent.logicWorldIndex);
                            }
                            req.setClientReceivedIndex(clientAgent.getClientReceivedIndex());
                            req.setPtCodeMd5(PtCode.md5);
                            clientAgent.sendLogic(BasePtCode.CONNECT_TYPE_REQ, req.build());
                            LoggerTool.systemLogger.info("logic连接激活，开始发送第一个请求 clientReceivedIndex={} tcpClient={},md5={}",
                                    clientAgent.getClientReceivedIndex(), clientAgent.logicConnect, PtCode.md5);
                        }
                    };
                    Timer timer = Timer.create().buildDelay(100).buildRepeated(50).buildInterval(100).buildCallback(callback);
                    timerPlugin.addTimer(timer);
                } else {
                    PbCommon.ConnectTypeReq.Builder req = PbCommon.ConnectTypeReq.newBuilder();
                    req.setType(SessionType.Client);
                    req.setPtCodeMd5(PtCode.md5);
                    req.setPlayerID(clientAgent.player.playerID);
                    req.setWorldIndex(clientAgent.getPreEnterSceneData().getSceneWorldIndex());
                    clientAgent.sendScene(BasePtCode.CONNECT_TYPE_REQ, req.build());
                    LoggerTool.systemLogger.info("scene连接激活，开始发送第一个请求 tcpClient={} playerID={}, index={}, md5={}",
                            clientAgent.sceneConnect, req.getPlayerID(), req.getWorldIndex(), PtCode.md5);
                }
            }
            case LogicEventAction.CHANNEL_WRITER_IDLE -> {
                Channel channel = (Channel) event.getData1();
                PbPacket.TcpPacketClient.Builder builder = PbPacket.TcpPacketClient.newBuilder();
                builder.setPtCode(BasePtCode.HEART_REQ);
                channel.writeAndFlush(builder);
            }
            case LogicEventAction.CHANNEL_READ -> {
                try {
                    Channel channel = (Channel) event.getData2();
                    TcpClient tcpClient = clientAgent.getConnect(channel);
                    if (ConfigConst.tcpPacketCombine) {
                        PbPacket.TcpPacketCombine packetList = (PbPacket.TcpPacketCombine) event.getData1();
                        for (int i = 0, len = packetList.getPacketsCount(); i < len; i++) {
                            handlePacket(clientAgent, tcpClient, packetList.getPackets(i));
                        }
                        if (tcpClient.getName().equals("logic")) {
                            if (packetList.getPacketsCount() == 1) {
                                PbPacket.TcpPacket packet = packetList.getPackets(0);
                                int ptCode = packet.getPtCode();
                                if (ptCode > BasePtCode.MAX) {
                                    clientAgent.clientReceivedIndexIncrease();
                                }
                            } else {
                                clientAgent.clientReceivedIndexIncrease();
                            }
                        }
                    } else {
                        PbPacket.TcpPacket tcpPacket = (PbPacket.TcpPacket) event.getData1();
                        handlePacket(clientAgent, tcpClient, tcpPacket);
                        clientAgent.clientReceivedIndexIncrease();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void handlePacket(ClientAgent clientAgent, TcpClient tcpClient, PbPacket.TcpPacket packet) throws Exception {
        for (int i = 0; i < packetHandlers.size(); i++) {
            PacketHandler packetHandler = packetHandlers.get(i);
            packetHandler.onHandlePacket(clientAgent, tcpClient, packet);
        }
        if (ClientConst.OPEN_GUI) {
            if (clientAgent.isGuiNeedRepaint) {
                gui.repaint();
                clientAgent.isGuiNeedRepaint = false;
            }
            if (clientAgent.getRespText() != null) {
                gui.setRespText(clientAgent.getRespText());
                clientAgent.setRespText(null);
            }
        }
    }

    /**
     * 发送指令
     *
     * @param command
     */
    public void sendGmCommand(String command) {
        clientAgent.sendGmCommand(command);
    }

    //释放技能
    public void useSkill(PbEntity.C2SEntityCastSkill.Builder req) {
        clientAgent.sendScene(PtCode.C2SEntityCastSkill, req.build());
    }

    /**
     * 发送移动开始命令
     */
    public void startMove(Vector3 dir) {
        clientAgent.startMove(dir);
    }

    /**
     * 发送移动停止命令
     */
    public void stopMove() {
        clientAgent.stopMove();
    }
}
