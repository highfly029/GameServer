package com.highfly029.client.utils;

import com.highfly029.client.data.entity.Entity;
import com.highfly029.client.data.item.ItemData;
import com.highfly029.client.data.player.EquipmentSlotData;
import com.highfly029.client.tool.ClientConfigTool;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.utils.PbCommonUtils;

/**
 * @ClassName PbClientUtils
 * @Description PbClientUtils
 * @Author liyunpeng
 **/
public class PbClientUtils {
    public static final int FLOAT2INT_SCALE = 1000;
    public static final float INT2FLOAT_SCALE = 1.0f / FLOAT2INT_SCALE;

    public static PbCommon.PbEntity genEntity(Entity entity) {
        PbCommon.PbEntity.Builder builder = PbCommon.PbEntity.newBuilder();
        builder.setEntityID(entity.getId());
        builder.setType(entity.getType());
        builder.setCurPos(PbCommonUtils.vector3Obj2Pb(entity.getCurPos()));
        builder.setDirection(PbCommonUtils.vector3Obj2Pb(entity.getDirection()));

        return builder.build();
    }

    /**
     * 物品数据
     *
     * @param itemData
     * @param index    背包格子索引
     * @return
     */
    public static PbCommon.PbItemData itemObj2Pb(ItemData itemData, int index) {
        PbCommon.PbItemData.Builder builder = PbCommon.PbItemData.newBuilder();
        builder.setItemID(itemData.getItemID());
        builder.setItemNum(itemData.getItemNum());
        builder.setIsBind(itemData.isBind());
        builder.setInvalidTime(itemData.getInvalidTime());
        builder.setIndex(index);

        return builder.build();
    }

    /**
     * 物品数据
     *
     * @param pbItemDataInDb
     * @return
     */
    public static ItemData itemPb2Obj(PbCommon.PbItemData pbItemDataInDb) {
        ItemData itemData = new ItemData();
        itemData.setItemTemplate(ClientConfigTool.getItemConfig().getItemTemplate(pbItemDataInDb.getItemID()));
        itemData.init(pbItemDataInDb.getItemID(), pbItemDataInDb.getItemNum(), pbItemDataInDb.getIsBind());
        itemData.setInvalidTime(pbItemDataInDb.getInvalidTime());
        return itemData;
    }


    /**
     * 装备槽
     *
     * @param equipmentSlotData
     * @return
     */
    public static PbCommon.PbEquipmentSlotData equipmentSlotObj2Pb(EquipmentSlotData equipmentSlotData, int slotIndex) {
        PbCommon.PbEquipmentSlotData.Builder builder = PbCommon.PbEquipmentSlotData.newBuilder();
        builder.setIsOpen(equipmentSlotData.isOpen);
        builder.setSlotIndex(slotIndex);
        if (equipmentSlotData.equipment != null) {
            builder.setItemData(itemObj2Pb(equipmentSlotData.equipment, 0));
        }
        return builder.build();
    }

    public static EquipmentSlotData equipmentSlotPb2Obj(PbCommon.PbEquipmentSlotData pbEquipmentSlotData) {
        EquipmentSlotData equipmentSlotData = new EquipmentSlotData();
        equipmentSlotData.isOpen = pbEquipmentSlotData.getIsOpen();
        if (pbEquipmentSlotData.hasItemData()) {
            equipmentSlotData.equipment = itemPb2Obj(pbEquipmentSlotData.getItemData());
        }
        return equipmentSlotData;
    }
}
