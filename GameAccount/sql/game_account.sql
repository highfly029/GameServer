/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100803
 Source Host           : localhost:3306
 Source Schema         : game_account

 Target Server Type    : MySQL
 Target Server Version : 100803
 File Encoding         : 65001

 Date: 03/11/2022 15:17:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `account_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '唯一主键',
  `user_id` varchar(128) NOT NULL COMMENT '第三方唯一用户id',
  `channel` varchar(16) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `source_logic_id` int(10) unsigned NOT NULL DEFAULT 0,
  `register_time` timestamp NULL DEFAULT NULL,
  `ip` varchar(32) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL COMMENT '国家',
  PRIMARY KEY (`account_id`) USING BTREE,
  UNIQUE KEY `user_id_channel` (`user_id`,`channel`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for area_const
-- ----------------------------
DROP TABLE IF EXISTS `area_const`;
CREATE TABLE `area_const` (
  `area_id` int(11) NOT NULL COMMENT '地区id',
  `area_name` varchar(255) NOT NULL COMMENT '地区名字',
  `contain_country` varchar(255) DEFAULT NULL COMMENT '包含的国家',
  `load_balance_server_ip` varchar(255) NOT NULL COMMENT '该地区对应的loadbalance服务器地址',
  PRIMARY KEY (`area_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

SET FOREIGN_KEY_CHECKS = 1;
