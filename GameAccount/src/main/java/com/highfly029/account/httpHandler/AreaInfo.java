package com.highfly029.account.httpHandler;

/**
 * @ClassName AreaInfo
 * @Description AreaInfo
 * @Author liyunpeng
 **/
public class AreaInfo {
    /**
     * 地区id
     */
    private int areaID;
    /**
     * 地区名字
     */
    private String areaName;
    /**
     * 地区包含的国家
     */
    private String[] countryArray;
    /**
     * 该地区loadBalance地址
     */
    private String loadBalanceIP;

    public int getAreaID() {
        return areaID;
    }

    public void setAreaID(int areaID) {
        this.areaID = areaID;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String[] getCountryArray() {
        return countryArray;
    }

    public void setCountryArray(String[] countryArray) {
        this.countryArray = countryArray;
    }

    public String getLoadBalanceIP() {
        return loadBalanceIP;
    }

    public void setLoadBalanceIP(String loadBalanceIP) {
        this.loadBalanceIP = loadBalanceIP;
    }
}
