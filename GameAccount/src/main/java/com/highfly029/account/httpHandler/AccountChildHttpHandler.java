package com.highfly029.account.httpHandler;

import java.util.function.BiConsumer;

import com.highfly029.account.AccountConst;
import com.highfly029.account.world.AccountChildWorld;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.net.http.BaseChildHttpHandler;
import com.highfly029.core.world.WorldThread;
import com.highfly029.utils.GeoIPUtils;
import com.highfly029.utils.StringUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjObjMap;

import io.vertx.mysqlclient.MySQLClient;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName BaseHttpHandler
 * @Description BaseHttpHandler
 * @Author liyunpeng
 **/
public abstract class AccountChildHttpHandler extends BaseChildHttpHandler {
    /**
     * 地区缓存数据 key=countryName
     */
    private ObjObjMap<String, AreaInfo> areaMapByCountryCache;
    /**
     * 地区缓存数据 key=areaID
     */
    private IntObjectMap<AreaInfo> areaMapByAreaCache;

    protected AccountChildWorld accountChildWorld;

    @Override
    public void init(WorldThread worldThread) {
        this.accountChildWorld = (AccountChildWorld) worldThread;
    }

    /**
     * 查询account
     *
     * @param userID
     * @param channel
     * @param ip
     * @param consumer
     */
    protected void queryAccount(String userID, String channel, String ip, BiConsumer<AccountInfo, String> consumer) {
        int index = accountChildWorld.getIndex();
        Tuple tuple = Tuple.of(userID, channel);
        MySQLUtils.sendChild(AccountConst.getMySQLPool(), "SELECT * FROM account WHERE user_id=? and channel=?", tuple, index, ((isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                int size = successRows.size();
                if (size == 1) {
                    BiConsumer<Boolean, String> loadConsumer = (ret, msg) -> {
                        if (ret) {
                            AccountInfo info = new AccountInfo();
                            Row row = successRows.iterator().next();
                            info.setAccountID(row.getLong("account_id"));
                            info.setUserID(userID);
                            info.setChannel(channel);
                            info.setCountry(row.getString("country"));
                            info.setAreaID(row.getInteger("area_id"));
                            Integer source_logic_id = row.getInteger("source_logic_id");
                            if (source_logic_id != null) {
                                info.setSourceLogicID(source_logic_id);
                            }
                            AreaInfo areaInfo = areaMapByAreaCache.get(info.getAreaID());
                            info.setLoadBalanceIP(areaInfo.getLoadBalanceIP());
                            consumer.accept(info, null);
                        } else {
                            consumer.accept(null, msg);
                        }
                    };
                    loadAccountCache(loadConsumer);

                } else if (size == 0) {
                    insertAccount(userID, channel, ip, consumer);
                } else {
                    consumer.accept(null, "queryAccount invalid size" + size);
                }
            } else {
                consumer.accept(null, errorMsg);
            }
        }));

    }


    /**
     * 插入account
     *
     * @param userID
     * @param channel
     * @param ip
     */
    private void insertAccount(String userID, String channel, String ip, BiConsumer<AccountInfo, String> consumer) {
        BiConsumer<Boolean, String> loadConsumer = (ret, msg) -> {
            if (ret) {
                String country = GeoIPUtils.getCountryCode(ip);
                AreaInfo areaInfo = findAreaInfo(country);
                if (areaInfo == null) {
                    consumer.accept(null, "cant find areaInfo country=" + country);
                    return;
                }
                int index = accountChildWorld.getIndex();
                Tuple tuple = Tuple.of(userID, channel, areaInfo.getAreaID(), ip, country);
                MySQLUtils.sendChild(AccountConst.getMySQLPool(), "INSERT INTO account(user_id,channel,area_id,register_time,ip,country) values(?,?,?,NOW(),?,?);", tuple, index, ((isSuccess, successRows, errorMsg) -> {
                    if (isSuccess) {
                        AccountInfo info = new AccountInfo();
                        long accountID = successRows.property(MySQLClient.LAST_INSERTED_ID);
                        info.setAccountID(accountID);
                        info.setUserID(userID);
                        info.setChannel(channel);
                        info.setCountry(country);
                        info.setAreaID(areaInfo.getAreaID());
                        info.setLoadBalanceIP(areaInfo.getLoadBalanceIP());
                        consumer.accept(info, null);
                    } else {
                        consumer.accept(null, errorMsg);
                    }
                }));
            } else {
                consumer.accept(null, msg);
            }
        };

        loadAccountCache(loadConsumer);
    }

    /**
     * 加载数据库缓存
     *
     * @param consumer
     */
    protected void loadAccountCache(BiConsumer<Boolean, String> consumer) {
        if (isExistCache()) {
            consumer.accept(true, null);
        } else {
            int index = accountChildWorld.getIndex();
            MySQLUtils.sendChild(AccountConst.getMySQLPool(), "SELECT area_id, area_name, contain_country, load_balance_server_ip FROM area_const;", null,
                    index, ((isSuccess, successRows, errorMsg) -> {
                        if (isSuccess) {
                            if (successRows.size() > 0) {

                                IntObjectMap<AreaInfo> map = new IntObjectMap<>(AreaInfo[]::new);
                                for (Row row : successRows) {
                                    int areaID = row.getInteger("area_id");
                                    AreaInfo areaInfo = map.get(areaID);
                                    if (areaInfo == null) {
                                        areaInfo = new AreaInfo();
                                        areaInfo.setAreaID(row.getInteger("area_id"));
                                        areaInfo.setAreaName(row.getString("area_name"));
                                        areaInfo.setLoadBalanceIP(row.getString("load_balance_server_ip"));
                                        String countries = row.getString("contain_country");
                                        areaInfo.setCountryArray(StringUtils.toStringArray(countries, ","));
                                        map.put(areaID, areaInfo);
                                    }

                                }
                                areaMapByCountryCache = new ObjObjMap<>(String[]::new, AreaInfo[]::new);
                                map.foreachImmutable((areaID, areaInfo) -> {
                                    for (String country : areaInfo.getCountryArray()) {
                                        areaMapByCountryCache.put(country, areaInfo);
                                    }

                                });
                                areaMapByAreaCache = map;

                                consumer.accept(true, null);
                            } else {
                                consumer.accept(false, "loadAccountCache size=0");
                            }
                        } else {
                            consumer.accept(false, errorMsg);
                        }
                    }));
        }
    }

    /**
     * 查找地区
     *
     * @param country
     * @return
     */
    private AreaInfo findAreaInfo(String country) {
        return areaMapByCountryCache.get(country);
    }

    /**
     * 清空缓存
     */
    protected void clearCache() {
        areaMapByCountryCache = null;
        areaMapByAreaCache = null;
    }

    /**
     * 是否存在缓存
     *
     * @return
     */
    private boolean isExistCache() {
        return areaMapByCountryCache != null && areaMapByAreaCache != null;
    }


}
