package com.highfly029.account.httpHandler;

import com.highfly029.account.enums.AccountPathEnum;
import com.highfly029.core.net.http.HttpConst;
import com.highfly029.core.utils.HttpUtils;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @ClassName InvalidateCacheHandler
 * @Description InvalidateCacheHandler
 * @Author liyunpeng
 **/
public class InvalidateCacheHandler extends AccountChildHttpHandler {
    @Override
    public String register() {
        return AccountPathEnum.invalidateCache.path();
    }

    @Override
    public void handler(Channel channel, FullHttpRequest request) {
        clearCache();
        HttpUtils.sendHttpResponse(channel, HttpConst.RESP_SUCCESS);
    }
}
