package com.highfly029.account.httpHandler;

import com.highfly029.account.AccountConst;
import com.highfly029.account.enums.AccountPathEnum;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.net.http.HttpConst;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.utils.JsonUtils;
import com.highfly029.utils.collection.ObjObjMap;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName AccountBindLogicHandler
 * @Description 账号绑定逻辑服
 * @Author liyunpeng
 **/
public class AccountBindLogicHandler extends AccountChildHttpHandler {
    @Override
    public String register() {
        return AccountPathEnum.accountBindLogic.path();
    }

    /**
     * demo
     * http://localhost:8082/account/accountBindLogic?accountID=9&logicID=1
     * @param channel
     * @param request
     */
    @Override
    public void handler(Channel channel, FullHttpRequest request) {
        ObjObjMap<String, String> params = HttpUtils.getRequestParams(request);
        String accountID = params.get("accountID");
        String logicID = params.get("logicID");

        int index = accountChildWorld.getIndex();
        Tuple tuple = Tuple.of(logicID, accountID);
        MySQLUtils.sendChild(AccountConst.getMySQLPool(), "UPDATE account SET source_logic_id=? WHERE account_id=? AND source_logic_id=0;", tuple, index, ((isSuccess, successRows, errorMsg) -> {
            ObjObjMap<String, String> result = new ObjObjMap<>(String[]::new, String[]::new);
            if (isSuccess) {
                result.put("result", HttpConst.RESP_SUCCESS);
                result.put("modified", String.valueOf(successRows.rowCount() > 0));
                HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            } else {
                LoggerTool.systemLogger.error("accountBindLogic failed connect mysql. {}", errorMsg);
                result.put("result", HttpConst.RESP_FAIL);
                HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            }
        }));
    }
}
