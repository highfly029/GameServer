package com.highfly029.account.httpHandler;

import java.util.function.BiConsumer;

import com.highfly029.account.enums.AccountPathEnum;
import com.highfly029.core.net.NetUtils;
import com.highfly029.core.net.http.HttpConst;
import com.highfly029.core.tool.HttpClientTool;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.utils.JsonUtils;
import com.highfly029.utils.collection.ObjObjMap;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @ClassName LoginAccountHandler
 * @Description LoginAccountHandler
 * @Author liyunpeng
 **/
public class GetAccountHandler extends AccountChildHttpHandler {
    @Override
    public String register() {
        return AccountPathEnum.getAccount.path();
    }

    /**
     * demo
     * http://localhost:8082/account/getAccount?userId=14001&gameId=1001&time=1624364209390&token=e6d660c67e0d9d598fb0421074797907&gameChannel=mySDK16&os=osx
     *
     * @param channel
     * @param request
     */
    @Override
    public void handler(Channel channel, FullHttpRequest request) {
        ObjObjMap<String, String> params = HttpUtils.getRequestParams(request);
        String userID = params.get("userId");
        String gameId = params.get("gameId");
        String time = params.get("time");
        String token = params.get("token");
        String gameChannel = params.get("gameChannel");
        String os = params.get("os");


        ObjObjMap<String, String> verifyMap = new ObjObjMap<>(String[]::new, String[]::new);
        verifyMap.put("userId", userID);
        verifyMap.put("gameId", gameId);
        verifyMap.put("time", time);
        verifyMap.put("token", token);

        int index = accountChildWorld.getIndex();
        HttpClientTool.get(index, 8081, "localhost", "/sdk/verify?" + HttpUtils.generateParams(verifyMap), ((isSuccess, body, errorMsg) -> {
            ObjObjMap<String, String> result = new ObjObjMap<>(String[]::new, String[]::new);
            if (isSuccess) {
                ObjObjMap<String, String> resultMap = JsonUtils.fromJson(body, ObjObjMap.class);
                if (resultMap.containsKey("result") && resultMap.get("result").equals(HttpConst.RESP_SUCCESS)) {
                    String ip = NetUtils.getIP(channel);
                    BiConsumer<AccountInfo, String> consumer = (info, msg) -> {
                        if (info != null) {
                            result.put("result", HttpConst.RESP_SUCCESS);
                            result.put("info", JsonUtils.toJson(info));
                        } else {
                            result.put("result", HttpConst.RESP_FAIL);
                            LoggerTool.systemLogger.error("queryAccount fail msg={}", msg);
                            result.put("errorMsg", msg);
                        }
                        HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
                    };
                    queryAccount(userID, gameChannel, ip, consumer);

                } else {
                    LoggerTool.systemLogger.error("GetAccountHandler verify failed {}", body);
                    result.put("result", HttpConst.RESP_FAIL);
                    HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
                }
            } else {
                LoggerTool.systemLogger.error("GetAccountHandler failed connect sdk server. {}", errorMsg);
                result.put("result", HttpConst.RESP_FAIL);
                HttpUtils.sendHttpResponse(channel, JsonUtils.toJson(result));
            }
        }));
    }
}
