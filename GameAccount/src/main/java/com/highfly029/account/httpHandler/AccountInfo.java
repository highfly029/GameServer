package com.highfly029.account.httpHandler;

/**
 * @ClassName AccountInfo
 * @Description AccountInfo
 * @Author liyunpeng
 **/
public class AccountInfo {
    /**
     * 帐号
     */
    private long accountID;
    /**
     * 第三方唯一用户
     */
    private String userID;
    /**
     * 渠道
     */
    private String channel;
    /**
     * 地区id
     */
    private int areaID;
    /**
     * 逻辑服源服id
     */
    private int sourceLogicID;
    /**
     * 国家
     */
    private String country;
    /**
     * 该地区loadBalance地址
     */
    private String loadBalanceIP;


    public String getLoadBalanceIP() {
        return loadBalanceIP;
    }

    public void setLoadBalanceIP(String loadBalanceIP) {
        this.loadBalanceIP = loadBalanceIP;
    }

    public long getAccountID() {
        return accountID;
    }

    public void setAccountID(long accountID) {
        this.accountID = accountID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public int getAreaID() {
        return areaID;
    }

    public void setAreaID(int areaID) {
        this.areaID = areaID;
    }

    public int getSourceLogicID() {
        return sourceLogicID;
    }

    public void setSourceLogicID(int sourceLogicID) {
        this.sourceLogicID = sourceLogicID;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "AccountInfo{" +
                "accountID=" + accountID +
                ", userID='" + userID + '\'' +
                ", channel='" + channel + '\'' +
                ", areaID=" + areaID +
                ", sourceLogicID=" + sourceLogicID +
                ", country='" + country + '\'' +
                '}';
    }
}
