package com.highfly029.account.enums;

import com.highfly029.core.interfaces.IHttpPath;

/**
 * @ClassName AccountPathEnum
 * @Description AccountPathEnum
 * @Author liyunpeng
 **/
public enum AccountPathEnum implements IHttpPath {
    /**
     * 获取账号
     */
    getAccount("getAccount"),

    /**
     * 账号绑定逻辑服
     */
    accountBindLogic("accountBindLogic"),

    /**
     * 使缓存无效
     */
    invalidateCache("invalidateCache") {
        @Override
        public boolean isOneChildHandleOnly() {
            return false;
        }

        @Override
        public boolean isAllChildHandle() {
            return true;
        }
    },


    ;


    private final String path;

    public String path() {
        return path;
    }

    AccountPathEnum(String path) {
        this.path = path;
    }

    @Override
    public boolean isOneChildHandleOnly() {
        return true;
    }

    @Override
    public boolean isAllChildHandle() {
        return false;
    }
}
