package com.highfly029.account.world;

import com.highfly029.account.AccountConst;
import com.highfly029.account.enums.AccountPathEnum;
import com.highfly029.core.interfaces.IHttpPath;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.world.GlobalWorld;

/**
 * @ClassName AccountGlobalWorld
 * @Description AccountGlobalWorld
 * @Author liyunpeng
 **/
public class AccountGlobalWorld extends GlobalWorld {

    public AccountGlobalWorld(int childWorldNum) {
        super(true, childWorldNum, 500);
    }

    @Override
    protected void onStartUpSuccess() {
        super.onStartUpSuccess();
        AccountConst.init(this);
    }

    @Override
    protected IHttpPath getHttpPath(String path) {
        AccountPathEnum pathEnum = null;
        try {
            pathEnum = AccountPathEnum.valueOf(path);
        } catch (Exception exception) {
            LoggerTool.httpLogger.error("not exist path", exception);
        }
        return pathEnum;
    }
}
