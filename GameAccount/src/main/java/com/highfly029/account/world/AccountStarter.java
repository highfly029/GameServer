package com.highfly029.account.world;

import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.DisruptorBuilder;
import com.highfly029.core.world.SleepingWaitExtendStrategy;
import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

/**
 * @ClassName AccountStarter
 * @Description AccountStarter
 * @Author liyunpeng
 **/
public class AccountStarter {
    public static void main(String[] args) {
        int childNum = 4;
        AccountGlobalWorld accountGlobalWorld = new AccountGlobalWorld(childNum);
        WaitStrategy waitStrategy = new SleepingWaitExtendStrategy(accountGlobalWorld);
        Disruptor disruptor = DisruptorBuilder.builder()
                .setPoolName("AccountGlobal")
                .setProducerType(ProducerType.MULTI)
                .setWorldThread(accountGlobalWorld)
                .setWaitStrategy(waitStrategy)
                .build();

        WorldTool worldTool = WorldTool.getInstance();
        worldTool.setDisruptor(disruptor);
        worldTool.setGlobalWorld(accountGlobalWorld);

        for (int i = 1; i <= childNum; i++) {
            AccountChildWorld accountChildWorld = new AccountChildWorld(i);
            Disruptor childDisruptor = DisruptorBuilder.builder()
                    .setPoolName("AccountChild")
                    .setProducerType(ProducerType.SINGLE)
                    .setWorldThread(accountChildWorld)
                    .setWaitStrategy(new BlockingWaitStrategy())
                    .setIndex(i)
                    .build();
            worldTool.addChildWorld(i, childDisruptor);
        }
        worldTool.start();
    }
}
