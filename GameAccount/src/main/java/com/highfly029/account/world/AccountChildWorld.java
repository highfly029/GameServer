package com.highfly029.account.world;

import com.highfly029.core.world.ChildWorld;

/**
 * @ClassName AccountChildWorld
 * @Description AccountChildWorld
 * @Author liyunpeng
 **/
public class AccountChildWorld extends ChildWorld {
    public AccountChildWorld(int index) {
        super(index, 100);
    }
}
