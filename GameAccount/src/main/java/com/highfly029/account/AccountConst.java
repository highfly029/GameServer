package com.highfly029.account;

import com.highfly029.core.world.GlobalWorld;

import io.vertx.mysqlclient.MySQLPool;

/**
 * @ClassName AccountConst
 * @Description AccountConst
 * @Author liyunpeng
 **/
public class AccountConst {
    /**
     * mySQLPool数据库连接
     */
    private static MySQLPool mySQLPool;

    public static final String loginAccount = "loginAccount";
    public static final String serverList = "serverList";
    public static final String roleInfo = "roleInfo";

    /**
     * afterOnStartUp后初始化
     */
    public static void init(GlobalWorld globalWorld) {
        mySQLPool = globalWorld.getMysqlTool().getConnection("account_db");
    }

    public static MySQLPool getMySQLPool() {
        return mySQLPool;
    }
}
