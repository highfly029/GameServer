gameProto notice:

文件开头配置
//module=login, startIndex=1, endIndex=100

消息名字前缀
	客户端到场景服
	C2S
	场景服到客户端
	S2C

	场景到中心
	Scene2Center
	中心到场景
	Center2Scene



Short.MAX_VALUE=32767
前100内置协议、不适用
common.proto			10 ~ 100
entity.proto 			100 ~ 500
scene.proto				500 ~ 1000
login.proto 			1000 ~ 1100
bag.proto 				1100 ~ 1200
asset.proto 			1200 ~ 1300
equipment.proto 		1300 ~ 1400
role.proto 				1400 ~ 1500
function.proto 			1500 ~ 1600
quest.proto 			1600 ~ 1700
mail.proto				1700 ~ 1800
auction.proto			1800 ~ 1900
rank.proto				1900 ~ 2000
union.proto 			2000 ~ 3000
activity.proto 			3000 ~ 3100

gm.proto 				32766 ~ 32767


