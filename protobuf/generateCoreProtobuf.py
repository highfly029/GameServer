#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import os
import shutil
import subprocess
import platform

def main():
    parent = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

    cwd = os.getcwd()
    if os.path.exists("com/highfly029"):
        shutil.rmtree("com/highfly029")
    
    pbExe = ""
    if platform.system() == "Windows":
        pbExe = os.path.join(parent, "protobuf/protoc-3.11.4-win64/bin/protoc.exe")
    else :
        pbExe = os.path.join(parent, "protobuf/protoc-3.11.4-osx-x86_64/bin/protoc")
    for file in os.listdir("coreProtobuf"):
        command = pbExe + " --proto_path=coreProtobuf --java_out=" + cwd + " " + file
        print(command)
        ret = subprocess.call(command, shell=True)

    

    GameProtobufDir = os.path.join(parent, "GameCore/src/main/java/com/highfly029/core/packet")

    if os.path.exists(GameProtobufDir):
        shutil.rmtree(GameProtobufDir)

    shutil.copytree("com/highfly029/core/packet", GameProtobufDir)


if __name__ == '__main__':
    main()
    os.system("pause")
