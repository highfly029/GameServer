#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import os
import shutil
import subprocess
import sys

if __name__ == '__main__':
    os.chdir("../GameUtils/")
    ret = os.system("mvn -DskipTests clean install")
    if (ret != 0):
        print("mvn failed")
        sys.exit(-1)

    os.chdir("../GameCore/")
    ret = os.system("mvn -DskipTests clean install")
    if (ret != 0):
        print("mvn failed")
        sys.exit(-1)

    os.chdir("../GameCommon/")
    ret = os.system("mvn -DskipTests clean install")
    if (ret != 0):
        print("mvn failed")
        sys.exit(-1)
