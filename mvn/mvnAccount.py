#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import os
import shutil
import subprocess
import sys

if __name__ == '__main__':
    os.chdir("../GameAccount/")
    ret = os.system("mvn -DskipTests clean install")
    if (ret != 0):
        print("mvn failed")
        sys.exit(-1)

    print("mvn success and then copy zip file")
    if not os.path.exists("../zipFile"):
        os.makedirs("../zipFile")
    shutil.copy("target/GameAccount-1.0.0.zip", "../zipFile/")

    os.system("pause")
