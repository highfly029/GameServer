package com.highfly029;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.instrument.ClassDefinition;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.asm.ClassReader;

/**
 * @ClassName GameReloadAgent
 * @Description 游戏热更新agent
 * @Author liyunpeng
 **/
public class GameHotfixAgent {
    public static void agentmain(String args, Instrumentation inst) {
        System.out.println("agentmain start... time=" + System.currentTimeMillis());
        String classPathDir = args;
        System.out.println("classPathDir=" + classPathDir);
        File classPathFile = new File(classPathDir);
        File[] classPathFileList = classPathFile.listFiles();
        Map<String, byte[]> classMap = new HashMap<>();
        for (File f : classPathFileList) {
            String filePath = f.getPath();
            if (f.isFile() && filePath.endsWith(".class")) {
                System.out.println("classFile=" + f);
                // 读取 class 文件字节码
                try {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(filePath, "r");
                    final byte[] bytes = new byte[(int) f.length()];
                    randomAccessFile.readFully(bytes);
                    String className = readClassName(bytes);
                    System.out.println("classFile name=" + className + " bytes size=" + bytes.length);
                    classMap.put(className, bytes);
                } catch (IOException e) {
                    System.out.println("load classFile fail:" + filePath);
                    e.printStackTrace();
                }
            }
        }
        Class<?>[] allClass = inst.getAllLoadedClasses();
        for (Class<?> c : allClass) {
            if (classMap.containsKey(c.getName())) {
                try {
                    byte[] bytes = classMap.get(c.getName());
                    ClassDefinition classDefinition = new ClassDefinition(c, bytes);
                    inst.redefineClasses(classDefinition);
                    System.out.println("热更新代码成功 " + c.getName());
                } catch (ClassNotFoundException | UnmodifiableClassException e) {
                    System.out.println("热更新代码失败 " + c.getName());
                    e.printStackTrace();
                }
            }
        }
        System.out.println("agentmain over...time=" + System.currentTimeMillis());
    }

    /**
     * 使用 asm 读取类名
     *
     * @param bytes
     * @return
     */
    private static String readClassName(final byte[] bytes) {
        return new ClassReader(bytes).getClassName().replace("/", ".");
    }
}
