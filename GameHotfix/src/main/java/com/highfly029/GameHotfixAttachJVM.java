package com.highfly029;

import java.io.IOException;

import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;
import com.sun.tools.attach.VirtualMachine;

/**
 * @ClassName GameAttachJVM
 * @Description GameAttachJVM
 * @Author liyunpeng
 **/
public class GameHotfixAttachJVM {
    public static void main(String[] args) throws IOException, AttachNotSupportedException, AgentLoadException, AgentInitializationException {
        String pid = args[0];
        String agentPath = args[1];
        String classPathDir = args[2];
        System.out.println("GameAttachJVM pid=" + pid + " agentPath=" + agentPath + " classPathDir=" + classPathDir);
        VirtualMachine vm = VirtualMachine.attach(pid);
        vm.loadAgent(agentPath, classPathDir);
        vm.detach();
    }
}
