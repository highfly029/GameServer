package com.highfly029.scene.utils;

import java.util.List;

import com.highfly029.GameNativeLibrary;

/**
 * @ClassName NavMeshUtils
 * @Description 服务器导航工具
 * @Author liyunpeng
 **/
public class NavMeshUtils {

    public static final byte SOLOMESH_MODE = 1;
    public static final byte TILECACHE_MODE = 2;

    private static final GameNativeLibrary gameNativeLibrary = new GameNativeLibrary();

    /**
     * 加载
     *
     * @param path
     * @return
     */
    public static boolean loadNavMesh(int mode, String name, String path) {
        return gameNativeLibrary.loadNavMesh(mode, name, path);
    }

    /**
     * 设置是否打印底层日志
     *
     * @return
     */
    public static void setPrint(int mode, String name, boolean isPrint) {
        gameNativeLibrary.setPrint(mode, name, isPrint);
    }

    /**
     * 释放
     *
     * @return
     */
    public static void release(int mode, String name) {
        gameNativeLibrary.release(mode, name);
    }

    /**
     * 寻路
     *
     * @param startX
     * @param startY
     * @param startZ
     * @param endX
     * @param endY
     * @param endZ
     * @return 路径点list 查找不到时，会返回null，或list.size==0
     */
    public static List<float[]> findPathStraight(int mode, String name, float startX, float startY, float startZ, float endX, float endY, float endZ) {
        return gameNativeLibrary.findPathStraight(mode, name, -startX, startY, startZ, -endX, endY, endZ);
    }

    /**
     * 碰撞检测
     *
     * @param startX
     * @param startY
     * @param startZ
     * @param endX
     * @param endY
     * @param endZ
     * @return 返回null表示没有发生碰撞, 否则表示碰撞点
     */
    public static float[] rayCast(int mode, String name, float startX, float startY, float startZ, float endX, float endY, float endZ) {
        return gameNativeLibrary.raycast(mode, name, -startX, startY, startZ, -endX, endY, endZ);
    }

    /****************** 动态阻挡*******************/

    public static int addObstacle(int mode, String name, float x, float y, float z, float radius, float height) {
        return gameNativeLibrary.addObstacle(mode, name, x, y, z, radius, height, true);
    }

    public static int addBoxObstacle(int mode, String name, float minX, float minY, float minZ, float maxX, float maxY, float maxZ) {
        return gameNativeLibrary.addBoxObstacle(mode, name, minX, minY, minZ, maxX, maxY, maxZ, true);
    }

    public static int addBoxObstacle(int mode, String name, float centerX, float centerY, float centerZ, float halfExtentsX, float halfExtentsY, float halfExtentsZ, float yRadians) {
        return gameNativeLibrary.addBoxObstacle(mode, name, centerX, centerY, centerZ, halfExtentsX, halfExtentsY, halfExtentsZ, yRadians, true);
    }

    /**
     * 删除一个动态阻挡
     *
     * @param mode
     * @param name
     * @param index
     * @return
     */
    public static boolean removeOneObstacle(int mode, String name, int index) {
        return gameNativeLibrary.removeOneObstacle(mode, name, index, true);
    }

    /**
     * 删除全部动态阻挡
     *
     * @param mode
     * @param name
     */
    public static void removeAllObstacle(int mode, String name) {
        gameNativeLibrary.removeAllObstacle(mode, name, true);
    }

}
