package com.highfly029.scene.utils;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.common.template.entity.EntityTypeConst;
import com.highfly029.scene.fight.entity.MoveEntity;
import com.highfly029.scene.fight.entity.PlayerEntity;
import com.highfly029.scene.fight.scene.Scene;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName BroadCastUtils
 * @Description 广播工具
 * @Author liyunpeng
 **/
public class BroadCastUtils {

    /**
     * 广播给视野内玩家
     *
     * @param moveEntity
     * @param ptCode     协议号
     * @param msg
     * @param self       true包括自己，false不包括自己
     */
    public static void broadCastToAOIPlayers(MoveEntity moveEntity, int ptCode, GeneratedMessageV3 msg, boolean self) {
        IntObjectMap<MoveEntity> allEntity = moveEntity.allMoveEntityInAOI;
        int freeValue = allEntity.getFreeValue();
        int[] keys = allEntity.getKeys();
        MoveEntity[] values = allEntity.getValues();
        int key;
        MoveEntity value;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                if ((value = values[i]) != null) {
                    if (value.isPlayer()) {
                        ((PlayerEntity) value).getEntitySession().send(ptCode, msg);
                    }
                }
            }
        }
        if (self && moveEntity.type == EntityTypeConst.PLAYER) {
            ((PlayerEntity) moveEntity).getEntitySession().send(ptCode, msg);
        }
    }

    /**
     * 广播给场景内所有玩家
     */
    public static void broadCastToScenePlayers(int ptCode, GeneratedMessageV3 msg, Scene scene) {
        IntObjectMap<PlayerEntity> allPlayerEntity = scene.allPlayerEntity;
        int freeValue = allPlayerEntity.getFreeValue();
        int[] keys = allPlayerEntity.getKeys();
        PlayerEntity[] values = allPlayerEntity.getValues();
        int key;
        PlayerEntity value;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                if ((value = values[i]) != null) {
                    value.getEntitySession().send(ptCode, msg);
                }
            }
        }
    }
}
