package com.highfly029.scene.utils;

import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbEntity;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.scene.fight.entity.Entity;
import com.highfly029.scene.fight.entity.MoveEntity;
import com.highfly029.scene.fight.skill.SkillData;

/**
 * @ClassName PbSceneUtils
 * @Description PbSceneUtils
 * @Author liyunpeng
 **/
public class PbSceneUtils {

    //实体全部信息
    public static PbCommon.PbEntity entityObj2Pb(Entity entity) {
        PbCommon.PbEntity.Builder entityBuilder = PbCommon.PbEntity.newBuilder();
        entityBuilder.setEntityID(entity.entityID);
        entityBuilder.setType(entity.type);
        entityBuilder.setCurPos(PbCommonUtils.vector3Obj2Pb(entity.curPos));
        entityBuilder.setDirection(PbCommonUtils.vector3Obj2Pb(entity.direction));

        return entityBuilder.build();
    }

    //技能数据
    public static SkillData skillDataPb2Obj(PbCommon.PbSkillData pbSkillData) {
        SkillData skillData = SkillData.create();
        skillData.skillID = pbSkillData.getSkillID();
        skillData.skillLevel = pbSkillData.getSkillLevel();
        skillData.targetType = (byte) pbSkillData.getSkillTargetType();
        skillData.targetEntityID = pbSkillData.getTargetEntityID();
        if (pbSkillData.hasTargetPos()) {
            skillData.targetPos = PbCommonUtils.vector3Pb2Obj(pbSkillData.getTargetPos());
        }
        if (pbSkillData.hasTargetDir()) {
            skillData.targetDir = PbCommonUtils.vector3Pb2Obj(pbSkillData.getTargetDir());
        }
        return skillData;
    }

    public static PbCommon.PbSkillData skillDataObj2Pb(SkillData skillData) {
        PbCommon.PbSkillData.Builder builder = PbCommon.PbSkillData.newBuilder();
        builder.setSkillID(skillData.skillID);
        builder.setSkillLevel(skillData.skillLevel);
        builder.setSkillTargetType(skillData.targetType);
        builder.setTargetEntityID(skillData.targetEntityID);
        if (skillData.targetPos != null) {
            builder.setTargetPos(PbCommonUtils.vector3Obj2Pb(skillData.targetPos));
        }
        if (skillData.targetDir != null) {
            builder.setTargetDir(PbCommonUtils.vector3Obj2Pb(skillData.targetDir));
        }
        return builder.build();
    }

    //移动信息
    public static PbEntity.S2CEntityMoveInfo obj2pbEntityMoveInfo(MoveEntity entity) {
        PbEntity.S2CEntityMoveInfo.Builder moveInfoBuilder = PbEntity.S2CEntityMoveInfo.newBuilder();
        moveInfoBuilder.setEntityID(entity.entityID);
        moveInfoBuilder.setCurPos(PbCommonUtils.vector3Obj2Pb(entity.curPos));
        moveInfoBuilder.setCurDir(PbCommonUtils.vector3Obj2Pb(entity.direction));
        moveInfoBuilder.setIsMoving(entity.isMoving);
        return moveInfoBuilder.build();
    }
}
