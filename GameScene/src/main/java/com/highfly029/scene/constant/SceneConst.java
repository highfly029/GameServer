package com.highfly029.scene.constant;

import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName SceneConst
 * @Description SceneConst
 * @Author liyunpeng
 **/
public class SceneConst {
    /**
     * 场景服子世界数量
     */
    public static final int SCENE_CHILD_WORLD_NUM = 2;
    /**
     * sceneFeatures
     */
    public static final IntSet sceneFeatures = new IntSet();

    static {
        String value = ConfigPropUtils.getValue("sceneFeatures");
        if (value != null && !value.isEmpty()) {
            String[] array = value.split(",");
            for (String type : array) {
                sceneFeatures.add(Integer.parseInt(type));
            }
        }
    }
}
