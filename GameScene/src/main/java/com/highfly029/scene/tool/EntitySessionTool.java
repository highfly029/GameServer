package com.highfly029.scene.tool;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.core.net.NetUtils;
import com.highfly029.core.net.tcp.ChannelAttribute;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.plugins.counter.IntegerCounter;
import com.highfly029.core.protocol.BasePtCode;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.SessionTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.scene.constant.SceneConst;
import com.highfly029.scene.session.ConnectErrorCode;
import com.highfly029.scene.session.EntitySession;
import com.highfly029.scene.world.Global2SceneEvent;
import com.highfly029.scene.world.SceneGlobalWorld;
import com.highfly029.utils.collection.IntIntMap;

import io.netty.channel.Channel;

/**
 * @ClassName EntitySessionTool
 * @Description EntitySessionTool
 * @Author liyunpeng
 **/
public class EntitySessionTool extends SessionTool {
    private SceneGlobalWorld sceneGlobalWorld;

    /**
     * 线程安全的集合，global线程和child线程共用
     */
    private static final Map<Long, EntitySession> playerEntitySessionMap = new ConcurrentHashMap<>();

    /**
     * 玩家在每个子世界的人数，非线程安全，只能global使用
     */
    private static final IntIntMap entityWorldPlayers = new IntIntMap();

    /**
     * socket计数器 global使用
     */
    private final IntegerCounter socketCounter = new IntegerCounter();

    public EntitySessionTool(GlobalWorld globalWorld) {
        super(globalWorld);
        sceneGlobalWorld = (SceneGlobalWorld) globalWorld;
    }

    public EntitySession createEntitySession() {
        return new EntitySession();
    }

    /**
     * 生成socketID
     *
     * @param session
     */
    public void generateSocketID(EntitySession session) {
        session.setSocketID(socketCounter.getNextValue());
    }

    /**
     * session离线超时
     *
     * @param channel
     * @param idleState
     */
    public void sessionOfflineTimeout(Channel channel, String idleState) {
        EntitySession session = (EntitySession) getSession(channel);
        long playerID = session != null ? session.getPlayerID() : 0;
        SceneLoggerTool.gameLogger.info("sessionOfflineTimeout playerID={} ip={} idleState={}", playerID, NetUtils.getIP(channel), idleState);
    }

    /**
     * session断线
     *
     * @param channel
     */
    public void sessionInactive(Channel channel) {
        SceneLoggerTool.gameLogger.info("sessionInactive ip={}, channel={}", NetUtils.getIP(channel), channel.id().asShortText());
        EntitySession session = (EntitySession) getSession(channel);

        if (session == null) {
            return;
        }
        if (session.getType() == SessionType.Client) {
            long playerID = session.getPlayerID();
            EntitySession entitySession = playerEntitySessionMap.remove(playerID);
            entityWorldPlayers.addValue(entitySession.getWorldIndex(), -1);

            if (entitySession.getPlayerEntity() != null) {
                sceneGlobalWorld.postEvent2Scene(entitySession.getWorldIndex(), Global2SceneEvent.PLAYER_INACTIVE, entitySession.getPlayerEntity(), null);
            } else {
                SceneLoggerTool.gameLogger.error("sessionInactive entitySession is null, ip={}, channel={}", NetUtils.getIP(channel), channel.id().asShortText());
            }
        }
        //立即删除
        removeSession(session);
    }

    /**
     * 处理第一个连接
     *
     * @param session
     * @param packet
     */
    public void handleFirstSceneConnect(EntitySession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbCommon.ConnectTypeReq req = PbCommon.ConnectTypeReq.parseFrom(packet.getData());
        session.setType(req.getType());
        //如果客户端传过来指定索引就用指定的，否则自动找一个人最少的子世界索引
        int worldIndex = req.getWorldIndex() > 0 ? req.getWorldIndex() : getLeastPlayersWorldIndex();
        long playerID = req.getPlayerID();
        SceneLoggerTool.gameLogger.info("handleFirstSceneConnect type={}, worldIndex={}, playerID={}, channel={}",
                session.getType(), worldIndex, playerID, session.getChannel().id().asShortText());

        PbCommon.ConnectTypeResp.Builder resp = PbCommon.ConnectTypeResp.newBuilder();
        resp.setType(SessionType.Scene);

        //测试时可以关闭校验
        if (req.getPtCodeMd5() == null || !req.getPtCodeMd5().equals(PtCode.md5)) {
            resp.setErrorCode(ConnectErrorCode.invalidMd5.ordinal());
            session.send(BasePtCode.CONNECT_TYPE_RESP, resp.build());
            SceneLoggerTool.systemLogger.error("handleFirstSceneConnect invalid md5={}, player update client", req.getPtCodeMd5());
            return;
        }
        //TODO 去logic验证token
        session.setPlayerID(playerID);
        session.getChannel().attr(ChannelAttribute.playerID).set(playerID);

        session.setWorldIndex(worldIndex);
        session.send(BasePtCode.CONNECT_TYPE_RESP, resp.build());

        addSession(session);
        playerEntitySessionMap.put(playerID, session);
        entityWorldPlayers.addValue(worldIndex, 1);
    }

    /**
     * 获取玩家最少的SceneEntityWorldIndex
     *
     * @return
     */
    private int getLeastPlayersWorldIndex() {
        int min = Integer.MAX_VALUE;
        int minIndex = 0;
        for (int i = 0; i < SceneConst.SCENE_CHILD_WORLD_NUM; i++) {
            int index = i * 2 + 1;
            int size = entityWorldPlayers.get(index);
            //如果当前场景子世界没人则直接返回
            if (size == 0) {
                return index;
            }
            if (size < min) {
                min = size;
                minIndex = index;
            }
        }
        return minIndex;
    }

    /**
     * 获取玩家实体单位的网络连接
     *
     * @param playerID
     * @return
     */
    public static EntitySession getEntitySession(long playerID) {
        return playerEntitySessionMap.get(playerID);
    }
}
