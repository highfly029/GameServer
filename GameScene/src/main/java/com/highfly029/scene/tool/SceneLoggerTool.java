package com.highfly029.scene.tool;

import com.highfly029.core.tool.LoggerTool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @ClassName SceneLoggerTool
 * @Description 场景日志
 * @Author liyunpeng
 **/
public class SceneLoggerTool extends LoggerTool {
    /**
     * game日志
     */
    public static final Logger gameLogger = LoggerFactory.getLogger("game");

    /**
     * 战斗日志
     */
    public static final Logger fightLogger = LoggerFactory.getLogger("fight");

    /**
     * 行为日志
     */
    public static final Logger actionLogger = LoggerFactory.getLogger("action");
}
