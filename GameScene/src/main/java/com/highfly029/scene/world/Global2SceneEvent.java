package com.highfly029.scene.world;

/**
 * @ClassName Global2SceneEvent
 * @Description Global2SceneEvent 场景自定义事件
 * @Author liyunpeng
 **/
public enum Global2SceneEvent {
    /**
     * 增加服务器会话
     */
    ADD_SERVER_SESSION,
    /**
     * 删除服务器会话
     */
    REMOVE_SERVER_SESSION,
    /**
     * 玩家断线
     */
    PLAYER_INACTIVE;

    ;
}
