package com.highfly029.scene.world;

import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.ChildWorld;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.scene.ai.CreateAIData;
import com.highfly029.scene.manager.AIManager;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName SceneAIWorld
 * @Description 场景AI子世界
 * @Author liyunpeng
 **/
public class SceneAIWorld extends ChildWorld {
    /**
     * 场景实体世界索引
     */
    private final int sceneEntityWorldIndex;

    private AIManager aiManager;

    public SceneAIWorld(int index) {
        super(index, 100);
        sceneEntityWorldIndex = index - 1;
    }

    @Override
    protected void loadAllUpdate() {
        aiManager = new AIManager(this);
        updateMaps.put(AIManager.class, aiManager);
    }

    @Override
    protected void onAfterLoadAll() {
        //重写父类空实现
    }

    public AIManager getAiManager() {
        return aiManager;
    }

    @Override
    protected void loadAllHttpHandler() throws Exception {

    }

    @Override
    protected void onWorldInnerEvent(LogicEvent logicEvent) {
        switch (logicEvent.getLogicEventAction()) {
            case LogicEventAction.C2C_BASE_EVENT -> {
                Entity2AIEvent event = (Entity2AIEvent) logicEvent.getData1();
                onEntity2AIEvent(event, logicEvent.getData2(), logicEvent.getData3());
            }
            default -> super.onWorldInnerEvent(logicEvent);
        }

    }

    /**
     * 向场景实体世界投递事件
     *
     * @param event
     * @param obj1
     * @param obj2
     */
    public void postEvent2SceneEntity(AI2EntityEvent event, Object obj1, Object obj2) {
        WorldTool.addChildWorldLogicEvent(sceneEntityWorldIndex, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.C2C_BASE_EVENT, event, obj1, obj2);
    }

    /**
     * 场景AI世界接收到场景实体世界的事件
     *
     * @param event
     * @param obj1
     * @param obj2
     */
    private void onEntity2AIEvent(Entity2AIEvent event, Object obj1, Object obj2) {
        switch (event) {
            case AddEntityAI -> {
                CreateAIData createAIData = (CreateAIData) obj1;
                aiManager.addEntityAI(createAIData);
            }
            case RemoveEntityAI -> {
                long key = (long) obj1;
                aiManager.removeEntityAI(key);
            }
            case OtherEntityEnterAOI -> {
                long key = (long) obj1;
                IntSet entityIDSet = (IntSet) obj2;
                aiManager.otherEntityEnterAOI(key, entityIDSet);
            }
            case OtherEntityLeaveAOI -> {
                long key = (long) obj1;
                IntSet entityIDSet = (IntSet) obj2;
                aiManager.otherEntityLeaveAOI(key, entityIDSet);
            }
            case EntityPosChange -> {
                long key = (long) obj1;
                long value = (long) obj2;
                aiManager.entityChangePos(key, value);
            }
        }
    }
}
