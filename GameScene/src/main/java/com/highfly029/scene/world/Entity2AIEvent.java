package com.highfly029.scene.world;

/**
 * @ClassName Entity2AIEvent
 * @Description Entity2AIEvent
 * @Author liyunpeng
 **/
public enum Entity2AIEvent {
    /**
     * 增加entityAI
     */
    AddEntityAI,

    /**
     * 移除entityAI
     */
    RemoveEntityAI,

    /**
     * 其他实体进入自己的aoi
     */
    OtherEntityEnterAOI,

    /**
     * 其他实体离开自己的aoi
     */
    OtherEntityLeaveAOI,

    /**
     * 实体位置发生变化
     */
    EntityPosChange,

    ;
}
