package com.highfly029.scene.world;

import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.DisruptorBuilder;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.core.world.SleepingWaitExtendStrategy;
import com.highfly029.scene.constant.SceneConst;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

/**
 * @ClassName SceneStarter
 * @Description 场景启动器
 * @Author liyunpeng
 **/
public class SceneStarter {

    public static void main(String[] args) {
        int index = 0;
        SceneGlobalWorld sceneGlobalWorld = new SceneGlobalWorld(SceneConst.SCENE_CHILD_WORLD_NUM);
        WaitStrategy waitStrategy = new SleepingWaitExtendStrategy(sceneGlobalWorld);
        Disruptor<LogicEvent> disruptor = DisruptorBuilder.builder()
                .setPoolName("SceneGlobal")
                .setProducerType(ProducerType.MULTI)
                .setWorldThread(sceneGlobalWorld)
                .setWaitStrategy(waitStrategy)
                .build();

        WorldTool worldTool = WorldTool.getInstance();
        worldTool.setDisruptor(disruptor);
        worldTool.setGlobalWorld(sceneGlobalWorld);

        for (int i = 1; i <= SceneConst.SCENE_CHILD_WORLD_NUM; i++) {
            index++;
            worldTool.addChildWorld(index, createSceneChildDisruptor(index));

            index++;
            worldTool.addChildWorld(index, createSceneAIDisruptor(index));
        }
        worldTool.start();
    }

    private static Disruptor<LogicEvent> createSceneChildDisruptor(int index) {
        SceneEntityWorld sceneEntityWorld = new SceneEntityWorld(index);
        WaitStrategy childWait = new SleepingWaitExtendStrategy(sceneEntityWorld);
        return DisruptorBuilder.builder()
                .setPoolName("SceneEntity")
                .setProducerType(ProducerType.MULTI)
                .setWorldThread(sceneEntityWorld)
                .setWaitStrategy(childWait)
                .setIndex(index)
                .build();
    }

    private static Disruptor<LogicEvent> createSceneAIDisruptor(int index) {
        SceneAIWorld sceneAIWorld = new SceneAIWorld(index);
        WaitStrategy childWait = new SleepingWaitExtendStrategy(sceneAIWorld);
        return DisruptorBuilder.builder()
                .setPoolName("SceneAI")
                .setProducerType(ProducerType.SINGLE)
                .setWorldThread(sceneAIWorld)
                .setWaitStrategy(childWait)
                .setIndex(index)
                .build();
    }
}
