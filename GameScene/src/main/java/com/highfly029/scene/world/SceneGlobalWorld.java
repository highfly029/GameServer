package com.highfly029.scene.world;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.common.data.feature.LoadBalanceFeature;
import com.highfly029.common.data.feature.SceneFeature;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.common.template.global.GlobalConst;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.interfaces.IServerGlobalProtocolHandler;
import com.highfly029.core.net.NetUtils;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.protocol.BasePtCode;
import com.highfly029.core.session.Session;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.scene.constant.SceneConst;
import com.highfly029.scene.session.EntitySession;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.EntitySessionTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.SysUtils;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.ObjObjMap;

import io.netty.channel.Channel;

/**
 * @ClassName SceneGlobalWorld
 * @Description 场景全局世界
 * @Author liyunpeng
 **/
public class SceneGlobalWorld extends GlobalWorld {

    public SceneGlobalWorld(int childWorldNum) {
        super(true, childWorldNum, 100);
    }

    @Override
    protected void loadTemplateConfig(boolean isHotFix) {
        super.loadTemplateConfig(isHotFix);
        TemplateTool.load("csv", ConfigPropUtils.getBoolValue("useBinDataFile"));
        //加载excel数据
        LoggerTool.systemLogger.info("scene dataTemplates.size={}", TemplateTool.dataTemplates.size());
    }

    @Override
    protected void clearTemplateConfig() {
        super.clearTemplateConfig();
        //清理excel数据
        TemplateTool.clear();
    }

    @Override
    protected void onHandleConfigData(ObjObjMap<Class, BaseGlobalConfig> configMap) {
        ConfigTool.setConfigData(configMap);
    }

    @Override
    protected void onHandleConstConfigData() {
        ConfigTool.setConstConfigData(TemplateTool.constDataMap);
    }

    @Override
    protected void onHandlerConfigOver() {
        ConfigTool.updateHotfixFlag();
    }

    @Override
    protected void onBeforeLoadAll() {
        super.onBeforeLoadAll();
        sessionTool = new EntitySessionTool(this);
    }

    @Override
    protected void registerAllPacketParser() throws Exception {
        registerPacketParser("com.highfly029.parser.pb");
        registerPacketParser("com.highfly029.parser.custom");
    }

    @Override
    protected void loadAllUpdate() {

    }

    @Override
    public void onTcpClientEvent(LogicEvent event) {
        byte action = event.getLogicEventAction();
        if (action == LogicEventAction.CHANNEL_READ) {
            Channel channel = (Channel) event.getData2();
            PbPacket.TcpPacket packet = (PbPacket.TcpPacket) event.getData1();
            tcpClientRead(channel, packet);
        } else {
            super.onTcpClientEvent(event);
            switch (action) {
                case LogicEventAction.CHANNEL_READER_IDLE, LogicEventAction.CHANNEL_WRITER_IDLE, LogicEventAction.CHANNEL_ALL_IDLE -> {
                    Channel channel = (Channel) event.getData1();
                    channel.writeAndFlush(serverHeartReq);
                }
                case LogicEventAction.CHANNEL_ACTIVE -> {
                    TcpClient tcpClient = (TcpClient) event.getData1();
                    Channel channel = (Channel) event.getData2();
                    PbCommon.ConnectTypeReq.Builder req = PbCommon.ConnectTypeReq.newBuilder();
                    req.setType(SessionType.Scene);
                    req.setIdentifier(ConfigConst.identifier);
                    req.setIsReConnect(tcpClient.isReconnect());
                    PbPacket.TcpPacket tcpPacket = createTcpPacket(BasePtCode.CONNECT_TYPE_REQ, req.build().toByteString());
                    channel.writeAndFlush(tcpPacket);
                    LoggerTool.systemLogger.info("scene 发送初始协议 {}", tcpClient);
                    tcpClient.setReconnect(true);
                }
                case LogicEventAction.CHANNEL_INACTIVE -> {
                    TcpClient tcpClient = (TcpClient) event.getData1();
                    Channel channel = (Channel) event.getData2();
                    Session session = sessionTool.getSession(channel);
                    if (session != null) {
                        //remove 会释放session 所以需要提前把type和list取出来投递给子世界
                        int type = session.getType();
                        int identifier = session.getIdentifier();
                        long index = MathUtils.getCompositeIndex(type, identifier);
                        IntList features = session.getFeatures();
                        sessionTool.removeSession(session);
                        postEvent2AllSceneEntityWorld(Global2SceneEvent.REMOVE_SERVER_SESSION, index, features);
                        SceneLoggerTool.gameLogger.info("channel inactive disconnect:{}", tcpClient);
                    } else {
                        SceneLoggerTool.gameLogger.error("channel inactive disconnect session is null {}", tcpClient);
                    }
                }
            }
        }
    }

    /**
     * tcp读
     *
     * @param channel
     * @param packet
     */
    private void tcpClientRead(Channel channel, PbPacket.TcpPacket packet) {
        int ptCode = packet.getPtCode();
        int type = 0;
        int identifier = 0;
        try {
            if (ptCode == BasePtCode.HEART_RESP) {
//                LoggerTool.systemLogger.info("收到心跳包");
            } else if (ptCode != BasePtCode.CONNECT_TYPE_RESP) {
                Session session = sessionTool.getSession(channel);
                type = session.getType();
                identifier = session.getIdentifier();
                IServerGlobalProtocolHandler handler = serverGlobalHandlerMaps.get(ptCode);
                handler.onServerProtocolHandler(session, ptCode, packet);
            } else {
                PbCommon.ConnectTypeResp resp = PbCommon.ConnectTypeResp.parseFrom(packet.getData());
                type = resp.getType();
                identifier = resp.getIdentifier();
                LoggerTool.systemLogger.info("scene connect success type={}, id={}, channel={}", type, identifier, channel.id().asShortText());

                Session session = sessionTool.getSession(type, identifier);
                if (session != null) {
                    //重复的连接 直接断开
                    LoggerTool.systemLogger.error("scene connect duplicate type={}, id={}, channel={}", type, identifier, channel.id().asShortText());
                    channel.close();
                    return;
                }
                if (type != SessionType.Center && type != SessionType.LoadBalance) {
                    LoggerTool.systemLogger.error("scene connect invalid type={}", type);
                    return;
                }

                session = sessionTool.create();
                session.init(type, identifier, channel);
                for (int feature : resp.getFeaturesList()) {
                    session.addFeature(feature);
                }
                sessionTool.addSession(session);

                postEvent2AllSceneEntityWorld(Global2SceneEvent.ADD_SERVER_SESSION, session, null);
            }
        } catch (Exception e) {
            SceneLoggerTool.gameLogger.error("tcpClientRead type=" + type + " id=" + identifier + " ptCode=" + ptCode + " channelShort=" + channel.id().asShortText(), e);
        }
    }

    @Override
    public void onTcpServerEvent(LogicEvent event) {
        byte action = event.getLogicEventAction();
        if (action == LogicEventAction.CHANNEL_READ) {
            Channel channel = (Channel) event.getData2();
            PbPacket.TcpPacketClient packet = (PbPacket.TcpPacketClient) event.getData1();
            tcpServerRead(channel, packet);
        } else {
            switch (action) {
                case LogicEventAction.CHANNEL_ALL_IDLE -> {
                    //服务器检测超时之后应该立刻断开连接、而不是延迟断开连接等待重连
                    Channel channel = (Channel) event.getData1();
                    String idleState = (String) event.getData2();
                    ((EntitySessionTool) sessionTool).sessionOfflineTimeout(channel, idleState);
                }
                case LogicEventAction.CHANNEL_INACTIVE -> {
                    Channel channel = (Channel) event.getData1();
                    ((EntitySessionTool) sessionTool).sessionInactive(channel);
                }
                default -> super.onTcpServerEvent(event);
            }
        }
    }

    /**
     * tcp读
     *
     * @param channel
     * @param packet
     */
    private void tcpServerRead(Channel channel, PbPacket.TcpPacketClient packet) {
        int ptCode = packet.getPtCode();
        try {
            long packetSequenceNum = packet.getSequenceNum();
            EntitySession session = (EntitySession) sessionTool.getSession(channel);
            switch (ptCode) {
                case BasePtCode.HEART_REQ -> {
                    //heart 如果需要心跳返回附带时间戳等信息，就使用这个
                    if (session != null) {
                        session.send(BasePtCode.HEART_RESP);
                    } else {
                        channel.close();
                        SceneLoggerTool.gameLogger.error("session is null, protocol={}, ip={}", ptCode, NetUtils.getIP(channel));
                    }
                }
                case BasePtCode.CONNECT_TYPE_REQ -> {
                    if (session != null && session.getChannel() != null) {
                        channel.close();
                        SceneLoggerTool.gameLogger.error("duplicate connect type req and close channel, protocol={}, ip={}", ptCode, NetUtils.getIP(channel));
                        return;
                    }
                    EntitySessionTool entitySessionTool = (EntitySessionTool) sessionTool;
                    session = entitySessionTool.createEntitySession();
                    //先生成socketID再使用
                    entitySessionTool.generateSocketID(session);
                    session.init(SessionType.Client, session.getSocketID(), channel);
                    entitySessionTool.handleFirstSceneConnect(session, packet);
                    session.setSequenceNum(packetSequenceNum);
                }
                case PtCode.C2SPlayerSwitchSceneWorldIndex -> {
                    //在主世界切换索引,前提是玩家目前状态是退出场景后，进入场景前
                    PbScene.C2SPlayerSwitchSceneWorldIndex req = PbScene.C2SPlayerSwitchSceneWorldIndex.parseFrom(packet.getData());
                    int oldSceneWorldIndex = session.getWorldIndex();
                    int newSceneWorldIndex = req.getSceneWorldIndex();
                    session.setWorldIndex(newSceneWorldIndex);
                    PbScene.S2CPlayerSwitchSceneWorldIndex.Builder builder = PbScene.S2CPlayerSwitchSceneWorldIndex.newBuilder();
                    builder.setSceneWorldIndex(newSceneWorldIndex);
                    session.send(PtCode.S2CPlayerSwitchSceneWorldIndex, builder.build());
                    SceneLoggerTool.systemLogger.info("PlayerSwitchSceneWorldIndex old={},new={}", oldSceneWorldIndex, newSceneWorldIndex);
                }
                default -> {
                    if (session != null && session.getChannel() != null) {
                        if (session.getSequenceNum() >= packetSequenceNum) {
                            channel.close();
                            SceneLoggerTool.gameLogger.error("ip={}, invalid sequenceNum={} localNum={} and close channel", NetUtils.getIP(channel), packetSequenceNum, session.getSequenceNum());
                        } else {
                            session.setSequenceNum(packetSequenceNum);
                            WorldTool.addChildWorldLogicEvent(session.getWorldIndex(), LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_CLIENT_PACKET, ptCode, session, packet);
                        }
                    } else {
                        channel.close();
                        SceneLoggerTool.gameLogger.error("session is null, protocol={}, ip={}", ptCode, NetUtils.getIP(channel));
                    }
                }
            }
        } catch (Exception e) {
            SceneLoggerTool.gameLogger.error("onTcpServerEvent ptCode=" + ptCode + " channelShort=" + channel.id().asShortText(), e);
        }
    }

    /**
     * 向场景子世界投递事件
     *
     * @param worldIndex
     * @param event
     * @param obj1
     * @param obj2
     */
    public void postEvent2Scene(int worldIndex, Global2SceneEvent event, Object obj1, Object obj2) {
        WorldTool.addChildWorldLogicEvent(worldIndex, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_BASE_EVENT, event, obj1, obj2);
    }

    /**
     * 向所有场景子世界投递事件
     *
     * @param event
     * @param obj1
     * @param obj2
     */
    public void postEvent2AllSceneEntityWorld(Global2SceneEvent event, Object obj1, Object obj2) {
        for (int i = 0; i < SceneConst.SCENE_CHILD_WORLD_NUM; i++) {
            int worldIndex = i * 2 + 1;
            WorldTool.addChildWorldLogicEvent(worldIndex, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_BASE_EVENT, event, obj1, obj2);
        }
    }

    @Override
    protected void onWorldInnerEvent(LogicEvent logicEvent) {
        super.onWorldInnerEvent(logicEvent);
        byte action = logicEvent.getLogicEventAction();
        switch (action) {
            case LogicEventAction.G2C_BASE_EVENT -> {
                Logic2GlobalEvent event = (Logic2GlobalEvent) logicEvent.getData1();
                onLogic2GlobalEvent(event, logicEvent.getData2(), logicEvent.getData3());
            }
        }
    }

    private void onLogic2GlobalEvent(Logic2GlobalEvent event, Object data1, Object data2) {
        switch (event) {
            case REMOVE_PLAYER_SESSION -> {
                EntitySession playerSession = (EntitySession) data1;
                sessionTool.removeSession(playerSession);
            }
            case ADD_PLAYER -> {
                int index = (int) data1;
                long playerID = (long) data2;
//                playerGlobalManager.addPlayer(index, playerID);
            }
            case REMOVE_PLAYER -> {
                int index = (int) data1;
                long playerID = (long) data2;
//                playerGlobalManager.removePlayer(index, playerID);
            }
            case GM_COMMAND -> {
                String[] stringCmd = (String[]) data1;
                int[] intCmd = (int[]) data2;
//                doGlobalGM(stringCmd, intCmd);
            }
        }
    }

    @Override
    public void onTick(int escapedMillTime) {
        super.onTick(escapedMillTime);
    }

    @Override
    protected void onSecond() {
        super.onSecond();
        //打印协议统计数据
//        StatisticsEnum.MSG_LENGTH.getPlugin().dump();
    }

    @Override
    protected void onStartUpSuccess() {
        super.onStartUpSuccess();
        pushServerInfo2LoadBalance();
    }

    /**
     * 推送服务器消息到负载均衡服
     */
    private void pushServerInfo2LoadBalance() {
        PbLoadBalance.S2LPushServerInfo.Builder builder = PbLoadBalance.S2LPushServerInfo.newBuilder();
        if (SceneConst.sceneFeatures.contains(SceneFeature.NORMAL_SCENE)) {
            if (SceneConst.SCENE_CHILD_WORLD_NUM > GlobalConst.MaxLineNumEveryScene) {
                SceneLoggerTool.gameLogger.error("必须保证每个场景子世界至少有一个分线场景组");
            }
        }
        String clientHost = ConfigPropUtils.getValue("clientHost");
        int clientPort = ConfigPropUtils.getIntValue("clientPort");

        builder.setIdentifier(ConfigConst.identifier);
        builder.setMaxMemory(SysUtils.getMaxMemory());
        builder.setTotalMemory(SysUtils.getTotalMemory());
        builder.setFreeMemory(SysUtils.getFreeMemory());
        builder.setClientHost(clientHost);
        builder.setClientPort(clientPort);
        if (SceneConst.sceneFeatures.isEmpty()) {
            SceneLoggerTool.gameLogger.error("sceneFeatures is null");
        }
        for (int i = 0; i < SceneConst.SCENE_CHILD_WORLD_NUM; i++) {
            int worldIndex = i * 2 + 1;
            builder.addWorldIndex(worldIndex);
        }
        builder.setType(SessionType.Scene);
        SceneConst.sceneFeatures.foreachImmutable(builder::addSceneFeatures);
        sendMsg2AllFeatureServer(SessionType.LoadBalance, LoadBalanceFeature.SCENE, PtCode.S2LPushServerInfo, builder.build());
        SceneLoggerTool.gameLogger.info("pushServerInfo2LoadBalance id={}", ConfigConst.identifier);
    }

    public void sendMessage2Logic(int ptCode, GeneratedMessageV3 message) {

    }
}
