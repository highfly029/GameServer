package com.highfly029.scene.world;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.common.data.feature.LoadBalanceFeature;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.ChildWorld;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.scene.clientProtocol.ISceneProtocolHandler;
import com.highfly029.scene.clientProtocol.ISceneProtocolRegister;
import com.highfly029.scene.fight.entity.PlayerEntity;
import com.highfly029.scene.manager.SceneManager;
import com.highfly029.scene.serverProtocol.broker.ISceneBrokerHandler;
import com.highfly029.scene.session.EntitySession;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.ReflectionUtils;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName SceneWorld
 * @Description 场景实体世界
 * @Author liyunpeng
 **/
public class SceneEntityWorld extends ChildWorld {
    /**
     * 场景AI世界索引
     */
    private int sceneAIWorldIndex;

    /**
     * 场景消息处理器集合
     */
    private final IntObjectMap<ISceneProtocolHandler> protocolHandlers = new IntObjectMap<>(ISceneProtocolHandler[]::new);

    /**
     * 服务器之间中转消息处理
     */
    private final IntObjectMap<ISceneBrokerHandler> sceneBrokerHandlers = new IntObjectMap<>(ISceneBrokerHandler[]::new);

    /**
     * 服务器之间的连接
     * SessionType -> identifier -> session
     */
    private LongObjectMap<Session> serverSessionMap = new LongObjectMap<>(Session[]::new);
    /**
     * 如果只有一个负载均衡场景分配服务器
     */
    private static final boolean singleLoadBalanceScene = true;
    private Session loadBalanceSceneSession;

    private SceneManager sceneManager;

    public SceneEntityWorld(int index) {
        super(index, 30);
        sceneAIWorldIndex = index + 1;
    }


    @Override
    protected void onAfterLoadAll() throws Exception {
        super.onAfterLoadAll();
        registerAllSceneProtocol();
        registerSceneBrokerHandler();
    }

    /**
     * 注册所有的场景协议
     */
    private void registerAllSceneProtocol() throws Exception {
        String packageName = this.getClass().getPackageName();
        packageName = packageName.substring(0, packageName.lastIndexOf('.'));
        ObjectList<Class> clsModules = ReflectionUtils.getAllClassByInterface(ISceneProtocolRegister.class, packageName);
        Class[] values = clsModules.getValues();
        Class cls;
        for (int i = 0, size = clsModules.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                ISceneProtocolRegister register = (ISceneProtocolRegister) cls.getDeclaredConstructor().newInstance();
                register.registerSceneProtocol(this);
            }
        }
    }


    /**
     * 服务器之间中转消息处理
     *
     * @throws Exception
     */
    private void registerSceneBrokerHandler() throws Exception {
        String packageName = this.getClass().getPackageName();
        packageName = packageName.substring(0, packageName.lastIndexOf('.'));
        ObjectList<Class> clsModules = ReflectionUtils.getAllClassByInterface(ISceneBrokerHandler.class, packageName);
        Class[] values = clsModules.getValues();
        Class cls;
        for (int i = 0, size = clsModules.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                ISceneBrokerHandler sceneBrokerHandler = (ISceneBrokerHandler) cls.getDeclaredConstructor().newInstance();
                int ptCode = sceneBrokerHandler.registerPtCode();
                sceneBrokerHandlers.put(ptCode, sceneBrokerHandler);
            }
        }
    }

    /**
     * 注册客户端协议
     *
     * @param ptCode
     * @param handler
     * @throws Exception
     */
    public void registerSceneProtocol(int ptCode, ISceneProtocolHandler handler) throws Exception {
        if (protocolHandlers.containsKey(ptCode)) {
            throw new Exception("scene registerProtocol duplicate ptCode:" + ptCode);
        } else {
            protocolHandlers.put(ptCode, handler);
        }
    }

    @Override
    protected void loadAllUpdate() {
        sceneManager = new SceneManager(this);
        updateMaps.put(SceneManager.class, sceneManager);
    }

    public SceneManager getSceneManager() {
        return sceneManager;
    }

    public IntObjectMap<ISceneBrokerHandler> getSceneBrokerHandlers() {
        return sceneBrokerHandlers;
    }

    @Override
    protected void onWorldInnerEvent(LogicEvent logicEvent) {
        switch (logicEvent.getLogicEventAction()) {
            case LogicEventAction.G2C_CLIENT_PACKET -> {
                int ptCode = (int) logicEvent.getData1();
                EntitySession session = (EntitySession) logicEvent.getData2();
                PbPacket.TcpPacketClient packet = (PbPacket.TcpPacketClient) logicEvent.getData3();
                long playerID = session.getPlayerID();
                ISceneProtocolHandler handler = protocolHandlers.get(ptCode);
                try {
                    handler.onClientProtocolHandler(session, packet);
                } catch (Exception e) {
                    SceneLoggerTool.gameLogger.error("SceneEntityWorld Index={} handlerClientError: playerID={}, ptCode={}", getIndex(), playerID, ptCode);
                    SceneLoggerTool.gameLogger.error("onChildWorldEvent", e);
                }
            }
            case LogicEventAction.G2C_BASE_EVENT -> {
                Global2SceneEvent event = (Global2SceneEvent) logicEvent.getData1();
                onGlobal2SceneEvent(event, logicEvent.getData2(), logicEvent.getData3());
            }
            case LogicEventAction.C2C_BASE_EVENT -> {
                AI2EntityEvent event = (AI2EntityEvent) logicEvent.getData1();
                onAI2EntityEvent(event, logicEvent.getData2(), logicEvent.getData3());
            }
            default -> super.onWorldInnerEvent(logicEvent);
        }
    }

    protected void onGlobal2SceneEvent(Global2SceneEvent event, Object data1, Object data2) {
        switch (event) {
            case ADD_SERVER_SESSION -> {
                Session session = (Session) data1;
                addServerSession(session);
            }
            case REMOVE_SERVER_SESSION -> {
                long index = (long) data1;
                IntList features = (IntList) data2;
                int type = MathUtils.getCompositeArg1(index);
                int identifier = MathUtils.getCompositeArg2(index);
                removeServerSession(type, identifier, features);
            }
            case PLAYER_INACTIVE -> {
                PlayerEntity playerEntity = (PlayerEntity) data1;
                playerInactive(playerEntity);
            }
        }
    }

    /**
     * 玩家断线
     * @param playerEntity
     */
    private void playerInactive(PlayerEntity playerEntity) {
        //下帧移除断线的玩家
        playerEntity.isRemoveNextTick = true;
    }

    /**
     * 增加服务器会话
     *
     * @param session
     */
    private void addServerSession(Session session) {
        long index = MathUtils.getCompositeIndex(session.getType(), session.getIdentifier());
        serverSessionMap.put(index, session);
        if (session.getType() == SessionType.LoadBalance) {
            if (singleLoadBalanceScene && session.getFeatures().contains(LoadBalanceFeature.SCENE)) {
                loadBalanceSceneSession = session;
            }
        }
    }

    /**
     * 删除服务器会话
     *  @param type
     * @param identifier
     * @param features
     */
    private void removeServerSession(int type, int identifier, IntList features) {
        long index = MathUtils.getCompositeIndex(type, identifier);
        serverSessionMap.remove(index);
        if (type == SessionType.LoadBalance) {
            if (singleLoadBalanceScene && features.contains(LoadBalanceFeature.SCENE)) {
                loadBalanceSceneSession = null;
            }
        }
    }

    /**
     * 获取指定session和功能的所有连接
     * @param sessionType
     * @param feature
     * @return
     */
    private ObjectList<Session> getAllSession(int sessionType, int feature) {
        ObjectList<Session> list = new ObjectList<>(Session[]::new);
        serverSessionMap.foreachImmutable((k, v) -> {
            if (v.getType() == sessionType && v.getFeatures().contains(feature)) {
                list.add(v);
            }
        });
        return list;
    }

    /**
     * 获取指定session和功能的随机连接
     * @param sessionType
     * @param feature
     * @return
     */
    private Session getRandomSession(int sessionType, int feature) {
        ObjectList<Session> list = getAllSession(sessionType, feature);
        return RandomUtils.random(list);
    }

    /**
     * 发送消息到所有负载均衡服务器
     *
     * @param ptCode
     * @param messageV3
     */
    public void sendMsg2AllLoadBalanceScene(int ptCode, GeneratedMessageV3 messageV3) {
        if (singleLoadBalanceScene) {
            loadBalanceSceneSession.sendMsgS2S(ptCode, messageV3);
        } else {
            ObjectList<Session> list = getAllSession(SessionType.LoadBalance, LoadBalanceFeature.SCENE);
            if (list == null || list.isEmpty()) {
                SceneLoggerTool.gameLogger.error("sendMsg2AllLoadBalanceScene empty", new Exception());
                return;
            }
            for (int i = 0; i < list.size(); i++) {
                list.get(i).sendMsgS2S(ptCode, messageV3);
            }
        }
    }

    /**
     * 发送消息到一个随机的负载均衡场景
     *
     * @param ptCode
     * @param messageV3
     */
    public void sendMsg2RandomLoadBalanceScene(int ptCode, GeneratedMessageV3 messageV3) {
        if (singleLoadBalanceScene) {
            loadBalanceSceneSession.sendMsgS2S(ptCode, messageV3);
        } else {
            Session session = getRandomSession(SessionType.LoadBalance, LoadBalanceFeature.SCENE);
            if (session == null) {
                SceneLoggerTool.gameLogger.error("sendMsg2RandomLoadBalanceScene session is null", new Exception());
                return;
            }
            session.sendMsgS2S(ptCode, messageV3);
        }
    }

    /**
     * 发送消息协议到逻辑服
     *
     * @param ptCode
     * @param messageV3
     */
    public void sendMsg2Logic(int to, int toIndex, long playerID, int ptCode, GeneratedMessageV3 messageV3) {
        PbLoadBalance.Scene2LogicMessage.Builder builder = PbLoadBalance.Scene2LogicMessage.newBuilder();
        builder.setFrom(ConfigConst.identifier);
        builder.setFromIndex(getIndex());
        builder.setTo(to);
        builder.setToIndex(toIndex);
        builder.setPlayerID(playerID);
        builder.setPtCode(ptCode);
        builder.setData(messageV3.toByteString());
        Session session = getRandomSession(SessionType.LoadBalance, LoadBalanceFeature.BROKER);
        if (session == null) {
            SceneLoggerTool.gameLogger.error("sendMsg2Logic session is null", new Exception());
            return;
        }
        session.sendMsgS2S(PtCode.Scene2LogicMessage, builder.build());
    }

    /**
     * 向场景实体世界投递事件
     *
     * @param event
     * @param obj1
     * @param obj2
     */
    public void postEvent2SceneAI(Entity2AIEvent event, Object obj1, Object obj2) {
        WorldTool.addChildWorldLogicEvent(sceneAIWorldIndex, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.C2C_BASE_EVENT, event, obj1, obj2);
    }
//
    /**
     * 场景实体世界接收到场景AI世界的事件
     *
     * @param event
     * @param obj1
     * @param obj2
     */
    private void onAI2EntityEvent(AI2EntityEvent event, Object obj1, Object obj2) {

    }
}
