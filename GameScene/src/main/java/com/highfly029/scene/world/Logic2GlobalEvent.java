package com.highfly029.scene.world;

/**
 * @ClassName Logic2GlobalEvent
 * @Description Logic2GlobalEvent 场景自定义事件
 * @Author liyunpeng
 **/
public enum Logic2GlobalEvent {
    /**
     * 删除玩家session
     */
    REMOVE_PLAYER_SESSION,
    /**
     * 增加玩家
     */
    ADD_PLAYER,
    /**
     * 删除玩家
     */
    REMOVE_PLAYER,

    /**
     * gm命令
     */
    GM_COMMAND,


    ;
}
