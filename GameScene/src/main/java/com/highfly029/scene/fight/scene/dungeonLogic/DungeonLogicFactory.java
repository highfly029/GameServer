package com.highfly029.scene.fight.scene.dungeonLogic;

import com.highfly029.common.template.dungeon.DungeonLogicTypeConst;

/**
 * @ClassName DungeonLogicFactory
 * @Description 副本逻辑创建工厂
 * @Author liyunpeng
 **/
public class DungeonLogicFactory {

    /**
     * 创建副本逻辑
     *
     * @param dungeonLogicType 副本逻辑类型
     * @return
     */
    public static DungeonLogic createDungeonLogic(int dungeonLogicType) {
        DungeonLogic dungeonLogic = null;
        switch (dungeonLogicType) {
            case DungeonLogicTypeConst.Test1 -> dungeonLogic = new Dungeon1();
            case DungeonLogicTypeConst.Test2 -> dungeonLogic = new Dungeon2();
        }
        return dungeonLogic;
    }
}
