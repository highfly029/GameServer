package com.highfly029.scene.fight.entity;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbEntity;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.common.template.state.StateConst;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.scene.config.SceneConfig;
import com.highfly029.scene.fight.buff.BuffData;
import com.highfly029.scene.fight.scene.Scene;
import com.highfly029.scene.session.EntitySession;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.utils.BroadCastUtils;
import com.highfly029.scene.utils.NavMeshUtils;
import com.highfly029.scene.utils.PbSceneUtils;
import com.highfly029.scene.world.SceneEntityWorld;
import com.highfly029.utils.collection.IntBooleanMap;
import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectSet;

/**
 * @ClassName PlayerEntity
 * @Description 玩家实体
 * @Author liyunpeng
 **/
public class PlayerEntity extends CharacterEntity {
    public long playerID;

    /**
     * 玩家实体对应的连接
     */
    private EntitySession entitySession;

    //技能扩展效果 skillID->extendSkillEffect->value
    private final IntObjectMap<IntIntMap> skillExtendEffectMap = new IntObjectMap<>(IntIntMap[]::new);
    //技能通用扩展效果
    private final IntIntMap skillCommonExtendEffectMap = new IntIntMap();

    public PlayerEntity(SceneEntityWorld sceneEntityWorld) {
        super(sceneEntityWorld);
    }

    public void setEntitySession(EntitySession entitySession) {
        this.entitySession = entitySession;
    }

    public EntitySession getEntitySession() {
        return entitySession;
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    @Override
    public boolean isBelongToPlayer() {
        return true;
    }

    /**
     * 获取主人工会ID
     *
     * @return
     */
    public int getMasterUnionID() {
        return 0;
    }

    /**
     * 获取主人队伍ID
     *
     * @return
     */
    public int getMasterTeamID() {
        return 0;
    }

    /**
     * 切换场景
     *
     * @param targetSceneID
     * @return
     */
    public boolean swithScene(int targetSceneID) {

        return true;
    }

    /**
     * 客户端发起移动
     *
     * @param packet
     */
    public void moveStartByClient(PbPacket.TcpPacketClient packet) {
        PbEntity.C2SEntityMoveStart req = null;
        try {
            req = PbEntity.C2SEntityMoveStart.parseFrom(packet.getData());
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        int entityID = req.getEntityID();
        Vector3 pos = PbCommonUtils.vector3Pb2Obj(req.getCurPos());
        Vector3 dir = PbCommonUtils.vector3Pb2Obj(req.getCurDir());
//            SceneLoggerTool.moduleLogger.info("moveStart info entityID={}, pos={}, dir={}", entityID, pos, dir);

        //TODO 检测pos 和dir 是否 在一定误差之内
        setDirection(dir);
        moveStart();
        PbEntity.S2CEntityMoveStart.Builder resp = PbEntity.S2CEntityMoveStart.newBuilder();
        resp.setResult(true);
        resp.setCurPos(PbCommonUtils.vector3Obj2Pb(curPos));
        entitySession.send(PtCode.S2CEntityMoveStart, resp.build());
    }

    /**
     * 按照方向移动
     */
    public void moveStart() {
        this.isMoving = true;
        //补偿20ms到50ms位置
    }

    /**
     * 客户端发起停止移动
     *
     * @param packet
     */
    public void moveStopByClient(PbPacket.TcpPacketClient packet) {
        moveStop();
        PbEntity.S2CEntityMoveStop.Builder resp = PbEntity.S2CEntityMoveStop.newBuilder();
        resp.setResult(true);
        entitySession.send(PtCode.S2CEntityMoveStop, resp.build());
    }

    public void moveStop() {
        //补偿20ms到50ms位置
        this.isMoving = false;
    }

    /**
     * 是否可以复活
     *
     * @return
     */
    public boolean isCanReborn() {
        return scene.isCanReborn(this);
    }

    /**
     * 扣除复活消耗
     */
    public void doRebornCost() {
        scene.doRebornCost(this);
    }

    @Override
    public int getSkillExtendEffect(int skillEffectType) {
        if (fightAllLogic.skillFightLogic.isSkillNow()) {
            IntIntMap map = skillExtendEffectMap.get(fightAllLogic.skillFightLogic.getCurrentSkillID());
            int extendValue = 0;
            if (map != null) {
                extendValue = map.get(skillEffectType);
            }
            return extendValue + skillCommonExtendEffectMap.get(skillEffectType);
        }
        return 0;
    }

    /**
     * 增加技能扩展效果
     *
     * @param skillID
     * @param skillEffectType
     * @param value
     */
    public void addSkillExtendEffect(int skillID, int skillEffectType, int value) {
        IntIntMap map = skillExtendEffectMap.get(skillID);
        if (map == null) {
            map = new IntIntMap();
            skillExtendEffectMap.put(skillID, map);
        }
        int nowValue = map.addValue(skillEffectType, value);
        SceneLoggerTool.actionLogger.info("addSkillExtendEffect skillID={},type={},value={}", skillID, skillEffectType, value);
    }

    /**
     * 增加通用技能扩展效果
     *
     * @param skillEffectType
     * @param value
     */
    public void addSkillCommonExtendEffect(int skillEffectType, int value) {
        int nowValue = skillCommonExtendEffectMap.addValue(skillEffectType, value);
        SceneLoggerTool.actionLogger.info("addSkillCommonExtendEffect type={},value={}", skillEffectType, value);
    }

    /**
     * 原地复活
     */
    public void rebornCurrentPos() {
        fightAllLogic.attributeFightLogic.fullHp();
        fightAllLogic.attributeFightLogic.fullMp();
        fightAllLogic.stateFightLogic.setState(StateConst.Death, false);
        onReborn();
    }


    @Override
    public void onTick(int escapedMillTime) {
        if (isMoving) {
//            通过高度图，可以得到完整坐标从(x0, y0, z0)点移到(x1, y1, z1)点
//            射线检查(x0, y0, z0+H) - (x1, y1, z1+H)间有无障碍物。
//
//            H值可以硬编码，射线检测要高点，不然沿着地面在做射线检测
//
//            检查 (x0, y0, z0) 、(x1, y1, z1)2点的z值高度差
//            只有无障碍物、z值高度差在一定范围: [-a, b]内，则可行走

            //step raycast检测 y + H
            //step curPos 和 targetPos 的高度差是否在一定范围之内
            //step 如果都通过则setPos, 否则广播停止移动

            Vector3 targetPos = this.nextFramePos(escapedMillTime);
            int height = 1;//玩家射线检测高度，硬编码
            float[] hitPoint = NavMeshUtils.rayCast(scene.navMeshMode, scene.navMeshName, curPos.x, curPos.y + height, curPos.z,
                    targetPos.x, targetPos.y + height, targetPos.z);
            if (hitPoint == null) {
                if (this.moveToPos(targetPos)) {
                    //tmp
                    PbEntity.S2CEntityMoveInfo moveInfoPush = PbSceneUtils.obj2pbEntityMoveInfo(this);
                    BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntityMoveInfo, moveInfoPush, true);
                }
            } else {
                //有阻挡，停止移动
                this.isMoving = false;
                PbEntity.S2CEntityMoveInfo moveInfoPush = PbSceneUtils.obj2pbEntityMoveInfo(this);
                BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntityMoveInfo, moveInfoPush, true);
            }
        }
        super.onTick(escapedMillTime);
        //发送tick后的数据
        entitySession.flush();
    }

    @Override
    public void onOtherEntityLeaveSelfAOI(ObjectSet<MoveEntity> leaveSelfAOISets) {
        PbEntity.S2CLeaveSelfAOIList.Builder builder = PbEntity.S2CLeaveSelfAOIList.newBuilder();
        Entity[] keys = leaveSelfAOISets.getKeys();
        Entity entity;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((entity = keys[i]) != null) {
                builder.addLeaveList(entity.entityID);
            }
        }
        entitySession.send(PtCode.S2CLeaveSelfAOIList, builder.build());
    }

    @Override
    public void onOtherEntityEnterSelfAOI(ObjectSet<MoveEntity> enterSelfAOISets) {
        PbEntity.S2CEnterSelfAOIList.Builder builder = PbEntity.S2CEnterSelfAOIList.newBuilder();
        Entity[] keys = enterSelfAOISets.getKeys();
        Entity entity;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((entity = keys[i]) != null) {
                builder.addEnterList(PbSceneUtils.entityObj2Pb(entity));
            }
        }
        entitySession.send(PtCode.S2CEnterSelfAOIList, builder.build());
    }

    @Override
    public void onUseSkillFail(int skillID, String reason) {
        PbEntity.S2CEntityCastSkillFail.Builder resp = PbEntity.S2CEntityCastSkillFail.newBuilder();
        resp.setEntityID(entityID);
        resp.setSkillID(skillID);
        entitySession.send(PtCode.S2CEntityCastSkillFail, resp.build());
    }

    @Override
    public void onStatePushSelf(IntBooleanMap sendSelfMap) {
        super.onStatePushSelf(sendSelfMap);
        PbEntity.S2CEntityState.Builder builder = getEntityStatePushBuilder(sendSelfMap);
        entitySession.send(PtCode.S2CEntityState, builder.build());
    }

    @Override
    public void onAttributePushSelf(IntIntMap sendSelfMap) {
        PbEntity.S2CEntityAttribute.Builder builder = getEntityAttributePushBuilder(sendSelfMap);
        entitySession.send(PtCode.S2CEntityAttribute, builder.build());
    }

    @Override
    public void onAddBuffPushSelf(BuffData buffData) {
        super.onAddBuffPushSelf(buffData);
        PbEntity.S2CEntityAddBuff.Builder builder = PbEntity.S2CEntityAddBuff.newBuilder();
        builder.setEntityID(entityID);
        builder.setBuffData(getEntityBuffPushBuilder(buffData));
        entitySession.send(PtCode.S2CEntityAddBuff, builder.build());
    }

    @Override
    public void onRemoveBuffPushSelf(int instanceID) {
        super.onRemoveBuffPushSelf(instanceID);
        PbEntity.S2CEntityRemoveBuff.Builder builder = PbEntity.S2CEntityRemoveBuff.newBuilder();
        builder.setEntityID(entityID);
        builder.setInstanceID(instanceID);
        entitySession.send(PtCode.S2CEntityRemoveBuff, builder.build());
    }

    @Override
    public void onRefreshBuffPushSelf(BuffData buffData) {
        super.onRefreshBuffPushSelf(buffData);
        PbEntity.S2CEntityRefreshBuff.Builder builder = PbEntity.S2CEntityRefreshBuff.newBuilder();
        builder.setEntityID(entityID);
        builder.setBuffData(getEntityBuffPushBuilder(buffData));
        entitySession.send(PtCode.S2CEntityRefreshBuff, builder.build());
    }

    @Override
    public void onRefreshBuffUseCntPushSelf(BuffData buffData) {
        super.onRefreshBuffUseCntPushSelf(buffData);
        PbEntity.S2CEntityRefreshBuffUseCnt.Builder builder = PbEntity.S2CEntityRefreshBuffUseCnt.newBuilder();
        builder.setEntityID(entityID);
        builder.setInstanceID(buffData.instanceID);
        builder.setUseCnt(buffData.useCnt);
        entitySession.send(PtCode.S2CEntityRefreshBuffUseCnt, builder.build());
    }

    @Override
    public void onRefreshBuffFloorNumPushSelf(BuffData buffData) {
        super.onRefreshBuffFloorNumPushSelf(buffData);
        PbEntity.S2CEntityRefreshBuffFloorNum.Builder builder = PbEntity.S2CEntityRefreshBuffFloorNum.newBuilder();
        builder.setEntityID(entityID);
        builder.setInstanceID(buffData.instanceID);
        builder.setFloorNum(buffData.floorNum);
        entitySession.send(PtCode.S2CEntityRefreshBuffFloorNum, builder.build());
    }

    @Override
    public void onBeginCoolDownPushSelf(byte coolDownType, int id, int realDuration) {
        PbEntity.S2CBeginCoolDown.Builder builder = PbEntity.S2CBeginCoolDown.newBuilder();
        builder.setCoolDownType(coolDownType);
        builder.setId(id);
        builder.setRealDuration(realDuration);
        entitySession.send(PtCode.S2CBeginCoolDown, builder.build());
    }

    @Override
    public void onRemoveCoolDownPushSelf(byte coolDownType, int id) {
        PbEntity.S2CRemoveCoolDown.Builder builder = PbEntity.S2CRemoveCoolDown.newBuilder();
        builder.setCoolDownType(coolDownType);
        builder.setId(id);
        entitySession.send(PtCode.S2CRemoveCoolDown, builder.build());
    }

    @Override
    public void onCoolDownGroupFixedTimeRefreshPushSelf(int groupID, int nowValue) {
        super.onCoolDownGroupFixedTimeRefreshPushSelf(groupID, nowValue);
        PbEntity.S2CCoolDownGroupFixedTimeRefresh.Builder builder = PbEntity.S2CCoolDownGroupFixedTimeRefresh.newBuilder();
        builder.setGroupID(groupID);
        builder.setNowValue(nowValue);
        entitySession.send(PtCode.S2CCoolDownGroupFixedTimeRefresh, builder.build());
    }

    @Override
    public void onCoolDownGroupPercentTimeRefreshPushSelf(int groupID, int nowPercentValue) {
        super.onCoolDownGroupPercentTimeRefreshPushSelf(groupID, nowPercentValue);
        PbEntity.S2CCoolDownGroupPercentTimeRefresh.Builder builder = PbEntity.S2CCoolDownGroupPercentTimeRefresh.newBuilder();
        builder.setGroupID(groupID);
        builder.setNowPercentValue(nowPercentValue);
        entitySession.send(PtCode.S2CCoolDownGroupPercentTimeRefresh, builder.build());
    }

    @Override
    public void onCoolDownTypeFixedTimeRefreshPushSelf(int coolDownType, int nowValue) {
        super.onCoolDownTypeFixedTimeRefreshPushSelf(coolDownType, nowValue);
        PbEntity.S2CCoolDownTypeFixedTimeRefresh.Builder builder = PbEntity.S2CCoolDownTypeFixedTimeRefresh.newBuilder();
        builder.setCoolDownType(coolDownType);
        builder.setNowValue(nowValue);
        entitySession.send(PtCode.S2CCoolDownTypeFixedTimeRefresh, builder.build());
    }

    @Override
    public void onCoolDownTypePercentTimeRefreshPushSelf(int coolDownType, int nowPercentValue) {
        super.onCoolDownTypePercentTimeRefreshPushSelf(coolDownType, nowPercentValue);
        PbEntity.S2CCoolDownTypePercentTimeRefresh.Builder builder = PbEntity.S2CCoolDownTypePercentTimeRefresh.newBuilder();
        builder.setCoolDownType(coolDownType);
        builder.setNowPercentValue(nowPercentValue);
        entitySession.send(PtCode.S2CCoolDownTypePercentTimeRefresh, builder.build());
    }

    @Override
    public void onPosChangePushSelf() {
        PbEntity.S2CSetEntityPos.Builder builder = PbEntity.S2CSetEntityPos.newBuilder();
        builder.setEntityID(entityID);
        builder.setCurPos(PbCommonUtils.vector3Obj2Pb(curPos));
        entitySession.send(PtCode.S2CSetEntityPos, builder.build());
    }

    @Override
    public void onAttributeChange(int num, short[] changeAttributeIDArray, int[] lastAttributeValueArray) {
        super.onAttributeChange(num, changeAttributeIDArray, lastAttributeValueArray);
//        SceneLoggerTool.gameLogger.info("onAttributeChange num={}, changeAttributeIDArray={}, lastAttributeValueArray={}", num, changeAttributeIDArray, lastAttributeValueArray);
//        SceneLoggerTool.gameLogger.info("onAttributeChange");
//        int entityID, oldValue, newValue;
//        for (int i = 0, len = changeAttributeIDArray.length; i < len; i++) {
//            if ((entityID = changeAttributeIDArray[i]) != 0) {
//                oldValue = lastAttributeValueArray[i];
//                newValue = getAttribute((short) entityID);
//                SceneLoggerTool.gameLogger.info("entityID={},oldValue={},newValue={}", entityID, oldValue, newValue);
//            }
//        }
    }

    @Override
    public void onDamage(int realDamageValue, CharacterEntity damageSource) {
        SceneLoggerTool.gameLogger.info("onDamage value={}, sourceID={}, curHp={}", realDamageValue, damageSource.entityID, fightAllLogic.attributeFightLogic.getHp());
    }

    @Override
    public void onDeath(CharacterEntity attackSource) {
        SceneLoggerTool.gameLogger.info("onDeath");
    }

    /**
     * 准备进入场景
     *
     * @param scene
     */
    public void prepareEnterScene(Scene scene) {
        SceneLoggerTool.gameLogger.info("准备进入场景 playerID={},entityID={}, sceneID={}, lineID={}, instanceID={}",
                playerID, entityID, scene.sceneID, scene.lineID, scene.instanceID);
    }

    @Override
    public void onEnterScene(Scene scene) {
        SceneLoggerTool.gameLogger.info("进入场景成功 playerID={},entityID={}, sceneID={}, lineID={}, instanceID={}",
                playerID, entityID, scene.sceneID, scene.lineID, scene.instanceID);
        super.onEnterScene(scene);
//        player.onEnterSceneSuccess(scene);
        refreshSceneInfo2LoadBalance(scene, true);
    }

    @Override
    public void onLeaveScene(Scene scene) {
        super.onLeaveScene(scene);
        SceneLoggerTool.gameLogger.info("离开场景成功 playerID={},entityID={}, sceneID={}, lineID={},instanceID={}",
                playerID, entityID, scene.sceneID, scene.lineID, scene.instanceID);

        refreshSceneInfo2LoadBalance(scene, false);
    }

    /**
     *
     * @param scene
     * @param isAdd
     */
    private void refreshSceneInfo2LoadBalance(Scene scene, boolean isAdd) {
        PbScene.S2LRefreshSceneInfo.Builder builder = PbScene.S2LRefreshSceneInfo.newBuilder();
        builder.setSceneServerID(ConfigConst.identifier);
        builder.setSceneWorldIndex(sceneEntityWorld.getIndex());
        builder.setSceneStrategy(SceneConfig.getSceneStrategy(scene.sceneID));
        builder.setKey(scene.getKey());
        builder.setIsAdd(isAdd);
        builder.setPlayerID(playerID);
        sceneEntityWorld.sendMsg2AllLoadBalanceScene(PtCode.S2LRefreshSceneInfo, builder.build());
    }
}
