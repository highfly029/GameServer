package com.highfly029.scene.fight.scene.sceneImp;

import com.highfly029.common.template.scene.SceneTypeConst;
import com.highfly029.scene.fight.scene.Scene;

/**
 * @ClassName SceneFactory
 * @Description 场景创建工厂
 * @Author liyunpeng
 **/
public class SceneFactory {

    public static Scene createScene(int sceneType) {
        Scene scene = null;
        switch (sceneType) {
            case SceneTypeConst.CityScene -> scene = new CityScene();
            case SceneTypeConst.FieldScene -> scene = new FieldScene();
            case SceneTypeConst.HomeScene -> scene = new HomeScene();
            case SceneTypeConst.UnionScene -> scene = new UnionScene();
            case SceneTypeConst.DungeonScene -> scene = new DungeonScene();
        }
        return scene;
    }
}
