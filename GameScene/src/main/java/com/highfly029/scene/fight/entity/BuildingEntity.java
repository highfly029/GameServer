package com.highfly029.scene.fight.entity;

import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName BuildingEntity
 * @Description 建筑实体
 * @Author liyunpeng
 **/
public class BuildingEntity extends FixedPosEntity {
    public BuildingEntity(SceneEntityWorld sceneEntityWorld) {
        super(sceneEntityWorld);
    }
}
