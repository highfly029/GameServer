package com.highfly029.scene.fight.entity;

import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName PetEntity
 * @Description 宠物实体
 * @Author liyunpeng
 **/
public class PetEntity extends CharacterEntity {
    public PetEntity(SceneEntityWorld sceneEntityWorld) {
        super(sceneEntityWorld);
    }
}
