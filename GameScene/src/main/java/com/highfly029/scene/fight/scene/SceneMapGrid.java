package com.highfly029.scene.fight.scene;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.scene.exception.SceneException;
import com.highfly029.scene.fight.entity.Entity;
import com.highfly029.scene.fight.entity.FixedPosEntity;
import com.highfly029.scene.fight.entity.MoveEntity;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.IntSet;


/**
 * @ClassName SceneMapInfo
 * @Description 场景地图相关信息
 * @Author liyunpeng
 **/
public class SceneMapGrid {

    //九宫格数据
    //单个格子长度
    public static final int GRID_LENGTH = 50;
    //单个格子宽度
    public static final int GRID_WIDTH = 50;
    //全部横向格子数
    private int gridSizeInLength;
    //全部纵向格子数
    private int gridSizeInWidth;
    //地图全部格子数据
    private int gridSize;
    //地图格子数据
    private GridInfo[] gridInfos;
    //非法格子id
    public static final int INVALID_GRID_ID = -1;
    //格子扫描半径 1就是九宫格 2是25宫格
    //九宫格会减少格子变化、节省cpu
    //25宫格会减少冗余单位、节省带宽
    public static final int SCAN_RADIUS = 2;

    /*************** cache *****************/
    private final IntList tmpGridList1 = new IntList();
    private final IntList tmpGridList2 = new IntList();
    private final IntList tmpGridList3 = new IntList();

    /*************** cache *****************/


    public static SceneMapGrid create() {
        return new SceneMapGrid();
    }

    public void init(int[] mapSize) {
        //地图长度
        int mapLength = mapSize[0];
        //地图宽度
        int mapWidth = mapSize[1];

        this.gridSizeInLength = mapLength / GRID_LENGTH;
        if (mapLength % GRID_LENGTH > 0) {
            this.gridSizeInLength++;
        }
        this.gridSizeInWidth = mapWidth / GRID_WIDTH;
        if (mapWidth % GRID_WIDTH > 0) {
            this.gridSizeInWidth++;
        }
        this.gridSize = this.gridSizeInLength * this.gridSizeInWidth;
        //只有gridInfos不存在或者gridInfos长度太小才重新创建
        if (this.gridInfos == null || this.gridInfos.length < this.gridSize) {
            this.gridInfos = new GridInfo[this.gridSize];
        }
        for (int i = 0; i < this.gridSize; i++) {
            this.gridInfos[i] = GridInfo.create();
        }

        if (gridInfos == null || gridSize <= 0) {
            throw new SceneException("sceneMapGrid init error");
        }
    }

    /**
     * 检查格子id合法性
     *
     * @param gridID
     * @return
     */
    private boolean checkGridValid(int gridID) {
        return gridID > INVALID_GRID_ID && gridID < gridSize && gridInfos[gridID] != null;
    }

    /**
     * 根据坐标计算格子ID
     *
     * @param pos
     * @return
     */
    public int getGridID(Vector3 pos) {
        return getGridID(pos.x, pos.z);
    }

    /**
     * 根据坐标计算格子ID
     *
     * @param posX
     * @param posZ
     * @return
     */
    public int getGridID(float posX, float posZ) {
        if (posX <= 0 || posZ <= 0) {
            return INVALID_GRID_ID;
        }
        int x = (int) (posX / GRID_LENGTH);
        int z = (int) (posZ / GRID_WIDTH);
        if (x >= gridSizeInLength || z >= gridSizeInWidth) {
            return INVALID_GRID_ID;
        }
        int gridID = x + z * gridSizeInLength;
        if (checkGridValid(gridID)) {
            return gridID;
        } else {
            return INVALID_GRID_ID;
        }
    }

    /**
     * 向场景格子中增加一个实体
     *
     * @param entity
     */
    public void addEntityToScene(int gridID, Entity entity) {
        this.gridInfos[gridID].addEntity(entity);
        if (entity.isMoveEntity()) {
            ((MoveEntity) entity).setGridID(gridID);
        } else if (entity.isFixedPosEntity()) {
            ((FixedPosEntity) entity).onAddEntityToScene();
        } else {
            SceneLoggerTool.gameLogger.error("addEntityToScene invalid type={}", entity.type);
        }
    }

    /**
     * 从场景格子中删除一个实体
     *
     * @param entity
     */
    public void delEntityFromScene(Entity entity) {
        int gridID = this.getGridID(entity.curPos);
        this.gridInfos[gridID].delEntity(entity);
        if (entity.isMoveEntity()) {
            ((MoveEntity) entity).setGridID(INVALID_GRID_ID);
        } else if (entity.isFixedPosEntity()) {
            ((FixedPosEntity) entity).onDelEntityFromScene();
        } else {
            SceneLoggerTool.gameLogger.error("delEntityFromScene invalid type={}", entity.type);
        }

    }

    /**
     * 更新格子内的实体id
     *
     * @param entity
     * @param oldGridID -1表示进入场景
     * @param newGridID -1表示离开场景
     */
    public void updateEntityGrid(Entity entity, int oldGridID, int newGridID) {
        if (oldGridID > INVALID_GRID_ID) {
            this.gridInfos[oldGridID].delEntity(entity);
        }
        if (newGridID > INVALID_GRID_ID) {
            this.gridInfos[newGridID].addEntity(entity);
        }
    }

    /**
     * 获取以指定位置所在格子为原点周围的所有实体id
     *
     * @param pos
     * @return
     */
    public void getEntityIDAroundPos(Vector3 pos, IntSet sets) {
        int gridID = this.getGridID(pos);
        if (gridID != INVALID_GRID_ID) {
            scanEntityIDAroundGrid(gridID, SCAN_RADIUS, sets);
        }
    }

    /**
     * 获取格子变化时受影响的实体id
     *
     * @param oldGridID          离开的格子
     * @param newGridID          进入的格子
     * @param leaveAOIEntitySets 返回离开aoi的实体列表
     * @param enterAOIEntitySets 返回新进入aoi的实体列表
     */
    public void getEntityIDWhenGridChanged(int oldGridID, int newGridID, IntSet leaveAOIEntitySets, IntSet enterAOIEntitySets) {
        IntList oldGridList = tmpGridList1;
        IntList newGridList = tmpGridList2;
        oldGridList.clear();
        newGridList.clear();
        scanGridByRect(oldGridID, SCAN_RADIUS, oldGridList);
        scanGridByRect(newGridID, SCAN_RADIUS, newGridList);
        int oldGridListSize = oldGridList.size();
        int newGridListSize = newGridList.size();
        //有非法格子，直接返回,进入场景离开场景时会出现
        if (oldGridListSize == 0 || newGridListSize == 0) {
            if (oldGridListSize > 0) {
                getEntityIDFromGridList(oldGridList, leaveAOIEntitySets);
            }
            if (newGridListSize > 0) {
                getEntityIDFromGridList(newGridList, enterAOIEntitySets);
            }
        } else {
            IntList tmpGridList = tmpGridList3;
            tmpGridList.clear();
            IntList.intersect(oldGridList, newGridList, tmpGridList);
            IntSet tmpEntitySets;
            int gridID;
            for (int i = 0; i < oldGridList.size(); i++) {
                gridID = oldGridList.get(i);
                if (!tmpGridList.contains(gridID)) {
                    if ((tmpEntitySets = getEntityIDFromGrid(gridID)) != null) {
                        leaveAOIEntitySets.addAll(tmpEntitySets);
                    }
                }
            }
            for (int i = 0; i < newGridList.size(); i++) {
                gridID = newGridList.get(i);
                if (!tmpGridList.contains(gridID)) {
                    if ((tmpEntitySets = getEntityIDFromGrid(gridID)) != null) {
                        enterAOIEntitySets.addAll(tmpEntitySets);
                    }
                }
            }
        }
    }

    /**
     * 从指定格子列表获取所有实体id
     *
     * @param gridList
     * @param sets     返回实体列表
     * @return
     */
    public void getEntityIDFromGridList(IntList gridList, IntSet sets) {
        if (gridList == null || gridList.isEmpty()) {
            return;
        }
        GridInfo gridInfo = null;
        for (int i = 0; i < gridList.size(); i++) {
            int gridID = gridList.get(i);
            if (gridID > INVALID_GRID_ID) {
                gridInfo = gridInfos[gridID];
                if (gridInfo != null && gridInfo.entitySet.size() > 0) {
                    sets.addAll(gridInfo.entitySet);
                }
            }
        }
    }

    /**
     * 从指定格子获取所有实体id
     *
     * @param gridID
     * @return
     */
    public IntSet getEntityIDFromGrid(int gridID) {
        if (gridID > INVALID_GRID_ID) {
            GridInfo gridInfo = gridInfos[gridID];
            return gridInfo.entitySet;
        }
        return null;
    }

    /**
     * 扫描以指定格子为原点周围的所有实体id
     *
     * @param gridID
     * @param scanRadius
     * @return
     */
    public void scanEntityIDAroundGrid(int gridID, int scanRadius, IntSet sets) {
        IntList tmpList = tmpGridList1;
        tmpList.clear();
        scanGridByRect(gridID, scanRadius, tmpList);
        getEntityIDFromGridList(tmpList, sets);
    }

    /**
     * 两个格子是否靠近、互相在各自的aoi内
     *
     * @param gridIDA
     * @param gridIDB
     * @return
     */
    public boolean isAroundGrid(int gridIDA, int gridIDB) {
        IntList tmpList = tmpGridList1;
        tmpList.clear();
        scanGridByRect(gridIDA, SCAN_RADIUS, tmpList);
        return tmpList.contains(gridIDB);
    }

    /**
     * 查找以指定格子为中心，指定半径的矩形区域内所有的格子id
     *
     * @param gridID
     * @param radius
     * @param gridList 返回格子id集合
     * @return
     */
    private void scanGridByRect(int gridID, int radius, IntList gridList) {
        //先不用检测
        if (gridID == INVALID_GRID_ID || radius <= 0) {
            return;
        }
        int startLength, startWidth, endLength, endWidth;
        int gridSizeInLength = this.gridSizeInLength;
        int gridSizeInWidth = this.gridSizeInWidth;
        int length = gridID % gridSizeInLength;
        int width = gridID / gridSizeInLength;
        startLength = length - radius;
        if (startLength < 0) {
            startLength = 0;
        }
        startWidth = width - radius;
        if (startWidth < 0) {
            startWidth = 0;
        }
        endLength = length + radius;
        int maxLength;
        if (endLength > (maxLength = gridSizeInLength - 1)) {
            endLength = maxLength;
        }
        endWidth = width + radius;
        int maxWidth;
        if (endWidth > (maxWidth = gridSizeInWidth - 1)) {
            endWidth = maxWidth;
        }
        for (int l = startLength; l <= endLength; l++) {
            for (int w = startWidth; w <= endWidth; w++) {
                int newGridID = l + w * gridSizeInLength;
                gridList.add(newGridID);
            }
        }
    }
}
