package com.highfly029.scene.fight.entity;

import com.highfly029.common.template.entity.EntityTypeConst;
import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName EntityFactory
 * @Description 实体创建工厂
 * @Author liyunpeng
 **/
public class EntityFactory {
    public static Entity createEntity(int type, SceneEntityWorld sceneEntityWorld) {
        Entity entity = null;
        switch (type) {
            case EntityTypeConst.PLAYER -> entity = new PlayerEntity(sceneEntityWorld);
            case EntityTypeConst.NPC -> entity = new NpcEntity(sceneEntityWorld);
            case EntityTypeConst.DROP -> entity = new DropEntity(sceneEntityWorld);
            case EntityTypeConst.MONSTER -> entity = new MonsterEntity(sceneEntityWorld);
            case EntityTypeConst.PET -> entity = new PetEntity(sceneEntityWorld);
            case EntityTypeConst.REGION -> entity = new RegionEntity(sceneEntityWorld);
        }
        return entity;
    }
}
