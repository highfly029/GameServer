package com.highfly029.scene.fight.effect;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.template.commonType.OffsetDirectionConst;
import com.highfly029.common.template.effect.EffectConst;
import com.highfly029.common.utils.Vector3Utils;
import com.highfly029.scene.fight.BaseFightLogic;
import com.highfly029.scene.fight.FightAllLogic;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.fight.specialMove.SpecialMoveLogic;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.utils.NavMeshUtils;

/**
 * @ClassName EffectFightLogic
 * @Description 技能效果数据逻辑
 * @Author liyunpeng
 **/
public class EffectFightLogic extends BaseFightLogic {
    private final Vector3 tmpVector = Vector3.create();

    public EffectFightLogic(FightAllLogic logic) {
        super(logic);
    }

    /**
     * 执行技能效果
     *
     * @param actions
     */
    public void doEffectAction(int[] actions, int startPos, CharacterEntity targetEntity) {
        CharacterEntity sourceEntity = parentLogic.characterEntity;
        int effectType = actions[startPos];
        switch (effectType) {
            case EffectConst.SelfAddBuff -> {
                int buffID = actions[startPos + 1];
                int buffLevel = actions[startPos + 2];
                sourceEntity.addBuff(buffID, buffLevel);
            }
            case EffectConst.TargetAddBuff -> {
                int buffID = actions[startPos + 1];
                int buffLevel = actions[startPos + 2];
                targetEntity.addBuff(buffID, buffLevel);
            }
            case EffectConst.SelfSubBuffUseCnt -> {
                int costType = actions[startPos + 1];
                int[] args = new int[actions.length - 2];
                for (int i = 0; i < args.length; i++) {
                    args[i] = actions[startPos + 2 + i];
                }
                sourceEntity.fightAllLogic.buffFightLogic.subBuffUseCnt(costType, args);
            }
            case EffectConst.TargetSubBuffUseCnt -> {
                int costType = actions[startPos + 1];
                int[] args = new int[actions.length - 2];
                for (int i = 0; i < args.length; i++) {
                    args[i] = actions[startPos + 2 + i];
                }
                targetEntity.fightAllLogic.buffFightLogic.subBuffUseCnt(costType, args);
            }
            case EffectConst.SelfRemoveBuff -> {
                int buffID = actions[startPos + 1];
                sourceEntity.removeBuff(buffID);
            }
            case EffectConst.TargetRemoveBuff -> {
                int buffID = actions[startPos + 1];
                targetEntity.removeBuff(buffID);
            }
            case EffectConst.SelfSetState -> {
                int stateType = actions[startPos + 1];
                boolean value = actions[startPos + 2] > 0;
                sourceEntity.fightAllLogic.stateFightLogic.setState(stateType, value);
            }
            case EffectConst.TargetSetState -> {
                int stateType = actions[startPos + 1];
                boolean value = actions[startPos + 2] > 0;
                targetEntity.fightAllLogic.stateFightLogic.setState(stateType, value);
            }
            case EffectConst.SelfDoAttack -> {

            }
            case EffectConst.TargetDoAttack -> {

            }
            case EffectConst.SelfCreatePuppet -> {

            }
            case EffectConst.TargetCreatePuppet -> {

            }
            case EffectConst.SelfAddAttribute -> {
                short attrID = (short) actions[startPos + 1];
                int attrValue = actions[startPos + 2];
                sourceEntity.addAttribute(attrID, attrValue);
            }
            case EffectConst.TargetAddAttribute -> {
                short attrID = (short) actions[startPos + 1];
                int attrValue = actions[startPos + 2];
                targetEntity.addAttribute(attrID, attrValue);
            }
            case EffectConst.SelfSubAttribute -> {
                short attrID = (short) actions[startPos + 1];
                int attrValue = actions[startPos + 2];
                sourceEntity.subAttribute(attrID, attrValue);
            }
            case EffectConst.TargetSubAttribute -> {
                short attrID = (short) actions[startPos + 1];
                int attrValue = actions[startPos + 2];
                targetEntity.subAttribute(attrID, attrValue);
            }
            case EffectConst.SelfSpecialMove -> {
                int offsetType = actions[startPos + 1];
                float distance = actions[startPos + 2];
                int time = actions[startPos + 3];
                sourceEntity.fightAllLogic.specialMoveLogic.startWithActive(offsetType, distance, time);
            }
            case EffectConst.TargetSpecialMove -> {
                if (targetEntity != null) {
                    SceneLoggerTool.gameLogger.info("TargetSpecialMove entityID={}", targetEntity.entityID);
                    SpecialMoveLogic specialMoveLogic = targetEntity.fightAllLogic.specialMoveLogic;
                    SpecialMoveLogic sourceSpecialMoveLogic = sourceEntity.fightAllLogic.specialMoveLogic;
                    specialMoveLogic.startWithPassive(sourceEntity.direction, sourceSpecialMoveLogic.specialMoveDuration, sourceSpecialMoveLogic.specialMoveSpeed);
                }
            }
            case EffectConst.SelfAvatarChangeModel -> {
                int modelID = actions[startPos + 1];
                parentLogic.avatarFightLogic.changeModel(modelID);
            }
            case EffectConst.SelfAvatarChangePart -> {
                int avatarPartType = actions[startPos + 1];
                int partID = actions[startPos + 2];
                parentLogic.avatarFightLogic.changeAvatarPart(avatarPartType, partID);
            }
            case EffectConst.SelfTeleportFixedPos -> {
                float x = actions[startPos + 1];
                float y = actions[startPos + 2];
                float z = actions[startPos + 3];
                tmpVector.set(x, y, z);
                sourceEntity.teleport(tmpVector);
            }
            case EffectConst.SelfTeleportDistance -> {
                int offsetType = actions[startPos + 1];
                int distance = actions[startPos + 2];
                tmpVector.copy(sourceEntity.direction);
                if (offsetType == OffsetDirectionConst.NegativeDirection) {
                    Vector3Utils.negative(tmpVector, tmpVector);
                }

                Vector3Utils.moveTowardsPos(tmpVector, sourceEntity.curPos, tmpVector, false, distance);

                float[] points = NavMeshUtils.rayCast(sourceEntity.scene.navMeshMode, sourceEntity.scene.navMeshName, sourceEntity.curPos.x, sourceEntity.curPos.y, sourceEntity.curPos.z, tmpVector.x, tmpVector.y, tmpVector.z);
                //如果有阻挡 则重新设置传送点坐标
                if (points != null) {
                    tmpVector.set(-points[0], points[1], points[2]);
                }
                sourceEntity.teleport(tmpVector);
            }
            default -> SceneLoggerTool.gameLogger.error("doEffectAction not exist effectType={}", effectType);
        }

    }
}
