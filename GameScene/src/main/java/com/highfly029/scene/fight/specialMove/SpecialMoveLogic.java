package com.highfly029.scene.fight.specialMove;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.template.commonType.OffsetDirectionConst;
import com.highfly029.common.utils.Vector3Utils;
import com.highfly029.scene.fight.BaseFightLogic;
import com.highfly029.scene.fight.FightAllLogic;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.fight.scene.SceneMapGrid;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.utils.NavMeshUtils;

/**
 * @ClassName SpecialMoveLogic
 * @Description 特殊移动
 * @Author liyunpeng
 **/
public class SpecialMoveLogic extends BaseFightLogic {
    private final Vector3 tmpVector1 = Vector3.create();

    /***************** specialMove **************/
    public boolean specialMovePassive = false;//默认主动的特殊移动
    public int specialMoveDuration;
    public float specialMoveSpeed;
    private final Vector3 specialMoveDir = Vector3.create();
    private final Vector3 specialMoveFinalPos = Vector3.create();

    public SpecialMoveLogic(FightAllLogic fightAllLogic) {
        super(fightAllLogic);
    }


    public void startWithPassive(Vector3 sourceSpecialMoveDir, int sourceSpecialMoveDuration, float sourceSpecialMoveSpeed) {
        CharacterEntity characterEntity = parentLogic.characterEntity;
        this.specialMovePassive = true;
        this.specialMoveDir.copy(sourceSpecialMoveDir);
        this.specialMoveDuration = sourceSpecialMoveDuration;
        this.specialMoveSpeed = sourceSpecialMoveSpeed;
        characterEntity.specialMoveStart(specialMovePassive, specialMoveDuration, specialMoveSpeed, specialMoveDir);
    }

    /**
     * 初始化特殊移动效果
     *
     * @param offsetType 偏移类型
     * @param distance   移动距离 (m)
     * @param time       移动时间 (ms) 最少1ms
     */
    public void startWithActive(int offsetType, float distance, int time) {
        CharacterEntity characterEntity = parentLogic.characterEntity;
        specialMoveDir.copy(characterEntity.direction);
        if (offsetType == OffsetDirectionConst.NegativeDirection) {
            Vector3Utils.negative(specialMoveDir, specialMoveDir);
        }
        specialMoveDuration = time;
        specialMoveSpeed = distance / time;
        Vector3Utils.moveTowardsPos(specialMoveFinalPos, characterEntity.curPos, specialMoveDir, false, distance);
        float[] points = NavMeshUtils.rayCast(characterEntity.scene.navMeshMode, characterEntity.scene.navMeshName, characterEntity.curPos.x, characterEntity.curPos.y, characterEntity.curPos.z, specialMoveFinalPos.x, specialMoveFinalPos.y, specialMoveFinalPos.z);
        if (points != null) {
            specialMoveFinalPos.set(-points[0], points[1], points[2]);
        }
        if (characterEntity.scene.sceneMapGrid.getGridID(specialMoveFinalPos) == SceneMapGrid.INVALID_GRID_ID) {
            SceneLoggerTool.gameLogger.error("special move start invalid finalPos", new Exception());
            return;
        }
        characterEntity.specialMoveStart(specialMovePassive, specialMoveDuration, specialMoveSpeed, specialMoveDir);
    }


    @Override
    public void onTick(int escapedMillTime) {
        CharacterEntity characterEntity = parentLogic.characterEntity;
        if (specialMoveDuration > 0) {
            int realTime;
            if (specialMoveDuration >= escapedMillTime) {
                specialMoveDuration -= escapedMillTime;
                realTime = escapedMillTime;
            } else {
                realTime = specialMoveDuration;
                specialMoveDuration = 0;
            }
            float dis = specialMoveSpeed * realTime;
            Vector3 tmpVector = tmpVector1;
            Vector3Utils.moveTowardsPos(tmpVector, characterEntity.curPos, specialMoveDir, false, dis);
            characterEntity.moveToPos(tmpVector);
            if (specialMoveDuration <= 0) {
                characterEntity.specialMoveOver(true);
            }
        }
    }
}
