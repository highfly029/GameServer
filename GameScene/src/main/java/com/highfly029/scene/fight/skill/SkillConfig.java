package com.highfly029.scene.fight.skill;

import com.highfly029.common.template.passiveSkill.PassiveSkillTemplate;
import com.highfly029.common.template.skill.SkillTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName SkillConfig
 * @Description 技能配置数据
 * @Author liyunpeng
 **/
public class SkillConfig extends BaseGlobalConfig {
    //技能id<<32|技能等级，技能模板
    private final LongObjectMap<SkillTemplate> skillTemplates = new LongObjectMap<>(SkillTemplate[]::new);
    //被动技能 id << 32 || skillLevel
    private final LongObjectMap<PassiveSkillTemplate> passiveSkillTemplates = new LongObjectMap<>(PassiveSkillTemplate[]::new);

    @Override
    public void check(boolean isHotfix) {
        //TODO 被动技能表 检测type符合 检测actions只有属性可以有多组
    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(SkillTemplate.class);
        SkillTemplate template;
        for (int i = 0, size = list.size(); i < size; i++) {
            template = (SkillTemplate) list.get(i);
            skillTemplates.put(MathUtils.getCompositeIndex(template.getSkillID(), template.getSkillLevel()), template);
        }

        PassiveSkillTemplate passiveSkillTemplate;
        list = TemplateTool.dataTemplates.get(PassiveSkillTemplate.class);
        for (int i = 0, size = list.size(); i < size; i++) {
            passiveSkillTemplate = (PassiveSkillTemplate) list.get(i);
            passiveSkillTemplates.put(MathUtils.getCompositeIndex(passiveSkillTemplate.getPassiveSkillID(), passiveSkillTemplate.getLevel()), passiveSkillTemplate);
        }
    }

    /**
     * 获取技能等级模板
     *
     * @param skillID
     * @param skillLevel
     * @return
     */
    public SkillTemplate getSkillTemplate(int skillID, int skillLevel) {
        return skillTemplates.get(MathUtils.getCompositeIndex(skillID, skillLevel));
    }

    /**
     * 获取被动技能模版
     *
     * @param skillID
     * @param skillLevel
     * @return
     */
    public PassiveSkillTemplate getPassiveSkillTemplate(int skillID, int skillLevel) {
        return passiveSkillTemplates.get(MathUtils.getCompositeIndex(skillID, skillLevel));
    }
}
