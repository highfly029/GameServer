package com.highfly029.scene.fight.entity;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbEntity;
import com.highfly029.common.template.model.ModelTemplate;
import com.highfly029.common.template.passiveSkill.PassiveSkillTemplate;
import com.highfly029.common.template.passiveSkill.PassiveSkillTypeConst;
import com.highfly029.common.template.skill.SkillTargetTypeConst;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.common.utils.Vector3Utils;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.scene.fight.FightAllLogic;
import com.highfly029.scene.fight.buff.BuffData;
import com.highfly029.scene.fight.bullet.BulletData;
import com.highfly029.scene.fight.skill.SkillData;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.utils.BroadCastUtils;
import com.highfly029.scene.utils.PbSceneUtils;
import com.highfly029.scene.world.SceneEntityWorld;
import com.highfly029.utils.collection.IntBooleanMap;
import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.IntList;

/**
 * @ClassName CharacterEntity
 * @Description 角色实体，处理属性 技能 buff 战斗
 * @Author liyunpeng
 **/
public class CharacterEntity extends MoveEntity {
    //种族血统(一级属性影响二级属性的条件)
    public int race;
    //角色实体等级
    public int level;
    /**
     * 视野半径，自己能看到别人的最大距离
     */
    public int viewRadius;
    /**
     * 势力
     */
    public byte force;
    /**
     * 是否被ai控制
     */
    public boolean controlByAI;
    /**
     * 移动类型
     */
    public int moveType;
    /**
     * 移动列表
     */
    public IntList moveList;

    /******************** cache *******************/
    public float tmpDistanceSquare;
    /**
     * 临时缓存目标点
     */
    public Vector3 tmpVector = Vector3.create();
    /******************** cache *******************/

    /**
     * 战斗数据逻辑
     */
    public FightAllLogic fightAllLogic;

    public CharacterEntity(SceneEntityWorld sceneEntityWorld) {
        super(sceneEntityWorld);
        fightAllLogic = new FightAllLogic(this);
    }

    @Override
    public boolean isCharacterEntity() {
        return true;
    }

    /**
     * 是否是怪物单位
     *
     * @return
     */
    public boolean isMonster() {
        return false;
    }

    /**
     * 是否归属于怪物
     *
     * @return
     */
    public boolean isBelongToMonster() {
        return false;
    }

    /**
     * 是否归属于玩家
     *
     * @return
     */
    public boolean isBelongToPlayer() {
        return false;
    }

    /**
     * 获取主人 entityID
     *
     * @return
     */
    public int getMasterEntityID() {
        return entityID;
    }

    /**
     * 获取主人 entity
     *
     * @return
     */
    public CharacterEntity getMasterEntity() {
        return this;
    }

    /**
     * 获取主人 势力ID
     *
     * @return
     */
    public byte getMasterForceID() {
        return force;
    }

    //增加属性
    public void addAttribute(short attrID, int attrValue) {
        fightAllLogic.attributeFightLogic.addOneAttribute(attrID, attrValue);
    }

    //减少属性
    public void subAttribute(short attrID, int attrValue) {
        fightAllLogic.attributeFightLogic.subOneAttribute(attrID, attrValue);
    }

    //获取属性
    public int getAttribute(short attrID) {
        return fightAllLogic.attributeFightLogic.getAttribute(attrID);
    }

    /**
     * 增加buff
     *
     * @param buffID
     * @param buffLevel
     */
    public void addBuff(int buffID, int buffLevel) {
        fightAllLogic.buffFightLogic.addBuff(buffID, buffLevel);
    }

    /**
     * 删除buff
     *
     * @param buffID
     */
    public void removeBuff(int buffID) {
        fightAllLogic.buffFightLogic.removeBuffByBuffID(buffID);

    }

    /**
     * 删除buff
     *
     * @param type
     */
    public void removeBuffByType(byte type) {
        fightAllLogic.buffFightLogic.removeBuffByRemoveType(type);
    }

    /**
     * 增加主动技能
     *
     * @param skillID
     * @param skillLevel
     */
    public void addActiveSkillData(int skillID, int skillLevel) {
        fightAllLogic.skillFightLogic.skillLevelMap.put(skillID, skillLevel);
    }

    /**
     * 获取技能扩展效果
     *
     * @param skillEffectType
     * @return
     */
    public int getSkillExtendEffect(int skillEffectType) {
        return 0;
    }

    /**
     * 增加被动技能
     *
     * @param skillID
     * @param skillLevel
     */
    public void addPassiveSkillData(int skillID, int skillLevel) {
        PassiveSkillTemplate passiveSkillTemplate = ConfigTool.getSkillConfig().getPassiveSkillTemplate(skillID, skillLevel);
        if (passiveSkillTemplate == null) {
            SceneLoggerTool.gameLogger.error("addPassiveSkillData not exist template skillID={}, skillLevel={}", skillID, skillLevel);
            return;
        }
        switch (passiveSkillTemplate.getType()) {
            case PassiveSkillTypeConst.ModifyAttribute -> {
                //如果等级>1 先删除之前增加的技能效果
                if (skillLevel > 1) {
                    PassiveSkillTemplate preTemplate = ConfigTool.getSkillConfig().getPassiveSkillTemplate(skillID, skillLevel - 1);
                    if (preTemplate.getType() != passiveSkillTemplate.getType()) {
                        SceneLoggerTool.gameLogger.error("addPassiveSkillData invalid type skillID={}, skillLevel={}", skillID, skillLevel);
                        return;
                    }
                    for (int[] action : preTemplate.getActions()) {
                        subAttribute((short) action[0], action[1]);
                    }
                }
                for (int[] action : passiveSkillTemplate.getActions()) {
                    addAttribute((short) action[0], action[1]);
                }
            }
            case PassiveSkillTypeConst.ModifySkillEffect -> {

            }
        }
    }

    /**
     * 删除被动技能
     *
     * @param skillID
     * @param skillLevel
     */
    public void removePassiveSkillData(int skillID, int skillLevel) {
        PassiveSkillTemplate passiveSkillTemplate = ConfigTool.getSkillConfig().getPassiveSkillTemplate(skillID, skillLevel);
        if (passiveSkillTemplate == null) {
            SceneLoggerTool.gameLogger.error("removePassiveSkillData not exist template skillID={}, skillLevel={}", skillID, skillLevel);
            return;
        }
        switch (passiveSkillTemplate.getType()) {
            case PassiveSkillTypeConst.ModifyAttribute -> {
                for (int[] action : passiveSkillTemplate.getActions()) {
                    subAttribute((short) action[0], action[1]);
                }
            }
            case PassiveSkillTypeConst.ModifySkillEffect -> {

            }
        }
    }

    /**
     * 同场景瞬移
     *
     * @param targetPos
     */
    public void teleport(Vector3 targetPos) {
        //如果是远距离传送跨越aoi则不必广播、否则必须广播新位置
        //现在没发区分是否远距离传送 全部都广播
        if (moveToPos(targetPos)) {
            //先推送位置改变、再推送aoi
            onPosChangePushAll(targetPos);
        }
    }

    /**
     * 客户端释放技能
     */
    public void clientUseSkill(PbEntity.C2SEntityCastSkill skillReq) {
        PbCommon.PbSkillData pbSkillData = skillReq.getSkillData();
        int skillID = pbSkillData.getSkillID();
        int skillLevel = pbSkillData.getSkillLevel();
        byte skillTargetType = (byte) pbSkillData.getSkillTargetType();
        int targetEntityID = 0;
        Vector3 targetPos = null;
        Vector3 targetDir = null;
        switch (skillTargetType) {
            case SkillTargetTypeConst.TargetEntityID -> {
                targetEntityID = pbSkillData.getTargetEntityID();
            }
            case SkillTargetTypeConst.TargetPosition -> {
                targetPos = PbCommonUtils.vector3Pb2Obj(pbSkillData.getTargetPos());
            }
            case SkillTargetTypeConst.TargetDirection -> {
                targetDir = PbCommonUtils.vector3Pb2Obj(pbSkillData.getTargetDir());
            }
        }
        //client check

        fightAllLogic.skillFightLogic.preUseSkill(skillID, targetEntityID, targetPos, targetDir);
    }

    /**
     * 释放技能失败
     *
     * @param skillID
     */
    public void onUseSkillFail(int skillID, String reason) {
    }


    //服务器释放技能
    public void serverUseSkill(int skillID, SkillData skillData) {
        fightAllLogic.skillFightLogic.preUseSkill(skillID, skillData.targetEntityID, skillData.targetPos, skillData.targetDir);
    }

    @Override
    public void onTick(int escapedMillTime) {
        fightAllLogic.onTick(escapedMillTime);
        super.onTick(escapedMillTime);
    }

    @Override
    public void onSecond() {
        fightAllLogic.onSecond();
    }

    /**
     * delayTime后应该所在位置( 100ms )
     *
     * @return
     */
    public Vector3 nextFramePos(int delayTime) {
        float dis = nextFrameDis(delayTime);
        Vector3Utils.moveTowardsPos(tmpVector, this.curPos, this.direction, false, dis);
        return tmpVector;
    }

    /**
     * delayTime后移动的距离
     *
     * @param delayTime
     * @return
     */
    public float nextFrameDis(int delayTime) {
        float speed = 50f;//每秒50米的速度前进
        return (float) (speed * delayTime * SystemTimeTool.SECOND_TIME_PER_MILL);
    }

    /**
     * 受到伤害
     *
     * @param realDamageValue 受到的真实伤害值(掉血值)
     * @param damageSource    伤害来源
     */
    public void onDamage(int realDamageValue, CharacterEntity damageSource) {
        scene.onCharacterDamage(damageSource, this, realDamageValue);
    }

    /**
     * 接受治疗
     *
     * @param realHealValue
     * @param healSource
     */
    public void onHeal(int realHealValue, CharacterEntity healSource) {

    }

    /**
     * 死亡
     *
     * @param attackSource
     */
    public void onDeath(CharacterEntity attackSource) {

    }

    /**
     * 复活
     */
    public void onReborn() {

    }

    /**
     * 属性变化通知
     *
     * @param num                     变化数量
     * @param changeAttributeIDArray  变化属性id
     * @param lastAttributeValueArray 变化之前的值
     */
    public void onAttributeChange(int num, short[] changeAttributeIDArray, int[] lastAttributeValueArray) {
//        SceneLoggerTool.gameLogger.info("onAttributeChange num={}, changeAttributeIDArray={}, lastAttributeValueArray={}", num, changeAttributeIDArray, lastAttributeValueArray);
    }

    /**
     * 属性改变推送自己
     *
     * @param sendSelfMap
     */
    public void onAttributePushSelf(IntIntMap sendSelfMap) {
//        SceneLoggerTool.gameLogger.info("onAttributePushSelf sendSelfMap={}", sendSelfMap);
    }

    /**
     * 属性改变推送其他玩家
     *
     * @param sendOtherMap
     */
    public void onAttributePushOther(IntIntMap sendOtherMap) {
//        SceneLoggerTool.gameLogger.info("onAttributePushOther sendOtherMap={}", sendOtherMap);
        PbEntity.S2CEntityAttribute.Builder builder = getEntityAttributePushBuilder(sendOtherMap);
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntityAttribute, builder.build(), false);
    }

    /**
     * 属性推送主人
     *
     * @param sendMasterMap
     */
    public void onAttributePushMaster(IntIntMap sendMasterMap) {
//        SceneLoggerTool.gameLogger.info("onAttributePushMaster sendMasterMap={}", sendMasterMap);
    }

    /**
     * 状态改变通知
     *
     * @param num                         状态改变的数量
     * @param lastDispatchStateIDArray    变化状态id
     * @param lastDispatchStateValueArray 变化之前的值
     */
    public void onStateChange(int num, short[] lastDispatchStateIDArray, boolean[] lastDispatchStateValueArray) {
        SceneLoggerTool.gameLogger.info("onStateChange num={}, lastDispatchStateIDArray={}, lastDispatchStateValueArray={}", num, lastDispatchStateIDArray, lastDispatchStateValueArray);
    }

    //状态改变推送自己
    public void onStatePushSelf(IntBooleanMap sendSelfMap) {
        SceneLoggerTool.gameLogger.info("onStatePushSelf sendSelfMap={}", sendSelfMap);
    }

    //状态改变推送其他玩家
    public void onStatePushOther(IntBooleanMap sendOtherMap) {
        SceneLoggerTool.gameLogger.info("onStatePushOther sendOtherMap={}", sendOtherMap);
        PbEntity.S2CEntityState.Builder builder = getEntityStatePushBuilder(sendOtherMap);
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntityState, builder.build(), false);
    }

    //状态改变推送master
    public void onStatePushMaster(IntBooleanMap sendMasterMap) {
        SceneLoggerTool.gameLogger.info("onStatePushMaster sendMasterMap={}", sendMasterMap);
    }

    /**
     * 获取属性改变推送pb
     *
     * @param sendMap
     * @return
     */
    protected PbEntity.S2CEntityAttribute.Builder getEntityAttributePushBuilder(IntIntMap sendMap) {
        PbEntity.S2CEntityAttribute.Builder builder = PbEntity.S2CEntityAttribute.newBuilder();
        builder.setEntityID(this.entityID);
        PbCommon.PbKeyValue.Builder keyValueBuilder = PbCommon.PbKeyValue.newBuilder();
        int freeValue = sendMap.getFreeValue();
        long[] tables = sendMap.getTable();
        long entry;
        int key;
        int value;
        for (int i = 0, len = tables.length; i < len; i++) {
            entry = tables[i];
            if ((key = (int) entry) != freeValue) {
                value = (int) (entry >>> 32);
                keyValueBuilder.clear();
                keyValueBuilder.setIntKey(key);
                keyValueBuilder.setIntValue(value);
                builder.addAttribute(keyValueBuilder.build());
            }
        }
        return builder;
    }

    /**
     * 获取状态改变推送pb
     *
     * @param sendMap
     * @return
     */
    protected PbEntity.S2CEntityState.Builder getEntityStatePushBuilder(IntBooleanMap sendMap) {
        PbEntity.S2CEntityState.Builder builder = PbEntity.S2CEntityState.newBuilder();
        builder.setEntityID(this.entityID);
        PbCommon.PbKeyValue.Builder keyValueBuilder = PbCommon.PbKeyValue.newBuilder();

        int freeValue = sendMap.getFreeValue();
        int[] keys = sendMap.getKeys();
        boolean[] values = sendMap.getValues();
        int key;
        boolean value;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                value = values[i];
                keyValueBuilder.clear();
                keyValueBuilder.setIntKey(key);
                keyValueBuilder.setBoolValue(value);
                builder.addState(keyValueBuilder);
            }
        }
        return builder;
    }

    /**
     * 增加buff推送自己
     *
     * @param buffData
     */
    public void onAddBuffPushSelf(BuffData buffData) {
        SceneLoggerTool.gameLogger.info("onAddBuffPushSelf buffData={}", buffData);
    }

    /**
     * 增加buff推送自己和周围
     *
     * @param buffData
     */
    public void onAddBuffPushAll(BuffData buffData) {
        SceneLoggerTool.gameLogger.info("onAddBuffPushAll buffData={}", buffData);
        PbEntity.S2CEntityAddBuff.Builder builder = PbEntity.S2CEntityAddBuff.newBuilder();
        builder.setEntityID(entityID);
        builder.setBuffData(getEntityBuffPushBuilder(buffData));
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntityAddBuff, builder.build(), true);
    }

    /**
     * 删除buff推送自己
     *
     * @param instanceID
     */
    public void onRemoveBuffPushSelf(int instanceID) {
        SceneLoggerTool.gameLogger.info("onRemoveBuffPushSelf instanceID={}", instanceID);
    }

    /**
     * 删除buff推送自己和周围
     *
     * @param instanceID
     */
    public void onRemoveBuffPushAll(int instanceID) {
        SceneLoggerTool.gameLogger.info("onRemoveBuffPushAll instanceID={}", instanceID);
        PbEntity.S2CEntityRemoveBuff.Builder builder = PbEntity.S2CEntityRemoveBuff.newBuilder();
        builder.setEntityID(entityID);
        builder.setInstanceID(instanceID);
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntityRemoveBuff, builder.build(), true);
    }

    /**
     * 刷新buff推送自己
     *
     * @param buffData
     */
    public void onRefreshBuffPushSelf(BuffData buffData) {
        SceneLoggerTool.gameLogger.info("onRefreshBuffPushSelf buffData={}", buffData);
    }

    /**
     * 刷新buff推送自己和周围
     *
     * @param buffData
     */
    public void onRefreshBuffPushAll(BuffData buffData) {
        PbEntity.S2CEntityRefreshBuff.Builder builder = PbEntity.S2CEntityRefreshBuff.newBuilder();
        builder.setEntityID(entityID);
        builder.setBuffData(getEntityBuffPushBuilder(buffData));
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntityRefreshBuff, builder.build(), true);
    }

    /**
     * 刷新buff使用次数
     *
     * @param buffData
     */
    public void onRefreshBuffUseCntPushSelf(BuffData buffData) {
        SceneLoggerTool.gameLogger.info("onRefreshBuffUseCntPushSelf buffData={}", buffData);
    }

    /**
     * 刷新buff叠层数
     *
     * @param buffData
     */
    public void onRefreshBuffFloorNumPushSelf(BuffData buffData) {
        SceneLoggerTool.gameLogger.info("onRefreshBuffFloorNumPushSelf buffData={}", buffData);
    }

    /**
     * 获取buffData pb
     *
     * @param buffData
     * @return
     */
    protected PbCommon.PbBuffData.Builder getEntityBuffPushBuilder(BuffData buffData) {
        PbCommon.PbBuffData.Builder builder = PbCommon.PbBuffData.newBuilder();
        builder.setInstanceID(buffData.instanceID);
        builder.setBuffID(buffData.buffID);
        builder.setLevel(buffData.level);
        builder.setEndTime(buffData.endTime);
        builder.setUseCnt(buffData.useCnt);
        builder.setFloorNum(buffData.floorNum);
        return builder;
    }

    /**
     * 改变avatar模型
     *
     * @param modelID
     * @param parts
     */
    public void onAvatarChangeModelPushAll(int modelID, byte[] parts) {
        SceneLoggerTool.gameLogger.info("onAvatarChangeModelPushAll modelID={}, parts={}", modelID, parts);
        PbEntity.S2CEntityChangeModel.Builder builder = PbEntity.S2CEntityChangeModel.newBuilder();
        builder.setEntityID(entityID);
        builder.setModelID(modelID);
        for (byte partID : parts) {
            builder.addParts(partID);
        }
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntityChangeModel, builder.build(), true);
    }

    /**
     * 只改变avatar部件
     *
     * @param avatarPartType
     * @param partID
     */
    public void onAvatarChangePartPushAll(int avatarPartType, int partID) {
        SceneLoggerTool.gameLogger.info("onAvatarChangePartPushAll avatarPartType={}, partID={}", avatarPartType, partID);
        PbEntity.S2CEntityChangeAvatarPart.Builder builder = PbEntity.S2CEntityChangeAvatarPart.newBuilder();
        builder.setEntityID(entityID);
        builder.setAvatarPartType(avatarPartType);
        builder.setPartID(partID);
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntityChangeAvatarPart, builder.build(), true);
    }

    /**
     * avatar变身刷新
     *
     * @param transformModelID
     */
    public void onAvatarTransformPushAll(int transformModelID) {
        SceneLoggerTool.gameLogger.info("onAvatarTransformPushAll transformModelID={}", transformModelID);
        PbEntity.S2CEntityAvatarTransform.Builder builder = PbEntity.S2CEntityAvatarTransform.newBuilder();
        builder.setEntityID(entityID);
        builder.setTransformModelID(transformModelID);
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntityAvatarTransform, builder.build(), true);
    }

    /**
     * 模型改变通知
     *
     * @param modelID
     */
    public void onAvatarModelChange(int modelID) {
        ModelTemplate modelTemplate = ConfigTool.getAvatarConfig().getModelTemplate(modelID);
        //更改碰撞半径
        collideRadius = modelTemplate.getCollideRadius();
        SceneLoggerTool.gameLogger.info("onAvatarModelChange modelID={}, collideRadius={}", modelID, collideRadius);
    }

    /**
     * 推送给自己 开始一个CD
     *
     * @param coolDownType
     * @param id
     * @param realDuration
     */
    public void onBeginCoolDownPushSelf(byte coolDownType, int id, int realDuration) {
        SceneLoggerTool.gameLogger.info("onBeginCoolDownPushSelf coolDownType={}, id={}, realDuration={}", coolDownType, id, realDuration);
    }

    /**
     * 刷新cd 删除cd
     *
     * @param coolDownType
     * @param id
     */
    public void onRemoveCoolDownPushSelf(byte coolDownType, int id) {
        SceneLoggerTool.gameLogger.info("onRemoveCoolDownPushSelf coolDownType={}, id={}", coolDownType, id);
    }

    /**
     * 推送给自己 刷新cd组固定值
     *
     * @param groupID
     * @param nowValue
     */
    public void onCoolDownGroupFixedTimeRefreshPushSelf(int groupID, int nowValue) {
        SceneLoggerTool.gameLogger.info("onCoolDownGroupFixedTimeRefreshPushSelf groupID={}, nowValue={}", groupID, nowValue);
    }

    /**
     * 推送给自己 刷新cd组万分比
     *
     * @param groupID
     * @param nowPercentValue
     */
    public void onCoolDownGroupPercentTimeRefreshPushSelf(int groupID, int nowPercentValue) {
        SceneLoggerTool.gameLogger.info("onCoolDownGroupPercentTimeRefreshPushSelf groupID={}, nowPercentValue={}", groupID, nowPercentValue);
    }

    /**
     * 推送给自己 刷新通用cd类型固定值变化量
     *
     * @param coolDownType
     * @param nowValue
     */
    public void onCoolDownTypeFixedTimeRefreshPushSelf(int coolDownType, int nowValue) {
        SceneLoggerTool.gameLogger.info("onCoolDownTypeFixedTimeRefreshPushSelf coolDownType={}, nowValue={}", coolDownType, nowValue);
    }

    /**
     * 推送给自己 刷新通用cd类型万分比变化量
     *
     * @param coolDownType
     * @param nowPercentValue
     */
    public void onCoolDownTypePercentTimeRefreshPushSelf(int coolDownType, int nowPercentValue) {
        SceneLoggerTool.gameLogger.info("onCoolDownTypePercentTimeRefreshPushSelf coolDownType={}, nowValue={}", coolDownType, nowPercentValue);
    }

    /**
     * 位置改变推送周围
     *
     * @param curPos
     */
    public void onPosChangePushAll(Vector3 curPos) {
        PbEntity.S2CSetEntityPos.Builder builder = PbEntity.S2CSetEntityPos.newBuilder();
        builder.setEntityID(entityID);
        builder.setCurPos(PbCommonUtils.vector3Obj2Pb(curPos));
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CSetEntityPos, builder.build(), true);
    }

    /**
     * 位置改变推送自己
     */
    public void onPosChangePushSelf() {

    }

    /**
     * 方向改变推送周围
     *
     * @param curDir
     */
    public void onDirChangePushAll(Vector3 curDir) {
        PbEntity.S2CSetEntityDir.Builder builder = PbEntity.S2CSetEntityDir.newBuilder();
        builder.setEntityID(entityID);
        builder.setCurDir(PbCommonUtils.vector3Obj2Pb(curDir));
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CSetEntityDir, builder.build(), true);
    }

    /**
     * 推送周围 创建子弹
     */
    public void onCreateBulletPushAll(BulletData bulletData, SkillData skillData) {
        PbEntity.S2CEntityCreateBullet.Builder builder = PbEntity.S2CEntityCreateBullet.newBuilder();
        builder.setEntityID(entityID);
        builder.setBulletID(bulletData.bulletTemplate.getBulletID());
        builder.setBulletLevel(bulletData.bulletTemplate.getBulletLevel());
        builder.setBulletInstanceID(bulletData.instanceID);
        builder.setSkillData(PbSceneUtils.skillDataObj2Pb(skillData));
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntityCreateBullet, builder.build(), true);
    }

    /**
     * 推送周围删除子弹
     */
    public void onRemoveBulletPushAll(BulletData bulletData) {
        PbEntity.S2CEntityRemoveBullet.Builder builder = PbEntity.S2CEntityRemoveBullet.newBuilder();
        builder.setEntityID(entityID);
        builder.setBulletInstanceID(bulletData.instanceID);
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntityRemoveBullet, builder.build(), true);
    }

    /**
     * 特殊移动开始
     *
     * @param specialMovePassive
     * @param specialMoveDuration
     * @param specialMoveSpeed
     * @param specialMoveDir
     */
    public void specialMoveStart(boolean specialMovePassive, int specialMoveDuration, float specialMoveSpeed, Vector3 specialMoveDir) {
        PbEntity.S2CEntitySpecialMoveStart.Builder builder = PbEntity.S2CEntitySpecialMoveStart.newBuilder();
        builder.setEntityID(entityID);
        builder.setIsPassive(specialMovePassive);
        builder.setDuration(specialMoveDuration);
        builder.setSpeed((int) (specialMoveSpeed * Vector3.FLOAT2INT_SCALE));
        builder.setDir(PbCommonUtils.vector3Obj2Pb(specialMoveDir));
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntitySpecialMoveStart, builder.build(), true);
        SceneLoggerTool.gameLogger.info("specialMoveStart entityID={}, curPos={}, dir={}", entityID, curPos, direction);
    }

    /**
     * 特殊移动结束
     *
     * @param isNormal
     */
    public void specialMoveOver(boolean isNormal) {
        PbEntity.S2CEntitySpecialMoveOver.Builder builder = PbEntity.S2CEntitySpecialMoveOver.newBuilder();
        builder.setEntityID(entityID);
        builder.setCurPos(PbCommonUtils.vector3Obj2Pb(curPos));
        BroadCastUtils.broadCastToAOIPlayers(this, PtCode.S2CEntitySpecialMoveOver, builder.build(), true);
        SceneLoggerTool.gameLogger.info("specialMoveOver entityID={}, curPos={}, dir={}", entityID, curPos, direction);
    }
}
