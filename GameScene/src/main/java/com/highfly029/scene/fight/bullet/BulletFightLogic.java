package com.highfly029.scene.fight.bullet;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.template.bullet.BulletTemplate;
import com.highfly029.common.template.bullet.BulletTypeConst;
import com.highfly029.common.utils.Vector3Utils;
import com.highfly029.scene.fight.BaseFightLogic;
import com.highfly029.scene.fight.FightAllLogic;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.fight.scene.SceneMapGrid;
import com.highfly029.scene.fight.skill.SkillData;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.utils.NavMeshUtils;
import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName BulletFightLogic
 * @Description 子弹数据逻辑
 * @Author liyunpeng
 **/
public class BulletFightLogic extends BaseFightLogic {
    //子弹集合
    private final IntObjectMap<BulletData> bulletDataMap = new IntObjectMap<>(BulletData[]::new);

    /************** tmp cache *********************/
    //子弹碰撞目标列表
    private final ObjectList<CharacterEntity> tmpHitTargets = new ObjectList<>();
    Vector3 tmpVector = Vector3.create();

    /************** tmp cache *********************/

    public BulletFightLogic(FightAllLogic logic) {
        super(logic);
    }

    /**
     * 生产子弹
     *
     * @param bulletID
     * @param bulletLevel
     * @param skillData
     * @param attackSource
     */
    public void doCreateBullet(int bulletID, int bulletLevel, SkillData skillData, CharacterEntity attackSource) {
        BulletTemplate tmpBulletTemplate = ConfigTool.getBulletConfig().getBulletTemplate(bulletID, bulletLevel);
        if (tmpBulletTemplate == null) {
            SceneLoggerTool.gameLogger.info("doCreateBullet template is null bulletID={}, bulletLevel={}", bulletID, bulletLevel);
            return;
        }
        BulletData bulletData = BulletData.create();
        bulletData.autoIncrementIndex();
        SkillData tmpSkillData = SkillData.create();
        tmpSkillData.copy(skillData);
        bulletData.skillData = tmpSkillData;

        bulletData.bulletTemplate = tmpBulletTemplate;

        if (bulletData.init(this)) {
            bulletDataMap.put(bulletData.instanceID, bulletData);
            attackSource.onCreateBulletPushAll(bulletData, skillData);
        } else {
            //销毁bulletData
        }
        SceneLoggerTool.gameLogger.info("doCreateBullet size={}", bulletDataMap.size());
    }

    @Override
    public void onTick(int escapedMillTime) {
        bulletDataMap.foreachImmutable((k, bulletData) -> {
            switch (bulletData.bulletTemplate.getBulletType()) {
                case BulletTypeConst.Immediately -> {
                    bulletDoAttack(bulletData);
                    bulletData.setDestroy(true);
                }
                case BulletTypeConst.FixedTime -> {
                    if ((bulletData.duration -= escapedMillTime) <= 0) {
                        bulletDoAttack(bulletData);
                        bulletData.setDestroy(true);
                    }
                }
                case BulletTypeConst.FixedSpeed -> {
                    if ((bulletData.duration -= escapedMillTime) <= 0) {
                        bulletDoAttack(bulletData);
                        bulletData.setDestroy(true);
                    }
                }
                case BulletTypeConst.HitStraight -> {
                    if ((bulletData.duration -= escapedMillTime) <= 0) {
                        bulletData.setDestroy(true);
                    } else {
                        bulletHitTick(bulletData, escapedMillTime);
                    }
                }
            }
        });

        bulletDataMap.foreachMutable((k, v) -> {
            if (v.isDestroy) {
                bulletDataMap.remove(k);
                if (v.bulletTemplate.getBulletType() == BulletTypeConst.HitStraight) {
                    parentLogic.characterEntity.onRemoveBulletPushAll(v);
                }
                v.clear();
            }
        });


    }

    /**
     * 子弹执行攻击
     */
    public void bulletDoAttack(BulletData bulletData) {
        SceneLoggerTool.gameLogger.info("bulletDoAttack");
        parentLogic.attackFightLogic.doAttackAction(bulletData.bulletTemplate.getAttackID(), bulletData.bulletTemplate.getAttackLevel(), bulletData.skillData, null);
    }

    /**
     * 子弹每帧碰撞检测
     *
     * @param bulletData
     * @param escapedMillTime
     */
    public void bulletHitTick(BulletData bulletData, int escapedMillTime) {
        CharacterEntity characterEntity = parentLogic.characterEntity;
        float dis = bulletData.speed * escapedMillTime;
        Vector3Utils.moveTowardsPos(this.tmpVector, bulletData.pos, bulletData.dir, false, dis);
        float[] points = NavMeshUtils.rayCast(characterEntity.scene.navMeshMode, characterEntity.scene.navMeshName, bulletData.pos.x, bulletData.pos.y, bulletData.pos.z, this.tmpVector.x, this.tmpVector.y, this.tmpVector.z);
        if (points != null) {
            //子弹不能穿越障碍物
            bulletData.setDestroy(true);
            return;
        }
        bulletData.pos.copy(this.tmpVector);
        if (characterEntity.scene.sceneMapGrid.getGridID(bulletData.pos) == SceneMapGrid.INVALID_GRID_ID) {
            //子弹离开地图范围
            bulletData.setDestroy(true);
            return;
        }
        tmpHitTargets.clear();
        BulletTemplate bulletTemplate = bulletData.bulletTemplate;
        parentLogic.attackFightLogic.selectAttackTargets(tmpHitTargets, parentLogic.characterEntity, bulletData.pos, bulletData.dir, bulletTemplate.getHitScopeParams(), 2, null, bulletTemplate.getInfluenceType());
        if (tmpHitTargets.isEmpty()) {
            return;
        }
        int tmpMaxInfluenceNum = bulletTemplate.getMaxInfluenceNum();
        int tmpTargetHitCnt = bulletTemplate.getTargetHitCnt();

        CharacterEntity targetEntity;
        IntIntMap tmpHitRecord = bulletData.hitRecord;

        for (int i = 0, len = tmpHitTargets.size(); i < len; i++) {
            targetEntity = tmpHitTargets.get(i);
            if (tmpMaxInfluenceNum > 0 && tmpHitRecord.size() >= tmpMaxInfluenceNum) {
                tmpHitTargets.set(i, null);
                bulletData.setDestroy(true);
            } else {
                int tmpHitCnt = tmpHitRecord.getOrDefault(targetEntity.entityID, 0);

                if (tmpTargetHitCnt > 0 && tmpHitCnt >= tmpTargetHitCnt) {
                    tmpHitTargets.set(i, null);
                } else {
                    tmpHitRecord.addValue(targetEntity.entityID, 1);
                }
            }
        }

        parentLogic.attackFightLogic.doAttackAction(bulletData.bulletTemplate.getAttackID(), bulletTemplate.getAttackLevel(), bulletData.skillData, tmpHitTargets);
    }
}
