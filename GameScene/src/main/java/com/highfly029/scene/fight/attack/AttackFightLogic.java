package com.highfly029.scene.fight.attack;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbEntity;
import com.highfly029.common.template.attack.AttackFormulaParamTypeConst;
import com.highfly029.common.template.attack.AttackFormulaTemplate;
import com.highfly029.common.template.attack.AttackFormulaTypeConst;
import com.highfly029.common.template.attack.AttackMomentConst;
import com.highfly029.common.template.attack.AttackTemplate;
import com.highfly029.common.template.attack.AttackTypeConst;
import com.highfly029.common.template.attack.ScopeTypeConst;
import com.highfly029.common.template.attack.SelectTargetTypeConst;
import com.highfly029.common.template.attribute.AttributeConst;
import com.highfly029.common.template.commonType.OffsetDirectionConst;
import com.highfly029.common.template.global.GlobalConst;
import com.highfly029.common.template.skill.SkillTargetTypeConst;
import com.highfly029.common.template.state.StateConst;
import com.highfly029.common.utils.Vector3Utils;
import com.highfly029.scene.fight.BaseFightLogic;
import com.highfly029.scene.fight.FightAllLogic;
import com.highfly029.scene.fight.attribute.AttributeFightLogic;
import com.highfly029.scene.fight.buff.BuffFightLogic;
import com.highfly029.scene.fight.effect.EffectFightLogic;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.fight.entity.MoveEntity;
import com.highfly029.scene.fight.skill.SkillData;
import com.highfly029.scene.fight.skill.SkillFightLogic;
import com.highfly029.scene.fight.state.StateFightLogic;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.utils.BroadCastUtils;
import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.collection.IntFloatMap;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName AttackFightLogic
 * @Description 攻击逻辑
 * @Author liyunpeng
 **/
public class AttackFightLogic extends BaseFightLogic {
    //攻击模板
    private AttackTemplate attackTemplate;

    /*************************缓存数据**************************/
    //攻击目标列表
    public ObjectList<CharacterEntity> tmpAttackTargets = new ObjectList<>();
    /**
     * 扇形角度缓存
     */
    private final IntFloatMap cosDeltaDiv2 = new IntFloatMap();
    private final IntFloatMap sinDeltaDiv2 = new IntFloatMap();

    private final ObjectList<CharacterEntity> killList = new ObjectList<>(4);

    private final Vector3 tmpCenterPos = Vector3.create();
    private final Vector3 tmpVector1 = Vector3.create();
    private final Vector3 tmpVector2 = Vector3.create();
    private final Vector3 tmpVector3 = Vector3.create();

    /*************************缓存数据**************************/

    public AttackFightLogic(FightAllLogic logic) {
        super(logic);
    }

    /**
     * 执行攻击行为
     *
     * @param attackID
     * @param attackLevel
     * @param skillData
     * @param attackTargets
     */
    public void doAttackAction(int attackID, int attackLevel, SkillData skillData, ObjectList<CharacterEntity> attackTargets) {
        AttackTemplate tmpAttackTemplate = attackTemplate = ConfigTool.getAttackConfig().getAttackTemplate(attackID, attackLevel);
        if (tmpAttackTemplate == null) {
            SceneLoggerTool.gameLogger.error("doAttackAction template is null, attackID={}, attackLevel={}", attackID, attackLevel);
            return;
        }
        CharacterEntity attackSource = parentLogic.characterEntity;
        if (attackSource == null) {
            return;
        }

        ObjectList<CharacterEntity> tmpAttackTargets = this.tmpAttackTargets;
        tmpAttackTargets.clear();

        //主目标
        CharacterEntity mainTarget = null;
        int tmpInfluenceType = tmpAttackTemplate.getInfluenceType();

        if (attackTargets != null) {
            tmpAttackTargets.addAll(attackTargets);
        } else {
            Vector3 startPos = tmpVector1;
            Vector3 startDir = tmpVector2;
            switch (skillData.targetType) {
                case SkillTargetTypeConst.TargetEntityID -> {
                    mainTarget = (CharacterEntity) attackSource.scene.allEntity.get(skillData.targetEntityID);
                    if (mainTarget != null) {
                        if (parentLogic.skillFightLogic.checkInfluenceFlag(attackSource, mainTarget, tmpInfluenceType)) {
                            tmpAttackTargets.add(mainTarget);
                        }
                    }
                    //TODO 如果是luna med类似的技能 选中目标单位但是会附带攻击周围的单位、则在这里增加其他的附属单位
                }
                case SkillTargetTypeConst.TargetPosition -> {
                    startDir.copy(attackSource.direction);
                    startPos.copy(skillData.targetPos);
                }
                case SkillTargetTypeConst.TargetDirection -> {
                    startDir.copy(attackSource.direction);
                    startPos.copy(attackSource.curPos);
                }
            }
            //只有非单体攻击才查找范围内的所有目标
            if (skillData.targetType != SkillTargetTypeConst.TargetEntityID) {
                Vector3 retStartPos = tmpCenterPos;
                getScopeOffset(retStartPos, startPos, startDir, tmpAttackTemplate.getScopeParams(), 1);
                selectAttackTargets(tmpAttackTargets, attackSource, retStartPos, startDir, tmpAttackTemplate.getScopeParams(), 3, tmpAttackTemplate.getSelectParams(), tmpInfluenceType);
            }
        }


        doAttackMomentEffects(AttackMomentConst.SourceAttackBegin, null);

        //没有攻击目标则返回
        if (tmpAttackTargets.isEmpty()) {
            doAttackMomentEffects(AttackMomentConst.SourceAttackEnd, null);
            return;
        }

        PbEntity.S2CAttackDamage.Builder attackDamagePushBuilder = PbEntity.S2CAttackDamage.newBuilder();
        attackDamagePushBuilder.setAttackID(attackID);
        attackDamagePushBuilder.setAttackLevel(attackLevel);
        attackDamagePushBuilder.setAttackSourceID(attackSource.entityID);

        killList.clear();
        //攻击源
        AttributeFightLogic attackSourceAttribute = attackSource.fightAllLogic.attributeFightLogic;
        StateFightLogic attackSourceState = attackSource.fightAllLogic.stateFightLogic;
        BuffFightLogic attackSourceBuff = attackSource.fightAllLogic.buffFightLogic;
        SkillFightLogic attackSourceSkill = attackSource.fightAllLogic.skillFightLogic;

        //攻击目标
        CharacterEntity attackTarget;
        AttributeFightLogic attackTargetAttribute;
        StateFightLogic attackTargetState;

        boolean isTargetCanRebound = tmpAttackTemplate.isTargetCanRebound();
        boolean isFirst = true;
        for (int i = 0, len = tmpAttackTargets.size(); i < len; i++) {
            attackTarget = tmpAttackTargets.get(i);
            if (attackTarget == null) {
                continue;
            }
            attackTargetAttribute = attackTarget.fightAllLogic.attributeFightLogic;
            attackTargetState = attackTarget.fightAllLogic.stateFightLogic;

            if (attackTargetState.cantBeAttackTarget()) {
                continue;
            }
            if (!attackTargetState.isAlive()) {
                continue;
            }
            PbEntity.PbAttackData.Builder attackDataBuilder = PbEntity.PbAttackData.newBuilder();
            attackDataBuilder.setAttackTargetID(attackTarget.entityID);

            int lastHp = attackTargetAttribute.getHp();
            doAttackMomentEffects(AttackMomentConst.EachTargetBeforeCheckHit, attackTarget);
            //是否命中
            boolean isHit;
            if (tmpAttackTemplate.isAbsHit()) {
                isHit = true;
            } else {
                if (attackSourceState.getState(StateConst.AbsHit)) {
                    isHit = true;
                } else {
                    if (attackTargetState.getState(StateConst.AbsDodge)) {
                        isHit = false;
                    } else {
                        isHit = calculateIsHit(attackSourceAttribute.getAttribute(AttributeConst.HitRate), attackTargetAttribute.getAttribute(AttributeConst.Dodge));
                    }
                }
            }
            attackDataBuilder.setIsHit(isHit);
            if (isHit) {
                if (isFirst) {
                    doAttackMomentEffects(AttackMomentConst.SourceHitFirstTargetBeforeDamage, null);
                }
                if (mainTarget == attackTarget) {
                    doAttackMomentEffects(AttackMomentConst.SourceHitMainTargetBeforeDamage, null);
                }
                doAttackMomentEffects(AttackMomentConst.SourceHitEachTargetBeforeDamage, null);
                doAttackMomentEffects(AttackMomentConst.EachTargetBeHitBeforeDamage, attackTarget);

                //是否暴击
                boolean isCritical;
                if (tmpAttackTemplate.isAbsCritical()) {
                    isCritical = true;
                } else {
                    if (attackTargetState.getState(StateConst.AbsTenacity)) {
                        isCritical = false;
                    } else {
                        if (attackSourceState.getState(StateConst.AbsCritical)) {
                            isCritical = true;
                        } else {
                            isCritical = calculateIsCritical(attackSourceAttribute.getAttribute(AttributeConst.Critical), attackTargetAttribute.getAttribute(AttributeConst.Tenacity));
                        }
                    }
                }
                attackDataBuilder.setIsCritical(isCritical);

                int[] attackArray;
                int attackType;
                //攻击值
                int attackValue;
                //伤害值
                int damageValue;
                //最终伤害值
                int finalDamageValue = 0;
                //最终扣血值
                int finalSubHp = 0;
                //最终治疗值
                int finalHealHp = 0;
                //反弹伤害
                int reboundDamageValue = 0;
                if (tmpAttackTemplate.getAttacks() != null) {
                    for (int j = 0, jLen = tmpAttackTemplate.getAttacks().length; j < jLen; j++) {
                        attackArray = tmpAttackTemplate.getAttacks()[j];
                        attackType = attackArray[0];
                        attackValue = calculateAttackFormulaValue(attackArray[1], attackSource.fightAllLogic, attackTarget.fightAllLogic);

                        if (attackArray.length == 3) {
                            //增加攻击固定值
                            attackValue = attackValue + attackArray[2];
                        } else if (attackArray.length == 4) {
                            //增加攻击固定值和万分比
                            attackValue = (int) ((attackValue + attackArray[2]) * (1 + attackArray[3] * RandomUtils.RatioPercent));
                        }

                        switch (attackType) {
                            case AttackTypeConst.PhysicsAttack -> {
                                if (!attackTargetState.isPhysicalImmune()) {
                                    damageValue = calculateDamage(attackType, attackValue, isCritical, attackSourceAttribute, attackTargetAttribute);
                                    //TODO 物理护盾消耗
                                    finalDamageValue = damageValue;
                                    finalSubHp += damageCostHp(finalDamageValue, attackTargetAttribute);
                                }
                            }
                            case AttackTypeConst.MagicAttack -> {
                                if (!attackTargetState.isMagicImmune()) {
                                    damageValue = calculateDamage(attackType, attackValue, isCritical, attackSourceAttribute, attackTargetAttribute);
                                    //TODO 魔法护盾消耗
                                    finalDamageValue = damageValue;
                                    finalSubHp += damageCostHp(finalDamageValue, attackTargetAttribute);
                                }
                            }
                            case AttackTypeConst.HolyAttack -> {
                                if (!attackTargetState.isDamageImmune()) {
                                    damageValue = calculateDamage(attackType, attackValue, isCritical, attackSourceAttribute, attackTargetAttribute);
                                    finalDamageValue = damageValue;
                                    finalSubHp += damageCostHp(finalDamageValue, attackTargetAttribute);
                                }
                            }

                            //生命移除
                            case AttackTypeConst.SubHp -> {
                                finalDamageValue = attackValue;
                                finalSubHp += damageCostHp(finalDamageValue, attackTargetAttribute);
                            }
                            case AttackTypeConst.AddHp -> {
                                if (!attackTargetState.cantBeHeal()) {
//                                    finalDamageValue = attackValue;
                                    finalHealHp += attackValue;
                                    attackTargetAttribute.addOneAttribute(AttributeConst.Hp, finalDamageValue);
                                }
                            }
                            case AttackTypeConst.AddMp -> {
//                                finalDamageValue = attackValue;
                                attackTargetAttribute.addOneAttribute(AttributeConst.Mp, finalDamageValue);
                            }
                            case AttackTypeConst.SubMp -> {
//                                finalDamageValue = attackValue;
                                attackTargetAttribute.subOneAttribute(AttributeConst.Mp, finalDamageValue);
                            }
                            default -> SceneLoggerTool.gameLogger.error("doAttackAction attackType {} dont exist", attackType);
                        }

                        if (finalDamageValue > 0) {
                            //反弹伤害
                            if (isTargetCanRebound) {
                                reboundDamageValue += finalDamageValue;

                            }
                            PbCommon.PbKeyValue.Builder keyValueBuilder = PbCommon.PbKeyValue.newBuilder();
                            keyValueBuilder.setIntKey(attackType);
                            keyValueBuilder.setIntValue(finalDamageValue);
                            attackDataBuilder.addDamages(keyValueBuilder);
                        }
                    }
                }

                //处理反弹伤害
                if (isTargetCanRebound && reboundDamageValue > 0) {
                    int reboundSubHp = damageCostHp(reboundDamageValue, attackSourceAttribute);
                    attackSource.onDamage(reboundSubHp, attackTarget);
                    //自己死亡
                    if (attackSourceAttribute.getAttribute(AttributeConst.Hp) <= 0 && attackSourceState.isAlive()) {
                        attackSource.fightAllLogic.doDeathAction(attackTarget);
                        break;
                    }
                    attackDataBuilder.setReboundValue(reboundSubHp);
                }

                if (finalSubHp > 0) {
                    //通知目标受到伤害
                    attackTarget.onDamage(finalDamageValue, attackSource);
                }

                if (finalHealHp > 0) {
                    attackTarget.onHeal(finalHealHp, attackSource);
                }

                if (lastHp > 0 && attackTargetAttribute.getAttribute(AttributeConst.Hp) <= 0 && attackTargetState.isAlive()) {
                    killList.add(attackTarget);
                    attackDataBuilder.setIsKill(true);
                }
                if (isFirst) {
                    doAttackMomentEffects(AttackMomentConst.SourceHitFirstTargetAfterDamage, null);
                }
                if (mainTarget == attackTarget) {
                    doAttackMomentEffects(AttackMomentConst.SourceHitMainTargetAfterDamage, null);
                }
                doAttackMomentEffects(AttackMomentConst.SourceHitEachTargetAfterDamage, null);
                doAttackMomentEffects(AttackMomentConst.EachTargetBeHitAfterDamage, attackTarget);
            }
            if (isFirst == true) {
                isFirst = false;
            }
            attackDamagePushBuilder.addAttacks(attackDataBuilder);
        }

        //TODO 只有需要推送的伤害才发送、没必要发送给aoi内的所有玩家
        //包括自己和自己的宠物打出的伤害 攻击被闪避的伤害也要发送
        //自己和自己的宠物被打的伤害
        BroadCastUtils.broadCastToAOIPlayers(attackSource, PtCode.S2CAttackDamage, attackDamagePushBuilder.build(), true);

        if (killList.size() > 0) {
            SceneLoggerTool.gameLogger.info("killList size = {}", killList.size());
        }
        for (int i = 0, len = killList.size(); i < len; i++) {
            attackTarget = killList.get(i);
            if (!attackTarget.fightAllLogic.stateFightLogic.cantBeKillByDamage()) {
                doAttackMomentEffects(AttackMomentConst.SourceKill, null);
                attackTarget.fightAllLogic.doDeathAction(attackSource);
                doAttackMomentEffects(AttackMomentConst.TargetBeKill, attackTarget);
            }
        }

        doAttackMomentEffects(AttackMomentConst.SourceAttackEnd, null);
    }

    /**
     * 执行攻击时刻产生的效果
     *
     * @param attackMomentType
     */
    private void doAttackMomentEffects(byte attackMomentType, CharacterEntity targetEntity) {
        EffectFightLogic effectFightLogic = parentLogic.effectFightLogic;
        int[][] momentEffectArrays;
        if ((momentEffectArrays = attackTemplate.getAttackMomentEffects()) != null) {
            for (int[] params : momentEffectArrays) {
                if (params[0] == attackMomentType) {
                    if (RandomUtils.checkProbability(params[1])) {
                        effectFightLogic.doEffectAction(params, 2, targetEntity);
                    }
                }
            }
        }
    }

    /**
     * 伤害扣血
     *
     * @param finalDamageValue
     * @param attackTargetAttribute
     */
    private int damageCostHp(int finalDamageValue, AttributeFightLogic attackTargetAttribute) {
        if (finalDamageValue <= 0) {
            return 0;
        }
        int curHp = attackTargetAttribute.getAttribute(AttributeConst.Hp);
        int subHp = finalDamageValue > curHp ? curHp : finalDamageValue;
        attackTargetAttribute.subOneAttribute(AttributeConst.Hp, subHp);
        return subHp;
    }

    /**
     * 计算攻击造成的伤害
     *
     * @param attackType
     * @param attackValue
     * @param isCritical
     * @param attackSourceAttribute
     * @param attackTargetAttribute
     * @return
     */
    private int calculateDamage(int attackType, int attackValue, boolean isCritical, AttributeFightLogic attackSourceAttribute, AttributeFightLogic attackTargetAttribute) {
        float damageValue;
        int defence = 0;
        int damageAddPercent = 0;
        int damageSubPercent = 0;
        switch (attackType) {
            case AttackTypeConst.PhysicsAttack -> {
                defence = attackTargetAttribute.getAttribute(AttributeConst.PhysicsDefence);
                damageAddPercent = attackSourceAttribute.getAttribute(AttributeConst.PhysicsDamageAddPercent);
                damageSubPercent = attackTargetAttribute.getAttribute(AttributeConst.PhysicsDamageSubPercent);
            }
            case AttackTypeConst.MagicAttack -> {
                defence = attackTargetAttribute.getAttribute(AttributeConst.MagicDefence);
                damageAddPercent = attackSourceAttribute.getAttribute(AttributeConst.MagicDamageAddPercent);
                damageSubPercent = attackTargetAttribute.getAttribute(AttributeConst.MagicDamageSubPercent);
            }
        }
        damageValue = attackValue - defence;
        if (isCritical) {
            damageValue = calculateCriticalDamage(damageValue, attackSourceAttribute.getAttribute(AttributeConst.CriticalDamageAddPercent));
        }
        damageValue = calculateRandomDamage(damageValue, attackSourceAttribute.getAttribute(AttributeConst.DamageRandomPercent));

        if (attackType != AttackTypeConst.HolyAttack) {
            //伤害增加
            if (damageAddPercent != 0) {
                damageValue *= (1 + damageAddPercent * RandomUtils.RatioPercent);
            }
            //伤害减少
            if (damageSubPercent != 0) {
                damageValue *= (1 - damageSubPercent * RandomUtils.RatioPercent);
            }
        }

        //总伤害增加
        int allDamageAddPercent = attackSourceAttribute.getAttribute(AttributeConst.AllDamageAddPercent);
        if (allDamageAddPercent > 0) {
            damageValue *= (1 + allDamageAddPercent * RandomUtils.RatioPercent);
        }

        //总伤害减少
        int allDamageSubPercent = attackTargetAttribute.getAttribute(AttributeConst.AllDamageSubPercent);
        if (allDamageSubPercent > 0) {
            damageValue *= (1 - allDamageSubPercent * RandomUtils.RatioPercent);
        }
        if (damageValue < 0) {
            damageValue = 0f;
        }
        return (int) damageValue;
    }

    /**
     * 计算伤害随机浮动后的值
     *
     * @param damageValue
     * @param damageRandomPercent
     * @return
     */
    private float calculateRandomDamage(float damageValue, int damageRandomPercent) {
        if (damageRandomPercent != 0) {
            float percent = RandomUtils.randomInt(damageRandomPercent) * RandomUtils.RatioPercent;
            if (RandomUtils.randomBoolean()) {
                percent = -percent;
            }
            damageValue *= (1 + percent);
        }
        return damageValue;
    }

    /**
     * 计算暴击伤害
     *
     * @param damageValue
     * @param criticalDamageAddPercent
     * @return
     */
    private float calculateCriticalDamage(float damageValue, int criticalDamageAddPercent) {
        float ratio = (GlobalConst.BaseCriticalDamage + criticalDamageAddPercent) * RandomUtils.RatioPercent;
        if (ratio < 1) {
            ratio = 1;
        }
        return damageValue * ratio;
    }

    /**
     * 根据攻击计算公式计算攻击
     *
     * @param attackFormulaID
     * @param attackSource
     * @param attackTarget
     * @return
     */
    private int calculateAttackFormulaValue(int attackFormulaID, FightAllLogic attackSource, FightAllLogic attackTarget) {
        AttackFormulaTemplate attackFormulaTemplate = ConfigTool.getAttackConfig().getAttackFormulaTemplate(attackFormulaID);
        switch (attackFormulaTemplate.getAttackFormulaType()) {
            case AttackFormulaTypeConst.Element1 -> {
                return getAttackFormulaParamValue(attackFormulaTemplate.getParam1(), attackSource, attackTarget);
            }
            case AttackFormulaTypeConst.Element2 -> {
                return (int) (getAttackFormulaParamValue(attackFormulaTemplate.getParam1(), attackSource, attackTarget) *
                        (1 + getAttackFormulaParamValue(attackFormulaTemplate.getParam2(), attackSource, attackTarget) * RandomUtils.RatioPercent));
            }
            case AttackFormulaTypeConst.Element3 -> {
                return (int) (getAttackFormulaParamValue(attackFormulaTemplate.getParam1(), attackSource, attackTarget) *
                        (1 + getAttackFormulaParamValue(attackFormulaTemplate.getParam2(), attackSource, attackTarget) * RandomUtils.RatioPercent)) +
                        getAttackFormulaParamValue(attackFormulaTemplate.getParam3(), attackSource, attackTarget);
            }
            case AttackFormulaTypeConst.Element4 -> {
                return (int) ((getAttackFormulaParamValue(attackFormulaTemplate.getParam1(), attackSource, attackTarget) *
                        (1 + getAttackFormulaParamValue(attackFormulaTemplate.getParam2(), attackSource, attackTarget) * RandomUtils.RatioPercent) + getAttackFormulaParamValue(attackFormulaTemplate.getParam3(), attackSource, attackTarget)) *
                        (1 + getAttackFormulaParamValue(attackFormulaTemplate.getParam4(), attackSource, attackTarget) * RandomUtils.RatioPercent));
            }
            case AttackFormulaTypeConst.TwoPlus -> {
                return getAttackFormulaParamValue(attackFormulaTemplate.getParam1(), attackSource, attackTarget) + getAttackFormulaParamValue(attackFormulaTemplate.getParam2(), attackSource, attackTarget);
            }
            case AttackFormulaTypeConst.Ratio -> {
                return (int) (getAttackFormulaParamValue(attackFormulaTemplate.getParam1(), attackSource, attackTarget) * getAttackFormulaParamValue(attackFormulaTemplate.getParam2(), attackSource, attackTarget) * RandomUtils.RatioPercent);
            }
        }
        return 0;
    }

    /**
     * 根据伤害公式参数的类型，计算参数值
     *
     * @param attackParam
     * @param attackSource
     * @param attackTarget
     * @return
     */
    private int getAttackFormulaParamValue(int[] attackParam, FightAllLogic attackSource, FightAllLogic attackTarget) {
        int value = 0;
        switch (attackParam[0]) {
            case AttackFormulaParamTypeConst.ConstValue -> {
                value = attackParam[1];
            }
            case AttackFormulaParamTypeConst.SourceAttribute -> {
                if (attackSource != null) {
                    value = attackSource.attributeFightLogic.getAttribute((short) attackParam[1]);
                }
            }
            case AttackFormulaParamTypeConst.TargetAttribute -> {
                if (attackTarget != null) {
                    value = attackTarget.attributeFightLogic.getAttribute((short) attackParam[1]);
                }
            }
            case AttackFormulaParamTypeConst.SourceLevel -> {
                if (attackSource != null) {
                    value = attackSource.characterEntity.level;
                }
            }
            case AttackFormulaParamTypeConst.TargetLevel -> {
                if (attackTarget != null) {
                    value = attackTarget.characterEntity.level;
                }
            }
        }
        return value;
    }

    /**
     * 计算是否暴击
     *
     * @param critical
     * @param tenacity
     * @return
     */
    private boolean calculateIsCritical(int critical, int tenacity) {
        int ratio = critical - tenacity + GlobalConst.BaseCritical;
        if (ratio <= 0) {
            return false;
        }
        if (ratio >= RandomUtils.RatioUnit) {
            return true;
        }
        return RandomUtils.randomInt(RandomUtils.RatioUnit) < ratio;
    }

    /**
     * 计算是否命中
     *
     * @param hitRate
     * @param dodge
     * @return
     */
    private boolean calculateIsHit(int hitRate, int dodge) {
        int ratio = hitRate - dodge + GlobalConst.BaseHitRate;
        if (ratio <= 0) {
            return false;
        }
        if (ratio >= RandomUtils.RatioUnit) {
            return true;
        }
        return RandomUtils.randomInt(RandomUtils.RatioUnit) < ratio;
    }

    /**
     * 获取偏移后的起始位置
     *
     * @param retStartPos
     * @param startPos
     * @param startDir
     * @param scopeParams
     * @param startIndex  起始索引
     */
    public void getScopeOffset(Vector3 retStartPos, Vector3 startPos, Vector3 startDir, int[] scopeParams, int startIndex) {
        if (startDir == null || scopeParams == null) {
            return;
        }
        int[] tmpParams = scopeParams;
        byte offsetDir = (byte) tmpParams[startIndex];
        int offset = tmpParams[startIndex + 1];

        //起始位置偏移量
        if (offset != 0) {
            if (offsetDir == OffsetDirectionConst.PositiveDirection) {
                Vector3Utils.moveTowardsPos(retStartPos, startPos, startDir, false, offset);
            } else if (offsetDir == OffsetDirectionConst.NegativeDirection) {
                retStartPos.copy(startDir);
                Vector3Utils.negative(retStartPos, retStartPos);
                Vector3Utils.moveTowardsPos(retStartPos, startPos, retStartPos, false, offset);
            } else {
                SceneLoggerTool.gameLogger.error("OffsetDirectionConst not exist {}", offsetDir);
            }
        } else {
            retStartPos.copy(startPos);
        }
    }

    /**
     * 选择出视野内攻击目标
     *
     * @param targets
     * @param attackSource
     * @param startPos
     * @param startDir
     * @param scopeParams
     * @param scopeStartIndex 起始索引
     * @param selectParams
     * @param influenceType   只选择影响类型的单位
     */
    public void selectAttackTargets(ObjectList<CharacterEntity> targets, CharacterEntity attackSource, Vector3 startPos, Vector3 startDir,
                                    int[] scopeParams, int scopeStartIndex, int[] selectParams, int influenceType) {
        SkillFightLogic skillFightLogic = parentLogic.skillFightLogic;

        int[] tmpParams = scopeParams;

        if (startPos == null || startDir == null) {
            return;
        }
        tmpCenterPos.copy(startPos);


        byte scopeType = (byte) tmpParams[scopeStartIndex];
        byte tmpSelectTargetType = 0;
        int[] tmpSelectParams = selectParams;
        if (tmpSelectParams != null) {
            tmpSelectTargetType = (byte) tmpSelectParams[0];
        }

        CharacterEntity tmpSelectEntity = null;
        switch (scopeType) {
            case ScopeTypeConst.Circle -> {
                int radius = tmpParams[scopeStartIndex + 1];
                int disSquare = radius * radius;

                IntObjectMap<MoveEntity> allMoveEntity = attackSource.allMoveEntityInAOI;
                int freeValue = allMoveEntity.getFreeValue();
                int[] keys = allMoveEntity.getKeys();
                MoveEntity[] values = allMoveEntity.getValues();
                MoveEntity value;
                int key;
                for (int i = 0, len = keys.length; i < len; i++) {
                    if ((key = keys[i]) != freeValue) {
                        if ((value = values[i]) != null) {
                            if (value.isCharacterEntity()) {
                                CharacterEntity characterEntity = (CharacterEntity) value;
                                if ((characterEntity.tmpDistanceSquare = Vector3Utils.distance3DSquare(characterEntity.curPos, tmpCenterPos)) <= disSquare) {
                                    if (tmpSelectTargetType == SelectTargetTypeConst.SelectRandomOne) {
                                        if (tmpSelectEntity == null && skillFightLogic.checkInfluenceFlag(attackSource, characterEntity, influenceType)) {
                                            tmpSelectEntity = characterEntity;
                                            break;
                                        }
                                    } else if (tmpSelectTargetType == SelectTargetTypeConst.SelectNearestOne) {
                                        if (tmpSelectEntity == null || tmpSelectEntity.tmpDistanceSquare > characterEntity.tmpDistanceSquare) {
                                            if (skillFightLogic.checkInfluenceFlag(attackSource, characterEntity, influenceType)) {
                                                tmpSelectEntity = characterEntity;
                                            }
                                        }
                                    } else {
                                        if (skillFightLogic.checkInfluenceFlag(attackSource, characterEntity, influenceType)) {
                                            targets.add(characterEntity);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            case ScopeTypeConst.Rectangle -> {
                int width = tmpParams[scopeStartIndex + 1];
                int length = tmpParams[scopeStartIndex + 2];
                float widthDiv2Square;
                widthDiv2Square = (widthDiv2Square = width / 2) * widthDiv2Square;
                float lengthSquare = length * length;
                //tmpVector1 = 终点向量
                Vector3Utils.moveTowardsPos(tmpVector1, tmpCenterPos, startDir, false, length);
                //tmpVector1 = 起点到终点的方向向量
                Vector3Utils.sub3D(tmpVector1, tmpVector1, tmpCenterPos);
                float dotValue;
                float ratio;

                IntObjectMap<MoveEntity> allMoveEntity = attackSource.allMoveEntityInAOI;
                int freeValue = allMoveEntity.getFreeValue();
                int[] keys = allMoveEntity.getKeys();
                MoveEntity[] values = allMoveEntity.getValues();
                MoveEntity value;
                int key;
                for (int i = 0, len = keys.length; i < len; i++) {
                    if ((key = keys[i]) != freeValue) {
                        if ((value = values[i]) != null) {
                            if (value.isCharacterEntity()) {
                                CharacterEntity characterEntity = (CharacterEntity) value;
                                //tmpVector2 = 起点到目标点的方向向量
                                Vector3Utils.sub3D(tmpVector2, characterEntity.curPos, tmpCenterPos);
                                dotValue = Vector3Utils.dot(tmpVector1, tmpVector2);
                                if (dotValue >= 0 && dotValue < lengthSquare) {
                                    ratio = dotValue / lengthSquare;
                                    //tmpVector2 = tmpVector1
                                    tmpVector2.copy(tmpVector1);
                                    tmpVector2.multiply(ratio);
                                    Vector3Utils.add3D(tmpVector2, tmpVector2, tmpCenterPos);
                                    //TODO 这里的tmpDistanceSquare 应该不对
                                    if ((characterEntity.tmpDistanceSquare = Vector3Utils.distance3DSquare(tmpVector2, characterEntity.curPos)) <= widthDiv2Square) {
                                        if (tmpSelectTargetType == SelectTargetTypeConst.SelectRandomOne) {
                                            if (tmpSelectEntity == null && skillFightLogic.checkInfluenceFlag(attackSource, characterEntity, influenceType)) {
                                                tmpSelectEntity = characterEntity;
                                                break;
                                            }
                                        } else if (tmpSelectTargetType == SelectTargetTypeConst.SelectNearestOne) {
                                            if (tmpSelectEntity == null || tmpSelectEntity.tmpDistanceSquare > characterEntity.tmpDistanceSquare) {
                                                if (skillFightLogic.checkInfluenceFlag(attackSource, characterEntity, influenceType)) {
                                                    tmpSelectEntity = characterEntity;
                                                }
                                            }
                                        } else {
                                            if (skillFightLogic.checkInfluenceFlag(attackSource, characterEntity, influenceType)) {
                                                targets.add(characterEntity);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //另一种方案 矩形坐标系转换
            }
            case ScopeTypeConst.Sector -> {
                int radius = tmpParams[scopeStartIndex + 1];
                int disSquare = radius * radius;
                int delta = tmpParams[scopeStartIndex + 2];


//                Vector3 tmpVector1 = this.tmpVector1;
//
//                float cosDeltaDiv2 = getCosDeltaDiv2(delta);
//
//                IntObjectMap<MoveEntity> allMoveEntity = attackSource.allMoveEntityInAOI;
//                int freeValue = allMoveEntity.getFreeValue();
//                int[] keys = allMoveEntity.getKeys();
//                MoveEntity[] values = allMoveEntity.getValues();
//                MoveEntity value;
//                int key;
//                for (int i = 0, len = keys.length; i < len; i++) {
//                    if ((key = keys[i]) != freeValue) {
//                        if ((value = values[i]) != null) {
//                            if (value.isCharacterEntity()) {
//                                CharacterEntity characterEntity = (CharacterEntity) value;
//                                if (((characterEntity).tmpDistanceSquare = Vector3Utils.distance3DSquare(characterEntity.curPos, tmpCenterPos)) <= disSquare) {
//                                    Vector3Utils.sub3D(tmpVector1, characterEntity.curPos, tmpCenterPos);
//                                    tmpVector1.normalise();
//                                    if (Vector3Utils.dot(tmpVector1, startDir) > cosDeltaDiv2) {
//                                        if (tmpSelectTargetType == SelectTargetTypeConst.SelectRandomOne) {
//                                            if (tmpSelectEntity == null && skillFightLogic.checkInfluenceFlag(attackSource, characterEntity, influenceType)) {
//                                                tmpSelectEntity = characterEntity;
//                                                break;
//                                            }
//                                        } else if (tmpSelectTargetType == SelectTargetTypeConst.SelectNearestOne) {
//                                            if (tmpSelectEntity == null || tmpSelectEntity.tmpDistanceSquare > characterEntity.tmpDistanceSquare) {
//                                                if (skillFightLogic.checkInfluenceFlag(attackSource, characterEntity, influenceType)) {
//                                                    tmpSelectEntity = characterEntity;
//                                                }
//                                            }
//                                        } else {
//                                            if (skillFightLogic.checkInfluenceFlag(attackSource, characterEntity, influenceType)) {
//                                                targets.add(characterEntity);
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }

                //另一种方案 性能更好
                Vector3 sectorStart = tmpVector1;
                Vector3 sectorEnd = tmpVector2;
                float cosDeltaDiv2 = getCosDeltaDiv2(delta);
                float sinDeltaDiv2 = getSinDeltaDiv2(delta);
                float xCos = startDir.x * cosDeltaDiv2;
                float zSin = startDir.z * sinDeltaDiv2;
                float xSin = startDir.x * sinDeltaDiv2;
                float zCos = startDir.z * cosDeltaDiv2;
                sectorStart.set(xCos + zSin, startDir.y, -xSin + zCos);
                sectorEnd.set(xCos - zSin, startDir.y, xSin + zCos);

                IntObjectMap<MoveEntity> allMoveEntity = attackSource.allMoveEntityInAOI;
                int freeValue = allMoveEntity.getFreeValue();
                int[] keys = allMoveEntity.getKeys();
                MoveEntity[] values = allMoveEntity.getValues();
                MoveEntity value;
                Vector3 tmp = tmpVector3;
                int key;
                for (int i = 0, len = keys.length; i < len; i++) {
                    if ((key = keys[i]) != freeValue) {
                        if ((value = values[i]) != null) {
                            if (value.isCharacterEntity()) {
                                CharacterEntity characterEntity = (CharacterEntity) value;
                                if (Vector3Utils.distance3DSquare(tmpCenterPos, characterEntity.curPos) <= disSquare) {
                                    Vector3Utils.sub3D(tmp, characterEntity.curPos, tmpCenterPos);
                                    if (Vector3Utils.isClockwise2D(sectorEnd, tmp) && !Vector3Utils.isClockwise2D(sectorStart, tmp)) {
                                        if (tmpSelectTargetType == SelectTargetTypeConst.SelectRandomOne) {
                                            if (tmpSelectEntity == null && skillFightLogic.checkInfluenceFlag(attackSource, characterEntity, influenceType)) {
                                                tmpSelectEntity = characterEntity;
                                                break;
                                            }
                                        } else if (tmpSelectTargetType == SelectTargetTypeConst.SelectNearestOne) {
                                            if (tmpSelectEntity == null || tmpSelectEntity.tmpDistanceSquare > characterEntity.tmpDistanceSquare) {
                                                if (skillFightLogic.checkInfluenceFlag(attackSource, characterEntity, influenceType)) {
                                                    tmpSelectEntity = characterEntity;
                                                }
                                            }
                                        } else {
                                            if (skillFightLogic.checkInfluenceFlag(attackSource, characterEntity, influenceType)) {
                                                targets.add(characterEntity);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        switch (tmpSelectTargetType) {
            case SelectTargetTypeConst.SelectRandom -> {
                int num = tmpSelectParams[1];
                int targetNum = targets.size();
                if (targetNum > num) {
                    int subNum = targetNum - num;
                    //删除最后的实体，提高效率
                    while (subNum > 0) {
                        targets.remove(--targetNum);
                        --subNum;
                    }
                }
            }
            case SelectTargetTypeConst.SelectNearest -> {
                int num = tmpSelectParams[1];
                int targetNum = targets.size();
                if (targetNum > num) {
                    int subNum = targetNum - num;
                    targets.sort(this::nearEntityComparator);
                    //排序后删除最后的实体，提高效率
                    while (subNum > 0) {
                        targets.remove(--targetNum);
                        --subNum;
                    }
                }
            }
            case SelectTargetTypeConst.SelectNearestOne -> {
                if (tmpSelectEntity != null) {
                    targets.add(tmpSelectEntity);
                }
            }
            case SelectTargetTypeConst.SelectRandomOne -> {
                if (tmpSelectEntity != null) {
                    targets.add(tmpSelectEntity);
                }
            }
        }

    }

    /**
     * 距离比较器，从小到大排序
     *
     * @param a
     * @param b
     * @return
     */
    private int nearEntityComparator(CharacterEntity a, CharacterEntity b) {
        float tmpA;
        float tmpB;
        if ((tmpA = a.tmpDistanceSquare) < (tmpB = b.tmpDistanceSquare)) {
            return -1;
        }
        if (tmpA > tmpB) {
            return 1;
        }
        return 0;
    }

    /**
     * 获取扇形弧度/2 的cos值
     *
     * @param delta
     * @return
     */
    private float getCosDeltaDiv2(int delta) {
        float value;
        if (cosDeltaDiv2.containsKey(delta)) {
            value = cosDeltaDiv2.get(delta);
        } else {
            value = (float) Math.cos(delta * Math.PI / 180 / 2);
            cosDeltaDiv2.put(delta, value);
        }
        return value;
    }

    /**
     * 获取扇形弧度/2 的sin值
     *
     * @param delta
     * @return
     */
    private float getSinDeltaDiv2(int delta) {
        float value;
        if (sinDeltaDiv2.containsKey(delta)) {
            value = sinDeltaDiv2.get(delta);
        } else {
            value = (float) Math.sin(delta * Math.PI / 180 / 2);
            sinDeltaDiv2.put(delta, value);
        }
        return value;
    }
}
