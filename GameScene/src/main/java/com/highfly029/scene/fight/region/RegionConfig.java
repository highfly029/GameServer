package com.highfly029.scene.fight.region;

import com.highfly029.common.template.regionEntity.RegionEntityTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName RegionConfig
 * @Description 区域配置数据
 * @Author liyunpeng
 **/
public class RegionConfig extends BaseGlobalConfig {
    private final IntObjectMap<RegionEntityTemplate> templates = new IntObjectMap<>(RegionEntityTemplate[]::new);

    @Override
    public void check(boolean isHotfix) {
        //TODO 检测dir必须为单位向量
    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(RegionEntityTemplate.class);
        RegionEntityTemplate template;
        for (int i = 0, size = list.size(); i < size; i++) {
            template = (RegionEntityTemplate) list.get(i);
            templates.put(template.getRegionEntityID(), template);
        }
    }

    /**
     * 获取区域模版
     *
     * @param regionID
     * @return
     */
    public RegionEntityTemplate getRegionEntityTemplate(int regionID) {
        return templates.get(regionID);
    }
}
