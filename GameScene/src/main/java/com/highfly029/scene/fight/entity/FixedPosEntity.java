package com.highfly029.scene.fight.entity;

import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbEntity;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.scene.utils.BroadCastUtils;
import com.highfly029.scene.utils.PbSceneUtils;
import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName FixedPosEntity
 * @Description 固定位置实例 但是位置可被强制改变
 * @Author liyunpeng
 **/
public abstract class FixedPosEntity extends Entity {

    public FixedPosEntity(SceneEntityWorld sceneEntityWorld) {
        super(sceneEntityWorld);
    }

    @Override
    public boolean isFixedPosEntity() {
        return true;
    }

    /**
     * 如果是不可移动的entity、推送给全场景的玩家创建
     */
    public void onAddEntityToScene() {
        PbEntity.S2CAddFixedPosEntity.Builder builder = PbEntity.S2CAddFixedPosEntity.newBuilder();
        builder.setEntity(PbSceneUtils.entityObj2Pb(this));
        BroadCastUtils.broadCastToScenePlayers(PtCode.S2CAddFixedPosEntity, builder.build(), scene);
    }

    /**
     * 如果是不可移动的entity、推送给全场景的玩家删除
     */
    public void onDelEntityFromScene() {
        PbEntity.S2CDelFixedPosEntity.Builder builder = PbEntity.S2CDelFixedPosEntity.newBuilder();
        builder.setEntityID(entityID);
        BroadCastUtils.broadCastToScenePlayers(PtCode.S2CDelFixedPosEntity, builder.build(), scene);
    }

    /**
     * 坐标变化
     */
    public void onPosChanged() {
        PbEntity.S2CFixedPosEntityChangePos.Builder builder = PbEntity.S2CFixedPosEntityChangePos.newBuilder();
        builder.setEntityID(entityID);
        builder.setPos(PbCommonUtils.vector3Obj2Pb(curPos));
        BroadCastUtils.broadCastToScenePlayers(PtCode.S2CFixedPosEntityChangePos, builder.build(), scene);
    }
}
