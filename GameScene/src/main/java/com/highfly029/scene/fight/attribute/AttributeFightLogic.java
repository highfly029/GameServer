package com.highfly029.scene.fight.attribute;

import com.highfly029.common.template.attribute.AttributeConst;
import com.highfly029.common.template.attribute.AttributeFormulaConst;
import com.highfly029.common.template.attribute.AttributePlusTypeConst;
import com.highfly029.common.template.global.GlobalConst;
import com.highfly029.scene.fight.BaseFightLogic;
import com.highfly029.scene.fight.FightAllLogic;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.collection.IntIntMap;

/**
 * @ClassName AttributeFightLogic
 * @Description 属性逻辑
 * @Author liyunpeng
 **/
public class AttributeFightLogic extends BaseFightLogic {
    //属性值
    private int[] attributes;
    //标记每个属性ID是否修改过
    private boolean[] modifiedAttributes;
    //标记整个属性模块是否修改过
    private boolean isModified;

    //是否需要推送
    private boolean isNeedSend;
    //是否需要通知
    private boolean isNeedDispatch;
    //是否需要自增
    private boolean isNeedIncrease;
    //是否需要自增数组
    private boolean[] isNeedIncreaseArray;

    //上一次刷新时的属性值
    private int[] lastAttributes;
    //上一次dispatch的属性ID
    private short[] lastDispatchAttrIDArray;
    //上一次dispatch的属性值
    private int[] lastDispatchAttrValueArray;
    //上一次当前最大值
    private int[] lastCurrentMaxValueArray;
    //自增属性时间累积
    private int autoIncreasePassTime;

    /************* 缓存 *****************/
    private final IntIntMap tmpSendSelfMap = new IntIntMap();
    private final IntIntMap tmpSendOtherMap = new IntIntMap();
    private final IntIntMap tmpSendMasterMap = new IntIntMap();

    public AttributeFightLogic(FightAllLogic fightAllLogic) {
        super(fightAllLogic);
        init();
    }

    /**
     * 初始化
     */
    private void init() {
        AttributeConfig attributeConfig = ConfigTool.getAttributeConfig();
        int arraySize = attributeConfig.getAttrArraySize();

        attributes = new int[arraySize];
        lastAttributes = new int[arraySize];
        modifiedAttributes = new boolean[arraySize];

        isNeedIncreaseArray = new boolean[attributeConfig.getIncreaseNecessaryList().length];
        lastDispatchAttrIDArray = new short[attributeConfig.getDispatchNecessaryList().length];
        lastDispatchAttrValueArray = new int[attributeConfig.getDispatchNecessaryList().length];

        lastCurrentMaxValueArray = new int[attributeConfig.getCurrentMaxList().length];
    }

    /**
     * 加一个属性
     *
     * @param attrID
     * @param attrValue
     */
    public void addOneAttribute(short attrID, int attrValue) {
        if (attrValue <= 0) {
            SceneLoggerTool.gameLogger.error("addOneAttribute attrID={}, invalid attrValue={}", attrID, attrValue);
            return;
        }
        int preValue = attributes[attrID];
        int value = preValue + attrValue;
        int maxValue;
        //如果存在最大值
        if ((maxValue = ConfigTool.getAttributeConfig().getMaxValueArray()[attrID]) > 0) {
            //如果已经是最大值、不增加
            if (preValue >= maxValue) {
                return;
            } else {
                value = value >= maxValue ? maxValue : value;
            }
        }
        modifyOneAttribute(attrID, value);
    }

    /**
     * 减一个属性
     *
     * @param attrID
     * @param attrValue
     */
    public void subOneAttribute(short attrID, int attrValue) {
        if (attrValue <= 0) {
            SceneLoggerTool.gameLogger.error("subOneAttribute attrID={}, invalid attrValue={}", attrID, attrValue);
            return;
        }
        int preValue;
        //如果已经=0、则直接返回、不能出现负数属性
        if ((preValue = attributes[attrID]) <= 0) {
            return;
        }
        int value = preValue - attrValue;
        value = value >= 0 ? value : 0;
        modifyOneAttribute(attrID, value);
    }

    /**
     * 修改单个属性值
     *
     * @param attrID
     * @param attrValue
     */
    private void modifyOneAttribute(short attrID, int attrValue) {
        if (ConfigTool.getAttributeConfig().getFormulaArray()[attrID] != null) {
            SceneLoggerTool.gameLogger.error("modifyOneAttribute invalid attrID={} cant modify, attrValue={}", attrID, attrValue);
            return;
        }
        attributes[attrID] = attrValue;
        markAttributeModify(attrID);
    }

    /**
     * 标记属性为修改过状态
     *
     * @param attrID
     */
    private void markAttributeModify(short attrID) {
        AttributeConfig attributeConfig = ConfigTool.getAttributeConfig();
        //如果是最大属性变化、当前属性也有可能变化
        if (attributeConfig.getMaxBoolArray()[attrID]) {
            int currentAttrID = attributeConfig.getMaxToCurrentArray()[attrID];
            modifiedAttributes[currentAttrID] = true;
            isModified = true;
        }
        //如果是当前属性变化、可能需要控制当前属性的最大值或者最小值
        if (attributeConfig.getCurrentBoolArray()[attrID]) {
            modifiedAttributes[attrID] = true;
            isModified = true;
        }
        if (attributeConfig.getMaybeSendBoolArray()[attrID]) {
            isNeedSend = true;
        }
        if (attributeConfig.getDispatchBoolArray()[attrID]) {
            isNeedDispatch = true;
        }

        short[] otherAttrArray = attributeConfig.getInfluenceOtherAttrArray()[attrID];
        if (otherAttrArray != null) {
            for (short otherID : otherAttrArray) {
                modifiedAttributes[otherID] = true;
                isModified = true;
                markAttributeModify(otherID);
            }
        }

        //种族属性影响
        int race = parentLogic.characterEntity.race;
        int[] raceArray;
        if (attributeConfig.getAttributeInfluenceArray()[race] != null && (raceArray = attributeConfig.getAttributeInfluenceArray()[race][attrID]) != null) {
            for (int influenceAttrID : raceArray) {
                modifiedAttributes[influenceAttrID] = true;
                isModified = true;
                markAttributeModify((short) influenceAttrID);
            }
        }
    }

    /**
     * 获取最新属性
     *
     * @param attrID
     * @return
     */
    public int getAttribute(short attrID) {
        if (modifiedAttributes[attrID]) {
            calOneAttribute(attrID);
        }
        return attributes[attrID];
    }

    /**
     * 获取属性的真实值，如果是万分比的则/10000
     *
     * @param attrID
     * @return
     */
    public float getAttributeReal(short attrID) {
        if (modifiedAttributes[attrID]) {
            calOneAttribute(attrID);
        }
        return ConfigTool.getAttributeConfig().getPercentBoolArray()[attrID] ? attributes[attrID] * RandomUtils.RatioPercent : attributes[attrID];
    }

    /**
     * 获取万分比属性值
     *
     * @param attrID
     * @return
     */
    public float getPercentAttribute(short attrID) {
        if (modifiedAttributes[attrID]) {
            calOneAttribute(attrID);
        }
        if (ConfigTool.getAttributeConfig().getPercentBoolArray()[attrID]) {
            return attributes[attrID] * RandomUtils.RatioPercent;
        } else {
            return 0f;
        }
    }

    /**
     * 计算单个属性
     *
     * @param attrID
     */
    private void calOneAttribute(short attrID) {
        AttributeConfig attributeConfig = ConfigTool.getAttributeConfig();
        int[] formula;
        int[] attributes = this.attributes;
        if ((formula = attributeConfig.getFormulaArray()[attrID]) != null) {
            attributes[attrID] = calculateFormulaResult(formula);
            //种族对属性的影响 只有带公式的属性生效
            int race = parentLogic.characterEntity.race;
            int[][] raceArray;
            if (attributeConfig.getAttributeBeInfluenceArray()[race] != null && (raceArray = attributeConfig.getAttributeBeInfluenceArray()[race][attrID]) != null) {
                for (int[] params : raceArray) {
                    int addValue = calculateAddRaceResult(params);
                    attributes[attrID] += addValue;
                }
            }
        }

        //如果是当前属性变化
        if (attributeConfig.getCurrentBoolArray()[attrID]) {
            short maxAttrID = attributeConfig.getCurrentToMaxArray()[attrID];

            int maxValue = getAttribute(maxAttrID);
            //最大值有可能会改变当前值、所以先获取最大值、再获取当前值
            int currentValue = attributes[attrID];

            if (currentValue > maxValue) {
                attributes[attrID] = maxValue;
            }

            if (currentValue < 0) {
                attributes[attrID] = 0;
            }
        }

        //如果是最大值变化 则影响当前值
        if (attributeConfig.getMaxBoolArray()[attrID]) {
            short currentAttrID = attributeConfig.getMaxToCurrentArray()[attrID];
            int index = attributeConfig.getCurrentMaxIndex()[attrID];
            int oldMax = lastCurrentMaxValueArray[index];
            int newMax = attributes[attrID];
            if (oldMax != newMax) {
                calCurrentWhenMaxChange(currentAttrID, oldMax, newMax);
                lastCurrentMaxValueArray[index] = newMax;
            }
        }
        modifiedAttributes[attrID] = false;
    }

    /**
     * 计算所有变化的属性，不推送
     */
    private void calAllAttributes() {
        isModified = false;

        boolean[] modifiedArray = modifiedAttributes;
        for (int i = 0, len = modifiedArray.length; i < len; i++) {
            short attrID = (short) i;
            if (modifiedArray[attrID]) {
                calOneAttribute(attrID);
            }
        }
    }

    /**
     * 检测自增属性
     */
    private void checkIncreaseAttribute() {
        AttributeConfig attributeConfig = ConfigTool.getAttributeConfig();
        isNeedIncrease = false;
        short maxAttrID;
        short currentAttrID;
        short increaseAttrID;
        short[] increaseList = attributeConfig.getIncreaseNecessaryList();
        boolean[] maxArray = attributeConfig.getMaxBoolArray();
        short[] increaseToCurrentArray = attributeConfig.getIncreaseToCurrentArray();
        short[] currentToMaxArray = attributeConfig.getCurrentToMaxArray();

        for (int i = 0, len = increaseList.length; i < len; i++) {
            increaseAttrID = increaseList[i];
            //有自增属性值
            if (getAttribute(increaseAttrID) > 0) {
                currentAttrID = increaseToCurrentArray[i];
                maxAttrID = currentToMaxArray[currentAttrID];
                //如果自增属性没有最大值或者有最大值但没达到
                if (!maxArray[maxAttrID] || getAttribute(currentAttrID) < getAttribute(maxAttrID)) {
                    isNeedIncreaseArray[i] = true;
                    isNeedIncrease = true;
                } else {
                    isNeedIncreaseArray[i] = false;
                }
            } else {
                isNeedIncreaseArray[i] = false;
            }
        }
    }

    @Override
    public void onTick(int escapedMillTime) {
        refreshAttributes();
        if (isNeedIncrease) {
            int duration = GlobalConst.AttributeAutoIncreaseDuration;
            if ((autoIncreasePassTime += escapedMillTime) >= duration) {
                //为了能够把停止tick期间自增的属性都加上、不得不用除法算一下自增了几次
                int cnt = autoIncreasePassTime / duration;
                autoIncreasePassTime -= duration * cnt;
                increaseAuto(cnt);
            }
        }
    }

    /**
     * 计算 刷新 推送 通知 属性组
     */
    public void refreshAttributes() {
        if (isModified) {
            calAllAttributes();
        }

        if (isNeedSend) {
            sendAllAttributes();
        }

        if (isNeedDispatch) {
            dispatchAllAttribute();
        }
    }

    /**
     * 推送所有需要推送的属性
     */
    private void sendAllAttributes() {
        AttributeConfig attributeConfig = ConfigTool.getAttributeConfig();
        isNeedSend = false;

        int[] lastSend = this.lastAttributes;

        int[] attributes = this.attributes;

        short[] tmpMaybeSendNecessaryList = attributeConfig.getMaybeSendNecessaryList();
        boolean[] tmpSendSelfBoolArray = attributeConfig.getSendSelfBoolArray();
        boolean[] tmpSendOtherBoolArray = attributeConfig.getSendOtherBoolArray();
        boolean[] tmpSendMasterBoolArray = attributeConfig.getSendMasterBoolArray();
        boolean[] tmpMaybeSendAndDispatchBoolArray = attributeConfig.getMaybeSendAndDispatchBoolArray();


        int value, lastValue;
        short attrID;
        for (int i = 0, len = tmpMaybeSendNecessaryList.length; i < len; i++) {
            attrID = tmpMaybeSendNecessaryList[i];
            if ((lastValue = lastSend[attrID]) != (value = attributes[attrID])) {
                if (tmpSendSelfBoolArray[attrID]) {
                    tmpSendSelfMap.put(attrID, value);
                }
                if (tmpSendOtherBoolArray[attrID]) {
                    tmpSendOtherMap.put(attrID, value);
                }
                if (tmpSendMasterBoolArray[attrID]) {
                    tmpSendMasterMap.put(attrID, value);
                }

                //仅仅只是推送而不是通知的属性
                if (!tmpMaybeSendAndDispatchBoolArray[attrID]) {
                    lastSend[attrID] = value;
                }
            }
        }

        CharacterEntity characterEntity = parentLogic.characterEntity;
        //如果推送自己
        if (!tmpSendSelfMap.isEmpty()) {
            characterEntity.onAttributePushSelf(tmpSendSelfMap);
            tmpSendSelfMap.clear();
        }
        //如果推送别人
        if (!tmpSendOtherMap.isEmpty()) {
            characterEntity.onAttributePushOther(tmpSendOtherMap);
            tmpSendOtherMap.clear();
        }
        //如果只推送主人
        if (!tmpSendMasterMap.isEmpty()) {
            characterEntity.onAttributePushMaster(tmpSendMasterMap);
            tmpSendMasterMap.clear();
        }
    }

    /**
     * 通知所有需要通知的属性
     */
    private void dispatchAllAttribute() {
        isNeedDispatch = false;
        int[] lastDispatch = lastAttributes;

        short[] tmpDispatchNecessaryList = ConfigTool.getAttributeConfig().getDispatchNecessaryList();
        int[] attributes = this.attributes;
        short[] tmpLastDispatchAttrIDArray = lastDispatchAttrIDArray;
        int[] tmpLastDispatchAttrValueArray = lastDispatchAttrValueArray;

        int num = 0;
        short attrID;
        int value, lastValue;
        for (int i = 0, len = tmpDispatchNecessaryList.length; i < len; i++) {
            attrID = tmpDispatchNecessaryList[i];
            if ((value = attributes[attrID]) != (lastValue = lastDispatch[attrID])) {
                //存储上一次变化
                tmpLastDispatchAttrIDArray[num] = attrID;
                tmpLastDispatchAttrValueArray[num] = lastValue;
                num++;
                //更新
                lastDispatch[attrID] = value;
            }
        }
        //如果有变化的属性、则通知
        if (num > 0) {
            CharacterEntity characterEntity = parentLogic.characterEntity;
            characterEntity.onAttributeChange(num, tmpLastDispatchAttrIDArray, tmpLastDispatchAttrValueArray);
        }
    }

    @Override
    public void onSecond() {
        //属性是否自增每秒检测一次、自增时间间隔要>=1秒
        checkIncreaseAttribute();
    }

    /**
     * 自增一次
     */
    private void increaseAuto(int cnt) {
        AttributeConfig attributeConfig = ConfigTool.getAttributeConfig();
        boolean[] tmpIsNeedIncreaseArray = isNeedIncreaseArray;
        short[] tmpIncreaseArray = attributeConfig.getIncreaseNecessaryList();
        short[] tmpIncreaseCurrentArray = attributeConfig.getIncreaseCurrentNecessaryList();
        int increaseValue;
        for (int i = 0, len = tmpIsNeedIncreaseArray.length; i < len; i++) {
            if (tmpIsNeedIncreaseArray[i]) {
                increaseValue = getAttribute(tmpIncreaseArray[i]);
                addOneAttribute(tmpIncreaseCurrentArray[i], increaseValue * cnt);
            }
        }
    }

    /**
     * 上限变化时修改当前属性
     *
     * @param currentAttrID
     * @param oldMax
     */
    private void calCurrentWhenMaxChange(short currentAttrID, int oldMax, int newMax) {

        //按照比例变化
        if (oldMax > 0) {
            int[] tmpAttributes = attributes;
            float tmpPercent = (float) newMax / oldMax;
            tmpAttributes[currentAttrID] = Math.round(tmpPercent * tmpAttributes[currentAttrID]);
        }

        //按照差值变化
    }

    /**
     * 计算种族影响属性
     *
     * @param params
     * @return
     */
    private int calculateAddRaceResult(int[] params) {
        int result = 0;
        switch (params[0]) {
            case AttributePlusTypeConst.Fixed -> {
                result = getAttribute((short) params[2]) * params[1];
            }
            case AttributePlusTypeConst.Percent -> {
                result = (int) (getAttribute((short) params[2]) * params[1] * RandomUtils.RatioPercent);
            }
        }
        return result;
    }

    /**
     * 计算属性公式结果
     *
     * @param formula
     * @return
     */
    private int calculateFormulaResult(int[] formula) {
        switch (formula[0]) {
            case AttributeFormulaConst.Element1 -> {
                return getAttribute((short) formula[1]);
            }
            case AttributeFormulaConst.Element2 -> {
                return (int) (getAttribute((short) formula[1]) * (1 + getPercentAttribute((short) formula[2])));
            }
            case AttributeFormulaConst.Element3 -> {
                return (int) (getAttribute((short) formula[1]) * (1 + getPercentAttribute((short) formula[2]))) + getAttribute((short) formula[3]);
            }
            case AttributeFormulaConst.Element4 -> {
                return (int) (((getAttribute((short) formula[1]) * (1 + getPercentAttribute((short) formula[2]))) + getAttribute((short) formula[3])) * (1 + getPercentAttribute((short) formula[4])));
            }
            case AttributeFormulaConst.TwoPlus -> {
                return getAttribute((short) formula[1]) + getAttribute((short) formula[2]);
            }
        }
        return 0;
    }

    /**
     * 当前血量
     *
     * @return
     */
    public int getHp() {
        return getAttribute(AttributeConst.Hp);
    }

    /**
     * 血量最大值
     *
     * @return
     */
    public int getHpMax() {
        return getAttribute(AttributeConst.HpMax);
    }

    /**
     * 当前魔法
     *
     * @return
     */
    public int getMp() {
        return getAttribute(AttributeConst.Mp);
    }

    /**
     * 魔法最大值
     *
     * @return
     */
    public int getMpMax() {
        return getAttribute(AttributeConst.MpMax);
    }

    /**
     * 满血
     */
    public void fullHp() {
        addOneAttribute(AttributeConst.Hp, getAttribute(AttributeConst.HpMax));
    }

    /**
     * 满魔
     */
    public void fullMp() {
        addOneAttribute(AttributeConst.Mp, getAttribute(AttributeConst.MpMax));
    }
}
