package com.highfly029.scene.fight.buff;

import com.highfly029.common.template.buff.BuffAddTypeConst;
import com.highfly029.common.template.buff.BuffCostTypeConst;
import com.highfly029.common.template.buff.BuffTemplate;
import com.highfly029.common.template.commonType.PushTypeConst;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.scene.fight.BaseFightLogic;
import com.highfly029.scene.fight.FightAllLogic;
import com.highfly029.scene.fight.effect.EffectFightLogic;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName BuffFightLogic
 * @Description buff数据和逻辑部分
 * @Author liyunpeng
 **/
public class BuffFightLogic extends BaseFightLogic {
    //buff消耗类型数量
    private static final int buffCostTypeNum = BuffCostTypeConst.count;

    //所有buff集合 key=instanceID, value=BuffData
    private final IntObjectMap<BuffData> buffDataAll = new IntObjectMap<>(BuffData[]::new);
    //buffID集合 key=buffID, value=BuffData(不包括并存buffID)
    private final IntObjectMap<BuffData> buffDataByID = new IntObjectMap<>(BuffData[]::new);
    //buffID, instanceID, BuffData(并存buff)
    private final IntObjectMap<IntObjectMap<BuffData>> buffDataByAllExistType = new IntObjectMap<>(IntObjectMap[]::new);

    //buff次数消耗数据集合 index=BuffCostTypeConst,key=instanceID, value=BuffData
    private final IntObjectMap<BuffData>[] buffCostTypeMap = new IntObjectMap[buffCostTypeNum];

    //buff组 (对一组buff统一修改buff等级)
    private final IntIntMap buffGroupWithLevel = new IntIntMap();

    public BuffFightLogic(FightAllLogic logic) {
        super(logic);
    }

    /**
     * 增加buff
     *
     * @param buffID
     * @param level
     * @return
     */
    public BuffData addBuff(int buffID, int level) {
        return addOneBuff(buffID, level, 0);
    }

    /**
     * 通过id删除buff
     *
     * @param buffID
     */
    public void removeBuffByBuffID(int buffID) {
        IntObjectMap<BuffData> existMap;
        if ((existMap = buffDataByAllExistType.get(buffID)) != null) {
            existMap.foreachMutable((k, v) -> removeOneBuff(v));
        } else {
            BuffData buffData;
            if ((buffData = buffDataByID.get(buffID)) != null) {
                removeOneBuff(buffData);
            }
        }
    }

    /**
     * 通过RemoveTypeConst 类型来删除buff
     *
     * @param type
     */
    public void removeBuffByRemoveType(byte type) {
        buffDataAll.foreachMutable((k, v) -> {
            if (v.buffTemplate.getRemoveType() == type) {
                removeOneBuff(v);
            }
        });
    }

    /**
     * 通过实例ID删除buff
     *
     * @param instanceID
     */
    public void removeBuffByInstanceID(int instanceID) {
        BuffData buffData = buffDataAll.get(instanceID);
        if (buffData == null) {
            return;
        }
        removeOneBuff(buffData);
    }

    /**
     * 消耗buff使用次数
     *
     * @param costType
     * @param args
     */
    public void subBuffUseCnt(int costType, int[] args) {
        IntObjectMap<BuffData> map = buffCostTypeMap[costType];
        if (map == null || map.isEmpty()) {
            return;
        }

        //先改变数据
        map.foreachImmutable((k, v) -> {
            if (v.buffTemplate.getCostGroup() != null) {
                int useCntMax;
                int costTypeArg;
                for (int i = 0, len = args.length; i < len; i++) {
                    if ((costTypeArg = v.buffTemplate.getCostGroup()[1]) == args[i]) {
                        if ((useCntMax = v.buffTemplate.getUseCntMax()) > 0 && v.useCnt < useCntMax) {
                            v.useCnt++;
                            if (v.useCnt < useCntMax) {
                                //如果没有用完次数、推送使用次数
                                parentLogic.characterEntity.onRefreshBuffUseCntPushSelf(v);
                            }
                        }
                        break;
                    }
                }

            }
        });
        //如果次数用完、删除buff
        map.foreachMutable((k, v) -> {
            if (v.buffTemplate.getCostGroup() != null) {
                int useCntMax;
                if ((useCntMax = v.buffTemplate.getUseCntMax()) > 0 && v.useCnt >= useCntMax) {
                    buffFinish(v);
                }
            }
        });
    }

    @Override
    public void onTick(int escapedMillTime) {
        if (!buffDataAll.isEmpty()) {
            long now = SystemTimeTool.getMillTime();
            buffDataAll.foreachMutable((k, v) -> {
                if (v.endTime > 0 && now > v.endTime) {
                    buffFinish(v);
                }
            });
        }
    }

    /**
     * buff结束
     *
     * @param buffData
     */
    private void buffFinish(BuffData buffData) {
        EffectFightLogic effectFightLogic = parentLogic.effectFightLogic;
        int[][] effects = buffData.buffTemplate.getOnFinishEffects();
        if (effects != null) {
            for (int i = 0, len = effects.length; i < len; i++) {
                effectFightLogic.doEffectAction(effects[i], 0, null);
            }
        }
        removeOneBuff(buffData);
    }

    /**
     * 实际增加一个buff
     *
     * @param buffID
     * @param level
     * @param adderEntityID
     * @return
     */
    private BuffData addOneBuff(int buffID, int level, int adderEntityID) {
        BuffData buffData = null;
        BuffData oldData;
        BuffTemplate buffTemplate = ConfigTool.getBuffConfig().getBuffTemplate(buffID, level);
        switch (buffTemplate.getAddType()) {
            //高等级叠加低等级，覆盖并刷新; 低等级叠加高等级，只刷新重置使用次数和持续时间,等级不变
            case BuffAddTypeConst.Normal -> {
                if ((oldData = buffDataByID.get(buffID)) != null) {
                    if (level > oldData.level) {
                        removeOneBuff(oldData);
                        addOneBuff(buffData = createOneBuff(buffID, level, adderEntityID, buffTemplate));
                    } else {
                        resetOneBuff(buffData = oldData, true, false);
                    }
                } else {
                    addOneBuff(buffData = createOneBuff(buffID, level, adderEntityID, buffTemplate));
                }
            }

            //新的直接替换旧的
            case BuffAddTypeConst.Replace -> {
                if ((oldData = buffDataByID.get(buffID)) != null) {
                    removeOneBuff(oldData);
                }
                addOneBuff(buffData = createOneBuff(buffID, level, adderEntityID, buffTemplate));
            }

            //同时存在
            case BuffAddTypeConst.Coexistence -> {
                //如果需要限制同时存在的最大数量、在这里按照规则删除
                addOneBuff(buffData = createOneBuff(buffID, level, adderEntityID, buffTemplate));
            }

            //叠层
            case BuffAddTypeConst.AddFloor -> {
                if ((oldData = buffDataByID.get(buffID)) != null) {
                    if (level > oldData.level) {
                        removeOneBuff(oldData);
                        //高等级替换低等级需要重新叠层 可以改成只要等级不相等就替换重新叠层
                        buffData = createOneBuff(buffID, level, adderEntityID, buffTemplate);
                        buffData.floorNum++;
                        addOneBuff(buffData);
                    } else {
                        buffData = oldData;
                        //层数增加
                        if (buffData.floorNum < buffData.buffTemplate.getFloorMax()) {
                            buffData.floorNum++;
                            this.parentLogic.characterEntity.onRefreshBuffFloorNumPushSelf(buffData);
                            //触发叠层最大值行为
                            if (buffData.floorNum == buffData.buffTemplate.getFloorMax()) {
                                onAddFloorNumMaxAction(buffData);
                            }
                        } else {
                            //在层数达到最大值后、如果有持续时间、重置持续时间和使用次数、没有则忽略
                            if (buffData.buffTemplate.getDuration() > 0) {
                                resetOneBuff(buffData, true, false);
                            }
                        }
                    }
                } else {
                    buffData = createOneBuff(buffID, level, adderEntityID, buffTemplate);
                    buffData.floorNum++;
                    addOneBuff(buffData);
                }
            }
            default -> SceneLoggerTool.gameLogger.error("cant support buffID={}, buffAddType:{}", buffID, buffTemplate.getAddType());
        }
        return buffData;
    }

    /**
     * 增加buff
     *
     * @param buffData
     */
    private void addOneBuff(BuffData buffData) {
        //add
        buffDataAll.put(buffData.instanceID, buffData);
        if (buffData.buffTemplate.getAddType() == BuffAddTypeConst.Coexistence) {
            buffDataByAllExistType.putIfAbsent(buffData.buffID, new IntObjectMap<>(BuffData[]::new)).put(buffData.instanceID, buffData);
        } else {
            buffDataByID.put(buffData.buffID, buffData);
        }
        int[] costGroup;
        if ((costGroup = buffData.buffTemplate.getCostGroup()) != null) {
            IntObjectMap<BuffData> map = buffCostTypeMap[costGroup[0]];
            if (map == null) {
                map = new IntObjectMap<>(BuffData[]::new);
                buffCostTypeMap[costGroup[0]] = map;
            }
            map.put(buffData.instanceID, buffData);
        }
        CharacterEntity characterEntity = parentLogic.characterEntity;
        if (buffData.buffTemplate.getPushType() == PushTypeConst.Self) {
            characterEntity.onAddBuffPushSelf(buffData);
        } else if (buffData.buffTemplate.getPushType() == PushTypeConst.All) {
            characterEntity.onAddBuffPushAll(buffData);
        }
        onAddOneBuffAction(buffData);
    }

    /**
     * 重置buff 时间
     *
     * @param buffData
     * @param isResetUseCnt   是否重置使用次数
     * @param isResetFloorNum 是否重置叠层数
     */
    private void resetOneBuff(BuffData buffData, boolean isResetUseCnt, boolean isResetFloorNum) {
        resetBuffEndTime(buffData);
        if (isResetUseCnt) {
            buffData.useCnt = 0;
        }
        if (isResetFloorNum) {
            buffData.floorNum = 0;
        }
        CharacterEntity characterEntity = parentLogic.characterEntity;
        if (buffData.buffTemplate.getPushType() == PushTypeConst.Self) {
            characterEntity.onRefreshBuffPushSelf(buffData);
        } else if (buffData.buffTemplate.getPushType() == PushTypeConst.All) {
            characterEntity.onRefreshBuffPushAll(buffData);
        }
    }

    /**
     * 删除buff
     *
     * @param buffData
     */
    private void removeOneBuff(BuffData buffData) {
        if (removeOneBuffFromData(buffData)) {
            CharacterEntity characterEntity = parentLogic.characterEntity;
            if (buffData.buffTemplate.getPushType() == PushTypeConst.Self) {
                characterEntity.onRemoveBuffPushSelf(buffData.instanceID);
            } else if (buffData.buffTemplate.getPushType() == PushTypeConst.All) {
                characterEntity.onRemoveBuffPushAll(buffData.instanceID);
            }
            onRemoveOneBuffAction(buffData);
        }
    }

    /**
     * 触发删除buff行为
     *
     * @param buffData
     */
    private void onRemoveOneBuffAction(BuffData buffData) {
        int[][] effects = buffData.buffTemplate.getOnRemoveEffects();
        if (effects == null) {
            return;
        }
        EffectFightLogic effectFightLogic = parentLogic.effectFightLogic;
        for (int i = 0, len = effects.length; i < len; i++) {
            effectFightLogic.doEffectAction(effects[i], 0, null);
        }
    }

    /**
     * 从数据集合中删除buff
     *
     * @param buffData
     * @return
     */
    private boolean removeOneBuffFromData(BuffData buffData) {
        if (buffDataAll.remove(buffData.instanceID) == null) {
            return false;
        }

        if (buffData.buffTemplate.getAddType() == BuffAddTypeConst.Coexistence) {
            buffDataByAllExistType.get(buffData.buffID).remove(buffData.instanceID);
        } else {
            buffDataByID.remove(buffData.buffID);
        }
        int[] costGroup;
        if ((costGroup = buffData.buffTemplate.getCostGroup()) != null) {
            IntObjectMap<BuffData> map = buffCostTypeMap[costGroup[0]];
            if (map != null) {
                map.remove(buffData.instanceID);
            }
        }
        return true;
    }

    /**
     * 触发增加buff行为
     *
     * @param buffData
     */
    private void onAddOneBuffAction(BuffData buffData) {
        int[][] effects = buffData.buffTemplate.getOnAddEffects();
        if (effects == null) {
            return;
        }
        EffectFightLogic effectFightLogic = parentLogic.effectFightLogic;
        for (int i = 0, len = effects.length; i < len; i++) {
            effectFightLogic.doEffectAction(effects[i], 0, null);
        }
    }

    /**
     * 触发到达叠层上限的行为
     *
     * @param buffData
     */
    private void onAddFloorNumMaxAction(BuffData buffData) {
        int[][] effects = buffData.buffTemplate.getOnFloorNumMaxEffects();
        if (effects == null) {
            return;
        }
        EffectFightLogic effectFightLogic = parentLogic.effectFightLogic;
        for (int i = 0, len = effects.length; i < len; i++) {
            effectFightLogic.doEffectAction(effects[i], 0, null);
        }
    }

    /**
     * 实际创建一个buff
     *
     * @param buffID
     * @param level
     * @param adderEntityID
     * @return
     */
    private BuffData createOneBuff(int buffID, int level, int adderEntityID, BuffTemplate buffTemplate) {
        BuffData buffData = BuffData.create();
        buffData.autoIncrementIndex();
        buffData.buffID = buffID;
        buffData.buffTemplate = buffTemplate;
        buffData.level = getBuffRealLevel(buffData.buffTemplate, level);
        buffData.buffTemplate = ConfigTool.getBuffConfig().getBuffTemplate(buffID, buffData.level);
        buffData.adderEntityID = adderEntityID;
        resetBuffEndTime(buffData);

        return buffData;
    }

    /**
     * 重置buff结束时间
     *
     * @param buffData
     */
    private void resetBuffEndTime(BuffData buffData) {
        //只有duration > 0 的才通过时间tick自动移除 否则不移除
        if (buffData.buffTemplate.getDuration() > 0) {
            long now = SystemTimeTool.getMillTime();
            buffData.endTime = now + getBuffRealDuration(buffData);
        }
    }

    /**
     * 获取buff真实的等级
     *
     * @param buffTemplate
     * @param level
     * @return
     */
    private int getBuffRealLevel(BuffTemplate buffTemplate, int level) {
        if (buffTemplate.getGroup() != null) {
            for (int groupID : buffTemplate.getGroup()) {
                level += buffGroupWithLevel.getOrDefault(groupID, 0);
            }
        }
        return level;
    }

    /**
     * 获取buff真实的持续时间
     *
     * @param data
     * @return
     */
    private int getBuffRealDuration(BuffData data) {
        return data.buffTemplate.getDuration();
    }
}
