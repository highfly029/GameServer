package com.highfly029.scene.fight.dungeon;

import com.highfly029.common.template.dungeon.DungeonTemplate;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName DungeonModule
 * @Description 副本模块
 * @Author liyunpeng
 **/
public class DungeonModule {


    /**
     * 副本集合 key=instanceID
     */
    private static final IntObjectMap<Dungeon> dungeonMap = new IntObjectMap<>(Dungeon[]::new);


    /**
     * 创建副本
     *
     * @param dungeonTemplate
     */
    public static Dungeon createDungeon(DungeonTemplate dungeonTemplate) {
        Dungeon dungeon = Dungeon.create();
        dungeon.initWithTemplate(dungeonTemplate);
        dungeonMap.put(dungeon.dungeonInstanceID, dungeon);
        return dungeon;
    }

    /**
     * 删除副本
     *
     * @param instanceID
     */
    public static void removeDungeon(int instanceID) {
        dungeonMap.remove(instanceID);
    }

    public void onTick(int escapedMillTime) {
        dungeonMap.foreachMutable((k, v) -> {
            if (v.isRemoveNextTick) {
                removeDungeon(k);
            }
        });
        int freeValue = dungeonMap.getFreeValue();
        int[] keys = dungeonMap.getKeys();
        Dungeon[] values = dungeonMap.getValues();
        int key;
        Dungeon dungeon;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                dungeon = values[i];
                dungeon.onTick(escapedMillTime);
            }
        }
    }

    public void onSecond() {
        int freeValue = dungeonMap.getFreeValue();
        int[] keys = dungeonMap.getKeys();
        Dungeon[] values = dungeonMap.getValues();
        int key;
        Dungeon dungeon;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                dungeon = values[i];
                dungeon.onSecond();
            }
        }
    }

}
