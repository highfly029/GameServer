package com.highfly029.scene.fight.scene.dungeonLogic;

import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.fight.entity.Entity;
import com.highfly029.scene.fight.entity.PlayerEntity;
import com.highfly029.scene.fight.scene.sceneImp.DungeonScene;

/**
 * @ClassName DungeonLogic
 * @Description 副本逻辑
 * @Author liyunpeng
 **/
public abstract class DungeonLogic {
    /**
     * 副本场景
     */
    public DungeonScene dungeonScene;

    /**
     * 副本逻辑初始化
     */
    public void onDungeonLogicInit() {

    }

    /**
     * entity进入副本场景
     *
     * @param entity
     */
    public void onEntityEnterDungeon(Entity entity) {
    }

    /**
     * entity离开副本场景
     *
     * @param entity
     */
    public void onEntityLeaveDungeon(Entity entity) {
    }

    /**
     * entity死亡通知
     *
     * @param characterEntity
     */
    public void onCharacterDeath(CharacterEntity characterEntity) {

    }

    /**
     * 战斗entity产生伤害通知
     *
     * @param attackSource
     * @param attackTarget
     * @param realDamageValue
     */
    public void onCharacterDamage(CharacterEntity attackSource, CharacterEntity attackTarget, int realDamageValue) {

    }

    /**
     * 玩家掉线
     *
     * @param playerEntity
     */
    public void onPlayerOffline(PlayerEntity playerEntity) {

    }

    /**
     * 玩家断线重连
     *
     * @param playerEntity
     */
    public void onPlayerReconnectLogin(PlayerEntity playerEntity) {

    }

    /**
     * 玩家登出
     *
     * @param playerEntity
     */
    public void onPlayerLogout(PlayerEntity playerEntity) {

    }

    /**
     * 是否可以复活
     *
     * @return
     */
    public boolean isCanReborn(PlayerEntity playerEntity) {
        return false;
    }

    /**
     * 扣除复活消耗
     */
    public void doRebornCost(PlayerEntity playerEntity) {

    }

    public boolean checkIsFriend(CharacterEntity sourceEntity, CharacterEntity targetEntity) {
        return false;
    }

    public boolean checkIsEnemy(CharacterEntity sourceEntity, CharacterEntity targetEntity) {
        return false;
    }
}
