package com.highfly029.scene.fight.entity;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.scene.ai.CreateAIData;
import com.highfly029.scene.fight.scene.Scene;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.world.Entity2AIEvent;
import com.highfly029.scene.world.SceneEntityWorld;
import com.highfly029.utils.MathUtils;

/**
 * @ClassName Entity
 * @Description 实体对象
 * @Author liyunpeng
 **/
public abstract class Entity {
    protected final SceneEntityWorld sceneEntityWorld;
    /**
     * 唯一id
     */
    public int entityID;
    /**
     * 类型
     */
    public int type;
    /**
     * 所在场景
     */
    public Scene scene;
    /**
     * 当前坐标
     */
    public Vector3 curPos = Vector3.create();
    /**
     * 当前角色朝向 必须为单位向量
     */
    public Vector3 direction = Vector3.create();
    /**
     * 是否隐身单位 变化时要广播aoi
     */
    public boolean isHide;
    /**
     * 碰撞半径
     */
    public float collideRadius;
    /**
     * 是否参与碰撞
     */
    public boolean isCollide;

    /**
     * 是否下一帧删除
     */
    public boolean isRemoveNextTick;

    public Entity(SceneEntityWorld sceneEntityWorld) {
        this.sceneEntityWorld = sceneEntityWorld;
    }


    @Override
    public String toString() {
        return "Entity{" +
                "entityID=" + entityID +
                ", type=" + type +
                ", scene=" + scene +
                ", curPos=" + curPos +
                ", direction=" + direction +
                ", isHide=" + isHide +
                ", collideRadius=" + collideRadius +
                ", isCollide=" + isCollide +
                '}';
    }

    public void init(int templateID) {

    }

    /**
     * 在加入scene之前实体初始化
     */
    public void initEntityBeforeAddToScene() {

    }

    public void onTick(int escapedMillTime) {

    }

    public void onSecond() {

    }

    public void setCurPos(Vector3 curPos) {
        Vector3 tmpCurPos = this.curPos;
        tmpCurPos.x = curPos.x;
        tmpCurPos.y = curPos.y;
        tmpCurPos.z = curPos.z;
    }

    public void setDirection(Vector3 direction) {
        Vector3 tmpDirection = this.direction;
        tmpDirection.x = direction.x;
        tmpDirection.y = direction.y;
        tmpDirection.z = direction.z;
    }

    /**
     * 实体立刻离开场景
     */
    public void leaveScene() {
        SceneLoggerTool.gameLogger.info("leaveScene sceneID={}, lineID={}", scene.sceneID, scene.lineID);
        scene.removeEntity(entityID);
    }

    /**
     * 离开场景通知
     *
     * @param scene
     */
    public void onLeaveScene(Scene scene) {
        //离开场景通知AI删除
        long key = MathUtils.getCompositeIndex(scene.instanceID, entityID);
        sceneEntityWorld.postEvent2SceneAI(Entity2AIEvent.RemoveEntityAI, key, null);
    }

    /**
     * 进入场景通知
     *
     * @param scene
     */
    public void onEnterScene(Scene scene) {
        CreateAIData createAIData = getCreateAIData();
        sceneEntityWorld.postEvent2SceneAI(Entity2AIEvent.AddEntityAI, createAIData, null);
    }

    /**
     * 获取创建AI需要的数据
     * @return CreateAIData
     */
    protected CreateAIData getCreateAIData() {
        CreateAIData createAIData = new CreateAIData();
        createAIData.setSceneInstanceID(scene.instanceID);
        createAIData.setEntityID(entityID);
        createAIData.setType(type);
        Vector3 pos = Vector3.create();
        pos.copy(curPos);
        createAIData.setCurPos(pos);
        return createAIData;
    }


    /************* 判断entity类型的快捷方式 代替 instance of ***************/

    /**
     * 是否是可移动有aoi的实体
     *
     * @return
     */
    public boolean isMoveEntity() {
        return false;
    }

    /**
     * 是否是固定位置的entity
     *
     * @return
     */
    public boolean isFixedPosEntity() {
        return false;
    }

    /**
     * 是否是可战斗的单位
     *
     * @return
     */
    public boolean isCharacterEntity() {
        return false;
    }
}
