package com.highfly029.scene.fight.dungeon;

import com.highfly029.common.template.dungeon.DungeonStateTypeConst;
import com.highfly029.common.template.dungeon.DungeonTemplate;
import com.highfly029.common.template.dungeonRoom.DungeonRoomTemplate;
import com.highfly029.scene.fight.scene.sceneImp.DungeonScene;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.utils.DateUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.LongSet;

/**
 * @ClassName Dungeon
 * @Description 副本
 * @Author liyunpeng
 **/
public class Dungeon {
    /**
     * 副本自增实例id
     */
    public static int dungeonIncreaseID;
    /**
     * 副本实例id
     */
    public int dungeonInstanceID;
    /**
     * 副本状态
     */
    private byte state;
    /**
     * tick经过时间
     */
    private int tickPassTime;
    /**
     * 战斗持续时间(秒)
     */
    private int fightSecTime;
    /**
     * 副本是否暂定
     */
    private boolean isStop;

    /**
     * 是否自动切换到下一个阶段
     */
    private boolean isAutoSwitchNext;

    /**
     * 副本里没有玩家的持续时间(秒)
     */
    private int noPlayerKeepTime;

    /**
     * 是否下一帧删除
     */
    public boolean isRemoveNextTick;

    /**
     * 副本房间场景集合 roomID -> DungeonScene
     */
    private final IntObjectMap<DungeonScene> map = new IntObjectMap<>(DungeonScene[]::new);

    /**
     * 副本模版
     */
    public DungeonTemplate dungeonTemplate;

    /**
     * 副本玩家集合 进入时加入 退出时删除 重进时检测
     */
    private final LongSet playerSet = new LongSet();

    public static Dungeon create() {
        return new Dungeon();
    }

    /**
     * 初始化
     *
     * @param dungeonTemplate
     */
    public void initWithTemplate(DungeonTemplate dungeonTemplate) {
        //副本数据初始化
        this.dungeonTemplate = dungeonTemplate;
        this.dungeonInstanceID = ++dungeonIncreaseID;

        isAutoSwitchNext = dungeonTemplate.isAutoSwitchNext();

        switchState(DungeonStateTypeConst.Enter);
    }

    public void onTick(int escapedMillTime) {
        if (isStop) {
            return;
        }
        if (tickPassTime > 0 && isAutoSwitchNext) {
            if ((tickPassTime -= escapedMillTime) <= 0) {
                tickPassTime = 0;
                switchNextState();
            }
        }
    }

    public void onSecond() {
        //副本没有玩家且超时后销毁副本
//        if (dungeonTemplate.getNoPlayerKeepTime() > 0 && playerMap.size() == 0) {
//            if (++noPlayerKeepTime >= dungeonTemplate.getNoPlayerKeepTime()) {
//                SceneLoggerTool.gameLogger.warn("noPlayerKeepTime={} destroy dungeonID={}, state={}", noPlayerKeepTime, dungeonTemplate.getDungeonID(), state);
//                onDestroy();
//            }
//        }

        if (isStop) {
            return;
        }

        if (state == DungeonStateTypeConst.Fight) {
            fightSecTime++;
        }
    }

    /**
     * 切换到副本下一个阶段
     */
    private void switchNextState() {
        switch (state) {
            case DungeonStateTypeConst.Enter -> {
                switchState(DungeonStateTypeConst.Prepare);
            }
            case DungeonStateTypeConst.Prepare -> {
                switchState(DungeonStateTypeConst.Fight);
            }
            case DungeonStateTypeConst.Fight -> {
                switchState(DungeonStateTypeConst.Finish);
            }
            case DungeonStateTypeConst.Finish -> {
                switchState(DungeonStateTypeConst.Reward);
            }
            case DungeonStateTypeConst.Reward -> {
                switchState(DungeonStateTypeConst.Quit);
            }
            case DungeonStateTypeConst.Quit -> {
                //退出时间结束 直接走销毁流程
                onDestroy();
            }
        }
    }

    /**
     * 副本状态切换
     *
     * @param state
     */
    public void switchState(byte state) {
        this.state = state;
        switch (state) {
            case DungeonStateTypeConst.Enter -> {
                tickPassTime = dungeonTemplate.getEnterDurationTime() * DateUtils.MILLIS_EVERY_SECOND;
                //至少一次tick
                tickPassTime = tickPassTime > 0 ? tickPassTime : 1;
                onEnter();
            }
            case DungeonStateTypeConst.Prepare -> {
                tickPassTime = dungeonTemplate.getPrepareDuratiionTime() * DateUtils.MILLIS_EVERY_SECOND;
                //至少一次tick
                tickPassTime = tickPassTime > 0 ? tickPassTime : 1;
                onPrepare();
            }
            case DungeonStateTypeConst.Fight -> {
                tickPassTime = dungeonTemplate.getFightDurationTime() * DateUtils.MILLIS_EVERY_SECOND;
                onFight();
            }
            case DungeonStateTypeConst.Finish -> {
                tickPassTime = dungeonTemplate.getFinishDurationTime() * DateUtils.MILLIS_EVERY_SECOND;
                //至少一次tick
                tickPassTime = tickPassTime > 0 ? tickPassTime : 1;
                onFinish();
            }
            case DungeonStateTypeConst.Reward -> {
                tickPassTime = dungeonTemplate.getRewardDurationTime() * DateUtils.MILLIS_EVERY_SECOND;
                //至少一次tick
                tickPassTime = tickPassTime > 0 ? tickPassTime : 1;
                onReward();
            }
            case DungeonStateTypeConst.Quit -> {
                tickPassTime = dungeonTemplate.getQuitDurationTime() * DateUtils.MILLIS_EVERY_SECOND;
                //至少一次tick
                tickPassTime = tickPassTime > 0 ? tickPassTime : 1;
                onQuit();
            }
        }
    }

    /**
     * 副本切换到进入阶段
     */
    protected void onEnter() {
        SceneLoggerTool.fightLogger.info("副本切换到进入阶段 dungeonID={}, state={}", dungeonTemplate.getDungeonID(), state);
    }

    /**
     * 副本切换到准备阶段
     */
    protected void onPrepare() {
        SceneLoggerTool.fightLogger.info("副本切换到准备阶段 dungeonID={}, state={}", dungeonTemplate.getDungeonID(), state);
    }

    /**
     * 副本切换到战斗阶段
     */
    protected void onFight() {
        SceneLoggerTool.fightLogger.info("副本切换到战斗阶段 dungeonID={}, state={}", dungeonTemplate.getDungeonID(), state);
    }

    /**
     * 副本切换到结算阶段
     */
    protected void onFinish() {
        SceneLoggerTool.fightLogger.info("副本切换到结算阶段 dungeonID={}, state={}", dungeonTemplate.getDungeonID(), state);
    }

    /**
     * 副本切换到奖励阶段
     */
    protected void onReward() {
        SceneLoggerTool.fightLogger.info("副本切换到奖励阶段 dungeonID={}, state={}", dungeonTemplate.getDungeonID(), state);
    }

    /**
     * 副本切换到退出阶段
     */
    protected void onQuit() {
        SceneLoggerTool.fightLogger.info("副本切换到退出阶段 dungeonID={}, state={}", dungeonTemplate.getDungeonID(), state);
        kickOutAllPlayers();
    }

    /**
     * 副本销毁
     */
    private void onDestroy() {
        isRemoveNextTick = true;
        SceneLoggerTool.fightLogger.info("副本销毁 dungeonID={}, state={}", dungeonTemplate.getDungeonID(), state);
    }

    /**
     * 踢出副本全部玩家
     */
    private void kickOutAllPlayers() {
//        playerMap.foreachMutable((k, v) -> {
//            v.entityModule.switchLastScene();
//            playerQuit(v, false, false);
//        });
    }

//    /**
//     * 获取入口场景
//     *
//     * @return
//     */
//    public DungeonScene getEntranceScene() {
//        DungeonRoomTemplate dungeonRoomTemplate = ConfigTool.getDungeonConfig().getDungeonRoomTemplate(dungeonTemplate.getDungeonID(), dungeonTemplate.getEntranceRoomID());
//        return getDungeonScene(dungeonRoomTemplate);
//    }

    /**
     * 或得一个副本场景 如果没有则创建
     *
     * @param dungeonRoomTemplate
     * @return
     */
    public DungeonScene getDungeonScene(DungeonRoomTemplate dungeonRoomTemplate) {
        DungeonScene dungeonScene = map.get(dungeonRoomTemplate.getDungeonRoomID());
        if (dungeonScene == null) {
            dungeonScene = null;// (DungeonScene) SceneModule.createScene(dungeonRoomTemplate.getSceneID());
            dungeonScene.initWithDungeon(this);
            dungeonScene.addPreSetEntity();
            map.put(dungeonRoomTemplate.getDungeonRoomID(), dungeonScene);
        }
        return dungeonScene;
    }

}
