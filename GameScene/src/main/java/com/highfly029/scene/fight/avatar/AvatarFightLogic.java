package com.highfly029.scene.fight.avatar;

import com.highfly029.common.template.model.AvatarPartTypeConst;
import com.highfly029.common.template.model.ModelTemplate;
import com.highfly029.scene.fight.BaseFightLogic;
import com.highfly029.scene.fight.FightAllLogic;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.utils.collection.IntIntMap;

/**
 * @ClassName AvatarFightLogic
 * @Description 换装逻辑 数值变化后直接推送、没必要tick统一修改
 * @Author liyunpeng
 **/
public class AvatarFightLogic extends BaseFightLogic {
    //模型ID
    private int modelID;
    //部件类型对应的部件ID 后续byte不合适可改为int或short
    private final byte[] parts = new byte[AvatarPartTypeConst.count];
    //变身模型ID 0表示没有变身
    private int transformModelID;

    public AvatarFightLogic(FightAllLogic fightAllLogic) {
        super(fightAllLogic);
    }

    /**
     * 用数据初始化换装
     *
     * @param modelID
     * @param parts
     */
    public void initWithData(int modelID, IntIntMap parts) {
        this.modelID = modelID;
        parts.foreachImmutable((k, v) -> {
            this.parts[k] = (byte) v;
        });
    }

    /**
     * 默认初始化换装
     *
     * @param modelID
     */
    public void initDefault(int modelID) {
        this.modelID = modelID;
        ModelTemplate modelTemplate = ConfigTool.getAvatarConfig().getModelTemplate(modelID);
        //先清理之前的部件
        for (int i = 0, len = this.parts.length; i < len; i++) {
            this.parts[i] = 0;
        }
        //再设置默认部件
        short[][] defaults = modelTemplate.getDefaultParts();
        if (defaults != null) {
            for (int i = 0, len = defaults.length; i < len; i++) {
                int partType = defaults[i][0];
                this.parts[partType] = (byte) defaults[i][1];
            }
        }
    }

    /**
     * 改变模型
     *
     * @param modelID
     */
    public void changeModel(int modelID) {
        if (this.modelID == modelID) {
            return;
        }
        this.modelID = modelID;
        ModelTemplate modelTemplate = ConfigTool.getAvatarConfig().getModelTemplate(modelID);
        //先清理之前的部件
        for (int i = 0, len = this.parts.length; i < len; i++) {
            this.parts[i] = 0;
        }
        //再设置默认部件
        short[][] defaults = modelTemplate.getDefaultParts();
        if (defaults != null) {
            for (int i = 0, len = defaults.length; i < len; i++) {
                int partType = defaults[i][0];
                this.parts[partType] = (byte) defaults[i][1];
            }
        }
        parentLogic.characterEntity.onAvatarChangeModelPushAll(modelID, parts);
        onAvatarModelChange(modelID);
    }

    /**
     * 只改变部件
     *
     * @param avatarPartType
     * @param partID         =0 表示去掉部件
     */
    public void changeAvatarPart(int avatarPartType, int partID) {
        if (partID > 0 && !ConfigTool.getAvatarConfig().isContainPartID(modelID, avatarPartType, partID)) {
            SceneLoggerTool.gameLogger.error("changeAvatarPart invalid modelID={}, avatarPartType={}, partID={}", modelID, avatarPartType, partID);
            return;
        }
        if (this.parts[avatarPartType] == partID) {
            return;
        }
        this.parts[avatarPartType] = (byte) partID;
        parentLogic.characterEntity.onAvatarChangePartPushAll(avatarPartType, partID);
    }

    /**
     * 变身
     *
     * @param modelID > 0 开启变身 =0 结束变身
     */
    public void transform(int modelID) {
        if (this.transformModelID == modelID) {
            return;
        }
        this.transformModelID = modelID;
        parentLogic.characterEntity.onAvatarTransformPushAll(this.transformModelID);
        if (this.transformModelID > 0) {
            onAvatarModelChange(this.transformModelID);
        } else {
            onAvatarModelChange(this.modelID);
        }
    }

    /**
     * 模型改变通知
     *
     * @param modelID
     */
    private void onAvatarModelChange(int modelID) {
        parentLogic.characterEntity.onAvatarModelChange(modelID);
    }
}
