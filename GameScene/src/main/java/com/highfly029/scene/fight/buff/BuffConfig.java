package com.highfly029.scene.fight.buff;

import com.highfly029.common.template.buff.BuffTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName BuffConfig
 * @Description buff配置数据
 * @Author liyunpeng
 **/
public class BuffConfig extends BaseGlobalConfig {
    //buffID << 32 | level, BuffTemplate
    private final LongObjectMap<BuffTemplate> buffTemplates = new LongObjectMap<>(BuffTemplate[]::new);

    @Override
    public void check(boolean isHotfix) {
        //TODO addType=3 叠层buff floorMax必须大于0
        //TODO duration=0的时候不会通过tick移除 只能是使用次数用完了自动删除或者根据removeType移除

    }

    @Override
    protected void dataProcess() {
        BuffTemplate buffTemplate;
        ObjectList<BaseTemplate> templateList = TemplateTool.dataTemplates.get(BuffTemplate.class);
        for (int i = 0, size = templateList.size(); i < size; i++) {
            buffTemplate = (BuffTemplate) templateList.get(i);
            buffTemplates.put(MathUtils.getCompositeIndex(buffTemplate.getBuffID(), buffTemplate.getBuffLevel()), buffTemplate);
        }
    }

    /**
     * 根据buffID和level获取BuffTemplate
     *
     * @param buffID
     * @param level
     * @return
     */
    public BuffTemplate getBuffTemplate(int buffID, int level) {
        return buffTemplates.get(MathUtils.getCompositeIndex(buffID, level));
    }
}
