package com.highfly029.scene.fight.entity;

import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName NpcEntity
 * @Description NPC实体
 * @Author liyunpeng
 **/
public class NpcEntity extends FixedPosEntity {
    public NpcEntity(SceneEntityWorld sceneEntityWorld) {
        super(sceneEntityWorld);
    }
}
