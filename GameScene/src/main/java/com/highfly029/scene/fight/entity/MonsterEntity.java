package com.highfly029.scene.fight.entity;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.template.attribute.AttributeConst;
import com.highfly029.common.template.entity.EntityTypeConst;
import com.highfly029.common.template.monster.DropFilterTypeConst;
import com.highfly029.common.template.monster.DropTypeConst;
import com.highfly029.common.template.monster.MonsterTemplate;
import com.highfly029.common.template.state.StateConst;
import com.highfly029.common.templateBase.basetype.Attr;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.scene.ai.CreateAIData;
import com.highfly029.scene.fight.attribute.AttributeFightLogic;
import com.highfly029.scene.fight.buff.BuffFightLogic;
import com.highfly029.scene.fight.monster.MonsterConfig;
import com.highfly029.scene.fight.skill.SkillFightLogic;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.world.Entity2AIEvent;
import com.highfly029.scene.world.SceneEntityWorld;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.IntLongMap;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.ObjectSet;

/**
 * @ClassName MonsterEntity
 * @Description 怪物
 * @Author liyunpeng
 **/
public class MonsterEntity extends CharacterEntity {
    /**
     * 怪物出生点
     */
    public Vector3 birthPos = Vector3.create();
    /**
     * 模板
     */
    public MonsterTemplate monsterTemplate;

    /**
     * 怪物尸体持续时间 ms
     */
    private int deadBodyKeepTime;

    /**
     * 第一个对怪物造成伤害的entity
     */
    private int firstAttacker;
    /**
     * 是否记录伤害列表
     */
    private boolean isRecordDamageList;
    /**
     * 怪物受到伤害列表
     */
    private IntLongMap damageListMap;

    /**
     * 单份掉落
     */
    private IntIntMap singleDropList;
    /**
     * 多人掉落
     */
    private LongObjectMap<IntIntMap> multiDropListMap;

    public MonsterEntity(SceneEntityWorld sceneEntityWorld) {
        super(sceneEntityWorld);
    }

    @Override
    public boolean isMonster() {
        return true;
    }

    @Override
    public boolean isBelongToMonster() {
        return true;
    }

    //初始化
    @Override
    public void init(int templateID) {
        MonsterTemplate template = monsterTemplate = ConfigTool.getMonsterConfig().getMonsterTemplate(templateID);
        this.race = template.getRace();
        this.force = template.getForceType();
        Attr[] attributes;
        Attr attr;
        int[][] skills;
        int[] skillArray;
        int[][] buffs;
        int[] buffArray;

        AttributeFightLogic attributeFightLogic = fightAllLogic.attributeFightLogic;
        if ((attributes = template.getAttributes()) != null) {
            for (int i = 0, len = attributes.length; i < len; i++) {
                attr = attributes[i];
                attributeFightLogic.addOneAttribute((short) attr.id, attr.value);
            }
        }
        attributeFightLogic.addOneAttribute(AttributeConst.Hp, 1);
        attributeFightLogic.addOneAttribute(AttributeConst.Mp, attributeFightLogic.getAttribute(AttributeConst.MpMax));

        if ((skills = template.getSkills()) != null) {
            SkillFightLogic skillFightLogic = fightAllLogic.skillFightLogic;
            for (int i = 0, len = skills.length; i < len; i++) {
                skillArray = skills[i];
                skillFightLogic.skillLevelMap.put(skillArray[0], skillArray[1]);
            }
        }
        if ((buffs = template.getBuffs()) != null) {
            BuffFightLogic buffFightLogic = fightAllLogic.buffFightLogic;
            for (int i = 0, len = buffs.length; i < len; i++) {
                buffArray = buffs[i];
                buffFightLogic.addBuff(buffArray[0], buffArray[1]);
            }
        }


        //判断怪物是否需要记录伤害列表
        if (template.getDropType() != DropTypeConst.None) {
            if (template.getDropFilterType() == DropFilterTypeConst.All || template.getDropFilterType() == DropFilterTypeConst.MostDamageAttacker ||
                    template.getDropFilterType() == DropFilterTypeConst.TimeFilter || template.getDropFilterType() == DropFilterTypeConst.DistanceFilter) {
                isRecordDamageList = true;
            }
        }

        //有可能产生多分掉落
        if (template.getDropType() == DropTypeConst.DeadBody && template.isDropUnique() == false) {
            multiDropListMap = new LongObjectMap<>(IntIntMap[]::new);
        }

        if (isRecordDamageList) {
            damageListMap = new IntLongMap();
        }
    }

    @Override
    protected CreateAIData getCreateAIData() {
        CreateAIData createAIData = super.getCreateAIData();
        createAIData.setTemplateID(monsterTemplate.getMonsterID());
        //怪物AI
        if (monsterTemplate.getAi() != null) {
            createAIData.setAiName(monsterTemplate.getAi());
        }
        //怪物技能
        if (monsterTemplate.getSkills() != null) {
            IntIntMap skills = new IntIntMap();
            for (int i = 0, len = monsterTemplate.getSkills().length; i < len; i++) {
                int[] skillArray = monsterTemplate.getSkills()[i];
                skills.put(skillArray[0], skillArray[1]);
            }
            createAIData.setSkills(skills);
        }
        return createAIData;
    }

    @Override
    public void onDeath(CharacterEntity attackSource) {
        MonsterConfig monsterConfig = ConfigTool.getMonsterConfig();
        if (monsterTemplate.isRevive()) {
            //只有编辑器或者表里种的怪才可以复活
            if (this.scene.isDynamicEntityID(entityID)) {
                SceneLoggerTool.fightLogger.error("monster dynamic generate cant revive sceneID={}, monsterID={}, entityID={}", scene.sceneTemplate.getSceneID(), monsterTemplate.getMonsterID(), entityID);
            } else {
                scene.monsterReviveMap.put(entityID, monsterTemplate.getReviveInterval() + SystemTimeTool.getMillTime());
            }
        }

        //存在尸体
        if (monsterTemplate.isDeadBody()) {
            this.deadBodyKeepTime = monsterTemplate.getDeadBodyKeepTime();
        } else {
            //没有尸体的话直接从场景删除
            scene.removeEntityNextTick(this);
        }

        byte dropType = monsterTemplate.getDropType();

        //怪物掉落
        if (dropType == DropTypeConst.DeadBody) {
            if (monsterTemplate.isDropUnique()) {
                //尸体里存在唯一一份掉落
                singleDropList = monsterConfig.getDropList(monsterTemplate, 0);
            } else {
                //每个符合条件的玩家都在尸体里生成一份掉落
                genDropListForEveryone(attackSource);

            }
        } else if (dropType == DropTypeConst.SceneEntity) {
            //一般情况下只推送给team 或者个人
            //一定时间后如果没有拾取则自动从场景删除
            Vector3 pos = Vector3.create();
            pos.x = this.curPos.x + 5;
            pos.y = this.curPos.y;
            pos.z = this.curPos.z + 5;
            DropEntity dropEntity = (DropEntity) scene.createEntity(EntityTypeConst.DROP, pos, null, false);
            dropEntity.initWithDropList(monsterConfig.getDropList(monsterTemplate, 0), SystemTimeTool.getMillTime() + 30 * 1000L);
            scene.addEntity(dropEntity);
        } else if (dropType == DropTypeConst.Bag) {
            //掉落直接进入attackSource的背包
        }
    }

    @Override
    public void onDamage(int realDamageValue, CharacterEntity damageSource) {
        super.onDamage(realDamageValue, damageSource);
        if (firstAttacker <= 0) {
            firstAttacker = damageSource.entityID;
        }
        if (isRecordDamageList) {
            damageListMap.addValue(damageSource.entityID, realDamageValue);
        }
    }

    /**
     * 每个符合条件的玩家都在尸体里生成一份掉落
     */
    private void genDropListForEveryone(CharacterEntity killer) {
        IntObjectMap<PlayerEntity> allPlayerEntity = scene.allPlayerEntity;
        MonsterConfig monsterConfig = ConfigTool.getMonsterConfig();
        switch (monsterTemplate.getDropFilterType()) {
            case DropFilterTypeConst.All -> {
                if (damageListMap != null && damageListMap.size() > 0) {
                    damageListMap.foreachImmutable((k, v) -> {
                        PlayerEntity playerEntity = allPlayerEntity.get(k);
                        if (playerEntity != null) {
                            long playerID = playerEntity.playerID;
                            IntIntMap dropList = monsterConfig.getDropList(monsterTemplate, playerID);
                            multiDropListMap.put(playerID, dropList);
                        }
                    });
                }
            }
            case DropFilterTypeConst.FirstAttacker -> {
                PlayerEntity playerEntity = allPlayerEntity.get(firstAttacker);
                if (playerEntity != null) {
                    long playerID = playerEntity.playerID;
                    IntIntMap dropList = monsterConfig.getDropList(monsterTemplate, playerID);
                    multiDropListMap.put(playerID, dropList);
                }
            }
            case DropFilterTypeConst.LastAttacker -> {
                if (killer.isPlayer()) {
                    PlayerEntity playerEntity = (PlayerEntity) killer;
                    long playerID = playerEntity.playerID;
                    IntIntMap dropList = monsterConfig.getDropList(monsterTemplate, playerID);
                    multiDropListMap.put(playerID, dropList);
                }
            }
            case DropFilterTypeConst.MostDamageAttacker -> {
                if (damageListMap != null && damageListMap.size() > 0) {
                    int freeValue = damageListMap.getFreeValue();
                    int[] keys = damageListMap.getKeys();
                    long[] values = damageListMap.getValues();
                    int key;
                    int entityID = 0;
                    long maxDamageValue = 0;
                    for (int i = 0, len = keys.length; i < len; i++) {
                        if ((key = keys[i]) != freeValue) {
                            if (values[i] > maxDamageValue) {
                                entityID = key;
                                maxDamageValue = values[i];
                            }
                        }
                    }
                    PlayerEntity playerEntity = allPlayerEntity.get(entityID);
                    if (playerEntity != null) {
                        long playerID = playerEntity.playerID;
                        IntIntMap dropList = monsterConfig.getDropList(monsterTemplate, playerID);
                        multiDropListMap.put(playerID, dropList);
                    }
                }
            }
        }
    }

    /**
     * 获取单份掉落
     *
     * @return
     */
    public IntIntMap getSingleDropList() {
        return singleDropList;
    }

    /**
     * 移除单份掉落
     */
    public void removeSingleDropList() {
        singleDropList = null;
    }

    /**
     * 获取个人掉落
     *
     * @param playerID
     * @return
     */
    public IntIntMap getPlayerDropList(long playerID) {
        return multiDropListMap.get(playerID);
    }

    /**
     * 删除个人掉落
     *
     * @param playerID
     */
    public void removePlayerDropList(long playerID) {
        multiDropListMap.remove(playerID);
    }

    @Override
    public void onOtherEntityEnterSelfAOI(ObjectSet<MoveEntity> enterSelfAOISets) {
        long key = MathUtils.getCompositeIndex(scene.instanceID, entityID);
        IntSet entityIDSet = new IntSet();
        enterSelfAOISets.foreachImmutable(v -> entityIDSet.add(v.entityID));
        sceneEntityWorld.postEvent2SceneAI(Entity2AIEvent.OtherEntityEnterAOI, key, entityIDSet);
    }

    @Override
    public void onOtherEntityLeaveSelfAOI(ObjectSet<MoveEntity> leaveSelfAOISets) {
        long key = MathUtils.getCompositeIndex(scene.instanceID, entityID);
        IntSet entityIDSet = new IntSet();
        leaveSelfAOISets.foreachImmutable(v -> entityIDSet.add(v.entityID));
        sceneEntityWorld.postEvent2SceneAI(Entity2AIEvent.OtherEntityLeaveAOI, key, entityIDSet);
    }

    @Override
    public void onTick(int escapedMillTime) {
        //TODO 考虑放到btInfo里面
//        if (btInfo.moveTickCount > 0) {
//            btInfo.moveTickCount--;
//            curPos.copy(this.nextFramePos(escapedMillTime));
//            if (btInfo.moveTickCount == 0) {
//                //push move stop
//            }
//            PbEntity.S2CEntityMoveInfo moveInfoPush = PbUtils.obj2pbEntityMoveInfo(this);
//            BroadCastUtils.broadCastToAOIPlayers(this, ScenePtCode.S2CEntityMoveInfo, moveInfoPush, true);
//        }
//
//        if (isMoving == true && btInfo.aiIsReachTargetPos == false) {
//            float dis = this.nextFrameDis(escapedMillTime);
//            if (Vector3Utils.distance3DSquare(curPos, btInfo.aiTargetPos) > dis * dis) {
//                Vector3Utils.moveTowardsPos(tmpVector, this.curPos, this.direction, false, dis);
//                curPos.copy(tmpVector);
//            } else {
//                curPos.copy(btInfo.aiTargetPos);
//                btInfo.aiIsReachTargetPos = true;
//            }
//            PbEntity.S2CEntityMoveInfo moveInfoPush = PbUtils.obj2pbEntityMoveInfo(this);
//            BroadCastUtils.broadCastToAOIPlayers(this, ScenePtCode.S2CEntityMoveInfo, moveInfoPush, true);
//        }
        //死亡后不执行后续tick
        if (fightAllLogic.stateFightLogic.getState(StateConst.Death)) {
            if ((deadBodyKeepTime -= escapedMillTime) <= 0) {
                //尸体从从场景删除
                scene.removeEntityNextTick(this);
            }
        } else {
            super.onTick(escapedMillTime);
        }
    }

    @Override
    public void onSecond() {
    }
}
