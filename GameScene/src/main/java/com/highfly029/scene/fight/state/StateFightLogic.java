package com.highfly029.scene.fight.state;

import com.highfly029.common.template.state.StateConst;
import com.highfly029.scene.fight.BaseFightLogic;
import com.highfly029.scene.fight.FightAllLogic;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.utils.collection.IntBooleanMap;

/**
 * @ClassName StateFightLogic
 * @Description 状态数据逻辑
 * @Author liyunpeng
 **/
public class StateFightLogic extends BaseFightLogic {
    //当前状态组
    private boolean[] stateValues;
    //上一帧的状态
    private boolean[] lastStateValues;
    //推送
    private boolean isChange;

    //上一次dispatch的状态ID
    private short[] lastDispatchStateIDArray;
    //上一次dispatch的状态值
    private boolean[] lastDispatchStateValueArray;

    /************* 缓存 *****************/
    private final IntBooleanMap tmpSendSelfMap = new IntBooleanMap();
    private final IntBooleanMap tmpSendOtherMap = new IntBooleanMap();
    private final IntBooleanMap tmpSendMasterMap = new IntBooleanMap();

    public StateFightLogic(FightAllLogic logic) {
        super(logic);
        init();
    }

    /**
     * 初始化
     */
    private void init() {
        int arraySize = ConfigTool.getStateConfig().getStateArraySize();

        stateValues = new boolean[arraySize];
        lastStateValues = new boolean[arraySize];

        lastDispatchStateIDArray = new short[arraySize];
        lastDispatchStateValueArray = new boolean[arraySize];
    }

    /**
     * 设置状态
     *
     * @param type
     * @param value
     */
    public void setState(int type, boolean value) {
        //如果状态变化了
        if (stateValues[type] != value) {
            stateValues[type] = value;
            isChange = true;
        }
    }

    /**
     * 获取状态
     *
     * @param type
     * @return
     */
    public boolean getState(int type) {
        return stateValues[type];
    }

    /**
     * 刷新状态
     */
    public void refreshState() {
        if (isChange) {
            sendAndDispatchAllState();
        }
    }

    @Override
    public void onTick(int escapedMillTime) {
        refreshState();
    }

    /**
     * 推送变化的状态
     */
    public void sendAndDispatchAllState() {
        StateConfig stateConfig = ConfigTool.getStateConfig();
        isChange = false;
        boolean[] lastStateValues = this.lastStateValues;
        boolean[] stateValues = this.stateValues;

        short[] tmpMaybeSendNecessaryList = stateConfig.getMaybeSendNecessaryList();

        boolean[] tmpSendSelfBoolArray = stateConfig.getSendSelfBoolArray();
        boolean[] tmpSendOtherBoolArray = stateConfig.getSendOtherBoolArray();
        boolean[] tmpSendMasterBoolArray = stateConfig.getSendMasterBoolArray();

        boolean value;
        short stateID;
        for (int i = 0, len = tmpMaybeSendNecessaryList.length; i < len; i++) {
            stateID = tmpMaybeSendNecessaryList[i];
            if (lastStateValues[stateID] != (value = stateValues[stateID])) {
                if (tmpSendSelfBoolArray[stateID]) {
                    tmpSendSelfMap.put(stateID, value);
                }
                if (tmpSendOtherBoolArray[stateID]) {
                    tmpSendOtherMap.put(stateID, value);
                }
                if (tmpSendMasterBoolArray[stateID]) {
                    tmpSendMasterMap.put(stateID, value);
                }
            }
        }

        CharacterEntity characterEntity = parentLogic.characterEntity;
        //如果推送自己
        if (!tmpSendSelfMap.isEmpty()) {
            characterEntity.onStatePushSelf(tmpSendSelfMap);
            tmpSendSelfMap.clear();
        }
        //如果推送别人
        if (!tmpSendOtherMap.isEmpty()) {
            characterEntity.onStatePushOther(tmpSendOtherMap);
            tmpSendOtherMap.clear();
        }
        //如果只推送主人
        if (!tmpSendMasterMap.isEmpty()) {
            characterEntity.onStatePushMaster(tmpSendMasterMap);
            tmpSendMasterMap.clear();
        }

        short[] tmpLastDispatchStateIDArray = this.lastDispatchStateIDArray;
        boolean[] tmpLastDispatchStateValueArray = this.lastDispatchStateValueArray;
        int num = 0;
        boolean lastValue;
        for (int i = 0, len = stateValues.length; i < len; i++) {
            stateID = (short) i;
            if ((lastValue = lastStateValues[stateID]) != (value = stateValues[stateID])) {
                tmpLastDispatchStateIDArray[num] = stateID;
                tmpLastDispatchStateValueArray[num] = lastValue;
                num++;
                lastStateValues[stateID] = value;
            }
        }
        if (num > 0) {
            characterEntity.onStateChange(num, tmpLastDispatchStateIDArray, tmpLastDispatchStateValueArray);
        }
    }

    /**
     * 是否活着
     *
     * @return
     */
    public boolean isAlive() {
        return !getState(StateConst.Death);
    }

    /**
     * 不可成为目标
     *
     * @return
     */
    public boolean cantBeTarget() {
        return getState(StateConst.CantBeTarget);
    }

    /**
     * 不可被攻击
     *
     * @return
     */
    public boolean cantBeAttackTarget() {
        return getState(StateConst.CantBeAttackTarget);
    }

    /**
     * 不可移动
     *
     * @return
     */
    public boolean cantMove() {
        return getState(StateConst.Death) || getState(StateConst.CantMove) || getState(StateConst.Vertigo);
    }

    /**
     * 不可转身
     *
     * @return
     */
    public boolean cantTurn() {
        return getState(StateConst.Death) || getState(StateConst.CantTurn) || getState(StateConst.Vertigo);
    }

    /**
     * 不可攻击
     *
     * @return
     */
    public boolean cantAttack() {
        return getState(StateConst.Death) || getState(StateConst.CantAttack) || getState(StateConst.Vertigo);
    }

    /**
     * 不可施法
     *
     * @return
     */
    public boolean cantCast() {
        return getState(StateConst.Death) || getState(StateConst.CantCast) || getState(StateConst.Vertigo);
    }

    /**
     * 不可成为敌方目标
     *
     * @return
     */
    public boolean cantBeEnemyTarget() {
        return getState(StateConst.CantBeEnemyTarget) || getState(StateConst.Invincible);
    }

    /**
     * 控制免疫
     *
     * @return
     */
    public boolean isControlImmune() {
        return getState(StateConst.ControlImmune) || getState(StateConst.Invincible);
    }

    /**
     * 伤害免疫
     *
     * @return
     */
    public boolean isDamageImmune() {
        return getState(StateConst.DamageImmune) || getState(StateConst.Invincible);
    }

    /**
     * 物理免疫
     *
     * @return
     */
    public boolean isPhysicalImmune() {
        return getState(StateConst.PhysicalImmune) || getState(StateConst.DamageImmune) || getState(StateConst.Invincible);
    }

    /**
     * 魔法免疫
     *
     * @return
     */
    public boolean isMagicImmune() {
        return getState(StateConst.MagicImmune) || getState(StateConst.DamageImmune) || getState(StateConst.Invincible);
    }

    /**
     * 无法被伤害击杀
     *
     * @return
     */
    public boolean cantBeKillByDamage() {
        return getState(StateConst.CantBeKillByDamage) || getState(StateConst.Invincible);
    }

    /**
     * 不可被治疗
     *
     * @return
     */
    public boolean cantBeHeal() {
        return getState(StateConst.CantBeHeal);
    }
}
