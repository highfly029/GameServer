package com.highfly029.scene.fight.attribute;

import com.highfly029.common.template.attribute.AttributeInfluenceTemplate;
import com.highfly029.common.template.attribute.AttributeTemplate;
import com.highfly029.common.template.commonType.PushTypeConst;
import com.highfly029.common.template.race.RaceConst;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.scene.exception.StartUpException;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName AttributeConfig
 * @Description 属性配置数据
 * @Author liyunpeng
 **/
public class AttributeConfig extends BaseGlobalConfig {

    //属性数组大小=最大属性ID + 1
    private short attrArraySize;

    //当前属性 -> 最大值属性
    private short[] currentToMaxArray;
    //最大值属性 -> 当前属性
    private short[] maxToCurrentArray;
    //是否是当前属性
    private boolean[] currentBoolArray;
    //是否是最大值属性
    private boolean[] maxBoolArray;
    //当前值的最大值list
    private short[] currentMaxList;
    //最大值索引->currentMaxList
    private short[] currentMaxIndex;


    //当前属性 -> 自增属性
    private short[] currentToIncreaseArray;
    //自增属性 -> 当前属性
    private short[] increaseToCurrentArray;
    //是否是自增属性
    private boolean[] increaseBoolArray;
    //必要的自增属性数组
    private short[] increaseNecessaryList;
    //必要的自增当前属性数组
    private short[] increaseCurrentNecessaryList;

    //计算公式 attrID -> formula[]
    private int[][] formulaArray;
    //影响其他属性的数组 attrID -> otherAttrID[]
    private short[][] influenceOtherAttrArray;

    //是否需要逻辑层通知
    private boolean[] dispatchBoolArray;
    //必要的逻辑通知属性
    private short[] dispatchNecessaryList;

    //属性最大值 >0才有效
    private int[] maxValueArray;

    //是否是万分比属性
    private boolean[] percentBoolArray;

    //是否是可能推送的属性
    private boolean[] maybeSendBoolArray;
    //必要的可能推送属性组
    private short[] maybeSendNecessaryList;
    //是否是推送给自己的属性
    private boolean[] sendSelfBoolArray;
    //是否是推送给别人的属性
    private boolean[] sendOtherBoolArray;
    //是否是只推送给master的属性
    private boolean[] sendMasterBoolArray;

    //既是可能发送又是通知的属性
    private boolean[] maybeSendAndDispatchBoolArray;


    //一级属性影响的二级属性组 int[种族][一级属性][一级属性影响的二级组属性]
    private int[][][] attributeInfluenceArray;
    //二级属性被影响的因子 int[种族][一级属性影响的二级组属性][一级属性][因子]
    private int[][][][] attributeBeInfluenceArray;


    @Override
    public void check(boolean isHotfix) {

    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> attributeList = TemplateTool.dataTemplates.get(AttributeTemplate.class);
        short attrSize = 0;
        AttributeTemplate template;
        for (int i = 0, size = attributeList.size(); i < size; i++) {
            template = (AttributeTemplate) attributeList.get(i);
            if (template.getAttributeID() > attrSize) {
                attrSize = template.getAttributeID();
            }
        }
        attrSize++;
        attrArraySize = attrSize;

        currentToMaxArray = new short[attrSize];
        maxToCurrentArray = new short[attrSize];
        currentBoolArray = new boolean[attrSize];
        maxBoolArray = new boolean[attrSize];
        currentMaxIndex = new short[attrSize];
        currentToIncreaseArray = new short[attrSize];
        increaseToCurrentArray = new short[attrSize];
        increaseBoolArray = new boolean[attrSize];

        formulaArray = new int[attrSize][];
        influenceOtherAttrArray = new short[attrSize][];

        maxValueArray = new int[attrSize];

        dispatchBoolArray = new boolean[attrSize];
        percentBoolArray = new boolean[attrSize];
        maybeSendBoolArray = new boolean[attrSize];
        sendSelfBoolArray = new boolean[attrSize];
        sendOtherBoolArray = new boolean[attrSize];
        sendMasterBoolArray = new boolean[attrSize];
        maybeSendAndDispatchBoolArray = new boolean[attrSize];

        IntObjectMap<IntSet> influenceOtherAttrMap = new IntObjectMap<>();

        IntList currentMaxValueList = new IntList();
        IntList increaseList = new IntList();
        IntList increaseCurrentList = new IntList();
        IntList dispatchList = new IntList();
        IntList maybeSendList = new IntList();

        for (int i = 0, size = attributeList.size(); i < size; i++) {
            template = (AttributeTemplate) attributeList.get(i);
            short attributeID = template.getAttributeID();
            //当前属性
            if (template.isCurrent()) {
                short currentMaxID = template.getCurrentMaxID();
                currentToMaxArray[attributeID] = currentMaxID;
                maxToCurrentArray[currentMaxID] = attributeID;

                currentBoolArray[attributeID] = true;
                maxBoolArray[currentMaxID] = true;

                currentMaxValueList.add(currentMaxID);
            }

            //会自增当前属性
            if (template.isAutoIncreaseCurrent()) {
                short autoIncreaseCurrentID = template.getAutoIncreaseCurrentID();

                currentToIncreaseArray[autoIncreaseCurrentID] = attributeID;
                increaseToCurrentArray[attributeID] = autoIncreaseCurrentID;

                increaseList.add(attributeID);
                increaseCurrentList.add(autoIncreaseCurrentID);

                increaseBoolArray[attributeID] = true;
            }

            maxValueArray[attributeID] = template.getMaxValue();

            //计算公式
            if (template.getFormula() != null && template.getFormula().length > 0) {
                formulaArray[attributeID] = template.getFormula();
                for (int j = 1, jLen = template.getFormula().length; j < jLen; j++) {
                    int attrID = template.getFormula()[j];
                    IntSet intSet = influenceOtherAttrMap.get(attrID);
                    if (intSet == null) {
                        intSet = new IntSet();
                        influenceOtherAttrMap.put(attrID, intSet);
                    }
                    intSet.add(attributeID);
                }

                if (template.getMaxValue() > 0) {
                    throw new StartUpException("attributeID=" + attributeID + " maxValue work when formula is null");
                }
            }

            if (template.isDispatch()) {
                dispatchBoolArray[attributeID] = true;
                dispatchList.add(attributeID);
            }

            byte pushType = template.getPushType();
            if (pushType == PushTypeConst.Self || pushType == PushTypeConst.All) {
                sendSelfBoolArray[attributeID] = true;
            }

            if (pushType == PushTypeConst.Other || pushType == PushTypeConst.All) {
                sendOtherBoolArray[attributeID] = true;
            }

            if (pushType == PushTypeConst.Master) {
                sendMasterBoolArray[attributeID] = true;
            }

            if (pushType != PushTypeConst.Off) {
                maybeSendBoolArray[attributeID] = true;
                maybeSendList.add(attributeID);

                if (dispatchBoolArray[attributeID]) {
                    maybeSendAndDispatchBoolArray[attributeID] = true;
                }
            }


            if (template.isPercent() == true) {
                percentBoolArray[attributeID] = true;
            }
        }

        influenceOtherAttrMap.foreachImmutable((k, v) -> {
            int attrID = k;
            int size = v.size();
            influenceOtherAttrArray[attrID] = new short[size];
            int freeValue = v.getFreeValue();
            int[] keys = v.getKeys();
            int key;
            int index = 0;
            for (int i = 0, len = keys.length; i < len; i++) {
                if ((key = keys[i]) != freeValue) {
                    influenceOtherAttrArray[attrID][index++] = (short) key;
                }
            }
        });

        currentMaxList = new short[currentMaxValueList.size()];
        for (int i = 0, len = currentMaxList.length; i < len; i++) {
            short maxAttrID = (short) currentMaxValueList.get(i);
            currentMaxList[i] = maxAttrID;
            currentMaxIndex[maxAttrID] = (short) i;
        }

        increaseNecessaryList = new short[increaseList.size()];
        for (int i = 0, len = increaseNecessaryList.length; i < len; i++) {
            increaseNecessaryList[i] = (short) increaseList.get(i);
        }

        increaseCurrentNecessaryList = new short[increaseCurrentList.size()];
        for (int i = 0, len = increaseCurrentNecessaryList.length; i < len; i++) {
            increaseCurrentNecessaryList[i] = (short) increaseCurrentList.get(i);
        }

        dispatchNecessaryList = new short[dispatchList.size()];
        for (int i = 0, len = dispatchNecessaryList.length; i < len; i++) {
            dispatchNecessaryList[i] = (short) dispatchList.get(i);
        }

        maybeSendNecessaryList = new short[maybeSendList.size()];
        for (int i = 0, len = maybeSendNecessaryList.length; i < len; i++) {
            maybeSendNecessaryList[i] = (short) maybeSendList.get(i);
        }


        //种族数量
        int raceArraySize = RaceConst.count + 1;
        attributeInfluenceArray = new int[raceArraySize][][];
        attributeBeInfluenceArray = new int[raceArraySize][][][];

        ObjectList<BaseTemplate> influenceList = TemplateTool.dataTemplates.get(AttributeInfluenceTemplate.class);
        AttributeInfluenceTemplate influenceTemplate;

        IntObjectMap<IntObjectMap<IntList>> tmpRaceInfluenceArray = new IntObjectMap<>(IntObjectMap[]::new);
        IntObjectMap<IntObjectMap<ObjectList>> tmpRaceBeInfluenceArray = new IntObjectMap<>(IntObjectMap[]::new);

        for (int i = 0, len = influenceList.size(); i < len; i++) {
            influenceTemplate = (AttributeInfluenceTemplate) influenceList.get(i);
            byte race = influenceTemplate.getRace();
            IntObjectMap<IntList> tmpMap = tmpRaceInfluenceArray.get(race);
            if (tmpMap == null) {
                tmpMap = new IntObjectMap<>(IntList[]::new);
                tmpRaceInfluenceArray.put(race, tmpMap);
            }
            int attrID = influenceTemplate.getAttrID();
            IntList tmpList = tmpMap.get(attrID);
            if (tmpList == null) {
                tmpList = new IntList();
                tmpMap.put(attrID, tmpList);
            }

            IntObjectMap<ObjectList> tmpBeMap = tmpRaceBeInfluenceArray.get(race);
            if (tmpBeMap == null) {
                tmpBeMap = new IntObjectMap<>(ObjectList[]::new);
                tmpRaceBeInfluenceArray.put(race, tmpBeMap);
            }
            int[][] factorArray = influenceTemplate.getInfluenceFactor();
            for (int j = 0, jLen = factorArray.length; j < jLen; j++) {
                int[] params = factorArray[j];
                int beAttrID = params[2];
                tmpList.add(beAttrID);
                ObjectList objectList = tmpBeMap.get(beAttrID);
                if (objectList == null) {
                    objectList = new ObjectList();
                    tmpBeMap.put(beAttrID, objectList);
                }
                int[] array = new int[3];
                for (int k = 0, kLen = params.length; k < kLen; k++) {
                    array[k] = params[k];
                }
                array[2] = attrID;
                objectList.add(array);
            }
        }

        for (int i = 0, len = attributeInfluenceArray.length; i < len; i++) {
            IntObjectMap<IntList> tmpMap = tmpRaceInfluenceArray.get(i);
            if (tmpMap != null && tmpMap.size() > 0) {
                attributeInfluenceArray[i] = new int[attrSize][];
                int race = i;
                tmpMap.foreachImmutable((k, v) -> {
                    attributeInfluenceArray[race][k] = new int[v.size()];
                    for (int j = 0, jLen = v.size(); j < jLen; j++) {
                        attributeInfluenceArray[race][k][j] = v.get(j);
                    }
                });
            }
        }

        for (int i = 0, len = attributeBeInfluenceArray.length; i < len; i++) {
            IntObjectMap<ObjectList> tmpBeMap = tmpRaceBeInfluenceArray.get(i);
            if (tmpBeMap != null && tmpBeMap.size() > 0) {
                int race = i;
                attributeBeInfluenceArray[i] = new int[attrSize][][];
                tmpBeMap.foreachImmutable((k, v) -> {
                    attributeBeInfluenceArray[race][k] = new int[v.size()][];
                    if (formulaArray[k] == null) {
                        throw new StartUpException("attributeID=" + k + " must be formula, because race=" + race + " influence");
                    }
                    for (int j = 0, jLen = v.size(); j < jLen; j++) {
                        int[] array = (int[]) v.get(j);
                        attributeBeInfluenceArray[race][k][j] = array;
                    }
                });
            }
        }
    }

    public short getAttrArraySize() {
        return attrArraySize;
    }

    public short[] getCurrentToMaxArray() {
        return currentToMaxArray;
    }

    public short[] getMaxToCurrentArray() {
        return maxToCurrentArray;
    }

    public boolean[] getCurrentBoolArray() {
        return currentBoolArray;
    }

    public boolean[] getMaxBoolArray() {
        return maxBoolArray;
    }

    public short[] getCurrentMaxList() {
        return currentMaxList;
    }

    public short[] getCurrentMaxIndex() {
        return currentMaxIndex;
    }

    public short[] getCurrentToIncreaseArray() {
        return currentToIncreaseArray;
    }

    public short[] getIncreaseToCurrentArray() {
        return increaseToCurrentArray;
    }

    public boolean[] getIncreaseBoolArray() {
        return increaseBoolArray;
    }

    public short[] getIncreaseNecessaryList() {
        return increaseNecessaryList;
    }

    public short[] getIncreaseCurrentNecessaryList() {
        return increaseCurrentNecessaryList;
    }

    public int[][] getFormulaArray() {
        return formulaArray;
    }

    public short[][] getInfluenceOtherAttrArray() {
        return influenceOtherAttrArray;
    }

    public boolean[] getDispatchBoolArray() {
        return dispatchBoolArray;
    }

    public short[] getDispatchNecessaryList() {
        return dispatchNecessaryList;
    }

    public int[] getMaxValueArray() {
        return maxValueArray;
    }

    public boolean[] getPercentBoolArray() {
        return percentBoolArray;
    }

    public boolean[] getMaybeSendBoolArray() {
        return maybeSendBoolArray;
    }

    public short[] getMaybeSendNecessaryList() {
        return maybeSendNecessaryList;
    }

    public boolean[] getSendSelfBoolArray() {
        return sendSelfBoolArray;
    }

    public boolean[] getSendOtherBoolArray() {
        return sendOtherBoolArray;
    }

    public boolean[] getSendMasterBoolArray() {
        return sendMasterBoolArray;
    }

    public boolean[] getMaybeSendAndDispatchBoolArray() {
        return maybeSendAndDispatchBoolArray;
    }

    public int[][][] getAttributeInfluenceArray() {
        return attributeInfluenceArray;
    }

    public int[][][][] getAttributeBeInfluenceArray() {
        return attributeBeInfluenceArray;
    }
}
