package com.highfly029.scene.fight.scene;

import java.util.concurrent.TimeUnit;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.template.entity.EntityCreateSourceConst;
import com.highfly029.common.template.entity.EntityIDGeneratorConst;
import com.highfly029.common.template.entity.EntityTypeConst;
import com.highfly029.common.template.monster.DropTypeConst;
import com.highfly029.common.template.scene.SceneFightModeConst;
import com.highfly029.common.template.scene.SceneTemplate;
import com.highfly029.common.template.sceneEditorEntity.SceneEditorEntityTemplate;
import com.highfly029.common.template.sceneMap.SceneMapTemplate;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.scene.exception.SceneException;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.fight.entity.DropEntity;
import com.highfly029.scene.fight.entity.Entity;
import com.highfly029.scene.fight.entity.EntityFactory;
import com.highfly029.scene.fight.entity.FixedPosEntity;
import com.highfly029.scene.fight.entity.MonsterEntity;
import com.highfly029.scene.fight.entity.MoveEntity;
import com.highfly029.scene.fight.entity.PlayerEntity;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.utils.NavMeshUtils;
import com.highfly029.scene.world.SceneEntityWorld;
import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.IntLongMap;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName Scene
 * @Description 场景信息类
 * @Author liyunpeng
 **/
public abstract class Scene {

    //场景实体世界
    private SceneEntityWorld sceneEntityWorld;
    //场景全部实体对象集合
    public IntObjectMap<Entity> allEntity = new IntObjectMap<>(Entity[]::new);
    //场景全部实体类型集合
    public IntObjectMap<IntSet> allTypeEntityID = new IntObjectMap<>(IntSet[]::new);
    //场景全部玩家集合
    public IntObjectMap<PlayerEntity> allPlayerEntity = new IntObjectMap<>(PlayerEntity[]::new);
    //场景全部不可移动实体对象
    public IntObjectMap<FixedPosEntity> allFixedPosEntity = new IntObjectMap<>(FixedPosEntity[]::new);
    //场景全部可移动实体对象
    public IntObjectMap<MoveEntity> allMoveEntity = new IntObjectMap<>(MoveEntity[]::new);
    //场景自增实例id
    public static int sceneIncreaseID;
    // 场景实例ID，非场景配表ID
    public int instanceID;
    //场景ID
    public int sceneID;
    //线ID
    public int lineID;
    //场景类型
    public int sceneType;
    //LoadBalance创建场景时的key
    private long key;
    // 场景内其他实体id 自增索引
    private int otherEntityIDIndex = EntityIDGeneratorConst.OtherEntityIDMin;
    //玩家实体id 自增索引
    private int playerEntityIDIndex = EntityIDGeneratorConst.PlayerEntityIDMin;
    // 场景地图信息
    public SceneMapGrid sceneMapGrid;
    //场景模板信息
    public SceneTemplate sceneTemplate;
    //阻挡数据
    public byte navMeshMode;
    public String navMeshName;
    //场景删除时间
    private long removeEndTime;
    /**
     * 怪物复活集合
     */
    public IntLongMap monsterReviveMap = new IntLongMap();

    public void setSceneEntityWorld(SceneEntityWorld sceneEntityWorld) {
        this.sceneEntityWorld = sceneEntityWorld;
    }


    public long getKey() {
        return key;
    }

    public void setKey(long key) {
        this.key = key;
    }

    /**
     * 场景初始化
     *
     * @param template
     * @return 是否成功
     */
    public boolean initWithTemplate(SceneTemplate template) {
        SceneMapTemplate sceneMapTemplate = ConfigTool.getSceneConfig().getSceneMapTemplate(template.getMapID());
        this.sceneTemplate = template;
        this.sceneType = template.getSceneType();
        this.instanceID = ++sceneIncreaseID;
        this.sceneID = template.getSceneID();
        this.sceneMapGrid = SceneMapGrid.create();
        this.navMeshMode = sceneMapTemplate.isTileCache() ? NavMeshUtils.TILECACHE_MODE : NavMeshUtils.SOLOMESH_MODE;
        this.navMeshName = String.valueOf(sceneMapTemplate.getMapID());

        this.sceneMapGrid.init(sceneMapTemplate.getMapSize());
        return true;
    }

    /**
     * 增加预设实体
     */
    public void addPreSetEntity() {
        IntObjectMap<SceneEditorEntityTemplate> map = ConfigTool.getSceneConfig().getSceneEditorEntityTemplate(sceneTemplate.getSceneID());
        if (map != null) {
            map.foreachImmutable((k, v) -> {
                if (v.isLoading()) {
                    Entity entity = createEntityWithExcel(v);
                    this.addEntity(entity);
                }
            });
        }
    }

    /**
     * entityID生成器
     *
     * @param createSource
     * @param type
     * @param instanceID
     * @return
     */
    private int generateEntityID(byte createSource, int type, int instanceID) {
        int entityID = 0;
        switch (createSource) {
            case EntityCreateSourceConst.Editor, EntityCreateSourceConst.Excel -> {
                entityID = instanceID;
            }
            case EntityCreateSourceConst.Dynamic -> {
                if (type == EntityTypeConst.PLAYER) {
                    entityID = playerEntityIDIndex++;
                    if (playerEntityIDIndex >= EntityIDGeneratorConst.OtherEntityIDMin) {
                        playerEntityIDIndex = EntityIDGeneratorConst.PlayerEntityIDMin;
                    }
                } else {
                    entityID = otherEntityIDIndex++;
                    if (otherEntityIDIndex == Integer.MAX_VALUE) {
                        otherEntityIDIndex = EntityIDGeneratorConst.OtherEntityIDMin;
                    }
                }
            }
        }
        return entityID;
    }

    /**
     * entityID是否自动生成
     *
     * @param entityID
     * @return
     */
    public boolean isDynamicEntityID(int entityID) {
        return entityID >= EntityIDGeneratorConst.OtherEntityIDMin;
    }

    /**
     * 根据编辑来源创建实体
     *
     * @return
     */
    public Entity createEntityWithExcel(SceneEditorEntityTemplate template) {
        Vector3 pos = Vector3.createWithFloats(template.getPos());
        Vector3 dir = Vector3.createWithFloats(template.getDir());

        Entity entity = EntityFactory.createEntity(template.getEntityType(), sceneEntityWorld);
        entity.entityID = generateEntityID(EntityCreateSourceConst.Excel, template.getEntityType(), template.getEntityID());
        entity.type = template.getEntityType();
        entity.setCurPos(pos);
        entity.setDirection(dir);
        entity.init(template.getTemplateID());
        entity.initEntityBeforeAddToScene();
        return entity;
    }

    /**
     * 在该场景创建一个实体
     *
     * @param type
     * @param pos
     * @param direction
     * @param isAutoEnterScene 是否创建完自动加入场景
     * @return
     */
    public Entity createEntity(byte type, Vector3 pos, Vector3 direction, boolean isAutoEnterScene) {
        Entity entity = EntityFactory.createEntity(type, sceneEntityWorld);
        entity.entityID = generateEntityID(EntityCreateSourceConst.Dynamic, type, 0);
        entity.type = type;
        entity.setCurPos(pos);
        if (direction != null) {
            entity.setDirection(direction);
        }

        entity.initEntityBeforeAddToScene();

        if (isAutoEnterScene) {
            this.addEntity(entity);
        }
        return entity;
    }

    /**
     * 在该场景创建一个实体 默认添加到场景
     *
     * @param type
     * @param pos
     * @param direction
     * @param templateID 对应类型的模版id
     * @return entity
     */
    public Entity createEntity(int type, Vector3 pos, Vector3 direction, int templateID) {
        Entity entity = EntityFactory.createEntity(type, sceneEntityWorld);
        entity.entityID = generateEntityID(EntityCreateSourceConst.Dynamic, type, 0);
        entity.type = type;
        entity.setCurPos(pos);
        if (direction != null) {
            entity.setDirection(direction);
        }
        entity.init(templateID);
        entity.initEntityBeforeAddToScene();
        addEntity(entity);
        return entity;
    }

    /**
     * 场景增加一个实体
     *
     * @param entity
     * @return
     */
    public boolean addEntity(Entity entity) {
        int entityID = entity.entityID;
        entity.scene = this;
        if (allEntity.containsKey(entityID)) {
            SceneLoggerTool.gameLogger.error("addEntityToScene duplicate entityID", new SceneException(entity.toString()));
            return false;
        } else {
            int gridID = sceneMapGrid.getGridID(entity.curPos);
            if (gridID == SceneMapGrid.INVALID_GRID_ID) {
                SceneLoggerTool.gameLogger.error("addEntityToScene invalid curPos entityID={}, curPos={}", entityID, entity.curPos);
                return false;
            }
            sceneMapGrid.addEntityToScene(gridID, entity);
            allEntity.put(entityID, entity);
            IntSet idSet = allTypeEntityID.get(entity.type);
            if (idSet == null) {
                idSet = new IntSet();
                allTypeEntityID.put(entity.type, idSet);
            }
            idSet.add(entityID);
            if (entity.isFixedPosEntity()) {
                allFixedPosEntity.put(entityID, (FixedPosEntity) entity);
            } else if (entity.isMoveEntity()) {
                allMoveEntity.put(entityID, (MoveEntity) entity);
                if (entity.type == EntityTypeConst.PLAYER) {
                    allPlayerEntity.put(entityID, (PlayerEntity) entity);
                }
            }
            onEntityEnterScene(entity);
            //玩家入场，重置删除场景标记
            if (entity.type == EntityTypeConst.PLAYER) {
                removeEndTime = 0;
            }
            return true;
        }
    }

    /**
     * 下一帧删除一个实体
     *
     * @param entity
     * @return
     */
    public void removeEntityNextTick(Entity entity) {
        entity.isRemoveNextTick = true;
    }

    /**
     * 立刻删除
     *
     * @param entityID
     */
    public void removeEntity(int entityID) {
        Entity entity = allEntity.remove(entityID);
        sceneMapGrid.delEntityFromScene(entity);
        IntSet idSet = allTypeEntityID.get(entity.type);
        if (idSet != null) {
            idSet.remove(entityID);
        }
        if (entity.type == EntityTypeConst.PLAYER) {
            allPlayerEntity.remove(entityID);
        }
        if (entity.isFixedPosEntity()) {
            allFixedPosEntity.remove(entityID);
        } else if (entity.isMoveEntity()) {
            allMoveEntity.remove(entityID);
        }
        onEntityLeaveScene(entity);
        entity.scene = null;

        //触发场景没有玩家后的删除检测
        if (isNeedRemove()) {
            removeEndTime = SystemTimeTool.getMillTime() + TimeUnit.MINUTES.toMillis(sceneTemplate.getKeepTime());
        }
    }

    /**
     * 场景是否需要删除
     * @return
     */
    protected boolean isNeedRemove() {
        if (allPlayerEntity.isEmpty() && sceneTemplate.getKeepTime() > 0) {
            return true;
        }
        return false;
    }

    public long getRemoveEndTime() {
        return removeEndTime;
    }

    /**
     * 修改不移动entity的位置
     *
     * @param entityID
     * @param pos
     */
    public void changeFixedPosEntityPos(int entityID, Vector3 pos) {
        FixedPosEntity fixedPosEntity = allFixedPosEntity.get(entityID);
        if (fixedPosEntity == null) {
            return;
        }
        //TODO check pos
        fixedPosEntity.curPos.copy(pos);
        fixedPosEntity.onPosChanged();
    }

    /**
     * entity进入一个场景通知
     *
     * @param entity
     */
    protected void onEntityEnterScene(Entity entity) {
//        SceneLoggerTool.gameLogger.info("onEntityEnterScene entity={}", entity);
        entity.onEnterScene(this);
    }

    /**
     * entity离开一个场景通知
     *
     * @param entity
     */
    protected void onEntityLeaveScene(Entity entity) {
//        SceneLoggerTool.gameLogger.info("onEntityLeaveScene entity={}", entity);
        entity.onLeaveScene(this);
    }

    /**
     * 战斗entity死亡通知
     *
     * @param characterEntity
     */
    public void onCharacterDeath(CharacterEntity characterEntity) {

    }

    /**
     * 战斗entity产生伤害通知
     *
     * @param attackSource
     * @param attackTarget
     * @param realDamageValue
     */
    public void onCharacterDamage(CharacterEntity attackSource, CharacterEntity attackTarget, int realDamageValue) {

    }

    /**
     * 玩家掉线
     *
     * @param playerEntity
     */
    public void onPlayerOffline(PlayerEntity playerEntity) {

    }

    /**
     * 玩家断线重连
     *
     * @param playerEntity
     */
    public void onPlayerReconnectLogin(PlayerEntity playerEntity) {

    }

    /**
     * 玩家登出
     *
     * @param playerEntity
     */
    public void onPlayerLogout(PlayerEntity playerEntity) {
        removeEntity(playerEntity.entityID);
    }

    /**
     * 是否可以复活
     *
     * @param playerEntity
     * @return
     */
    public boolean isCanReborn(PlayerEntity playerEntity) {
        return false;
    }

    /**
     * 扣除复活消耗
     *
     * @param playerEntity
     */
    public void doRebornCost(PlayerEntity playerEntity) {

    }

    /**
     * 检测是否是敌对关系
     *
     * @param sourceEntity
     * @param targetEntity
     * @return
     */
    public boolean checkIsEnemy(CharacterEntity sourceEntity, CharacterEntity targetEntity) {
        Scene scene = sourceEntity.scene;
        switch (scene.sceneTemplate.getFightMode()) {
            case SceneFightModeConst.PEACE -> {
                return false;
            }
            case SceneFightModeConst.PVP -> {
                if (sourceEntity.getMasterEntityID() != targetEntity.getMasterEntityID()) {
                    return true;
                }
            }
            case SceneFightModeConst.PVE -> {
                if ((sourceEntity.isBelongToPlayer() && targetEntity.isBelongToMonster()) ||
                        (sourceEntity.isBelongToMonster() && targetEntity.isBelongToPlayer())) {
                    return true;
                }
            }
            case SceneFightModeConst.GVG -> {
                //玩家force之间的战斗
                if (sourceEntity.isBelongToPlayer() && targetEntity.isBelongToPlayer()) {
                    if (sourceEntity.getMasterForceID() != targetEntity.getMasterForceID()) {
                        return true;
                    }
                }
            }
            case SceneFightModeConst.UVU -> {
                if (sourceEntity.isBelongToPlayer() && targetEntity.isBelongToPlayer()) {
                    if (((PlayerEntity) sourceEntity.getMasterEntity()).getMasterUnionID() != ((PlayerEntity) targetEntity.getMasterEntity()).getMasterUnionID()) {
                        return true;
                    }
                }
            }
            case SceneFightModeConst.TVT -> {
                if (sourceEntity.isBelongToPlayer() && targetEntity.isBelongToPlayer()) {
                    if (((PlayerEntity) sourceEntity.getMasterEntity()).getMasterTeamID() != ((PlayerEntity) targetEntity.getMasterEntity()).getMasterTeamID()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 检测是否是友方关系
     *
     * @param sourceEntity
     * @param targetEntity
     * @return
     */
    public boolean checkIsFriend(CharacterEntity sourceEntity, CharacterEntity targetEntity) {
        Scene scene = sourceEntity.scene;
        switch (scene.sceneTemplate.getFightMode()) {
            case SceneFightModeConst.PEACE -> {
                return true;
            }
            case SceneFightModeConst.PVP -> {
                if (sourceEntity.getMasterEntityID() != targetEntity.getMasterEntityID()) {
                    return false;
                }
            }
            case SceneFightModeConst.PVE -> {
                if ((sourceEntity.isBelongToPlayer() && targetEntity.isBelongToMonster()) ||
                        (sourceEntity.isBelongToMonster() && targetEntity.isBelongToPlayer())) {
                    return false;
                }
            }
            case SceneFightModeConst.GVG -> {
                //玩家force之间的战斗
                if (sourceEntity.isBelongToPlayer() && targetEntity.isBelongToPlayer()) {
                    if (sourceEntity.getMasterForceID() != targetEntity.getMasterForceID()) {
                        return false;
                    }
                }
            }
            case SceneFightModeConst.UVU -> {
                if (sourceEntity.isBelongToPlayer() && targetEntity.isBelongToPlayer()) {
                    if (((PlayerEntity) sourceEntity.getMasterEntity()).getMasterUnionID() != ((PlayerEntity) targetEntity.getMasterEntity()).getMasterUnionID()) {
                        return false;
                    }
                }
            }
            case SceneFightModeConst.TVT -> {
                if (sourceEntity.isBelongToPlayer() && targetEntity.isBelongToPlayer()) {
                    if (((PlayerEntity) sourceEntity.getMasterEntity()).getMasterTeamID() != ((PlayerEntity) targetEntity.getMasterEntity()).getMasterTeamID()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public int addObstacle(Vector3 pos, float radius, float height) {
        if (navMeshMode != NavMeshUtils.TILECACHE_MODE) {
            return -2;
        }
        float x = -pos.x;
        float y = pos.y;
        float z = pos.z;
        return NavMeshUtils.addObstacle(navMeshMode, navMeshName, x, y, z, radius, height);
    }

    public int addBoxObstacle(float minX, float minY, float minZ, float length, float width, float height) {
        if (navMeshMode != NavMeshUtils.TILECACHE_MODE) {
            return -2;
        }
        float maxX = -minX + length;
        float maxY = minY + height;
        float maxZ = minZ + width;
        return NavMeshUtils.addBoxObstacle(navMeshMode, navMeshName, -minX, minY, minZ, maxX, maxY, maxZ);
    }

    public boolean removeOneObstacle(int index) {
        if (navMeshMode != NavMeshUtils.TILECACHE_MODE) {
            return false;
        }
        return NavMeshUtils.removeOneObstacle(navMeshMode, navMeshName, index);
    }

    public void removeAllObstacle() {
        if (navMeshMode != NavMeshUtils.TILECACHE_MODE) {
            return;
        }
        NavMeshUtils.removeAllObstacle(navMeshMode, navMeshName);
    }

    /**
     * 复活一个怪物
     *
     * @param entityID
     */
    public void reviveMonster(int entityID) {
        SceneEditorEntityTemplate template = ConfigTool.getSceneConfig().getSceneEditorEntityTemplate(this.sceneTemplate.getSceneID()).get(entityID);
        Vector3 pos = Vector3.createWithFloats(template.getPos());
        Vector3 dir = Vector3.createWithFloats(template.getDir());
        MonsterEntity monsterEntity = (MonsterEntity) this.createEntity(EntityTypeConst.MONSTER, pos, dir, false);
        monsterEntity.init(template.getTemplateID());
        this.addEntity(monsterEntity);
    }

    /**
     * 获取出生点坐标
     *
     * @param isRandomDistance
     * @return
     */
    public Vector3 getBirthPos(boolean isRandomDistance) {
        Vector3 vector3 = Vector3.create();
        float[] array = sceneTemplate.getBirthPos();
        vector3.set(array[0], array[1], array[2]);
        if (isRandomDistance) {
            vector3.x = vector3.x + (RandomUtils.randomBoolean() ? 1 : -1) * RandomUtils.randomInt(sceneTemplate.getRandomDistance());
//            vector3.y = vector3.y +  (RandomUtils.randomBoolean() ? 1 : -1) * RandomUtils.randomInt(sceneTemplate.randomDistance);
            vector3.z = vector3.z + (RandomUtils.randomBoolean() ? 1 : -1) * RandomUtils.randomInt(sceneTemplate.getRandomDistance());

        }

        return vector3;
    }

    /**
     * 获取玩家实体
     *
     * @param playerID
     * @return
     */
    public PlayerEntity getPlayerEntity(long playerID) {
        IntObjectMap<PlayerEntity> allEntity = this.allPlayerEntity;
        int freeValue = allEntity.getFreeValue();
        int[] keys = allEntity.getKeys();
        PlayerEntity[] values = allEntity.getValues();
        int key;
        PlayerEntity value;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                if ((value = values[i]) != null) {
                    if (value.playerID == playerID) {
                        return value;
                    }
                }
            }
        }
        return null;
    }

    /**
     * 拾取场景掉落物
     *
     * @param entityID
     */
    public void pickUpDropItems(int entityID) {
        PlayerEntity playerEntity = null;
        Entity entity = playerEntity.scene.allEntity.get(entityID);

        if (entity == null) {
            SceneLoggerTool.gameLogger.error("pickUpDropItems entity is null,playerID={}, sceneID={}, entityID={}", playerEntity.playerID, playerEntity.scene.sceneID, entityID);
            return;
        }

        IntIntMap dropList = null;
        if (entity.isMoveEntity()) {
            //掉落物在怪物尸体上
            MonsterEntity monsterEntity = (MonsterEntity) entity;
            if (monsterEntity.monsterTemplate.getDropType() != DropTypeConst.DeadBody) {
                SceneLoggerTool.gameLogger.error("pickUpDropItems dropType is invalid,playerID={}, sceneID={}, entityID={}", playerEntity.playerID, playerEntity.scene.sceneID, entityID);
                return;
            }
            if (monsterEntity.monsterTemplate.isDropUnique()) {
                boolean isHaveTeam = true;
                if (isHaveTeam && monsterEntity.monsterTemplate.isDropTeamMode()) {
                    //TODO 按照队伍设置的分配模式 分配掉落物

                } else {
                    //个人直接领取
                    dropList = monsterEntity.getSingleDropList();
                    if (dropList == null || dropList.size() == 0) {
                        SceneLoggerTool.gameLogger.error("pickUpDropItems DeadBody single not exist. playerID={},sceneID={}", playerEntity.playerID, playerEntity.scene.sceneID);
                        return;
                    }
                    //移除掉落
                    monsterEntity.removeSingleDropList();
                }
            } else {
                //每个人各自领取自己的掉落物
                dropList = monsterEntity.getPlayerDropList(playerEntity.playerID);
                if (dropList == null || dropList.size() == 0) {
                    SceneLoggerTool.gameLogger.error("pickUpDropItems DeadBody multi not exist. playerID={},sceneID={}", playerEntity.playerID, playerEntity.scene.sceneID);
                    return;
                }
                //移除掉落
                monsterEntity.removePlayerDropList(playerEntity.playerID);
            }
        } else if (entity.isFixedPosEntity()) {
            //掉落物在场景上
            DropEntity dropEntity = (DropEntity) entity;
            dropList = dropEntity.dropList;
            if (dropList == null || dropList.size() == 0) {
                SceneLoggerTool.gameLogger.error("pickUpDropItems SceneEntity not exist. playerID={},sceneID={}", playerEntity.playerID, playerEntity.scene.sceneID);
                return;
            }
            //移除掉落
            playerEntity.scene.removeEntityNextTick(entity);
        }

        if (dropList != null) {
            //TODO 通知逻辑服增加dropList
        }
    }

    public void onTick(int escapedMillTime) {
        //不会重复删除
        allEntity.foreachMutable((k, v) -> {
            if (v.isRemoveNextTick) {
                v.scene.removeEntity(k);
            }
        });

        int freeValue = allEntity.getFreeValue();
        int[] keys = allEntity.getKeys();
        Entity[] values = allEntity.getValues();
        int key;
        Entity entity;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                entity = values[i];
                entity.onTick(escapedMillTime);
            }
        }
    }

    public void onSecond() {
        int freeValue = allEntity.getFreeValue();
        int[] keys = allEntity.getKeys();
        Entity[] values = allEntity.getValues();
        int key;
        Entity entity;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                entity = values[i];
                entity.onSecond();
            }
        }

        long now = SystemTimeTool.getMillTime();
        monsterReviveMap.foreachMutable((entityID, time) -> {
            if (now >= time) {
                monsterReviveMap.remove(entityID);
                reviveMonster(entityID);
            }
        });
    }

    @Override
    public String toString() {
        return "Scene{" +
                "sceneID=" + sceneTemplate.getSceneID() +
                ", instanceID=" + instanceID +
                '}';
    }
}
