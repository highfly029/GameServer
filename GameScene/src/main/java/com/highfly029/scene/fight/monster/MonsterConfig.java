package com.highfly029.scene.fight.monster;

import java.io.File;

import com.highfly029.common.template.drop.DropRandomTypeConst;
import com.highfly029.common.template.drop.DropTemplate;
import com.highfly029.common.template.monster.MonsterTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.core.plugins.behaviortree.BehaviorTree;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjObjMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName MonsterConfig
 * @Description 怪物配置数据
 * @Author liyunpeng
 **/
public class MonsterConfig extends BaseGlobalConfig {
    //monsterTemplateID -> monsterTemplate
    private final IntObjectMap<MonsterTemplate> monsterTemplates = new IntObjectMap<>(MonsterTemplate[]::new);
    //ai json file name -> ai behaviorTree
    private final ObjObjMap<String, BehaviorTree> monsterBehaviorTrees = new ObjObjMap<>(String[]::new, BehaviorTree[]::new);
    //dropID -> DropTemplate
    private final IntObjectMap<DropTemplate> dropTemplates = new IntObjectMap<>(DropTemplate[]::new);
    //掉落累计权重缓存
    private final IntIntMap dropWeightCache = new IntIntMap();

    @Override
    public void check(boolean isHotfix) {
        //TODO 怪物复活间隔必须大于尸体持续时间
        //TODO 如果怪物掉落类型是DeadBody 必须有尸体
    }

    @Override
    protected void dataProcess() {
        String pathStr = ConfigPropUtils.getConfigPath() + "/ai";
        File aiFileDir = new File(pathStr);
        if (aiFileDir.isDirectory()) {
            File[] files = aiFileDir.listFiles();
            for (File file : files) {
                if (!file.isDirectory() && file.getName().toLowerCase().endsWith(".json")) {
                    String aiName = file.getName().split("\\.")[0];
                    BehaviorTree behaviorTree = new BehaviorTree();
                    behaviorTree.initWithJsonFile(file.getPath());
                    monsterBehaviorTrees.put(aiName, behaviorTree);
                }
            }
        }

        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(MonsterTemplate.class);
        MonsterTemplate template;
        for (int i = 0, size = list.size(); i < size; i++) {
            template = (MonsterTemplate) list.get(i);
            monsterTemplates.put(template.getMonsterID(), template);
        }

        list = TemplateTool.dataTemplates.get(DropTemplate.class);
        DropTemplate dropTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            dropTemplate = (DropTemplate) list.get(i);
            //计算累加概率
            if (dropTemplate.getDropRandomType() == DropRandomTypeConst.WeightRandom || dropTemplate.getDropRandomType() == DropRandomTypeConst.WeightRandomNoRepeat) {
                if (dropTemplate.getRandomItems() != null) {
                    int sum = 0;
                    for (int j = 0, jLen = dropTemplate.getRandomItems().length; j < jLen; j++) {
                        sum += dropTemplate.getRandomItems()[j][2];
                    }
                    dropWeightCache.put(dropTemplate.getDropID(), sum);
                }
            }
            dropTemplates.put(dropTemplate.getDropID(), dropTemplate);
        }
    }

    /**
     * 获取怪物模板
     *
     * @param monsterTemplateID
     * @return
     */
    public MonsterTemplate getMonsterTemplate(int monsterTemplateID) {
        return monsterTemplates.get(monsterTemplateID);
    }

    /**
     * 获取行为树
     *
     * @param aiName
     * @return
     */
    public BehaviorTree getMonsterBehaviorTree(String aiName) {
        return monsterBehaviorTrees.get(aiName);
    }

    /**
     * 获取掉落模板
     *
     * @param dropID
     * @return
     */
    public DropTemplate getDropTemplate(int dropID) {
        return dropTemplates.get(dropID);
    }


    /**
     * 获取掉落生成的物品列表
     *
     * @param template
     * @return
     */
    public IntIntMap getDropList(MonsterTemplate template, long playerID) {
        int dropID = template.getDropID();
        DropTemplate dropTemplate = getDropTemplate(dropID);
        int randomTimes = dropTemplate.getRandomTimes();
        int[][] randomItems = dropTemplate.getRandomItems();
//        ObjectList<ItemData> list = new ObjectList<>();
//
//        if (dropTemplate.getFixedItems() != null) {
//            for (int i = 0, len = dropTemplate.getFixedItems().length; i < len; i++) {
//                int[] itemArray = dropTemplate.getFixedItems()[i];
//                boolean isBind = itemArray[2] > 0;
//                ItemData itemData = ItemFactory.create(itemArray[0]);
//                itemData.init(itemArray[0], itemArray[1], isBind);
//                list.add(itemData);
//            }
//        }
//
//        if (player != null) {
//            //特定职业掉落
//            if (dropTemplate.getProfessionItems() != null) {
//                for (int i = 0, len = dropTemplate.getProfessionItems().length; i < len; i++) {
//                    int[] itemArray = dropTemplate.getProfessionItems()[i];
//                    boolean isBind = itemArray[2] > 0;
//                    ItemData itemData = ItemFactory.create(itemArray[0]);
//                    itemData.init(itemArray[0], itemArray[1], isBind);
//                    list.add(itemData);
//                }
//            }
//
//            //TODO
//            //如果怪物身上有任务掉落物、并且检测player接取了此任务、则掉落任务道具
//            //掉落物可能会根据player数据不同而不同 ex:等级 杀怪次数
//        }
//
//
//        if (randomItems != null) {
//            switch (dropTemplate.getDropRandomType()) {
//                case DropRandomTypeConst.SingleRandom: {
//                    for (int i = 0; i < randomTimes; i++) {
//                        for (int j = 0, jLen = randomItems.length; j < jLen; j++) {
//                            int[] itemArray = randomItems[j];
//                            int probability = itemArray[2];
//                            if (RandomUtils.checkProbability(probability)) {
//                                boolean isBind = itemArray[3] > 0;
//                                ItemData itemData = ItemFactory.create(itemArray[0]);
//                                itemData.init(itemArray[0], itemArray[1], isBind);
//                                list.add(itemData);
//                            }
//                        }
//                    }
//                    break;
//                }
//                case DropRandomTypeConst.WeightRandom: {
//                    for (int i = 0; i < randomTimes; i++) {
//                        int randNum = RandomUtils.randomInt(dropWeightCache.get(dropID));
//                        int index = 0;
//                        while (randNum >= randomItems[index][2]) {
//                            randNum -= randomItems[index++][2];
//                        }
//                        int[] itemArray = randomItems[index];
//                        boolean isBind = itemArray[3] > 0;
//                        ItemData itemData = ItemFactory.create(itemArray[0]);
//                        itemData.init(itemArray[0], itemArray[1], isBind);
//                        list.add(itemData);
//                    }
//                    break;
//                }
//                case DropRandomTypeConst.SingleRandomNoRepeat: {
//                    break;
//                }
//                case DropRandomTypeConst.WeightRandomNoRepeat: {
//                    break;
//                }
//            }
//        }
//        //list 物品合并
//        ItemContainer.merge(list);
        return new IntIntMap();
    }
}
