package com.highfly029.scene.fight.entity;

import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.scene.world.SceneEntityWorld;
import com.highfly029.utils.collection.IntIntMap;

/**
 * @ClassName DropEntity
 * @Description 掉落物
 * @Author liyunpeng
 **/
public class DropEntity extends FixedPosEntity {
    /**
     * 删除的时间戳
     */
    public long removeTime;
    /**
     * 掉落物
     */
    public IntIntMap dropList;

    public DropEntity(SceneEntityWorld sceneEntityWorld) {
        super(sceneEntityWorld);
    }


//    public void initWithItemData(ItemData itemData, int dropNum, long removeTime) {
//        ItemData newItemData = itemData.copy(itemData);
//        newItemData.setItemNum(dropNum);
//        dropList = new IntIntMap();
//        dropList.add(newItemData);
//        this.removeTime = removeTime;
//    }

    public void initWithDropList(IntIntMap map, long removeTime) {
        dropList = map;
        this.removeTime = removeTime;
    }

    //优先根据怪物掉落方式来决定 谁可以看到掉落物
    //如果掉落物产生太多太频繁的话可以改为只推送给个人或者队伍

    @Override
    public void onAddEntityToScene() {
        super.onAddEntityToScene();
    }

    @Override
    public void onDelEntityFromScene() {
        super.onDelEntityFromScene();
    }

    @Override
    public void onSecond() {
        if (removeTime > 0 && SystemTimeTool.getMillTime() >= removeTime) {
            scene.removeEntityNextTick(this);
        }
    }
}
