package com.highfly029.scene.fight.scene;

import com.highfly029.scene.fight.entity.Entity;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName MapGridInfo
 * @Description 地图格子信息
 * @Author liyunpeng
 **/
public class GridInfo {
    //格子中的实体集合
    public IntSet entitySet = new IntSet();

    public static GridInfo create() {
        return new GridInfo();
    }


    //增加一个实体
    public void addEntity(Entity entity) {
        entitySet.add(entity.entityID);
    }

    //删除一个实体
    public void delEntity(Entity entity) {
        entitySet.remove(entity.entityID);
    }


}
