package com.highfly029.scene.fight;

/**
 * @ClassName BaseFightLogic
 * @Description 战斗基础数据逻辑
 * @Author liyunpeng
 **/
public class BaseFightLogic {
    //父战斗逻辑
    protected FightAllLogic parentLogic;

    public FightAllLogic getParentLogic() {
        return parentLogic;
    }

    public BaseFightLogic(FightAllLogic fightAllLogic) {
        this.parentLogic = fightAllLogic;
    }

    public void onTick(int escapedMillTime) {
    }

    public void onSecond() {
    }
}
