package com.highfly029.scene.fight.avatar;

import com.highfly029.common.template.model.AvatarPartTemplate;
import com.highfly029.common.template.model.ModelTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName AvatarConfig
 * @Description 换装配置数据
 * @Author liyunpeng
 **/
public class AvatarConfig extends BaseGlobalConfig {
    //modelID -> modelTemplate
    private final IntObjectMap<ModelTemplate> modelTemplates = new IntObjectMap<>(ModelTemplate[]::new);
    //modelID << 16 | avatarPartType -> partID set
    private final IntObjectMap<IntSet> avatarPartsMap = new IntObjectMap<>(IntSet[]::new);


    @Override
    public void check(boolean isHotfix) {
        //TODO partID > 0 && avatarPartType > 0
    }

    @Override
    protected void dataProcess() {
        ModelTemplate modelTemplate;
        ObjectList<BaseTemplate> templateList = TemplateTool.dataTemplates.get(ModelTemplate.class);
        for (int i = 0, size = templateList.size(); i < size; i++) {
            modelTemplate = (ModelTemplate) templateList.get(i);
            modelTemplates.put(modelTemplate.getModelID(), modelTemplate);
        }

        AvatarPartTemplate avatarPartTemplate;
        templateList = TemplateTool.dataTemplates.get(AvatarPartTemplate.class);
        for (int i = 0, size = templateList.size(); i < size; i++) {
            avatarPartTemplate = (AvatarPartTemplate) templateList.get(i);
            int key = avatarPartTemplate.getModelID() << 16 | avatarPartTemplate.getAvatarPartType();
            IntSet set = avatarPartsMap.get(key);
            if (set == null) {
                set = new IntSet();
                avatarPartsMap.put(key, set);
            }
            set.add(avatarPartTemplate.getPartID());
        }
    }

    /**
     * 获取模型模板
     *
     * @param modelID
     * @return
     */
    public ModelTemplate getModelTemplate(int modelID) {
        return modelTemplates.get(modelID);
    }

    /**
     * 是否存在部件ID
     *
     * @param modelID
     * @param avatarPartType
     * @param partID
     * @return
     */
    public boolean isContainPartID(int modelID, int avatarPartType, int partID) {
        int key = modelID << 16 | avatarPartType;
        IntSet set = avatarPartsMap.get(key);
        if (set == null) {
            return false;
        } else {
            return set.contains(partID);
        }
    }
}
