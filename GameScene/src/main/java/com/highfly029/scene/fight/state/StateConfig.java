package com.highfly029.scene.fight.state;

import com.highfly029.common.template.commonType.PushTypeConst;
import com.highfly029.common.template.state.StateTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName StateConfig
 * @Description 状态配置数据
 * @Author liyunpeng
 **/
public class StateConfig extends BaseGlobalConfig {
    private int stateArraySize;

    //必要的可能推送属性组
    private short[] maybeSendNecessaryList;
    //是否是可能推送的属性
    private boolean[] maybeSendBoolArray;
    //是否是推送给自己的属性
    private boolean[] sendSelfBoolArray;
    //是否是推送给别人的属性
    private boolean[] sendOtherBoolArray;
    //是否是只推送给master的属性
    private boolean[] sendMasterBoolArray;

    @Override
    public void check(boolean isHotfix) {

    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(StateTemplate.class);
        short stateSize = 0;
        StateTemplate template;
        for (int i = 0, size = list.size(); i < size; i++) {
            template = (StateTemplate) list.get(i);
            if (template.getStateID() > stateSize) {
                stateSize = template.getStateID();
            }
        }
        stateSize++;
        stateArraySize = stateSize;

        maybeSendBoolArray = new boolean[stateSize];
        sendSelfBoolArray = new boolean[stateSize];
        sendOtherBoolArray = new boolean[stateSize];
        sendMasterBoolArray = new boolean[stateSize];

        IntList maybeSendList = new IntList();

        for (int i = 0, len = list.size(); i < len; i++) {
            template = (StateTemplate) list.get(i);
            int stateID = template.getStateID();
            byte pushType = template.getPushType();

            if (pushType == PushTypeConst.Self || pushType == PushTypeConst.All) {
                sendSelfBoolArray[stateID] = true;
            }

            if (pushType == PushTypeConst.Other || pushType == PushTypeConst.All) {
                sendOtherBoolArray[stateID] = true;
            }

            if (pushType == PushTypeConst.Master) {
                sendMasterBoolArray[stateID] = true;
            }

            if (pushType != PushTypeConst.Off) {
                maybeSendBoolArray[stateID] = true;
                maybeSendList.add(stateID);
            }
        }

        maybeSendNecessaryList = new short[maybeSendList.size()];
        for (int i = 0, len = maybeSendNecessaryList.length; i < len; i++) {
            maybeSendNecessaryList[i] = (short) maybeSendList.get(i);
        }
    }

    public int getStateArraySize() {
        return stateArraySize;
    }

    public short[] getMaybeSendNecessaryList() {
        return maybeSendNecessaryList;
    }

    public boolean[] getMaybeSendBoolArray() {
        return maybeSendBoolArray;
    }

    public boolean[] getSendSelfBoolArray() {
        return sendSelfBoolArray;
    }

    public boolean[] getSendOtherBoolArray() {
        return sendOtherBoolArray;
    }

    public boolean[] getSendMasterBoolArray() {
        return sendMasterBoolArray;
    }
}
