package com.highfly029.scene.fight.skill;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.template.skill.SkillTargetTypeConst;
import com.highfly029.common.template.skill.SkillTemplate;
import com.highfly029.common.utils.Vector3Utils;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.tool.ConfigTool;

/**
 * @ClassName SkillData
 * @Description 技能数据
 * @Author liyunpeng
 **/
public class SkillData {
    public int skillID;
    public int skillLevel;
    public byte targetType;
    public int targetEntityID;
    public Vector3 targetPos;
    public Vector3 targetDir;

    public SkillData copy(SkillData skillData) {
        this.skillID = skillData.skillID;
        this.skillLevel = skillData.skillLevel;
        this.targetType = skillData.targetType;
        this.targetEntityID = skillData.targetEntityID;
        this.targetPos = skillData.targetPos;
        this.targetDir = skillData.targetDir;
        return this;
    }

    /**
     * 使用技能初始化技能数据
     *
     * @param skillID
     * @param sourceEntity
     * @param targetEntity
     */
    public void initWithSkill(int skillID, CharacterEntity sourceEntity, CharacterEntity targetEntity) {
        int skillLevel = sourceEntity.fightAllLogic.skillFightLogic.skillLevelMap.get(skillID);
        SkillTemplate skillTemplate = ConfigTool.getSkillConfig().getSkillTemplate(skillID, skillLevel);
        this.skillID = skillID;
        this.targetType = skillTemplate.getTargetType();
        switch (this.targetType) {
            case SkillTargetTypeConst.TargetEntityID -> {
                this.targetEntityID = targetEntity.entityID;
            }
            case SkillTargetTypeConst.TargetDirection -> {
                if (this.targetDir == null) {
                    this.targetDir = Vector3.create();
                }
                Vector3Utils.getDir(this.targetDir, sourceEntity.curPos, targetEntity.curPos);
            }
            case SkillTargetTypeConst.TargetPosition -> {
                if (this.targetPos == null) {
                    this.targetPos = Vector3.create();
                }
                this.targetPos.copy(targetEntity.curPos);
            }
        }

    }

    public static SkillData create() {
        SkillData skillData = new SkillData();
        return skillData;
    }
}
