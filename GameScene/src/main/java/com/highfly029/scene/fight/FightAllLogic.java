package com.highfly029.scene.fight;

import com.highfly029.common.template.commonType.RemoveTypeConst;
import com.highfly029.common.template.state.StateConst;
import com.highfly029.scene.fight.attack.AttackFightLogic;
import com.highfly029.scene.fight.attribute.AttributeFightLogic;
import com.highfly029.scene.fight.avatar.AvatarFightLogic;
import com.highfly029.scene.fight.buff.BuffFightLogic;
import com.highfly029.scene.fight.bullet.BulletFightLogic;
import com.highfly029.scene.fight.coolDown.CoolDownLogic;
import com.highfly029.scene.fight.effect.EffectFightLogic;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.fight.skill.SkillFightLogic;
import com.highfly029.scene.fight.specialMove.SpecialMoveLogic;
import com.highfly029.scene.fight.state.StateFightLogic;

/**
 * @ClassName FightAllLogic
 * @Description 战斗数据逻辑
 * @Author liyunpeng
 **/
public class FightAllLogic {
    public AttributeFightLogic attributeFightLogic;
    public BuffFightLogic buffFightLogic;
    public SkillFightLogic skillFightLogic;
    public StateFightLogic stateFightLogic;
    public AttackFightLogic attackFightLogic;
    public EffectFightLogic effectFightLogic;
    public BulletFightLogic bulletFightLogic;
    public AvatarFightLogic avatarFightLogic;
    public CoolDownLogic coolDownLogic;
    public SpecialMoveLogic specialMoveLogic;

    public CharacterEntity characterEntity;

    public FightAllLogic(CharacterEntity characterEntity) {
        this.characterEntity = characterEntity;
        attributeFightLogic = new AttributeFightLogic(this);
        buffFightLogic = new BuffFightLogic(this);
        skillFightLogic = new SkillFightLogic(this);
        stateFightLogic = new StateFightLogic(this);
        attackFightLogic = new AttackFightLogic(this);
        effectFightLogic = new EffectFightLogic(this);
        bulletFightLogic = new BulletFightLogic(this);
        avatarFightLogic = new AvatarFightLogic(this);
        coolDownLogic = new CoolDownLogic(this);
        specialMoveLogic = new SpecialMoveLogic(this);
    }

    public void onTick(int escapedMillTime) {
        effectFightLogic.onTick(escapedMillTime);
        bulletFightLogic.onTick(escapedMillTime);
        skillFightLogic.onTick(escapedMillTime);
        buffFightLogic.onTick(escapedMillTime);
        attributeFightLogic.onTick(escapedMillTime);
        stateFightLogic.onTick(escapedMillTime);
        coolDownLogic.onTick(escapedMillTime);
        specialMoveLogic.onTick(escapedMillTime);
    }

    public void onSecond() {
        attributeFightLogic.onSecond();
    }

    /**
     * 执行死亡行为
     *
     * @param attackSource 伤害源
     */
    public void doDeathAction(CharacterEntity attackSource) {
        if (!stateFightLogic.isAlive()) {
            return;
        }
        stateFightLogic.setState(StateConst.Death, true);
//        stateFightLogic.refreshState();

        //stop move

        //stop skill

        //死亡移除buff
        buffFightLogic.removeBuffByRemoveType(RemoveTypeConst.Death);
        characterEntity.onDeath(attackSource);
        characterEntity.scene.onCharacterDeath(characterEntity);
    }
}
