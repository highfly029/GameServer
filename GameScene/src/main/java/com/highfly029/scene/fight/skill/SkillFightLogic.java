package com.highfly029.scene.fight.skill;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.template.attribute.AttributeConst;
import com.highfly029.common.template.commonType.CoolDownTypeConst;
import com.highfly029.common.template.skill.SkillConditionConst;
import com.highfly029.common.template.skill.SkillCostTypeConst;
import com.highfly029.common.template.skill.SkillDamageTypeConst;
import com.highfly029.common.template.skill.SkillInfluenceFlagConst;
import com.highfly029.common.template.skill.SkillTargetTypeConst;
import com.highfly029.common.template.skill.SkillTemplate;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.common.utils.Vector3Utils;
import com.highfly029.scene.fight.BaseFightLogic;
import com.highfly029.scene.fight.FightAllLogic;
import com.highfly029.scene.fight.attribute.AttributeFightLogic;
import com.highfly029.scene.fight.effect.EffectFightLogic;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.fight.scene.Scene;
import com.highfly029.scene.fight.state.StateFightLogic;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.utils.BroadCastUtils;
import com.highfly029.utils.collection.IntIntMap;

/**
 * @ClassName SkillFightLogic
 * @Description 技能数据逻辑
 * @Author liyunpeng
 **/
public class SkillFightLogic extends BaseFightLogic {
    //当前拥有的主动技能id和等级
    public IntIntMap skillLevelMap = new IntIntMap();

    /************* 技能数据 *******************/
    private final SkillData skillData = SkillData.create();
    private int currentSkillID;
    private SkillTemplate skillTemplate;

    //技能前摇时间
    private int shakeBeforeSkillTime;
    //技能后摇时间
    private int shakeAfterSkillTime;
    //技能间隔次数
    private int intervalCnt;
    //技能间隔时间
    private int intervalTime;

    //当前已经经过事件索引
    private int curPassIndex;
    //技能总时间
    private int allTime;
    //当前技能经过总时间
    private int curPassTime;

    public SkillFightLogic(FightAllLogic logic) {
        super(logic);
    }


//    public void preUseSkill(SkillData skillData) {
//        preUseSkill(skillData.skillID, skillData.targetEntityID, skillData.targetPos, skillData.targetDir);
//    }

    /**
     * 技能释放前置
     *
     * @param skillID
     * @param targetEntityID
     * @param targetPos
     * @param targetDir
     */
    public void preUseSkill(int skillID, int targetEntityID, Vector3 targetPos, Vector3 targetDir) {
        CharacterEntity sourceEntity = parentLogic.characterEntity;
        StateFightLogic stateFightLogic = parentLogic.stateFightLogic;

        int skillLevel = skillLevelMap.getOrDefault(skillID, 0);
        if (skillLevel <= 0) {
            SceneLoggerTool.gameLogger.error("don't have skillLevelMap skillID={}, level={}", skillID, skillLevel);
            sourceEntity.onUseSkillFail(skillID, null);
            return;
        }
        if (!stateFightLogic.isAlive()) {
            SceneLoggerTool.gameLogger.error("use skill entity is death skillID={}, level={}", skillID, skillLevel);
            sourceEntity.onUseSkillFail(skillID, null);
            return;
        }
        //替换后的技能等级和技能id

        SkillTemplate tmpSkillTemplate = skillTemplate = ConfigTool.getSkillConfig().getSkillTemplate(skillID, skillLevel);
        if (tmpSkillTemplate == null) {
            SceneLoggerTool.gameLogger.error("skill template not exist skillID={}", skillID);
            sourceEntity.onUseSkillFail(skillID, null);
            return;
        }
        CharacterEntity targetEntity = null;
        byte targetType = tmpSkillTemplate.getTargetType();
        switch (targetType) {
            case SkillTargetTypeConst.TargetEntityID -> {
                if (targetEntityID <= 0) {
                    SceneLoggerTool.gameLogger.error("skill target entityID invalid, skillID={}, targetEntityID={}", skillID, targetEntityID);
                    sourceEntity.onUseSkillFail(skillID, null);
                    return;
                } else {
                    targetEntity = (CharacterEntity) sourceEntity.scene.allEntity.get(targetEntityID);
                    if (targetEntity == null) {
                        SceneLoggerTool.gameLogger.error("skill target entityID not exist, skillID={}, targetEntityID={}", skillID, targetEntityID);
                        sourceEntity.onUseSkillFail(skillID, null);
                        return;
                    } else if (!checkInfluenceFlag(sourceEntity, targetEntity, tmpSkillTemplate.getInfluenceFlag())) {
                        SceneLoggerTool.gameLogger.error("checkTargetInfluence false skillID={}", skillID);
                        sourceEntity.onUseSkillFail(skillID, null);
                        return;
                    }
                }
            }
            case SkillTargetTypeConst.TargetPosition -> {
                if (targetPos == null || targetPos.equal(Vector3.ZERO)) {
                    SceneLoggerTool.gameLogger.error("skill targetPos invalid, skillID={}", skillID);
                    sourceEntity.onUseSkillFail(skillID, null);
                    return;
                }
            }
            case SkillTargetTypeConst.TargetDirection -> {
                if (targetDir == null || targetDir.equal(Vector3.ZERO)) {
                    SceneLoggerTool.gameLogger.error("skill targetDir invalid, skillID={}", skillID);
                    sourceEntity.onUseSkillFail(skillID, null);
                    return;
                }
                if (!targetDir.isNormalized()) {
                    SceneLoggerTool.gameLogger.error("skill targetDir invalid not normalized, skillID={}", skillID);
                    sourceEntity.onUseSkillFail(skillID, null);
                    return;
                }
            }
        }

        if (tmpSkillTemplate.getDamageType() == SkillDamageTypeConst.PhysicsSkillType && stateFightLogic.cantAttack()) {
            SceneLoggerTool.gameLogger.error("skill cant attack skillID={}", skillID);
            sourceEntity.onUseSkillFail(skillID, null);
            return;
        }

        if (tmpSkillTemplate.getDamageType() == SkillDamageTypeConst.MagicSkillType && stateFightLogic.cantCast()) {
            SceneLoggerTool.gameLogger.error("skill cant cast skillID={}", skillID);
            sourceEntity.onUseSkillFail(skillID, null);
            return;
        }

        if (isShakeBeforeSkill() && !skillTemplate.isBeforeCanInterrupt()) {
            SceneLoggerTool.gameLogger.error("cant interrupt shakeBeforeSkill skillID={}", skillID);
            sourceEntity.onUseSkillFail(skillID, null);
            return;
        }

        if (isShakeAfterSkill() && !skillTemplate.isAfterCanInterrupt()) {
            SceneLoggerTool.gameLogger.error("cant interrupt shakeAfterSkill skillID={}", skillID);
            sourceEntity.onUseSkillFail(skillID, null);
            return;
        }

        if (isSkillInterval()) {
            SceneLoggerTool.gameLogger.error("cant interrupt skill interval skillID={}", skillID);
            sourceEntity.onUseSkillFail(skillID, null);
            return;
        }

        if (tmpSkillTemplate.getSkillConditions() != null) {
            for (int[] params : tmpSkillTemplate.getSkillConditions()) {
                if (!checkOneSkillCondition(params, 0)) {
                    SceneLoggerTool.gameLogger.error("checkOneSkillCondition false skillID={}", skillID);
                    sourceEntity.onUseSkillFail(skillID, null);
                    return;
                }
            }
        }

        //check cd
        if (sourceEntity.fightAllLogic.coolDownLogic.isCoolDown(CoolDownTypeConst.ActiveSkillCoolDown, skillID)) {
            SceneLoggerTool.gameLogger.error("isCoolDown skillID={}", skillID);
            return;
        }

        if (tmpSkillTemplate.getCostType() > 0) {
            if (!checkSkillCost(tmpSkillTemplate.getCostType(), tmpSkillTemplate.getCostArray())) {
                SceneLoggerTool.gameLogger.error("checkSkillCost false skillID={}", skillID);
                sourceEntity.onUseSkillFail(skillID, null);
                return;
            }
        }

        if (!checkSkillDistance(tmpSkillTemplate, targetType, targetPos, targetEntity)) {
            SceneLoggerTool.gameLogger.error("checkSkillDistance false skillID={}", skillID);
            sourceEntity.onUseSkillFail(skillID, null);
            return;
        }

        //技能实际消耗
        doSkillCost(tmpSkillTemplate.getCostType(), tmpSkillTemplate.getCostArray());

        //技能实际走cd
        sourceEntity.fightAllLogic.coolDownLogic.beginCoolDown(CoolDownTypeConst.ActiveSkillCoolDown, skillID, tmpSkillTemplate.getCoolDownDuration(), tmpSkillTemplate.getCoolDownGroups());

        skillData.targetType = targetType;
        skillData.targetEntityID = targetEntityID;
        skillData.targetPos = targetPos;
        skillData.targetDir = targetDir;

        useSkill(skillID, skillLevel, targetEntity);
    }

    /**
     * 检测是否能符合任意其中一个影响标志
     *
     * @param sourceEntity
     * @param targetEntity
     * @param influenceFlag 影响标志二进制集合
     * @return
     */
    public boolean checkInfluenceFlag(CharacterEntity sourceEntity, CharacterEntity targetEntity, int influenceFlag) {
        if (sourceEntity == null || targetEntity == null || influenceFlag == 0) {
            return false;
        }
        Scene scene = sourceEntity.scene;
        int flag = 1;
        while (flag <= influenceFlag) {
            if ((flag & influenceFlag) > 0) {
                switch (flag) {
                    case SkillInfluenceFlagConst.Enemy -> {
                        if (scene.checkIsEnemy(sourceEntity, targetEntity)) {
                            return true;
                        }
                    }
                    case SkillInfluenceFlagConst.Friend -> {
                        if (scene.checkIsFriend(sourceEntity, targetEntity)) {
                            return true;
                        }
                    }
                    case SkillInfluenceFlagConst.Self -> {
                        if (sourceEntity.entityID == targetEntity.entityID) {
                            return true;
                        }
                    }
                }
            }
            flag <<= 1;
        }
        return false;
    }

    /**
     * 技能释放距离判定
     *
     * @param skillTemplate
     * @param targetType
     * @param targetPos
     * @param targetEntity
     * @return
     */
    private boolean checkSkillDistance(SkillTemplate skillTemplate, byte targetType, Vector3 targetPos, CharacterEntity targetEntity) {
        CharacterEntity sourceEntity = parentLogic.characterEntity;
        float distance = skillTemplate.getDistance();
        if (distance > 0) {
            if (SkillTargetTypeConst.TargetEntityID == targetType) {
                float curDistance = Vector3Utils.distance3D(sourceEntity.curPos, targetEntity.curPos);
                return curDistance <= distance + targetEntity.collideRadius;
            } else if (SkillTargetTypeConst.TargetPosition == targetType) {
                float curDistanceSqrt = Vector3Utils.distance3DSquare(sourceEntity.curPos, targetPos);
                return curDistanceSqrt <= distance * distance;
            }
        }
        return true;
    }

    /**
     * 检测技能消耗
     *
     * @param costType
     * @param costArray
     * @return
     */
    public boolean checkSkillCost(byte costType, int[] costArray) {
        if (costArray == null) {
            return true;
        }
        switch (costType) {
            case SkillCostTypeConst.Attribute -> {
                AttributeFightLogic attributeFightLogic = parentLogic.attributeFightLogic;
                if (attributeFightLogic.getAttribute((short) costArray[0]) < costArray[1]) {
                    return false;
                }
            }
            case SkillCostTypeConst.Item -> {

            }
            case SkillCostTypeConst.Assets -> {

            }
        }
        return true;
    }

    /**
     * 技能实际消耗
     *
     * @param costType
     * @param costArray
     */
    public void doSkillCost(byte costType, int[] costArray) {
        if (costArray == null) {
            return;
        }
        switch (costType) {
            case SkillCostTypeConst.Attribute -> {
                AttributeFightLogic attributeFightLogic = parentLogic.attributeFightLogic;
                attributeFightLogic.subOneAttribute((short) costArray[0], costArray[1]);
            }
            case SkillCostTypeConst.Item -> {

            }
            case SkillCostTypeConst.Assets -> {

            }
        }
    }

    /**
     * 技能条件判断
     *
     * @param params
     * @param startPos
     * @return
     */
    public boolean checkOneSkillCondition(int[] params, int startPos) {
        byte type = (byte) params[startPos];
        switch (type) {
            case SkillConditionConst.OnState -> {
                return parentLogic.stateFightLogic.getState(params[startPos + 1]);
            }
            case SkillConditionConst.OffState -> {
                return !parentLogic.stateFightLogic.getState(params[startPos + 1]);
            }
            case SkillConditionConst.AttributeGreaterThan -> {
                return parentLogic.attributeFightLogic.getAttribute((short) params[startPos + 1]) > params[startPos + 2];
            }
            case SkillConditionConst.AttributeGreaterThanOrEqual -> {
                return parentLogic.attributeFightLogic.getAttribute((short) params[startPos + 1]) >= params[startPos + 2];
            }
            case SkillConditionConst.AttributeLessThan -> {
                return parentLogic.attributeFightLogic.getAttribute((short) params[startPos + 1]) < params[startPos + 2];
            }
            case SkillConditionConst.AttributeLessThanOrEqual -> {
                return parentLogic.attributeFightLogic.getAttribute((short) params[startPos + 1]) <= params[startPos + 2];
            }
            case SkillConditionConst.AttributeNotEqual -> {
                return parentLogic.attributeFightLogic.getAttribute((short) params[startPos + 1]) != params[startPos + 2];
            }
        }
        return true;
    }

    //技能释放
    public void useSkill(int skillID, int skillLevel, CharacterEntity targetEntity) {
        /**
         * 如果上一个技能存在则stopSkill(pre)
         * 如果不可移动则stopMove
         * 角色转向目标方向
         * 广播开始释放技能
         * 如果有释放技能才触发的buff，则按照几率触发
         * 初始化技能
         * 根据攻击速度、施法速度、重新计算前摇 后摇 间隔时间
         * 设置技能步骤标记，开始tick技能
         */

        CharacterEntity characterEntity = parentLogic.characterEntity;

        if (isSkillNow()) {
            //强制终止上一个技能
            clearSkill();
        }

        //停止移动
        characterEntity.isMoving = false;

        //角色转向
        Vector3 dir = characterEntity.direction;
        switch (skillData.targetType) {
            case SkillTargetTypeConst.TargetEntityID -> {
                //如果施法者是自己 则不执行转向操作
                if (characterEntity.entityID != targetEntity.entityID) {
                    Vector3Utils.getDir(dir, characterEntity.curPos, targetEntity.curPos);
                }
            }
            case SkillTargetTypeConst.TargetPosition -> {
                Vector3Utils.getDir(dir, characterEntity.curPos, skillData.targetPos);
            }
            case SkillTargetTypeConst.TargetDirection -> {
                dir.copy(skillData.targetDir);
            }
        }

        currentSkillID = skillID;
        SkillTemplate tmpSkillTemplate = skillTemplate;

        com.highfly029.common.protocol.packet.PbEntity.S2CEntityCastSkill.Builder builder = com.highfly029.common.protocol.packet.PbEntity.S2CEntityCastSkill.newBuilder();
        builder.setEntityID(characterEntity.entityID);
        builder.setSkillID(skillID);
        builder.setSkillLevel(skillLevel);
        builder.setSkillTargetType(skillData.targetType);
        switch (skillData.targetType) {
            case SkillTargetTypeConst.TargetEntityID -> {
                builder.setTargetEntityID(skillData.targetEntityID);
            }
            case SkillTargetTypeConst.TargetPosition -> {
                builder.setTargetPos(PbCommonUtils.vector3Obj2Pb(skillData.targetPos));
            }
            case SkillTargetTypeConst.TargetDirection -> {
                builder.setTargetDir(PbCommonUtils.vector3Obj2Pb(skillData.targetDir));
            }
        }

        BroadCastUtils.broadCastToAOIPlayers(characterEntity, PtCode.S2CEntityCastSkill, builder.build(), false);

        shakeBeforeSkillTime = tmpSkillTemplate.getShakeBeforeSkill();
        shakeAfterSkillTime = tmpSkillTemplate.getShakeAfterSkill();
        intervalCnt = tmpSkillTemplate.getIntervalCnt();
        intervalTime = tmpSkillTemplate.getIntervalTime();

        curPassIndex = 0;

        calSkillNeedPassTime();
        SceneLoggerTool.gameLogger.info("use skill name={} entityID={}, level={}", tmpSkillTemplate.getName(), skillID, skillLevel);

    }

    /**
     * 计算技能需要经过的时间
     */
    private void calSkillNeedPassTime() {
        float speed;
        float unitSpeed;//单位速度
        if (skillTemplate.getDamageType() == SkillDamageTypeConst.PhysicsSkillType) {
            if ((speed = parentLogic.attributeFightLogic.getAttributeReal(AttributeConst.AttackSpeed)) != 0) {
                //攻击速度影响物理技能
                unitSpeed = 1 / (1 + speed);
                shakeBeforeSkillTime = (int) (shakeBeforeSkillTime * unitSpeed);
                intervalTime = (int) (intervalTime * unitSpeed);
                shakeAfterSkillTime = (int) (shakeAfterSkillTime * unitSpeed);
            }
        } else if (skillTemplate.getDamageType() == SkillDamageTypeConst.MagicSkillType) {
            if ((speed = parentLogic.attributeFightLogic.getAttributeReal(AttributeConst.CastSpeed)) != 0) {
                //施法速度影响魔法技能
                unitSpeed = 1 / (1 + speed);
                shakeBeforeSkillTime = (int) (shakeBeforeSkillTime * unitSpeed);
                intervalTime = (int) (intervalTime * unitSpeed);
                shakeAfterSkillTime = (int) (shakeAfterSkillTime * unitSpeed);
            }
        }
        curPassTime = 0;
        allTime = shakeBeforeSkillTime + intervalTime * intervalCnt + shakeAfterSkillTime;
    }

    /**
     * 停止释放技能
     */
    public void stopSkill() {
        clearSkill();
        //push
    }

    @Override
    public void onTick(int escapedMillTime) {
        if (!isSkillNow()) {
            return;
        }
        if (curPassTime <= allTime) {
            setSkillFrameTime(escapedMillTime);
        }
    }

    /**
     * 设置技能帧经过时间
     *
     * @param passTime
     */
    private void setSkillFrameTime(int passTime) {
        int tmpCurPassTime = curPassTime += passTime;
        int tmpShakeBeforeSkillTime = shakeBeforeSkillTime;
        int tmpIntervalCnt = intervalCnt;
        int tmpIntervalTime = intervalTime;

        EffectFightLogic effectFightLogic = parentLogic.effectFightLogic;

        int[][] effects;
        int[] effect;
        int attackID = skillTemplate.getIntervalAttackID();
        int attackLevel = skillTemplate.getAttackLevel() > 0 ? skillTemplate.getAttackLevel() : 1;
        int bulletID = skillTemplate.getIntervalBulletID();
        int bulletLevel = skillTemplate.getBulletLevel() > 0 ? skillTemplate.getBulletLevel() : 1;
        //前摇
        if (curPassIndex == 0) {
            if (tmpCurPassTime >= tmpShakeBeforeSkillTime) {
                curPassIndex++;
                if ((effects = skillTemplate.getEffectsBeforeSkill()) != null) {
                    for (int i = 0, len = effects.length; i < len; i++) {
                        if ((effect = effects[i]) != null) {
                            effectFightLogic.doEffectAction(effect, 0, null);
                        }
                    }
                }
            } else {
                return;
            }
        }

        //技能间隔
        while (curPassIndex <= tmpIntervalCnt) {
            if (tmpCurPassTime >= tmpShakeBeforeSkillTime + tmpIntervalTime * curPassIndex) {
                curPassIndex++;
                if (attackID > 0) {
                    parentLogic.attackFightLogic.doAttackAction(attackID, attackLevel, skillData, null);
                }
                if (bulletID > 0) {
                    parentLogic.bulletFightLogic.doCreateBullet(bulletID, bulletLevel, skillData, parentLogic.characterEntity);
                }
            } else {
                return;
            }
        }

        //后摇
        if (curPassIndex == tmpIntervalCnt + 1) {
            curPassIndex++;
            if ((effects = skillTemplate.getEffectsAfterSkill()) != null) {
                for (int i = 0, len = effects.length; i < len; i++) {
                    if ((effect = effects[i]) != null) {
                        effectFightLogic.doEffectAction(effect, 0, null);
                    }
                }
            }
        }

        if (tmpCurPassTime >= allTime) {
            onSkillOver();
        }
    }

    /**
     * 技能结束处理
     */
    private void onSkillOver() {
        clearSkill();
    }

    /**
     * 清理技能数据
     */
    private void clearSkill() {
        currentSkillID = 0;

        shakeBeforeSkillTime = 0;
        shakeAfterSkillTime = 0;
        intervalCnt = 0;
        intervalTime = 0;

        curPassIndex = 0;
        allTime = 0;
        curPassTime = 0;
    }

    /**
     * 技能前摇中
     *
     * @return
     */
    public boolean isShakeBeforeSkill() {
        return currentSkillID > 0 && curPassIndex == 0;
    }

    /**
     * 技能后摇中
     *
     * @return
     */
    public boolean isShakeAfterSkill() {
        return currentSkillID > 0 && curPassIndex == intervalCnt + 2;
    }

    /**
     * 技能施法间隔中
     *
     * @return
     */
    public boolean isSkillInterval() {
        return currentSkillID > 0 && curPassIndex > 0 && curPassIndex <= intervalCnt;
    }

    /**
     * 是否正在放技能
     *
     * @return
     */
    public boolean isSkillNow() {
        return currentSkillID > 0;
    }

    public int getCurrentSkillID() {
        return currentSkillID;
    }
}
