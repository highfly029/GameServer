package com.highfly029.scene.fight.attack;

import com.highfly029.common.template.attack.AttackFormulaTemplate;
import com.highfly029.common.template.attack.AttackTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName AttackConfig
 * @Description 攻击配置数据
 * @Author liyunpeng
 **/
public class AttackConfig extends BaseGlobalConfig {
    //attackID << 32 | attackLevel, AttackTemplate
    private final LongObjectMap<AttackTemplate> attackTemplates = new LongObjectMap<>(AttackTemplate[]::new);
    //attackFormulaID, AttackFormulaTemplate
    private AttackFormulaTemplate[] attackFormulaTemplates;

    @Override
    public void check(boolean isHotfix) {

    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(AttackTemplate.class);
        AttackTemplate attackTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            attackTemplate = (AttackTemplate) list.get(i);
            attackTemplates.put(MathUtils.getCompositeIndex(attackTemplate.getAttackID(), attackTemplate.getAttackLevel()), attackTemplate);
        }

        list = TemplateTool.dataTemplates.get(AttackFormulaTemplate.class);
        AttackFormulaTemplate attackFormulaTemplate;
        int maxAttackFormulaSize = 0;
        for (int i = 0, size = list.size(); i < size; i++) {
            attackFormulaTemplate = (AttackFormulaTemplate) list.get(i);
            maxAttackFormulaSize = Math.max(maxAttackFormulaSize, attackFormulaTemplate.getAttackFormulaID());
        }
        attackFormulaTemplates = new AttackFormulaTemplate[maxAttackFormulaSize + 1];
        for (int i = 0, size = list.size(); i < size; i++) {
            attackFormulaTemplate = (AttackFormulaTemplate) list.get(i);
            attackFormulaTemplates[attackFormulaTemplate.getAttackFormulaID()] = attackFormulaTemplate;
        }
    }

    /**
     * 获取攻击计算公式模板
     *
     * @param attackFormulaID
     * @return
     */
    public AttackFormulaTemplate getAttackFormulaTemplate(int attackFormulaID) {
        return attackFormulaTemplates[attackFormulaID];
    }

    /**
     * 获取攻击模板
     *
     * @param attackID
     * @param attackLevel
     * @return
     */
    public AttackTemplate getAttackTemplate(int attackID, int attackLevel) {
        return attackTemplates.get(MathUtils.getCompositeIndex(attackID, attackLevel));
    }
}
