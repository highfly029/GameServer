package com.highfly029.scene.fight.entity;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.template.attack.ScopeTypeConst;
import com.highfly029.common.template.regionEntity.RegionEntityTemplate;
import com.highfly029.common.utils.Vector3Utils;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.world.SceneEntityWorld;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName RegionEntity
 * @Description 区域实体
 * @Author liyunpeng
 **/
public class RegionEntity extends MoveEntity {
    public RegionEntityTemplate template;

    /**
     * 矩形区域当前点转换成的起始点(curPos在矩形中心 startPos在矩形一边的中点)
     */
    private Vector3 startPos;

    /**
     * 起点到终点的方向向量
     */
    private Vector3 startToEndDir;
    /**
     * 半径平方
     */
    private float radiusSquare;

    /**
     * 宽度/2 平方
     */
    private float widthDiv2Square;

    /**
     * 在区域中的全部entity
     */
    private final IntObjectMap<MoveEntity> regionAllEntity = new IntObjectMap<>();

    /****** cache *******/
    private final Vector3 tmpVector1 = Vector3.create();

    public RegionEntity(SceneEntityWorld sceneEntityWorld) {
        super(sceneEntityWorld);
    }

    @Override
    public void init(int templateID) {
        template = ConfigTool.getRegionConfig().getRegionEntityTemplate(templateID);
        int[] scopeParams = template.getScopeParams();
        switch (scopeParams[0]) {
            case ScopeTypeConst.Circle -> {
                int radius = scopeParams[1];
                radiusSquare = radius * radius;
            }
            case ScopeTypeConst.Rectangle -> {
                startPos = Vector3.create();
                startToEndDir = Vector3.create();
                float[] dir = template.getDirection();
                direction.set(dir[0], dir[1], dir[2]);
                int width = scopeParams[1];
                int length = scopeParams[2];
                tmpVector1.copy(direction);
                tmpVector1.x = -tmpVector1.x;
                tmpVector1.y = -tmpVector1.y;
                tmpVector1.z = -tmpVector1.z;
                Vector3Utils.moveTowardsPos(startPos, curPos, tmpVector1, false, length / 2);

                widthDiv2Square = (widthDiv2Square = width / 2) * widthDiv2Square;
                radiusSquare = length * length;
                //tmpVector1 = 终点向量
                Vector3Utils.moveTowardsPos(tmpVector1, startPos, direction, false, length);
                //tmpVector1 = 起点到终点的方向向量
                Vector3Utils.sub3D(startToEndDir, tmpVector1, startPos);

                SceneLoggerTool.gameLogger.info("region rectangle centerPos={}", startPos);
            }
        }
    }

    @Override
    public void onTick(int escapedMillTime) {
        if (allMoveEntityInAOI.size() > 0) {
            MoveEntity[] values = allMoveEntityInAOI.getValues();
            MoveEntity entity;
            for (int i = 0, len = values.length; i < len; i++) {
                if ((entity = values[i]) != null) {
                    if (isInRegion(entity)) {
                        if (!regionAllEntity.contains(entity.entityID)) {
                            //之前不在区域内
                            if (regionAllEntity.size() == 0) {
                                onFirstEntityEnterRegion(entity);
                            }
                            onEnterRegion(entity);
                            regionAllEntity.put(entity.entityID, entity);
                        }
                        onTickInRegion(entity, escapedMillTime);
                    } else {
                        //之前在区域内 现在离开
                        if (regionAllEntity.contains(entity.entityID)) {
                            onLeaveRegion(entity);
                            regionAllEntity.remove(entity.entityID);
                            if (regionAllEntity.size() == 0) {
                                onLastEntityLeaveRegion(entity);
                            }
                        }
                    }
                }
            }
        }
        super.onTick(escapedMillTime);
    }

    /**
     * 判断是否在区域内
     *
     * @param otherEntity
     * @return
     */
    private boolean isInRegion(MoveEntity otherEntity) {
        int[] scopeParams = template.getScopeParams();
        switch (scopeParams[0]) {
            case ScopeTypeConst.Circle -> {
                if (Vector3Utils.distance3DSquare(otherEntity.curPos, curPos) <= radiusSquare) {
                    return true;
                }
            }
            case ScopeTypeConst.Rectangle -> {
                Vector3Utils.sub3D(tmpVector1, otherEntity.curPos, startPos);
                float dotValue = Vector3Utils.dot(startToEndDir, tmpVector1);
                if (dotValue >= 0 && dotValue < radiusSquare) {
                    float ratio = dotValue / radiusSquare;
                    tmpVector1.copy(startToEndDir);
                    tmpVector1.multiply(ratio);
                    Vector3Utils.add3D(tmpVector1, tmpVector1, startPos);
                    if (Vector3Utils.distance3DSquare(tmpVector1, otherEntity.curPos) <= widthDiv2Square) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 持续在区域内
     *
     * @param otherEntity
     */
    private void onTickInRegion(MoveEntity otherEntity, int escapedMillTime) {
//        SceneLoggerTool.gameLogger.info("region onTickInRegion ={}", otherEntity);
        if (otherEntity.isCharacterEntity()) {
            CharacterEntity CharacterEntity = (CharacterEntity) otherEntity;
            int[][] actions = template.getTickActions();
            if (actions != null) {
                for (int[] action : actions) {
                    CharacterEntity.fightAllLogic.effectFightLogic.doEffectAction(action, 0, null);
                }
            }
        }
    }

    /**
     * 进入区域
     *
     * @param otherEntity
     */
    protected void onEnterRegion(MoveEntity otherEntity) {
        SceneLoggerTool.gameLogger.info("region onEnterRegion ={}", otherEntity);

        if (otherEntity.isCharacterEntity()) {
            CharacterEntity CharacterEntity = (CharacterEntity) otherEntity;
            int[][] actions = template.getEnterActions();
            if (actions != null) {
                for (int[] action : actions) {
                    CharacterEntity.fightAllLogic.effectFightLogic.doEffectAction(action, 0, null);
                }
            }
        }
    }

    /**
     * 离开区域
     *
     * @param otherEntity
     */
    protected void onLeaveRegion(MoveEntity otherEntity) {
        SceneLoggerTool.gameLogger.info("region onLeaveRegion ={}", otherEntity);

        if (otherEntity.isCharacterEntity()) {
            CharacterEntity CharacterEntity = (CharacterEntity) otherEntity;
            int[][] actions = template.getLeaveActions();
            if (actions != null) {
                for (int[] action : actions) {
                    CharacterEntity.fightAllLogic.effectFightLogic.doEffectAction(action, 0, null);
                }
            }
        }
    }

    /**
     * 第一个实体进入区域
     *
     * @param otherEntity
     */
    protected void onFirstEntityEnterRegion(MoveEntity otherEntity) {
        SceneLoggerTool.gameLogger.info("region onFirstEntityEnterRegion ={}", otherEntity);

        if (otherEntity.isCharacterEntity()) {
            CharacterEntity CharacterEntity = (CharacterEntity) otherEntity;
            int[][] actions = template.getFirstEnterActions();
            if (actions != null) {
                for (int[] action : actions) {
                    CharacterEntity.fightAllLogic.effectFightLogic.doEffectAction(action, 0, null);
                }
            }
        }
    }

    /**
     * 最后一个实体离开区域
     *
     * @param otherEntity
     */
    protected void onLastEntityLeaveRegion(MoveEntity otherEntity) {
        SceneLoggerTool.gameLogger.info("region onLastEntityLeaveRegion ={}", otherEntity);

        if (otherEntity.isCharacterEntity()) {
            CharacterEntity CharacterEntity = (CharacterEntity) otherEntity;
            int[][] actions = template.getLastLeaveActions();
            if (actions != null) {
                for (int[] action : actions) {
                    CharacterEntity.fightAllLogic.effectFightLogic.doEffectAction(action, 0, null);
                }
            }
        }
    }
}
