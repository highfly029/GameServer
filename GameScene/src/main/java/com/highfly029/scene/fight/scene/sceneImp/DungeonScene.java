package com.highfly029.scene.fight.scene.sceneImp;

import com.highfly029.scene.fight.dungeon.Dungeon;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.fight.entity.Entity;
import com.highfly029.scene.fight.entity.PlayerEntity;
import com.highfly029.scene.fight.scene.Scene;
import com.highfly029.scene.fight.scene.dungeonLogic.DungeonLogic;
import com.highfly029.scene.fight.scene.dungeonLogic.DungeonLogicFactory;

/**
 * @ClassName DungeonScene
 * @Description 副本场景
 * @Author liyunpeng
 **/
public class DungeonScene extends Scene {
    /**
     * 副本逻辑
     */
    private DungeonLogic dungeonLogic;

    /**
     * 副本对象
     */
    private Dungeon dungeon;

    /**
     * 当前副本是否完成
     */
    public boolean isComplete;

    /**
     * 用副本初始化
     *
     * @param dungeon
     */
    public void initWithDungeon(Dungeon dungeon) {
        this.dungeon = dungeon;
        dungeonLogic = DungeonLogicFactory.createDungeonLogic(dungeon.dungeonTemplate.getDungeonLogicType());
    }

    /**
     * 获取副本数据
     *
     * @return
     */
    public Dungeon getDungeon() {
        return dungeon;
    }

    @Override
    protected void onEntityEnterScene(Entity entity) {
        super.onEntityEnterScene(entity);
        dungeonLogic.onEntityEnterDungeon(entity);
    }

    @Override
    protected void onEntityLeaveScene(Entity entity) {
        super.onEntityLeaveScene(entity);
        dungeonLogic.onEntityLeaveDungeon(entity);
    }

    @Override
    public void onCharacterDeath(CharacterEntity characterEntity) {
        dungeonLogic.onCharacterDeath(characterEntity);
    }

    @Override
    public void onCharacterDamage(CharacterEntity attackSource, CharacterEntity attackTarget, int realDamageValue) {
        dungeonLogic.onCharacterDamage(attackSource, attackTarget, realDamageValue);
    }

    @Override
    public void onPlayerOffline(PlayerEntity playerEntity) {
        dungeonLogic.onPlayerOffline(playerEntity);
    }

    @Override
    public void onPlayerReconnectLogin(PlayerEntity playerEntity) {
        dungeonLogic.onPlayerReconnectLogin(playerEntity);
    }

    @Override
    public void onPlayerLogout(PlayerEntity playerEntity) {
        super.onPlayerLogout(playerEntity);
        dungeonLogic.onPlayerLogout(playerEntity);
//        dungeon.playerLogout(playerEntity.player);
    }

    @Override
    public boolean isCanReborn(PlayerEntity playerEntity) {
        return dungeonLogic.isCanReborn(playerEntity);
    }

    @Override
    public void doRebornCost(PlayerEntity playerEntity) {
        dungeonLogic.doRebornCost(playerEntity);
    }

    @Override
    public boolean checkIsFriend(CharacterEntity sourceEntity, CharacterEntity targetEntity) {
        return super.checkIsFriend(sourceEntity, targetEntity);
    }

    @Override
    public boolean checkIsEnemy(CharacterEntity sourceEntity, CharacterEntity targetEntity) {
        return super.checkIsEnemy(sourceEntity, targetEntity);
    }

}
