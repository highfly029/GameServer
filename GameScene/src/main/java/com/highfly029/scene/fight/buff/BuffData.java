package com.highfly029.scene.fight.buff;

import com.highfly029.common.template.buff.BuffTemplate;

/**
 * @ClassName BuffData
 * @Description buff数据体
 * @Author liyunpeng
 **/
public class BuffData {
    //buff自增序列
    private static int index = 0;
    // 实例id
    public int instanceID;
    //buffID
    public int buffID;
    //等级
    public int level;
    //buff结束时间 毫秒 0没有自动结束时间
    public long endTime;
    //使用次数 默认0 使用一次加一次
    public int useCnt;
    //叠加层数 默认0 可叠加类型的默认1 叠加一次加一次
    public int floorNum;
    //添加者 > 0 才生效
    public int adderEntityID;
    //buff配置
    public BuffTemplate buffTemplate;

    /**
     * 自增实例id
     */
    public void autoIncrementIndex() {
        instanceID = ++index;
    }

    /**
     * 创建buff数据实例
     *
     * @return
     */
    public static BuffData create() {
        BuffData buffData = new BuffData();
        return buffData;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("buffID=").append(buffID).append("level=").append(level).append("instanceID=").append(instanceID)
                .append("endTime=").append(endTime).append("useCnt=").append(useCnt).append("floorNum=").append(floorNum).append("adderEntityID=").append(adderEntityID);
        return stringBuilder.toString();
    }
}
