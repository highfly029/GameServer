package com.highfly029.scene.fight.entity;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.scene.fight.scene.Scene;
import com.highfly029.scene.fight.scene.SceneMapGrid;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.world.Entity2AIEvent;
import com.highfly029.scene.world.SceneEntityWorld;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;
import com.highfly029.utils.collection.ObjectSet;

/**
 * @ClassName MoveEntity
 * @Description 可移动的实体，处理移动和AOI
 * @Author liyunpeng
 **/
public abstract class MoveEntity extends Entity {
    /**
     * 是否在移动中
     */
    public boolean isMoving;
    /**
     * 实体aoi内的所有角色
     */
    @Deprecated
    public IntSet entityInAOI = new IntSet();
    /**
     * 实体aoi内的所有可移动entity
     */
    public IntObjectMap<MoveEntity> allMoveEntityInAOI = new IntObjectMap<>(MoveEntity[]::new);
    /**
     * 通过绑定进入到entity的aoi内
     */
    public IntObjectMap<MoveEntity> bindMoveEntityInAOI = new IntObjectMap<>(MoveEntity[]::new);
    /**
     * 当前场景格子id,默认-1
     */
    protected int gridID = SceneMapGrid.INVALID_GRID_ID;
    /**
     * 上一个场景格子id
     */
    private int lastGridID = SceneMapGrid.INVALID_GRID_ID;
    /**
     * 一个tick内离开自己aoi的集合
     */
    private final ObjectSet<MoveEntity> leaveSelfAOISets = new ObjectSet<>(MoveEntity[]::new);
    /**
     * 一个tick内进入自己aoi的集合
     */
    private final ObjectSet<MoveEntity> enterSelfAOISets = new ObjectSet<>(MoveEntity[]::new);
    /*************** cache *****************/

    /**
     * entity格子改变时 离开其他entity的aoi集合
     */
    private final IntSet leaveOtherEntityAOISets = new IntSet();

    /**
     * entity格子改变时 进入其他entity的aoi集合
     */
    private final IntSet enterOtherEntityAOISets = new IntSet();

    /*************** cache *****************/
    public MoveEntity(SceneEntityWorld sceneEntityWorld) {
        super(sceneEntityWorld);
    }

    /**
     * 移动坐标位置
     *
     * @param targetPos
     */
    public boolean moveToPos(Vector3 targetPos) {
        Scene scene = this.scene;
        int newGridID = scene.sceneMapGrid.getGridID(targetPos);
        if (newGridID == SceneMapGrid.INVALID_GRID_ID) {
            SceneLoggerTool.gameLogger.error("moveToPos invalid targetPos={}", targetPos);
            return false;
        }
        setCurPos(targetPos);
        setGridID(newGridID);
        onChangePos();
        return true;
    }

    @Override
    public boolean isMoveEntity() {
        return true;
    }

    /**
     * 是否是玩家单位
     *
     * @return
     */
    public boolean isPlayer() {
        return false;
    }

    /**
     * 实体位置发生变化
     */
    private void onChangePos() {
        long key = MathUtils.getCompositeIndex(scene.instanceID, entityID);

        long value = MathUtils.getCompositeIndex((int) curPos.x * Vector3.FLOAT2INT_SCALE, (int) (curPos.z * Vector3.FLOAT2INT_SCALE));
        sceneEntityWorld.postEvent2SceneAI(Entity2AIEvent.EntityPosChange, key, value);
    }

    /**
     * 一次tick里 统一广播进出aoi事件
     *
     * @param escapedMillTime
     */
    @Override
    public void onTick(int escapedMillTime) {
        ObjectSet<MoveEntity> enterSelfAOISets = this.enterSelfAOISets;
        ObjectSet<MoveEntity> leaveSelfAOISets = this.leaveSelfAOISets;

        //每次tick最后统一修改一次aoi、保证在一次tick里 所有的广播是相同的aoi、防止客户端收到aoi事件后在aoi里找不到entity

        //先推送离开场景aoi 再推送进入场景aoi 因为切换场景时先离开当前场景再进入下一个场景
        if (leaveSelfAOISets.size() > 0) {
            leaveSelfAOISets.foreachImmutable((entity) -> allMoveEntityInAOI.remove(entity.entityID));
            onOtherEntityLeaveSelfAOI(leaveSelfAOISets);
            leaveSelfAOISets.clear();
        }
        if (enterSelfAOISets.size() > 0) {
            enterSelfAOISets.foreachImmutable((entity) -> allMoveEntityInAOI.put(entity.entityID, entity));
            onOtherEntityEnterSelfAOI(enterSelfAOISets);
            enterSelfAOISets.clear();
        }
    }

    /**
     * 实体所在格子发生变化
     *
     * @param oldGridID -1表示进入场景
     * @param newGridID -1表示离开场景
     */
    private void onChangeGrid(int oldGridID, int newGridID) {
        Scene scene = this.scene;
        scene.sceneMapGrid.updateEntityGrid(this, oldGridID, newGridID);

        IntSet leaveAOISets = leaveOtherEntityAOISets;
        IntSet enterAOISets = enterOtherEntityAOISets;
        leaveAOISets.clear();
        enterAOISets.clear();

        //获取格子变化时受影响的id
        scene.sceneMapGrid.getEntityIDWhenGridChanged(oldGridID, newGridID, leaveAOISets, enterAOISets);

        IntObjectMap<MoveEntity> allEntity = scene.allMoveEntity;

        MoveEntity entity;
        int freeValue = leaveAOISets.getFreeValue();
        int[] keys = leaveAOISets.getKeys();
        int entityID;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((entityID = keys[i]) != freeValue) {
                if (entityID != this.entityID) {
                    entity = allEntity.get(entityID);
                    entity.otherEntityLeaveSelfAOI(this);
                    this.otherEntityLeaveSelfAOI(entity);
                }
            }
        }

        freeValue = enterAOISets.getFreeValue();
        keys = enterAOISets.getKeys();
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((entityID = keys[i]) != freeValue) {
                if (entityID != this.entityID) {
                    entity = allEntity.get(entityID);
                    (entity).otherEntityEnterSelfAOI(this);
                    this.otherEntityEnterSelfAOI(entity);
                }
            }
        }
    }

    /**
     * 其他实体离开自己的aoi
     *
     * @param otherEntity 其他实体
     */
    protected void otherEntityLeaveSelfAOI(MoveEntity otherEntity) {
        //只有不绑定aoi的单位才会从aoi里删除
        if (!bindMoveEntityInAOI.contains(otherEntity.entityID)) {
            leaveSelfAOISets.add(otherEntity);
        }
    }

    /**
     * 其他实体进入自己的aoi
     *
     * @param otherEntity 其他实体
     */
    protected void otherEntityEnterSelfAOI(MoveEntity otherEntity) {
        //只有不绑定aoi的单位才会添加到默认aoi
        if (!bindMoveEntityInAOI.contains(otherEntity.entityID)) {
            enterSelfAOISets.add(otherEntity);
        }
    }

    /**
     * 增加entity到绑定aoi里 必须互相绑定
     *
     * @param otherEntity
     */
    public void addEntityToBindAOI(MoveEntity otherEntity) {
        if (entityID == otherEntity.entityID) {
            return;
        }

        otherEntityEnterSelfAOI(otherEntity);
        otherEntity.otherEntityEnterSelfAOI(this);

        bindMoveEntityInAOI.put(otherEntity.entityID, otherEntity);
        otherEntity.bindMoveEntityInAOI.put(entityID, this);
    }

    //TODO 绑定aoi的entity 突然离线或者掉线 绑定aoi怎么处理

    /**
     * 从绑定aoi里删除entity、互相删除
     *
     * @param otherEntity
     */
    public void removeEntityFromBindAOI(MoveEntity otherEntity) {
        if (entityID == otherEntity.entityID) {
            return;
        }
        bindMoveEntityInAOI.remove(otherEntity.entityID);
        otherEntity.bindMoveEntityInAOI.remove(entityID);

        //只有不在aoi内才会在解除绑定后从默认aoi里移除
        if (!scene.sceneMapGrid.isAroundGrid(gridID, otherEntity.gridID)) {
            otherEntityLeaveSelfAOI(otherEntity);
            otherEntity.otherEntityLeaveSelfAOI(this);
        }
    }

    /**
     * 其他实体离开自己的aoi通知
     */
    public void onOtherEntityLeaveSelfAOI(ObjectSet<MoveEntity> leaveSelfAOISets) {

    }

    /**
     * 其他实体进入自己的aoi通知
     */
    public void onOtherEntityEnterSelfAOI(ObjectSet<MoveEntity> enterSelfAOISets) {

    }

    public void setGridID(int gridID) {
        if (this.gridID != gridID) {
            this.lastGridID = this.gridID;
            this.gridID = gridID;
            this.onChangeGrid(this.lastGridID, this.gridID);
        }
    }
}
