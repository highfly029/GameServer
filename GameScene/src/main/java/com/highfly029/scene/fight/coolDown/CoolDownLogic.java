package com.highfly029.scene.fight.coolDown;

import com.highfly029.common.template.global.GlobalConst;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.scene.fight.BaseFightLogic;
import com.highfly029.scene.fight.FightAllLogic;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.LongObjectMap;

/**
 * @ClassName CoolDownLogic
 * @Description cd数据逻辑
 * 服务器只push触发事件点、客户端需要做同样的cd逻辑
 * @Author liyunpeng
 **/
public class CoolDownLogic extends BaseFightLogic {
    /**
     * 全部数据
     */
    private final LongObjectMap<CoolDownData> coolDownAllData = new LongObjectMap<>(CoolDownData[]::new);
    /**
     * 增加或减少固定值的组
     */
    private final IntIntMap coolDownGroupFixedMap = new IntIntMap();
    /**
     * 增加或减少万分比的组
     */
    private final IntIntMap coolDownGroupPercentMap = new IntIntMap();
    /**
     * 所有cd通用的固定值变化量
     */
    private final IntIntMap coolDownTypeFixedMap = new IntIntMap();
    /**
     * 所有cd通用的万分比变化量
     */
    private final IntIntMap coolDownTypePercentMap = new IntIntMap();
    /**
     * 全局cd结束时间
     */
    private long globalCoolDownEndTime;


    public CoolDownLogic(FightAllLogic fightAllLogic) {
        super(fightAllLogic);
    }

    /**
     * 是否处于cd中
     *
     * @param coolDownType cd类型 主动技能 被动技能 使用物品
     * @param id           技能id 物品id
     * @return
     */
    public boolean isCoolDown(byte coolDownType, int id) {
        long now = SystemTimeTool.getMillTime();
        if (globalCoolDownEndTime > 0 && globalCoolDownEndTime > now) {
            //全局cd还未结束
            return true;
        }
        //cd数据唯一标示
        long identifier = MathUtils.getCompositeIndex(coolDownType, id);
        CoolDownData coolDownData;
        if ((coolDownData = coolDownAllData.get(identifier)) != null) {
            //没有cd时间
            if (coolDownData.realDuration <= 0) {
                return false;
            }
            //cd还未结束
            return coolDownData.endTime > now;
        }
        return false;
    }

    /**
     * 开始一个CD
     *
     * @param coolDownType cd类型 主动技能 被动技能 使用物品
     * @param id           技能id 物品id
     * @param duration     持续时间
     * @param groups       cd组
     */
    public void beginCoolDown(byte coolDownType, int id, int duration, int[] groups) {
        long now = SystemTimeTool.getMillTime();
        if (globalCoolDownEndTime > 0 && globalCoolDownEndTime > now) {
            //全局cd还未结束
            return;
        }
        //没有cd
        if (duration <= 0) {
            return;
        }
        CoolDownData coolDownData;
        //cd数据唯一标示
        long identifier = MathUtils.getCompositeIndex(coolDownType, id);
        if ((coolDownData = coolDownAllData.get(identifier)) != null) {
            //cd还未结束
            if (coolDownData.endTime > now) {
                return;
            }
        }
        coolDownData = CoolDownData.create();
        coolDownData.coolDownType = coolDownType;
        coolDownData.id = id;

        setCoolDownRealDuration(coolDownData, duration, groups);

        coolDownData.endTime = now + coolDownData.realDuration;

        coolDownAllData.put(identifier, coolDownData);

        if (GlobalConst.GlobalCoolDownDuration > 0) {
            //增加全局cd
            globalCoolDownEndTime = now + GlobalConst.GlobalCoolDownDuration;
        }

        parentLogic.characterEntity.onBeginCoolDownPushSelf(coolDownType, id, coolDownData.realDuration);
    }

    /**
     * 删除cd
     *
     * @param coolDownType cd类型 主动技能 被动技能 使用物品
     * @param id           技能id 物品id
     */
    private void removeCoolDown(byte coolDownType, int id) {
        //cd数据唯一标示
        long identifier = MathUtils.getCompositeIndex(coolDownType, id);
        coolDownAllData.remove(identifier);
        parentLogic.characterEntity.onRemoveCoolDownPushSelf(coolDownType, id);
    }

    /**
     * 重置cd 刷新cd
     *
     * @param coolDownType
     * @param id
     */
    public void resetCoolDown(byte coolDownType, int id) {
        removeCoolDown(coolDownType, id);
    }

    /**
     * 设置cd真实的持续时间
     *
     * @param coolDownData
     * @return
     */
    private void setCoolDownRealDuration(CoolDownData coolDownData, int duration, int[] groups) {
        int fixedValue = 0;
        int percentValue = 0;
        if (groups != null) {
            for (int groupID : groups) {
                fixedValue += coolDownGroupFixedMap.get(groupID);
                percentValue += coolDownGroupPercentMap.get(groupID);
            }
        }
        fixedValue += coolDownTypeFixedMap.get(coolDownData.coolDownType);
        percentValue += coolDownTypePercentMap.get(coolDownData.coolDownType);

        duration = (int) (duration * (1 + RandomUtils.RatioPercent * percentValue) + fixedValue);

        coolDownData.realDuration = duration;
    }

    /**
     * 修改组cd固定时间
     *
     * @param groupID
     * @param value   正数增加 负数减少
     */
    public void modifyCoolDownGroupFixedTime(int groupID, int value) {
        int nowValue = coolDownGroupFixedMap.addValue(groupID, value);
        if (nowValue == 0) {
            coolDownGroupFixedMap.remove(groupID);
        }
        parentLogic.characterEntity.onCoolDownGroupFixedTimeRefreshPushSelf(groupID, nowValue);
    }

    /**
     * 修改组cd万分比时间
     *
     * @param groupID
     * @param percentValue 正数增加 负数减少
     */
    public void modifyCoolDownGroupPercentTime(int groupID, int percentValue) {
        int nowValue = coolDownGroupPercentMap.addValue(groupID, percentValue);
        if (nowValue == 0) {
            coolDownGroupPercentMap.remove(groupID);
        }
        parentLogic.characterEntity.onCoolDownGroupPercentTimeRefreshPushSelf(groupID, nowValue);
    }

    /**
     * 修改通用cd固定值变化量
     *
     * @param coolDownType
     * @param value        正数增加 负数减少
     */
    public void modifyCoolDownFixedValue(byte coolDownType, int value) {
        int nowValue = coolDownTypeFixedMap.addValue(coolDownType, value);
        if (nowValue == 0) {
            coolDownTypeFixedMap.remove(coolDownType);
        }
        parentLogic.characterEntity.onCoolDownTypeFixedTimeRefreshPushSelf(coolDownType, nowValue);
    }

    /**
     * 修改通用cd万分比变化量
     *
     * @param coolDownType
     * @param percentValue
     */
    public void modifyCoolDownPercentValue(byte coolDownType, int percentValue) {
        int nowValue = coolDownTypePercentMap.addValue(coolDownType, percentValue);
        if (nowValue == 0) {
            coolDownTypePercentMap.remove(nowValue);
        }
        parentLogic.characterEntity.onCoolDownTypePercentTimeRefreshPushSelf(coolDownType, nowValue);
    }

    @Override
    public void onTick(int escapedMillTime) {
        long now = SystemTimeTool.getMillTime();
        coolDownAllData.foreachMutable((k, v) -> {
            //删除cd结束的数据
            if (now >= v.endTime) {
                removeCoolDown(v.coolDownType, v.id);
            }
        });
    }
}
