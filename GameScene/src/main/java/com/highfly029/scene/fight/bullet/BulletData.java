package com.highfly029.scene.fight.bullet;


import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.template.bullet.BulletTemplate;
import com.highfly029.common.template.bullet.BulletTypeConst;
import com.highfly029.common.template.skill.SkillTargetTypeConst;
import com.highfly029.common.utils.Vector3Utils;
import com.highfly029.scene.fight.attack.AttackFightLogic;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.fight.skill.SkillData;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.utils.collection.IntIntMap;

/**
 * @ClassName BulletData
 * @Description 子弹数据
 * @Author liyunpeng
 **/
public class BulletData {
    //bullet自增序列
    private static int index = 0;
    // 实例id
    public int instanceID;

    //子弹持续时间
    public int duration;
    //速度 每毫秒多少米
    public float speed;

    public boolean isDestroy;

    //子弹位置
    public Vector3 pos;
    //子弹朝向
    public Vector3 dir;

    public SkillData skillData;

    //子弹碰撞记录
    public IntIntMap hitRecord;

    //子弹配置
    public BulletTemplate bulletTemplate;


    public boolean init(BulletFightLogic bulletFightLogic) {
        CharacterEntity attackSource = bulletFightLogic.getParentLogic().characterEntity;
        AttackFightLogic attackFightLogic = bulletFightLogic.getParentLogic().attackFightLogic;
        byte tmpBulletType = bulletTemplate.getBulletType();

        byte skillTargetType = skillData.targetType;
        switch (tmpBulletType) {
            case BulletTypeConst.Immediately -> {
                if (skillTargetType == SkillTargetTypeConst.TargetDirection) {
                    SceneLoggerTool.gameLogger.error("cant set bullet Immediately with TargetDirection, bulletID={}", bulletTemplate.getBulletID());
                    return false;
                }
            }
            case BulletTypeConst.FixedTime -> {
                if (skillTargetType == SkillTargetTypeConst.TargetDirection) {
                    SceneLoggerTool.gameLogger.error("cant set bullet FixedTime with TargetDirection, bulletID={}", bulletTemplate.getBulletID());
                    return false;
                }
                duration = bulletTemplate.getMaxTime();
            }
            case BulletTypeConst.FixedSpeed -> {
                if (skillTargetType == SkillTargetTypeConst.TargetEntityID) {
                    CharacterEntity targetEntity = (CharacterEntity) attackSource.scene.allEntity.get(skillData.targetEntityID);
                    if (targetEntity == null) {
                        return false;
                    } else {
                        float dis = Vector3Utils.distance3D(attackSource.curPos, targetEntity.curPos);
                        duration = (int) (dis / bulletTemplate.getSpeed() * 1000);
                    }
                } else if (skillTargetType == SkillTargetTypeConst.TargetPosition) {
                    float dis = Vector3Utils.distance3D(attackSource.curPos, skillData.targetPos);
                    duration = (int) (dis / bulletTemplate.getSpeed() * 1000);
                } else {
                    SceneLoggerTool.gameLogger.error("cant set skillTargetType TargetDirection and bulletType fixedSpeed bulletID={}", bulletTemplate.getBulletID());
                    return false;
                }

            }
            case BulletTypeConst.HitStraight -> {
                duration = bulletTemplate.getMaxTime();
                speed = bulletTemplate.getSpeed() / 1000;
                pos = Vector3.create();
                dir = Vector3.create();
                pos.copy(attackSource.curPos);
                switch (skillTargetType) {
                    case SkillTargetTypeConst.TargetEntityID -> {
                        CharacterEntity targetEntity = (CharacterEntity) attackSource.scene.allEntity.get(skillData.targetEntityID);
                        if (targetEntity == null) {
                            return false;
                        } else {
                            Vector3Utils.getDir(dir, attackSource.curPos, targetEntity.curPos);
                            attackFightLogic.getScopeOffset(pos, pos, dir, bulletTemplate.getHitScopeParams(), 0);
                            //TODO 如果假定目标单位不移动 则能计算出子弹到达目标单位的时间
                            //如果目标单位移动 则分别处理 移动后放弃攻击和移动后子弹继续跟随目标
                        }
                    }
                    case SkillTargetTypeConst.TargetPosition -> {
                        Vector3Utils.getDir(dir, attackSource.curPos, skillData.targetPos);
                        attackFightLogic.getScopeOffset(pos, pos, dir, bulletTemplate.getHitScopeParams(), 0);
                        float dis = Vector3Utils.distance3D(attackSource.curPos, skillData.targetPos);
                        duration = (int) (dis / bulletTemplate.getSpeed() * 1000);
                    }
                    case SkillTargetTypeConst.TargetDirection -> {
                        dir.copy(skillData.targetDir);
                        attackFightLogic.getScopeOffset(pos, pos, dir, bulletTemplate.getHitScopeParams(), 0);
                    }
                }
                if (hitRecord == null) {
                    hitRecord = new IntIntMap();
                } else {
                    hitRecord.clear();
                }

            }
            default -> {
                SceneLoggerTool.gameLogger.error("not exist bulletType {}", tmpBulletType);
                return false;
            }
        }
        return true;

    }

    /**
     * 回收前清理
     */
    public void clear() {

    }


    public void setDestroy(boolean destroy) {
        isDestroy = destroy;
    }

    /**
     * 自增实例id
     */
    public void autoIncrementIndex() {
        instanceID = ++index;
    }

    public static BulletData create() {
        BulletData bulletData = new BulletData();
        return bulletData;
    }
}
