package com.highfly029.scene.fight.bullet;

import com.highfly029.common.template.bullet.BulletTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName BulletConfig
 * @Description 子弹配置数据
 * @Author liyunpeng
 **/
public class BulletConfig extends BaseGlobalConfig {
    //bulletID << 32 | bulletLevel, BulletLevelTemplate
    private final LongObjectMap<BulletTemplate> bulletTemplates = new LongObjectMap<>(BulletTemplate[]::new);

    @Override
    public void check(boolean isHotfix) {

    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(BulletTemplate.class);
        BulletTemplate bulletTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            bulletTemplate = (BulletTemplate) list.get(i);
            bulletTemplates.put(MathUtils.getCompositeIndex(bulletTemplate.getBulletID(), bulletTemplate.getBulletLevel()), bulletTemplate);
        }
    }

    /**
     * 获取BulletTemplate
     *
     * @param bulletID
     * @return
     */
    public BulletTemplate getBulletTemplate(int bulletID, int level) {
        return bulletTemplates.get(MathUtils.getCompositeIndex(bulletID, level));
    }
}
