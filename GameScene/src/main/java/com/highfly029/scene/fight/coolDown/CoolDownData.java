package com.highfly029.scene.fight.coolDown;


/**
 * @ClassName CoolDownData
 * @Description cd数据
 * @Author liyunpeng
 **/
public class CoolDownData {
    /**
     * cd类型
     */
    public byte coolDownType;
    /**
     * id
     */
    public int id;
    /**
     * 结束时间
     */
    public long endTime;
    /**
     * 实际持续时间
     */
    public int realDuration;

    public static CoolDownData create() {
        CoolDownData coolDownData = new CoolDownData();
        return coolDownData;
    }
}
