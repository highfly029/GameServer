package com.highfly029.scene.config;

import com.highfly029.common.data.scene.SceneStrategyEnum;
import com.highfly029.common.template.scene.SceneTemplate;
import com.highfly029.common.template.scene.SceneTypeConst;
import com.highfly029.common.template.sceneEditorEntity.SceneEditorEntityTemplate;
import com.highfly029.common.template.sceneMap.SceneMapTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName SceneConfig
 * @Description SceneConfig
 * @Author liyunpeng
 **/
public class SceneConfig extends BaseGlobalConfig {
    //sceneID -> SceneTemplate
    private final IntObjectMap<SceneTemplate> sceneTemplates = new IntObjectMap<>(SceneTemplate[]::new);
    //sceneMapID -> SceneMapTemplate
    private final IntObjectMap<SceneMapTemplate> sceneMapTemplates = new IntObjectMap<>(SceneMapTemplate[]::new);
    //sceneID -> instanceID -> SceneEditorEntityTemplate
    private final IntObjectMap<IntObjectMap<SceneEditorEntityTemplate>> sceneEditorMap = new IntObjectMap<>(IntObjectMap[]::new);

    @Override
    public void check(boolean isHotfix) {
        //TODO 检测sceneEditorEntity instanceID符合规范 dir是单位向量
    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(SceneTemplate.class);
        SceneTemplate sceneTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            sceneTemplate = (SceneTemplate) list.get(i);
            sceneTemplates.put(sceneTemplate.getSceneID(), sceneTemplate);
        }

        list = TemplateTool.dataTemplates.get(SceneMapTemplate.class);
        SceneMapTemplate sceneMapTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            sceneMapTemplate = (SceneMapTemplate) list.get(i);
            sceneMapTemplates.put(sceneMapTemplate.getMapID(), sceneMapTemplate);
        }

        list = TemplateTool.dataTemplates.get(SceneEditorEntityTemplate.class);
        SceneEditorEntityTemplate sceneEditorEntityTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            sceneEditorEntityTemplate = (SceneEditorEntityTemplate) list.get(i);
            IntObjectMap<SceneEditorEntityTemplate> map = sceneEditorMap.get(sceneEditorEntityTemplate.getSceneID());
            if (map == null) {
                map = new IntObjectMap<>(SceneEditorEntityTemplate[]::new);
                sceneEditorMap.put(sceneEditorEntityTemplate.getSceneID(), map);
            }
            map.put(sceneEditorEntityTemplate.getEntityID(), sceneEditorEntityTemplate);
        }
    }

    /**
     * 获取场景地图模板
     *
     * @param sceneMapID
     * @return
     */
    public SceneMapTemplate getSceneMapTemplate(int sceneMapID) {
        return sceneMapTemplates.get(sceneMapID);
    }

    /**
     * 获取场景模板
     *
     * @param sceneID
     * @return
     */
    public SceneTemplate getSceneTemplate(int sceneID) {
        return sceneTemplates.get(sceneID);
    }

    /**
     * 获取场景内所有配置的entity
     *
     * @param sceneID
     * @return
     */
    public IntObjectMap<SceneEditorEntityTemplate> getSceneEditorEntityTemplate(int sceneID) {
        return sceneEditorMap.get(sceneID);
    }

    /**
     * 获取场景id对应的分配策略id
     * @param sceneID
     * @return
     */
    public static int getSceneStrategy(int sceneID) {
        SceneTemplate sceneTemplate = ConfigTool.getSceneConfig().getSceneTemplate(sceneID);
        switch (sceneTemplate.getSceneType()) {
            case SceneTypeConst.CityScene, SceneTypeConst.FieldScene -> {
                return SceneStrategyEnum.LINE.ordinal();
            }
            case SceneTypeConst.HomeScene -> {
                return SceneStrategyEnum.HOME.ordinal();
            }
            case SceneTypeConst.UnionScene -> {
                return SceneStrategyEnum.UNION.ordinal();
            }
            case SceneTypeConst.DungeonScene -> {
                return SceneStrategyEnum.DUNGEON.ordinal();
            }
        }
        return -1;
    }
}
