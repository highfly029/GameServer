package com.highfly029.scene.session;

/**
 * @ClassName ConnectErrorCode
 * @Description 连接错误码
 * @Author liyunpeng
 **/
public enum ConnectErrorCode {
    invalidMd5,

    oldSessionNull,

    playerNull,

    playerIDZero,

    invalidToken,
}
