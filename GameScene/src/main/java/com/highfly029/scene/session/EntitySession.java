package com.highfly029.scene.session;

import com.highfly029.core.session.Session;
import com.highfly029.scene.fight.entity.PlayerEntity;

/**
 * @ClassName EntitySession
 * @Description EntitySession
 * @Author liyunpeng
 **/
public class EntitySession extends Session {
    /**
     * 客户端自增协议序号
     */
    private long sequenceNum;
    /**
     * 服务端生成的channel唯一标示
     */
    private int socketID;
    /**
     * session令牌
     */
    private int token;

    private long playerID;

    private PlayerEntity playerEntity;

    public long getSequenceNum() {
        return sequenceNum;
    }

    public void setSequenceNum(long sequenceNum) {
        this.sequenceNum = sequenceNum;
    }

    public int getSocketID() {
        return socketID;
    }

    public void setSocketID(int socketID) {
        this.socketID = socketID;
    }

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

    public long getPlayerID() {
        return playerID;
    }

    public void setPlayerID(long playerID) {
        this.playerID = playerID;
    }

    public PlayerEntity getPlayerEntity() {
        return playerEntity;
    }

    public void setPlayerEntity(PlayerEntity playerEntity) {
        this.playerEntity = playerEntity;
    }
}
