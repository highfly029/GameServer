package com.highfly029.scene.packetParser;

import com.google.protobuf.ByteString;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.core.interfaces.IPacketParser;

/**
 * @ClassName C2SRoleListParser
 * @Description C2SRoleListParser
 * @Author liyunpeng
 **/
public class C2SRoleListParser implements IPacketParser {

    @Override
    public int getPtCode() {
        return PtCode.C2SRoleList;
    }

    @Override
    public String getName() {
        return "C2SRoleList";
    }

    @Override
    public boolean isParserHex() {
        return true;
    }

    @Override
    public String parser(ByteString byteString) {
        return null;
    }

}
