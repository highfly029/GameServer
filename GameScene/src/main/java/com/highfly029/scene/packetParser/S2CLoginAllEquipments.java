package com.highfly029.scene.packetParser;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbEquipment;
import com.highfly029.core.interfaces.IPacketParser;

/**
 * @ClassName S2CLoginAllEquipments
 * @Description S2CLoginAllEquipments
 * @Author liyunpeng
 **/
public class S2CLoginAllEquipments implements IPacketParser {
    @Override
    public int getPtCode() {
        return PtCode.S2CLoginPushPlayerAllEquipments;
    }

    @Override
    public String getName() {
        return "S2CLoginPushPlayerAllEquipments";
    }

    @Override
    public boolean isParserHex() {
        return true;
    }

    @Override
    public String parser(ByteString byteString) {
        try {
            PbEquipment.S2CLoginPushPlayerAllEquipments pb = PbEquipment.S2CLoginPushPlayerAllEquipments.parseFrom(byteString);
            return pb.toString();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return null;
    }
}
