package com.highfly029.scene.packetParser;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLogin;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.core.tool.LoggerTool;

/**
 * @ClassName C2SLoginParser
 * @Description C2SLoginParser
 * @Author liyunpeng
 **/
public class C2SLoginParser implements IPacketParser {
    @Override
    public int getPtCode() {
        return PtCode.C2SRoleLogin;
    }

    @Override
    public String getName() {
        return "C2SRoleLogin";
    }

    @Override
    public boolean isParserHex() {
        return true;
    }

    @Override
    public String parser(ByteString byteString) {
        PbLogin.C2SRoleLogin req;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            req = PbLogin.C2SRoleLogin.parseFrom(byteString.toByteArray());
            stringBuilder.append(req.getAccountID()).append("|")
                    .append(req.getPlayerID());
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
