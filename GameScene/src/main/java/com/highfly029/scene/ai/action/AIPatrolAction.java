package com.highfly029.scene.ai.action;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.highfly029.common.data.base.Vector3;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeNodeState;
import com.highfly029.core.plugins.behaviortree.node.BaseActionNode;
import com.highfly029.scene.ai.AIEntity;
import com.highfly029.scene.ai.BehaviorTreeAIEntityInfo;

/**
 * @ClassName AIPatrolAction
 * @Description 巡逻
 * @Author liyunpeng
 **/
public class AIPatrolAction extends BaseActionNode {
    //巡逻路径点
    private Vector3[] pointPath;
    //true循环路径点 false反向路径点
    private boolean isLoop;

    @Override
    public void initWithJson(JsonObject jsonObject) throws Exception {
        JsonElement element = jsonObject.get("pointPath");
        JsonArray jsonArray = element.getAsJsonArray();
        int size = jsonArray.size();
        pointPath = new Vector3[size];
        for (int i = 0; i < size; i++) {
            JsonObject jsonPoint = jsonArray.get(i).getAsJsonObject();
            pointPath[i] = Vector3.create();
            pointPath[i].x = jsonPoint.get("x").getAsFloat();
            pointPath[i].y = jsonPoint.get("y").getAsFloat();
            pointPath[i].z = jsonPoint.get("z").getAsFloat();
        }
        isLoop = jsonObject.get("isLoop").getAsBoolean();
    }

    @Override
    public byte execute(BehaviorTreeInfo behaviorTreeInfo) {
        BehaviorTreeAIEntityInfo btInfo = (BehaviorTreeAIEntityInfo) behaviorTreeInfo;
        AIEntity aiEntity = btInfo.aiEntity;
//        Vector3 targetPos = pointPath[btInfo.aiPatrolPointPathIndex];
//        if (targetPos == null) {
//            return BehaviorTreeNodeState.FAIL;
//        } else {
//            //第一次执行
//            if (characterEntity.isMoving == false) {
//                characterEntity.isMoving = true;
//                Vector3Utils.getDir(characterEntity.direction, characterEntity.curPos, targetPos);
//                btInfo.aiTargetPos.copy(targetPos);
//            }
//
//            if (btInfo.aiIsReachTargetPos) {
//                if (isLoop) {
//                    btInfo.aiPatrolPointPathIndex++;
//                    if (btInfo.aiPatrolPointPathIndex == pointPath.length) {
//                        btInfo.aiPatrolPointPathIndex = 0;
//                    }
//                } else {
//
//                }
//                targetPos = pointPath[btInfo.aiPatrolPointPathIndex];
//                btInfo.aiTargetPos.copy(targetPos);
//                Vector3Utils.getDir(characterEntity.direction, characterEntity.curPos, targetPos);
//                btInfo.aiIsReachTargetPos = false;
//            }
//            return BehaviorTreeNodeState.SUCCESS;
//        }
        return BehaviorTreeNodeState.FAIL;
    }
}
