package com.highfly029.scene.ai.condition;

import com.google.gson.JsonObject;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeNodeState;
import com.highfly029.core.plugins.behaviortree.node.BaseConditionNode;
import com.highfly029.scene.ai.AIEntity;
import com.highfly029.scene.ai.BehaviorTreeAIEntityInfo;
import com.highfly029.scene.ai.HatredInfo;
import com.highfly029.scene.tool.SceneLoggerTool;

/**
 * @ClassName AISearchNearbyEnemyCondition
 * @Description 检查周围有没有敌人
 * @Author liyunpeng
 **/
public class AISearchNearbyEnemyCondition extends BaseConditionNode {
    //检测半径 默认10
    private final int radius = 10;
    //检测范围形状
    private int[] scopeParams = null;

    @Override
    public void initWithJson(JsonObject jsonObject) throws Exception {
        //圆形范围
        scopeParams = new int[]{0, 0, 0, 1, radius};
    }

    @Override
    public byte execute(BehaviorTreeInfo behaviorTreeInfo) {
        BehaviorTreeAIEntityInfo btInfo = (BehaviorTreeAIEntityInfo) behaviorTreeInfo;
        AIEntity aiEntity = btInfo.aiEntity;
        var ref = new Object() {
            private HatredInfo lastInfo = null;
            private int entityID;
        };
        aiEntity.getHatredMap().foreachMutable((entityID, info) -> {
            if (ref.lastInfo == null) {
                ref.lastInfo = info;
                ref.entityID = entityID;
            } else if (ref.lastInfo.getHatredValue() < info.getHatredValue()) {
                ref.lastInfo = info;
                ref.entityID = entityID;
            } else if (ref.lastInfo.getAllDamage() < info.getAllDamage()) {
                ref.lastInfo = info;
                ref.entityID = entityID;
            }
        });
        if (ref.lastInfo != null) {
//            SceneLoggerTool.fightLogger.warn("查找到周围的敌人 entityID={}", ref.entityID);
            return BehaviorTreeNodeState.SUCCESS;
        } else {
            return BehaviorTreeNodeState.FAIL;
        }
//        AttackFightLogic attackFightLogic = characterEntity.fightAllLogic.attackFightLogic;
//        ObjectList<CharacterEntity> characterEntityList = attackFightLogic.tmpAttackTargets;
//        attackFightLogic.selectAttackTargets(characterEntityList, characterEntity, characterEntity.curPos, characterEntity.direction, scopeParams, 3, null, 1);
//        if (characterEntityList.size() > 0) {
//            btInfo.aiSelectEnemy = characterEntityList.get(0);
//            characterEntityList.clear();
//            return BehaviorTreeNodeState.SUCCESS;
//        } else {
//            btInfo.aiSelectEnemy = null;
//            return BehaviorTreeNodeState.FAIL;
//        }

    }
}
