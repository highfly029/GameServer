package com.highfly029.scene.ai;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;
import com.highfly029.scene.fight.skill.SkillData;

/**
 * @ClassName BehaviorTreeCharacterEntityInfo
 * @Description 战斗角色存储的行为树信息
 * @Author liyunpeng
 **/
public class BehaviorTreeAIEntityInfo extends BehaviorTreeInfo {
    public AIEntity aiEntity;
    /***************** AI ***********************/
    //怪物移动几个帧
    public byte moveTickCount;
    //怪物巡逻点索引
    public byte aiPatrolPointPathIndex = 0;
    //怪物行走目标点
    public Vector3 aiTargetPos = Vector3.create();
    //怪物是否达到目标点
    public boolean aiIsReachTargetPos = false;
    //怪物技能数据
    public SkillData aiSkillData = SkillData.create();
    public int aiSkillID;
    //怪物选中的敌人
    public AIEntity aiSelectEnemy;
    //怪物仇恨列表 hateList


    public BehaviorTreeAIEntityInfo(AIEntity aiEntity) {
        this.aiEntity = aiEntity;
    }


}
