package com.highfly029.scene.ai;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.utils.collection.IntIntMap;

/**
 * @ClassName CreateAIData
 * @Description CreateAIData 创建AI时需要的数据
 * @Author liyunpeng
 **/
public class CreateAIData {
    /**
     * 场景实例id
     */
    private int sceneInstanceID;
    /**
     * 实例id
     */
    private int entityID;
    /**
     * 实体类型
     */
    private int type;
    /**
     * 对应的实体类型的templateID
     */
    private int templateID;
    /**
     * ai名字
     */
    private String aiName;

    /**
     * 当前坐标
     */
    private Vector3 curPos;

    /**
     * 拥有的技能 id -> level
     */
    private IntIntMap skills;

    public int getSceneInstanceID() {
        return sceneInstanceID;
    }

    public void setSceneInstanceID(int sceneInstanceID) {
        this.sceneInstanceID = sceneInstanceID;
    }

    public int getEntityID() {
        return entityID;
    }

    public void setEntityID(int entityID) {
        this.entityID = entityID;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTemplateID() {
        return templateID;
    }

    public void setTemplateID(int templateID) {
        this.templateID = templateID;
    }

    public String getAiName() {
        return aiName;
    }

    public void setAiName(String aiName) {
        this.aiName = aiName;
    }

    public IntIntMap getSkills() {
        return skills;
    }

    public void setSkills(IntIntMap skills) {
        this.skills = skills;
    }

    public Vector3 getCurPos() {
        return curPos;
    }

    public void setCurPos(Vector3 curPos) {
        this.curPos = curPos;
    }
}
