package com.highfly029.scene.ai.action;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeNodeState;
import com.highfly029.core.plugins.behaviortree.node.BaseActionNode;
import com.highfly029.scene.ai.AIEntity;
import com.highfly029.scene.ai.BehaviorTreeAIEntityInfo;

/**
 * @ClassName AIWanderAction
 * @Description 以出生点为圆心的半径范围内闲逛
 * @Author liyunpeng
 **/
public class AIWanderAction extends BaseActionNode {
    //闲逛半径
    private int radius;

    @Override
    public void initWithJson(JsonObject jsonObject) throws Exception {
        JsonElement element = jsonObject.get("radius");
        if (element != null) {
            radius = element.getAsInt();
        }
    }

    @Override
    public byte execute(BehaviorTreeInfo behaviorTreeInfo) {
        BehaviorTreeAIEntityInfo btInfo = (BehaviorTreeAIEntityInfo) behaviorTreeInfo;
        AIEntity aiEntity = btInfo.aiEntity;
//        if (characterEntity.isMonster()) {
//            MonsterEntity monsterEntity = (MonsterEntity) characterEntity;
//            btInfo.moveTickCount = 5;
//            if (Vector3Utils.distance3DSquare(characterEntity.curPos, monsterEntity.birthPos) > radius * radius) {
//                Vector3Utils.getDir(characterEntity.direction, characterEntity.curPos, monsterEntity.birthPos);
//            } else {
//                characterEntity.direction.randomNormalise();
//            }
//        }
        return BehaviorTreeNodeState.SUCCESS;
    }
}
