package com.highfly029.scene.ai.action;

import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeNodeState;
import com.highfly029.core.plugins.behaviortree.node.BaseActionNode;
import com.highfly029.scene.ai.AIEntity;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.ai.BehaviorTreeAIEntityInfo;
import com.highfly029.scene.tool.SceneLoggerTool;

/**
 * @ClassName AIRestAction
 * @Description 休息
 * @Author liyunpeng
 **/
public class AIRestAction extends BaseActionNode {
    private int testTime;

    @Override
    public byte execute(BehaviorTreeInfo behaviorTreeInfo) {
        BehaviorTreeAIEntityInfo btInfo = (BehaviorTreeAIEntityInfo) behaviorTreeInfo;
        AIEntity aiEntity = btInfo.aiEntity;
        SceneLoggerTool.gameLogger.info("{}休息!", aiEntity.getEntityID());
        if (++testTime <= 3) {
            behaviorTreeInfo.runningNode = this;
            return BehaviorTreeNodeState.RUNNING;
        } else {
            testTime = 0;
            return BehaviorTreeNodeState.SUCCESS;
        }
    }
}
