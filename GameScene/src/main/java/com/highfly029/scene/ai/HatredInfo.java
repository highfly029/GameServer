package com.highfly029.scene.ai;

import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.utils.collection.LongLongMap;

/**
 * @ClassName HatredInfo
 * @Description 仇恨信息
 * @Author liyunpeng
 **/
public class HatredInfo {
    /**
     * 受到的全部伤害
     */
    private long allDamage;
    /**
     * 仇恨值
     */
    private int hatredValue;
    /**
     * 每个时刻收到的伤害
     */
    private final LongLongMap damageOfTimestamp = new LongLongMap();

    public long getAllDamage() {
        return allDamage;
    }

    public void setAllDamage(long allDamage) {
        this.allDamage = allDamage;
    }

    public void addDamage(long damage) {
        long now = SystemTimeTool.getMillTime();
        damageOfTimestamp.put(now, damage);
        //清理时间过长的伤害
        damageOfTimestamp.foreachMutable((k, v) -> {
            if (now - k > 10000) {
                damageOfTimestamp.remove(k);
            }
        });
        //重新计算
        allDamage = 0;
        damageOfTimestamp.foreachImmutable((k, v) -> {
            allDamage += v;
        });
    }

    public int getHatredValue() {
        return hatredValue;
    }

    public void setHatredValue(int hatredValue) {
        this.hatredValue = hatredValue;
    }

    public LongLongMap getDamageOfTimestamp() {
        return damageOfTimestamp;
    }
}
