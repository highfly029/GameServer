package com.highfly029.scene.ai.action;

import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeNodeState;
import com.highfly029.core.plugins.behaviortree.node.BaseActionNode;
import com.highfly029.scene.ai.AIEntity;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.ai.BehaviorTreeAIEntityInfo;

/**
 * @ClassName AIUseSkillAction
 * @Description 释放技能
 * @Author liyunpeng
 **/
public class AIUseSkillAction extends BaseActionNode {

    @Override
    public byte execute(BehaviorTreeInfo behaviorTreeInfo) {
        BehaviorTreeAIEntityInfo btInfo = (BehaviorTreeAIEntityInfo) behaviorTreeInfo;
        AIEntity aiEntity = btInfo.aiEntity;
//        characterEntity.serverUseSkill(btInfo.aiSkillID, btInfo.aiSkillData);
        return BehaviorTreeNodeState.SUCCESS;
    }
}
