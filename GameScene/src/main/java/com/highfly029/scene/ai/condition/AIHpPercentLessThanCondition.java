package com.highfly029.scene.ai.condition;

import com.google.gson.JsonObject;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeNodeState;
import com.highfly029.core.plugins.behaviortree.node.BaseConditionNode;
import com.highfly029.scene.ai.AIEntity;
import com.highfly029.scene.fight.attribute.AttributeFightLogic;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.ai.BehaviorTreeAIEntityInfo;
import com.highfly029.scene.tool.SceneLoggerTool;

/**
 * @ClassName AIHpPercentLessThanCondition
 * @Description 血量百分比少于
 * @Author liyunpeng
 **/
public class AIHpPercentLessThanCondition extends BaseConditionNode {
    private float hpPercentValue;

    @Override
    public void initWithJson(JsonObject jsonObject) throws Exception {

    }

    @Override
    public byte execute(BehaviorTreeInfo behaviorTreeInfo) {
        BehaviorTreeAIEntityInfo btInfo = (BehaviorTreeAIEntityInfo) behaviorTreeInfo;
        AIEntity aiEntity = btInfo.aiEntity;
//        AttributeFightLogic attributeFightLogic = characterEntity.fightAllLogic.attributeFightLogic;
//        int curHp = attributeFightLogic.getHp();
//        int hpMax = attributeFightLogic.getHpMax();
//        float hpPercent = curHp * 1.0f / hpMax;
//        SceneLoggerTool.gameLogger.info("{}血量百分比 hpPercent={}, hpPercentValue={}", characterEntity.entityID, hpPercent, hpPercentValue);
//        if (hpPercent < hpPercentValue) {
//            return BehaviorTreeNodeState.SUCCESS;
//        } else {
//            return BehaviorTreeNodeState.FAIL;
//        }
        return BehaviorTreeNodeState.FAIL;
    }
}
