package com.highfly029.scene.ai.condition;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeNodeState;
import com.highfly029.core.plugins.behaviortree.node.BaseConditionNode;
import com.highfly029.scene.ai.AIEntity;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.ai.BehaviorTreeAIEntityInfo;
import com.highfly029.scene.tool.SceneLoggerTool;

/**
 * @ClassName AIHpFixedLessThanCondition
 * @Description 血量固定值少于
 * @Author liyunpeng
 **/
public class AIHpFixedLessThanCondition extends BaseConditionNode {
    private int hpFixedValue;

    @Override
    public void initWithJson(JsonObject jsonObject) throws Exception {
        JsonElement element = jsonObject.get("hpFixedValue");
        if (element != null) {
            hpFixedValue = element.getAsInt();
        } else {
            //set default value
        }
    }

    @Override
    public byte execute(BehaviorTreeInfo behaviorTreeInfo) {
        BehaviorTreeAIEntityInfo btInfo = (BehaviorTreeAIEntityInfo) behaviorTreeInfo;
        AIEntity aiEntity = btInfo.aiEntity;
//        int curHp = characterEntity.fightAllLogic.attributeFightLogic.getAttribute(AttributeConst.Hp);
//        SceneLoggerTool.gameLogger.info("{}血量固定值 curHp={}, hpFixedValue={}", characterEntity.entityID, curHp, hpFixedValue);
//        if (curHp < hpFixedValue) {
//            return BehaviorTreeNodeState.SUCCESS;
//        } else {
//            return BehaviorTreeNodeState.FAIL;
//        }
        return BehaviorTreeNodeState.FAIL;
    }
}
