package com.highfly029.scene.ai.condition;

import com.google.gson.JsonObject;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeNodeState;
import com.highfly029.core.plugins.behaviortree.node.BaseConditionNode;
import com.highfly029.scene.ai.AIEntity;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.ai.BehaviorTreeAIEntityInfo;
import com.highfly029.utils.collection.IntIntMap;

/**
 * @ClassName AISelectSkillCondition
 * @Description 选择一个技能
 * @Author liyunpeng
 **/
public class AISelectSkillCondition extends BaseConditionNode {
    private int skillID;
    private int skillLevel;

    @Override
    public void initWithJson(JsonObject jsonObject) throws Exception {
        skillID = jsonObject.get("skillID").getAsInt();
        skillLevel = jsonObject.get("skillLevel").getAsInt();
    }

    @Override
    public byte execute(BehaviorTreeInfo behaviorTreeInfo) {
        BehaviorTreeAIEntityInfo btInfo = (BehaviorTreeAIEntityInfo) behaviorTreeInfo;
        AIEntity aiEntity = btInfo.aiEntity;

//        IntIntMap map = characterEntity.fightAllLogic.skillFightLogic.skillLevelMap;
//        if (btInfo.aiSelectEnemy == null) {
//            return BehaviorTreeNodeState.FAIL;
//        }
//        if (skillID > 0 && skillLevel > 0) {
//            if (map.getOrDefault(skillID, 0) == skillLevel) {
//                btInfo.aiSkillData.initWithSkill(skillID, characterEntity, btInfo.aiSelectEnemy);
//                btInfo.aiSkillID = skillID;
//                return BehaviorTreeNodeState.SUCCESS;
//            } else {
//                return BehaviorTreeNodeState.FAIL;
//            }
//        } else {
//            return BehaviorTreeNodeState.FAIL;
//        }
        return BehaviorTreeNodeState.SUCCESS;
    }
}
