package com.highfly029.scene.ai;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.core.plugins.behaviortree.BehaviorTree;
import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName AIEntity
 * @Description AIEntity ai实体
 * @Author liyunpeng
 **/
public class AIEntity {
    /**
     * 行为树
     */
    private BehaviorTree behaviorTree;
    /**
     * 是否处于激活状态
     */
    private boolean isActive;

    /**
     * 实体类型
     */
    private int type;
    /**
     * 实体模版id
     */
    private int templateID;
    /**
     * 场景实例id
     */
    private int sceneInstanceID;
    /**
     * 实体id
     */
    private int entityID;

    /**
     * 当前坐标
     */
    private Vector3 curPos;

    /**
     * 实体单位aoi集合
     */
    private final IntObjectMap<AIEntity> entityAOIMap = new IntObjectMap<>(AIEntity[]::new);

    /**
     * 技能集合
     */
    private IntIntMap skills = new IntIntMap();
    /**
     * 行为树信息
     */
    private BehaviorTreeAIEntityInfo behaviorTreeAIEntityInfo;

    /**
     * 仇恨集合
     */
    private final IntObjectMap<HatredInfo> hatredMap = new IntObjectMap<>();

    public static AIEntity create() {
        return new AIEntity();
    }

    public void execute() {
        behaviorTree.execute(behaviorTreeAIEntityInfo);
    }

    /**
     * 增加受到的伤害值
     *
     * @param entityID
     * @param damage
     */
    public void addDamage(int entityID, long damage) {
        HatredInfo hatredInfo = hatredMap.get(entityID);
        hatredInfo.addDamage(damage);
    }

    /**
     * 更新仇恨值
     *
     * @param entityID
     * @param hatred   仇恨值
     * @param isAdd    增加或减少
     */
    public void updateHatred(int entityID, int hatred, boolean isAdd) {
        HatredInfo hatredInfo = hatredMap.get(entityID);
        int preHatred = hatredInfo.getHatredValue();
        int nowHatred = isAdd ? preHatred + hatred : preHatred - hatred;
        hatredInfo.setHatredValue(nowHatred);
    }

    public IntObjectMap<HatredInfo> getHatredMap() {
        return hatredMap;
    }

    public BehaviorTree getBehaviorTree() {
        return behaviorTree;
    }

    public void setBehaviorTree(BehaviorTree behaviorTree) {
        this.behaviorTree = behaviorTree;

        behaviorTreeAIEntityInfo = new BehaviorTreeAIEntityInfo(this);
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        //只有设置了行为树才会被激活
        if (behaviorTree != null) {
            isActive = active;
        }
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTemplateID() {
        return templateID;
    }

    public void setTemplateID(int templateID) {
        this.templateID = templateID;
    }

    public int getSceneInstanceID() {
        return sceneInstanceID;
    }

    public void setSceneInstanceID(int sceneInstanceID) {
        this.sceneInstanceID = sceneInstanceID;
    }

    public int getEntityID() {
        return entityID;
    }

    public void setEntityID(int entityID) {
        this.entityID = entityID;
    }

    public void addAIEntity(AIEntity aiEntity) {
        entityAOIMap.put(aiEntity.getEntityID(), aiEntity);
        hatredMap.put(aiEntity.getEntityID(), new HatredInfo());
    }

    public void removeAIEntity(AIEntity aiEntity) {
        entityAOIMap.remove(aiEntity.getEntityID());
        hatredMap.remove(aiEntity.getEntityID());
    }

    public void removeAIEntity(int entityID) {
        entityAOIMap.remove(entityID);
        hatredMap.remove(entityID);
    }

    public IntObjectMap<AIEntity> getEntityAOIMap() {
        return entityAOIMap;
    }

    public IntIntMap getSkills() {
        return skills;
    }

    public void setSkills(IntIntMap skills) {
        this.skills = skills;
    }

    public Vector3 getCurPos() {
        return curPos;
    }

    public void setCurPos(Vector3 curPos) {
        this.curPos = curPos;
    }
}
