package com.highfly029.scene.ai.action;

import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeNodeState;
import com.highfly029.core.plugins.behaviortree.node.BaseActionNode;
import com.highfly029.scene.ai.AIEntity;
import com.highfly029.scene.fight.entity.CharacterEntity;
import com.highfly029.scene.ai.BehaviorTreeAIEntityInfo;
import com.highfly029.scene.tool.SceneLoggerTool;

/**
 * @ClassName AISpeakAction
 * @Description 说话
 * @Author liyunpeng
 **/
public class AISpeakAction extends BaseActionNode {
    @Override
    public byte execute(BehaviorTreeInfo behaviorTreeInfo) {
        BehaviorTreeAIEntityInfo btInfo = (BehaviorTreeAIEntityInfo) behaviorTreeInfo;
        AIEntity aiEntity = btInfo.aiEntity;
//        SceneLoggerTool.gameLogger.info("{}说话!", aiEntity.getEntityID());
        return BehaviorTreeNodeState.SUCCESS;
    }
}
