package com.highfly029.scene.ai;

/**
 * @ClassName MonsterAIMode
 * @Description 怪物ai模式
 * @Author liyunpeng
 **/
public class AIMode {
    //未激活
    public static final byte INACTIVE = 0;
    //激活
    public static final byte ACTIVE = 1;
    //频繁的
    public static final byte FREQUENT = 2;
}
