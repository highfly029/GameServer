package com.highfly029.scene.clientProtocol;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbEntity;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.scene.fight.entity.PlayerEntity;
import com.highfly029.scene.session.EntitySession;
import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName EntityProtocol
 * @Description EntityProtocol
 * @Author liyunpeng
 **/
public class EntityProtocol implements ISceneProtocolRegister {
    private SceneEntityWorld sceneEntityWorld;

    @Override
    public void registerSceneProtocol(SceneEntityWorld sceneEntityWorld) throws Exception {
        this.sceneEntityWorld = sceneEntityWorld;

        sceneEntityWorld.registerSceneProtocol(PtCode.C2SEntityMoveStart, this::entityMoveStart);
        sceneEntityWorld.registerSceneProtocol(PtCode.C2SEntityMoveStop, this::entityMoveStop);
        sceneEntityWorld.registerSceneProtocol(PtCode.C2SEntityCastSkill, this::entityCastSkill);
    }

    /**
     * 玩家请求开始移动
     *
     * @param entitySession
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void entityMoveStart(EntitySession entitySession, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbEntity.C2SEntityMoveStart req = PbEntity.C2SEntityMoveStart.parseFrom(packet.getData());

        PlayerEntity playerEntity = entitySession.getPlayerEntity();
        playerEntity.moveStartByClient(packet);
    }

    /**
     * 玩家请求停止移动
     *
     * @param entitySession
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void entityMoveStop(EntitySession entitySession, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbEntity.C2SEntityMoveStart req = PbEntity.C2SEntityMoveStart.parseFrom(packet.getData());

        PlayerEntity playerEntity = entitySession.getPlayerEntity();
        playerEntity.moveStopByClient(packet);
    }

    /**
     * 玩家释放技能
     *
     * @param entitySession
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void entityCastSkill(EntitySession entitySession, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbEntity.C2SEntityCastSkill req = PbEntity.C2SEntityCastSkill.parseFrom(packet.getData());
        PlayerEntity playerEntity = entitySession.getPlayerEntity();
        playerEntity.clientUseSkill(req);
    }
}
