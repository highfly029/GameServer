package com.highfly029.scene.clientProtocol;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.scene.session.EntitySession;

/**
 * @ClassName IEntityProtocolHandler
 * @Description 场景服协议处理
 * @Author liyunpeng
 **/
public interface ISceneProtocolHandler {
    void onClientProtocolHandler(EntitySession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException;
}
