package com.highfly029.scene.clientProtocol;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.scene.fight.entity.PlayerEntity;
import com.highfly029.scene.fight.scene.Scene;
import com.highfly029.scene.session.EntitySession;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName SceneProtocol
 * @Description SceneProtocol
 * @Author liyunpeng
 **/
public class SceneProtocol implements ISceneProtocolRegister {
    private SceneEntityWorld sceneEntityWorld;

    @Override
    public void registerSceneProtocol(SceneEntityWorld sceneEntityWorld) throws Exception {
        this.sceneEntityWorld = sceneEntityWorld;

        sceneEntityWorld.registerSceneProtocol(PtCode.C2SPlayerEnterScene, this::playerEnterScene);
        sceneEntityWorld.registerSceneProtocol(PtCode.C2SPickUpDropItems, this::pickUpDropItems);
    }

    /**
     * 玩家请求进入场景
     *
     * @param entitySession
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void playerEnterScene(EntitySession entitySession, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbScene.C2SPlayerEnterScene req = PbScene.C2SPlayerEnterScene.parseFrom(packet.getData());
        int logicServerID = req.getLogicServerID();
        int logicServerWorldIndex = req.getLogicServerWorldIndex();
        int sceneID = req.getSceneID();
        int instanceID = req.getInstanceID();

        Scene scene = sceneEntityWorld.getSceneManager().getScene(instanceID);
        
        if (scene == null) {
            //该场景有概率过期自动删除 需要客户端重新发起一遍进入场景流程
            SceneLoggerTool.gameLogger.error("playerEnterScene scene is null, playerID={}, serverID={}, index={}, sceneID={}, instanceID={}", entitySession.getPlayerID(), logicServerID, logicServerWorldIndex, sceneID, instanceID);
            return;
        }
        SceneLoggerTool.gameLogger.info("playerEnterScene playerID={}, serverID={}, index={}, sceneID={}, instanceID={}", entitySession.getPlayerID(), logicServerID, logicServerWorldIndex, sceneID, scene.instanceID);
        long playerID = entitySession.getPlayerID();
        PbScene.Scene2LogicGetEnterSceneData.Builder builder = PbScene.Scene2LogicGetEnterSceneData.newBuilder();
        builder.setSceneID(sceneID);
        builder.setInstanceID(scene.instanceID);
        sceneEntityWorld.sendMsg2Logic(logicServerID, logicServerWorldIndex, playerID, PtCode.Scene2LogicGetEnterSceneData, builder.build());
    }

    /**
     * 拾取场景掉落物
     *
     * @param entitySession
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void pickUpDropItems(EntitySession entitySession, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbScene.C2SPickUpDropItems resp = PbScene.C2SPickUpDropItems.parseFrom(packet.getData());
        PlayerEntity playerEntity = entitySession.getPlayerEntity();
        playerEntity.scene.pickUpDropItems(resp.getEntityID());
    }
}
