package com.highfly029.scene.clientProtocol;

import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName IEntityProtocolRegister
 * @Description 场景服协议注册
 * @Author liyunpeng
 **/
public interface ISceneProtocolRegister {
    void registerSceneProtocol(SceneEntityWorld sceneEntityWorld) throws Exception;
}
