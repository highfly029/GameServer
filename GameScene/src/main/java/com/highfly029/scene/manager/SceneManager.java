package com.highfly029.scene.manager;

import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.common.template.scene.SceneTemplate;
import com.highfly029.common.template.scene.SceneTypeConst;
import com.highfly029.common.template.sceneMap.SceneMapTemplate;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.manager.BaseManager;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.scene.config.SceneConfig;
import com.highfly029.scene.fight.scene.Scene;
import com.highfly029.scene.fight.scene.sceneImp.SceneFactory;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.utils.NavMeshUtils;
import com.highfly029.scene.world.SceneEntityWorld;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.LongObjectMap;

/**
 * @ClassName SceneManager
 * @Description 场景管理类
 * @Author liyunpeng
 **/
public class SceneManager extends BaseManager {
    /**
     * 场景实体世界
     */
    private SceneEntityWorld sceneEntityWorld;
    /**
     * 场景集合 key=instanceID
     */
    private final IntObjectMap<Scene> sceneMap = new IntObjectMap<>(Scene[]::new);

    /**
     * 场景分线集合(只存储有分线的场景) key = sceneID << 32 | lineID
     */
    private final LongObjectMap<Scene> sceneLineMap = new LongObjectMap<>(Scene[]::new);

    //mapID->soloMeshName
    private final IntObjectMap<String> soloMeshNames = new IntObjectMap<>(String[]::new);

    public SceneManager(SceneEntityWorld sceneEntityWorld) {
        super(sceneEntityWorld);
        this.sceneEntityWorld = sceneEntityWorld;
    }

    /**
     * 是否是需要分线的场景类型
     * @param sceneType
     * @return
     */
    private boolean isNeedLine(int sceneType) {
        switch (sceneType) {
            case SceneTypeConst.FieldScene, SceneTypeConst.CityScene -> {
                return true;
            }
        }
        return false;
    }

    /**
     * 创建场景
     *
     * @param sceneID
     * @param lineID
     * @return
     */
    public Scene createScene(int sceneID, int lineID) {
        SceneTemplate sceneTemplate = ConfigTool.getSceneConfig().getSceneTemplate(sceneID);
        Scene scene = SceneFactory.createScene(sceneTemplate.getSceneType());
        scene.lineID = lineID;
        if (!scene.initWithTemplate(sceneTemplate)) {
            SceneLoggerTool.fightLogger.error("createScene fail sceneID={}", sceneID);
            return null;
        }
        SceneMapTemplate sceneMapTemplate = ConfigTool.getSceneConfig().getSceneMapTemplate(sceneTemplate.getMapID());
        String path = ConfigPropUtils.getConfigPath() + "/navmesh/" + sceneMapTemplate.getNavMesh() + ".bin";
        if (scene.navMeshMode == NavMeshUtils.SOLOMESH_MODE) {
            //如果没加载过网格才加载
            if (!soloMeshNames.contains(sceneTemplate.getMapID())) {
                boolean result = NavMeshUtils.loadNavMesh(NavMeshUtils.SOLOMESH_MODE, scene.navMeshName, path);
                NavMeshUtils.setPrint(NavMeshUtils.SOLOMESH_MODE, scene.navMeshName, false);
                if (result) {
                    //如果加载成功 则存储 避免重复加载
                    soloMeshNames.put(sceneTemplate.getMapID(), scene.navMeshName);
                    SceneLoggerTool.gameLogger.info("{} navmesh load success, mapID={}, name={}", path, sceneTemplate.getMapID(), scene.navMeshName);
                } else {
                    SceneLoggerTool.gameLogger.error("{} navmesh load failed, mapID={}, name={}", path, sceneTemplate.getMapID(), scene.navMeshName);
                }
            }
        } else if (scene.navMeshMode == NavMeshUtils.TILECACHE_MODE) {
            boolean result = NavMeshUtils.loadNavMesh(NavMeshUtils.TILECACHE_MODE, scene.navMeshName, path);
            NavMeshUtils.setPrint(NavMeshUtils.TILECACHE_MODE, scene.navMeshName, false);
            if (result) {
                SceneLoggerTool.gameLogger.info("{} navmesh load success, mapID={}, name={}", path, sceneTemplate.getMapID(), scene.navMeshName);
            } else {
                SceneLoggerTool.gameLogger.error("{} navmesh load failed, mapID={}, name={}", path, sceneTemplate.getMapID(), scene.navMeshName);
            }
        }
        scene.setSceneEntityWorld(sceneEntityWorld);
        sceneMap.put(scene.instanceID, scene);
        if (isNeedLine(scene.sceneType)) {
            sceneLineMap.put(MathUtils.getCompositeIndex(sceneID, lineID), scene);
        }
        SceneLoggerTool.gameLogger.info("createScene success sceneID={},instanceID={}", sceneID, scene.instanceID);
        //通过sceneEditEntity.xlsx 和 editor创建场景里的初始怪物
        scene.addPreSetEntity();
        return scene;
    }

    /**
     * 删除场景
     *
     * @param scene
     */
    public void removeScene(Scene scene) {
        if (scene.navMeshMode == NavMeshUtils.TILECACHE_MODE) {
            NavMeshUtils.release(scene.navMeshMode, scene.navMeshName);
        }
        sceneMap.remove(scene.instanceID);
        if (isNeedLine(scene.sceneType)) {
            sceneLineMap.remove(MathUtils.getCompositeIndex(scene.sceneID, scene.lineID));
        }
        SceneLoggerTool.gameLogger.info("removeScene sceneID={},lineID={},instanceID={}", scene.sceneID, scene.lineID, scene.instanceID);
        PbScene.S2LRemoveScene.Builder builder = PbScene.S2LRemoveScene.newBuilder();
        builder.setSceneServerID(ConfigConst.identifier);
        builder.setSceneWorldIndex(sceneEntityWorld.getIndex());
        builder.setSceneStrategy(SceneConfig.getSceneStrategy(scene.sceneID));
        builder.setKey(scene.getKey());
        sceneEntityWorld.sendMsg2AllLoadBalanceScene(PtCode.S2LRemoveScene, builder.build());
    }

    /**
     * 根据实例获取场景
     *
     * @param instanceID
     * @return
     */
    public Scene getScene(int instanceID) {
        return sceneMap.get(instanceID);
    }

    /**
     * 根据场景id和线id获取场景
     *
     * @param sceneID
     * @param lineID
     * @return
     */
    public Scene getSceneByLine(int sceneID, int lineID) {
        return sceneLineMap.get(MathUtils.getCompositeIndex(sceneID, lineID));
    }

    @Override
    public void onTick(int escapedMillTime) {
        //不会重复删除
        long now = SystemTimeTool.getMillTime();
        sceneMap.foreachMutable((k, v) -> {
            if (v.getRemoveEndTime() > 0 && now >= v.getRemoveEndTime()) {
                removeScene(v);
            }
        });

        int freeValue = sceneMap.getFreeValue();
        int[] keys = sceneMap.getKeys();
        Scene[] values = sceneMap.getValues();
        int key;
        Scene scene;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                scene = values[i];
                scene.onTick(escapedMillTime);
            }
        }
    }

    @Override
    public void onSecond() {
        int freeValue = sceneMap.getFreeValue();
        int[] keys = sceneMap.getKeys();
        Scene[] values = sceneMap.getValues();
        int key;
        Scene scene;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                scene = values[i];
                scene.onSecond();
            }
        }
    }

    @Override
    public void onMinute() {

    }

    @Override
    public void onShutDown() {
        sceneMap.foreachMutable((k, v) -> {
            removeScene(v);
        });
    }
}
