package com.highfly029.scene.manager;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.core.manager.BaseManager;
import com.highfly029.core.plugins.behaviortree.BehaviorTree;
import com.highfly029.scene.ai.AIEntity;
import com.highfly029.scene.ai.CreateAIData;
import com.highfly029.scene.exception.SceneException;
import com.highfly029.scene.tool.ConfigTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.world.SceneAIWorld;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.collection.IntSet;
import com.highfly029.utils.collection.LongObjectMap;

/**
 * @ClassName AIManager
 * @Description AIManager
 * @Author liyunpeng
 **/
public class AIManager extends BaseManager {
    /**
     * ai行为树集合
     * key = sceneInstanceID << 32 | entityID
     * value = AIEntity
     */
    private final LongObjectMap<AIEntity> aiEntityMap = new LongObjectMap<>(AIEntity[]::new);

    public AIManager(SceneAIWorld world) {
        super(world);
    }

    /**
     * 增加实体AI
     *
     * @param createAIData 创建AI需要的数据
     */
    public void addEntityAI(CreateAIData createAIData) {
        long key = MathUtils.getCompositeIndex(createAIData.getSceneInstanceID(), createAIData.getEntityID());
        AIEntity aiEntity = AIEntity.create();
        aiEntity.setSceneInstanceID(createAIData.getSceneInstanceID());
        aiEntity.setEntityID(createAIData.getEntityID());
        aiEntity.setType(createAIData.getType());
        aiEntity.setTemplateID(createAIData.getTemplateID());
        if (createAIData.getSkills() != null) {
            aiEntity.setSkills(createAIData.getSkills());
        }
        aiEntity.setCurPos(createAIData.getCurPos());
        if (createAIData.getAiName() != null) {
            BehaviorTree aiBehaviorTree = ConfigTool.getMonsterConfig().getMonsterBehaviorTree(createAIData.getAiName());
            if (aiBehaviorTree == null) {
                throw new SceneException("monster ID=" + createAIData.getEntityID() + " cant add aiBehaviorTree!");
            }
            aiEntity.setBehaviorTree(aiBehaviorTree);
        }
        aiEntityMap.put(key, aiEntity);
        SceneLoggerTool.fightLogger.info("增加实体AI type={}, entityID={}", aiEntity.getType(), aiEntity.getEntityID());
    }

    /**
     * 获取AI实体
     *
     * @param sceneInstanceID 场景实例ID
     * @param entityID        实体ID
     * @return AIEntity
     */
    public AIEntity getAIEntity(int sceneInstanceID, int entityID) {
        long key = MathUtils.getCompositeIndex(sceneInstanceID, entityID);
        return aiEntityMap.get(key);
    }

    /**
     * 删除实体AI
     *
     * @param key sceneInstanceID << 32 | entityID
     */
    public void removeEntityAI(long key) {
        aiEntityMap.remove(key);
        SceneLoggerTool.fightLogger.info("删除实体AI entityID={}", (int) key);
    }

    /**
     * 其他实体进入自己的aoi
     *
     * @param key         sceneInstanceID << 32 | entityID
     * @param entityIDSet 实体id集合
     */
    public void otherEntityEnterAOI(long key, IntSet entityIDSet) {
        AIEntity aiEntity = aiEntityMap.get(key);
        int sceneInstanceID = MathUtils.getCompositeArg1(key);
        entityIDSet.foreachImmutable(entityID -> {
            AIEntity other = aiEntityMap.get(MathUtils.getCompositeIndex(sceneInstanceID, entityID));
            aiEntity.addAIEntity(other);
            SceneLoggerTool.fightLogger.info("其他实体进入自己的aoi type={}, entityID={}, otherEntityID={}", aiEntity.getType(), aiEntity.getEntityID(), other.getEntityID());
        });
        //aoi内有移动单位则激活
        aiEntity.setActive(true);
    }

    /**
     * 其他实体离开自己的aoi
     *
     * @param key         sceneInstanceID << 32 | entityID
     * @param entityIDSet 实体id集合
     */
    public void otherEntityLeaveAOI(long key, IntSet entityIDSet) {
        AIEntity aiEntity = aiEntityMap.get(key);
        int sceneInstanceID = MathUtils.getCompositeArg1(key);
        entityIDSet.foreachImmutable(entityID -> {
            //entityID被删除的话map里拿不到，直接删除
            aiEntity.removeAIEntity(entityID);
            SceneLoggerTool.fightLogger.info("其他实体离开自己的aoi type={}, entityID={}, otherEntityID={}", aiEntity.getType(), aiEntity.getEntityID(), entityID);
        });
        //aoi内无移动单位、则不激活
        if (aiEntity.getEntityAOIMap().isEmpty()) {
            aiEntity.setActive(false);
        }
    }

    /**
     * 实体位置发生变化
     *
     * @param key
     * @param value
     */
    public void entityChangePos(long key, long value) {
        AIEntity aiEntity = aiEntityMap.get(key);
        int preX = MathUtils.getCompositeArg1(value);
        int preZ = MathUtils.getCompositeArg2(value);
        float x = preX * Vector3.INT2FLOAT_SCALE;
        float z = preZ * Vector3.INT2FLOAT_SCALE;
        aiEntity.getCurPos().x = x;
        aiEntity.getCurPos().z = z;
//        SceneLoggerTool.fightLogger.info("entityChangePos id={}, x={}, z={}, preX={}, preZ={}", aiEntity.getEntityID(), x, z, preX, preZ);
    }

    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {
        aiEntityMap.foreachMutable((key, aiEntity) -> {
            if (aiEntity.isActive()) {
                aiEntity.execute();
            }
        });
    }

    @Override
    public void onMinute() {

    }

    @Override
    public void onShutDown() {

    }
}
