package com.highfly029.scene.manager;

import com.highfly029.core.manager.BaseManager;
import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName DungeonManager
 * @Description 副本管理类
 * @Author liyunpeng
 **/
public class DungeonManager extends BaseManager {

    public DungeonManager(SceneEntityWorld sceneEntityWorld) {
        super(sceneEntityWorld);
    }

    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {

    }

    @Override
    public void onMinute() {

    }

    @Override
    public void onShutDown() {

    }
}
