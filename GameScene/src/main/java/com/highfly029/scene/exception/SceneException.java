package com.highfly029.scene.exception;

/**
 * @ClassName SceneException
 * @Description 场景相关异常
 * @Author liyunpeng
 **/
public class SceneException extends BaseException {
    public SceneException(String message) {
        super(message);
    }

    public SceneException(String message, Throwable cause) {
        super(message, cause);
    }
}
