package com.highfly029.scene.serverProtocol.broker.scene;

import com.google.protobuf.ByteString;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.scene.fight.entity.PlayerEntity;
import com.highfly029.scene.serverProtocol.broker.ISceneBrokerHandler;
import com.highfly029.scene.session.EntitySession;
import com.highfly029.scene.tool.EntitySessionTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName KickOutPlayerEntityBrokerHandler
 * @Description KickOutPlayerEntityBrokerHandler
 * @Author liyunpeng
 **/
public class KickOutPlayerEntityBrokerHandler implements ISceneBrokerHandler {
    @Override
    public int registerPtCode() {
        return PtCode.L2SKickOutPlayerEntity;
    }

    @Override
    public void handle(SceneEntityWorld sceneEntityWorld, int from, int fromIndex, int to, int toIndex, long playerID, ByteString byteString) throws Exception {
        SceneLoggerTool.gameLogger.info("L2SKickOutPlayerEntity playerID={}", playerID);
        EntitySession entitySession = EntitySessionTool.getEntitySession(playerID);
        if (entitySession == null) {
            return;
        }
        entitySession.close();

        PlayerEntity playerEntity = entitySession.getPlayerEntity();
        if (playerEntity == null) {
            return;
        }
        playerEntity.isRemoveNextTick = true;
    }
}
