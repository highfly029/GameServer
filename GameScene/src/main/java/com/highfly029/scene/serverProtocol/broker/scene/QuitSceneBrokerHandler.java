package com.highfly029.scene.serverProtocol.broker.scene;

import com.google.protobuf.ByteString;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.scene.fight.entity.PlayerEntity;
import com.highfly029.scene.serverProtocol.broker.ISceneBrokerHandler;
import com.highfly029.scene.session.EntitySession;
import com.highfly029.scene.tool.EntitySessionTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName QuitSceneBrokerHandler
 * @Description QuitSceneBrokerHandler
 * @Author liyunpeng
 **/
public class QuitSceneBrokerHandler implements ISceneBrokerHandler {
    @Override
    public int registerPtCode() {
        return PtCode.L2SQuitScene;
    }

    @Override
    public void handle(SceneEntityWorld sceneEntityWorld, int from, int fromIndex, int to, int toIndex, long playerID, ByteString byteString) throws Exception {
        EntitySession entitySession = EntitySessionTool.getEntitySession(playerID);
        if (entitySession == null) {
            SceneLoggerTool.gameLogger.error("QuitScene entitySession is null playerID={}", playerID);
            return;
        }
        PlayerEntity playerEntity = entitySession.getPlayerEntity();
        if (playerEntity == null) {
            SceneLoggerTool.gameLogger.error("QuitScene playerEntity is null playerID={}", playerID);
            return;
        }
        //服务端只退出当前场景 由客户端决定是否切换连接或者worldIndex
        playerEntity.isRemoveNextTick = true;
        int sceneID = 0;
        int instanceID = 0;
        if (playerEntity.scene != null) {
            sceneID = playerEntity.scene.sceneID;
            instanceID = playerEntity.scene.instanceID;
        }
        SceneLoggerTool.gameLogger.info("QuitScene playerID={},sceneID={}, instanceID={}", playerID, sceneID, instanceID);
    }
}
