package com.highfly029.scene.serverProtocol.broker;

import com.google.protobuf.ByteString;
import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName ISceneBrokerHandler
 * @Description 服务器之间中转消息处理
 * @Author liyunpeng
 **/
public interface ISceneBrokerHandler {
    /**
     * 注册的协议号
     *
     * @return
     */
    int registerPtCode();

    /**
     * 处理逻辑
     *
     * @param from       源服务器id
     * @param fromIndex  源服务器子世界索引
     * @param to         目标服务器id
     * @param toIndex    目标服务器子世界索引
     * @param playerID   玩家id
     * @param byteString 数据
     * @throws Exception
     */
    void handle(SceneEntityWorld sceneEntityWorld, int from, int fromIndex, int to, int toIndex, long playerID, ByteString byteString) throws Exception;
}
