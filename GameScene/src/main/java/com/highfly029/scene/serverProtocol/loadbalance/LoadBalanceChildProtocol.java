package com.highfly029.scene.serverProtocol.loadbalance;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.common.data.scene.SceneStrategyEnum;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.core.interfaces.IRegisterServerChildProtocol;
import com.highfly029.core.session.Session;
import com.highfly029.core.world.ChildWorld;
import com.highfly029.scene.fight.scene.Scene;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.world.SceneEntityWorld;
import com.highfly029.utils.MathUtils;

/**
 * @ClassName LoadBalanceChildProtocol
 * @Description LoadBalanceChildProtocol
 * @Author liyunpeng
 **/
public class LoadBalanceChildProtocol implements IRegisterServerChildProtocol {
    private SceneEntityWorld sceneEntityWorld;

    @Override
    public void registerChildProtocol(ChildWorld childWorld) throws Exception {
        this.sceneEntityWorld = (SceneEntityWorld) childWorld;

        childWorld.registerServerChildProtocol(PtCode.L2SCreateScene, this::createScene);
    }

    /**
     * 创建场景请求
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void createScene(Session session, int ptCode, GeneratedMessageV3 message) {
        PbScene.L2SCreateScene req = (PbScene.L2SCreateScene) message;
        int sceneID = req.getSceneID();
        PbScene.S2LCreateSceneResult.Builder builder = PbScene.S2LCreateSceneResult.newBuilder();
        builder.setLoadBalanceServerID(req.getLoadBalanceServerID());
        builder.setSceneServerID(req.getSceneServerID());
        builder.setSceneWorldIndex(req.getSceneWorldIndex());
        builder.setSceneID(sceneID);
        builder.setSceneStrategy(req.getSceneStrategy());
        builder.setKey(req.getKey());
        SceneLoggerTool.gameLogger.info("loadBalance createScene loadBalanceServerID={} serverID={}, sceneWorldIndex={}, sceneStrategy={},sceneID={}, key={}",
                req.getLoadBalanceServerID(), req.getSceneServerID(), req.getSceneWorldIndex(), req.getSceneStrategy(), req.getSceneID(), req.getKey());
        //分线场景设置lineID
        int lineID = 0;
        if (req.getSceneStrategy() == SceneStrategyEnum.LINE.ordinal()) {
            lineID = MathUtils.getCompositeArg2(req.getKey());
        }
        Scene scene = sceneEntityWorld.getSceneManager().createScene(sceneID, lineID);
        if (scene != null) {
            scene.setKey(req.getKey());


            builder.setLimitNum(scene.sceneTemplate.getLimitNum());
            builder.setResult(true);
            builder.setInstanceID(scene.instanceID);
            sceneEntityWorld.sendMsg2AllLoadBalanceScene(PtCode.S2LCreateSceneResult, builder.build());
        } else {
            builder.setResult(false);
            session.sendMsgS2S(PtCode.S2LCreateSceneResult, builder.build());
        }
    }
}
