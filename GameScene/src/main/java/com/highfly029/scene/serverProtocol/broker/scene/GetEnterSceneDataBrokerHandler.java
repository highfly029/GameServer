package com.highfly029.scene.serverProtocol.broker.scene;

import com.google.protobuf.ByteString;
import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.common.template.entity.EntityTypeConst;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.scene.fight.entity.Entity;
import com.highfly029.scene.fight.entity.PlayerEntity;
import com.highfly029.scene.fight.scene.Scene;
import com.highfly029.scene.serverProtocol.broker.ISceneBrokerHandler;
import com.highfly029.scene.session.EntitySession;
import com.highfly029.scene.tool.EntitySessionTool;
import com.highfly029.scene.tool.SceneLoggerTool;
import com.highfly029.scene.utils.PbSceneUtils;
import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName GetEnterSceneDataBrokerHandler
 * @Description 获取登陆场景数据
 * @Author liyunpeng
 **/
public class GetEnterSceneDataBrokerHandler implements ISceneBrokerHandler {
    @Override
    public int registerPtCode() {
        return PtCode.Logic2SceneGetEnterSceneData;
    }

    @Override
    public void handle(SceneEntityWorld sceneEntityWorld, int from, int fromIndex, int to, int toIndex, long playerID, ByteString byteString) throws Exception {
        PbScene.Logic2SceneGetEnterSceneData req = PbScene.Logic2SceneGetEnterSceneData.parseFrom(byteString);
        Scene scene = sceneEntityWorld.getSceneManager().getScene(req.getInstanceID());
        if (scene == null) {
            SceneLoggerTool.gameLogger.error("scene GetEnterSceneDataBrokerHandler cant find scene, instanceID={}", req.getInstanceID());
            return;
        }
        Vector3 pos = PbCommonUtils.vector3Pb2Obj(req.getCurPos());
        Vector3 dir = PbCommonUtils.vector3Pb2Obj(req.getCurDir());
        Entity entity = scene.createEntity(EntityTypeConst.PLAYER, pos, dir, false);
        PlayerEntity playerEntity = (PlayerEntity) entity;
        playerEntity.playerID = playerID;

        SceneLoggerTool.gameLogger.info("玩家实体进入场景 playerID={},entityID={}, sceneID={}, lineID={},instanceID={}",
                playerID, entity.entityID, scene.sceneID, scene.lineID, scene.instanceID);

        //设置种族
        playerEntity.race = req.getRace();
        //设置模型
        playerEntity.fightAllLogic.avatarFightLogic.initWithData(req.getModelID(), PbCommonUtils.keyValueList2IntIntMap(req.getPartsList()));

        PbCommonUtils.keyValueList2IntIntMap(req.getActiveSkillsList()).foreachImmutable((skillID, level) -> playerEntity.addActiveSkillData(skillID, level));

        //增加属性
        PbCommonUtils.keyValueList2IntIntMap(req.getAttributesList()).foreachImmutable((attrID, attrValue) -> playerEntity.addAttribute((short) attrID, attrValue));

        //进入场景默认满血
        playerEntity.fightAllLogic.attributeFightLogic.fullHp();

        PlayerEntity prePlayerEntity = scene.getPlayerEntity(playerID);
        //如果在进入的时候还有上一次的entity、则删除、如果有需求不删除再单独处理
        if (prePlayerEntity != null) {
            scene.removeEntity(prePlayerEntity.entityID);
            SceneLoggerTool.fightLogger.error("enterScene exist playerEntity playerID={}, sceneID={}", playerID, scene.sceneID);
        }
        //session和entity互相关联
        EntitySession entitySession = EntitySessionTool.getEntitySession(playerID);
        playerEntity.setEntitySession(entitySession);
        entitySession.setPlayerEntity(playerEntity);

        playerEntity.prepareEnterScene(scene);
        //加入场景
        scene.addEntity(playerEntity);
        PbScene.S2CPlayerEnterScene.Builder builder = PbScene.S2CPlayerEnterScene.newBuilder();
        builder.setResult(true);
        builder.setPlayer(PbSceneUtils.entityObj2Pb(playerEntity));
        playerEntity.getEntitySession().send(PtCode.S2CPlayerEnterScene, builder.build());
    }
}
