package com.highfly029.scene.serverProtocol.loadbalance;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;

/**
 * @ClassName LoadBalanceGlobalProtocol
 * @Description LoadBalanceGlobalProtocol
 * @Author liyunpeng
 **/
public class LoadBalanceGlobalProtocol implements IRegisterServerGlobalProtocol {
    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {
        globalWorld.registerServerGlobalProtocol(PtCode.L2SCreateScene, this::createScene);
    }

    /**
     * 创建场景请求
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void createScene(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbScene.L2SCreateScene req = PbScene.L2SCreateScene.parseFrom(packet.getData());
        WorldTool.addChildWorldLogicEvent(req.getSceneWorldIndex(), LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }
}
