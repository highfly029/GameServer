package com.highfly029.scene.serverProtocol.broker;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.core.interfaces.IRegisterServerChildProtocol;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.world.ChildWorld;
import com.highfly029.scene.world.SceneEntityWorld;

/**
 * @ClassName BrokerChildProtocol
 * @Description BrokerChildProtocol
 * @Author liyunpeng
 **/
public class BrokerChildProtocol implements IRegisterServerChildProtocol {
    private SceneEntityWorld sceneEntityWorld;

    @Override
    public void registerChildProtocol(ChildWorld childWorld) throws Exception {
        this.sceneEntityWorld = (SceneEntityWorld) childWorld;

        childWorld.registerServerChildProtocol(PtCode.LoadBalance2SceneMessage, this::loadBalance2Scene);
    }

    /**
     * 别的服发送到场景服的消息
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void loadBalance2Scene(Session session, int ptCode, GeneratedMessageV3 message) {
        PbLoadBalance.LoadBalance2SceneMessage req = (PbLoadBalance.LoadBalance2SceneMessage) message;
        ISceneBrokerHandler sceneBrokerHandler = sceneEntityWorld.getSceneBrokerHandlers().get(req.getPtCode());
        try {
            sceneBrokerHandler.handle(sceneEntityWorld, req.getFrom(), req.getFromIndex(), req.getToIndex(), req.getToIndex(), req.getPlayerID(), req.getData());
        } catch (Exception e) {
            LoggerTool.systemLogger.error("loadBalance2Scene error, from={}, fromIndex={}, to={}, toIndex={}, ptCode={}, playerID={}",
                    req.getFrom(), req.getFromIndex(), req.getToIndex(), req.getToIndex(), req.getPtCode(), req.getPlayerID());
            LoggerTool.systemLogger.error("loadBalance2Scene", e);
        }
    }
}
