package com.highfly029.scene.serverProtocol.broker;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;

/**
 * @ClassName BrokerGlobalProtocol
 * @Description BrokerGlobalProtocol
 * @Author liyunpeng
 **/
public class BrokerGlobalProtocol implements IRegisterServerGlobalProtocol {
    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {

        globalWorld.registerServerGlobalProtocol(PtCode.LoadBalance2SceneMessage, this::loadBalance2Scene);
    }

    /**
     * 别的服发送到场景服的消息
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void loadBalance2Scene(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbLoadBalance.LoadBalance2SceneMessage req = PbLoadBalance.LoadBalance2SceneMessage.parseFrom(packet.getData());
        WorldTool.addChildWorldLogicEvent(req.getToIndex(), LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }
}
