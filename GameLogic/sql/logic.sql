/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100803
 Source Host           : localhost:3306
 Source Schema         : my_game_logic_1

 Target Server Type    : MySQL
 Target Server Version : 100803
 File Encoding         : 65001

 Date: 07/12/2022 15:09:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `account_id` int(11) NOT NULL COMMENT '帐号id',
  `account_data` longblob DEFAULT NULL COMMENT '帐号数据',
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for global
-- ----------------------------
DROP TABLE IF EXISTS `global`;
CREATE TABLE `global` (
  `initTime` datetime DEFAULT NULL COMMENT '第一次初始化数据库时间',
  `openServerTime` datetime DEFAULT NULL COMMENT '开服时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for mail
-- ----------------------------
DROP TABLE IF EXISTS `mail`;
CREATE TABLE `mail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '邮件唯一id',
  `player_id` bigint(20) NOT NULL COMMENT '玩家角色id',
  `type` int(11) NOT NULL COMMENT '邮件类型',
  `title_id` int(11) NOT NULL COMMENT '标题id',
  `content_id` int(11) NOT NULL COMMENT '内容id',
  `extra_args` blob DEFAULT NULL COMMENT '邮件额外参数',
  `is_read` tinyint(4) DEFAULT NULL COMMENT '是否已读',
  `is_receive` tinyint(4) DEFAULT NULL COMMENT '是否领取附件',
  `attachment` blob DEFAULT NULL COMMENT '附件内容',
  `behavior_source` int(11) DEFAULT NULL COMMENT '行为来源',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `playerIndex` (`player_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for player
-- ----------------------------
DROP TABLE IF EXISTS `player`;
CREATE TABLE `player` (
  `player_id` bigint(20) NOT NULL COMMENT '雪花算法生成',
  `account_id` int(11) NOT NULL COMMENT '帐号id',
  `versionCode` bigint(20) DEFAULT NULL COMMENT '数据版本号',
  `bag` longblob DEFAULT NULL,
  `asset` longblob DEFAULT NULL,
  `equipment` longblob DEFAULT NULL,
  `role` longblob DEFAULT NULL,
  `function` longblob DEFAULT NULL,
  `quest` longblob DEFAULT NULL,
  `unionData` longblob DEFAULT NULL,
  PRIMARY KEY (`player_id`) USING BTREE,
  KEY `account` (`account_id`) USING BTREE,
  KEY `player_account` (`player_id`,`account_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for system_mail
-- ----------------------------
DROP TABLE IF EXISTS `system_mail`;
CREATE TABLE `system_mail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `start_time` bigint(20) DEFAULT NULL COMMENT '邮件开始时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '邮件结束时间',
  `reg_start_time` bigint(20) DEFAULT NULL COMMENT '玩家注册开始时间',
  `reg_end_time` bigint(20) DEFAULT NULL COMMENT '玩家注册结束时间',
  `level_min` int(11) DEFAULT NULL COMMENT '玩家最小等级',
  `level_max` int(11) DEFAULT NULL COMMENT '玩家最大等级',
  `title_id` int(11) DEFAULT NULL COMMENT '标题id',
  `content_id` int(11) DEFAULT NULL COMMENT '内容id',
  `extra_args` blob DEFAULT NULL COMMENT '邮件额外参数',
  `attachment` blob DEFAULT NULL COMMENT '附件内容',
  `channel` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '渠道',
  `operator` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '操作人员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

SET FOREIGN_KEY_CHECKS = 1;
