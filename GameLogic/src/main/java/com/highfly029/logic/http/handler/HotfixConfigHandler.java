package com.highfly029.logic.http.handler;

import com.highfly029.core.net.http.BaseGlobalHttpHandler;
import com.highfly029.core.tool.ThreadPoolExecutorTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.core.world.WorldThread;
import com.highfly029.logic.http.LogicHttpPathEnum;
import com.highfly029.logic.tool.LogicLoggerTool;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @ClassName HotfixConfigHandler
 * @Description HotfixConfigHandler
 * @Author liyunpeng
 **/
public class HotfixConfigHandler extends BaseGlobalHttpHandler {

    @Override
    public void init(WorldThread worldThread) {

    }

    @Override
    public String register() {
        return LogicHttpPathEnum.hotfixConfig.path();
    }

    @Override
    public void handler(Channel channel, FullHttpRequest request) {
        ThreadPoolExecutorTool.execute(() -> {
            LogicLoggerTool.gameLogger.info("hotfixConfig begin load");
            try {
                WorldTool.getInstance().getGlobalWorld().handleAllConfigs(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            LogicLoggerTool.gameLogger.info("hotfixConfig end load");
        });
        HttpUtils.sendHttpResponse(channel, "hotfixConfig success");
    }
}
