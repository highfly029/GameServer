package com.highfly029.logic.http;

import com.highfly029.core.interfaces.IHttpPath;

/**
 * @ClassName LogicHttpPathEnum
 * @Description LogicHttpPathEnum
 * @Author liyunpeng
 **/
public enum LogicHttpPathEnum implements IHttpPath {

    /**
     * 热更config
     */
    hotfixConfig("hotfixConfig"),

    /**
     * 热更property
     */
    hotfixProperty("hotfixProperty"),

    ;

    private final String path;

    public String path() {
        return path;
    }

    LogicHttpPathEnum(String path) {
        this.path = path;
    }

    /**
     * 是否只有一个child处理
     *
     * @return
     */
    @Override
    public boolean isOneChildHandleOnly() {
        return false;
    }

    /**
     * 是否所有child处理
     *
     * @return
     */
    @Override
    public boolean isAllChildHandle() {
        return false;
    }
}
