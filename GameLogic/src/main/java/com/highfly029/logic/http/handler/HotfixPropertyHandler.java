package com.highfly029.logic.http.handler;

import com.highfly029.core.net.http.BaseGlobalHttpHandler;
import com.highfly029.core.tool.ThreadPoolExecutorTool;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.core.world.WorldThread;
import com.highfly029.logic.http.LogicHttpPathEnum;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.utils.ConfigPropUtils;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @ClassName HotfixPropertyHandler
 * @Description HotfixPropertyHandler
 * @Author liyunpeng
 **/
public class HotfixPropertyHandler extends BaseGlobalHttpHandler {
    @Override
    public void init(WorldThread worldThread) {

    }

    @Override
    public String register() {
        return LogicHttpPathEnum.hotfixProperty.path();
    }

    @Override
    public void handler(Channel channel, FullHttpRequest request) {
        ThreadPoolExecutorTool.execute(() -> {
            LogicLoggerTool.gameLogger.info("hotfixProperty begin load {}", ConfigPropUtils.getValue("game.a"));
            try {
                ConfigPropUtils.load();
            } catch (Exception e) {
                e.printStackTrace();
            }
            LogicLoggerTool.gameLogger.info("hotfixProperty end load {}", ConfigPropUtils.getValue("game.a"));
        });
        HttpUtils.sendHttpResponse(channel, "hotfixProperty success");
    }
}
