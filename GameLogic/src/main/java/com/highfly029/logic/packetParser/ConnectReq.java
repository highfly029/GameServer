package com.highfly029.logic.packetParser;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.core.protocol.BasePtCode;

/**
 * @ClassName ConnectReq
 * @Description ConnectReq
 * @Author liyunpeng
 **/
public class ConnectReq implements IPacketParser {
    @Override
    public int getPtCode() {
        return BasePtCode.CONNECT_TYPE_REQ;
    }

    @Override
    public String getName() {
        return "CONNECT_TYPE_REQ";
    }

    @Override
    public boolean isParserHex() {
        return true;
    }

    @Override
    public String parser(ByteString byteString) {
        try {
            PbCommon.ConnectTypeReq pb = PbCommon.ConnectTypeReq.parseFrom(byteString);
            return pb.toString();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return null;
    }
}
