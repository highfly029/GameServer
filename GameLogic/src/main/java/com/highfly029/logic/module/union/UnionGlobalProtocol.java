package com.highfly029.logic.module.union;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.world.Global2LogicEvent;
import com.highfly029.logic.world.LogicGlobalWorld;

/**
 * @ClassName UnionGlobalProtocol
 * @Description UnionGlobalProtocol
 * @Author liyunpeng
 **/
public class UnionGlobalProtocol implements IRegisterServerGlobalProtocol {
    private LogicGlobalWorld logicGlobalWorld;

    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {
        logicGlobalWorld = (LogicGlobalWorld) globalWorld;

        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicGetUnionList, this::getUnionListResult);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicCreateUnion, this::createUnionResult);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicSyncPlayerUnionInfo, this::syncPlayerUnionInfoResult);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicQuitUnion, this::quitUnionResult);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicDisbandUnion, this::disbandUnionResult);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicUpdateUnion, this::updateUnionFromCenter);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicJoinUnionDoubleCheck, this::joinUnionDoubleCheck);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicApplyJoinUnion, this::applyJoinUnionResult);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicInviteJoinUnion, this::inviteJoinUnionNotice);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicHandleJoinUnion, this::handleJoinUnionResult);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicHandleInviteJoinUnion, this::onHandleBeInviteJoinUnionResult);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicKickOutUnionMember, this::kickOutMemberResult);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicChangeLeader, this::changeLeaderResult);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicModifyUnionName, this::modifyUnionNameResult);
    }

    /**
     * 获取联盟列表
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void getUnionListResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicGetUnionList req = PbUnion.Center2LogicGetUnionList.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("getUnionList not exist playerID={}, index={}", playerID, index);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

    /**
     * 创建联盟结果
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void createUnionResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicCreateUnion req = PbUnion.Center2LogicCreateUnion.parseFrom(packet.getData());
        long createPlayerID = req.getCreatePlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(req.getCreatePlayerID());
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("createUnionResult not exist playerID={}, index={}", createPlayerID, index);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

    /**
     * 拉取联盟信息的结果
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void syncPlayerUnionInfoResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicSyncPlayerUnionInfo req = PbUnion.Center2LogicSyncPlayerUnionInfo.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("syncPlayerUnionInfoResult not exist playerID={}, index={}", playerID, index);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

    /**
     * 退出联盟结果
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void quitUnionResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicQuitUnion req = PbUnion.Center2LogicQuitUnion.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("quitUnionResult not exist playerID={}, index={}", playerID, index);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

    /**
     * 解散联盟结果
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void disbandUnionResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicDisbandUnion req = PbUnion.Center2LogicDisbandUnion.parseFrom(packet.getData());
        long playerID = req.getPlayerID();

        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("disbandUnionResult not exist playerID={}, index={}", playerID, index);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);

    }

    /**
     * 申请加入联盟结果
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void applyJoinUnionResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicApplyJoinUnion req = PbUnion.Center2LogicApplyJoinUnion.parseFrom(packet.getData());

        long playerID = req.getPlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("applyJoinUnionResult not exist playerID={}, index={}", playerID, index);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);

    }

    /**
     * center服更新联盟数据
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void updateUnionFromCenter(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicUpdateUnion req = PbUnion.Center2LogicUpdateUnion.parseFrom(packet.getData());

        for (int i = 0, len = req.getPlayersCount(); i < len; i++) {
            long playerID = req.getPlayers(i);
            int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
            if (index <= 0) {
                LogicLoggerTool.gameLogger.error("updateUnionFromCenter not exist playerID={}, index={}", playerID, index);
                continue;
            }
            //更新到每个玩家所在的childWorld
            logicGlobalWorld.postEvent2Child(index, Global2LogicEvent.UPDATE_UNION_DATA, playerID, req.getUpdateUnionData());
        }
    }

    /**
     * 加入联盟检测
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void joinUnionDoubleCheck(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicJoinUnionDoubleCheck req = PbUnion.Center2LogicJoinUnionDoubleCheck.parseFrom(packet.getData());
        long playerID = req.getTargetPlayerID();
        logicGlobalWorld.pushOfflineMsg2Player(playerID, ptCode, req);
    }

    /**
     * 邀请加入联盟通知
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void inviteJoinUnionNotice(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicInviteJoinUnion req = PbUnion.Center2LogicInviteJoinUnion.parseFrom(packet.getData());

        long playerID = req.getTargetPlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("inviteJoinUnionResult not exist playerID={}, index={}", playerID, index);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

    /**
     * 处理入盟消息结果
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void handleJoinUnionResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicHandleJoinUnion req = PbUnion.Center2LogicHandleJoinUnion.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("handleJoinUnionResult not exist playerID={}, index={}", playerID, index);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

    /**
     * 玩家响应邀请加入联盟结果
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void onHandleBeInviteJoinUnionResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicHandleInviteJoinUnion req = PbUnion.Center2LogicHandleInviteJoinUnion.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("onHandleBeInviteJoinUnionResult not exist playerID={}, index={}", playerID, index);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }


    /**
     * 踢出成员结果
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void kickOutMemberResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicKickOutUnionMember req = PbUnion.Center2LogicKickOutUnionMember.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("kickOutMemberResult not exist playerID={}, index={}", playerID, index);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

    /**
     * 更换盟主结果
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void changeLeaderResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicChangeLeader req = PbUnion.Center2LogicChangeLeader.parseFrom(packet.getData());
        long playerID = req.getPlayerID();

        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("changeLeaderResult not exist playerID={}, index={}", playerID, index);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);

    }


    /**
     * 修改联盟名字
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void modifyUnionNameResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbUnion.Center2LogicModifyUnionName req = PbUnion.Center2LogicModifyUnionName.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("modifyUnionNameResult not exist playerID={}, index={}", playerID, index);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

}
