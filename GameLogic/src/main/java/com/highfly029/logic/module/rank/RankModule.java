package com.highfly029.logic.module.rank;

import java.util.List;

import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbRank;
import com.highfly029.common.redis.RedisKey;
import com.highfly029.common.redisRank.BaseRedisRank;
import com.highfly029.common.redisRank.RedisRankData;
import com.highfly029.logic.module.BaseModule;

/**
 * @ClassName RankModule
 * @Description RankModule
 * @Author liyunpeng
 **/
public class RankModule extends BaseModule {

    /**
     * 获取排行榜
     * @param argsList
     */
    public void getRankList(List<Integer> argsList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int arg : argsList) {
            stringBuilder.append(arg).append(":");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        String key = RedisKey.REDIS_PREFIX_KEY.value() + stringBuilder;

        BaseRedisRank rank = BaseRedisRank.getRedisRank(key);
        if (rank == null) {
            player.error("getRankList invalid key", key);
            return;
        }
        rank.getRankList(player.getChildWorldIndex(), consumer -> {
            PbRank.S2CGetRankListResult.Builder builder = PbRank.S2CGetRankListResult.newBuilder();
            List<RedisRankData> list = (List<RedisRankData>) consumer;
            for (RedisRankData data : list) {
                builder.addRanks(data.toPbRankData());
            }
            player.send(PtCode.S2CGetRankListResult, builder.build());
        });
    }

}
