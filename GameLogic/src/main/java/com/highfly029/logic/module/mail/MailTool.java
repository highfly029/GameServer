package com.highfly029.logic.module.mail;

import java.util.ArrayList;
import java.util.List;

import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbMail;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.logic.common.container.item.ItemData;
import com.highfly029.logic.utils.PbLogicUtils;
import com.highfly029.utils.collection.IntLongMap;
import com.highfly029.utils.collection.ObjectList;

import io.vertx.core.buffer.Buffer;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName MailTool
 * @Description MailTool
 * @Author liyunpeng
 **/
public class MailTool {

    private MailTool() {
    }

    /**
     * 创建邮件数据
     *
     * @param type
     * @param titleID
     * @param contentID
     * @param behaviorSource
     * @param titleArgs
     * @param contentArgs
     * @param itemList
     * @param assetMap
     * @return
     */
    public static MailData createMailData(int type, int titleID, int contentID, int behaviorSource,
                                          ObjectList<String> titleArgs, ObjectList<String> contentArgs,
                                          ObjectList<ItemData> itemList, IntLongMap assetMap) {
        MailData mailData = new MailData();
        mailData.setType(type);
        mailData.setTitleID(titleID);
        mailData.setContentID(contentID);
        mailData.setBehaviorSource(behaviorSource);

        if (titleArgs != null || contentArgs != null) {
            MailExtraArgs mailExtraArgs = new MailExtraArgs();
            if (titleArgs != null) {
                mailExtraArgs.setTitleArgs(titleArgs);
            }
            if (contentArgs != null) {
                mailExtraArgs.setContentArgs(contentArgs);
            }
            mailData.setMailExtraArgs(mailExtraArgs);
        }
        if (itemList != null || assetMap != null) {
            MailAttachment mailAttachment = new MailAttachment();
            if (itemList != null) {
                mailAttachment.setItemList(itemList);
            }
            if (assetMap != null) {
                mailAttachment.setAssetMap(assetMap);
            }
            mailData.setAttachment(mailAttachment);
        }
        mailData.setCreateTime(SystemTimeTool.getMillTime());
        return mailData;
    }

    /**
     * 向数据库插入邮件数据
     *
     * @param playerID
     * @param mailData
     */
    public static Tuple createMailTuple(long playerID, MailData mailData) {
        List<Object> dataList = new ArrayList<>();
        dataList.add(0);//mailID那一列
        dataList.add(playerID);
        dataList.add(mailData.getType());
        dataList.add(mailData.getTitleID());
        dataList.add(mailData.getContentID());
        PbMail.PbMailExtraArgs.Builder extraArgsBuilder = PbMail.PbMailExtraArgs.newBuilder();
        if (mailData.getMailExtraArgs() != null) {
            ObjectList<String> titleArgs = mailData.getMailExtraArgs().getTitleArgs();
            if (titleArgs != null && !titleArgs.isEmpty()) {
                for (int i = 0; i < titleArgs.size(); i++) {
                    extraArgsBuilder.addTitleArgs(titleArgs.get(i));
                }
            }
            ObjectList<String> contentArgs = mailData.getMailExtraArgs().getContentArgs();
            if (contentArgs != null && !contentArgs.isEmpty()) {
                for (int i = 0; i < contentArgs.size(); i++) {
                    extraArgsBuilder.addContentArgs(contentArgs.get(i));
                }
            }
        }
        Buffer mailArgsBuffer = Buffer.buffer(extraArgsBuilder.build().toByteArray());
        dataList.add(mailArgsBuffer);
        dataList.add(0);
        dataList.add(0);
        PbMail.PbMailAttachment.Builder attachmentBuilder = PbMail.PbMailAttachment.newBuilder();
        if (mailData.getAttachment() != null) {
            ObjectList<ItemData> itemList = mailData.getAttachment().getItemList();
            if (itemList != null && !itemList.isEmpty()) {
                for (int i = 0; i < itemList.size(); i++) {
                    ItemData itemData = itemList.get(i);
                    PbCommon.PbItemData pbItemData = PbLogicUtils.itemObj2Pb(itemData, 0);
                    attachmentBuilder.addItemList(pbItemData);
                }
            }
            IntLongMap assetMap = mailData.getAttachment().getAssetMap();
            if (assetMap != null && !assetMap.isEmpty()) {
                assetMap.foreachImmutable((k, v) -> {
                    PbCommon.PbKeyValue.Builder kvBuilder = PbCommon.PbKeyValue.newBuilder();
                    kvBuilder.setIntKey(k);
                    kvBuilder.setLongValue(v);
                    attachmentBuilder.addAssetList(kvBuilder);
                });
            }
        }
        Buffer attachmentBuffer = Buffer.buffer(attachmentBuilder.build().toByteArray());
        dataList.add(attachmentBuffer);
        dataList.add(mailData.getBehaviorSource());
        dataList.add(mailData.getCreateTime());
        return Tuple.wrap(dataList);
    }
}
