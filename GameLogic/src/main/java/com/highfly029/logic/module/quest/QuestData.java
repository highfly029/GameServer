package com.highfly029.logic.module.quest;

import com.highfly029.common.template.quest.QuestExecuteOrderConst;
import com.highfly029.common.template.quest.QuestTemplate;
import com.highfly029.logic.module.goal.GoalData;

/**
 * @ClassName QuestData
 * @Description 任务数据
 * @Author liyunpeng
 **/
public class QuestData {
    /**
     * 任务id
     */
    private int questID;
    /**
     * 过期时间戳, 0表示永远不会过期
     */
    private long invalidTime = 0L;
    /**
     * 任务目标数据
     */
    private GoalData[] goals;
    /**
     * 任务模板
     */
    private QuestTemplate questTemplate;

    public int getQuestID() {
        return questID;
    }

    public void setQuestID(int questID) {
        this.questID = questID;
    }

    public long getInvalidTime() {
        return invalidTime;
    }

    public void setInvalidTime(long invalidTime) {
        this.invalidTime = invalidTime;
    }

    public GoalData[] getGoals() {
        return goals;
    }

    public void setGoals(GoalData[] goals) {
        this.goals = goals;
    }

    public QuestTemplate getQuestTemplate() {
        return questTemplate;
    }

    public void setQuestTemplate(QuestTemplate questTemplate) {
        this.questTemplate = questTemplate;
    }

    public static QuestData create() {
        QuestData questData = new QuestData();
        return questData;
    }

    /**
     * 是否完成
     *
     * @return
     */
    public boolean isComplete() {
        if (questTemplate.getGoals() == null) {
            //如果此任务没有目标、则默认完成
            return true;
        } else {
            if (questTemplate.getQuestExecuteOrder() == QuestExecuteOrderConst.Anyone) {
                //只需要完成一个目标
                for (GoalData goalData : goals) {
                    if (goalData.isComplete()) {
                        return true;
                    }
                }
                return false;
            } else if (questTemplate.getQuestExecuteOrder() == QuestExecuteOrderConst.Serial) {
                //按顺序全部完成
                //目标还没有全部都接取
                if (goals.length < questTemplate.getGoals().length) {
                    return false;
                }
                //已全部接取但没有全部完成
                for (GoalData goalData : goals) {
                    if (!goalData.isComplete()) {
                        return false;
                    }
                }
                return true;
            } else {
                //需要全部完成
                for (GoalData goalData : goals) {
                    if (!goalData.isComplete()) {
                        return false;
                    }
                }
                return true;
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("questID=").append(questID).append("(");
        for (int i = 0, len = goals.length; i < len; i++) {
            GoalData v = goals[i];
            if (v != null) {
                stringBuilder.append("goalID=").append(v.getGoalID()).append(", progress=").append(v.getProgress());
            }
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}
