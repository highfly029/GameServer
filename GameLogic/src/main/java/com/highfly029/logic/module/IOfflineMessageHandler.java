package com.highfly029.logic.module;

import com.google.protobuf.ByteString;

/**
 * @ClassName IOfflineMessageHandler
 * @Description 离线消息解析器
 * @Author liyunpeng
 **/
public interface IOfflineMessageHandler {
    /**
     * 注册的协议号
     *
     * @return
     */
    int registerPtCode();

    /**
     * 处理逻辑
     *
     * @param player
     * @param byteString
     */
    void handle(Player player, ByteString byteString) throws Exception;
}
