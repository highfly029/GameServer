package com.highfly029.logic.module.rank;

import com.highfly029.common.redisRank.PlayerRedisRank;
import com.highfly029.common.redisRank.PlayerRedisRankData;
import com.highfly029.common.redisRank.RedisRankKey;
import com.highfly029.common.redisRank.maker.IRankScoreMaker;
import com.highfly029.common.redisRank.maker.StandardMaker;
import com.highfly029.common.redisRank.provider.StandardProvider;
import com.highfly029.common.template.rank.RankTypeConst;

/**
 * @ClassName PlayerLevelRank
 * @Description 玩家等级排行榜
 * @Author liyunpeng
 **/
public class PlayerLevelRank extends PlayerRedisRank<StandardProvider, PlayerRedisRankData> {
    public PlayerLevelRank(RedisRankKey redisRankKey, IRankScoreMaker<StandardProvider, PlayerRedisRankData> rankScoreMaker) {
        super(redisRankKey, rankScoreMaker);
    }

    @Override
    protected PlayerRedisRankData createRedisRankData() {
        return new PlayerRedisRankData();
    }

    private static class LazyHolder {
        private static final PlayerLevelRank INSTANCE = new PlayerLevelRank(new RedisRankKey(RankTypeConst.PlayerLevel), new StandardMaker<>());
    }

    public static PlayerLevelRank getInstance() {
        return LazyHolder.INSTANCE;
    }
}
