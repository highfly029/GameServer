package com.highfly029.logic.module.gm;

import com.highfly029.logic.module.Player;

/**
 * @ClassName GMCmd
 * @Description gm命令对象
 * @Author liyunpeng
 **/
public class GMCmd {
    /**
     * 命令回调
     */
    private GMCallback callback;

    public GMCmd(GMCallback callback) {
        this.callback = callback;
    }

    public String call(Player player, String[] stringCmdArray, int[] intCmdArray) {
        return callback.call(player, stringCmdArray, intCmdArray);
    }
}
