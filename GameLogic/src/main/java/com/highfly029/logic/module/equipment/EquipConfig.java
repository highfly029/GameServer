package com.highfly029.logic.module.equipment;

import com.highfly029.common.template.equipment.EquipmentFashionSlotTemplate;
import com.highfly029.common.template.equipment.EquipmentNormalSlotTemplate;
import com.highfly029.common.template.equipment.EquipmentSuitTemplate;
import com.highfly029.common.template.equipment.EquipmentTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName EquipConfig
 * @Description EquipConfig
 * @Author liyunpeng
 **/
public class EquipConfig extends BaseGlobalConfig {
    private final IntObjectMap<EquipmentTemplate> equipmentTemplates = new IntObjectMap<>(EquipmentTemplate[]::new);
    private final IntObjectMap<EquipmentNormalSlotTemplate> normalSlotTemplates = new IntObjectMap<>(EquipmentNormalSlotTemplate[]::new);
    private final IntObjectMap<EquipmentFashionSlotTemplate> fashionSlotTemplates = new IntObjectMap<>(EquipmentFashionSlotTemplate[]::new);
    private final IntObjectMap<EquipmentSuitTemplate> equipmentSuitTemplates = new IntObjectMap<>(EquipmentSuitTemplate[]::new);

    @Override
    public void check(boolean isHotfix) {

    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(EquipmentTemplate.class);
        EquipmentTemplate equipmentTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            equipmentTemplate = (EquipmentTemplate) list.get(i);
            equipmentTemplates.put(equipmentTemplate.getId(), equipmentTemplate);
        }

        list = TemplateTool.dataTemplates.get(EquipmentNormalSlotTemplate.class);
        EquipmentNormalSlotTemplate equipmentSlotTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            equipmentSlotTemplate = (EquipmentNormalSlotTemplate) list.get(i);
            normalSlotTemplates.put(equipmentSlotTemplate.getId(), equipmentSlotTemplate);
        }

        list = TemplateTool.dataTemplates.get(EquipmentFashionSlotTemplate.class);
        EquipmentFashionSlotTemplate fashionSlotTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            fashionSlotTemplate = (EquipmentFashionSlotTemplate) list.get(i);
            fashionSlotTemplates.put(fashionSlotTemplate.getId(), fashionSlotTemplate);
        }

        list = TemplateTool.dataTemplates.get(EquipmentSuitTemplate.class);
        EquipmentSuitTemplate equipmentSuitTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            equipmentSuitTemplate = (EquipmentSuitTemplate) list.get(i);
            equipmentSuitTemplates.put(equipmentSuitTemplate.getId(), equipmentSuitTemplate);
        }
    }

    /**
     * 获取装备模板
     *
     * @param equipmentID
     * @return
     */
    public EquipmentTemplate getEquipmentTemplate(int equipmentID) {
        return equipmentTemplates.get(equipmentID);
    }

    /**
     * 获取装备套装模板对应的套装数量的actions
     *
     * @param suitID
     * @param suitNum
     * @return
     */
    public int[][] getEquipmentSuitActions(int suitID, int suitNum) {
        EquipmentSuitTemplate equipmentSuitTemplate = equipmentSuitTemplates.get(suitID);
        if (equipmentSuitTemplate == null) {
            return null;
        }
        int[][] actions = null;
        switch (suitNum) {
            case 1 -> {
                actions = equipmentSuitTemplate.getAddPlayerActions1();
            }
            case 2 -> {
                actions = equipmentSuitTemplate.getAddPlayerActions2();
            }
            case 3 -> {
                actions = equipmentSuitTemplate.getAddPlayerActions3();
            }
            case 4 -> {
                actions = equipmentSuitTemplate.getAddPlayerActions4();
            }
            case 5 -> {
                actions = equipmentSuitTemplate.getAddPlayerActions5();
            }
            case 6 -> {
                actions = equipmentSuitTemplate.getAddPlayerActions6();
            }
            case 7 -> {
                actions = equipmentSuitTemplate.getAddPlayerActions7();
            }
        }
        return actions;
    }

    /**
     * 获取普通装备槽位模版
     *
     * @return
     */
    public IntObjectMap<EquipmentNormalSlotTemplate> getNormalSlotTemplates() {
        return normalSlotTemplates;
    }

    /**
     * 获取时装槽位模版
     *
     * @return
     */
    public IntObjectMap<EquipmentFashionSlotTemplate> getFashionSlotTemplates() {
        return fashionSlotTemplates;
    }
}
