package com.highfly029.logic.module.role;

import com.highfly029.common.template.roleLevel.RoleLevelTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName RoleConfig
 * @Description RoleConfig
 * @Author liyunpeng
 **/
public class RoleConfig extends BaseGlobalConfig {
    private final ObjectList<RoleLevelTemplate> templates = new ObjectList<>(RoleLevelTemplate[]::new);

    @Override
    public void check(boolean isHotfix) {

    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(RoleLevelTemplate.class);
        RoleLevelTemplate template;

        //等级0目前为null
        templates.add(null);
        for (int i = 0, size = list.size(); i < size; i++) {
            template = (RoleLevelTemplate) list.get(i);
            templates.add(template);
        }
    }

    /**
     * 获取角色等级模版
     *
     * @param level
     * @return
     */
    public RoleLevelTemplate getLevelTemplate(int level) {
        return templates.get(level);
    }
}
