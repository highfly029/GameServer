package com.highfly029.logic.module.activity;

import com.highfly029.common.template.activity.ActivityTemplate;
import com.highfly029.common.template.activity.ActivityTypeConst;
import com.highfly029.logic.module.activity.impl.Test1;
import com.highfly029.logic.module.activity.impl.Test2;
import com.highfly029.logic.module.activity.impl.Test3;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.tool.ConfigTool;

/**
 * @ClassName ActivityFactory
 * @Description ActivityFactory
 * @Author liyunpeng
 **/
public class ActivityFactory {
    public static BaseActivity create(Player player, int id) {
        ActivityConfig activityConfig = ConfigTool.getActivityConfig();
        ActivityTemplate activityTemplate = activityConfig.getActivityTemplateMap().get(id);
        switch (activityTemplate.getActivityType()) {
            case ActivityTypeConst.ActivityTest1 -> {
                return new Test1(player, activityTemplate);
            }
            case ActivityTypeConst.ActivityTest2 -> {
                return new Test2(player, activityTemplate);
            }
            case ActivityTypeConst.ActivityTest3 -> {
                return new Test3(player, activityTemplate);
            }
            default -> {
                return null;
            }
        }
    }
}
