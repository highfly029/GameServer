package com.highfly029.logic.module.mail;

import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName MailExtraArgs
 * @Description MailExtraArgs
 * @Author liyunpeng
 **/
public class MailExtraArgs {
    /**
     * 邮件标题参数
     */
    private ObjectList<String> titleArgs;
    /**
     * 邮件内容参数
     */
    private ObjectList<String> contentArgs;

    public ObjectList<String> getTitleArgs() {
        return titleArgs;
    }

    public void setTitleArgs(ObjectList<String> titleArgs) {
        this.titleArgs = titleArgs;
    }

    public ObjectList<String> getContentArgs() {
        return contentArgs;
    }

    public void setContentArgs(ObjectList<String> contentArgs) {
        this.contentArgs = contentArgs;
    }

    @Override
    public String toString() {
        return "MailExtraArgs{" +
                "titleArgs=" + titleArgs +
                ", contentArgs=" + contentArgs +
                '}';
    }
}
