package com.highfly029.logic.module.goal;

import com.highfly029.common.template.goal.GoalTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName GoalConfig
 * @Description GoalConfig
 * @Author liyunpeng
 **/
public class GoalConfig extends BaseGlobalConfig {
    private final IntObjectMap<GoalTemplate> templates = new IntObjectMap<>(GoalTemplate[]::new);

    @Override
    public void check(boolean isHotfix) {

    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(GoalTemplate.class);
        GoalTemplate template;
        for (int i = 0, size = list.size(); i < size; i++) {
            template = (GoalTemplate) list.get(i);
            templates.put(template.getGoalID(), template);
        }
    }

    /**
     * 获取goal模板
     *
     * @param goalID
     * @return
     */
    public GoalTemplate getGoalTemplate(int goalID) {
        return templates.get(goalID);
    }
}
