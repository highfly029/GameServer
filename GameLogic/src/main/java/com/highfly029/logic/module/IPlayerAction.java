package com.highfly029.logic.module;

/**
 * @ClassName PlayerActionInterface
 * @Description 角色行为接口
 * @Author liyunpeng
 **/
public interface IPlayerAction {

    /**
     * 初始化玩家数据
     */
    void onPlayerInitData();

    /**
     * 创建角色
     */
    void onPlayerCreateNewRole();

    /**
     * 玩家登录之前
     */
    void onPlayerBeforeLogin();

    /**
     * 玩家登录推送基础数据
     */
    void onPlayerLoginPushBaseClientData();

    /**
     * 玩家登录推送后续数据 可以在进入场景之后触发
     */
    void onPlayerLoginPushNextClientData();

    /**
     * 玩家登录之后
     */
    void onPlayerAfterLogin();

    /**
     * 删除角色
     */
    void onPlayerDeleteRole();

    /**
     * 玩家断线
     */
    void onPlayerOffline();

    /**
     * 玩家重连登陆
     */
    void onPlayerReconnectLogin();

    /**
     * 玩家登出
     */
    void onPlayerLogout();

    /**
     * 每个玩家每帧调用
     *
     * @param escapedMillTime
     */
    void onPlayerTick(int escapedMillTime);

    /**
     * 每个玩家每秒调用
     */
    void onPlayerSecond();

    /**
     * 每个玩家每分钟调用
     */
    void onPlayerMinute();

    /**
     * 每个玩家跨天调用
     *
     * @param isLogin 是否登陆跨天
     */
    void onPlayerDaily(boolean isLogin);
}
