package com.highfly029.logic.module.activity;

import com.highfly029.common.protocol.packet.PbActivity;
import com.highfly029.common.template.activity.ActivityTemplate;
import com.highfly029.logic.module.Player;

/**
 * @ClassName BaseActivity
 * @Description 活动基类
 * @Author liyunpeng
 **/
public abstract class BaseActivity {
    /**
     * 玩家引用
     */
    protected Player player;
    /**
     * 活动配置数据
     */
    protected ActivityTemplate activityTemplate;

    public BaseActivity(Player player, ActivityTemplate activityTemplate) {
        this.player = player;
        this.activityTemplate = activityTemplate;
    }

    public int getId() {
        return activityTemplate.getId();
    }

    /**
     * obj转pb
     *
     * @return
     */
    public abstract PbActivity.PbActivityData activityDataToPb();

    /**
     * pb转obj
     *
     * @param pbActivityData
     */
    public abstract void activityDataInitFromPb(PbActivity.PbActivityData pbActivityData);

    /**
     * 活动开启
     */
    public void onOpen() {

    }

    /**
     * 活动关闭
     */
    public void onClose(boolean forceClose) {

    }

    public void onPlayerBeforeLogin() {

    }

    public void onPlayerAfterLogin() {

    }

    public void onSecond() {

    }

    /**
     * 活动跨天
     */
    public void onDaily(boolean isLogin) {

    }
}
