package com.highfly029.logic.module.scene;

import java.util.function.Consumer;

import com.highfly029.common.data.feature.LoadBalanceFeature;
import com.highfly029.common.data.base.Location;
import com.highfly029.common.data.feature.SceneFeature;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.core.session.Session;
import com.highfly029.core.session.SessionType;
import com.highfly029.logic.module.BaseModule;

/**
 * @ClassName SceneModule
 * @Description 场景模块
 * @Author liyunpeng
 **/
public class SceneModule extends BaseModule {
    /**
     * 向负载均衡服获取指定场景服务器信息
     * 进入场景的第一步
     *
     * @param sceneID 场景ID
     */
    private void getSceneLocation(int sceneID, Consumer<PbScene.S2LGetSceneServerInfo.Builder> consumer) {
        PbScene.S2LGetSceneServerInfo.Builder builder = PbScene.S2LGetSceneServerInfo.newBuilder();
        builder.setPlayerID(player.getPlayerID());
        builder.setLogicWorldIndex(player.getChildWorldIndex());
        builder.setSceneFeature(SceneFeature.NORMAL_SCENE);
        builder.setSceneStrategy(SceneConfig.getSceneStrategy(sceneID));
        builder.setSceneID(sceneID);
        consumer.accept(builder);
        Session session = player.getLogicChildWorld().getRandomSession(SessionType.LoadBalance, LoadBalanceFeature.SCENE);
        if (session == null) {
            player.error("getSceneLocation empty", new Exception());
            return;
        }
        session.sendMsgS2S(PtCode.S2LGetSceneServerInfo, builder.build());
    }

    /**
     * 进入家园场景
     *
     * @param sceneID
     * @param homePlayerID
     */
    public void enterHomeSceneLocation(int sceneID, long homePlayerID) {
        getSceneLocation(sceneID, builder -> builder.setHomePlayerID(homePlayerID));
    }

    /**
     * 进入工会场景
     *
     * @param sceneID
     * @param unionID
     */
    public void enterUnionSceneLocation(int sceneID, long unionID) {
        getSceneLocation(sceneID, builder -> builder.setUnionID(unionID));
    }

    /**
     * 进入分线场景
     *
     * @param sceneID
     * @param lineID  可以=0
     */
    public void enterLinedSceneLocation(int sceneID, int lineID) {
        getSceneLocation(sceneID, builder -> builder.setLineID(lineID));
    }

    /**
     * 检测是否能够进入场景
     *
     * @param sceneID
     * @return
     */
    private boolean checkCanEnterScene(int sceneID, int lineID) {
        Location location = player.getLocation();
        if (location.getSceneID() == sceneID && location.getLineID() == lineID) {
            player.warn("checkCanEnterScene duplicate scene", sceneID, lineID);
            return false;
        }
        return true;
    }

    /**
     * 收到负载均衡服决策后分配的场景服信息
     */
    public void onGetSceneServerInfo(PbScene.L2SGetSceneServerInfo req) {
        PbScene.S2CPreEnterSceneInfo.Builder builder = PbScene.S2CPreEnterSceneInfo.newBuilder();
        builder.setHost(req.getClientHost());
        builder.setPort(req.getClientPort());
        builder.setSceneID(req.getSceneID());
        builder.setSceneWorldIndex(req.getSceneWorldIndex());
        builder.setInstanceID(req.getInstanceID());
        player.getLocation().setSceneServerID(req.getSceneServerID());
        player.getLocation().setSceneWorldIndex(req.getSceneWorldIndex());
        player.info("onGetSceneServerInfo", req.getClientHost(), req.getClientPort(),
                req.getSceneServerID(), req.getSceneID(), req.getSceneWorldIndex(), req.getInstanceID());
        if (req.getIsCreateScene()) {
            //TODO 如果是新创建的场景例如家园或者联盟，需要再发送到场景服数据来填充场景
        }
        player.send(PtCode.S2CPreEnterSceneInfo, builder.build());
    }

    /**
     * 切换场景
     *
     * @param sceneID
     * @param lineID
     */
    public void switchScene(int sceneID, int lineID) {
        if (!checkCanEnterScene(sceneID, lineID)) {
            return;
        }
        player.sendMsg2Scene(PtCode.L2SQuitScene, null);
        enterLinedSceneLocation(sceneID, lineID);
    }
}
