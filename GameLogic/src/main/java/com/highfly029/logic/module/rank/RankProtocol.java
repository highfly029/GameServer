package com.highfly029.logic.module.rank;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbRank;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.logic.module.IClientProtocolRegister;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName RankProtocol
 * @Description RankProtocol
 * @Author liyunpeng
 **/
public class RankProtocol implements IClientProtocolRegister {
    @Override
    public void registerClientProtocol(LogicChildWorld logicChildWorld) throws Exception {
        logicChildWorld.registerClientProtocol(PtCode.C2SGetRankList, this::getRankList);
    }

    /**
     * 获取排行榜
     *
     * @param session
     * @param packet
     */
    public void getRankList(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbRank.C2SGetRankList req = PbRank.C2SGetRankList.parseFrom(packet.getData());
        session.getPlayer().getRankModule().getRankList(req.getArgsList());
    }
}
