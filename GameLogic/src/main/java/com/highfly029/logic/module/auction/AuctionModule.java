package com.highfly029.logic.module.auction;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.auction.AuctionItemData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbAuction;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.template.asset.PlayerAssetConst;
import com.highfly029.common.template.behaviorSource.BehaviorSourceConst;
import com.highfly029.common.template.item.ItemTemplate;
import com.highfly029.common.template.mail.MailTypeConst;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.logic.common.container.item.ItemData;
import com.highfly029.logic.common.container.item.ItemFactory;
import com.highfly029.logic.module.BaseModule;
import com.highfly029.logic.module.bag.PlayerItemContainer;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.logic.utils.PbLogicUtils;

/**
 * @ClassName AuctionModule
 * @Description 拍卖行模块
 * @Author liyunpeng
 **/
public class AuctionModule extends BaseModule {

    @Override
    public void onPlayerInitData() {
        setData(new PlayerAuctionData());
    }

    @Override
    public PlayerAuctionData getData() {
        return (PlayerAuctionData) super.getData();
    }

    /**
     * 拍卖行查询
     *
     * @param auctionType 拍卖类型
     */
    public void auctionSearch(int auctionType) {
        PbAuction.Logic2CenterAuctionSearch.Builder builder = PbAuction.Logic2CenterAuctionSearch.newBuilder();
        builder.setAuctionType(auctionType);
        builder.setPlayerID(player.getPlayerID());
        player.getLogicChildWorld().getAuctionSession().sendMsgS2S(PtCode.Logic2CenterAuctionSearch, builder.build());
    }

    /**
     * 拍卖行上架物品
     *
     * @param index
     * @param itemID
     * @param itemNum
     * @param price
     */
    public void auctionShelfItem(int index, int itemID, int itemNum, int price) {
        PlayerItemContainer playerItemContainer = player.getBagModule().getPlayerBagItemContainer();
        ItemData itemData = playerItemContainer.getItemByIndex(index);
        if (itemData == null) {
            player.warn("auctionShelfItem itemData is null", index, itemID, itemNum);
            return;
        }
        if (itemData.getItemID() != itemID) {
            player.warn("auctionShelfItem invalid itemID", index, itemID, itemNum, itemData.getItemID());
            return;
        }
        if (itemData.getItemNum() < itemNum) {
            player.warn("auctionShelfItem invalid itemNum", index, itemID, itemNum, itemData.getItemNum());
            return;
        }

        long shelfTime = SystemTimeTool.getMillTime();
        if (getData().getPrepareShelfItemMap().contains(shelfTime)) {
            player.warn("auctionShelfItem frequent", index, itemID, itemNum, itemData.getItemNum());
            return;
        }

        int costAssetNum = price / 100;
        if (!player.getAssetModule().hasAsset(PlayerAssetConst.Diamond, costAssetNum)) {
            player.warn("auctionShelfItem not enough asset", index, itemID, itemNum, itemData.getItemNum());
            return;
        }

        player.getAssetModule().removeAsset(PlayerAssetConst.Diamond, costAssetNum, BehaviorSourceConst.Gm);
        playerItemContainer.removeItemByIndex(index, itemNum, BehaviorSourceConst.Gm);

        PbAuction.Logic2CenterAuctionShelfItem.Builder builder = PbAuction.Logic2CenterAuctionShelfItem.newBuilder();
        builder.setPlayerID(player.getPlayerID());
        builder.setAuctionType(itemData.getItemTemplate().getAuctionType());
        AuctionItemData auctionItemData = new AuctionItemData();
        auctionItemData.setItemID(itemID);
        auctionItemData.setItemNum(itemNum);
        auctionItemData.setPrice(price);
        auctionItemData.setShelfTime(shelfTime);
        if (itemData.isHaveExtraData()) {
            PbCommon.PbItemData pbItemData = PbLogicUtils.itemObj2Pb(itemData, 0);
            auctionItemData.setItemData(pbItemData.toByteString());
        }
        builder.setAuctionItemData(PbCommonUtils.auctionItemDataObj2Pb(auctionItemData));

        getData().getPrepareShelfItemMap().put(shelfTime, auctionItemData);
        getData().setChange(true);

        player.getLogicChildWorld().getAuctionSession().sendMsgS2S(PtCode.Logic2CenterAuctionShelfItem, builder.build());
    }

    /**
     * 拍卖行上架结果
     *
     * @param result
     * @param auctionItemData
     */
    public void onAuctionShelfItem(boolean result, AuctionItemData auctionItemData) {
        getData().getPrepareShelfItemMap().remove(auctionItemData.getShelfTime());
        if (result) {
            //上架记录
            getData().getShelfItemMap().put(auctionItemData.getId(), auctionItemData);
            getData().setChange(true);
        } else {
            //TODO rollback item and asset
        }

        PbAuction.L2CAuctionShelfItem.Builder builder = PbAuction.L2CAuctionShelfItem.newBuilder();
        builder.setResult(result);
        builder.setItemID(auctionItemData.getItemID());
        player.send(PtCode.L2CAuctionShelfItem, builder.build());
    }

    /**
     * 拍卖行购买物品
     *
     * @param auctionItemData
     */
    public void auctionBuy(AuctionItemData auctionItemData) {
        if (!player.getAssetModule().hasAsset(PlayerAssetConst.Diamond, auctionItemData.getPrice())) {
            player.warn("auctionBuy not enough asset", auctionItemData);
            return;
        }


        ItemTemplate itemTemplate = ConfigTool.getBagConfig().getItemTemplate(auctionItemData.getItemID());
        if (itemTemplate == null) {
            player.warn("auctionBuy not exist itemTemplate", auctionItemData);
            return;
        }

        player.getAssetModule().removeAsset(PlayerAssetConst.Diamond, auctionItemData.getPrice(), BehaviorSourceConst.Gm);

        PbAuction.Logic2CenterAuctionBuy.Builder builder = PbAuction.Logic2CenterAuctionBuy.newBuilder();
        builder.setAuctionType(itemTemplate.getAuctionType());
        builder.setAuctionItemData(PbCommonUtils.auctionItemDataObj2Pb(auctionItemData));
        builder.setPlayerID(player.getPlayerID());

        player.getLogicChildWorld().getAuctionSession().sendMsgS2S(PtCode.Logic2CenterAuctionBuy, builder.build());
    }

    private ItemData getItemData(AuctionItemData auctionItemData) {
        if (auctionItemData.getItemData() != null && !auctionItemData.getItemData().isEmpty()) {
            try {
                PbCommon.PbItemData pbItemData = PbCommon.PbItemData.parseFrom(auctionItemData.getItemData());
                return PbLogicUtils.itemPb2Obj(pbItemData);
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        } else {
            return ItemFactory.create(auctionItemData.getItemID(), auctionItemData.getItemNum());
        }
        return null;
    }

    /**
     * 收到中心服的拍卖行购买结果
     *
     * @param result
     * @param auctionItemData
     */
    public void onAuctionBuy(boolean result, AuctionItemData auctionItemData) {
        player.info("onAuctionBuy", result, auctionItemData);
        if (result) {
            ItemData itemData = getItemData(auctionItemData);
            player.getMailModule().addMail(MailTypeConst.Normal, 0, 0, BehaviorSourceConst.Gm, itemData);
        } else {
            //TODO rollback asset
        }
    }

    /**
     * 卖家收到中心服的拍卖行购买结果
     *
     * @param buyPlayerID
     * @param auctionItemData
     */
    public void onAuctionBuyToSeller(long buyPlayerID, AuctionItemData auctionItemData) {
        player.info("卖家收到中心服的拍卖行购买结果", buyPlayerID);
        getData().getSellItemMap().put(auctionItemData.getId(), auctionItemData);
        getData().setChange(true);

        //卖家收到钻石
        player.getAssetModule().addAsset(PlayerAssetConst.Diamond, auctionItemData.getPrice(), BehaviorSourceConst.Gm);
    }

    /**
     * 拍卖行取消上架
     *
     * @param auctionType
     * @param auctionID
     */
    public void auctionCancelShelfItem(int auctionType, long auctionID) {
        if (!getData().getShelfItemMap().contains(auctionID)) {
            player.warn("auctionCancelShelfItem not exist id", auctionType, auctionID);
            return;
        }
        PbAuction.Logic2CenterAuctionCancelShelfItem.Builder builder = PbAuction.Logic2CenterAuctionCancelShelfItem.newBuilder();
        builder.setAuctionType(auctionType);
        builder.setId(auctionID);
        builder.setPlayerID(player.getPlayerID());
        player.getLogicChildWorld().getAuctionSession().sendMsgS2S(PtCode.Logic2CenterAuctionCancelShelfItem, builder.build());
    }

    /**
     * 拍卖行取消上架结果
     *
     * @param result
     * @param auctionItemData
     */
    public void onAuctionCancelShelfItem(boolean result, AuctionItemData auctionItemData) {
        player.info("拍卖行取消上架结果", auctionItemData);
        if (result) {
            getData().getShelfItemMap().remove(auctionItemData.getId());
            getData().setChange(true);
            ItemData itemData = getItemData(auctionItemData);
            player.getMailModule().addMail(MailTypeConst.Normal, 0, 0, BehaviorSourceConst.Gm, itemData);
        } else {
            player.error("onAuctionCancelShelfItem fail", auctionItemData.getId());
        }
    }

    /**
     * 拍卖行上架物品到期下架
     *
     * @param auctionItemData
     */
    public void onAuctionExpireCancel(AuctionItemData auctionItemData) {
        player.info("拍卖行上架物品到期下架", auctionItemData);
        getData().getShelfItemMap().remove(auctionItemData.getId());
        getData().setChange(true);
    }
}
