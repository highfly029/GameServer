package com.highfly029.logic.module.scene;

import com.highfly029.common.data.scene.SceneStrategyEnum;
import com.highfly029.common.template.scene.SceneTemplate;
import com.highfly029.common.template.scene.SceneTypeConst;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName SceneConfig
 * @Description SceneConfig
 * @Author liyunpeng
 **/
public class SceneConfig extends BaseGlobalConfig {
    //sceneID -> SceneTemplate
    private final IntObjectMap<SceneTemplate> sceneTemplates = new IntObjectMap<>(SceneTemplate[]::new);


    @Override
    public void check(boolean isHotfix) {

    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(SceneTemplate.class);
        SceneTemplate sceneTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            sceneTemplate = (SceneTemplate) list.get(i);
            sceneTemplates.put(sceneTemplate.getSceneID(), sceneTemplate);
        }
    }

    /**
     * 获取场景模板
     *
     * @param sceneID
     * @return
     */
    public SceneTemplate getSceneTemplate(int sceneID) {
        return sceneTemplates.get(sceneID);
    }

    /**
     * 获取场景id对应的分配策略id
     *
     * @param sceneID
     * @return
     */
    public static int getSceneStrategy(int sceneID) {
        SceneTemplate sceneTemplate = ConfigTool.getSceneConfig().getSceneTemplate(sceneID);
        switch (sceneTemplate.getSceneType()) {
            case SceneTypeConst.CityScene, SceneTypeConst.FieldScene -> {
                return SceneStrategyEnum.LINE.ordinal();
            }
            case SceneTypeConst.HomeScene -> {
                return SceneStrategyEnum.HOME.ordinal();
            }
            case SceneTypeConst.UnionScene -> {
                return SceneStrategyEnum.UNION.ordinal();
            }
            case SceneTypeConst.DungeonScene -> {
                return SceneStrategyEnum.DUNGEON.ordinal();
            }
        }
        return -1;
    }
}
