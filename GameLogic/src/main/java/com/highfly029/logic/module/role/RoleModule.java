package com.highfly029.logic.module.role;

import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.data.playerLiteInfo.PlayerLiteInfoCacheTool;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbRole;
import com.highfly029.common.redis.PlayerLiteInfoKeyEnum;
import com.highfly029.common.redisRank.provider.StandardProvider;
import com.highfly029.common.template.global.GlobalConst;
import com.highfly029.common.template.goal.GoalTypeConst;
import com.highfly029.common.template.playerCondition.PlayerConditionConst;
import com.highfly029.common.template.roleLevel.RoleLevelTemplate;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.logic.module.BaseModule;
import com.highfly029.logic.module.rank.PlayerLevelRank;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.utils.collection.IntIntMap;

/**
 * @ClassName RoleModule
 * @Description 角色模块
 * @Author liyunpeng
 **/
public class RoleModule extends BaseModule {
    /**
     * 所有模块对角色的属性加成
     */
    public IntIntMap attributeMap = new IntIntMap();


    @Override
    public void onPlayerInitData() {
        setData(new PlayerRoleData());
    }

    @Override
    public PlayerRoleData getData() {
        return (PlayerRoleData) super.getData();
    }

    @Override
    public void onPlayerCreateNewRole() {
        //新角色1级
        getData().setLevel(1);
        //新角色设置跨天时间
        getData().setNextDailyTime(SystemTimeTool.getNextDailyTime());
        //创建时间
        getData().setCreateTime(SystemTimeTool.getMillTime());
    }

    @Override
    public void onPlayerAfterLogin() {
        RoleLevelTemplate template = ConfigTool.getRoleConfig().getLevelTemplate(getData().getLevel());
        if (template.getAttributes() != null) {
            for (int[] attr : template.getAttributes()) {
                attributeMap.addValue(attr[0], attr[1]);
            }
        }
    }

    @Override
    public void onPlayerLoginPushBaseClientData() {
        PbRole.S2CLoginPushPlayerRole.Builder builder = PbRole.S2CLoginPushPlayerRole.newBuilder();
        builder.setPlayerRoleData((PbRole.PbPlayerRoleData) getData().save());
        player.send(PtCode.S2CLoginPushPlayerRole, builder.build());
    }

    @Override
    public void onPlayerLogout() {

    }

    /**
     * 获取角色等级
     *
     * @return
     */
    public int getLevel() {
        return getData().getLevel();
    }

    /**
     * 增加等级
     *
     * @param level
     */
    public void addLevel(int level) {

        int oldLevel = getData().getLevel();
        getData().setLevel(level + oldLevel);

        for (int i = oldLevel + 1; i <= getData().getLevel(); i++) {
            onLevelUp(i);
        }
        onLevelChange();
        getData().setChange(true);
        PbRole.S2CAddLevel.Builder builder = PbRole.S2CAddLevel.newBuilder();
        builder.setLevel(getData().getLevel());
        player.send(PtCode.S2CAddLevel, builder.build());
    }

    /**
     * 每次升级都调用
     *
     * @param level 升级后等级
     */
    public void onLevelUp(int level) {
        player.info("onLevelUp", level);
    }

    /**
     * 等级变化 一次性升多级只调用一次
     */
    public void onLevelChange() {
        player.getFunctionModule().checkFunction(PlayerConditionConst.Level);
        player.getGoalModule().triggerGoalEvent(GoalTypeConst.ReachLevel);
        PlayerLiteInfoCacheTool.getInstance().updatePlayerLiteInfo(
                player.getChildWorldIndex(), player.getPlayerID(),
                PlayerLiteInfoKeyEnum.PLAYER_LEVEL, String.valueOf(getLevel()));

        PlayerLevelRank.getInstance().commitScore(player.getChildWorldIndex(), player.getPlayerID(), StandardProvider.of(getLevel()));
    }

    /**
     * 增加经验 满级后0经验
     *
     * @param exp            增加的经验
     * @param behaviorSource
     */
    public void addExp(int exp, int behaviorSource) {
        if (exp <= 0) {
            return;
        }
        //已经满级
        if (getData().getLevel() == GlobalConst.RoleMaxLv) {
            return;
        }
        //实际增加的经验值
        int realAddExp = 0;
        //实际增加的等级
        int realAddLevel = 0;

        RoleConfig roleConfig = ConfigTool.getRoleConfig();
        while (true) {
            int curLevel = getData().getLevel() + realAddLevel;
            //已经满级
            if (curLevel == GlobalConst.RoleMaxLv) {
                break;
            }
            //没有经验可加
            if (exp == 0) {
                break;
            }
            int curNeedMaxExp = roleConfig.getLevelTemplate(curLevel).getMaxExp();
            if (getData().getExp() + exp < curNeedMaxExp) {
                //没升级
                getData().setExp(getData().getExp() + exp);
                exp = 0;
                realAddExp += exp;
                break;
            } else {
                //升级
                int curNeedExp = curNeedMaxExp - getData().getExp();
                exp -= curNeedExp;
                getData().setExp(0);
                realAddExp += curNeedExp;
                realAddLevel++;
            }
        }

        if (realAddLevel > 0) {
            addLevel(realAddLevel);
        }
        int restExp = exp;
        getData().setChange(true);
        player.info("addExp", restExp, realAddExp, realAddLevel, getData().getExp(), getData().getLevel());

        PbRole.S2CAddExp.Builder builder = PbRole.S2CAddExp.newBuilder();
        builder.setExp(getData().getExp());
        player.send(PtCode.S2CAddExp, builder.build());
    }

    /**
     * 增加经验 满级后满经验
     *
     * @param exp
     * @param behaviorSource
     */
    public void addExp2(int exp, int behaviorSource) {

    }

    /**
     * 增加经验 满级后满经验并且存储溢出的经验
     *
     * @param exp
     * @param behaviorSource
     */
    public void addExp3(int exp, int behaviorSource) {

    }

    /**
     * gm重置
     */
    public void resetByGm() {
        getData().setExp(0);
        getData().setLevel(1);
        getData().setChange(true);

        PbRole.S2CAddLevel.Builder builder = PbRole.S2CAddLevel.newBuilder();
        builder.setLevel(getData().getLevel());
        player.send(PtCode.S2CAddLevel, builder.build());

        PbRole.S2CAddExp.Builder builder2 = PbRole.S2CAddExp.newBuilder();
        builder2.setExp(getData().getExp());
        player.send(PtCode.S2CAddExp, builder2.build());
    }

    /**
     * 记录位置信息
     */
    public void recordPosInfo(int preSceneID, int preLineID, Vector3 prePos, Vector3 preDir) {

    }

    @Override
    public void onPlayerMinute() {
        //副本里不存储位置
//        if (player.playerDungeonModule.isInDungeon()) {
//            return;
//        }
    }

    @Override
    public void onPlayerDaily(boolean isLogin) {
        player.info("onPlayerDaily", isLogin, SystemTimeTool.getNextDailyTime());
        //设置下一次跨天时间
        getData().setNextDailyTime(SystemTimeTool.getNextDailyTime());
        getData().setChange(true);
    }
}
