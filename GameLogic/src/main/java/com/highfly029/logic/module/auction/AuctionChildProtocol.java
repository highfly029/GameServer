package com.highfly029.logic.module.auction;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbAuction;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.interfaces.IRegisterServerChildProtocol;
import com.highfly029.core.session.Session;
import com.highfly029.core.world.ChildWorld;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName AuctionChildProtocol
 * @Description AuctionChildProtocol
 * @Author liyunpeng
 **/
public class AuctionChildProtocol implements IRegisterServerChildProtocol {
    private LogicChildWorld logicChildWorld;

    @Override
    public void registerChildProtocol(ChildWorld childWorld) throws Exception {
        this.logicChildWorld = (LogicChildWorld) childWorld;
        childWorld.registerServerChildProtocol(PtCode.Center2LogicAuctionSearch, this::onAuctionSearch);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicAuctionShelfItem, this::onAuctionShelfItem);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicAuctionBuy, this::onAuctionBuy);
    }

    /**
     * 收到中心服的拍卖行查询结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void onAuctionSearch(Session session, int ptCode, GeneratedMessageV3 message) {
        PbAuction.Center2LogicAuctionSearch req = (PbAuction.Center2LogicAuctionSearch) message;
        long playerID = req.getPlayerID();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("onAuctionSearch not exist playerID={}", playerID);
            return;
        }
        PbAuction.L2CAuctionSearch.Builder builder = PbAuction.L2CAuctionSearch.newBuilder();
        builder.setAuctionType(req.getAuctionType());
        req.getAuctionItemListList().forEach(data -> {
            PbAuction.PbAuctionItemData.Builder auctionItemBuilder = PbAuction.PbAuctionItemData.newBuilder();
            auctionItemBuilder.setId(data.getId());
            auctionItemBuilder.setItemID(data.getItemID());
            auctionItemBuilder.setItemNum(data.getItemNum());
            auctionItemBuilder.setPrice(data.getPrice());
            auctionItemBuilder.setShelfTime(data.getShelfTime());
            if (!data.getItemByteData().isEmpty()) {
                try {
                    PbCommon.PbItemData pbItemData = PbCommon.PbItemData.parseFrom(data.getItemByteData());
                    auctionItemBuilder.setItemPbData(pbItemData);
                } catch (InvalidProtocolBufferException e) {
                    player.error("onAuctionSearch", e);
                }
            }
            builder.addAuctionItemList(auctionItemBuilder.build());
        });
        player.send(PtCode.L2CAuctionSearch, builder.build());
    }

    /**
     * 收到中心服的拍卖行上架结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void onAuctionShelfItem(Session session, int ptCode, GeneratedMessageV3 message) {
        PbAuction.Center2LogicAuctionShelfItem req = (PbAuction.Center2LogicAuctionShelfItem) message;
        long playerID = req.getPlayerID();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("onAuctionShelfItem not exist playerID={}", playerID);
            return;
        }
        player.getAuctionModule().onAuctionShelfItem(req.getResult(), PbCommonUtils.auctionItemDataPb2Obj(req.getAuctionItemData()));
    }

    /**
     * 收到中心服的拍卖行购买结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void onAuctionBuy(Session session, int ptCode, GeneratedMessageV3 message) {
        PbAuction.Center2LogicAuctionBuy req = (PbAuction.Center2LogicAuctionBuy) message;
        long playerID = req.getPlayerID();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("onAuctionBuy not exist playerID={}", playerID);
            return;
        }
        player.getAuctionModule().onAuctionBuy(req.getResult(), PbCommonUtils.auctionItemDataPb2Obj(req.getAuctionItemData()));
    }
}
