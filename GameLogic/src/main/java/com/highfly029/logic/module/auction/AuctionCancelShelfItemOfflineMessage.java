package com.highfly029.logic.module.auction;

import com.google.protobuf.ByteString;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbAuction;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.logic.module.IOfflineMessageHandler;
import com.highfly029.logic.module.Player;

/**
 * @ClassName AuctionCancelShelfItemOfflineMessage
 * @Description AuctionCancelShelfItemOfflineMessage
 * @Author liyunpeng
 **/
public class AuctionCancelShelfItemOfflineMessage implements IOfflineMessageHandler {
    @Override
    public int registerPtCode() {
        return PtCode.Center2LogicAuctionCancelShelfItem;
    }

    @Override
    public void handle(Player player, ByteString byteString) throws Exception {
        PbAuction.Center2LogicAuctionCancelShelfItem req = PbAuction.Center2LogicAuctionCancelShelfItem.parseFrom(byteString);
        player.getAuctionModule().onAuctionCancelShelfItem(req.getResult(), PbCommonUtils.auctionItemDataPb2Obj(req.getAuctionItemData()));
    }
}
