package com.highfly029.logic.module;

import com.highfly029.common.data.DataBaseAccess;

/**
 * @ClassName PlayerDataBaseAccess
 * @Description PlayerDataBaseAccess
 * @Author liyunpeng
 **/
public abstract class PlayerDataBaseAccess extends DataBaseAccess {
    private Player player;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
