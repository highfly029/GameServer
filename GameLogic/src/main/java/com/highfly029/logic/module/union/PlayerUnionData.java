package com.highfly029.logic.module.union;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.DataBaseAccess;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.logic.module.PlayerDataBaseAccess;
import com.highfly029.utils.collection.LongLongMap;
import com.highfly029.utils.collection.LongObjectMap;

/**
 * @ClassName PlayerUnionData
 * @Description PlayerUnionData
 * @Author liyunpeng
 **/
public class PlayerUnionData extends PlayerDataBaseAccess {
    /**
     * 联盟ID
     */
    private long unionID;
    /**
     * 被邀请列表
     */
    private final LongObjectMap<String> beInviteJoinMap = new LongObjectMap();
    /**
     * 申请列表
     */
    private final LongObjectMap<String> applyJoinMap = new LongObjectMap<>();

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbUnion.PbPlayerUnionData pbPlayerUnionData = PbUnion.PbPlayerUnionData.parseFrom(data);
        unionID = pbPlayerUnionData.getUnionID();
        int num = pbPlayerUnionData.getBeInviteListCount();
        for (int i = 0; i < num; i++) {
            PbCommon.PbKeyValue keyValue = pbPlayerUnionData.getBeInviteList(i);
            beInviteJoinMap.put(keyValue.getLongKey(), keyValue.getStrValue());
        }
        num = pbPlayerUnionData.getApplyListCount();
        for (int i = 0; i < num; i++) {
            PbCommon.PbKeyValue keyValue = pbPlayerUnionData.getApplyList(i);
            applyJoinMap.put(keyValue.getLongKey(), keyValue.getStrValue());
        }
        return pbPlayerUnionData;
    }

    @Override
    public GeneratedMessageV3 save() {
        PbUnion.PbPlayerUnionData.Builder builder = PbUnion.PbPlayerUnionData.newBuilder();
        builder.setUnionID(unionID);
        beInviteJoinMap.foreachImmutable((k, v) -> {
            PbCommon.PbKeyValue.Builder kvBuilder = PbCommon.PbKeyValue.newBuilder();
            kvBuilder.setLongKey(k);
            kvBuilder.setStrValue(v);
            builder.addBeInviteList(kvBuilder.build());
        });
        applyJoinMap.foreachImmutable((k, v) -> {
            PbCommon.PbKeyValue.Builder kvBuilder = PbCommon.PbKeyValue.newBuilder();
            kvBuilder.setLongKey(k);
            kvBuilder.setStrValue(v);
            builder.addApplyList(kvBuilder.build());
        });
        return builder.build();
    }

    @Override
    public String getName() {
        return "unionData";
    }


    public long getUnionID() {
        return unionID;
    }

    public void setUnionID(long unionID) {
        this.unionID = unionID;
    }

    public void addBeInviteJoinList(long targetUnionID, String unionName) {
        beInviteJoinMap.put(targetUnionID, unionName);
    }

    public void addApplyJoinList(long targetUnionID, String unionName) {
        applyJoinMap.put(targetUnionID, unionName);
    }

    /**
     * 是否申请过targetUnionID
     * @param targetUnionID
     * @return
     */
    public boolean checkApplyJoinList(long targetUnionID) {
        return applyJoinMap.contains(targetUnionID);
    }
}
