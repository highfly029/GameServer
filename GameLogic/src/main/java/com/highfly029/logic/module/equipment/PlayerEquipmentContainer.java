package com.highfly029.logic.module.equipment;

import com.highfly029.common.template.behaviorSource.BehaviorSourceConst;
import com.highfly029.logic.common.container.equipment.EquipmentContainer;
import com.highfly029.logic.common.container.item.EquipmentSlotData;
import com.highfly029.logic.common.container.item.ItemData;
import com.highfly029.logic.common.container.item.extra.EquipmentExtraData;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.tool.ConfigTool;

/**
 * @ClassName PlayerEquipmentContainer
 * @Description 玩家装备容器
 * @Author liyunpeng
 **/
public class PlayerEquipmentContainer extends EquipmentContainer {

    private final Player player;
    /**
     * 玩家装备类型
     */
    private final int equipmentType;

    public PlayerEquipmentContainer(int equipmentType, Player player) {
        this.equipmentType = equipmentType;
        this.player = player;
    }

    public int getEquipmentType() {
        return equipmentType;
    }

    /**
     * 是否可穿上装备
     *
     * @param slot
     * @param equipmentSlotData
     * @param isCanReplace
     * @return
     */
    public boolean checkCanPutOn(int slot, EquipmentSlotData equipmentSlotData, boolean isCanReplace) {
        if (equipmentSlotData == null) {
            player.warn("playerEquipmentContainer checkCanPutOn slot is invalid", slot);
            return false;
        }
        if (!equipmentSlotData.isOpen) {
            player.warn("playerEquipmentContainer checkCanPutOn slot is not open", slot);
            return false;
        }
        //不可替换装备增加判断
        if (!isCanReplace) {
            if (equipmentSlotData.equipment != null) {
                player.warn("playerEquipmentContainer checkCanPutOn slot equipment is not null", slot);
                return false;
            }
        }
        return true;
    }

    /**
     * 是否可脱装备
     *
     * @param slot
     * @return
     */
    public boolean checkCanPutOff(int slot, EquipmentSlotData equipmentSlotData) {
        if (equipmentSlotData == null) {
            player.warn("playerEquipmentContainer checkCanPutOff slot is invalid", slot);
            return false;
        }
        if (!equipmentSlotData.isOpen) {
            player.warn("playerEquipmentContainer checkCanPutOff slot is not open", slot);
            return false;
        }
        if (equipmentSlotData.equipment == null) {
            player.warn("playerEquipmentContainer checkCanPutOff slot equipment is null", slot);
            return false;
        }
        return true;
    }

    @Override
    public void onEquipmentPutOn(int slot, ItemData itemData) {
        super.onEquipmentPutOn(slot, itemData);
        EquipmentExtraData equipmentExtraData = (EquipmentExtraData) itemData.getExtraData();
        int[][] baseActions = equipmentExtraData.getEquipmentTemplate().getBasePlayerActions();
//        if (baseActions != null) {
//            for (int[] action : baseActions) {
//                player.triggerPlayerAction(action, true, BehaviorSourceConst.PutOnEquipment);
//            }
//        }
//        if (itemData.equipmentExtraData.attributes != null) {
//            IntIntMap intIntMap = itemData.equipmentExtraData.attributes;
//            intIntMap.foreachImmutable((attrID, attrValue) -> player.playerEntity.addAttribute((short) attrID, attrValue));
//        }
    }

    @Override
    public void onEquipmentPutOff(int slot, ItemData itemData) {
        super.onEquipmentPutOff(slot, itemData);
        EquipmentExtraData equipmentExtraData = (EquipmentExtraData) itemData.getExtraData();
        int[][] baseActions = equipmentExtraData.getEquipmentTemplate().getBasePlayerActions();
//        if (baseActions != null) {
//            for (int[] action : baseActions) {
//                player.triggerPlayerAction(action, false, BehaviorSourceConst.PutOffEquipment);
//            }
//        }
//        if (itemData.equipmentExtraData.attributes != null) {
//            IntIntMap intIntMap = itemData.equipmentExtraData.attributes;
//            intIntMap.foreachImmutable((attrID, attrValue) -> player.playerEntity.subAttribute((short) attrID, attrValue));
//        }
    }

    @Override
    public void onAddEquipmentSuitNum(int suitID, int newSuitNum) {
        super.onAddEquipmentSuitNum(suitID, newSuitNum);
        int[][] actions = ConfigTool.getEquipConfig().getEquipmentSuitActions(suitID, newSuitNum);
        if (actions != null) {
            for (int[] action : actions) {
                player.triggerPlayerAction(action, true, BehaviorSourceConst.PutOnEquipment);
            }
        }
    }

    @Override
    public void onRemoveEquipmentSuitNum(int suitID, int preSuitNum) {
        super.onRemoveEquipmentSuitNum(suitID, preSuitNum);
        int[][] actions = ConfigTool.getEquipConfig().getEquipmentSuitActions(suitID, preSuitNum);
        if (actions != null) {
            for (int[] action : actions) {
                player.triggerPlayerAction(action, false, BehaviorSourceConst.PutOffEquipment);
            }
        }
    }
}
