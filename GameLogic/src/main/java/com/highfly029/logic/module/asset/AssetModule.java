package com.highfly029.logic.module.asset;

import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbAsset;
import com.highfly029.common.template.asset.PlayerAssetConst;
import com.highfly029.logic.module.BaseModule;
import com.highfly029.logic.tool.ConfigTool;

/**
 * @ClassName AssetModule
 * @Description 玩家资产模块
 * @Author liyunpeng
 **/
public class AssetModule extends BaseModule {
    @Override
    public void onPlayerInitData() {
        setData(new PlayerAssetData(PlayerAssetConst.count));
    }

    @Override
    public PlayerAssetData getData() {
        return (PlayerAssetData) super.getData();
    }

    @Override
    public void onPlayerCreateNewRole() {
        //新号初始化资产数据
    }

    @Override
    public void onPlayerBeforeLogin() {
        player.info("onPlayerBeforeLogin curList,  maxList", getData().getCurAssets(), getData().getMaxAssets());
    }

    @Override
    public void onPlayerLoginPushBaseClientData() {
        PbAsset.S2CLoginPushPlayerAllAsset.Builder builder = PbAsset.S2CLoginPushPlayerAllAsset.newBuilder();
        builder.setPlayerAssetData((PbAsset.PbPlayerAssetData) getData().save());
        player.send(PtCode.S2CLoginPushPlayerAllAsset, builder.build());
    }

    /**
     * 增加资产上限
     *
     * @param type
     * @param addMaxValue 增加的最大值
     */
    public void addAssetMax(int type, long addMaxValue) {
        if (addMaxValue > 0) {
            getData().getMaxAssets()[type] += addMaxValue;
            getData().setChange(true);
            PbAsset.S2CAddMaxAsset.Builder builder = PbAsset.S2CAddMaxAsset.newBuilder();
            builder.setType(type);
            builder.setAddMaxValue(addMaxValue);
            player.send(PtCode.S2CAddMaxAsset, builder.build());
        }
    }

    /**
     * 减少资产上限
     *
     * @param type
     * @param removeMaxValue 减少的最大值
     */
    public void removeAssetMax(int type, long removeMaxValue) {
        if (removeMaxValue > 0 && getData().getMaxAssets()[type] >= removeMaxValue) {
            getData().getMaxAssets()[type] -= removeMaxValue;
            getData().setChange(true);
            PbAsset.S2CRemoveMaxAsset.Builder builder = PbAsset.S2CRemoveMaxAsset.newBuilder();
            builder.setType(type);
            builder.setRemoveMaxValue(removeMaxValue);
            player.send(PtCode.S2CRemoveMaxAsset, builder.build());
        }
    }

    /**
     * 是否达到资产上限
     *
     * @param type
     * @return
     */
    public boolean isAssetMax(int type) {
        long maxValue;
        return (maxValue = getData().getMaxAssets()[type]) > 0 && getData().getCurAssets()[type] >= maxValue;
    }

    /**
     * 添加资产
     *
     * @param type
     * @param value
     * @param behaviorSource
     */
    public void addAsset(int type, long value, int behaviorSource) {
        if (value <= 0) {
            return;
        }
        long[] curAssetArray = getData().getCurAssets();
        long preValue = curAssetArray[type];
        if (!ConfigTool.getAssetConfig().getTemplate(type).isCanExceedMax()) {
            long maxValue;
            if ((maxValue = getData().getMaxAssets()[type]) > 0) {
                //已经达到上限
                if (preValue >= maxValue) {
                    return;
                }

                if (value + preValue > maxValue) {
                    value = maxValue - preValue;
                }
            }
        }
        curAssetArray[type] = preValue + value;
        getData().setChange(true);

        long addCurValue = value;
        PbAsset.S2CAddCurAsset.Builder builder = PbAsset.S2CAddCurAsset.newBuilder();
        builder.setType(type);
        builder.setAddCurValue(addCurValue);
        player.send(PtCode.S2CAddCurAsset, builder.build());
        onAddAsset(type, preValue, curAssetArray[type], behaviorSource);
    }

    /**
     * 添加资产、不受上限限制
     *
     * @param type
     * @param value
     * @param behaviorSource
     */
    public void addAssetNoLimit(int type, long value, int behaviorSource) {
        if (value <= 0) {
            return;
        }
        long[] curAssetArray = getData().getCurAssets();
        curAssetArray[type] += value;
        getData().setChange(true);
        PbAsset.S2CAddCurAsset.Builder builder = PbAsset.S2CAddCurAsset.newBuilder();
        builder.setType(type);
        builder.setAddCurValue(value);
        player.send(PtCode.S2CAddCurAsset, builder.build());
        onAddAsset(type, value, curAssetArray[type], behaviorSource);
    }

    /**
     * 移除资产
     *
     * @param type
     * @param value
     * @param behaviorSource
     */
    public void removeAsset(int type, long value, int behaviorSource) {
        if (value <= 0) {
            return;
        }
        long[] curAssetArray = getData().getCurAssets();
        long preValue = curAssetArray[type];
        if (preValue == 0) {
            return;
        }
        if (preValue >= value) {
            curAssetArray[type] -= value;
        } else {
            curAssetArray[type] = 0;
        }
        getData().setChange(true);
        long curValue = curAssetArray[type];
        long removeCurValue = preValue - curValue;
        PbAsset.S2CRemoveCurAsset.Builder builder = PbAsset.S2CRemoveCurAsset.newBuilder();
        builder.setType(type);
        builder.setRemoveCurValue(removeCurValue);
        player.send(PtCode.S2CRemoveCurAsset, builder.build());
        onRemoveAsset(type, preValue, curValue, behaviorSource);
    }

    /**
     * 获取资产
     *
     * @param type
     * @return
     */
    public long getAsset(int type) {
        return getData().getCurAssets()[type];
    }

    /**
     * 是否拥有资产
     *
     * @param type
     * @param value
     * @return
     */
    public boolean hasAsset(int type, long value) {
        if (value < 0) {
            return false;
        }
        return getData().getCurAssets()[type] >= value;
    }

    /**
     * 增加资产通知
     *
     * @param type
     * @param preValue
     * @param newValue
     * @param behaviorSource
     */
    public void onAddAsset(int type, long preValue, long newValue, int behaviorSource) {
        player.info("onAddAsset type, preValue, newValue", type, preValue, newValue, behaviorSource);
    }

    /**
     * 删除资产通知
     *
     * @param type
     * @param preValue
     * @param newValue
     * @param behaviorSource
     */
    public void onRemoveAsset(int type, long preValue, long newValue, int behaviorSource) {
        player.info("onRemoveAsset type, preValue, newValue", type, preValue, newValue, behaviorSource);
    }

}
