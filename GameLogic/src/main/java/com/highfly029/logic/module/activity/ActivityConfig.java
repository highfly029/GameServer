package com.highfly029.logic.module.activity;

import com.highfly029.common.data.activity.ActivityTimeData;
import com.highfly029.common.template.activity.ActivityDriveTypeConst;
import com.highfly029.common.template.activity.ActivityRelativeTimeConst;
import com.highfly029.common.template.activity.ActivityTemplate;
import com.highfly029.common.template.activity.ActivityTimeTypeConst;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.logic.exception.StartUpException;
import com.highfly029.utils.StringUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName ActivityConfig
 * @Description ActivityConfig
 * @Author liyunpeng
 **/
public class ActivityConfig extends BaseGlobalConfig {
    private final IntObjectMap<ActivityTemplate> activityTemplateMap = new IntObjectMap<>(ActivityTemplate[]::new);

    private final IntObjectMap<ActivityTimeData> playerActivityTemplateDataMap = new IntObjectMap<>(ActivityTimeData[]::new);

    private final IntObjectMap<ActivityTimeData> logicActivityTemplateDataMap = new IntObjectMap<>(ActivityTimeData[]::new);

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(ActivityTemplate.class);
        ActivityTemplate activityTemplate;
        for (int i = 0, size = list.size(); i < size; i++) {
            activityTemplate = (ActivityTemplate) list.get(i);
            activityTemplateMap.put(activityTemplate.getId(), activityTemplate);

            ActivityTimeData activityTimeData = new ActivityTimeData(activityTemplate);
            if (activityTemplate.getActivityDriveType() == ActivityDriveTypeConst.Player) {
                playerActivityTemplateDataMap.put(activityTemplate.getId(), activityTimeData);
            } else if (activityTemplate.getActivityDriveType() == ActivityDriveTypeConst.Logic) {
                logicActivityTemplateDataMap.put(activityTemplate.getId(), activityTimeData);
            }
        }
    }

    /**
     * 获取活动模板类
     *
     * @param id
     * @return
     */
    public ActivityTemplate getTemplate(int id) {
        return activityTemplateMap.get(id);
    }

    public IntObjectMap<ActivityTemplate> getActivityTemplateMap() {
        return activityTemplateMap;
    }

    public IntObjectMap<ActivityTimeData> getPlayerActivityTemplateDataMap() {
        return playerActivityTemplateDataMap;
    }

    public IntObjectMap<ActivityTimeData> getLogicActivityTemplateDataMap() {
        return logicActivityTemplateDataMap;
    }

    @Override
    public void check(boolean isHotfix) {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(ActivityTemplate.class);
        for (int i = 0; i < list.size(); i++) {
            ActivityTemplate activityTemplate = (ActivityTemplate) list.get(i);
            if (activityTemplate.getActivityDriveType() == ActivityDriveTypeConst.Center && activityTemplate.getActivityTimeType() == ActivityTimeTypeConst.RelativeTime) {
                throw new StartUpException("中心服不应该有相对时间的活动 " + activityTemplate.getId());
            }
            if (activityTemplate.getActivityDriveType() == ActivityDriveTypeConst.Player && activityTemplate.getActivityTimeType() == ActivityTimeTypeConst.CycleTime) {
                //player驱动的不应该存在循环活动，因为开启时间和结束时间没有落地,应该改为logic驱动循环活动
                throw new StartUpException("player驱动的不应该存在循环活动 " + activityTemplate.getId());
            }
            if (activityTemplate.getActivityTimeType() == ActivityTimeTypeConst.RelativeTime) {
                int[] array = StringUtils.toIntArray(activityTemplate.getStartTimeStr(), ":");
                if (array[0] == ActivityRelativeTimeConst.PlayerRegisterTime && activityTemplate.getActivityDriveType() != ActivityDriveTypeConst.Player) {
                    throw new StartUpException("注册后活动必须player驱动 " + activityTemplate.getId());
                }
                if (array[0] == ActivityRelativeTimeConst.LogicOpenServerTime && activityTemplate.getActivityDriveType() != ActivityDriveTypeConst.Logic) {
                    throw new StartUpException("开服后活动必须logic驱动 " + activityTemplate.getId());
                }
            }
        }
    }
}
