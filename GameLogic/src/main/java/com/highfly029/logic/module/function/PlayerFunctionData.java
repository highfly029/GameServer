package com.highfly029.logic.module.function;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.DataBaseAccess;
import com.highfly029.common.protocol.packet.PbFunction;
import com.highfly029.logic.module.PlayerDataBaseAccess;
import com.highfly029.utils.collection.IntBooleanMap;

/**
 * @ClassName PlayerFunctionData
 * @Description 功能开启数据
 * @Author liyunpeng
 **/
public class PlayerFunctionData extends PlayerDataBaseAccess {
    private IntBooleanMap openFunctions = new IntBooleanMap();

    public IntBooleanMap getOpenFunctions() {
        return openFunctions;
    }

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbFunction.PbPlayerFunctionData pbPlayerFunctionData = PbFunction.PbPlayerFunctionData.parseFrom(data);
        for (int i = 0, size = pbPlayerFunctionData.getOpenFunctionsCount(); i < size; i++) {
            int functionID = pbPlayerFunctionData.getOpenFunctions(i);
            openFunctions.put(functionID, true);
        }
        return pbPlayerFunctionData;
    }

    @Override
    public GeneratedMessageV3 save() {
        PbFunction.PbPlayerFunctionData.Builder builder = PbFunction.PbPlayerFunctionData.newBuilder();
        int freeValue = openFunctions.getFreeValue();
        int[] keys = openFunctions.getKeys();
        boolean[] values = openFunctions.getValues();
        int key;
        boolean value;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                value = values[i];
                if (value) {
                    builder.addOpenFunctions(key);
                }
            }
        }
        return builder.build();
    }

    @Override
    public String getName() {
        return "function";
    }
}
