package com.highfly029.logic.module.union;

import com.google.protobuf.ByteString;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.logic.module.IOfflineMessageHandler;
import com.highfly029.logic.module.Player;

/**
 * @ClassName UnionDoubleCheckOfflineMsgParser
 * @Description UnionDoubleCheckOfflineMsgParser
 * @Author liyunpeng
 **/
public class UnionDoubleCheckOfflineMsgParser implements IOfflineMessageHandler {
    @Override
    public int registerPtCode() {
        return PtCode.Center2LogicJoinUnionDoubleCheck;
    }

    @Override
    public void handle(Player player, ByteString byteString) throws Exception {
        PbUnion.Center2LogicJoinUnionDoubleCheck req = PbUnion.Center2LogicJoinUnionDoubleCheck.parseFrom(byteString.toByteArray());
        player.getUnionModule().onJoinUnionDoubleCheck(req.getOperatorPlayerID(), req.getUnionID(), req.getType());
    }
}
