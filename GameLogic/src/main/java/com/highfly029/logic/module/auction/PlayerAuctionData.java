package com.highfly029.logic.module.auction;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.DataBaseAccess;
import com.highfly029.common.data.auction.AuctionItemData;
import com.highfly029.common.protocol.packet.PbAuction;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.logic.module.PlayerDataBaseAccess;
import com.highfly029.utils.collection.LongObjectMap;

/**
 * @ClassName PlayerAuctionData
 * @Description PlayerAuctionData
 * @Author liyunpeng
 **/
public class PlayerAuctionData extends PlayerDataBaseAccess {
    /**
     * 准备上架物品数据 key=shelfTime
     */
    private final LongObjectMap<AuctionItemData> prepareShelfItemMap = new LongObjectMap<>(AuctionItemData[]::new);
    /**
     * 已经上架物品数据 key=id
     */
    private final LongObjectMap<AuctionItemData> shelfItemMap = new LongObjectMap<>(AuctionItemData[]::new);
    /**
     * 已经卖出物品数据 key=id
     */
    private final LongObjectMap<AuctionItemData> sellItemMap = new LongObjectMap<>(AuctionItemData[]::new);

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbAuction.PbPlayerAuctionData pbPlayerAuctionData = PbAuction.PbPlayerAuctionData.parseFrom(data);
        if (pbPlayerAuctionData.getPrepareShelfItemsCount() > 0) {
            for (PbAuction.PbAuctionItemData pbAuctionItemData : pbPlayerAuctionData.getPrepareShelfItemsList()) {
                AuctionItemData auctionItemData = PbCommonUtils.auctionItemDataPb2Obj(pbAuctionItemData);
                prepareShelfItemMap.put(auctionItemData.getShelfTime(), auctionItemData);
            }
        }
        if (pbPlayerAuctionData.getShelfItemsCount() > 0) {
            for (PbAuction.PbAuctionItemData pbAuctionItemData : pbPlayerAuctionData.getShelfItemsList()) {
                AuctionItemData auctionItemData = PbCommonUtils.auctionItemDataPb2Obj(pbAuctionItemData);
                shelfItemMap.put(auctionItemData.getId(), auctionItemData);
            }
        }
        if (pbPlayerAuctionData.getSellItemsCount() > 0) {
            for (PbAuction.PbAuctionItemData pbAuctionItemData : pbPlayerAuctionData.getSellItemsList()) {
                AuctionItemData auctionItemData = PbCommonUtils.auctionItemDataPb2Obj(pbAuctionItemData);
                sellItemMap.put(auctionItemData.getId(), auctionItemData);
            }
        }
        return pbPlayerAuctionData;
    }

    @Override
    public GeneratedMessageV3 save() {
        PbAuction.PbPlayerAuctionData.Builder builder = PbAuction.PbPlayerAuctionData.newBuilder();
        prepareShelfItemMap.foreachImmutable((k, v) -> {
            PbAuction.PbAuctionItemData pbAuctionItemData = PbCommonUtils.auctionItemDataObj2Pb(v);
            builder.addPrepareShelfItems(pbAuctionItemData);
        });
        shelfItemMap.foreachImmutable((k, v) -> {
            PbAuction.PbAuctionItemData pbAuctionItemData = PbCommonUtils.auctionItemDataObj2Pb(v);
            builder.addShelfItems(pbAuctionItemData);
        });
        sellItemMap.foreachImmutable((k, v) -> {
            PbAuction.PbAuctionItemData pbAuctionItemData = PbCommonUtils.auctionItemDataObj2Pb(v);
            builder.addSellItems(pbAuctionItemData);
        });
        return builder.build();
    }

    public LongObjectMap<AuctionItemData> getPrepareShelfItemMap() {
        return prepareShelfItemMap;
    }

    public LongObjectMap<AuctionItemData> getShelfItemMap() {
        return shelfItemMap;
    }

    public LongObjectMap<AuctionItemData> getSellItemMap() {
        return sellItemMap;
    }

    @Override
    public String getName() {
        return "auction";
    }
}
