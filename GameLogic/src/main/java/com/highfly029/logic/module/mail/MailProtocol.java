package com.highfly029.logic.module.mail;

import java.util.List;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbMail;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.logic.module.IClientProtocolRegister;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.utils.PbLogicUtils;
import com.highfly029.logic.world.LogicChildWorld;
import com.highfly029.utils.collection.LongList;

/**
 * @ClassName MailProtocol
 * @Description MailProtocol
 * @Author liyunpeng
 **/
public class MailProtocol implements IClientProtocolRegister {
    private LogicChildWorld logicChildWorld;

    @Override
    public void registerClientProtocol(LogicChildWorld logicChildWorld) throws Exception {
        this.logicChildWorld = logicChildWorld;

        logicChildWorld.registerClientProtocol(PtCode.C2SGetMailList, this::getMailList);
        logicChildWorld.registerClientProtocol(PtCode.C2SReadMail, this::readMail);
        logicChildWorld.registerClientProtocol(PtCode.C2SReceiveMailAttachment, this::receiveMailAttachment);
        logicChildWorld.registerClientProtocol(PtCode.C2SDeleteMail, this::deleteMail);
    }

    /**
     * 获取邮件列表
     *
     * @param session
     * @param packet
     */
    public void getMailList(PlayerSession session, PbPacket.TcpPacketClient packet) {
        session.getPlayer().getMailModule().getMailList(true, map -> {
            PbMail.S2CReturnMailList.Builder builder = PbMail.S2CReturnMailList.newBuilder();
            map.foreachImmutable((mailID, mailData) -> {
                PbMail.PbMailData pbMailData = PbLogicUtils.mailObj2Pb(mailData);
                builder.addMailList(pbMailData);
            });

            session.send(PtCode.S2CReturnMailList, builder.build());
        });
    }

    /**
     * 读取邮件
     *
     * @param session
     * @param packet
     */
    public void readMail(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbMail.C2SReadMail req = PbMail.C2SReadMail.parseFrom(packet.getData());
        List<Long> list = req.getMailIDList();
        session.getPlayer().getMailModule().readMail(list);
    }

    /**
     * 领取邮件附件
     *
     * @param session
     * @param packet
     */
    public void receiveMailAttachment(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbMail.C2SReceiveMailAttachment req = PbMail.C2SReceiveMailAttachment.parseFrom(packet.getData());
        List<Long> list = req.getMailIDList();
        session.getPlayer().getMailModule().receiveMailAttachment(list);
    }

    /**
     * 删除邮件
     *
     * @param session
     * @param packet
     */
    public void deleteMail(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbMail.C2SDeleteMail req = PbMail.C2SDeleteMail.parseFrom(packet.getData());
        List<Long> list = req.getMailIDList();
        session.getPlayer().getMailModule().deleteMail(list);
    }
}
