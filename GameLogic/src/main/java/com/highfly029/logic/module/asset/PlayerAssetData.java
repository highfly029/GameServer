package com.highfly029.logic.module.asset;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.DataBaseAccess;
import com.highfly029.common.protocol.packet.PbAsset;
import com.highfly029.logic.module.PlayerDataBaseAccess;

/**
 * @ClassName AssetData
 * @Description 资产数据
 * @Author liyunpeng
 **/
public class PlayerAssetData extends PlayerDataBaseAccess {
    private long[] curAssets;
    private long[] maxAssets;

    public PlayerAssetData(int size) {
        curAssets = new long[size];
        maxAssets = new long[size];
    }

    public long[] getCurAssets() {
        return curAssets;
    }

    public long[] getMaxAssets() {
        return maxAssets;
    }

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbAsset.PbPlayerAssetData pbPlayerAssetData = PbAsset.PbPlayerAssetData.parseFrom(data);
        if (pbPlayerAssetData.getCurAssetListCount() > 0) {
            for (int i = 0; i < pbPlayerAssetData.getCurAssetListCount(); i++) {
                long value = pbPlayerAssetData.getCurAssetList(i);
                curAssets[i] = value;
            }
        }
        if (pbPlayerAssetData.getMaxAssetListCount() > 0) {
            for (int i = 0; i < pbPlayerAssetData.getMaxAssetListCount(); i++) {
                long value = pbPlayerAssetData.getMaxAssetList(i);
                maxAssets[i] = value;
            }
        }
        return pbPlayerAssetData;
    }

    @Override
    public GeneratedMessageV3 save() {
        PbAsset.PbPlayerAssetData.Builder builder = PbAsset.PbPlayerAssetData.newBuilder();
        for (int i = 0; i < curAssets.length; i++) {
            long value = curAssets[i];
            builder.addCurAssetList(value);
        }
        for (int i = 0; i < maxAssets.length; i++) {
            long value = maxAssets[i];
            builder.addMaxAssetList(value);
        }
        return builder.build();
    }

    @Override
    public String getName() {
        return "asset";
    }
}
