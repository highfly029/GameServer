package com.highfly029.logic.module.goal;

import com.highfly029.common.template.goal.GoalTemplate;
import com.highfly029.common.template.goal.GoalTypeConst;
import com.highfly029.logic.tool.ConfigTool;

/**
 * @ClassName GoalData
 * @Description 目标数据
 * @Author liyunpeng
 **/
public class GoalData {
    /**
     * 实例id
     */
    private int instanceID;
    /**
     * 目标拥有者类型
     */
    private byte goalMasterType;
    /**
     * 目标主人ID
     */
    private int goalMasterID;
    /**
     * 目标id
     */
    private int goalID;
    /**
     * 目标进度
     */
    private int progress;
    /**
     * 模板
     */
    private GoalTemplate goalTemplate;

    public int getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(int instanceID) {
        this.instanceID = instanceID;
    }

    public byte getGoalMasterType() {
        return goalMasterType;
    }

    public void setGoalMasterType(byte goalMasterType) {
        this.goalMasterType = goalMasterType;
    }

    public int getGoalMasterID() {
        return goalMasterID;
    }

    public void setGoalMasterID(int goalMasterID) {
        this.goalMasterID = goalMasterID;
    }

    public int getGoalID() {
        return goalID;
    }

    public void setGoalID(int goalID) {
        this.goalID = goalID;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public GoalTemplate getGoalTemplate() {
        return goalTemplate;
    }

    public static GoalData createByPb(int goalID, int progress) {
        GoalData goalData = new GoalData();
        goalData.goalID = goalID;
        goalData.goalTemplate = ConfigTool.getGoalConfig().getGoalTemplate(goalID);
        goalData.progress = progress;
        return goalData;
    }

    public static GoalData create(int goalID) {
        GoalData goalData = new GoalData();
        goalData.goalID = goalID;
        goalData.goalTemplate = ConfigTool.getGoalConfig().getGoalTemplate(goalID);
        goalData.progress = goalData.goalTemplate.getInitNum();
        return goalData;
    }

    /**
     * 初始化时不需要触发激活、只需要刷新就可能完成的目标
     *
     * @return
     */
    public boolean isInitCanRefresh() {
        switch (goalTemplate.getGoalType()) {
            case GoalTypeConst.ReachLevel, GoalTypeConst.OwnItem -> {
                return true;
            }
        }
        return false;
    }

    /**
     * 是否完成
     *
     * @return
     */
    public boolean isComplete() {
        return progress >= goalTemplate.getCompleteNum();
    }


}
