package com.highfly029.logic.module.common;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.world.LogicGlobalWorld;

/**
 * @ClassName CommonGlobalProtocol
 * @Description CommonGlobalProtocol
 * @Author liyunpeng
 **/
public class CommonGlobalProtocol implements IRegisterServerGlobalProtocol {
    private LogicGlobalWorld logicGlobalWorld;
    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {
        logicGlobalWorld = (LogicGlobalWorld) globalWorld;
        globalWorld.registerServerGlobalProtocol(PtCode.Center2SceneInfoCode, this::infoCodeResult);
    }

    /**
     * 收到中心服的消息码
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void infoCodeResult(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbCommon.Center2SceneInfoCode req = PbCommon.Center2SceneInfoCode.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        int infoCode = req.getInfoCode();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("infoCodeResult not exist playerID={}, inCode={}", playerID, infoCode);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }
}
