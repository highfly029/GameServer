package com.highfly029.logic.module.bag;

import com.highfly029.logic.common.container.item.ItemData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbBag;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.template.bag.PlayerBagTypeConst;
import com.highfly029.common.template.goal.GoalTypeConst;
import com.highfly029.logic.common.container.item.ItemContainer;
import com.highfly029.logic.exception.BaseException;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.utils.PbLogicUtils;
import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName PlayerItemContainer
 * @Description 玩家物品容器
 * @Author liyunpeng
 **/
public class PlayerItemContainer extends ItemContainer {
    private Player player;
    /**
     * 背包类型
     */
    private int bagType;

    public PlayerItemContainer(int gridNum, int bagType, Player player) {
        super(gridNum);
        this.bagType = bagType;
        this.player = player;
    }

    public int getBagType() {
        return bagType;
    }

    @Override
    protected void printErrorLog(int type, int index, int itemID, int itemNum) {
        if (type == ERROR_LOG_TYPE_INVALID_INDEX) {
            player.error("itemContainer putItemToNewArrayIndex invalid index", index);
        } else if (type == ERROR_LOG_TYPE_PUT_ITEM_TO_NOT_NULL_INDEX) {
            player.error("itemContainer putItemToNewArrayIndex has not null itemData index=" + index, new BaseException("item"));
        } else if (type == ERROR_LOG_TYPE_NOT_EXIST_TEMPLATE) {
            player.error("itemContainer isCanAddItemSuccess not exist itemTemplate itemID={}, itemNum={}", itemID, itemNum);
        }
    }

    @Override
    protected boolean isCanUseItem(ItemData itemData) {
        //判断物品使用等级
        return player.getRoleModule().getLevel() >= itemData.getItemTemplate().getItemUseLevel();
    }

    @Override
    protected void generateCreateID(ItemData itemData) {
        if (itemData.getItemTemplate().isCanTrade() && itemData.getCreateID() == 0) {
            itemData.setCreateID(player.getPlayerID());
        }
    }

    @Override
    protected void onDropItemToScene(ItemData itemData, int dropNum) {
//        PlayerEntity playerEntity = player.playerEntity;
//        if (playerEntity == null) {
//            return;
//        }
//        DropEntity dropEntity = (DropEntity) playerEntity.scene.createEntity(EntityTypeConst.DROP, playerEntity.curPos, playerEntity.direction, false);
//        dropEntity.initWithItemData(itemData, dropNum, SystemTimeTool.getMillTime() + 60 * 1000L);
//        playerEntity.scene.addEntity(dropEntity);
    }

    @Override
    protected void onSendMessageAddItems(IntIntMap addItems, IntObjectMap<ItemData> newItems, int behaviorSource) {
        player.info("send message addItems, newItems, behaviorSource", addItems, newItems, behaviorSource);
        PbBag.S2CAddItems.Builder builder = PbBag.S2CAddItems.newBuilder();
        if (addItems.size() > 0) {
            addItems.foreachImmutable((k, v) -> {
                PbCommon.PbKeyValue.Builder pbKeyValueBuilder = PbCommon.PbKeyValue.newBuilder();
                pbKeyValueBuilder.setIntKey(k).setIntValue(v);
                builder.addAddItems(pbKeyValueBuilder.build());
            });
        }
        if (newItems.size() > 0) {
            newItems.foreachImmutable((k, v) -> {
                PbCommon.PbItemData pbItemData = PbLogicUtils.itemObj2Pb(v, k);
                builder.addNewItems(pbItemData);
            });
        }
        builder.setBagType(bagType).setBehaviorSource(behaviorSource);
        player.send(PtCode.S2CAddItems, builder.build());
        player.getBagModule().getData().setChange(true);
    }

    @Override
    protected void onSendMessageRemoveItems(IntIntMap removeItems, int behaviorSource) {
        player.info("send message removeItems, behaviorSource", removeItems, behaviorSource);
        PbBag.S2CRemoveItems.Builder builder = PbBag.S2CRemoveItems.newBuilder();
        if (removeItems.size() > 0) {
            removeItems.foreachImmutable((k, v) -> {
                PbCommon.PbKeyValue.Builder pbKeyValueBuilder = PbCommon.PbKeyValue.newBuilder();
                pbKeyValueBuilder.setIntKey(k).setIntValue(v);
                builder.addRemoveItems(pbKeyValueBuilder.build());
            });
        }
        builder.setBagType(bagType).setBehaviorSource(behaviorSource);
        player.send(PtCode.S2CRemoveItems, builder.build());
        player.getBagModule().getData().setChange(true);
    }

    @Override
    protected void onSendMessageRemoveItemByIndex(int index, int num, int behaviorSource) {
        player.info("send message removeItemByIndexAbsolutely", index, num, behaviorSource);
        PbBag.S2CRemoveItemByIndex.Builder builder = PbBag.S2CRemoveItemByIndex.newBuilder();
        builder.setBagType(bagType).setIndex(index).setNum(num).setBehaviorSource(behaviorSource);
        player.send(PtCode.S2CRemoveItemByIndex, builder.build());
        player.getBagModule().getData().setChange(true);
    }

    @Override
    protected void onSendMessageResetGridNum(int gridNum) {
        player.info("send message resetGridNum", gridNum);
        PbBag.S2CResetGridNum.Builder builder = PbBag.S2CResetGridNum.newBuilder();
        builder.setBagType(bagType).setNewGridNum(gridNum);
        player.send(PtCode.S2CResetGridNum, builder.build());
        player.getBagModule().getData().setChange(true);
    }

    @Override
    protected void onSendMessageSort(ItemData[] itemArray) {
        PbBag.S2CSort.Builder builder = PbBag.S2CSort.newBuilder();
        ItemData itemData;
        for (int i = 0, len = itemArray.length; i < len; i++) {
            if ((itemData = itemArray[i]) != null) {
                PbCommon.PbItemData pbItemData = PbLogicUtils.itemObj2Pb(itemData, i);
                builder.addItems(pbItemData);
            }
        }
        builder.setBagType(bagType);
        player.send(PtCode.S2CSort, builder.build());
        player.getBagModule().getData().setChange(true);
    }

    @Override
    protected void onSendMessageSplitItem(int index, int num, int newIndex, ItemData newItemData) {
        player.info("send message splitItem", index, num, newIndex, newItemData);
        PbBag.S2CSplitItem.Builder builder = PbBag.S2CSplitItem.newBuilder();
        PbCommon.PbItemData pbItemData = PbLogicUtils.itemObj2Pb(newItemData, newIndex);
        builder.setBagType(bagType).setIndex(index).setNum(num).setNewItemData(pbItemData);
        player.send(PtCode.S2CSplitItem, builder.build());
        player.getBagModule().getData().setChange(true);
    }

    @Override
    protected void onSendMessageSwapItemByIndex(int fromIndex, int toIndex) {
        player.info("send message swapItemByIndex fromIndex, toIndex", fromIndex, toIndex);
        PbBag.S2CSwapItemByIndex.Builder builder = PbBag.S2CSwapItemByIndex.newBuilder();
        builder.setBagType(bagType).setFromIndex(fromIndex).setToIndex(toIndex);
        player.send(PtCode.S2CSwapItemByIndex, builder.build());
        player.getBagModule().getData().setChange(true);
    }

    @Override
    protected void onItemAdd(int itemID, int addItemNum, int behaviorSource) {
        player.info("onItemAdd itemID, addItemNum, behaviorSource", itemID, addItemNum, behaviorSource);

        if (bagType == PlayerBagTypeConst.Bag) {
            player.getGoalModule().triggerGoalEvent(GoalTypeConst.GetNewItem, itemID, addItemNum);
            player.getGoalModule().triggerGoalEvent(GoalTypeConst.OwnItem, itemID, addItemNum);
        }
    }

    @Override
    protected void onItemRemove(int itemID, int removeItemNum, int behaviorSource) {
        player.info("onItemRemove itemID, removeItemNum, behaviorSource", itemID, removeItemNum, behaviorSource);
    }

    @Override
    protected void onItemUse(ItemData itemData, int useNum, int behaviorSource) {
        player.info("onItemUse itemID, useNum, behaviorSource", itemData.getItemID(), useNum, behaviorSource);
    }
}
