package com.highfly029.logic.module.equipment;

import com.highfly029.logic.common.container.item.ItemData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbEquipment;
import com.highfly029.common.template.behaviorSource.BehaviorSourceConst;
import com.highfly029.common.template.equipment.EquipmentTypeConst;
import com.highfly029.common.template.item.ItemTypeConst;
import com.highfly029.logic.common.container.item.extra.EquipmentExtraData;
import com.highfly029.logic.module.BaseModule;
import com.highfly029.logic.module.bag.PlayerItemContainer;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.logic.utils.PbLogicUtils;

/**
 * @ClassName EquipmentModule
 * @Description EquipmentModule
 * @Author liyunpeng
 **/
public class EquipmentModule extends BaseModule {

    @Override
    public void onPlayerInitData() {
        setData(new PlayerEquipmentData(player));
    }

    @Override
    public PlayerEquipmentData getData() {
        return (PlayerEquipmentData) super.getData();
    }

    @Override
    public void onPlayerCreateNewRole() {
        EquipConfig equipConfig = ConfigTool.getEquipConfig();
        //初始化装备槽
        PlayerEquipmentContainer normalEquipmentContainer = getData().registerEquipContainer(EquipmentTypeConst.Normal);
        equipConfig.getNormalSlotTemplates().foreachImmutable((i, template) -> {
            normalEquipmentContainer.addInitSlot(template.getId(), template.isDefaultOpen());
        });

        //初始化时装槽
        PlayerEquipmentContainer fashionEquipmentContainer = getData().registerEquipContainer(EquipmentTypeConst.Fashion);
        equipConfig.getFashionSlotTemplates().foreachImmutable((i, template) -> {
            fashionEquipmentContainer.addInitSlot(template.getId(), template.isDefaultOpen());
        });
    }

    @Override
    public void onPlayerBeforeLogin() {
        //TODO 登录时 添加 装备效果 装备套装效果
    }

    @Override
    public void onPlayerLoginPushBaseClientData() {
        PbEquipment.S2CLoginPushPlayerAllEquipments.Builder builder = PbEquipment.S2CLoginPushPlayerAllEquipments.newBuilder();
        builder.setPlayerEquipmentData((PbEquipment.PbPlayerEquipmentData) getData().save());
        player.send(PtCode.S2CLoginPushPlayerAllEquipments, builder.build());
    }

    /**
     * 获取装备容器
     *
     * @param equipmentType
     * @return
     */
    public PlayerEquipmentContainer getPlayerEquipmentContainer(int equipmentType) {
        return getData().getPlayerEquipmentContainer(equipmentType);
    }

    /**
     * 穿装备
     */
    public void putOnEquipment(int bagType, int index, int equipmentType) {
        PlayerItemContainer playerItemContainer = player.getBagModule().getPlayerItemContainer(bagType);
        if (playerItemContainer == null) {
            player.warn("putOnEquipment invalid params bagType, index, equipmentType", bagType, index, equipmentType);
            return;
        }
        PlayerEquipmentContainer playerEquipmentContainer = getPlayerEquipmentContainer(equipmentType);
        if (playerEquipmentContainer == null) {
            player.warn("putOnEquipment invalid params bagType, index, equipmentType", bagType, index, equipmentType);
            return;
        }
        ItemData itemData = playerItemContainer.getItemByIndex(index);
        if (itemData == null) {
            player.warn("putOnEquipment invalid params bagType, index, equipmentType", bagType, index, equipmentType);
            return;
        }
        if (itemData.getItemTemplate().getType() != ItemTypeConst.Equipment) {
            player.warn("putOnEquipment item type is not Equipment bagType, index, equipmentType", bagType, index, equipmentType);
            return;
        }
        EquipmentExtraData equipmentExtraData = (EquipmentExtraData) itemData.getExtraData();
        if (equipmentExtraData.getEquipmentTemplate().getEquipmentType() != equipmentType) {
            player.warn("putOnEquipment item type is not Equipment bagType, index, equipmentType", bagType, index, equipmentType);
            return;
        }

        int slot = equipmentExtraData.getEquipmentTemplate().getSlot();

        ItemData preEquipItemData = playerEquipmentContainer.getEquipment(slot);

        //TODO 检测穿戴物品条件 check itemData itemLevel itemUseLevel

        if (!playerEquipmentContainer.putOnEquipment(slot, itemData, true)) {
            return;
        }
        playerItemContainer.removeItemByIndexDirectly(index, BehaviorSourceConst.PutOnEquipment);

        if (preEquipItemData != null) {
            playerItemContainer.addItemByIndex(preEquipItemData, index, BehaviorSourceConst.PutOnEquipment);
        }
        getData().setChange(true);

        PbEquipment.S2CPutOnEquipment.Builder builder = PbEquipment.S2CPutOnEquipment.newBuilder();
        builder.setEquipmentType(equipmentType);
        builder.setSlot(slot);
        builder.setItemData(PbLogicUtils.itemObj2Pb(itemData, 0));
        player.send(PtCode.S2CPutOnEquipment, builder.build());
    }

    /**
     * 脱装备
     */
    public void putOffEquipment(int equipmentType, int slot, int bagType) {
        PlayerEquipmentContainer playerEquipmentContainer = getPlayerEquipmentContainer(equipmentType);
        if (playerEquipmentContainer == null) {
            player.warn("putOffEquipment invalid params equipmentType, slot, bagType", equipmentType, slot, bagType);
            return;
        }
        PlayerItemContainer playerItemContainer = player.getBagModule().getPlayerItemContainer(bagType);
        if (playerItemContainer == null) {
            player.warn("putOffEquipment invalid params equipmentType, slot, bagType", equipmentType, slot, bagType);
            return;
        }
        ItemData itemData = playerEquipmentContainer.getEquipment(slot);
        if (itemData == null) {
            player.warn("putOffEquipment invalid params equipmentType, slot, bagType", equipmentType, slot, bagType);
            return;
        }

        if (!playerItemContainer.isCanAddItemSuccess(itemData.getItemID(), itemData.getItemNum(), itemData.isBind())) {
            player.warn("putOffEquipment cant addItemToBag equipmentType, slot, bagType", equipmentType, slot, bagType);
            return;
        }

        if (!playerEquipmentContainer.putOffEquipment(slot)) {
            return;
        }
        playerItemContainer.addItem(itemData, BehaviorSourceConst.PutOffEquipment);
        getData().setChange(true);

        PbEquipment.S2CPutOffEquipment.Builder builder = PbEquipment.S2CPutOffEquipment.newBuilder();
        builder.setEquipmentType(equipmentType);
        builder.setSlot(slot);
        player.send(PtCode.S2CPutOffEquipment, builder.build());
    }
}
