package com.highfly029.logic.module.mail;

import java.util.List;
import java.util.function.Consumer;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.logic.common.container.item.ItemData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbMail;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.logic.module.BaseModule;
import com.highfly029.logic.utils.PbLogicUtils;
import com.highfly029.utils.collection.IntLongMap;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.ObjectList;

import io.vertx.core.buffer.Buffer;
import io.vertx.mysqlclient.MySQLClient;
import io.vertx.mysqlclient.MySQLPool;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName MailModule
 * @Description 邮件模块
 * @Author liyunpeng
 **/
public class MailModule extends BaseModule {
    private LongObjectMap<MailData> mailMap = new LongObjectMap<>();

    public void addMail(int type, int titleID, int contentID, int behaviorSource) {
        addMail(type, titleID, contentID, behaviorSource, null, null, null, null);
    }

    public void addMail(int type, int titleID, int contentID, int behaviorSource, ObjectList<String> titleArgs, ObjectList<String> contentArgs) {
        addMail(type, titleID, contentID, behaviorSource, titleArgs, contentArgs, null, null);
    }

    public void addMail(int type, int titleID, int contentID, int behaviorSource, ItemData... itemDataArray) {
        ObjectList<ItemData> itemList = new ObjectList<>(ItemData[]::new);
        itemList.addArray(itemDataArray);
        addMail(type, titleID, contentID, behaviorSource, null, null, itemList, null);
    }

    public void addMail(int type, int titleID, int contentID, int behaviorSource, ObjectList<ItemData> itemList) {
        addMail(type, titleID, contentID, behaviorSource, null, null, itemList, null);
    }

    public void addMail(int type, int titleID, int contentID, int behaviorSource, IntLongMap assetMap) {
        addMail(type, titleID, contentID, behaviorSource, null, null, null, assetMap);
    }

    public void addMail(int type, int titleID, int contentID, int behaviorSource, ObjectList<ItemData> itemList, IntLongMap assetMap) {
        addMail(type, titleID, contentID, behaviorSource, null, null, itemList, assetMap);
    }

    public void addMail(int type, int titleID, int contentID, int behaviorSource,
                        ObjectList<String> titleArgs, ObjectList<String> contentArgs,
                        ObjectList<ItemData> itemList, IntLongMap assetMap) {

        long playerID = player.getPlayerID();
        MySQLPool pool = player.getLogicChildWorld().getConnectionPool(player);
        int worldIndex = player.getChildWorldIndex();
        MailData mailData = MailTool.createMailData(type, titleID, contentID, behaviorSource, titleArgs, contentArgs, itemList, assetMap);
        String insertSQLStr = "INSERT INTO mail values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Tuple tuple = MailTool.createMailTuple(playerID, mailData);
        MySQLUtils.sendChild(pool, insertSQLStr, tuple, worldIndex, (isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                long mailID = successRows.property(MySQLClient.LAST_INSERTED_ID);
                mailData.setMailID(mailID);
                mailMap.put(mailID, mailData);
                PbMail.PbMailData pbMailData = PbLogicUtils.mailObj2Pb(mailData);
                PbMail.S2CAddMail.Builder builder = PbMail.S2CAddMail.newBuilder();
                builder.setMail(pbMailData);
                player.send(PtCode.S2CAddMail, builder.build());
                player.info("addMail success", mailData);
            } else {
                player.error("addMail insert fail", errorMsg, mailData);
            }
        });
    }

    /**
     * global新增邮件通知
     * @param mailData
     */
    public void onGlobalAddMail(MailData mailData) {
        player.info("onGlobalAddMail", mailData);
        mailMap.put(mailData.getMailID(), mailData);
        PbMail.PbMailData pbMailData = PbLogicUtils.mailObj2Pb(mailData);
        PbMail.S2CAddMail.Builder builder = PbMail.S2CAddMail.newBuilder();
        builder.setMail(pbMailData);
        player.send(PtCode.S2CAddMail, builder.build());

    }

    @Override
    public void onPlayerAfterLogin() {
        super.onPlayerAfterLogin();
        getMailList(true, map -> {
            player.info("邮件内容", map);
        });
    }

    /**
     * 获取全部邮件列表
     *
     * @param isPullFromDB 是否从数据库拉去最新数据
     * @param consumer     全部邮件
     */
    public void getMailList(boolean isPullFromDB, Consumer<LongObjectMap<MailData>> consumer) {
        if (isPullFromDB) {
            String sql = "SELECT * FROM mail WHERE player_id = ?";
            Tuple tuple = Tuple.of(player.getPlayerID());
            MySQLUtils.sendChild(player.getLogicChildWorld().getConnectionPool(player), sql, tuple, player.getChildWorldIndex(), (isSuccess, successRows, errorMsg) -> {
                if (isSuccess) {
                    LongObjectMap<MailData> map = new LongObjectMap<>();
                    for (Row row : successRows) {
                        MailData mailData = new MailData();
                        long mailID = row.getLong("id");
                        mailData.setMailID(mailID);
                        int type = row.getInteger("type");
                        mailData.setType(type);
                        int titleID = row.getInteger("title_id");
                        mailData.setTitleID(titleID);
                        int contentID = row.getInteger("content_id");
                        mailData.setContentID(contentID);
                        Buffer extraArgsBuffer = row.getBuffer("extra_args");
                        if (extraArgsBuffer.length() > 0) {
                            try {
                                PbMail.PbMailExtraArgs pbMailExtraArgs = PbMail.PbMailExtraArgs.parseFrom(extraArgsBuffer.getBytes());
                                MailExtraArgs mailExtraArgs = new MailExtraArgs();
                                if (!pbMailExtraArgs.getTitleArgsList().isEmpty()) {
                                    ObjectList<String> titleArgs = new ObjectList<>(String[]::new);
                                    for (String title : pbMailExtraArgs.getTitleArgsList()) {
                                        titleArgs.add(title);
                                    }
                                    mailExtraArgs.setTitleArgs(titleArgs);
                                }
                                if (!pbMailExtraArgs.getContentArgsList().isEmpty()) {
                                    ObjectList<String> contentArgs = new ObjectList<>(String[]::new);
                                    for (String content : pbMailExtraArgs.getContentArgsList()) {
                                        contentArgs.add(content);
                                    }
                                    mailExtraArgs.setContentArgs(contentArgs);
                                }
                                mailData.setMailExtraArgs(mailExtraArgs);
                            } catch (InvalidProtocolBufferException e) {
                                player.error("getMailList invalid extraArgs mailID=" + mailID, e);
                            }
                        }
                        int isRead = row.getInteger("is_read");
                        mailData.setIsRead(isRead);
                        int isReceive = row.getInteger("is_receive");
                        mailData.setIsReceive(isReceive);
                        Buffer attachmentBuffer = row.getBuffer("attachment");
                        if (attachmentBuffer.length() > 0) {
                            try {
                                PbMail.PbMailAttachment pbMailAttachment = PbMail.PbMailAttachment.parseFrom(attachmentBuffer.getBytes());
                                MailAttachment attachment = new MailAttachment();
                                if (!pbMailAttachment.getItemListList().isEmpty()) {
                                    ObjectList<ItemData> itemList = new ObjectList<>(ItemData[]::new);
                                    for (PbCommon.PbItemData pbItemData : pbMailAttachment.getItemListList()) {
                                        ItemData itemData = PbLogicUtils.itemPb2Obj(pbItemData);
                                        itemList.add(itemData);
                                    }
                                    attachment.setItemList(itemList);
                                }
                                if (!pbMailAttachment.getAssetListList().isEmpty()) {
                                    IntLongMap assetMap = new IntLongMap();
                                    for (PbCommon.PbKeyValue pbKeyValue : pbMailAttachment.getAssetListList()) {
                                        assetMap.put(pbKeyValue.getIntKey(), pbKeyValue.getLongValue());
                                    }
                                    attachment.setAssetMap(assetMap);
                                }
                                mailData.setAttachment(attachment);
                            } catch (InvalidProtocolBufferException e) {
                                player.error("getMailList invalid attachment mailID=" + mailID, e);
                            }
                        }
                        int behaviorSource = row.getInteger("behavior_source");
                        mailData.setBehaviorSource(behaviorSource);
                        long createTime = row.getLong("create_time");
                        mailData.setCreateTime(createTime);
                        map.put(mailID, mailData);
                    }
                    mailMap = map;
                    consumer.accept(mailMap);
                } else {
                    player.error("getMailList", errorMsg);
                }
            });
        } else {
            consumer.accept(mailMap);
        }
    }

    /**
     * 读取邮件
     *
     * @param list
     */
    public void readMail(List<Long> list) {
        if (list == null || list.isEmpty()) {
            player.warn("readMail invalid list");
            return;
        }
        for (long mailID : list) {
            MailData mailData = mailMap.get(mailID);
            if (mailData == null) {
                player.warn("readMail not exist", mailID);
                return;
            }
            if (mailData.getIsRead() > 0) {
                player.warn("readMail duplicate read", mailID);
                return;
            }
        }
        StringBuilder sqlBuilder = new StringBuilder("UPDATE mail SET is_read=1 WHERE player_id = ");
        sqlBuilder.append(player.getPlayerID());
        sqlBuilder.append(" AND id IN (");
        for (long mailID : list) {
            sqlBuilder.append(mailID).append(",");
        }
        sqlBuilder.deleteCharAt(sqlBuilder.length() - 1);
        sqlBuilder.append(");");
        MySQLPool pool = player.getLogicChildWorld().getConnectionPool(player);
        int worldIndex = player.getChildWorldIndex();
        MySQLUtils.sendChild(pool, sqlBuilder.toString(), null, worldIndex, (isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                for (long mailID : list) {
                    MailData mailData = mailMap.get(mailID);
                    if (mailData == null) {
                        player.error("readMail after db not exist", mailID);
                        continue;
                    }
                    mailData.setIsRead(1);
                }
                PbMail.S2CRefreshMailReadFlag.Builder builder = PbMail.S2CRefreshMailReadFlag.newBuilder();
                builder.addAllMailID(list);
                player.send(PtCode.S2CRefreshMailReadFlag, builder.build());
            } else {
                player.error("readMail", errorMsg);
            }
        });
    }

    /**
     * 领取邮件附件
     *
     * @param list
     */
    public void receiveMailAttachment(List<Long> list) {
        if (list == null || list.isEmpty()) {
            player.warn("receiveMailAttachment invalid list");
            return;
        }
        int needGridNum = 0;
        for (long mailID : list) {
            MailData mailData = mailMap.get(mailID);
            if (mailData == null) {
                player.warn("receiveMailAttachment not exist", mailID);
                return;
            }
            if (mailData.getAttachment() == null) {
                player.warn("receiveMailAttachment not exist attachment", mailID);
                return;
            }
            if (mailData.getIsReceive() > 0) {
                player.warn("receiveMailAttachment duplicate read", mailID);
                return;
            }
            if (mailData.getAttachment().getItemList() != null) {
                needGridNum += mailData.getAttachment().getItemList().size();
            }
        }
        //只检测占用的格子,不考虑堆叠,所以邮件里的ItemData必须能放入一个格子中,否则在创建邮件数据的时候拆分处理
        if (!player.getBagModule().getPlayerBagItemContainer().isHaveMoreFreeGridNum(needGridNum)) {
            player.warn("receiveMailAttachment cant add to bag");
            return;
        }

        StringBuilder sqlBuilder = new StringBuilder("UPDATE mail SET is_receive=1 WHERE player_id = ");
        sqlBuilder.append(player.getPlayerID());
        sqlBuilder.append(" AND id IN (");
        for (long mailID : list) {
            sqlBuilder.append(mailID).append(",");
        }
        sqlBuilder.deleteCharAt(sqlBuilder.length() - 1);
        sqlBuilder.append(");");
        MySQLPool pool = player.getLogicChildWorld().getConnectionPool(player);
        int worldIndex = player.getChildWorldIndex();
        MySQLUtils.sendChild(pool, sqlBuilder.toString(), null, worldIndex, (isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                for (long mailID : list) {
                    MailData mailData = mailMap.get(mailID);
                    if (mailData == null) {
                        player.error("receiveMailAttachment after db not exist", mailID);
                        continue;
                    }
                    MailAttachment attachment = mailData.getAttachment();
                    if (attachment == null) {
                        player.error("receiveMailAttachment after db attachment is null", mailID, mailData);
                        continue;
                    }
                    mailData.setIsReceive(1);
                    if (attachment.getAssetMap() != null) {
                        attachment.getAssetMap().foreachImmutable((k, v) -> {
                            player.getAssetModule().addAsset(k, v, mailData.getBehaviorSource());
                        });
                    }
                    if (attachment.getItemList() != null) {
                        player.getBagModule().getPlayerBagItemContainer().addItems(attachment.getItemList(), mailData.getBehaviorSource());
                    }
                }
                PbMail.S2CRefreshMailReceiveFlag.Builder builder = PbMail.S2CRefreshMailReceiveFlag.newBuilder();
                builder.addAllMailID(list);
                player.send(PtCode.S2CRefreshMailReceiveFlag, builder.build());
            } else {
                player.error("receiveMailAttachment", errorMsg);
            }
        });
    }

    /**
     * 删除邮件
     *
     * @param list
     */
    public void deleteMail(List<Long> list) {
        if (list == null || list.isEmpty()) {
            player.warn("deleteMail invalid list");
            return;
        }
        for (long mailID : list) {
            MailData mailData = mailMap.get(mailID);
            if (mailData == null) {
                player.warn("deleteMail not exist", mailID);
                return;
            }
            if (mailData.getAttachment() != null && mailData.getIsReceive() <= 0) {
                player.warn("deleteMail exist attachment", mailID);
                return;
            }
        }
        StringBuilder sqlBuilder = new StringBuilder("DELETE FROM mail WHERE player_id = ");
        sqlBuilder.append(player.getPlayerID());
        sqlBuilder.append(" AND id IN (");
        for (long mailID : list) {
            sqlBuilder.append(mailID).append(",");
        }
        sqlBuilder.deleteCharAt(sqlBuilder.length() - 1);
        sqlBuilder.append(");");
        MySQLPool pool = player.getLogicChildWorld().getConnectionPool(player);
        int worldIndex = player.getChildWorldIndex();
        MySQLUtils.sendChild(pool, sqlBuilder.toString(), null, worldIndex, (isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                for (long mailID : list) {
                    mailMap.remove(mailID);
                }
                PbMail.S2CDeleteSuccessMail.Builder builder = PbMail.S2CDeleteSuccessMail.newBuilder();
                builder.addAllMailID(list);
                player.send(PtCode.S2CDeleteSuccessMail, builder.build());
            } else {
                player.error("deleteMail", errorMsg);
            }
        });
    }
}
