package com.highfly029.logic.module.activity;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.packet.PbActivity;
import com.highfly029.logic.module.PlayerDataBaseAccess;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName PlayerActivityData
 * @Description PlayerActivityData
 * @Author liyunpeng
 **/
public class PlayerActivityData extends PlayerDataBaseAccess {
    private final IntObjectMap<BaseActivity> activities = new IntObjectMap<>(BaseActivity[]::new);

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbActivity.PbPlayerActivityData pbPlayerActivityData = PbActivity.PbPlayerActivityData.parseFrom(data);
        int num = pbPlayerActivityData.getAllActivityCount();
        if (num > 0) {
            for (int i = 0; i < num; i++) {
                PbActivity.PbActivityData pbActivityData = pbPlayerActivityData.getAllActivity(i);
                int id = pbActivityData.getId();
                BaseActivity baseActivity = ActivityFactory.create(getPlayer(), id);
                baseActivity.activityDataInitFromPb(pbActivityData);
                activities.put(id, baseActivity);
            }
        }
        return pbPlayerActivityData;
    }

    @Override
    public GeneratedMessageV3 save() {
        PbActivity.PbPlayerActivityData.Builder builder = PbActivity.PbPlayerActivityData.newBuilder();
        activities.foreachImmutable((id, activityData) -> builder.addAllActivity(activityData.activityDataToPb()));
        return builder.build();
    }

    public IntObjectMap<BaseActivity> getActivities() {
        return activities;
    }

    @Override
    public String getName() {
        return "activity";
    }
}
