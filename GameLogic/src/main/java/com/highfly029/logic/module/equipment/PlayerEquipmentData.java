package com.highfly029.logic.module.equipment;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbEquipment;
import com.highfly029.logic.module.PlayerDataBaseAccess;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.utils.PbLogicUtils;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName PlayerEquipmentData
 * @Description 玩家装备数据
 * @Author liyunpeng
 **/
public class PlayerEquipmentData extends PlayerDataBaseAccess {
    /**
     * 玩家装备数据集合
     */
    private IntObjectMap<PlayerEquipmentContainer> playerEquipmentsMap = new IntObjectMap<>(PlayerEquipmentContainer[]::new);

    private final Player player;

    public PlayerEquipmentData(Player player) {
        this.player = player;
    }

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbEquipment.PbPlayerEquipmentData pbPlayerEquipmentData = PbEquipment.PbPlayerEquipmentData.parseFrom(data);
        int num = pbPlayerEquipmentData.getEquipmentDatasCount();
        if (num > 0) {
            for (int i = 0; i < num; i++) {
                PbCommon.PbEquipmentData pbEquipmentData = pbPlayerEquipmentData.getEquipmentDatas(i);
                PlayerEquipmentContainer playerEquipmentContainer = PbLogicUtils.playerEquipmentPb2Obj(pbEquipmentData, player);
                playerEquipmentsMap.put(playerEquipmentContainer.getEquipmentType(), playerEquipmentContainer);
            }
        }
        return pbPlayerEquipmentData;
    }

    @Override
    public GeneratedMessageV3 save() {
        PbEquipment.PbPlayerEquipmentData.Builder builder = PbEquipment.PbPlayerEquipmentData.newBuilder();
        int freeValue = playerEquipmentsMap.getFreeValue();
        int[] keys = playerEquipmentsMap.getKeys();
        PlayerEquipmentContainer[] values = playerEquipmentsMap.getValues();
        int key;
        PlayerEquipmentContainer value;
        for (int i = 0; i < keys.length; i++) {
            if ((key = keys[i]) != freeValue) {
                if ((value = values[i]) != null) {
                    PbCommon.PbEquipmentData pbEquipmentData = PbLogicUtils.playerEquipmentObj2Pb(value);
                    builder.addEquipmentDatas(pbEquipmentData);
                }
            }
        }
        return builder.build();
    }

    @Override
    public String getName() {
        return "equipment";
    }

    /**
     * 注册容器
     *
     * @param equipmentType
     * @return
     */
    public PlayerEquipmentContainer registerEquipContainer(int equipmentType) {
        if (playerEquipmentsMap.containsKey(equipmentType)) {
            return null;
        }
        PlayerEquipmentContainer playerEquipmentContainer = new PlayerEquipmentContainer(equipmentType, player);
        playerEquipmentsMap.put(equipmentType, playerEquipmentContainer);
        return playerEquipmentContainer;
    }

    /**
     * 获取装备容器
     *
     * @param equipmentType
     * @return
     */
    public PlayerEquipmentContainer getPlayerEquipmentContainer(int equipmentType) {
        return playerEquipmentsMap.get(equipmentType);
    }
}
