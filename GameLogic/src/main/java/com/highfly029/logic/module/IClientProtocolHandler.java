package com.highfly029.logic.module;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.logic.session.PlayerSession;

/**
 * @ClassName ProtocolHandler
 * @Description client协议处理
 * @Author liyunpeng
 **/
public interface IClientProtocolHandler {
    void onClientProtocolHandler(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException;
}
