package com.highfly029.logic.module.gm;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbGm;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.logic.module.IClientProtocolRegister;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName GMProtocol
 * @Description GMProtocol
 * @Author liyunpeng
 **/
public class GMProtocol implements IClientProtocolRegister {
    @Override
    public void registerClientProtocol(LogicChildWorld logicChildWorld) throws Exception {
        logicChildWorld.registerClientProtocol(PtCode.C2SGmCommand, this::gmCommandHandler);
    }

    private void gmCommandHandler(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbGm.C2SGmCommand req = PbGm.C2SGmCommand.parseFrom(packet.getData());
        String command = req.getCommand();
        session.getPlayer().getGmModule().gmCommandHandler(command);
    }
}
