package com.highfly029.logic.module.bag;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbBag;
import com.highfly029.common.template.behaviorSource.BehaviorSourceConst;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.logic.module.IClientProtocolRegister;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName BagProtocol
 * @Description 背包协议
 * @Author liyunpeng
 **/
public class BagProtocol implements IClientProtocolRegister {

    @Override
    public void registerClientProtocol(LogicChildWorld logicChildWorld) throws Exception {
        logicChildWorld.registerClientProtocol(PtCode.C2SUseItemByIndex, this::useItemByIndex);
        logicChildWorld.registerClientProtocol(PtCode.C2SRemoveItemByIndex, this::removeItemByIndex);
        logicChildWorld.registerClientProtocol(PtCode.C2SSwapItemByIndex, this::swapItemByIndex);
        logicChildWorld.registerClientProtocol(PtCode.C2SSplitItem, this::splitItem);
        logicChildWorld.registerClientProtocol(PtCode.C2SSort, this::sort);
        logicChildWorld.registerClientProtocol(PtCode.C2SResetGridNum, this::resetGridNum);
    }

    /**
     * 通过索引使用物品
     */
    public void useItemByIndex(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbBag.C2SUseItemByIndex req = PbBag.C2SUseItemByIndex.parseFrom(packet.getData());

        PlayerItemContainer playerItemContainer = session.getPlayer().getBagModule().getPlayerItemContainer(req.getBagType());
        if (playerItemContainer == null) {
            return;
        }
        playerItemContainer.useItemByIndex(req.getIndex(), req.getNum(), BehaviorSourceConst.UseItemByIndex);
    }

    /**
     * 通过索引删除物品
     */
    public void removeItemByIndex(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbBag.C2SRemoveItemByIndex req = PbBag.C2SRemoveItemByIndex.parseFrom(packet.getData());
        PlayerItemContainer playerItemContainer = session.getPlayer().getBagModule().getPlayerItemContainer(req.getBagType());
        if (playerItemContainer == null) {
            return;
        }
        playerItemContainer.removeItemByIndex(req.getIndex(), req.getNum(), BehaviorSourceConst.RemoveItemByIndex);
    }

    /**
     * 通过索引交换物品,如果toIndex没有物品则视为移动物品
     */
    public void swapItemByIndex(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbBag.C2SSwapItemByIndex req = PbBag.C2SSwapItemByIndex.parseFrom(packet.getData());
        PlayerItemContainer playerItemContainer = session.getPlayer().getBagModule().getPlayerItemContainer(req.getBagType());
        if (playerItemContainer == null) {
            return;
        }
        playerItemContainer.swapItemByIndex(req.getFromIndex(), req.getToIndex());
    }

    /**
     * 拆分物品
     */
    public void splitItem(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbBag.C2SSplitItem req = PbBag.C2SSplitItem.parseFrom(packet.getData());
        PlayerItemContainer playerItemContainer = session.getPlayer().getBagModule().getPlayerItemContainer(req.getBagType());
        if (playerItemContainer == null) {
            return;
        }
        playerItemContainer.splitItem(req.getIndex(), req.getNum());
    }

    /**
     * 整理背包
     */
    public void sort(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbBag.C2SSort req = PbBag.C2SSort.parseFrom(packet.getData());
        PlayerItemContainer playerItemContainer = session.getPlayer().getBagModule().getPlayerItemContainer(req.getBagType());
        if (playerItemContainer == null) {
            return;
        }
        playerItemContainer.sort();
    }

    /**
     * 重置背包格子数
     */
    public void resetGridNum(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbBag.C2SResetGridNum req = PbBag.C2SResetGridNum.parseFrom(packet.getData());
        PlayerItemContainer playerItemContainer = session.getPlayer().getBagModule().getPlayerItemContainer(req.getBagType());
        if (playerItemContainer == null) {
            return;
        }
        playerItemContainer.resetGridNum(req.getNewGridNum());
    }
}
