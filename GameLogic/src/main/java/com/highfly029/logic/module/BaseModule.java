package com.highfly029.logic.module;

/**
 * @ClassName BaseModule
 * @Description BaseModule
 * @Author liyunpeng
 **/
public class BaseModule implements IPlayerAction {
    protected Player player;

    /**
     * 逻辑模块对应的内存数据
     */
    private PlayerDataBaseAccess data;

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setData(PlayerDataBaseAccess data) {
        this.data = data;
    }

    public PlayerDataBaseAccess getData() {
        return data;
    }

    @Override
    public void onPlayerInitData() {

    }

    @Override
    public void onPlayerCreateNewRole() {

    }

    @Override
    public void onPlayerBeforeLogin() {

    }

    @Override
    public void onPlayerLoginPushBaseClientData() {

    }

    @Override
    public void onPlayerLoginPushNextClientData() {

    }

    @Override
    public void onPlayerAfterLogin() {

    }

    @Override
    public void onPlayerDeleteRole() {

    }

    @Override
    public void onPlayerOffline() {

    }

    @Override
    public void onPlayerReconnectLogin() {

    }


    @Override
    public void onPlayerLogout() {

    }

    @Override
    public void onPlayerTick(int escapedMillTime) {

    }

    @Override
    public void onPlayerSecond() {

    }

    @Override
    public void onPlayerMinute() {

    }

    @Override
    public void onPlayerDaily(boolean isLogin) {

    }
}
