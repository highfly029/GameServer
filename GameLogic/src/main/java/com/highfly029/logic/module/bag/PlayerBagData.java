package com.highfly029.logic.module.bag;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.packet.PbBag;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.logic.module.PlayerDataBaseAccess;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.utils.PbLogicUtils;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName PlayerBagData
 * @Description 玩家背包物品容器数据
 * @Author liyunpeng
 **/
public class PlayerBagData extends PlayerDataBaseAccess {
    /**
     * 玩家背包数据集合
     */
    private IntObjectMap<PlayerItemContainer> playerBagsMap = new IntObjectMap<>(PlayerItemContainer[]::new);

    private final Player player;

    public PlayerBagData(Player player) {
        this.player = player;
    }

    @Override
    public GeneratedMessageV3 save() {
        PbBag.PbPlayerBagData.Builder builder = PbBag.PbPlayerBagData.newBuilder();
        int freeValue = this.playerBagsMap.getFreeValue();
        int[] keys = this.playerBagsMap.getKeys();
        PlayerItemContainer[] values = this.playerBagsMap.getValues();
        int key;
        PlayerItemContainer value;
        int len = keys.length;
        for (int i = 0; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                if ((value = values[i]) != null) {
                    PbCommon.PbItemContainerData pbItemContainerData = PbLogicUtils.playerItemContainerObj2Pb(value, value.getBagType());
                    builder.addItemContainerDatas(pbItemContainerData);
                }
            }
        }
        return builder.build();
    }

    @Override
    public String getName() {
        return "bag";
    }

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbBag.PbPlayerBagData pbPlayerBagsData = PbBag.PbPlayerBagData.parseFrom(data);
        int num = pbPlayerBagsData.getItemContainerDatasCount();
        if (num > 0) {
            for (int i = 0; i < num; i++) {
                PbCommon.PbItemContainerData pbItemContainerData = pbPlayerBagsData.getItemContainerDatas(i);
                PlayerItemContainer playerItemContainer = PbLogicUtils.playerItemContainerPb2Obj(pbItemContainerData, player);
                playerItemContainer.resetCache();
                playerBagsMap.put(playerItemContainer.getBagType(), playerItemContainer);
            }
        }
        return pbPlayerBagsData;
    }

    /**
     * 注册物品容器
     *
     * @param type
     * @param gridNum
     * @return
     */
    public PlayerItemContainer registerItemContainer(int type, int gridNum) {
        if (this.playerBagsMap.containsKey(type)) {
            player.error("duplicate registerItemContainer", type);
            return null;
        }
        PlayerItemContainer playerItemContainer = new PlayerItemContainer(gridNum, type, player);
        playerBagsMap.put(type, playerItemContainer);
        return playerItemContainer;
    }

    /**
     * 获取物品容器
     *
     * @param type
     * @return
     */
    public PlayerItemContainer getPlayerItemContainer(int type) {
        return playerBagsMap.get(type);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PlayerBagData:");
        if (playerBagsMap != null && !playerBagsMap.isEmpty()) {
            playerBagsMap.foreachImmutable((k, v) -> {
                sb.append("key=").append(k);
                sb.append("value=").append(v);
            });
        }
        sb.append("PlayerBagData end.");
        return sb.toString();
    }
}
