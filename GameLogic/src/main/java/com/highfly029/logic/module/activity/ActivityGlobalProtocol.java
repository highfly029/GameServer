package com.highfly029.logic.module.activity;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbActivity;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.logic.world.Global2LogicEvent;
import com.highfly029.logic.world.LogicGlobalWorld;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName ActivityGlobalProtocol
 * @Description ActivityGlobalProtocol
 * @Author liyunpeng
 **/
public class ActivityGlobalProtocol implements IRegisterServerGlobalProtocol {
    private LogicGlobalWorld logicGlobalWorld;

    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {
        logicGlobalWorld = (LogicGlobalWorld) globalWorld;
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicReturnOpenedActivity, this::onReturnCenterOpenedActivity);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicOpenActivity, this::onOpenActivity);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicCloseActivity, this::onCloseActivity);
    }

    /**
     * 返回中心服开启的活动
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void onReturnCenterOpenedActivity(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbActivity.Center2LogicReturnOpenedActivity req = PbActivity.Center2LogicReturnOpenedActivity.parseFrom(packet.getData());
        IntSet initSet = new IntSet();
        for (int i = 0, len = req.getOpenedIDListCount(); i < len; i++) {
            initSet.add(req.getOpenedIDList(i));
        }
        logicGlobalWorld.postEvent2AllChild(Global2LogicEvent.INIT_ACTIVITY, initSet, null);
    }

    private void onOpenActivity(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbActivity.Center2LogicOpenActivity req = PbActivity.Center2LogicOpenActivity.parseFrom(packet.getData());
        logicGlobalWorld.postEvent2AllChild(Global2LogicEvent.OPEN_ACTIVITY, req.getId(), null);
    }

    private void onCloseActivity(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbActivity.Center2LogicCloseActivity req = PbActivity.Center2LogicCloseActivity.parseFrom(packet.getData());
        logicGlobalWorld.postEvent2AllChild(Global2LogicEvent.CLOSE_ACTIVITY, req.getId(), req.getForceClose());
    }
}
