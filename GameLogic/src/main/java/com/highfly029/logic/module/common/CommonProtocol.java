package com.highfly029.logic.module.common;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.logic.module.IClientProtocolRegister;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.common.data.playerLiteInfo.PlayerLiteInfoCacheTool;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName CommonProtocol
 * @Description CommonProtocol
 * @Author liyunpeng
 **/
public class CommonProtocol implements IClientProtocolRegister {
    @Override
    public void registerClientProtocol(LogicChildWorld logicChildWorld) throws Exception {
        logicChildWorld.registerClientProtocol(PtCode.C2SGetPlayerLiteInfo, this::getPlayerLiteInfo);
    }

    /**
     * 获取玩家精简信息
     */
    public void getPlayerLiteInfo(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbCommon.C2SGetPlayerLiteInfo req = PbCommon.C2SGetPlayerLiteInfo.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        PlayerLiteInfoCacheTool.getInstance().getPlayerLiteInfo(session.getWorldIndex(), playerID, playerLiteInfo -> {
            if (playerLiteInfo != null) {
                PbCommon.S2CPlayerLiteInfo.Builder builder = PbCommon.S2CPlayerLiteInfo.newBuilder();
                PbCommon.PbPlayerLiteInfoData pbPlayerLiteInfoData = PbCommonUtils.playerLiteInfoObj2Pb(playerLiteInfo);
                builder.setPlayerLiteInfo(pbPlayerLiteInfoData);
                session.getPlayer().send(PtCode.S2CPlayerLiteInfo, builder.build());
            }
        });
    }
}
