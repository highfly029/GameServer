package com.highfly029.logic.module.quest;

import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbQuest;
import com.highfly029.common.template.goal.GoalMasterTypeConst;
import com.highfly029.common.template.quest.QuestAcceptTypeConst;
import com.highfly029.common.template.quest.QuestCommitTypeConst;
import com.highfly029.common.template.quest.QuestExecuteOrderConst;
import com.highfly029.common.template.quest.QuestTemplate;
import com.highfly029.common.template.quest.QuestTypeConst;
import com.highfly029.logic.module.BaseModule;
import com.highfly029.logic.module.goal.GoalData;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.logic.utils.PbLogicUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName QuestModule
 * @Description 任务模块
 * @Author liyunpeng
 **/
public class QuestModule extends BaseModule {

    @Override
    public void onPlayerInitData() {
        setData(new PlayerQuestData());
    }

    @Override
    public PlayerQuestData getData() {
        return (PlayerQuestData) super.getData();
    }

    @Override
    public void onPlayerCreateNewRole() {
        //新号初始自动接取主线任务
        ConfigTool.getQuestConfig().getTemplates().foreachImmutable((k, v) -> {
            //主线任务且服务器自动接取且没有前置任务且没有接取条件
            if (v.getType() == QuestTypeConst.Main && v.getAcceptType() == QuestAcceptTypeConst.Server && v.getAcceptConditions() == null && v.getPreQuests() == null) {
                acceptQuestDirectly(v);
            }
        });
    }

    @Override
    public void onPlayerBeforeLogin() {
        this.getData().getAllQuest().foreachImmutable((k, v) -> {
            player.info("questType", k);
            player.info("completeQuests", v.getCompleteQuestIDSet());
            player.info("acceptQuests", v.getAcceptQuestMap());
            v.getAcceptQuestMap().foreachImmutable((questID, questData) -> {
                if (questData.getGoals() != null && questData.getGoals().length > 0) {
                    for (int i = 0, len = questData.getGoals().length; i < len; i++) {
                        GoalData goalData = questData.getGoals()[i];
                        //玩家登陆前把没有完成的目标重新注册
                        if (!goalData.isComplete()) {
                            player.getGoalModule().registerGoal(goalData, GoalMasterTypeConst.Quest, questID);
                        }
                    }
                }
            });
        });
    }

    @Override
    public void onPlayerLoginPushBaseClientData() {
        PbQuest.S2CLoginPushPlayerQuestData.Builder builder = PbQuest.S2CLoginPushPlayerQuestData.newBuilder();
        builder.setPlayerQuestData((PbQuest.PbPlayerQuestData) this.getData().save());
        player.send(PtCode.S2CLoginPushPlayerQuestData, builder.build());
    }

    /**
     * 接取任务
     *
     * @param questType
     * @param questID
     */
    public void acceptQuest(int questType, int questID) {
        if (isAcceptedQuest(questType, questID)) {
            player.error("acceptQuest already accept quest questType, questID", questType, questID);
            return;
        }
        if (isCompleteQuest(questType, questID)) {
            player.error("acceptQuest already complete quest questType, questID", questType, questID);
            return;
        }
        QuestTemplate questTemplate = ConfigTool.getQuestConfig().getQuestTemplate(questID);

        if (!checkAcceptQuestCondition(questTemplate)) {
            return;
        }

        //检测接任务给道具时背包是否能放下
        if (questTemplate.getAcceptGiveItems() != null) {
            //TODO

        }

        acceptQuestDirectly(questTemplate);
    }

    /**
     * 检测接取任务的前置条件
     *
     * @param questTemplate
     * @return
     */
    private boolean checkAcceptQuestCondition(QuestTemplate questTemplate) {
        //检测前置任务是否完成
        if (questTemplate.getPreQuests() != null) {
            for (int[] preQuest : questTemplate.getPreQuests()) {
                if (!player.getQuestModule().isCompleteQuest(preQuest[0], preQuest[1])) {
                    player.error("acceptQuest check condition preQuests cant complete",
                            questTemplate.getType(), questTemplate.getQuestID(), preQuest[0], preQuest[1]);
                    return false;
                }
            }
        }

        //任务接取条件
        if (questTemplate.getAcceptConditions() != null) {
            for (int i = 0, len = questTemplate.getAcceptConditions().length; i < len; i++) {
                if (!player.checkRoleCondition(questTemplate.getAcceptConditions()[i])) {
                    player.error("acceptQuest checkCondition fail",
                            questTemplate.getType(), questTemplate.getQuestID(), i);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 提交任务
     *
     * @param questType
     * @param questID
     * @param selectRewardIndex
     */
    public void commitQuest(int questType, int questID, int selectRewardIndex) {
        IntObjectMap<QuestData> acceptQuests = getData().getAcceptQuestMap(questType);
        if (acceptQuests == null) {
            player.error("commitQuest acceptQuests is null", questType, questID);
            return;
        }

        QuestData questData = acceptQuests.get(questID);
        if (questData == null) {
            player.error("commitQuest questData is null", questType, questID);
            return;
        }

        if (!questData.isComplete()) {
            player.error("commitQuest quest not complete", questType, questID);
            return;
        }

        QuestTemplate questTemplate = questData.getQuestTemplate();

        //交任务是否能成功扣道具
        if (questTemplate.getCommitCostItems() != null) {
            //TODO
        }

        //检测奖励能否放入背包
        if (questTemplate.getSelectOneReward() != null) {
            //选择一个任务奖励
            int rewardItemID = questTemplate.getSelectOneReward()[selectRewardIndex];
            //TODO check
        } else {
            //固定任务奖励
            int reward = questTemplate.getReward();
            //TODO check
        }

        commitQuestDirectly(questData, selectRewardIndex, true);
    }

    /**
     * 放弃任务
     *
     * @param questType
     * @param questID
     */
    public void giveUpQuest(int questType, int questID) {
        IntObjectMap<QuestData> acceptQuests = getData().getAcceptQuestMap(questType);
        if (acceptQuests == null) {
            player.error("giveUpQuest acceptQuests is null", questType, questID);
            return;
        }

        QuestData questData = acceptQuests.get(questID);
        if (questData == null) {
            player.error("giveUpQuest questData is null", questType, questID);
            return;
        }
        giveUpQuestDirectly(questData);
    }

    /**
     * 直接放弃任务
     */
    private void giveUpQuestDirectly(QuestData questData) {
        QuestTemplate questTemplate = questData.getQuestTemplate();
        int questType = questTemplate.getType();
        int questID = questData.getQuestID();
        if (!questTemplate.isCanGiveUp()) {
            player.error("giveUpQuestDirectly cant give up", questType, questID);
            return;
        }

        //删除目标
        for (GoalData goalData : questData.getGoals()) {
            player.getGoalModule().removeGoal(goalData);
        }

        //删除接取的任务
        getData().removeAcceptQuestData(questType, questID);
        getData().setChange(true);
        PbQuest.S2CGiveUpQuest.Builder builder = PbQuest.S2CGiveUpQuest.newBuilder();
        builder.setQuestType(questType);
        builder.setQuestID(questID);
        player.send(PtCode.S2CGiveUpQuest, builder.build());
        onQuestGiveUp(questData);
    }


    /**
     * 是否已接取过任务
     *
     * @param questID
     * @return
     */
    private boolean isAcceptedQuest(int questType, int questID) {
        IntObjectMap<QuestData> acceptQuests = getData().getAcceptQuestMap(questType);
        if (acceptQuests != null) {
            return acceptQuests.containsKey(questID);
        } else {
            return false;
        }
    }

    /**
     * 是否已完成任务
     *
     * @param questID
     * @return
     */
    private boolean isCompleteQuest(int questType, int questID) {
        IntSet completeQuests = getData().getCompleteQuestIDSet(questType);
        if (completeQuests != null) {
            return completeQuests.contains(questID);
        } else {
            return false;
        }
    }

    /**
     * gm命令接任务
     *
     * @param questID
     */
    public void acceptQuestByGM(int questID) {
        QuestTemplate questTemplate = ConfigTool.getQuestConfig().getQuestTemplate(questID);
        int questType = questTemplate.getType();
        if (isAcceptedQuest(questType, questID)) {
            player.error("acceptQuestByGM questID already accept", questID);
            return;
        }

        if (isCompleteQuest(questType, questID)) {
            player.error("acceptQuestByGM questID already complete", questID);
            return;
        }

        acceptQuestDirectly(questTemplate);
    }

    /**
     * 直接接取任务 不判断条件
     *
     * @param questTemplate
     */
    private void acceptQuestDirectly(QuestTemplate questTemplate) {
        QuestData questData = QuestData.create();
        questData.setQuestID(questTemplate.getQuestID());
        questData.setQuestTemplate(questTemplate);

        //设置任务目标
        if (questTemplate.getGoals() != null) {
            if (questTemplate.getQuestExecuteOrder() == QuestExecuteOrderConst.Serial) {
                //顺序执行方式 只添加第一个目标、完成后添加下一个
                GoalData[] goals = new GoalData[1];
                goals[0] = player.getGoalModule().addGoal(GoalMasterTypeConst.Quest, questData.getQuestID(), questTemplate.getGoals()[0]);
                questData.setGoals(goals);
            } else {
                int len = questTemplate.getGoals().length;
                GoalData[] goals = new GoalData[len];
                for (int i = 0; i < len; i++) {
                    goals[i] = player.getGoalModule().addGoal(GoalMasterTypeConst.Quest, questData.getQuestID(), questTemplate.getGoals()[i]);
                }
                questData.setGoals(goals);
            }
        }
        //TODO 如果有过期时间 设置

        getData().addAcceptQuestData(questTemplate.getType(), questData.getQuestID(), questData);

        //接取任务时给物品
        if (questTemplate.getAcceptGiveItems() != null) {
            //TODO
        }

        //send
        sendAcceptQuest(questData);

        onQuestAccept(questData);

        //如果判断条件马上完成的任务、则直接执行完成逻辑
        boolean isComplete = questData.isComplete();
        if (isComplete) {
            questTryAutoCommit(questData, true);
        }
        getData().setChange(true);
    }

    /**
     * 推送客户端接取任务
     */
    private void sendAcceptQuest(QuestData questData) {
        //创建号登陆前接取的任务不发消息
        if (player.isLoginSuccess()) {
            PbQuest.S2CAcceptQuest.Builder builder = PbQuest.S2CAcceptQuest.newBuilder();
            builder.setQuestType(questData.getQuestTemplate().getType());
            PbQuest.PbQuestData pbQuestData = PbLogicUtils.questObj2Pb(questData);
            builder.setQuestData(pbQuestData);
            player.send(PtCode.S2CAcceptQuest, builder.build());
        }
    }

    /**
     * 任务尝试自动提交
     *
     * @param questData
     */
    public void questTryAutoCommit(QuestData questData, boolean isDeleteGoal) {
        if (questData.getQuestTemplate().getCommitType() == QuestCommitTypeConst.Server) {
            //服务器自动提交任务
            commitQuestDirectly(questData, -1, isDeleteGoal);
        }
    }

    /**
     * gm命令完成任务
     *
     * @param questID
     */
    public void commitQuestByGM(int questID) {
        QuestTemplate questTemplate = ConfigTool.getQuestConfig().getQuestTemplate(questID);
        if (questTemplate == null) {
            player.error("commitQuestByGM dont exist questID", questID);
            return;
        }
        int questType = questTemplate.getType();
        if (isCompleteQuest(questType, questID)) {
            return;
        }
        if (!isAcceptedQuest(questType, questID)) {
            return;
        }
        IntObjectMap<QuestData> acceptQuests = getData().getAcceptQuestMap(questType);
        if (acceptQuests == null) {
            return;
        }
        QuestData questData = acceptQuests.get(questID);
        if (questData != null && questData.isComplete()) {
            commitQuestDirectly(questData, -1, true);
        }
    }

    /**
     * 直接提交任务、不判断条件
     *
     * @param questData
     * @param selectRewardIndex 选择任务奖励的索引
     * @param isDeleteGoal
     */
    private void commitQuestDirectly(QuestData questData, int selectRewardIndex, boolean isDeleteGoal) {
        QuestTemplate questTemplate = questData.getQuestTemplate();

        if (isDeleteGoal) {
            //只有在非goalModule触发的时候才删除
            for (GoalData goalData : questData.getGoals()) {
                player.getGoalModule().removeGoal(goalData);
            }
        }

        //删除已接任务
        getData().removeAcceptQuestData(questTemplate.getType(), questData.getQuestID());


        //增加完成任务记录
        getData().addCompleteQuestID(questTemplate.getType(), questData.getQuestID());

        if (questTemplate.getCommitCostItems() != null) {
            //提交任务时扣除物品
        }

        //任务奖励
        if (questTemplate.getSelectOneReward() != null) {
            //选择一个任务奖励
            int rewardItemID = questTemplate.getSelectOneReward()[selectRewardIndex];
            //TODO add
        } else {
            //固定任务奖励
            int reward = questTemplate.getReward();
            //TODO add
        }
        getData().setChange(true);
        //发送客户端完成任务
        sendCommitQuest(questData);

        onQuestComplete(questData);
    }

    /**
     * 推送客户端完成任务
     *
     * @param questData
     */
    private void sendCommitQuest(QuestData questData) {
        if (player.isLoginSuccess()) {
            PbQuest.S2CCommitQuest.Builder builder = PbQuest.S2CCommitQuest.newBuilder();
            builder.setQuestType(questData.getQuestTemplate().getType());
            builder.setQuestID(questData.getQuestID());
            player.send(PtCode.S2CCommitQuest, builder.build());
        }
    }

    /**
     * 任务接取通知
     *
     * @param questData
     */
    protected void onQuestAccept(QuestData questData) {
        player.info("onQuestAccept", questData.getQuestID());
    }


    /**
     * 任务完成通知
     *
     * @param questData
     */
    protected void onQuestComplete(QuestData questData) {
        QuestConfig questConfig = ConfigTool.getQuestConfig();
        player.info("onQuestComplete", questData.getQuestID());
        //如果后续任务可以自动接取且条件满足、则自动接取后续任务
        IntSet afterQuestIDSet = questConfig.getAfterQuestIDSet(questData.getQuestTemplate().getType(), questData.getQuestID());
        if (afterQuestIDSet != null && afterQuestIDSet.size() > 0) {
            afterQuestIDSet.foreachImmutable(k -> {
                QuestTemplate questTemplate = questConfig.getQuestTemplate(k);
                //如果前置任务满足、自动接取后续任务
                if (checkAcceptQuestCondition(questTemplate)) {
                    acceptQuestDirectly(questTemplate);
                }
            });
        }
    }

    /**
     * 任务放弃通知
     *
     * @param questData
     */
    protected void onQuestGiveUp(QuestData questData) {
        player.info("onQuestGiveUp", questData.getQuestID());
    }

    /**
     * 目标进度变化通知
     *
     * @param goalData
     */
    public void onGoalProgressChanged(GoalData goalData) {
        getData().setChange(true);
        //更新任务目标进度
        QuestTemplate questTemplate = ConfigTool.getQuestConfig().getQuestTemplate(goalData.getGoalMasterID());
        PbQuest.S2CUpdateGoalData.Builder builder = PbQuest.S2CUpdateGoalData.newBuilder();
        builder.setQuestType(questTemplate.getType());
        builder.setQuestID(goalData.getGoalMasterID());
        builder.setGoalID(goalData.getGoalID());
        builder.setProgress(goalData.getProgress());
        player.send(PtCode.S2CUpdateGoalData, builder.build());
    }

    /**
     * 目标完成通知
     *
     * @param goalData
     */
    public void onGoalCompleted(GoalData goalData) {
        QuestTemplate questTemplate = ConfigTool.getQuestConfig().getQuestTemplate(goalData.getGoalMasterID());
        IntObjectMap<QuestData> acceptQuests = getData().getAcceptQuestMap(questTemplate.getType());
        if (acceptQuests == null) {
            player.error("onGoalCompleted acceptQuests is null", goalData.getGoalMasterID(), goalData.getGoalID());
            return;
        }
        QuestData questData = acceptQuests.get(goalData.getGoalMasterID());
        if (questData == null) {
            player.error("onGoalCompleted questData is nul", goalData.getGoalMasterID(), goalData.getGoalID());
            return;
        }

        if (questData.isComplete()) {
            questTryAutoCommit(questData, false);
        } else {
            //顺序执行的目标、添加下一个目标
            if (questTemplate.getQuestExecuteOrder() == QuestExecuteOrderConst.Serial) {
                boolean preGoalIDFind = false;
                for (int i = 0, len = questTemplate.getGoals().length; i < len; i++) {
                    if (preGoalIDFind) {
                        //找到下一个目标id
                        int nextGoalID = questTemplate.getGoals()[i];
                        GoalData[] newGoals = new GoalData[questData.getGoals().length + 1];
                        System.arraycopy(questData.getGoals(), 0, newGoals, 0, questData.getGoals().length);
                        questData.setGoals(newGoals);
                        GoalData newGoalData = player.getGoalModule().addGoal(GoalMasterTypeConst.Quest, questData.getQuestID(), nextGoalID);
                        questData.getGoals()[newGoals.length - 1] = newGoalData;
                        sendAddGoalData(questData, newGoals.length - 1);
                        //如果顺序执行的goal自动完成了、则继续执行完成事件
                        if (newGoalData.isComplete()) {
                            onGoalCompleted(newGoalData);
                        }
                        break;
                    }
                    if (questTemplate.getGoals()[i] == goalData.getGoalID()) {
                        preGoalIDFind = true;
                    }
                }
            }
        }
        getData().setChange(true);
    }

    /**
     * 推送增加任务目标
     *
     * @param questData
     * @param index
     */
    private void sendAddGoalData(QuestData questData, int index) {
        PbQuest.S2CAddGoalData.Builder builder = PbQuest.S2CAddGoalData.newBuilder();
        builder.setQuestType(questData.getQuestTemplate().getType());
        builder.setQuestID(questData.getQuestID());
        builder.setGoal(PbLogicUtils.goalObj2Pb(questData.getGoals()[index]));
        player.send(PtCode.S2CAddGoalData, builder.build());
    }
}
