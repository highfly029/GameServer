package com.highfly029.logic.module;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.function.Consumer;

import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.googlecode.protobuf.format.JsonFormat;
import com.highfly029.common.data.DataBaseAccess;
import com.highfly029.common.data.base.Location;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.common.redis.PlayerLiteInfoKeyEnum;
import com.highfly029.common.redis.RedisKey;
import com.highfly029.common.template.playerCondition.PlayerConditionConst;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.net.NetUtils;
import com.highfly029.core.tool.RedisClientTool;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.logic.module.achievement.AchievementModule;
import com.highfly029.logic.module.activity.ActivityModule;
import com.highfly029.logic.module.asset.AssetModule;
import com.highfly029.logic.module.auction.AuctionModule;
import com.highfly029.logic.module.bag.BagModule;
import com.highfly029.logic.module.equipment.EquipmentModule;
import com.highfly029.logic.module.function.FunctionModule;
import com.highfly029.logic.module.gm.GMModule;
import com.highfly029.logic.module.goal.GoalModule;
import com.highfly029.logic.module.login.LoginModule;
import com.highfly029.logic.module.mail.MailModule;
import com.highfly029.logic.module.quest.QuestModule;
import com.highfly029.logic.module.rank.RankModule;
import com.highfly029.logic.module.role.RoleModule;
import com.highfly029.logic.module.scene.SceneModule;
import com.highfly029.logic.module.union.UnionModule;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.world.LogicChildWorld;
import com.highfly029.utils.ByteUtils;
import com.highfly029.utils.collection.ObjectList;

import io.netty.buffer.ByteBufUtil;
import io.vertx.core.buffer.Buffer;
import io.vertx.redis.client.Command;
import io.vertx.redis.client.Request;
import io.vertx.redis.client.Response;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName Player
 * @Description 玩家
 * @Author liyunpeng
 **/
public class Player {
    private LogicChildWorld logicChildWorld;
    private long accountID;
    private long playerID;
    //源逻辑服id
    private int sourceLogicID;
    //数据版本号
    private long versionCode;
    private PlayerSession session;
    private Location location;
    /**
     * 超时后的登出截止时间
     */
    private long logoutEndTime;

    private ObjectList<BaseModule> moduleList;

    /************** 快捷引用 *****************/
    private GMModule gmModule;
    private BagModule bagModule;
    private AssetModule assetModule;
    private EquipmentModule equipmentModule;
    private RoleModule roleModule;
    private FunctionModule functionModule;
    private GoalModule goalModule;
    private QuestModule questModule;
    private AchievementModule achievementModule;
    private UnionModule unionModule;
    private LoginModule loginModule;
    private SceneModule sceneModule;
    private MailModule mailModule;
    private AuctionModule auctionModule;
    private ActivityModule activityModule;
    private RankModule rankModule;

    /************** 快捷引用 *****************/

    /**
     * 注册玩家身上的模块 快捷引用
     */
    public void registerPlayerModule(LogicChildWorld logicChildWorld) {
        this.logicChildWorld = logicChildWorld;
        moduleList = new ObjectList<>(4, BaseModule[]::new);

        gmModule = new GMModule();
        moduleList.add(gmModule);

        bagModule = new BagModule();
        moduleList.add(bagModule);

        assetModule = new AssetModule();
        moduleList.add(assetModule);

        equipmentModule = new EquipmentModule();
        moduleList.add(equipmentModule);

        roleModule = new RoleModule();
        moduleList.add(roleModule);

        functionModule = new FunctionModule();
        moduleList.add(functionModule);

        goalModule = new GoalModule();
        moduleList.add(goalModule);

        questModule = new QuestModule();
        moduleList.add(questModule);

        achievementModule = new AchievementModule();
        moduleList.add(achievementModule);

        unionModule = new UnionModule();
        moduleList.add(unionModule);

        loginModule = new LoginModule();
        moduleList.add(loginModule);

        sceneModule = new SceneModule();
        moduleList.add(sceneModule);

        mailModule = new MailModule();
        moduleList.add(mailModule);

        auctionModule = new AuctionModule();
        moduleList.add(auctionModule);

        activityModule = new ActivityModule();
        moduleList.add(activityModule);

        rankModule = new RankModule();
        moduleList.add(rankModule);

        setPlayer();
    }

    public GMModule getGmModule() {
        return gmModule;
    }

    public BagModule getBagModule() {
        return bagModule;
    }

    public AssetModule getAssetModule() {
        return assetModule;
    }

    public EquipmentModule getEquipmentModule() {
        return equipmentModule;
    }

    public RoleModule getRoleModule() {
        return roleModule;
    }

    public FunctionModule getFunctionModule() {
        return functionModule;
    }

    public GoalModule getGoalModule() {
        return goalModule;
    }

    public QuestModule getQuestModule() {
        return questModule;
    }

    public AchievementModule getAchievementModule() {
        return achievementModule;
    }

    public UnionModule getUnionModule() {
        return unionModule;
    }

    public LoginModule getLoginModule() {
        return loginModule;
    }

    public SceneModule getSceneModule() {
        return sceneModule;
    }

    public MailModule getMailModule() {
        return mailModule;
    }

    public AuctionModule getAuctionModule() {
        return auctionModule;
    }

    public ActivityModule getActivityModule() {
        return activityModule;
    }

    public RankModule getRankModule() {
        return rankModule;
    }

    public LogicChildWorld getLogicChildWorld() {
        return logicChildWorld;
    }

    public PlayerSession getSession() {
        return session;
    }

    public void setSession(PlayerSession session) {
        this.session = session;
    }

    public long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(long versionCode) {
        this.versionCode = versionCode;
    }

    public int getSourceLogicID() {
        return sourceLogicID;
    }

    public void setSourceLogicID(int sourceLogicID) {
        this.sourceLogicID = sourceLogicID;
    }

    /**
     * 设置模块player
     */
    private void setPlayer() {
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            list[i].setPlayer(this);
        }
    }

    /**
     * 获取插入新player的sql字符串
     *
     * @return
     */
    public String getInsertSQLStr() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO player(");
        stringBuilder.append("player_id").append(",");
        stringBuilder.append("account_id").append(",");
        stringBuilder.append("versionCode").append(",");
        int num = 3;
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            DataBaseAccess dataBaseAccesses = list[i].getData();
            if (dataBaseAccesses != null) {
                num++;
                stringBuilder.append(dataBaseAccesses.getName()).append(",");
            }
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        stringBuilder.append(") VALUES(");
        for (int i = 0; i < num; i++) {
            stringBuilder.append("?").append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        stringBuilder.append(");");
        return stringBuilder.toString();
    }

    /**
     * 获取插入新player的sql数据
     *
     * @return
     */
    public Tuple getInsertSQLTuple() {
        List<Object> dataList = new ArrayList<>();
        dataList.add(playerID);
        dataList.add(accountID);
        dataList.add(1);
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            DataBaseAccess dataBaseAccesses = list[i].getData();
            if (dataBaseAccesses != null) {
                Buffer buffer = Buffer.buffer(dataBaseAccesses.save().toByteArray());
                dataList.add(buffer);
            }
        }
        return Tuple.wrap(dataList);
    }

    /**
     * 获取全部模块的数据库字段名
     *
     * @return
     */
    public String getAllModuleSQLName() {
        StringBuilder stringBuilder = new StringBuilder();
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            DataBaseAccess dataBaseAccesses = list[i].getData();
            if (dataBaseAccesses != null) {
                stringBuilder.append("`").append(dataBaseAccesses.getName()).append("`,");
            }
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    /**
     * 加载数据库数据
     *
     * @param row
     * @throws InvalidProtocolBufferException
     */
    public void load(Row row) throws InvalidProtocolBufferException {
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            PlayerDataBaseAccess playerDataBaseAccess = list[i].getData();
            if (playerDataBaseAccess != null) {
                String name = playerDataBaseAccess.getName();
                Buffer buffer = row.getBuffer(name);
                byte[] data = buffer.getBytes();
                playerDataBaseAccess.setPlayer(this);
                playerDataBaseAccess.load(data);
            }
        }
    }

    /**
     * 加载redis数据
     */
    public void load(Response allResponse) throws InvalidProtocolBufferException {
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            PlayerDataBaseAccess playerDataBaseAccess = list[i].getData();
            if (playerDataBaseAccess != null) {
                String name = playerDataBaseAccess.getName();
                Response response = allResponse.get(name);
                if (response != null) {
                    Buffer buffer = response.toBuffer();
                    byte[] data = buffer.getBytes();
                    playerDataBaseAccess.setPlayer(this);
                    playerDataBaseAccess.load(data);
                }
            }
        }
    }

    public int getChildWorldIndex() {
        return logicChildWorld.getIndex();
    }

    public long getAccountID() {
        return accountID;
    }

    public long getPlayerID() {
        return playerID;
    }

    /**
     * 是否可以发送消息
     *
     * @return
     */
    public boolean isCanSendMessage() {
        return session != null;
    }

    /**
     * 是否已经登陆成功
     *
     * @return
     */
    public boolean isLoginSuccess() {
        return session != null && location.getState() == Location.LOGIN_SUCCESS;
    }

    public void send(int protocolCode) {
        if (session != null) {
            session.send(protocolCode);
        }
    }

    public void send(int protocolCode, GeneratedMessageV3 msg) {
        if (session != null) {
            session.send(protocolCode, msg);
        }
    }

    public Location getLocation() {
        return location;
    }

    /**
     * 玩家发送消息到场景服
     *
     * @param ptCode
     * @param messageV3
     */
    public void sendMsg2Scene(int ptCode, GeneratedMessageV3 messageV3) {
        logicChildWorld.sendMsg2Scene(location.getSceneServerID(), location.getSceneWorldIndex(), playerID, ptCode, messageV3);
    }

    /**
     * 初始化玩家内存数据
     *
     * @param logicChildWorld
     * @param accountID
     * @param playerID
     */
    public void init(LogicChildWorld logicChildWorld, long accountID, long playerID) {
        registerPlayerModule(logicChildWorld);
        this.playerID = playerID;
        this.accountID = accountID;
        location = Location.create();
        //登陆中
        location.setState(Location.LOGIN_ING);
        location.setPlayerID(playerID);
        onStateChange();
        location.setLogicServerID(ConfigConst.identifier);
        location.setLogicWorldIndex(logicChildWorld.getIndex());
    }

    /**
     * 初始化玩家数据
     *
     * @return
     */
    public void onPlayerInitData() {
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            list[i].onPlayerInitData();
        }
    }

    /**
     * 玩家创建新角色
     */
    public void onPlayerCreateNewRole() {
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            list[i].onPlayerCreateNewRole();
        }
    }

    /**
     * 初始化玩家精简信息
     */
    public void initPlayerLiteInfo() {
        Request request = Request.cmd(Command.HMSET).arg(RedisKey.PLAYER_LITE_INFO.value() + playerID)
                .arg(PlayerLiteInfoKeyEnum.PLAYER_ID.value()).arg(playerID)
                .arg(PlayerLiteInfoKeyEnum.PLAYER_NAME.value()).arg("name" + playerID)
                .arg(PlayerLiteInfoKeyEnum.PLAYER_LEVEL.value()).arg(roleModule.getLevel());
        RedisClientTool.send(getChildWorldIndex(), request, (isSuccess, response, errorMsg) -> {
            if (!isSuccess) {
                error("initPlayerLiteInfo redis fail", errorMsg);
            }
        });
    }

    /**
     * 玩家登录之前
     */
    public void onPlayerBeforeLogin() {
        try {
            BaseModule[] list = moduleList.getValues();
            for (int i = 0, len = moduleList.size(); i < len; i++) {
                list[i].onPlayerBeforeLogin();
            }
        } catch (Exception e) {
            error("onPlayerBeforeLogin", e);
        }

        //登录时检测跨天
        if (SystemTimeTool.getMillTime() >= roleModule.getData().getNextDailyTime()) {
            onPlayerDaily(true);
        }
    }

    /**
     * 推送玩家的定位状态
     */
    private void pushPlayerLocation2LoadBalance() {
        PbLoadBalance.S2LPushPlayerLocationState.Builder builder = PbLoadBalance.S2LPushPlayerLocationState.newBuilder();
        builder.setPlayerID(playerID);
        builder.setAccountID(accountID);
        builder.setLocation(PbCommonUtils.locationObj2Pb(getLocation()));
        logicChildWorld.sendMsg2AllLoadBalanceLogic(PtCode.S2LPushPlayerLocationState, builder.build());
    }


    /**
     * 玩家登录推送基础数据
     */
    public void onPlayerLoginPushBaseClientData() {
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            list[i].onPlayerLoginPushBaseClientData();
        }
    }

    /**
     * 玩家登录推送后续数据 可以在进入场景之后触发
     */
    public void onPlayerLoginPushNextClientData() {
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            list[i].onPlayerLoginPushNextClientData();
        }
    }

    /**
     * 处理redis离线消息
     */
    public void handlerRedisOfflineMsg(Consumer<Boolean> consumer) {
        Request request = Request.cmd(Command.LRANGE).arg(RedisKey.PLAYER_OFFLINE_MSG.value() + playerID)
                .arg(0).arg(-1);
        RedisClientTool.send(getChildWorldIndex(), request, (isSuccess, response, errorMsg) -> {
            if (isSuccess) {
                int size = response.size();
                //元素成对处理
                size = size / 2 * 2;
                info("handlerRedisOfflineMsg success", size);
                if (size > 0) {
                    Request request1 = Request.cmd(Command.LTRIM).arg(RedisKey.PLAYER_OFFLINE_MSG.value() + playerID)
                            .arg(size).arg(-1);
                    int finalSize = size;
                    RedisClientTool.send(getChildWorldIndex(), request1, ((isSuccess1, response1, errorMsg1) -> {
                        if (isSuccess1) {
                            for (int i = 0; i < finalSize; ) {
                                int ptCode = response.get(i).toInteger();
                                byte[] array = response.get(i + 1).toBytes();
                                ByteString byteString = ByteString.copyFrom(array);
                                onPlayerProcessOfflineMsg(ptCode, byteString);
                                i += 2;
                            }
                            consumer.accept(true);
                        } else {
                            error("handlerRedisOfflineMsg LTRIM fail", finalSize, errorMsg1);
                            consumer.accept(false);
                        }
                    }));
                } else {
                    consumer.accept(true);
                }
            } else {
                error("handlerRedisOfflineMsg RANGE fail", errorMsg);
                consumer.accept(false);
            }
        });
    }

    /**
     * 玩家处理离线消息
     *
     * @param ptCode
     * @param msg
     */
    public void onPlayerProcessOfflineMsg(int ptCode, String msg) {
        if (msg == null) {
            error("onPlayerProcessOfflineMsg msg is null", ptCode, msg);
            return;
        }
        byte[] array = Base64.getDecoder().decode(msg);
        ByteString byteString = ByteString.copyFrom(array);
        onPlayerProcessOfflineMsg(ptCode, byteString);
    }

    /**
     * 玩家处理离线消息
     *
     * @param ptCode
     * @param byteString
     */
    public void onPlayerProcessOfflineMsg(int ptCode, ByteString byteString) {
        IOfflineMessageHandler offlineMessageHandler = logicChildWorld.getOfflineMessageParser(ptCode);
        if (offlineMessageHandler == null) {
            error("onPlayerProcessOfflineMsg offlineMessageParser is null", ptCode);
            return;
        }
        try {
            info("onPlayerProcessOfflineMsg before handle", ptCode);
            offlineMessageHandler.handle(this, byteString);
        } catch (Exception e) {
            error("onPlayerProcessOfflineMsg error", ptCode);
            error("onPlayerProcessOfflineMsg", e);
        }
    }

    /**
     * 玩家登录之后
     */
    public void onPlayerAfterLogin() {
        info("onPlayerAfterLogin");
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            try {
                list[i].onPlayerAfterLogin();
            } catch (Exception e) {
                error("onPlayerAfterLogin", e);
            }
        }


        location.setState(Location.LOGIN_SUCCESS);
        onStateChange();

        //TODO 是否需要setChange都设置为true来同步login中的数据更改
    }

    /**
     * 玩家删除角色
     */
    public void onPlayerDeleteRole() {
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            list[i].onPlayerDeleteRole();
        }
    }

    /**
     * 玩家断线
     */
    public void offline() {
        info("onPlayerOffline");
        //离线重连中
        location.setState(Location.OFFLINE_ING);
        //设置超时时间
        logoutEndTime = SystemTimeTool.getMillTime() + 30000;
        onStateChange();

        onPlayerOffline();
    }

    private void onPlayerOffline() {

        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            list[i].onPlayerOffline();
        }
    }

    /**
     * 玩家重连登陆
     */
    public void reconnect(PlayerSession playerSession) {

        playerSession.setPlayer(this);
        PlayerSession oldSession = session;
        this.session = playerSession;
        session.copy(oldSession);

        int delta = session.getSendCacheDelta(playerSession.getClientReceivedIndex());

        PbCommon.S2CReconnectResult.Builder builder = PbCommon.S2CReconnectResult.newBuilder();
        if (delta < 0) {
            builder.setIsSuccess(false);
            session.send(PtCode.S2CReconnectResult, builder.build());
            error("reconnect invalid sendCache", playerSession.getClientReceivedIndex(), playerSession.getServerSendSerialNum(), NetUtils.getIP(session.getChannel()));
            session.close();
            logicChildWorld.getPlayerManager().removePlayer(playerID);
            return;
        }
        //发送重连期间客户端未接收到的数据
        if (delta > 0) {
            session.processSendCache(delta, messageV3 -> session.getChannel().writeAndFlush(messageV3));
        }
        builder.setIsSuccess(true);
        //client need reset clientReceivedIndex
        session.send(PtCode.S2CReconnectResult, builder.build());
        //server reset sendIndex
        session.resetSendIndex();
        onPlayerReconnectLogin();
        //重置登出时间
        logoutEndTime = 0;
    }

    /**
     * 是否触发登出行为
     *
     * @return
     */
    public boolean isTriggerLogout() {
        return logoutEndTime > 0 && SystemTimeTool.getMillTime() > logoutEndTime;
    }


    private void onPlayerReconnectLogin() {
        info("onPlayerReconnectLogin");

        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            list[i].onPlayerReconnectLogin();
        }
//        if (playerEntity != null && playerEntity.scene != null) {
//            playerEntity.scene.onPlayerReconnectLogin(playerEntity);
//        }
        location.setState(Location.LOGIN_SUCCESS);
        onStateChange();
    }

    /**
     * 玩家登出
     */
    public void logout() {
        location.setState(Location.LOGOUT);
        logoutEndTime = 0;
        onStateChange();
        onPlayerLogout();
        saveToMysql();
    }

    private void onPlayerLogout() {
        info("onPlayerLogout");
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            list[i].onPlayerLogout();
        }

    }

    /**
     * 状态信息变化
     */
    private void onStateChange() {
        info("onStateChange", location.getState());
        pushPlayerLocation2LoadBalance();
        syncRedisPlayerInfo();
    }

    /**
     * 同步redis信息
     */
    private void syncRedisPlayerInfo() {
//        Request request = Request.cmd(Command.HMSET).arg(RedisKey.PLAYER_LITE_INFO.value() + playerID).arg(PlayerHashKey.gameServerID).arg(ConfigConst.identifier)
//                .arg(PlayerHashKey.state).arg(location.getState());
//        RedisClientTool.send(getChildWorldIndex(), request, (isSuccess, response, errorMsg) -> {
//            if (!isSuccess) {
//                error("syncRedisPlayerInfo fail msg={}", errorMsg);
//            }
//        });
    }

    /**
     * 玩家数据缓存redis
     */
    public void saveToRedis() {
        ++versionCode;
        info("saveToRedis", versionCode);
        Request request = Request.cmd(Command.HMSET).arg(RedisKey.PLAYER_CACHE_DATA.value() + playerID)
                .arg("versionCode").arg(versionCode);

        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            DataBaseAccess dataBaseAccesses = list[i].getData();
            if (dataBaseAccesses != null) {
                Buffer buffer = Buffer.buffer(dataBaseAccesses.save().toByteArray());
                request.arg(dataBaseAccesses.getName()).arg(buffer);
            }
        }
        RedisClientTool.send(getChildWorldIndex(), request, (isSuccess, response, errorMsg) -> {
            if (isSuccess) {
                info("saveToRedis success", versionCode);
            } else {
                error("saveToRedis fail", versionCode, errorMsg);
            }
        });
    }

    /**
     * 保存变化的数据到redis
     */
    private void saveChangedDataToRedis(Consumer<Boolean> consumer) {
        Request request = Request.cmd(Command.HMSET).arg(RedisKey.PLAYER_CACHE_DATA.value() + playerID);
        BaseModule[] list = moduleList.getValues();
        ObjectList<String> changedList = new ObjectList<>();
        boolean changed = false;
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            DataBaseAccess dataBaseAccesses = list[i].getData();
            if (dataBaseAccesses != null && dataBaseAccesses.isChange()) {
                Buffer buffer = Buffer.buffer(dataBaseAccesses.save().toByteArray());
                request.arg(dataBaseAccesses.getName()).arg(buffer);
                dataBaseAccesses.setChange(false);
                changed = true;
                changedList.add(dataBaseAccesses.getName());
            }
        }
        if (changed) {
            ++versionCode;
            request.arg("versionCode").arg(versionCode);
            info("saveChangedDataToRedis", versionCode, changedList);
            RedisClientTool.send(getChildWorldIndex(), request, (isSuccess, response, errorMsg) -> {
                if (isSuccess) {
                    info("saveChangedDataToRedis success", versionCode);
                    if (consumer != null) {
                        consumer.accept(true);
                    }
                } else {
                    error("saveChangedDataToRedis fail", versionCode, errorMsg);
                    if (consumer != null) {
                        consumer.accept(false);
                    }
                }
            });
        } else {
            if (consumer != null) {
                Request ping = Request.cmd(Command.PING);
                RedisClientTool.send(getChildWorldIndex(), ping, (isSuccess, response, errorMsg) -> {
                    consumer.accept(isSuccess);
                });
            }
        }
    }


    /**
     * 玩家数据存库
     */
    private void saveToMysql() {
        Consumer<Boolean> consumer = (isRedisSuccess) -> {
            ++versionCode;
            info("saveToMysql", versionCode);
            List<Object> dataList = new ArrayList<>();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("UPDATE player SET `versionCode`=?,");
            dataList.add(versionCode);

            BaseModule[] list = moduleList.getValues();
            for (int i = 0, len = moduleList.size(); i < len; i++) {
                DataBaseAccess dataBaseAccesses = list[i].getData();
                if (dataBaseAccesses != null) {
                    stringBuilder.append("`").append(dataBaseAccesses.getName()).append("`=?,");
                    Buffer buffer = Buffer.buffer(dataBaseAccesses.save().toByteArray());
                    dataList.add(buffer);
                }
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            stringBuilder.append(" WHERE player_id=").append(playerID).append(";");


            MySQLUtils.sendChild(logicChildWorld.getConnectionPool(this), stringBuilder.toString(), Tuple.wrap(dataList), getChildWorldIndex(), (isSuccess, rows, errorMsg) -> {
                if (isSuccess) {
                    info("saveToMysql success", accountID, versionCode);
                    if (isRedisSuccess) {
                        //redis和mysql都落地成功后，设置redis过期时间,自增redisVersionCode
                        ++versionCode;
                        Request request = Request.cmd(Command.HSET).arg(RedisKey.PLAYER_CACHE_DATA.value() + playerID)
                                .arg("versionCode").arg(versionCode);
                        RedisClientTool.send(getChildWorldIndex(), request, (isSuccess1, response, errorMsg1) -> {
                            if (!isSuccess1) {
                                error("saveToMysql redis update fail", versionCode, errorMsg1);
                            }
                        });

                        Request expireRequest = Request.cmd(Command.EXPIRE).arg(RedisKey.PLAYER_CACHE_DATA.value() + playerID).arg(60);
                        RedisClientTool.send(getChildWorldIndex(), expireRequest, (isSuccess1, response, errorMsg1) -> {
                            if (!isSuccess1) {
                                error("saveToMysql redis expire fail", versionCode, errorMsg1);
                            }
                        });

                    } else {
                        //redis落地失败，mysql落地成功，此时应该redisVersionCode<mysqlVersionCode
                        error("saveToMysql redis fail mysql success", versionCode);
                    }
                } else {
                    if (isRedisSuccess) {
                        //redis落地成功，mysql落地失败,此时应该redisVersionCode>mysqlVersionCode,且redis缓存无过期时间
                        error("saveToMysql redis success mysql fail", versionCode, errorMsg);
                    } else {
                        //redis和mysql都落地失败后，内存数据存储到本机文件中,手动修复数据
                        error("saveToMysql redis fail mysql fail", versionCode, errorMsg);
                        saveToFile();
                    }
                }
            });
        };

        boolean isTest = false;
        if (isTest) {
            //测试服不落地mysql，防止有的data没有setChange导致数据回档
            saveChangedDataToRedis(null);
        } else {
            saveChangedDataToRedis(consumer);
        }
    }

    public void saveToFile() {
        String fileName = Player.class.getResource("/").getPath();
        fileName += playerID + "_" + SystemTimeTool.getMillTime() + ".bin";
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(fileName);
            BaseModule[] list = moduleList.getValues();
            for (int i = 0, len = moduleList.size(); i < len; i++) {
                DataBaseAccess dataBaseAccesses = list[i].getData();
                if (dataBaseAccesses != null) {
                    fileOutputStream.write(dataBaseAccesses.getName().getBytes());
                    fileOutputStream.write("=".getBytes());
                    fileOutputStream.write(ByteBufUtil.hexDump(dataBaseAccesses.save().toByteArray()).getBytes());
                    fileOutputStream.write("\r\n".getBytes());
                }
            }
            fileOutputStream.close();
        } catch (Exception e) {
            error("saveToFile", e);
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    error("saveToFile close", e);
                }
            }
        }
    }

    public void parseBinFile(String fileName) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(fileName);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] array = line.split("=");
                if (array.length == 2) {
                    String name = array[0];
                    byte[] bytes = ByteUtils.hexStringToByteArray(array[1]);
                    BaseModule[] list = moduleList.getValues();
                    for (int i = 0, len = moduleList.size(); i < len; i++) {
                        DataBaseAccess dataBaseAccesses = list[i].getData();
                        if (dataBaseAccesses != null && dataBaseAccesses.getName().equals(name)) {
                            GeneratedMessageV3 generatedMessageV3 = dataBaseAccesses.load(bytes);
                            JsonFormat jsonFormat = new JsonFormat();
                            String content = jsonFormat.printToString(generatedMessageV3);
                            info("parseBinFile", array[0], content);
                        }
                    }
                }

                line = bufferedReader.readLine();
            }
            inputStreamReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * 发送消息码
     *
     * @param infoCode
     */
    public void sendInfoCode(int infoCode) {
        PbCommon.S2CInfoCode.Builder builder = PbCommon.S2CInfoCode.newBuilder();
        builder.setInfoCode(infoCode);
        send(PtCode.S2CInfoCode, builder.build());
    }

    /**
     * 发送消息码
     *
     * @param infoCode
     * @param args
     */
    public void sendInfoCode(int infoCode, String... args) {
        PbCommon.S2CInfoCode.Builder builder = PbCommon.S2CInfoCode.newBuilder();
        builder.setInfoCode(infoCode);
        for (String arg : args) {
            builder.addArgs(arg);
        }
        send(PtCode.S2CInfoCode, builder.build());
    }

    /**
     * 玩家每帧调用
     *
     * @param escapedMillTime
     */
    public void onPlayerTick(int escapedMillTime) {
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            list[i].onPlayerTick(escapedMillTime);
        }
    }

    /**
     * 玩家每秒调用
     */
    public void onPlayerSecond() {
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            list[i].onPlayerSecond();
        }
    }

    /**
     * 玩家每分钟调用
     */
    public void onPlayerMinute() {
        BaseModule[] list = moduleList.getValues();
        for (int i = 0, len = moduleList.size(); i < len; i++) {
            list[i].onPlayerMinute();
        }
        saveChangedDataToRedis(null);
    }

    /**
     * 玩家跨天
     *
     * @param isLogin 是否登陆跨天
     */
    public void onPlayerDaily(boolean isLogin) {
        try {
            BaseModule[] list = moduleList.getValues();
            for (int i = 0, len = moduleList.size(); i < len; i++) {
                list[i].onPlayerDaily(isLogin);
            }
        } catch (Exception e) {
            error("onPlayerDaily", e);
        }
    }

    /**
     * 各个模块触发玩家行为
     *
     * @param actions
     * @param isAdd          增加或者删除
     * @param behaviorSource
     */
    public void triggerPlayerAction(int[] actions, boolean isAdd, int behaviorSource) {
        int actionType = actions[0];
//        switch (actionType) {
//            case PlayerActionConst.AddAttribute: {
//                if (isAdd) {
//                    playerEntity.addAttribute((short) actions[1], actions[2]);
//                } else {
//                    playerEntity.subAttribute((short) actions[1], actions[2]);
//                }
//                break;
//            }
//            case PlayerActionConst.AvatarChangePart: {
//                if (isAdd) {
//                    playerEntity.fightAllLogic.avatarFightLogic.changeAvatarPart(actions[1], actions[2]);
//                } else {
//                    playerEntity.fightAllLogic.avatarFightLogic.changeAvatarPart(actions[1], 0);
//                }
//                break;
//            }
//            default: {
//                SceneLoggerTool.gameLogger.error("triggerPlayerAction action={}, isAdd={} behaviorSource={}  actionType dont exist", actions, isAdd, behaviorSource);
//                break;
//            }
//        }
    }

    /**
     * 检测角色条件
     *
     * @param condition
     * @return
     */
    public boolean checkRoleCondition(int[] condition) {
        int conditionType = condition[0];
        switch (conditionType) {
            case PlayerConditionConst.Level -> {
                int level = condition[1];
                if (roleModule.getLevel() >= level) {
                    return true;
                }
            }
            default -> error("checkRoleCondition conditionType dont exist", conditionType);
        }
        return false;
    }

    /**
     * 检测是否符合全部条件
     *
     * @param conditions
     * @return
     */
    public boolean checkAllRoleCondition(int[][] conditions) {
        //如果没有条件默认return true
        if (conditions == null || conditions.length == 0) {
            return true;
        }
        for (int[] condition : conditions) {
            if (!checkRoleCondition(condition)) {
                return false;
            }
        }
        return true;
    }

    public void debug(String key, Object... objects) {
        LogicLoggerTool.gameLogger.debug("playerID={}, key={}, {}", playerID, key, objects);
    }

    public void info(String key, Object... objects) {
        LogicLoggerTool.gameLogger.info("playerID={}, key={}, {}", playerID, key, objects);
    }

    public void warn(String key, Object... objects) {
        LogicLoggerTool.gameLogger.warn("playerID={}, key={}, {}", playerID, key, objects);
    }

    public void error(String key, Object... objects) {
        LogicLoggerTool.gameLogger.error("playerID={}, key={}, {}", playerID, key, objects);
    }

    public void error(String key, Throwable throwable) {
        LogicLoggerTool.gameLogger.error(key + " playerID=" + playerID, throwable);
    }


}
