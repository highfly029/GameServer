package com.highfly029.logic.module.union;

import com.google.protobuf.ByteString;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.logic.module.IOfflineMessageHandler;
import com.highfly029.logic.module.Player;

/**
 * @ClassName UnionOfflineMessageParser
 * @Description UnionOfflineMessageParser
 * @Author liyunpeng
 **/
public class UnionOfflineMessageTestParser implements IOfflineMessageHandler {
    @Override
    public int registerPtCode() {
        return PtCode.Center2LogicModifyUnionName;
    }

    @Override
    public void handle(Player player, ByteString byteString) throws Exception {
        PbUnion.Center2LogicModifyUnionName req = PbUnion.Center2LogicModifyUnionName.parseFrom(byteString.toByteArray());
        player.getUnionModule().onModifyUnionNameResult(req.getResult());
    }
}
