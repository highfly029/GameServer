package com.highfly029.logic.module.role;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.protocol.packet.PbRole;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.logic.module.PlayerDataBaseAccess;

/**
 * @ClassName PlayerRoleData
 * @Description 玩家角色数据
 * @Author liyunpeng
 **/
public class PlayerRoleData extends PlayerDataBaseAccess {
    private int level;
    private int exp;
    private int lastSceneID;
    private int lastLineID;
    private Vector3 lastPos = Vector3.create();
    private Vector3 lastDir = Vector3.create();
    private long nextDailyTime;
    private long createTime;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getLastSceneID() {
        return lastSceneID;
    }

    public void setLastSceneID(int lastSceneID) {
        this.lastSceneID = lastSceneID;
    }

    public int getLastLineID() {
        return lastLineID;
    }

    public void setLastLineID(int lastLineID) {
        this.lastLineID = lastLineID;
    }

    public Vector3 getLastPos() {
        return lastPos;
    }

    public void setLastPos(Vector3 lastPos) {
        this.lastPos = lastPos;
    }

    public Vector3 getLastDir() {
        return lastDir;
    }

    public void setLastDir(Vector3 lastDir) {
        this.lastDir = lastDir;
    }

    public long getNextDailyTime() {
        return nextDailyTime;
    }

    public void setNextDailyTime(long nextDailyTime) {
        this.nextDailyTime = nextDailyTime;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbRole.PbPlayerRoleData pbPlayerRoleData = PbRole.PbPlayerRoleData.parseFrom(data);
        level = pbPlayerRoleData.getLevel();
        exp = pbPlayerRoleData.getExp();
        lastSceneID = pbPlayerRoleData.getLastSceneID();
        lastLineID = pbPlayerRoleData.getLastLineID();
        lastPos = PbCommonUtils.vector3Pb2Obj(pbPlayerRoleData.getLastPos());
        lastDir = PbCommonUtils.vector3Pb2Obj(pbPlayerRoleData.getLastDir());
        nextDailyTime = pbPlayerRoleData.getNextDailyTime();
        createTime = pbPlayerRoleData.getCreateTime();
        return pbPlayerRoleData;
    }

    @Override
    public GeneratedMessageV3 save() {
        PbRole.PbPlayerRoleData.Builder builder = PbRole.PbPlayerRoleData.newBuilder();
        builder.setLevel(level);
        builder.setExp(exp);
        builder.setLastSceneID(lastSceneID);
        builder.setLastLineID(lastLineID);
        builder.setLastPos(PbCommonUtils.vector3Obj2Pb(lastPos));
        builder.setLastDir(PbCommonUtils.vector3Obj2Pb(lastDir));
        builder.setNextDailyTime(nextDailyTime);
        builder.setCreateTime(createTime);
        return builder.build();
    }

    @Override
    public String getName() {
        return "role";
    }
}
