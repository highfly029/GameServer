package com.highfly029.logic.module.activity.impl;

import com.highfly029.common.protocol.packet.PbActivity;
import com.highfly029.common.template.activity.ActivityTemplate;
import com.highfly029.logic.module.activity.BaseActivity;
import com.highfly029.logic.module.Player;

/**
 * @ClassName Test2
 * @Description Test2
 * @Author liyunpeng
 **/
public class Test2 extends BaseActivity {
    public Test2(Player player, ActivityTemplate activityTemplate) {
        super(player, activityTemplate);
    }

    @Override
    public PbActivity.PbActivityData activityDataToPb() {
        PbActivity.PbActivityData.Builder builder = PbActivity.PbActivityData.newBuilder();
        builder.setId(getId());
        return builder.build();
    }

    @Override
    public void activityDataInitFromPb(PbActivity.PbActivityData pbActivityData) {

    }

    @Override
    public void onOpen() {
        super.onOpen();
        player.info("test2 onOpen", getId());
    }

    @Override
    public void onClose(boolean forceClose) {
        super.onClose(forceClose);
        player.info("test2 onClose", getId(), forceClose);
    }

    @Override
    public void onPlayerBeforeLogin() {
        super.onPlayerBeforeLogin();
        player.info("test2 onPlayerBeforeLogin", getId());
    }

    @Override
    public void onDaily(boolean isLogin) {
        super.onDaily(isLogin);
        player.info("test2 onDaily", getId());
    }
}
