package com.highfly029.logic.module.union;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.union.UnionLite;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.core.interfaces.IRegisterServerChildProtocol;
import com.highfly029.core.session.Session;
import com.highfly029.core.world.ChildWorld;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName UnionChildProtocol
 * @Description UnionChildProtocol
 * @Author liyunpeng
 **/
public class UnionChildProtocol implements IRegisterServerChildProtocol {
    private LogicChildWorld logicChildWorld;

    @Override
    public void registerChildProtocol(ChildWorld childWorld) throws Exception {
        this.logicChildWorld = (LogicChildWorld) childWorld;

        childWorld.registerServerChildProtocol(PtCode.Center2LogicGetUnionList, this::getUnionListResult);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicCreateUnion, this::createUnionResult);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicSyncPlayerUnionInfo, this::syncPlayerUnionInfoResult);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicQuitUnion, this::quitUnionResult);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicDisbandUnion, this::disbandUnionResult);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicApplyJoinUnion, this::applyJoinUnionResult);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicInviteJoinUnion, this::inviteJoinUnionNotice);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicHandleInviteJoinUnion, this::onHandleBeInviteJoinUnionResult);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicHandleJoinUnion, this::handleJoinUnionResult);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicKickOutUnionMember, this::kickOutMemberResult);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicChangeLeader, this::changeLeaderResult);
        childWorld.registerServerChildProtocol(PtCode.Center2LogicModifyUnionName, this::modifyUnionNameResult);
    }

    /**
     * 获取联盟列表
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void getUnionListResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Center2LogicGetUnionList req = (PbUnion.Center2LogicGetUnionList) message;
        Player player = logicChildWorld.getPlayerManager().getPlayer(req.getPlayerID());
        PbUnion.S2CGetUnionList.Builder builder = PbUnion.S2CGetUnionList.newBuilder();
        builder.addAllUnionList(req.getUnionListList());
        player.send(PtCode.S2CGetUnionList, builder.build());


        //print
        int num = req.getUnionListCount();
        for (int i = 0; i < num; i++) {
            PbUnion.PbUnionLiteData pbUnionLiteData = req.getUnionList(i);
            UnionLite unionLite = new UnionLite();
            try {
                unionLite.load(pbUnionLiteData.toByteArray());
            } catch (InvalidProtocolBufferException e) {
                player.error("getUnionListResult", e);
            }
            player.info("getUnionListResult", i, unionLite);
        }
    }

    /**
     * 创建联盟结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void createUnionResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Center2LogicCreateUnion req = (PbUnion.Center2LogicCreateUnion) message;
        long createPlayerID = req.getCreatePlayerID();
        boolean result = req.getResult();
        long unionID = req.getUnionID();
        String unionName = req.getName();

        Player player = logicChildWorld.getPlayerManager().getPlayer(createPlayerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("createUnionResult player is null createPlayerID={}, unionID={}, result={}",
                    createPlayerID, unionID, result);
            return;
        }
        player.info("createUnionResult", result, unionID, unionName);

        player.getUnionModule().onCreateUnionResult(result, unionID, unionName, req.getUnionData(), false);
    }

    /**
     * 同步联盟信息的结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void syncPlayerUnionInfoResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Center2LogicSyncPlayerUnionInfo req = (PbUnion.Center2LogicSyncPlayerUnionInfo) message;
        long unionID = req.getUnionID();
        long playerID = req.getPlayerID();
        String name = req.getName();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("syncPlayerUnionInfoResult player is null playerID={}, unionID={}", playerID, unionID);
            return;
        }
        //没找到联盟
        if (unionID == 0) {
            player.getUnionModule().getData().setUnionID(0);
            player.getUnionModule().getData().setChange(true);
            player.getUnionModule().onPlayerLiteUnionDataChange();
        } else {
            player.getUnionModule().onSyncPlayerUnionInfo(unionID, name, req.getUnionData(), req.getIsPull());
        }
        player.info("syncPlayerUnionInfoResult", unionID, name);
    }

    /**
     * 退出联盟结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void quitUnionResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Center2LogicQuitUnion req = (PbUnion.Center2LogicQuitUnion) message;
        boolean result = req.getResult();
        long unionID = req.getUnionID();
        long playerID = req.getPlayerID();
        int errorCode = req.getErrorCode();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("quitUnionResult player is null playerID={}, unionID={}, result={}", playerID, unionID, result);
            return;
        }

        player.getUnionModule().quitUnionResult(result, unionID, errorCode);
    }

    /**
     * 解散联盟结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void disbandUnionResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Center2LogicDisbandUnion req = (PbUnion.Center2LogicDisbandUnion) message;
        long playerID = req.getPlayerID();
        long unionID = req.getUnionID();
        boolean result = req.getResult();

        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("disbandUnionResult player is null playerID={}, unionID={}", playerID, unionID);
            return;
        }
        if (result) {
            player.getUnionModule().getData().setUnionID(0);
            player.getUnionModule().getData().setChange(true);
        }

        PbUnion.S2CDisbandUnion.Builder builder = PbUnion.S2CDisbandUnion.newBuilder();
        builder.setResult(result);
        builder.setErrorCode(req.getErrorCode());
        player.send(PtCode.S2CDisbandUnion, builder.build());
    }

    /**
     * 申请加入联盟结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void applyJoinUnionResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Center2LogicApplyJoinUnion req = (PbUnion.Center2LogicApplyJoinUnion) message;

        long playerID = req.getPlayerID();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("applyJoinUnionResult playerID={} is null", playerID);
            return;
        }
        PbUnion.S2CApplyJoinUnion.Builder builder = PbUnion.S2CApplyJoinUnion.newBuilder();
        builder.setResult(req.getResult());
        builder.setErrorCode(req.getErrorCode());
        player.send(PtCode.S2CApplyJoinUnion, builder.build());
    }

    /**
     * 邀请加入联盟通知
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void inviteJoinUnionNotice(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Center2LogicInviteJoinUnion req = (PbUnion.Center2LogicInviteJoinUnion) message;

        long playerID = req.getTargetPlayerID();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("inviteJoinUnionResult playerID={} is null", playerID);
            return;
        }
        player.getUnionModule().onInviteJoinUnionNotice(req.getOperatorPlayerID(), req.getUnionID(), req.getName());
    }

    /**
     * 处理入盟消息结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void handleJoinUnionResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Center2LogicHandleJoinUnion req = (PbUnion.Center2LogicHandleJoinUnion) message;
        long playerID = req.getPlayerID();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("handleJoinUnionResult playerID={} is null", playerID);
            return;
        }
        PbUnion.S2CHandleJoinUnion.Builder builder = PbUnion.S2CHandleJoinUnion.newBuilder();
        builder.setResult(req.getResult());
        builder.setErrorCode(req.getErrorCode());
        player.send(PtCode.S2CApplyJoinUnion, builder.build());
    }

    /**
     * 玩家响应邀请加入联盟结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void onHandleBeInviteJoinUnionResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Center2LogicHandleInviteJoinUnion req = (PbUnion.Center2LogicHandleInviteJoinUnion) message;
        long playerID = req.getPlayerID();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("onHandleBeInviteJoinUnionResult playerID={} is null", playerID);
            return;
        }

        player.getUnionModule().onHandleBeInviteJoinUnionResult(req.getUnionID(), req.getResult());
    }


    /**
     * 踢出成员结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void kickOutMemberResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Center2LogicKickOutUnionMember req = (PbUnion.Center2LogicKickOutUnionMember) message;
        boolean result = req.getResult();
        int errorCode = req.getErrorCode();
        long playerID = req.getPlayerID();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("kickOutMemberResult playerID={} is null", playerID);
            return;
        }
        PbUnion.S2CKickOutUnionMember.Builder builder = PbUnion.S2CKickOutUnionMember.newBuilder();
        builder.setResult(result).setErrorCode(errorCode);
        player.send(PtCode.S2CKickOutUnionMember, builder.build());
    }

    /**
     * 更换盟主结果
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void changeLeaderResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Center2LogicChangeLeader req = (PbUnion.Center2LogicChangeLeader) message;
        long playerID = req.getPlayerID();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("changeLeaderResult playerID={} is null", playerID);
            return;
        }
        PbUnion.S2CChangeLeader.Builder builder = PbUnion.S2CChangeLeader.newBuilder();
        builder.setResult(req.getResult()).setErrorCode(req.getErrorCode());
        player.send(PtCode.S2CChangeLeader, builder.build());
    }


    /**
     * 修改联盟名字
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void modifyUnionNameResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbUnion.Center2LogicModifyUnionName req = (PbUnion.Center2LogicModifyUnionName) message;
        long playerID = req.getPlayerID();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("modifyUnionNameResult playerID={} is null", playerID);
            return;
        }
        player.getUnionModule().onModifyUnionNameResult(req.getResult());
    }

}
