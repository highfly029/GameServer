package com.highfly029.logic.module.bag;

import com.highfly029.common.template.item.ItemTemplate;
import com.highfly029.common.template.item.ItemTypeConst;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.logic.exception.StartUpException;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName BagConfig
 * @Description BagConfig
 * @Author liyunpeng
 **/
public class BagConfig extends BaseGlobalConfig {
    //itemID -> ItemTemplate
    private final IntObjectMap<ItemTemplate> itemTemplates = new IntObjectMap<>(ItemTemplate[]::new);

    @Override
    public void check(boolean isHotfix) {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(ItemTemplate.class);
        if (list == null) {
            return;
        }
        ItemTemplate template;
        for (int i = 0, size = list.size(); i < size; i++) {
            template = (ItemTemplate) list.get(i);
            //检测单个物品堆叠必须大于0
            if (template.getPileMax() <= 0) {
                throw new StartUpException("item template id=" + template.getItemID() + " pileMax invalid =" + template.getPileMax());
            }
            //validTime > 0的物品 堆叠必须==1
            if (template.getValidTime() > 0 && template.getPileMax() != 1) {
                throw new StartUpException("item template id=" + template.getItemID() + " pileMax invalid =" + template.getPileMax() + " validTime=" + template.getValidTime());
            }
            //检测 装备物品 堆叠必须==1
            if (template.getType() == ItemTypeConst.Equipment && template.getPileMax() != 1) {
                throw new StartUpException("item template id=" + template.getItemID() + " pileMax invalid =" + template.getPileMax() + " type=equip");
            }
        }
    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(ItemTemplate.class);
        if (list != null && !list.isEmpty()) {
            ItemTemplate template;
            for (int i = 0, size = list.size(); i < size; i++) {
                template = (ItemTemplate) list.get(i);
                itemTemplates.put(template.getItemID(), template);
            }
        }
    }

    /**
     * 获取物品模板
     *
     * @param itemID
     * @return
     */
    public ItemTemplate getItemTemplate(int itemID) {
        return itemTemplates.get(itemID);
    }
}
