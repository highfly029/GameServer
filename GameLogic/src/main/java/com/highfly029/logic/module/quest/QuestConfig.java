package com.highfly029.logic.module.quest;

import com.highfly029.common.template.quest.QuestTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName QuestConfig
 * @Description QuestConfig
 * @Author liyunpeng
 **/
public class QuestConfig extends BaseGlobalConfig {
    private final IntObjectMap<QuestTemplate> templates = new IntObjectMap<>(QuestTemplate[]::new);

    /**
     * 任务的后续任务集合
     */
    private final LongObjectMap<IntSet> afterQuestIDMap = new LongObjectMap<>(IntSet[]::new);

    @Override
    public void check(boolean isHotfix) {
        //TODO 如果是服务器自动接取的任务 保证一定会接取成功
    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(QuestTemplate.class);
        QuestTemplate template;
        for (int i = 0, size = list.size(); i < size; i++) {
            template = (QuestTemplate) list.get(i);
            templates.put(template.getQuestID(), template);
            //有前置任务
            if (template.getPreQuests() != null) {
                for (int[] preQuest : template.getPreQuests()) {
                    int preQuestType = preQuest[0];
                    int preQuestID = preQuest[1];
                    IntSet intSet = getAfterQuestIDSet(preQuestType, preQuestID);
                    if (intSet == null) {
                        intSet = new IntSet();
                        afterQuestIDMap.put(MathUtils.getCompositeIndex(preQuestType, preQuestID), intSet);
                    }
                    intSet.add(template.getQuestID());
                }
            }
        }
    }

    /**
     * 获取后置任务集合
     *
     * @param questType
     * @param questID
     * @return
     */
    public IntSet getAfterQuestIDSet(int questType, int questID) {
        return afterQuestIDMap.get(MathUtils.getCompositeIndex(questType, questID));
    }

    /**
     * 获取任务模板
     *
     * @param questID
     * @return
     */
    public QuestTemplate getQuestTemplate(int questID) {
        return templates.get(questID);
    }

    public IntObjectMap<QuestTemplate> getTemplates() {
        return templates;
    }
}
