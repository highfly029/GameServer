package com.highfly029.logic.module.auction;

import com.google.protobuf.ByteString;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbAuction;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.logic.module.IOfflineMessageHandler;
import com.highfly029.logic.module.Player;

/**
 * @ClassName AuctionExpireCancelOfflineMessage
 * @Description AuctionExpireCancelOfflineMessage
 * @Author liyunpeng
 **/
public class AuctionExpireCancelOfflineMessage implements IOfflineMessageHandler {
    @Override
    public int registerPtCode() {
        return PtCode.Center2LogicAuctionExpireCancel;
    }

    @Override
    public void handle(Player player, ByteString byteString) throws Exception {
        PbAuction.Center2LogicAuctionExpireCancel req = PbAuction.Center2LogicAuctionExpireCancel.parseFrom(byteString);
        player.getAuctionModule().onAuctionExpireCancel(PbCommonUtils.auctionItemDataPb2Obj(req.getAuctionItemData()));
    }
}
