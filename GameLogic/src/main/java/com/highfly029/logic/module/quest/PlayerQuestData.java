package com.highfly029.logic.module.quest;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.DataBaseAccess;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbQuest;
import com.highfly029.logic.module.PlayerDataBaseAccess;
import com.highfly029.logic.module.goal.GoalData;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.logic.utils.PbLogicUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName PlayerQuestData
 * @Description 玩家任务数据
 * @Author liyunpeng
 **/
public class PlayerQuestData extends PlayerDataBaseAccess {
    /**
     * 所有的任务数据 questType -> QuestTypeDataMap
     */
    private IntObjectMap<QuestTypeDataMap> allQuest = new IntObjectMap<>(QuestTypeDataMap[]::new);

    public IntObjectMap<QuestTypeDataMap> getAllQuest() {
        return allQuest;
    }

    @Override
    public GeneratedMessageV3 load(byte[] data) throws InvalidProtocolBufferException {
        PbQuest.PbPlayerQuestData pbPlayerQuestData = PbQuest.PbPlayerQuestData.parseFrom(data);
        int num = pbPlayerQuestData.getQuestDatasCount();
        if (num > 0) {
            QuestConfig questConfig = ConfigTool.getQuestConfig();
            for (int i = 0; i < num; i++) {
                PbQuest.PbQuestTypeData pbQuestTypeData = pbPlayerQuestData.getQuestDatas(i);
                int questType = pbQuestTypeData.getQuestType();
                QuestTypeDataMap questTypeDataMap = new QuestTypeDataMap();
                for (int j = 0, jLen = pbQuestTypeData.getCompleteQuestsCount(); j < jLen; j++) {
                    questTypeDataMap.getCompleteQuestIDSet().add(pbQuestTypeData.getCompleteQuests(j));
                }

                for (int j = 0, jLen = pbQuestTypeData.getAcceptQuestsCount(); j < jLen; j++) {
                    PbQuest.PbQuestData pbQuestData = pbQuestTypeData.getAcceptQuests(j);
                    QuestData questData = QuestData.create();
                    questData.setQuestID(pbQuestData.getQuestID());
                    questData.setQuestTemplate(questConfig.getQuestTemplate(questData.getQuestID()));
                    questData.setInvalidTime(pbQuestData.getInvalidTime());
                    int goalNum = pbQuestData.getGoalsCount();
                    if (goalNum > 0) {
                        GoalData[] goals = new GoalData[goalNum];
                        for (int k = 0; k < goalNum; k++) {
                            PbCommon.PbGoalData pbGoalData = pbQuestData.getGoals(k);
                            GoalData goalData = PbLogicUtils.goalPb2Obj(pbGoalData);
                            goals[k] = goalData;
                        }
                        questData.setGoals(goals);
                    }
                    questTypeDataMap.getAcceptQuestMap().put(questData.getQuestID(), questData);
                }
                allQuest.put(questType, questTypeDataMap);
            }
        }
        return pbPlayerQuestData;
    }

    @Override
    public GeneratedMessageV3 save() {
        PbQuest.PbPlayerQuestData.Builder builder = PbQuest.PbPlayerQuestData.newBuilder();
        int freeValue = allQuest.getFreeValue();
        int[] keys = allQuest.getKeys();
        QuestTypeDataMap[] values = allQuest.getValues();
        int key;
        QuestTypeDataMap value;
        for (int i = 0; i < keys.length; i++) {
            if ((key = keys[i]) != freeValue) {
                if ((value = values[i]) != null) {
                    PbQuest.PbQuestTypeData.Builder pbQuestTypeDataBuilder = PbQuest.PbQuestTypeData.newBuilder();
                    pbQuestTypeDataBuilder.setQuestType(key);
                    if (value.getCompleteQuestIDSet().size() > 0) {
                        value.getCompleteQuestIDSet().foreachImmutable(completeQuestID -> {
                            pbQuestTypeDataBuilder.addCompleteQuests(completeQuestID);
                        });
                    }
                    if (value.getAcceptQuestMap().size() > 0) {
                        value.getAcceptQuestMap().foreachImmutable((questID, questData) -> {
                            PbQuest.PbQuestData pbQuestData = PbLogicUtils.questObj2Pb(questData);
                            pbQuestTypeDataBuilder.addAcceptQuests(pbQuestData);
                        });
                    }
                    builder.addQuestDatas(pbQuestTypeDataBuilder);
                }
            }
        }
        return builder.build();
    }

    @Override
    public String getName() {
        return "quest";
    }

    /**
     * 获取完成的任务集合
     *
     * @param questType
     * @return
     */
    public IntSet getCompleteQuestIDSet(int questType) {
        QuestTypeDataMap questTypeDataMap = allQuest.get(questType);
        if (questTypeDataMap == null) {
            return null;
        } else {
            return questTypeDataMap.getCompleteQuestIDSet();
        }
    }

    /**
     * 获取接取的任务集合
     *
     * @param questType
     * @return
     */
    public IntObjectMap getAcceptQuestMap(int questType) {
        QuestTypeDataMap questTypeDataMap = allQuest.get(questType);
        if (questTypeDataMap == null) {
            return null;
        } else {
            return questTypeDataMap.getAcceptQuestMap();
        }
    }

    /**
     * 增加接取的任务数据
     *
     * @param questType
     * @param questID
     * @param questData
     */
    public void addAcceptQuestData(int questType, int questID, QuestData questData) {
        QuestTypeDataMap questTypeDataMap = allQuest.get(questType);
        if (questTypeDataMap == null) {
            questTypeDataMap = new QuestTypeDataMap();
            allQuest.put(questType, questTypeDataMap);
        }

        questTypeDataMap.getAcceptQuestMap().put(questID, questData);
    }

    /**
     * 删除已接的任务
     *
     * @param questType
     * @param questID
     */
    public void removeAcceptQuestData(int questType, int questID) {
        QuestTypeDataMap questTypeDataMap = allQuest.get(questType);
        if (questTypeDataMap != null) {
            questTypeDataMap.getAcceptQuestMap().remove(questID);
        }
    }

    /**
     * 增加完成的任务记录
     *
     * @param questType
     * @param questID
     */
    public void addCompleteQuestID(int questType, int questID) {
        QuestTypeDataMap questTypeDataMap = allQuest.get(questType);
        if (questTypeDataMap == null) {
            questTypeDataMap = new QuestTypeDataMap();
            allQuest.put(questType, questTypeDataMap);
        }
        questTypeDataMap.getCompleteQuestIDSet().add(questID);
    }

}


