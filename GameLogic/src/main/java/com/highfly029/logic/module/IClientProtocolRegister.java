package com.highfly029.logic.module;

import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName IClientProtocolRegister
 * @Description 客户端协议注册
 * @Author liyunpeng
 **/
public interface IClientProtocolRegister {
    void registerClientProtocol(LogicChildWorld logicChildWorld) throws Exception;
}
