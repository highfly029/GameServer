package com.highfly029.logic.module.function;

import com.highfly029.common.template.function.FunctionTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName FunctionConfig
 * @Description FunctionConfig
 * @Author liyunpeng
 **/
public class FunctionConfig extends BaseGlobalConfig {
    private final IntObjectMap<FunctionTemplate> templates = new IntObjectMap<>(FunctionTemplate[]::new);

    @Override
    public void check(boolean isHotfix) {

    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(FunctionTemplate.class);
        FunctionTemplate template;
        for (int i = 0, size = list.size(); i < size; i++) {
            template = (FunctionTemplate) list.get(i);
            templates.put(template.getFunctionID(), template);
        }
    }

    public IntObjectMap<FunctionTemplate> getTemplates() {
        return templates;
    }
}
