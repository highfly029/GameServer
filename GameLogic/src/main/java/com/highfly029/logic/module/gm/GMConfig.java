package com.highfly029.logic.module.gm;

import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.collection.ObjObjMap;

/**
 * @ClassName GMConfig
 * @Description GMConfig
 * @Author liyunpeng
 **/
public class GMConfig extends BaseGlobalConfig {
    /**
     * gm命令集合
     */
    private final ObjObjMap<String, GMCmd> cmdMap = new ObjObjMap<>(String[]::new, GMCmd[]::new);
    /**
     * 帮助描述
     */
    private final StringBuilder helpDescStringBuilder = new StringBuilder();
    /**
     * 帮助描述
     */
    private String helpDesc;

    @Override
    public void check(boolean isHotfix) {
        //
    }

    @Override
    protected void dataProcess() {
        registerCmd();
    }

    private void registerCmd() {
        //如果不允许使用gm命令 则不注册
        if (!ConfigPropUtils.getBoolValue(ConfigConst.isOpenGM)) {
            return;
        }

        addCmd(GMModule::addAsset, "增加资产 assetType addNum", "addAsset");
        addCmd(GMModule::removeAsset, "减少资产 assetType removeNum", "removeAsset");
        addCmd(GMModule::addAssetMax, "增加资产上限 assetType addNum", "addAssetMax", "aam");
        addCmd(GMModule::removeAssetMax, "减少资产上限 assetType removeNum", "removeAssetMax");

        addCmd(GMModule::addItem, "增加物品 itemID itemNum [isBind]", "addItem");
        addCmd(GMModule::addItem2, "增加物品 itemID itemNum", "addItem2");
        addCmd(GMModule::addItem3, "增加物品 itemID itemNum", "addItem3");
        addCmd(GMModule::useItemByIndex, "使用物品 index num", "useItemByIndex");
        addCmd(GMModule::removeItemByIndex, "删除物品 index num", "removeItemByIndex");
        addCmd(GMModule::swapItemByIndex, "交换物品 fromIndex, toIndex", "swapItemByIndex");
        addCmd(GMModule::splitItemByIndex, "拆分物品 index num", "splitItemByIndex");
        addCmd(GMModule::sortBag, "背包排序", "sort", "sortBag");
        addCmd(GMModule::resetGridNum, "修改背包格子 gridNum", "resetGridNum");
        addCmd(GMModule::clearBag, "背包清空", "clearBag");

        addCmd(GMModule::addBindAOI, "增加到绑定aoi playerID", "addBindAOI");
        addCmd(GMModule::removeBindAOI, "移除绑定aoi playerID", "removeBindAOI");
        addCmd(GMModule::teleport, "同场景瞬移 posX, posZ", "teleport");
        addCmd(GMModule::createEntity, "创建entity type, id, posX, posZ", "createEntity", "ce");
        addCmd(GMModule::deleteEntity, "删除entity entityID", "deleteEntity", "de");
        addCmd(GMModule::changeFixedPosEntityPos, "修改FixedPosEntity坐标 entityID, posX, posZ", "changeFixedPosEntityPos");

        addCmd(GMModule::addBuff, "增加buff buffID buffLevel", "addBuff", "ab");
        addCmd(GMModule::removeBuff, "删除buff buffID", "removeBuff", "rb");

        addCmd(GMModule::addAttribute, "增加属性 ID num", "addAttribute", "aa");
        addCmd(GMModule::subAttribute, "减少属性 ID num", "subAttribute", "sa");
        addCmd(GMModule::showAttribute, "显示属性", "showAttribute", "showAttr");
        addCmd(GMModule::refreshAttribute, "刷新属性", "refreshAttribute", "ra");

        addCmd(GMModule::setState, "设置状态 ID value", "setState", "ss");
        addCmd(GMModule::refreshState, "刷新状态", "refreshState", "rs");

        addCmd(GMModule::addPassiveSkill, "增加被动技能 skillID skillLevel", "addPassiveSkill", "aps");
        addCmd(GMModule::removePassiveSkill, "删除被动技能 skillID skillLevel", "removePassiveSkill", "rps");

        addCmd(GMModule::addBoxObstacle, "增加阻挡 x y z length width height", "addBoxObstacle", "abo");
        addCmd(GMModule::removeOneObstacle, "删除单个阻挡 index", "removeOneObstacle", "roo");
        addCmd(GMModule::removeAllObstacle, "删除全部阻挡", "removeAllObstacle", "rao");

        addCmd(GMModule::addExp, "增加经验 exp", "addExp", "ae");
        addCmd(GMModule::addLevel, "增加等级 level", "addLevel", "addLv", "lv");
        addCmd(GMModule::resetRole, "重置角色经验等级", "resetRole", "rr");

        addCmd(GMModule::addMail1, "增加邮件 type,titleID,contentID,behaviorSource", "addMail1", "addMail", "am");
        addCmd(GMModule::addMail2, "增加邮件 type,titleID,contentID,behaviorSource", "addMail2");
        addCmd(GMModule::addMail3, "增加邮件 type,titleID,contentID,behaviorSource", "addMail3");
        addCmd(GMModule::readMail, "读邮件 mailID", "readMail");
        addCmd(GMModule::receiveMailAttachment, "领邮件 mailID", "receiveMailAttachment");
        addCmd(GMModule::deleteMail, "deleteMail mailID", "deleteMail");

        addCmd(GMModule::openFunction, "开启功能 functionID", "openFunction", "of");

        addCmd(GMModule::getRankList, "获取排行榜列表 args", "getRankList");
        addCmd(GMModule::settlement, "排行榜结算", "settlement");

        addCmd(GMModule::triggerGoalEvent, "触发目标事件 goalType args", "triggerGoalEvent", "tge");

        addCmd(GMModule::acceptQuest, "接取任务 questID", "acceptQuest", "aq");
        addCmd(GMModule::commitQuest, "提交任务 questID", "commitQuest", "cq");

        addCmd(GMModule::enterDungeon, "进入副本 dungeonID level", "enterDungeon", "ed");
        addCmd(GMModule::switchDungeonRoom, "切换副本房间 roomID", "switchDungeonRoom", "sdr");
        addCmd(GMModule::quitDungeon, "退出副本", "quitDungeon", "qd");

        addCmd(GMModule::switchScene, "切换场景 sceneID lineID", "switchScene");
        addCmd(GMModule::enterHomeScene, "进入指定玩家家园场景 playerID", "enterHomeScene");
        addCmd(GMModule::enterUnionScene, "进入联盟场景 playerID", "enterUnionScene");

        addCmd(GMModule::getUnionList, "获取联盟列表 pageIndex", "getUnionList");
        addCmd(GMModule::createUnion, "创建联盟 unionName", "createUnion");
        addCmd(GMModule::quitUnion, "主动退出联盟", "quitUnion");
        addCmd(GMModule::disbandUnion, "解散联盟", "disbandUnion");
        addCmd(GMModule::applyJoinUnion, "申请加入联盟 unionID", "applyJoinUnion");
        addCmd(GMModule::inviteJoinUnion, "邀请加入联盟 targetPlayerID", "inviteJoinUnion");
        addCmd(GMModule::handleBeInviteJoinUnion, "响应被邀请 targetUnionID", "handleBeInviteJoinUnion");
        addCmd(GMModule::handleJoinUnion, "处理入盟消息 handleType handleResult targetPlayerID", "handleJoinUnion");
        addCmd(GMModule::kickOutMember, "踢出成员 targetPlayerID", "kickOutMember");
        addCmd(GMModule::changeLeader, "更换盟主 targetPlayerID", "changeLeader");
        addCmd(GMModule::modifyUnionName, "修改联盟名字 name", "modifyUnionName");
        addCmd(GMModule::modifyUnionSetting, "修改联盟设置", "modifyUnionSetting");
        addCmd(GMModule::modifyUnionMemberPosition, "修改联盟成员职位 targetPlayerID position ", "modifyUnionMemberPosition");
        addCmd(GMModule::changePermission, "修改权限 position permission", "changePermission");

        addCmd(GMModule::auctionSearch, "拍卖行查询 auctionType", "auctionSearch");
        addCmd(GMModule::auctionShelfItem, "拍卖行上架 index itemID itemNum price", "auctionShelfItem");
        addCmd(GMModule::auctionBuy, "拍卖行购买 auctionID itemID", "auctionBuy");
        addCmd(GMModule::auctionCancel, "拍卖行取消上架 auctionType auctionID", "auctionCancel");

        addCmd(GMModule::server2Server, "服务器之间通讯", "server2Server", "s2s");
        addCmd(GMModule::saveToRedis, "变化的数据缓存redis", "saveToRedis");
        addCmd(GMModule::saveToFile, "内存数据存储到文件", "saveToFile");
        addCmd(GMModule::parseBinFile, "解析文件", "parseBinFile");
        addCmd(GMModule::pushOfflineMsg, "推送离线消息 playerID", "pushOfflineMsg");
        addCmd(GMModule::test, "测试命令 args", "test");
        addCmd(GMModule::nb, "变成大号", "nb");
        addCmd(GMModule::getPlayerLiteInfo, "获取玩家精简信息", "getPlayerLiteInfo");
        addCmd(GMModule::netClose, "网络断线", "netClose");
        addCmd(GMModule::oomTest, "oomTest", "oomTest");
        addCmd(GMModule::globalGm, "global命令 args", "globalGm", "ggm");
        addCmd(GMModule::help, "帮助", "help");
        addCmd(GMModule::hotfixConfig, "热更配置", "hotfixConfig", "hc");
        addCmd(GMModule::hotfixProperty, "热更property", "hotfixProperty", "hp");

        generateHelpDesc();
    }


    /**
     * 增加gm命令
     */
    private void addCmd(GMCallback callback, String desc, String... cmdArray) {
        for (String cmd : cmdArray) {
            if (cmdMap.contains(cmd)) {
                LogicLoggerTool.systemLogger.error("add duplicate cmd = {}", cmd);
                return;
            }
        }
        for (String cmd : cmdArray) {
            GMCmd gmCmd = new GMCmd(callback);
            cmdMap.put(cmd, gmCmd);
        }
        helpDescStringBuilder.append(desc).append(" ");
        for (String cmd : cmdArray) {
            helpDescStringBuilder.append(cmd).append(" ");
        }
        helpDescStringBuilder.append("\n");
    }

    /**
     * 生成帮助文档
     */
    private void generateHelpDesc() {
        helpDesc = helpDescStringBuilder.toString();
    }

    /**
     * 获取命令信息
     *
     * @param cmd 命令
     * @return
     */
    public GMCmd getGMCmd(String cmd) {
        return cmdMap.get(cmd);
    }

    public String getHelpDesc() {
        return helpDesc;
    }
}
