package com.highfly029.logic.module.bag;

import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbBag;
import com.highfly029.common.template.bag.PlayerBagTypeConst;
import com.highfly029.logic.module.BaseModule;

/**
 * @ClassName BagModule
 * @Description 物品容易模块
 * @Author liyunpeng
 **/
public class BagModule extends BaseModule {
    @Override
    public void onPlayerInitData() {
        setData(new PlayerBagData(player));
        player.info("BagModule onPlayerInitData");
    }

    public PlayerBagData getData() {
        return (PlayerBagData) super.getData();
    }

    @Override
    public void onPlayerCreateNewRole() {
        getData().registerItemContainer(PlayerBagTypeConst.Bag, 20);
        player.info("BagModule onPlayerCreateNewRole");
    }

    @Override
    public void onPlayerBeforeLogin() {
        player.info("BagModule onPlayerBeforeLogin {}", getData());
    }

    @Override
    public void onPlayerLoginPushBaseClientData() {
        PbBag.S2CLoginPushPlayerAllBags.Builder builder = PbBag.S2CLoginPushPlayerAllBags.newBuilder();
        builder.setPlayerBagData((PbBag.PbPlayerBagData) getData().save());
        player.send(PtCode.S2CLoginPushPlayerAllBags, builder.build());
    }

    /**
     * 获取玩家背包物品容器
     *
     * @return
     */
    public PlayerItemContainer getPlayerBagItemContainer() {
        return getData().getPlayerItemContainer(PlayerBagTypeConst.Bag);
    }

    /**
     * 获取背包容器
     *
     * @param bagType
     * @return
     */
    public PlayerItemContainer getPlayerItemContainer(int bagType) {
        return getData().getPlayerItemContainer(bagType);
    }

}
