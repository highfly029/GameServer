package com.highfly029.logic.module.union;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.logic.module.IClientProtocolRegister;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName UnionProtocol
 * @Description 联盟协议
 * @Author liyunpeng
 **/
public class UnionProtocol implements IClientProtocolRegister {
    @Override
    public void registerClientProtocol(LogicChildWorld logicChildWorld) throws Exception {
        logicChildWorld.registerClientProtocol(PtCode.C2SGetUnionList, this::getUnionList);
        logicChildWorld.registerClientProtocol(PtCode.C2SCreateUnion, this::createUnion);
        logicChildWorld.registerClientProtocol(PtCode.C2SQuitUnion, this::quitUnion);
        logicChildWorld.registerClientProtocol(PtCode.C2SDisbandUnion, this::disbandUnion);
        logicChildWorld.registerClientProtocol(PtCode.C2SApplyJoinUnion, this::applyJoinUnion);
        logicChildWorld.registerClientProtocol(PtCode.C2SInviteJoinUnion, this::inviteJoinUnion);
        logicChildWorld.registerClientProtocol(PtCode.C2SHandleJoinUnion, this::handleJoinUnion);
        logicChildWorld.registerClientProtocol(PtCode.C2SHandleInviteJoinUnion, this::handleBeInviteJoinUnion);
        logicChildWorld.registerClientProtocol(PtCode.C2SKickOutUnionMember, this::kickOutMember);
        logicChildWorld.registerClientProtocol(PtCode.C2SChangeLeader, this::changeLeader);
        logicChildWorld.registerClientProtocol(PtCode.C2SModifyUnionName, this::modifyUnionName);
        logicChildWorld.registerClientProtocol(PtCode.C2SModifyUnionSetting, this::modifyUnionSetting);
        logicChildWorld.registerClientProtocol(PtCode.C2SModifyUnionMemberPosition, this::modifyUnionMemberPosition);
        logicChildWorld.registerClientProtocol(PtCode.C2SChangePermission, this::changePermission);
    }

    /**
     * 获取联盟列表
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void getUnionList(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        Player player = playerSession.getPlayer();
        PbUnion.C2SGetUnionList req = PbUnion.C2SGetUnionList.parseFrom(tcpPacketClient.getData());
        int pageIndex = req.getPageIndex();
        String unionName = req.getUnionName();
        player.getUnionModule().getUnionList(pageIndex, unionName);

    }

    /**
     * 创建联盟
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void createUnion(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        Player player = playerSession.getPlayer();
        PbUnion.C2SCreateUnion c2SCreateUnion = PbUnion.C2SCreateUnion.parseFrom(tcpPacketClient.getData());
        String unionName = c2SCreateUnion.getName();

        player.getUnionModule().createUnion(player.getPlayerID(), unionName);

    }

    /**
     * 主动退出联盟
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void quitUnion(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        Player player = playerSession.getPlayer();
        player.getUnionModule().quitUnion();
    }

    /**
     * 解散联盟
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void disbandUnion(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        playerSession.getPlayer().getUnionModule().disbandUnion();
    }

    /**
     * 申请加入联盟
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void applyJoinUnion(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbUnion.C2SApplyJoinUnion req = PbUnion.C2SApplyJoinUnion.parseFrom(tcpPacketClient.getData());
        long targetUnionID = req.getUnionID();
        playerSession.getPlayer().getUnionModule().applyJoinUnion(targetUnionID);
    }


    /**
     * 邀请加入联盟
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void inviteJoinUnion(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbUnion.C2SInviteJoinUnion req = PbUnion.C2SInviteJoinUnion.parseFrom(tcpPacketClient.getData());
        int logicServerID = req.getLogicServerID();
        long targetPlayerID = req.getTargetPlayerID();
        playerSession.getPlayer().getUnionModule().inviteJoinUnion(logicServerID, targetPlayerID);
    }

    /**
     * 处理入盟消息
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void handleJoinUnion(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbUnion.C2SHandleJoinUnion req = PbUnion.C2SHandleJoinUnion.parseFrom(tcpPacketClient.getData());
        int handleType = req.getHandleType();
        int handleResult = req.getHandleResult();
        long targetPlayerID = req.getTargetPlayerID();
        playerSession.getPlayer().getUnionModule().handleJoinUnion(handleType, handleResult, targetPlayerID);
    }

    /**
     * 玩家响应邀请加入联盟
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void handleBeInviteJoinUnion(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbUnion.C2SHandleInviteJoinUnion req = PbUnion.C2SHandleInviteJoinUnion.parseFrom(tcpPacketClient.getData());
        long targetUnionID = req.getTargetUnionID();
        playerSession.getPlayer().getUnionModule().handleBeInviteJoinUnion(targetUnionID, req.getHandleResultType());
    }

    /**
     * 踢出成员
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void kickOutMember(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbUnion.C2SKickOutUnionMember req = PbUnion.C2SKickOutUnionMember.parseFrom(tcpPacketClient.getData());
        long targetPlayerID = req.getTargetPlayerID();
        playerSession.getPlayer().getUnionModule().kickOutMember(targetPlayerID);
    }

    /**
     * 更换盟主
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void changeLeader(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbUnion.C2SChangeLeader req = PbUnion.C2SChangeLeader.parseFrom(tcpPacketClient.getData());
        long targetPlayerID = req.getTargetPlayerID();
        playerSession.getPlayer().getUnionModule().changeLeader(targetPlayerID);
    }

    /**
     * 修改联盟名字
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void modifyUnionName(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbUnion.C2SModifyUnionName req = PbUnion.C2SModifyUnionName.parseFrom(tcpPacketClient.getData());
        String name = req.getName();

        playerSession.getPlayer().getUnionModule().modifyUnionName(name);
    }

    /**
     * 修改联盟设置
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void modifyUnionSetting(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbUnion.C2SModifyUnionSetting req = PbUnion.C2SModifyUnionSetting.parseFrom(tcpPacketClient.getData());
        playerSession.getPlayer().getUnionModule().modifyUnionSetting(req.getSettingType(), req.getSetting());
    }

    /**
     * 修改联盟成员职位
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void modifyUnionMemberPosition(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbUnion.C2SModifyUnionMemberPosition req = PbUnion.C2SModifyUnionMemberPosition.parseFrom(tcpPacketClient.getData());
        playerSession.getPlayer().getUnionModule().modifyUnionMemberPosition(req.getTargetPlayerID(), req.getPosition());
    }

    /**
     * 修改权限
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void changePermission(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbUnion.C2SChangePermission req = PbUnion.C2SChangePermission.parseFrom(tcpPacketClient.getData());
        playerSession.getPlayer().getUnionModule().changePermission(req.getPosition(), req.getPermission());
    }
}
