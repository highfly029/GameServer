package com.highfly029.logic.module.goal;

import com.highfly029.common.template.goal.GoalMasterTypeConst;
import com.highfly029.common.template.goal.GoalTypeConst;
import com.highfly029.logic.module.BaseModule;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName GoalModule
 * @Description 目标模块
 * @Author liyunpeng
 **/
public class GoalModule extends BaseModule {
    /**
     * 全部的目标数据 goalType -> goalInstance -> goalData
     */
    private final IntObjectMap<IntObjectMap<GoalData>> allGoalData = new IntObjectMap<>(IntObjectMap[]::new);
    /**
     * 目标自增索引
     */
    private int goalAutoIncrementIndex;

    /**
     * 注册目标、只注册没有完成的目标
     */
    public void registerGoal(GoalData goalData, byte goalMasterType, int goalMasterID) {
        goalData.setGoalMasterType(goalMasterType);
        goalData.setGoalMasterID(goalMasterID);
        goalData.setInstanceID(++goalAutoIncrementIndex);
//        goalData.goalTemplate = GoalModule.getGoalTemplate(goalData.goalID);

        int goalType = goalData.getGoalTemplate().getGoalType();
        IntObjectMap<GoalData> goalTypeMapData = allGoalData.get(goalType);
        if (goalTypeMapData == null) {
            goalTypeMapData = new IntObjectMap<>(GoalData[]::new);
            allGoalData.put(goalType, goalTypeMapData);
        }
        goalTypeMapData.put(goalData.getInstanceID(), goalData);
    }

    /**
     * 增加目标
     *
     * @param goalMasterType
     * @param goalMasterID
     * @param goalID
     */
    public GoalData addGoal(byte goalMasterType, int goalMasterID, int goalID) {
        GoalData goalData = GoalData.create(goalID);
        goalData.setGoalMasterType(goalMasterType);
        goalData.setGoalMasterID(goalMasterID);
        goalData.setInstanceID(++goalAutoIncrementIndex);

        int goalType = goalData.getGoalTemplate().getGoalType();
        //如果是不需要触发的只需要判断条件就能完成的目标、直接检测完成的不放入map里、直接返回。。
        if (goalData.isInitCanRefresh()) {
            checkGoals(goalData, null);
            if (goalData.isComplete()) {
                return goalData;
            }
        }

        IntObjectMap<GoalData> goalTypeMapData = allGoalData.get(goalType);
        if (goalTypeMapData == null) {
            goalTypeMapData = new IntObjectMap<>(GoalData[]::new);
            allGoalData.put(goalType, goalTypeMapData);
        }
        goalTypeMapData.put(goalData.getInstanceID(), goalData);
        return goalData;
    }

    /**
     * 删除目标 不能在triggerGoalEvent里调用
     *
     * @param goalData
     */
    public void removeGoal(GoalData goalData) {
        int goalType = goalData.getGoalTemplate().getGoalType();
        IntObjectMap<GoalData> goalTypeMapData = allGoalData.get(goalType);
        if (goalTypeMapData == null) {
            return;
        }
        goalTypeMapData.remove(goalData.getInstanceID());
    }

    /**
     * 触发目标事件
     *
     * @param goalType
     * @param params
     */
    public void triggerGoalEvent(int goalType, int... params) {
        IntObjectMap<GoalData> goalTypeMapData = allGoalData.get(goalType);
        if (goalTypeMapData == null || goalTypeMapData.size() == 0) {
            return;
        }
        IntObjectMap<GoalData> map = new IntObjectMap<>(GoalData[]::new);
        //过滤出完成的目标，后面统一删除和触发
        goalTypeMapData.foreachImmutable((k, goalData) -> {
            int preProgress = goalData.getProgress();
            boolean isComplete = goalData.isComplete();
            checkGoals(goalData, params);
            if (preProgress != goalData.getProgress()) {
                onGoalProgressChanged(goalData);
            }
            if (goalData.isComplete() && !isComplete) {
                map.put(k, goalData);
            }
        });

        map.foreachImmutable((k, goalData) -> {
            goalTypeMapData.remove(k);
            onGoalCompleted(goalData);
        });
    }

    /**
     * 目标进度变化通知
     *
     * @param goalData
     */
    private void onGoalProgressChanged(GoalData goalData) {
        switch (goalData.getGoalMasterType()) {
            case GoalMasterTypeConst.Quest -> {
                player.getQuestModule().onGoalProgressChanged(goalData);
            }
            case GoalMasterTypeConst.Achievement -> {
            }
        }
    }

    /**
     * 目标完成通知
     *
     * @param goalData
     */
    private void onGoalCompleted(GoalData goalData) {
        switch (goalData.getGoalMasterType()) {
            case GoalMasterTypeConst.Quest -> {
                player.getQuestModule().onGoalCompleted(goalData);
            }
            case GoalMasterTypeConst.Achievement -> {
            }
        }
    }

    /**
     * 检测目标是否完成
     *
     * @param goalData
     * @param params   如果只需要刷新则为null
     */
    private void checkGoals(GoalData goalData, int[] params) {
        int goalType = goalData.getGoalTemplate().getGoalType();
        int[] goalParams = goalData.getGoalTemplate().getGoalParams();
        int completeNum = goalData.getGoalTemplate().getCompleteNum();
        switch (goalType) {
            case GoalTypeConst.ReachLevel -> {
                if (player.getRoleModule().getLevel() >= goalParams[0]) {
                    goalData.setProgress(1);
                }
            }
            case GoalTypeConst.GetNewItem -> {
                if (goalParams[0] == params[0]) {
                    goalData.setProgress(goalData.getProgress() + params[1]);
                }
            }
            case GoalTypeConst.OwnItem -> {
                int itemID = goalParams[0];
                if (params != null) {
                    if (params[0] == itemID) {
                        //优化
                        if (params[1] >= completeNum) {
                            goalData.setProgress(params[1]);
                        } else {
                            goalData.setProgress(player.getBagModule().getPlayerBagItemContainer().getItemNum(itemID));
                        }
                    }
                } else {
                    goalData.setProgress(player.getBagModule().getPlayerBagItemContainer().getItemNum(itemID));
                }
            }
        }
    }
}
