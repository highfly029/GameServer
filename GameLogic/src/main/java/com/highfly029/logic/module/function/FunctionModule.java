package com.highfly029.logic.module.function;

import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbFunction;
import com.highfly029.logic.module.BaseModule;
import com.highfly029.logic.tool.ConfigTool;

/**
 * @ClassName FunctionModule
 * @Description 功能开启模块
 * @Author liyunpeng
 **/
public class FunctionModule extends BaseModule {
    @Override
    public void onPlayerInitData() {
        setData(new PlayerFunctionData());
    }

    @Override
    public PlayerFunctionData getData() {
        return (PlayerFunctionData) super.getData();
    }

    @Override
    public void onPlayerBeforeLogin() {
        checkAllFunctions();
    }

    @Override
    public void onPlayerLoginPushBaseClientData() {
        PbFunction.S2CLoginPushPlayerFunction.Builder builder = PbFunction.S2CLoginPushPlayerFunction.newBuilder();
        builder.setPlayerFunctionData((PbFunction.PbPlayerFunctionData) getData().save());
        player.send(PtCode.S2CLoginPushPlayerFunction, builder.build());
    }

    /**
     * 功能开启
     *
     * @param functionID
     */
    public void openFunction(int functionID) {
        this.getData().getOpenFunctions().put(functionID, true);
        getData().setChange(true);
        PbFunction.S2COpenFunction.Builder builder = PbFunction.S2COpenFunction.newBuilder();
        builder.setFunctionID(functionID);
        player.send(PtCode.S2COpenFunction, builder.build());

        onFunctionOpen(functionID);
    }

    /**
     * 检测所有功能
     */
    public void checkAllFunctions() {
        ConfigTool.getFunctionConfig().getTemplates().foreachImmutable((k, v) -> {
            if (!this.getData().getOpenFunctions().get(k)) {
                if (v.getConditions() == null || v.getConditions().length == 0) {
                    //功能开启条件不填的话默认不开启
                } else {
                    boolean isOpen = true;
                    for (int i = 0, len = v.getConditions().length; i < len; i++) {
                        //有一个检测不过则不能开启
                        if (!player.checkRoleCondition(v.getConditions()[i])) {
                            isOpen = false;
                            break;
                        }
                    }
                    if (isOpen) {
                        openFunction(k);
                    }
                }
            }
        });
    }

    /**
     * 检测功能开启
     *
     * @param conditionType
     */
    public void checkFunction(int conditionType) {
        ConfigTool.getFunctionConfig().getTemplates().foreachImmutable((k, v) -> {
            if (!this.getData().getOpenFunctions().get(k)) {
                if (v.getConditions() != null) {
                    boolean hasConditionType = false;
                    //先判断是否包含此功能开启条件
                    for (int i = 0, len = v.getConditions().length; i < len; i++) {
                        if (v.getConditions()[i][0] == conditionType) {
                            hasConditionType = true;
                            break;
                        }
                    }
                    if (hasConditionType) {
                        boolean isOpen = true;
                        for (int i = 0, len = v.getConditions().length; i < len; i++) {
                            //有一个检测不过则不能开启
                            if (!player.checkRoleCondition(v.getConditions()[i])) {
                                isOpen = false;
                                break;
                            }
                        }
                        if (isOpen) {
                            openFunction(k);
                        }
                    }
                }
            }
        });
    }

    /**
     * 功能是否开启
     *
     * @param functionID
     * @return
     */
    public boolean isFunctionOpen(int functionID) {
        return getData().getOpenFunctions().get(functionID);
    }

    /**
     * 功能开启通知
     *
     * @param functionID
     */
    private void onFunctionOpen(int functionID) {
        player.info("onFunctionOpen", functionID);
    }
}
