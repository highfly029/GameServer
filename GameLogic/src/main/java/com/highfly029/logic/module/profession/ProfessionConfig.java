package com.highfly029.logic.module.profession;

import com.highfly029.common.template.profession.ProfessionTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName ProfessionConfig
 * @Description ProfessionConfig
 * @Author liyunpeng
 **/
public class ProfessionConfig extends BaseGlobalConfig {
    /**
     * 玩家职业模板
     */
    private final IntObjectMap<ProfessionTemplate> professionTemplates = new IntObjectMap<>(ProfessionTemplate[]::new);

    @Override
    public void check(boolean isHotfix) {

    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(ProfessionTemplate.class);
        ProfessionTemplate template;
        for (int i = 0, size = list.size(); i < size; i++) {
            template = (ProfessionTemplate) list.get(i);
            professionTemplates.put(template.getProfessionID(), template);
        }
    }

    /**
     * 获取职业模板
     *
     * @param id
     * @return
     */
    public ProfessionTemplate getProfessionTemplate(int id) {
        return professionTemplates.get(id);
    }
}
