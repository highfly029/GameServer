package com.highfly029.logic.module.common;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.core.interfaces.IRegisterServerChildProtocol;
import com.highfly029.core.session.Session;
import com.highfly029.core.world.ChildWorld;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName CommonChildProtocol
 * @Description CommonChildProtocol
 * @Author liyunpeng
 **/
public class CommonChildProtocol implements IRegisterServerChildProtocol {
    private LogicChildWorld logicChildWorld;

    @Override
    public void registerChildProtocol(ChildWorld childWorld) throws Exception {
        this.logicChildWorld = (LogicChildWorld) childWorld;
        childWorld.registerServerChildProtocol(PtCode.Center2SceneInfoCode, this::infoCodeResult);
    }

    /**
     * 收到中心服的消息码
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void infoCodeResult(Session session, int ptCode, GeneratedMessageV3 message) {
        PbCommon.Center2SceneInfoCode req = (PbCommon.Center2SceneInfoCode) message;
        long playerID = req.getPlayerID();
        int infoCode = req.getInfoCode();
        Player player = logicChildWorld.getPlayerManager().getPlayer(req.getPlayerID());
        if (player == null) {
            LogicLoggerTool.gameLogger.error("infoCodeResult playerID={}, inCode={}", playerID, infoCode);
            return;
        }
        PbCommon.S2CInfoCode.Builder builder = PbCommon.S2CInfoCode.newBuilder();
        builder.setInfoCode(infoCode);
        builder.addAllArgs(req.getArgsList());
        player.send(PtCode.S2CInfoCode, builder.build());
    }
}
