package com.highfly029.logic.module.login;

import java.util.function.Consumer;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLogin;
import com.highfly029.common.redis.RedisKey;
import com.highfly029.common.template.global.GlobalConst;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.net.http.HttpConst;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.tool.HttpClientTool;
import com.highfly029.core.tool.RedisClientTool;
import com.highfly029.logic.global.LogicConst;
import com.highfly029.logic.module.IClientProtocolRegister;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.world.LogicChildWorld;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.JsonUtils;
import com.highfly029.utils.collection.ObjObjMap;

import io.vertx.redis.client.Command;
import io.vertx.redis.client.Request;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName LoginProtocol
 * @Description 登陆协议
 * @Author liyunpeng
 **/
public class LoginProtocol implements IClientProtocolRegister {
    private LogicChildWorld logicChildWorld;

    @Override
    public void registerClientProtocol(LogicChildWorld logicChildWorld) throws Exception {
        this.logicChildWorld = logicChildWorld;

        logicChildWorld.registerClientProtocol(PtCode.C2SRoleList, this::roleListHandler);
        logicChildWorld.registerClientProtocol(PtCode.C2SRoleCreate, this::roleCreateHandler);
        logicChildWorld.registerClientProtocol(PtCode.C2SRoleLogin, this::roleLoginHandler);
    }

    //角色列表
    public void roleListHandler(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbLogin.C2SRoleList req = PbLogin.C2SRoleList.parseFrom(packet.getData());
        long accountID = req.getAccountID();
        int sourceLogicID = req.getSourceLogicID();

        MySQLUtils.sendChild(logicChildWorld.getConnectionPool(sourceLogicID), "SELECT player_id FROM player WHERE account_id = " + accountID + ";", null, logicChildWorld.getIndex(), (isSuccess, rows, errorMsg) -> {
            if (isSuccess) {
                PbLogin.S2CRoleList.Builder builder = PbLogin.S2CRoleList.newBuilder();
                PbLogin.RoleListInfo.Builder playerInfoBuilder = PbLogin.RoleListInfo.newBuilder();
                for (Row row : rows) {
                    playerInfoBuilder.clear();
                    long playerID = row.getLong(0);
                    playerInfoBuilder.setPlayerID(playerID);
                    playerInfoBuilder.setName("name" + playerID);
                    builder.addPlayers(playerInfoBuilder.build());
                }
                builder.setAccountID(accountID);
                session.send(PtCode.S2CRoleList, builder.build());
                LogicLoggerTool.gameLogger.info("roleList get success accountID={}", accountID);
            } else {
                LogicLoggerTool.gameLogger.error("roleList get fail accountID={}, errorMsg={}", accountID, errorMsg);
            }
        });
    }

    //角色创建
    public void roleCreateHandler(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbLogin.C2SRoleCreate req = PbLogin.C2SRoleCreate.parseFrom(packet.getData());
        long accountID = req.getAccountID();
        long newPlayerID = LogicConst.playerIDSnowFlake.nextID();

        Player player = logicChildWorld.getPlayerManager().create();
        player.init(logicChildWorld, accountID, newPlayerID);
        player.onPlayerInitData();
        player.onPlayerCreateNewRole();
        player.info("roleCreateHandler begin", accountID, newPlayerID);
        String insertSQLStr = player.getInsertSQLStr();
        Tuple tuple = player.getInsertSQLTuple();
        MySQLUtils.sendChild(logicChildWorld.getConnectionPool(player), insertSQLStr, tuple, logicChildWorld.getIndex(), (isSuccess, rows, errorMsg) -> {
            if (isSuccess) {
                if (rows.rowCount() == 1) {
                    try {
                        player.info("roleCreateHandler insert success", accountID);
                        HttpClientTool.get(logicChildWorld.getIndex(), ConfigPropUtils.getIntValue("accountPort"), ConfigPropUtils.getValue("accountHost"), "/account/accountBindLogic?accountID=" + accountID + "&logicID=" + ConfigConst.identifier, (isSuccess1, body, errorMsg1) -> {
                            if (isSuccess1) {
                                ObjObjMap<String, String> resultMap = JsonUtils.fromJson(body, ObjObjMap.class);
                                if (resultMap.containsKey("result") && resultMap.get("result").equals(HttpConst.RESP_SUCCESS)) {
                                    player.info("roleCreateHandler accountBindLogic success", accountID, resultMap.get("modified"));
                                    PbLogin.S2CRoleCreate.Builder builder = PbLogin.S2CRoleCreate.newBuilder();
                                    builder.setAccountID(accountID);
                                    builder.setPlayerID(newPlayerID);
                                    player.onPlayerBeforeLogin();
                                    player.initPlayerLiteInfo();
                                    player.saveToRedis();

                                    session.setPlayer(player);
                                    player.setSession(session);
                                    //先通知客户端登录成功
                                    session.send(PtCode.S2CRoleCreate, builder.build());
                                    //然后再推送数据
                                    player.onPlayerLoginPushBaseClientData();
                                    player.onPlayerAfterLogin();

                                    //插入成功后才会添加
                                    logicChildWorld.getPlayerManager().addPlayer(player);

                                    player.getSceneModule().enterHomeSceneLocation(GlobalConst.BornSceneID, player.getPlayerID());
                                } else {
                                    player.error("roleCreateHandler account bind fail", accountID, body);
                                }
                            } else {
                                player.error("roleCreateHandler account connect fail", accountID, errorMsg1);
                            }
                        });

                    } catch (Exception e) {
                        player.error("roleCreateHandler fail", e);
                    }
                } else {
                    player.error("roleCreateHandler insert fail accountID", accountID);
                }
            } else {
                player.error("roleCreateHandler mysql connect fail ", accountID, errorMsg);
            }
        });
    }

    //角色登录
    public void roleLoginHandler(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbLogin.C2SRoleLogin req = PbLogin.C2SRoleLogin.parseFrom(packet.getData());
        //check account and setSession
        long accountID = req.getAccountID();
        long playerID = req.getPlayerID();
        long startTime = System.currentTimeMillis();
        Tuple tuple = Tuple.of(playerID, accountID);
        Player player = logicChildWorld.getPlayerManager().create();
        player.init(logicChildWorld, req.getAccountID(), req.getPlayerID());
        player.setSourceLogicID(req.getSourceLogicID());
        player.onPlayerInitData();
        player.info("roleLoginHandle", req.getAccountID());

        //内存数据处理完之后的工作
        Consumer<Long> afterInitData = (getDataTime) -> {
            Request request = Request.cmd(Command.PERSIST).arg(RedisKey.PLAYER_CACHE_DATA.value() + playerID);
            RedisClientTool.send(player.getChildWorldIndex(), request, (isSuccess1, response, errorMsg1) -> {
                if (!isSuccess1) {
                    player.error("afterInitData redis PERSIST fail", errorMsg1);
                }
            });

            //先通知客户端登录成功
            PbLogin.S2CRoleLogin.Builder resp = PbLogin.S2CRoleLogin.newBuilder();
            resp.setPlayerID(playerID);
            session.send(PtCode.S2CRoleLogin, resp.build());
            //然后再推送数据
            player.onPlayerLoginPushBaseClientData();
            player.onPlayerAfterLogin();
            long endTime = System.currentTimeMillis();
            player.info("login afterInitData success", getDataTime - startTime, endTime - startTime);

            logicChildWorld.getPlayerManager().addPlayer(player);

            //上次所在的场景
            int lastSceneID = GlobalConst.BornSceneID;
            player.getSceneModule().enterHomeSceneLocation(GlobalConst.BornSceneID, playerID);
        };

        Consumer<Long> initDataFromMysql = (versionCode) -> {
            String allModuleSQLName = player.getAllModuleSQLName();
            MySQLUtils.sendChild(logicChildWorld.getConnectionPool(player), "SELECT " + allModuleSQLName + "  FROM player WHERE player_id=? AND account_id=?;", tuple, logicChildWorld.getIndex(), (isSuccess, successRows, errorMsg) -> {
                if (isSuccess) {
                    long getDataTime = System.currentTimeMillis();
                    int rowSize = successRows.size();
                    if (rowSize == 0) {
                        player.error("login initDataFromMysql fail, cant find player in db", accountID);
                    } else if (rowSize == 1) {
                        Row row = successRows.iterator().next();
                        try {
                            player.info("login initDataFromMysql versionCode", versionCode);

                            player.load(row);
                            player.setVersionCode(versionCode);

                            player.onPlayerBeforeLogin();

                            player.handlerRedisOfflineMsg(result -> {
                                if (result) {
                                    player.saveToRedis();

                                    player.setSession(session);
                                    session.setPlayer(player);
                                    afterInitData.accept(getDataTime);
                                } else {
                                    player.error("login initDataFromMysql handlerRedisOfflineMsg fail");
                                }
                            });

                        } catch (InvalidProtocolBufferException e) {
                            player.error("login initDataFromMysql parse fail", e);
                        } catch (Exception e) {
                            player.error("login initDataFromMysql fail", e);
                        }
                    } else {
                        player.error("login initDataFromMysql fail, more than 1 player", accountID, rowSize);
                    }
                } else {
                    player.error("login initDataFromMysql connect fail", errorMsg);
                }
            });
        };

        Consumer<Long> initDataFromRedis = (versionCode) -> {
            Request request = Request.cmd(Command.HGETALL).arg(RedisKey.PLAYER_CACHE_DATA.value() + playerID);
            RedisClientTool.send(player.getChildWorldIndex(), request, (isSuccess, response, errorMsg) -> {
                if (isSuccess) {
                    long getDataTime = System.currentTimeMillis();
                    try {
                        player.info("login initDataFromRedis", versionCode);
                        player.load(response);

                        player.setVersionCode(versionCode);
                        player.onPlayerBeforeLogin();

                        player.handlerRedisOfflineMsg(result -> {
                            if (result) {
                                player.setSession(session);
                                session.setPlayer(player);
                                afterInitData.accept(getDataTime);
                            } else {
                                player.error("login initDataFromRedis handlerRedisOfflineMsg fail");
                            }
                        });
                    } catch (InvalidProtocolBufferException e) {
                        player.error("login initDataFromRedis parse fail", e);
                    } catch (Exception e) {
                        player.error("login initDataFromRedis fail", e);
                    }
                } else {
                    player.error("login initDataFromRedis connect fail", errorMsg);
                }
            });
        };

        MySQLUtils.sendChild(logicChildWorld.getConnectionPool(player), "SELECT `versionCode` FROM player WHERE player_id=? AND account_id=?;", tuple, logicChildWorld.getIndex(), (isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                Row row = successRows.iterator().next();
                long mysqlVersionCode = row.getLong("versionCode");

                Request request = Request.cmd(Command.HGET).arg(RedisKey.PLAYER_CACHE_DATA.value() + playerID).arg("versionCode");
                RedisClientTool.send(player.getChildWorldIndex(), request, (isSuccess2, response, errorMsg2) -> {
                    if (isSuccess2) {
                        if (response != null) {
                            long redisVersionCode = response.toLong();
                            player.info("login mysql success redis success", redisVersionCode, mysqlVersionCode);
                            //正常情况下redis版本比mysql版本新
                            if (redisVersionCode >= mysqlVersionCode) {
                                initDataFromRedis.accept(redisVersionCode);
                            } else {
                                initDataFromMysql.accept(mysqlVersionCode);
                            }
                        } else {
                            player.info("login mysql success redis not exist cache", mysqlVersionCode);
                            //正常情况下redis缓存过期
                            initDataFromMysql.accept(mysqlVersionCode);
                        }

                    } else {
                        //退出登录
                        player.error("login mysql success redis get versionCode fail", mysqlVersionCode, errorMsg2);
                    }
                });
            } else {
                //退出登录
                player.error("login mysql get versionCode fail", errorMsg);
            }
        });

    }

    /**
     * 登陆失败
     *
     * @param playerSession
     * @param reason
     */
    private void onLoginFail(PlayerSession playerSession, int reason) {
//        playerSession.send();
        playerSession.close();
    }
}
