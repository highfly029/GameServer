package com.highfly029.logic.module.asset;

import com.highfly029.common.template.asset.PlayerAssetTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName AssetConfig
 * @Description AssetConfig
 * @Author liyunpeng
 **/
public class AssetConfig extends BaseGlobalConfig {
    private final IntObjectMap<PlayerAssetTemplate> playerAssetTemplates = new IntObjectMap<>(PlayerAssetTemplate[]::new);

    @Override
    public void check(boolean isHotfix) {

    }

    @Override
    protected void dataProcess() {
        ObjectList<BaseTemplate> list = TemplateTool.dataTemplates.get(PlayerAssetTemplate.class);
        PlayerAssetTemplate template;
        for (int i = 0, size = list.size(); i < size; i++) {
            template = (PlayerAssetTemplate) list.get(i);
            playerAssetTemplates.put(template.getAssetID(), template);
        }
    }

    /**
     * 获取资产模版
     *
     * @param type
     * @return
     */
    public PlayerAssetTemplate getTemplate(int type) {
        return playerAssetTemplates.get(type);
    }
}
