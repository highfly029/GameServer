package com.highfly029.logic.module.auction;

import com.google.protobuf.ByteString;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbAuction;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.logic.module.IOfflineMessageHandler;
import com.highfly029.logic.module.Player;

/**
 * @ClassName AuctionBuyToSellerOfflineMessage
 * @Description AuctionBuyToSellerOfflineMessage
 * @Author liyunpeng
 **/
public class AuctionBuyToSellerOfflineMessage implements IOfflineMessageHandler {
    @Override
    public int registerPtCode() {
        return PtCode.Center2LogicAuctionBuyToSeller;
    }

    @Override
    public void handle(Player player, ByteString byteString) throws Exception {
        PbAuction.Center2LogicAuctionBuyToSeller req = PbAuction.Center2LogicAuctionBuyToSeller.parseFrom(byteString);
        player.getAuctionModule().onAuctionBuyToSeller(req.getBuyPlayerID(), PbCommonUtils.auctionItemDataPb2Obj(req.getAuctionItemData()));
    }
}
