package com.highfly029.logic.module.activity;

import com.highfly029.common.data.activity.ActivityTimeData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbActivity;
import com.highfly029.common.template.activity.ActivityDriveTypeConst;
import com.highfly029.common.template.activity.ActivityRelativeTimeConst;
import com.highfly029.common.template.activity.ActivityTemplate;
import com.highfly029.common.template.activity.ActivityTimeTypeConst;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.logic.manager.ActivityChildManager;
import com.highfly029.logic.module.BaseModule;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName ActivityModule
 * @Description 活动模块
 * @Author liyunpeng
 **/
public class ActivityModule extends BaseModule {

    @Override
    public void onPlayerInitData() {
        setData(new PlayerActivityData());
    }

    @Override
    public PlayerActivityData getData() {
        return (PlayerActivityData) super.getData();
    }

    @Override
    public void onPlayerBeforeLogin() {
        ActivityChildManager activityChildManager = player.getLogicChildWorld().getActivityChildManager();
        IntSet openedActivitySet = activityChildManager.getOpenedActivitySet();
        ActivityConfig activityConfig = ConfigTool.getActivityConfig();
        IntObjectMap<BaseActivity> activities = getData().getActivities();
        //logic和center驱动的活动
        openedActivitySet.foreachImmutable(id -> {
            //新增活动
            if (!activities.contains(id)) {
                onOpenActivity(id);
            }
        });
        activities.foreachMutable((id, activityData) -> {
            ActivityTemplate activityTemplate = activityConfig.getTemplate(id);
            if (activityTemplate.getActivityDriveType() != ActivityDriveTypeConst.Player) {
                //已经不存在的活动
                if (!openedActivitySet.contains(id)) {
                    onCloseActivity(id, false);
                }
            }
        });

        //player驱动的活动


        activities.foreachImmutable((id, activityData) -> {
            player.info("onPlayerBeforeLogin activity", id);
        });

        getData().getActivities().foreachImmutable((id, activityData) -> activityData.onPlayerBeforeLogin());
    }

    @Override
    public void onPlayerAfterLogin() {
        getData().getActivities().foreachImmutable((id, activityData) -> activityData.onPlayerAfterLogin());
    }

    @Override
    public void onPlayerSecond() {
        ActivityConfig activityConfig = ConfigTool.getActivityConfig();
        IntObjectMap<ActivityTemplate> activityTemplateMap = activityConfig.getActivityTemplateMap();
        IntObjectMap<ActivityTimeData> playerActivityTemplateDataMap = activityConfig.getPlayerActivityTemplateDataMap();
        IntObjectMap<BaseActivity> activities = getData().getActivities();
        long now = SystemTimeTool.getMillTime();
        activityTemplateMap.foreachImmutable((id, activityTemplate) -> {
            if (activityTemplate.getActivityDriveType() == ActivityDriveTypeConst.Player) {
                ActivityTimeData activityTimeData = playerActivityTemplateDataMap.get(id);
                //强制关闭活动
                if (activityTimeData.getForceCloseTime() > 0 && now >= activityTimeData.getForceCloseTime()) {
                    if (activities.contains(activityTemplate.getId())) {
                        onCloseActivity(activityTemplate.getId(), true);
                    }
                    return;
                }

                if (activityTemplate.getActivityTimeType() == ActivityTimeTypeConst.RelativeTime &&
                        activityTimeData.getRelativeArgs()[0] == ActivityRelativeTimeConst.PlayerRegisterTime) {
                    long t1 = player.getRoleModule().getData().getCreateTime() + activityTimeData.getRelativeArgs()[1] * 1000L;
                    long t2 = t1 + activityTemplate.getDurationTime() * 1000L;
                    if (now >= t1 && now <= t2) {
                        if (!activities.contains(activityTemplate.getId())) {
                            onOpenActivity(activityTemplate.getId());
                        }
                    }
                    if (now > t2 && activities.contains(activityTemplate.getId())) {
                        onCloseActivity(activityTemplate.getId(), false);
                    }
                } else {
                    if (now >= activityTimeData.getStartTime() && now <= activityTimeData.getEndTime()) {
                        if (!activities.contains(activityTemplate.getId())) {
                            onOpenActivity(activityTemplate.getId());
                        }
                    }
                    if (now > activityTimeData.getEndTime() && activities.contains(activityTemplate.getId())) {
                        onCloseActivity(activityTemplate.getId(), false);
                    }
                }
            }
        });

        getData().getActivities().foreachImmutable((id, activityData) -> activityData.onSecond());
    }

    /**
     * 开启活动
     *
     * @param id
     */
    public void onOpenActivity(int id) {
        BaseActivity baseActivity = ActivityFactory.create(player, id);
        getData().getActivities().put(id, baseActivity);
        getData().setChange(true);
        baseActivity.onOpen();
        PbActivity.S2COpenActivity.Builder builder = PbActivity.S2COpenActivity.newBuilder();
        builder.setActivityData(baseActivity.activityDataToPb());
        player.send(PtCode.S2COpenActivity, builder.build());
    }

    /**
     * 关闭活动
     *
     * @param id
     * @param forceClose
     */
    public void onCloseActivity(int id, boolean forceClose) {
        BaseActivity baseActivity = getData().getActivities().get(id);
        getData().getActivities().remove(id);
        getData().setChange(true);
        baseActivity.onClose(forceClose);
        PbActivity.S2CCloseActivity.Builder builder = PbActivity.S2CCloseActivity.newBuilder();
        builder.setId(id);
        player.send(PtCode.S2CCloseActivity, builder.build());
    }

    @Override
    public void onPlayerDaily(boolean isLogin) {
        getData().getActivities().foreachImmutable((id, activityData) -> activityData.onDaily(isLogin));
    }

}
