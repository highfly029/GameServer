package com.highfly029.logic.module.quest;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbQuest;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.logic.module.IClientProtocolRegister;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName QuestProtocol
 * @Description QuestProtocol
 * @Author liyunpeng
 **/
public class QuestProtocol implements IClientProtocolRegister {
    @Override
    public void registerClientProtocol(LogicChildWorld logicChildWorld) throws Exception {
        logicChildWorld.registerClientProtocol(PtCode.C2SAcceptQuest, this::acceptQuest);
        logicChildWorld.registerClientProtocol(PtCode.C2SCommitQuest, this::commitQuest);
        logicChildWorld.registerClientProtocol(PtCode.C2SGiveUpQuest, this::giveUpQuest);
    }

    /**
     * 接取任务
     *
     * @param playerSession
     * @param tcpPacketClient
     */
    private void acceptQuest(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbQuest.C2SAcceptQuest req = PbQuest.C2SAcceptQuest.parseFrom(tcpPacketClient.getData());
        int questType = req.getQuestType();
        int questID = req.getQuestID();
        playerSession.getPlayer().getQuestModule().acceptQuest(questType, questID);
    }

    /**
     * 提交任务
     *
     * @param playerSession
     * @param tcpPacketClient
     */
    private void commitQuest(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbQuest.C2SCommitQuest req = PbQuest.C2SCommitQuest.parseFrom(tcpPacketClient.getData());
        int questType = req.getQuestType();
        int questID = req.getQuestID();
        int selectRewardIndex = req.getSelectRewardIndex();
        playerSession.getPlayer().getQuestModule().commitQuest(questType, questID, selectRewardIndex);
    }


    /**
     * 放弃任务
     *
     * @param playerSession
     * @param tcpPacketClient
     * @throws InvalidProtocolBufferException
     */
    private void giveUpQuest(PlayerSession playerSession, PbPacket.TcpPacketClient tcpPacketClient) throws InvalidProtocolBufferException {
        PbQuest.C2SGiveUpQuest req = PbQuest.C2SGiveUpQuest.parseFrom(tcpPacketClient.getData());
        int questType = req.getQuestType();
        int questID = req.getQuestID();
        playerSession.getPlayer().getQuestModule().giveUpQuest(questType, questID);
    }

}
