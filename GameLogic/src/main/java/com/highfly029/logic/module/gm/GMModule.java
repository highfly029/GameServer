package com.highfly029.logic.module.gm;

import java.util.ArrayList;
import java.util.List;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.auction.AuctionItemData;
import com.highfly029.common.redisRank.RedisRankSettleData;
import com.highfly029.logic.common.container.item.ItemData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbGm;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.common.redis.RedisKey;
import com.highfly029.common.template.behaviorSource.BehaviorSourceConst;
import com.highfly029.common.template.union.UnionUpdateTypeConst;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.RedisClientTool;
import com.highfly029.core.tool.ThreadPoolExecutorTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.logic.common.container.item.ItemFactory;
import com.highfly029.logic.module.BaseModule;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.module.rank.PlayerLevelRank;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.common.data.playerLiteInfo.PlayerLiteInfoCacheTool;
import com.highfly029.logic.world.Logic2GlobalEvent;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.collection.IntLongMap;
import com.highfly029.utils.collection.ObjectList;

import io.vertx.redis.client.Command;
import io.vertx.redis.client.Redis;
import io.vertx.redis.client.Request;
import io.vertx.redis.client.Response;

/**
 * @ClassName GMModule
 * @Description gm
 * @Author liyunpeng
 **/
public class GMModule extends BaseModule {
    /**
     * 返回值
     */
    private static final String success = "success";

    /**
     * 测试用自增数字
     */
    private int increaseIndex;

    public void onModuleStartUp() {

    }

    /**
     * gm命令
     *
     * @param command
     */
    public void gmCommandHandler(String command) throws InvalidProtocolBufferException {

        String result = success;
        result = doGmCommand(command);
        PbGm.S2CGmCommand.Builder resp = PbGm.S2CGmCommand.newBuilder();
        resp.setResult(result);
        player.send(PtCode.S2CGmCommand, resp.build());
    }

    @Override
    public void onPlayerTick(int escapedMillTime) {
//        PbGm.S2CGmCommand.Builder resp = PbGm.S2CGmCommand.newBuilder();
//        resp.setResult(String.valueOf(++increaseIndex));
//        LogicLoggerTool.gameLogger.info("自增数字={}", increaseIndex);
//        player.send(PtCode.S2CGmCommand, resp.build());
    }

    @Override
    public void onPlayerSecond() {
//        PbGm.S2CGmCommand.Builder resp = PbGm.S2CGmCommand.newBuilder();
//        resp.setResult(String.valueOf(++increaseIndex));
//        SceneLoggerTool.gameLogger.info("自增数字={}", increaseIndex);
//        player.session.send(PtCode.S2CGmCommand, resp.build());
    }

    /**
     * 执行gm命令
     *
     * @param command
     * @return
     */
    public String doGmCommand(String command) {
        player.info("gm command", command);
        String result = "fail";
        if (command.startsWith("/gm")) {
            String[] array = command.split("\\s+");
            if (array.length >= 2) {
                GMCmd gmCmd = ConfigTool.getGMConfig().getGMCmd(array[1]);
                if (gmCmd == null) {
                    player.error("not exist gm cmd", command);
                    return result;
                }

                String[] stringCmdArray = new String[array.length - 2];
                for (int i = 0; i < stringCmdArray.length; i++) {
                    stringCmdArray[i] = array[i + 2];
                }
                int[] intCmdArray = new int[array.length - 2];
                try {
                    for (int i = 0; i < intCmdArray.length; i++) {
                        intCmdArray[i] = Integer.valueOf(array[i + 2]);
                    }
                } catch (Exception e) {
                    intCmdArray = null;
                }

                result = gmCmd.call(player, stringCmdArray, intCmdArray);
            }
        } else {
            player.error("gm command invalid", command);
        }
        return result;
    }

    /**
     * 发送到global的gm命令
     *
     * @param player
     * @param stringCmd
     * @param intCmd
     * @return
     */
    public static String globalGm(Player player, String[] stringCmd, int[] intCmd) {
        player.getLogicChildWorld().postEvent2Global(Logic2GlobalEvent.GM_COMMAND, stringCmd, intCmd);
        return success;
    }

    /**
     * 服务器之间通讯
     *
     * @param player
     * @param stringCmd
     * @param intCmd
     * @return
     */
    public static String server2Server(Player player, String[] stringCmd, int[] intCmd) {
        //k=protocolID, v=msg
        int ptCode = PtCode.Center2LogicModifyUnionName;
        PbUnion.Center2LogicModifyUnionName req = PbUnion.Center2LogicModifyUnionName.newBuilder()
                .setPlayerID(player.getPlayerID())
                .setResult(true)
                .build();
        player.getLogicChildWorld().sendMsg2Scene(1, 1, player.getPlayerID(), ptCode, req);
        return success;
    }

    public static String saveToRedis(Player player, String[] stringCmd, int[] intCmd) {
        player.saveToRedis();
        return success;
    }

    public static String saveToFile(Player player, String[] stringCmd, int[] intCmd) {
        player.saveToFile();
        return success;
    }

    public static String parseBinFile(Player player, String[] stringCmd, int[] intCmd) {
        player.parseBinFile(stringCmd[0]);
        return success;
    }

    public static String pushOfflineMsg(Player player, String[] stringCmd, int[] intCmd) {
        long playerID = Long.parseLong(stringCmd[0]);
        int ptCode = PtCode.Center2LogicModifyUnionName;
        PbUnion.Center2LogicModifyUnionName req = PbUnion.Center2LogicModifyUnionName.newBuilder()
                .setPlayerID(playerID)
                .setResult(false)
                .build();
        player.getLogicChildWorld().pushOfflineMsg2Player(playerID, ptCode, req);
        return success;
    }

    //临时测试命令
    public static String test(Player player, String[] stringCmd, int[] intCmd) {
        switch (intCmd[0]) {
            case 1 -> {
                Request request = Request.cmd(Command.HGET).arg(RedisKey.PLAYER_LITE_INFO.value() + "1001").arg("playerID");
                RedisClientTool.send(player.getChildWorldIndex(), request, (isSuccess, response, errorMsg) -> {
                    if (isSuccess) {
                        player.info("redis success={}", response);
                    } else {
                        player.info("redis fail={}", errorMsg);
                    }
                });
            }
            case 2 -> {
                Request request = Request.cmd(Command.HSET).arg(RedisKey.PLAYER_LITE_INFO.value() + "1001").arg("playerID").arg("10000000000");
                RedisClientTool.send(player.getChildWorldIndex(), request, (isSuccess, response, errorMsg) -> {
                    if (isSuccess) {
                        player.info("redis success={}", response);
                    } else {
                        player.info("redis fail={}", errorMsg);
                    }
                });
            }
            case 3 -> {
                Request request = Request.cmd(Command.HMGET).arg(RedisKey.PLAYER_LITE_INFO.value() + "1001").arg("playerID").arg("name");
                RedisClientTool.send(player.getChildWorldIndex(), request, (isSuccess, response, errorMsg) -> {
                    if (isSuccess) {
                        long playerID = response.get(0).toLong();
                        String name = response.get(1).toString();
                        player.info("redis success={} playerID={}, name={}", response, playerID, name);
                    } else {
                        player.info("redis fail={}", errorMsg);
                    }
                });
            }
            case 4 -> {
                Request request = Request.cmd(Command.HMGET).arg(RedisKey.PLAYER_LITE_INFO.value() + "1002").arg("test").arg("playerID");
                RedisClientTool.send(player.getChildWorldIndex(), request, (isSuccess, response, errorMsg) -> {
                    if (isSuccess) {
                        player.info("redis size={}, {}", response.size(), response);
                        Response resp1 = response.get(0);
                        long playerID = resp1.toLong();
                        int age = response.get(1).toInteger();
                        String name = response.get(2).toString();
                        player.info("redis success={} playerID={}, age={}, name={}", response, playerID, age, name);
                    } else {
                        player.info("redis fail={}", errorMsg);
                    }
                });
            }
        }
        return success;
    }

    //新号直接变牛逼大号
    public static String nb(Player player, String[] stringCmd, int[] intCmd) {
        //TODO
        return success;
    }

    //获取玩家精简信息
    public static String getPlayerLiteInfo(Player player, String[] stringCmd, int[] intCmd) {
        long playerID = Long.parseLong(stringCmd[0]);
        PlayerLiteInfoCacheTool.getInstance().getPlayerLiteInfo(player.getChildWorldIndex(), playerID, playerLiteInfo -> {
            if (playerLiteInfo != null) {
                LoggerTool.systemLogger.info("playerLiteInfo={}", playerLiteInfo);
            }
        });
        return success;
    }

    //网络断线
    public static String netClose(Player player, String[] stringCmd, int[] intCmd) {
        player.getSession().getChannel().close();
        return success;
    }

    public static String oomTest(Player player, String[] stringCmd, int[] intCmd) {
        player.info("oomTest1 num={}", player.getLogicChildWorld().testMap.size());
        for (int i = 0; i < 200000000; i++) {
            player.getLogicChildWorld().testMap.add(i, new long[1024]);
        }
        player.info("oomTest2 num={}", player.getLogicChildWorld().testMap.size());
        return success;
    }

    //热更配置
    public static String hotfixConfig(Player player, String[] stringCmd, int[] intCmd) {
        ThreadPoolExecutorTool.execute(() -> {
            player.info("hotfixConfig begin load");
            try {
                WorldTool.getInstance().getGlobalWorld().handleAllConfigs(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            player.info("hotfixConfig end load");
        });
        return success;
    }

    //热更property
    public static String hotfixProperty(Player player, String[] stringCmd, int[] intCmd) {
        ThreadPoolExecutorTool.execute(() -> {
            player.info("hotfixProperty begin load {}", ConfigPropUtils.getValue("game.a"));
            try {
                ConfigPropUtils.load();
            } catch (Exception e) {
                e.printStackTrace();
            }
            player.info("hotfixProperty end load {}", ConfigPropUtils.getValue("game.a"));
        });
        return success;
    }

    //帮助
    public static String help(Player player, String[] stringCmd, int[] intCmd) {
        return ConfigTool.getGMConfig().getHelpDesc();
    }

    //资产
    public static String addAsset(Player player, String[] stringCmd, int[] intCmd) {
        player.getAssetModule().addAsset(intCmd[0], intCmd[1], BehaviorSourceConst.Gm);
        return success;
    }

    public static String removeAsset(Player player, String[] stringCmd, int[] intCmd) {
        player.getAssetModule().removeAsset(intCmd[0], intCmd[1], BehaviorSourceConst.Gm);
        return success;
    }

    public static String addAssetMax(Player player, String[] stringCmd, int[] intCmd) {
        player.getAssetModule().addAssetMax(intCmd[0], intCmd[1]);
        return success;
    }

    public static String removeAssetMax(Player player, String[] stringCmd, int[] intCmd) {
        player.getAssetModule().removeAssetMax(intCmd[0], intCmd[1]);
        return success;
    }

    //背包
    public static String addItem(Player player, String[] stringCmd, int[] intCmd) {
        boolean isBind = false;
        if (intCmd.length == 3 && intCmd[2] != 0) {
            isBind = true;
        }
        player.getBagModule().getPlayerBagItemContainer().addItem(intCmd[0], intCmd[1], isBind, BehaviorSourceConst.Gm);
        return success;
    }

    public static String addItem2(Player player, String[] stringCmd, int[] intCmd) {
        int[][] array = new int[intCmd.length / 2][];
        int pos = 0;
        for (int i = 0; i < intCmd.length;) {
            int[] data = new int[2];
            data[0] = intCmd[i];
            data[1] = intCmd[i + 1];
            i += 2;
            array[pos++] = data;
        }
        player.getBagModule().getPlayerBagItemContainer().addItems(array, false, BehaviorSourceConst.Gm);
        return success;
    }

    public static String addItem3(Player player, String[] stringCmd, int[] intCmd) {
        ObjectList<ItemData> itemList = new ObjectList<>(ItemData[]::new);
        for (int i = 0; i < intCmd.length;) {
            ItemData itemData = ItemFactory.create(intCmd[i], intCmd[i + 1]);
            itemList.add(itemData);
            i += 2;
        }
        player.getBagModule().getPlayerBagItemContainer().addItems(itemList, BehaviorSourceConst.Gm);
        return success;
    }

    public static String useItemByIndex(Player player, String[] stringCmd, int[] intCmd) {
        player.getBagModule().getPlayerBagItemContainer().useItemByIndex(intCmd[0], intCmd[1], BehaviorSourceConst.Gm);
        return success;
    }

    public static String removeItemByIndex(Player player, String[] stringCmd, int[] intCmd) {
        player.getBagModule().getPlayerBagItemContainer().removeItemByIndex(intCmd[0], intCmd[1], BehaviorSourceConst.Gm);
        return success;
    }

    public static String swapItemByIndex(Player player, String[] stringCmd, int[] intCmd) {
        player.getBagModule().getPlayerBagItemContainer().swapItemByIndex(intCmd[0], intCmd[1]);
        return success;
    }

    public static String splitItemByIndex(Player player, String[] stringCmd, int[] intCmd) {
        player.getBagModule().getPlayerBagItemContainer().splitItem(intCmd[0], intCmd[1]);
        return success;
    }

    public static String sortBag(Player player, String[] stringCmd, int[] intCmd) {
        player.getBagModule().getPlayerBagItemContainer().sort();
        return success;
    }

    public static String resetGridNum(Player player, String[] stringCmd, int[] intCmd) {
        player.getBagModule().getPlayerBagItemContainer().resetGridNum(intCmd[0]);
        return success;
    }

    public static String clearBag(Player player, String[] stringCmd, int[] intCmd) {
        player.getBagModule().getPlayerBagItemContainer().clearByGM();
        return success;
    }

    //bind aoi
    public static String addBindAOI(Player player, String[] stringCmd, int[] intCmd) {
//        Player otherPlayer = sceneChildWorld.getPlayerManager().getPlayer(intCmd[0]);
//        if (otherPlayer != null) {
//            otherPlayer.playerEntity.addEntityToBindAOI(player.playerEntity);
//        }
        return success;
    }

    public static String removeBindAOI(Player player, String[] stringCmd, int[] intCmd) {
//        Player otherPlayer = sceneChildWorld.getPlayerManager().getPlayer(intCmd[0]);
//        if (otherPlayer != null) {
//            otherPlayer.playerEntity.removeEntityFromBindAOI(player.playerEntity);
//        }
        return success;
    }

    public static String teleport(Player player, String[] stringCmd, int[] intCmd) {
//        Vector3 pos = Vector3.create();
//        pos.x = intCmd[0];
//        pos.z = intCmd[1];
//        player.playerEntity.teleport(pos);
        return success;
    }

    public static String createEntity(Player player, String[] stringCmd, int[] intCmd) {
//        int type = intCmd[0];
//        Vector3 pos = Vector3.create();
//        int id = intCmd[1];
//
//        pos.x = intCmd[2];
//        pos.z = intCmd[3];
//        Vector3 dir = Vector3.create();
//        dir.set(0, 0, 1);
//        player.entityModule.createEntity(type, id, pos, dir);
        return success;
    }

    public static String deleteEntity(Player player, String[] stringCmd, int[] intCmd) {
//        int entityID = intCmd[0];
//        Entity entity = player.playerEntity.scene.allEntity.get(entityID);
//        player.playerEntity.scene.removeEntityNextTick(entity);
        return success;
    }

    public static String changeFixedPosEntityPos(Player player, String[] stringCmd, int[] intCmd) {
//        int entityID = intCmd[0];
//        Vector3 pos = Vector3.create();
//        pos.x = intCmd[1];
//        pos.z = intCmd[2];
//        player.playerEntity.scene.changeFixedPosEntityPos(entityID, pos);
        return success;
    }

    //buff
    public static String addBuff(Player player, String[] stringCmd, int[] intCmd) {
//        player.playerEntity.addBuff(intCmd[0], intCmd[1]);
        return success;
    }

    public static String removeBuff(Player player, String[] stringCmd, int[] intCmd) {
//        player.playerEntity.removeBuff(intCmd[0]);
        return success;
    }

    //属性
    public static String addAttribute(Player player, String[] stringCmd, int[] intCmd) {
//        player.playerEntity.addAttribute((short) intCmd[0], intCmd[1]);
        return success;
    }

    public static String subAttribute(Player player, String[] stringCmd, int[] intCmd) {
//        player.playerEntity.subAttribute((short) intCmd[0], intCmd[1]);
        return success;
    }

    public static String showAttribute(Player player, String[] stringCmd, int[] intCmd) {
//        AttributeFightLogic attributeFightLogic = player.playerEntity.fightAllLogic.attributeFightLogic;
//        for (int i = 0; i < ConfigTool.getAttributeConfig().getAttrArraySize(); i++) {
//            int value = attributeFightLogic.getAttribute((short) i);
//            if (value != 0) {
//                SceneLoggerTool.gameLogger.info("id={}, value={}", i, value);
//            }
//        }
        return success;
    }

    public static String refreshAttribute(Player player, String[] stringCmd, int[] intCmd) {
//        player.playerEntity.fightAllLogic.attributeFightLogic.refreshAttributes();
        return success;
    }

    //state
    public static String setState(Player player, String[] stringCmd, int[] intCmd) {
//        short stateID = (short) intCmd[0];
//        boolean value = intCmd[1] > 0;
//        player.playerEntity.fightAllLogic.stateFightLogic.setState(stateID, value);
        return success;
    }

    public static String refreshState(Player player, String[] stringCmd, int[] intCmd) {
//        player.playerEntity.fightAllLogic.stateFightLogic.refreshState();
        return success;
    }

    public static String addPassiveSkill(Player player, String[] stringCmd, int[] intCmd) {
//        int skillID = intCmd[0];
//        int skillLevel = intCmd[1];
//        player.playerEntity.addPassiveSkillData(skillID, skillLevel);
        return success;
    }

    public static String removePassiveSkill(Player player, String[] stringCmd, int[] intCmd) {
//        int skillID = intCmd[0];
//        int skillLevel = intCmd[1];
//        player.playerEntity.removePassiveSkillData(skillID, skillLevel);
        return success;
    }

    //tileCache
    public static String addBoxObstacle(Player player, String[] stringCmd, int[] intCmd) {
//        float minX = intCmd[0];
//        float minY = intCmd[1];
//        float minZ = intCmd[2];
//        float length = intCmd[3];
//        float width = intCmd[4];
//        float height = intCmd[5];
//        int index = player.playerEntity.scene.addBoxObstacle(minX, minY, minZ, length, width, height);
//        return String.valueOf(index);
        return success;
    }

    public static String removeOneObstacle(Player player, String[] stringCmd, int[] intCmd) {
//        int index = intCmd[0];
//        player.playerEntity.scene.removeOneObstacle(index);
        return success;
    }

    public static String removeAllObstacle(Player player, String[] stringCmd, int[] intCmd) {
//        player.playerEntity.scene.removeAllObstacle();
        return success;
    }

    //role
    public static String addExp(Player player, String[] stringCmd, int[] intCmd) {
        player.getRoleModule().addExp(intCmd[0], BehaviorSourceConst.Gm);
        return success;
    }

    public static String addLevel(Player player, String[] stringCmd, int[] intCmd) {
        player.getRoleModule().addLevel(intCmd[0]);
        return success;
    }

    public static String resetRole(Player player, String[] stringCmd, int[] intCmd) {
        player.getRoleModule().resetByGm();
        return success;
    }

    //mail
    public static String addMail1(Player player, String[] stringCmd, int[] intCmd) {
        int type = intCmd[0];
        int titleID = intCmd[1];
        int contentID = intCmd[2];
        int behaviorSource = intCmd[3];
        player.getMailModule().addMail(type, titleID, contentID, behaviorSource);
        return success;
    }

    public static String addMail2(Player player, String[] stringCmd, int[] intCmd) {
        int type = Integer.parseInt(stringCmd[0]);
        int titleID = Integer.parseInt(stringCmd[1]);
        int contentID = Integer.parseInt(stringCmd[2]);
        int behaviorSource = Integer.parseInt(stringCmd[3]);
        ObjectList<String> titleArgs = new ObjectList<>(String[]::new);
        titleArgs.add(stringCmd[4]);
        ObjectList<String> contentArgs = new ObjectList<>(String[]::new);
        contentArgs.add(stringCmd[5]);
        player.getMailModule().addMail(type, titleID, contentID, behaviorSource, titleArgs, contentArgs);
        return success;
    }

    public static String addMail3(Player player, String[] stringCmd, int[] intCmd) {
        int type = intCmd[0];
        int titleID = intCmd[1];
        int contentID = intCmd[2];
        int behaviorSource = intCmd[3];
        int itemID = intCmd[4];
        int itemNum = intCmd[5];

        ObjectList<ItemData> itemList = null;
        if (itemID > 0) {
            ItemData itemData = ItemFactory.create(itemID, itemNum);
            itemList = new ObjectList<>(ItemData[]::new);
            itemList.add(itemData);
        }

        IntLongMap assetMap = null;
        if (intCmd.length > 6) {
            int assetType = intCmd[6];
            int assetNum = intCmd[7];
            assetMap = new IntLongMap();
            assetMap.put(assetType, assetNum);
        }
        player.getMailModule().addMail(type, titleID, contentID, behaviorSource, itemList, assetMap);
        return success;
    }

    public static String readMail(Player player, String[] stringCmd, int[] intCmd) {
        List<Long> list = new ArrayList<>();
        for (int mailID : intCmd) {
            list.add((long) mailID);
        }
        player.getMailModule().readMail(list);
        return success;
    }

    public static String receiveMailAttachment(Player player, String[] stringCmd, int[] intCmd) {
        List<Long> list = new ArrayList<>();
        for (int mailID : intCmd) {
            list.add((long) mailID);
        }
        player.getMailModule().receiveMailAttachment(list);
        return success;
    }

    public static String deleteMail(Player player, String[] stringCmd, int[] intCmd) {
        List<Long> list = new ArrayList<>();
        for (int mailID : intCmd) {
            list.add((long) mailID);
        }
        player.getMailModule().deleteMail(list);
        return success;
    }

    //function
    public static String openFunction(Player player, String[] stringCmd, int[] intCmd) {
        player.getFunctionModule().openFunction(intCmd[0]);
        return success;
    }

    //rank
    public static String getRankList(Player player, String[] stringCmd, int[] intCmd) {
        List<Integer> argsList = new ArrayList<>(intCmd.length);
        for (int arg : intCmd) {
            argsList.add(arg);
        }
        player.getRankModule().getRankList(argsList);
        return success;
    }

    public static String settlement(Player player, String[] stringCmd, int[] intCmd) {
        PlayerLevelRank.getInstance().settlement(player.getChildWorldIndex(), true, list -> {
            for (RedisRankSettleData settleData : list) {
                player.info("settle rank=", settleData.getRank(), " memberID=", settleData.getMemberID());
            }
        });
        return success;
    }


    //goal
    public static String triggerGoalEvent(Player player, String[] stringCmd, int[] intCmd) {
        int goalType = intCmd[0];
        int[] params = null;
        if (intCmd.length > 1) {
            params = new int[intCmd.length - 1];
            for (int i = 0; i < params.length; i++) {
                params[i] = intCmd[i + 1];
            }
        }
        player.getGoalModule().triggerGoalEvent(goalType, params);
        return success;
    }

    //quest
    public static String acceptQuest(Player player, String[] stringCmd, int[] intCmd) {
        player.getQuestModule().acceptQuestByGM(intCmd[0]);
        return success;
    }

    public static String commitQuest(Player player, String[] stringCmd, int[] intCmd) {
        player.getQuestModule().commitQuestByGM(intCmd[0]);
        return success;
    }

    //dungeon
    public static String enterDungeon(Player player, String[] stringCmd, int[] intCmd) {
//        int dungeonID = intCmd[0];
//        int level = intCmd[1];
//        player.playerDungeonModule.enterDungeon(dungeonID, level);
        return success;
    }

    public static String switchDungeonRoom(Player player, String[] stringCmd, int[] intCmd) {
//        int roomID = intCmd[0];
//        player.playerDungeonModule.switchDungeonRoom(roomID);
        return success;
    }

    public static String quitDungeon(Player player, String[] stringCmd, int[] intCmd) {
//        player.playerDungeonModule.quitDungeon();
        return success;
    }

    //entity
    public static String switchScene(Player player, String[] stringCmd, int[] intCmd) {
        int sceneID = intCmd[0];
        int lineID = intCmd[1];
        player.getSceneModule().switchScene(sceneID, lineID);
        return success;
    }

    public static String enterHomeScene(Player player, String[] stringCmd, int[] intCmd) {
        long playerID = Long.parseLong(stringCmd[0]);
        player.getSceneModule().enterHomeSceneLocation(1, playerID);
        return success;
    }

    public static String enterUnionScene(Player player, String[] stringCmd, int[] intCmd) {
        player.getSceneModule().enterUnionSceneLocation(2, 123);
        return success;
    }

    //union
    public static String getUnionList(Player player, String[] stringCmd, int[] intCmd) {
        int pageIndex = intCmd[0];
        player.getUnionModule().getUnionList(pageIndex, null);
        return success;
    }

    public static String createUnion(Player player, String[] stringCmd, int[] intCmd) {
        String name = stringCmd[0];
        player.getUnionModule().createUnion(player.getPlayerID(), name);
        return success;
    }

    public static String quitUnion(Player player, String[] stringCmd, int[] intCmd) {
        player.getUnionModule().quitUnion();
        return success;
    }

    public static String disbandUnion(Player player, String[] stringCmd, int[] intCmd) {
        player.getUnionModule().disbandUnion();
        return success;
    }

    public static String applyJoinUnion(Player player, String[] stringCmd, int[] intCmd) {
        long targetUnionID = Long.valueOf(stringCmd[0]);
        player.getUnionModule().applyJoinUnion(targetUnionID);
        return success;
    }

    public static String inviteJoinUnion(Player player, String[] stringCmd, int[] intCmd) {
        long targetPlayerID = Long.valueOf(stringCmd[0]);
        player.getUnionModule().inviteJoinUnion(ConfigConst.identifier, targetPlayerID);
        return success;
    }

    public static String handleBeInviteJoinUnion(Player player, String[] stringCmd, int[] intCmd) {
        long targetUnionID = Long.parseLong(stringCmd[0]);
        int handleResultType = Integer.parseInt(stringCmd[1]);
        player.getUnionModule().handleBeInviteJoinUnion(targetUnionID, handleResultType);
        return success;
    }

    public static String handleJoinUnion(Player player, String[] stringCmd, int[] intCmd) {
        int handleType = Integer.parseInt(stringCmd[0]);
        int handleResult = Integer.parseInt(stringCmd[1]);
        long targetPlayerID = Long.parseLong(stringCmd[2]);
        player.getUnionModule().handleJoinUnion(handleType, handleResult, targetPlayerID);
        return success;
    }

    public static String kickOutMember(Player player, String[] stringCmd, int[] intCmd) {
        long targetPlayerID = Long.parseLong(stringCmd[0]);
        player.getUnionModule().kickOutMember(targetPlayerID);
        return success;
    }

    public static String changeLeader(Player player, String[] stringCmd, int[] intCmd) {
        long targetPlayerID = Long.parseLong(stringCmd[0]);
        player.getUnionModule().changeLeader(targetPlayerID);
        return success;
    }

    public static String modifyUnionName(Player player, String[] stringCmd, int[] intCmd) {
        player.getUnionModule().modifyUnionName(stringCmd[0]);
        return success;
    }

    public static String modifyUnionSetting(Player player, String[] stringCmd, int[] intCmd) {
        PbUnion.PbUnionSettingData.Builder builder = PbUnion.PbUnionSettingData.newBuilder();
        int type = UnionUpdateTypeConst.ModifyJoinNeedApply;
        builder.setJoinNeedApply(true);

        player.getUnionModule().modifyUnionSetting(type, builder.build());
        return success;
    }

    public static String modifyUnionMemberPosition(Player player, String[] stringCmd, int[] intCmd) {
        long targetPlayerID = Long.parseLong(stringCmd[0]);
        int position = Integer.parseInt(stringCmd[1]);
        player.getUnionModule().modifyUnionMemberPosition(targetPlayerID, position);
        return success;
    }

    public static String changePermission(Player player, String[] stringCmd, int[] intCmd) {
        int position = Integer.parseInt(stringCmd[0]);
        long permission = Long.parseLong(stringCmd[1]);
        player.getUnionModule().changePermission(position, permission);
        return success;
    }

    //auction
    public static String auctionSearch(Player player, String[] stringCmd, int[] intCmd) {
        int auctionType = intCmd[0];
        player.getAuctionModule().auctionSearch(auctionType);
        return success;
    }

    public static String auctionShelfItem(Player player, String[] stringCmd, int[] intCmd) {
        int index = intCmd[0];
        int itemID = intCmd[1];
        int itemNum = intCmd[2];
        int price = intCmd[3];
        player.getAuctionModule().auctionShelfItem(index, itemID, itemNum, price);
        return success;
    }

    public static String auctionBuy(Player player, String[] stringCmd, int[] intCmd) {
        AuctionItemData auctionItemData = new AuctionItemData();
        int id = intCmd[0];
        int itemID = intCmd[1];
//        int itemNum = intCmd[2];
//        int price = intCmd[3];
        auctionItemData.setId(id);
        auctionItemData.setItemID(itemID);
        player.getAuctionModule().auctionBuy(auctionItemData);
        return success;
    }

    public static String auctionCancel(Player player, String[] stringCmd, int[] intCmd) {
        int auctionType = intCmd[0];
        int auctionID = intCmd[1];
        player.getAuctionModule().auctionCancelShelfItem(auctionType, auctionID);
        return success;
    }
}
