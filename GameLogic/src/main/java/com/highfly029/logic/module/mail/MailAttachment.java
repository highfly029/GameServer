package com.highfly029.logic.module.mail;

import com.highfly029.logic.common.container.item.ItemData;
import com.highfly029.utils.collection.IntLongMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName MailAttachment
 * @Description MailAttachment
 * @Author liyunpeng
 **/
public class MailAttachment {
    /**
     * 邮件物品
     */
    private ObjectList<ItemData> itemList;
    /**
     * 邮件货币
     */
    private IntLongMap assetMap;

    public ObjectList<ItemData> getItemList() {
        return itemList;
    }

    public void setItemList(ObjectList<ItemData> itemList) {
        this.itemList = itemList;
    }

    public IntLongMap getAssetMap() {
        return assetMap;
    }

    public void setAssetMap(IntLongMap assetMap) {
        this.assetMap = assetMap;
    }

    @Override
    public String toString() {
        return "MailAttachment{" +
                "itemList=" + itemList +
                ", assetMap=" + assetMap +
                '}';
    }
}
