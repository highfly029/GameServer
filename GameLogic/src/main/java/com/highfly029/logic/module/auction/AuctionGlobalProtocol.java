package com.highfly029.logic.module.auction;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.auction.AuctionItemData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbAuction;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.template.behaviorSource.BehaviorSourceConst;
import com.highfly029.common.template.mail.MailTypeConst;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.logic.common.container.item.ItemData;
import com.highfly029.logic.common.container.item.ItemFactory;
import com.highfly029.logic.module.mail.MailData;
import com.highfly029.logic.module.mail.MailTool;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.utils.PbLogicUtils;
import com.highfly029.logic.world.LogicGlobalWorld;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName AuctionGlobalProtocol
 * @Description AuctionGlobalProtocol
 * @Author liyunpeng
 **/
public class AuctionGlobalProtocol implements IRegisterServerGlobalProtocol {
    private LogicGlobalWorld logicGlobalWorld;

    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {
        logicGlobalWorld = (LogicGlobalWorld) globalWorld;
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicAuctionSearch, this::onAuctionSearch);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicAuctionShelfItem, this::onAuctionShelfItem);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicAuctionBuy, this::onAuctionBuy);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicAuctionBuyToSeller, this::onAuctionBuyToSeller);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicAuctionCancelShelfItem, this::onAuctionCancelShelfItem);
        globalWorld.registerServerGlobalProtocol(PtCode.Center2LogicAuctionExpireCancel, this::onAuctionExpireCancel);
    }

    /**
     * 收到中心服的拍卖行查询结果
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void onAuctionSearch(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbAuction.Center2LogicAuctionSearch req = PbAuction.Center2LogicAuctionSearch.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("onAuctionSearch not exist playerID={}", playerID);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

    /**
     * 收到中心服的拍卖行上架结果
     *
     * @param session
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void onAuctionShelfItem(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbAuction.Center2LogicAuctionShelfItem req = PbAuction.Center2LogicAuctionShelfItem.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("onAuctionShelfItem not exist playerID={}", playerID);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

    /**
     * 收到中心服的拍卖行购买结果
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void onAuctionBuy(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbAuction.Center2LogicAuctionBuy req = PbAuction.Center2LogicAuctionBuy.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        int index = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(playerID);
        if (index <= 0) {
            LogicLoggerTool.gameLogger.error("onAuctionBuy not exist playerID={}", playerID);
            return;
        }
        WorldTool.addChildWorldLogicEvent(index, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

    /**
     * 出售者收到中心服的拍卖行购买结果
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void onAuctionBuyToSeller(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbAuction.Center2LogicAuctionBuyToSeller req = PbAuction.Center2LogicAuctionBuyToSeller.parseFrom(packet.getData());
        long playerID = req.getSellerPlayerID();
        logicGlobalWorld.pushOfflineMsg2PlayerInCurrentServer(playerID, ptCode, req);
    }

    /**
     * 拍卖行取消上架物品结果
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void onAuctionCancelShelfItem(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbAuction.Center2LogicAuctionCancelShelfItem req = PbAuction.Center2LogicAuctionCancelShelfItem.parseFrom(packet.getData());
        long playerID = req.getPlayerID();
        logicGlobalWorld.pushOfflineMsg2PlayerInCurrentServer(playerID, ptCode, req);
    }

    /**
     * 拍卖行上架物品到期下架
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void onAuctionExpireCancel(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbAuction.Center2LogicAuctionExpireCancel req = PbAuction.Center2LogicAuctionExpireCancel.parseFrom(packet.getData());
        long playerID = req.getSellerPlayerID();
        AuctionItemData auctionItemData = PbCommonUtils.auctionItemDataPb2Obj(req.getAuctionItemData());
        ItemData itemData = null;
        if (auctionItemData.getItemData() != null && !auctionItemData.getItemData().isEmpty()) {
            try {
                PbCommon.PbItemData pbItemData = PbCommon.PbItemData.parseFrom(auctionItemData.getItemData());
                itemData = PbLogicUtils.itemPb2Obj(pbItemData);
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        } else {
            itemData = ItemFactory.create(auctionItemData.getItemID(), auctionItemData.getItemNum());
        }
        if (itemData == null) {
            LogicLoggerTool.gameLogger.error("onAuctionExpireCancel itemData is null playerID={}, auctionID={}", playerID, auctionItemData.getId());
            return;
        }
        ObjectList<ItemData> itemList = new ObjectList<>(ItemData[]::new);
        itemList.add(itemData);
        MailData mailData = MailTool.createMailData(MailTypeConst.Normal, 0, 0, BehaviorSourceConst.Gm, null, null, itemList, null);
        logicGlobalWorld.addMail(playerID, mailData);

        logicGlobalWorld.pushOfflineMsg2PlayerInCurrentServer(playerID, ptCode, req);
    }
}
