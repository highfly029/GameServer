package com.highfly029.logic.module.mail;

import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName MailData
 * @Description MailData
 * @Author liyunpeng
 **/
public class MailData {
    /**
     * 邮件唯一id
     */
    private long mailID;
    /**
     * 邮件类型
     */
    private int type;
    /**
     * 邮件标题ID
     */
    private int titleID;
    /**
     * 邮件内容ID
     */
    private int contentID;
    /**
     * 额外参数
     */
    private MailExtraArgs mailExtraArgs;
    /**
     * 邮件附件
     */
    private MailAttachment attachment;
    /**
     * 是否已读
     */
    private int isRead;
    /**
     * 是否领取过
     */
    private int isReceive;
    /**
     * 行为来源
     */
    private int behaviorSource;
    /**
     * 创建时间
     */
    private long createTime;

    public MailExtraArgs getMailExtraArgs() {
        return mailExtraArgs;
    }

    public void setMailExtraArgs(MailExtraArgs mailExtraArgs) {
        this.mailExtraArgs = mailExtraArgs;
    }

    public long getMailID() {
        return mailID;
    }

    public void setMailID(long mailID) {
        this.mailID = mailID;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTitleID() {
        return titleID;
    }

    public void setTitleID(int titleID) {
        this.titleID = titleID;
    }

    public int getContentID() {
        return contentID;
    }

    public void setContentID(int contentID) {
        this.contentID = contentID;
    }

    public MailAttachment getAttachment() {
        return attachment;
    }

    public void setAttachment(MailAttachment attachment) {
        this.attachment = attachment;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int getIsReceive() {
        return isReceive;
    }

    public void setIsReceive(int isReceive) {
        this.isReceive = isReceive;
    }

    public int getBehaviorSource() {
        return behaviorSource;
    }

    public void setBehaviorSource(int behaviorSource) {
        this.behaviorSource = behaviorSource;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "MailData{" +
                "mailID=" + mailID +
                ", type=" + type +
                ", titleID=" + titleID +
                ", contentID=" + contentID +
                ", mailExtraArgs=" + mailExtraArgs +
                ", attachment=" + attachment +
                ", isRead=" + isRead +
                ", isReceive=" + isReceive +
                ", behaviorSource=" + behaviorSource +
                ", createTime=" + createTime +
                '}';
    }
}
