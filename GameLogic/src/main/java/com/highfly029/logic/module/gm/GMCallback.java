package com.highfly029.logic.module.gm;

import com.highfly029.logic.module.Player;

/**
 * @ClassName GMCallback
 * @Description gm命令回调
 * @Author liyunpeng
 **/
public interface GMCallback {
    String call(Player player, String[] stringCmdArray, int[] intCmdArray);
}
