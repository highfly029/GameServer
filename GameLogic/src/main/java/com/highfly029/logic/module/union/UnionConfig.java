package com.highfly029.logic.module.union;

import com.highfly029.common.template.union.UnionTemplate;
import com.highfly029.common.templateBase.BaseTemplate;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.utils.ACAutomaton;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName UnionConfig
 * @Description UnionConfig
 * @Author liyunpeng
 **/
public class UnionConfig extends BaseGlobalConfig {
    /**
     * 联盟配置
     */
    private UnionTemplate unionTemplate;
    /**
     * 联盟名字敏感词检测
     */
    private ACAutomaton unionNameACAutomaton;

    @Override
    public void check(boolean isHotfix) {

    }

    @Override
    protected void dataProcess() {
        char[] ignore = new char[]{};
        char[] caseArray = new char[]{'C'};
        unionNameACAutomaton = new ACAutomaton(ignore, caseArray, '*');
        ObjectList<String> list = new ObjectList<>(String[]::new);

        list.add("C");

        unionNameACAutomaton.init(list);

        ObjectList<BaseTemplate> templateObjectList = TemplateTool.dataTemplates.get(UnionTemplate.class);
        unionTemplate = (UnionTemplate) templateObjectList.get(0);
    }

    /**
     * 获取联盟模版
     *
     * @return
     */
    public UnionTemplate getUnionTemplate() {
        return unionTemplate;
    }

    /**
     * 联盟名字是否合法
     *
     * @param name
     * @return
     */
    public boolean invalid(String name) {
        return unionNameACAutomaton.contain(name);
    }
}
