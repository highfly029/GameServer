package com.highfly029.logic.module.union;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.data.union.UnionMember;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.common.redis.PlayerLiteInfoKeyEnum;
import com.highfly029.common.redis.RedisKey;
import com.highfly029.common.template.asset.PlayerAssetConst;
import com.highfly029.common.template.behaviorSource.BehaviorSourceConst;
import com.highfly029.common.template.infoCode.InfoCodeConst;
import com.highfly029.common.template.union.UnionPositionConst;
import com.highfly029.common.template.union.UnionTemplate;
import com.highfly029.common.template.union.UnionUpdateTypeConst;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.tool.RedisClientTool;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.logic.module.BaseModule;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.tool.ConfigTool;

import io.vertx.redis.client.Command;
import io.vertx.redis.client.Request;

/**
 * @ClassName UnionModule
 * @Description 联盟模块
 * @Author liyunpeng
 **/
public class UnionModule extends BaseModule {
    /**
     * 玩家所在联盟内存数据
     */
    private PlayerUnion playerUnion;

    /*********************************** memory cache ***********************************
     /**
     * 创建联盟过期时间 防止连点
     */
    private long createUnionExpireTime;
    /**
     * 创建联盟限制间隔 防止连点
     */
    private static final int CREATE_UNION_LIMIT_DURATION = 10000;

    /**
     * 预加入联盟的锁过期时间
     */
    private long prepareJoinUnionExpireTime;
    /**
     * 预加入联盟的加锁时间
     */
    private static final int PREPARE_JOIN_UNION_LOCK_TIME = 60 * 1000;


    @Override
    public void onPlayerInitData() {
        setData(new PlayerUnionData());
    }

    @Override
    public PlayerUnionData getData() {
        return (PlayerUnionData) super.getData();
    }

    @Override
    public void onPlayerBeforeLogin() {
        super.onPlayerBeforeLogin();
    }

    @Override
    public void onPlayerAfterLogin() {
        //如果有联盟 向center服拉取联盟信息
        if (getData().getUnionID() > 0) {
            PbUnion.Logic2CenterPullPlayerUnionInfo.Builder builder = PbUnion.Logic2CenterPullPlayerUnionInfo.newBuilder();
            builder.setPlayerID(player.getPlayerID());
            builder.setUnionID(getData().getUnionID());
            builder.setLocation(PbCommonUtils.locationObj2Pb(player.getLocation()));

            player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterPullPlayerUnionInfo, builder.build());
        }
    }

    @Override
    public void onPlayerLogout() {
        super.onPlayerLogout();
        if (getData().getUnionID() > 0) {
            PbUnion.Logic2CenterMemberLogout.Builder builder = PbUnion.Logic2CenterMemberLogout.newBuilder();
            builder.setPlayerID(player.getPlayerID());
            builder.setUnionID(getData().getUnionID());
            player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterMemberLogout, builder.build());
        }
    }

    /**
     * 同步联盟数据后更新玩家内存
     *
     * @param unionID
     * @param name
     * @param pbUnionData
     * @param isPull
     */
    public void onSyncPlayerUnionInfo(long unionID, String name, PbUnion.PbUnionData pbUnionData, boolean isPull) {
        if (isPull && unionID != getData().getUnionID()) {
            player.warn("onSyncPlayerUnionInfo change", unionID, getData().getUnionID());
        }


        getData().setUnionID(unionID);
        getData().setChange(true);
        playerUnion = PlayerUnion.create();
        try {
            playerUnion.load(pbUnionData.toByteArray());
        } catch (InvalidProtocolBufferException e) {
            player.error("onSyncPlayerUnionInfo", e);
        }
        playerUnion.setUnionID(unionID);
        playerUnion.setUnionName(name);
        if (!isPull) {
            onPlayerLiteUnionDataChange();
        }
    }

    /**
     * 获取联盟列表
     *
     * @param pageIndex
     */
    public void getUnionList(int pageIndex, String unionName) {
        PbUnion.Logic2CenterGetUnionList.Builder builder = PbUnion.Logic2CenterGetUnionList.newBuilder();
        builder.setPageIndex(pageIndex);
        if (unionName != null) {
            builder.setUnionName(unionName);
        }
        builder.setPlayerID(player.getPlayerID());
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterGetUnionList, builder.build());
    }

    /**
     * 创建联盟
     *
     * @param createPlayerID 创建者id
     * @param name           联盟名字
     */
    public void createUnion(long createPlayerID, String name) {
        if (getData().getUnionID() > 0) {
            player.sendInfoCode(InfoCodeConst.UnknownError);
            player.warn("createUnion duplicate");
            return;
        }
        long now = SystemTimeTool.getMillTime();
        if (createUnionExpireTime > 0 && now < createUnionExpireTime) {
            player.warn("createUnion frequent", createUnionExpireTime);
            return;
        }

        UnionConfig unionConfig = ConfigTool.getUnionConfig();
        UnionTemplate unionTemplate = unionConfig.getUnionTemplate();

        if (name == null || name.length() == 0 || name.length() > unionTemplate.getNameMaxCharNum()) {
            player.warn("createUnion invalid name length", name);
            return;
        }
        if (!player.checkAllRoleCondition(unionTemplate.getCreateCondition())) {
            player.warn("createUnion checkCondition invalid", name);
            return;
        }
        if (!player.getAssetModule().hasAsset(PlayerAssetConst.Diamond, unionTemplate.getCreateCostNum())) {
            player.warn("createUnion asset not enough", name);
            return;
        }
        //检测联盟名字合法性
        if (unionConfig.invalid(name)) {
            player.warn("createUnion invalid name", name);
            return;
        }

        player.getAssetModule().removeAsset(PlayerAssetConst.Diamond, unionTemplate.getCreateCostNum(), BehaviorSourceConst.CreateUnion);
        createUnionExpireTime = now + CREATE_UNION_LIMIT_DURATION;

        PbUnion.Logic2CenterCreateUnion.Builder builder = PbUnion.Logic2CenterCreateUnion.newBuilder();
        builder.setCreatePlayerID(player.getPlayerID());
        builder.setName(name);
        builder.setLocation(PbCommonUtils.locationObj2Pb(player.getLocation()));
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterCreateUnion, builder.build());
        player.info("createUnion 2 center", name);
    }

    /**
     * 创建联盟结果
     *
     * @param result
     * @param unionID
     */
    public void onCreateUnionResult(boolean result, long unionID, String name, PbUnion.PbUnionData pbUnionData, boolean isPull) {
        if (result) {
            onSyncPlayerUnionInfo(unionID, name, pbUnionData, isPull);
        } else {
            //回滚之前的消耗
            UnionTemplate unionTemplate = ConfigTool.getUnionConfig().getUnionTemplate();
            player.getAssetModule().addAsset(PlayerAssetConst.Diamond, unionTemplate.getCreateCostNum(), BehaviorSourceConst.CreateUnionFail);

            createUnionExpireTime = 0;
        }

        PbUnion.S2CCreateUnion.Builder builder = PbUnion.S2CCreateUnion.newBuilder();
        builder.setResult(result);
        player.send(PtCode.S2CCreateUnion, builder.build());
    }


    /**
     * 主动退出联盟
     */
    public void quitUnion() {
        if (getData().getUnionID() <= 0) {
            player.warn("quitUnion dont have union");
            return;
        }
        PbUnion.Logic2CenterQuitUnion.Builder builder = PbUnion.Logic2CenterQuitUnion.newBuilder();
        builder.setPlayerID(player.getPlayerID());
        builder.setUnionID(getData().getUnionID());
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterQuitUnion, builder.build());
    }

    /**
     * 主动退出联盟结果
     *
     * @param result
     * @param unionID
     * @param errorCode
     */
    public void quitUnionResult(boolean result, long unionID, int errorCode) {
        player.info("quitUnionResult", unionID, result, errorCode);
        if (result) {
            getData().setUnionID(0);
            getData().setChange(true);
        }
        PbUnion.S2CQuitUnion.Builder builder = PbUnion.S2CQuitUnion.newBuilder();
        builder.setResult(result).setErrorCode(errorCode);
        player.send(PtCode.S2CQuitUnion, builder.build());

    }

    /**
     * 解散联盟
     */
    public void disbandUnion() {
        if (getData().getUnionID() <= 0) {
            player.warn("disbandUnion not exist union");
            return;
        }
        PbUnion.Logic2CenterDisbandUnion.Builder builder = PbUnion.Logic2CenterDisbandUnion.newBuilder();
        builder.setPlayerID(player.getPlayerID());
        builder.setUnionID(getData().getUnionID());
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterDisbandUnion, builder.build());
    }

    /**
     * 申请加入联盟
     *
     * @param targetUnionID
     */
    public void applyJoinUnion(long targetUnionID) {
        if (getData().getUnionID() > 0) {
            player.warn("applyJoinUnion but have union", getData().getUnionID(), targetUnionID);
            return;
        }
        //记录申请列表
//        if (getData().checkApplyJoinList(targetUnionID)) {
//            player.warn("applyJoinUnion duplicate apply join", targetUnionID);
//            return;
//        }
        PbUnion.Logic2CenterApplyJoinUnion.Builder builder = PbUnion.Logic2CenterApplyJoinUnion.newBuilder();
        builder.setPlayerID(player.getPlayerID());
        builder.setUnionID(targetUnionID);
        builder.setLevel(player.getRoleModule().getLevel());
        builder.setLogicServerID(ConfigConst.identifier);
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterApplyJoinUnion, builder.build());
    }


    /**
     * 邀请加入联盟
     *
     * @param logicServerID
     * @param targetPlayerID 目标玩家id
     */
    public void inviteJoinUnion(int logicServerID, long targetPlayerID) {
        if (getData().getUnionID() == 0) {
            player.sendInfoCode(InfoCodeConst.UnknownError);
            return;
        }

        //检测被邀请的玩家有没有联盟 只有目标玩家在同一个线程中才会默认检测
        Player target = player.getLogicChildWorld().getPlayerManager().getPlayer(targetPlayerID);
        if (target != null) {
            long targetUnionID;
            if ((targetUnionID = target.getUnionModule().getData().getUnionID()) > 0) {
                player.error("inviteJoinUnion targetPlayer exist union", targetPlayerID, targetUnionID);
                return;
            }
        }

        PbUnion.Logic2CenterInviteJoinUnion.Builder builder = PbUnion.Logic2CenterInviteJoinUnion.newBuilder();
        builder.setPlayerID(player.getPlayerID());
        builder.setTargetPlayerID(targetPlayerID);
        builder.setUnionID(getData().getUnionID());
        builder.setLogicServerID(logicServerID);
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterInviteJoinUnion, builder.build());
    }

    /**
     * 邀请加入联盟通知
     *
     * @param operationID
     * @param unionID
     */
    public void onInviteJoinUnionNotice(long operationID, long unionID, String name) {
        long selfUnionID = getData().getUnionID();
        if (selfUnionID > 0) {
            player.warn("onInviteJoinUnionNotice already exist union", selfUnionID);
            return;
        }
        UnionTemplate unionTemplate = ConfigTool.getUnionConfig().getUnionTemplate();
        getData().addBeInviteJoinList(unionID, name);
        getData().setChange(true);

        //通知玩家被邀请
        PbUnion.S2CInviteJoinUnion.Builder builder = PbUnion.S2CInviteJoinUnion.newBuilder();
        builder.setOperatorPlayerID(operationID);
        builder.setUnionID(unionID);
        builder.setName(name);
        player.send(PtCode.S2CInviteJoinUnion, builder.build());
    }

    /**
     * 加入联盟检测
     *
     * @param operatorID
     * @param unionID
     * @param type
     */
    public void onJoinUnionDoubleCheck(long operatorID, long unionID, int type) {
        PbUnion.Logic2CenterJoinUnionDoubleCheckResult.Builder builder = PbUnion.Logic2CenterJoinUnionDoubleCheckResult.newBuilder();
        boolean result = false;
        do {
            //已经加入别的联盟了
            if (getData().getUnionID() > 0) {
                player.warn("onJoinUnionDoubleCheck already has unionID", unionID, operatorID);
                break;
            }
            long now = SystemTimeTool.getMillTime();
            //可能正在加入别的联盟过程中
            if (prepareJoinUnionExpireTime > 0 && now < prepareJoinUnionExpireTime) {
                player.warn("onJoinUnionDoubleCheck prepareJoinUnionExpireTime", prepareJoinUnionExpireTime, unionID, operatorID);
                break;
            }
            prepareJoinUnionExpireTime = now + PREPARE_JOIN_UNION_LOCK_TIME;
            builder.setLocation(PbCommonUtils.locationObj2Pb(player.getLocation()));
            result = true;
        } while (false);
        builder.setResult(result);
        builder.setOperatorPlayerID(operatorID);
        builder.setTargetPlayerID(player.getPlayerID());
        builder.setUnionID(unionID);
        builder.setType(type);

        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterJoinUnionDoubleCheckResult, builder.build());
    }

    /**
     * 处理入盟消息
     *
     * @param handleType
     * @param handleResult
     * @param targetPlayerID
     */
    public void handleJoinUnion(int handleType, int handleResult, long targetPlayerID) {
        long unionID = getData().getUnionID();
        if (unionID == 0) {
            player.warn("handleJoinUnion not exist union", unionID);
            return;
        }

        PbUnion.Logic2CenterHandleJoinUnion.Builder builder = PbUnion.Logic2CenterHandleJoinUnion.newBuilder();
        builder.setPlayerID(player.getPlayerID());
        builder.setHandleType(handleType);
        builder.setHandleResult(handleResult);
        builder.setTargetPlayerID(targetPlayerID);
        builder.setUnionID(unionID);
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterHandleJoinUnion, builder.build());
    }

    /**
     * 玩家响应邀请加入联盟
     *
     * @param targetUnionID
     */
    public void handleBeInviteJoinUnion(long targetUnionID, int handleResultType) {
        long playerID = player.getPlayerID();
        if (getData().getUnionID() > 0) {
            player.warn("handleBeInviteJoinUnion already exist union", getData().getUnionID(), targetUnionID);
            return;
        }
        long now = SystemTimeTool.getMillTime();

        //可能正在加入别的联盟过程中
        if (prepareJoinUnionExpireTime > 0 && now < prepareJoinUnionExpireTime) {
            player.warn("handleBeInviteJoinUnion prepareJoinUnionExpire", prepareJoinUnionExpireTime, targetUnionID);
            return;
        }
        PbUnion.Logic2CenterHandleInviteJoinUnion.Builder builder = PbUnion.Logic2CenterHandleInviteJoinUnion.newBuilder();
        builder.setPlayerID(playerID);
        builder.setUnionID(targetUnionID);
        builder.setLocation(PbCommonUtils.locationObj2Pb(player.getLocation()));
        builder.setHandleResultType(handleResultType);

        prepareJoinUnionExpireTime = now + PREPARE_JOIN_UNION_LOCK_TIME;
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterHandleInviteJoinUnion, builder.build());
    }

    /**
     * 玩家响应邀请加入联盟结果
     *
     * @param unionID
     * @param result
     */
    public void onHandleBeInviteJoinUnionResult(long unionID, boolean result) {
        //清理缓存
        prepareJoinUnionExpireTime = 0;
    }

    /**
     * 踢出成员
     *
     * @param targetPlayerID
     */
    public void kickOutMember(long targetPlayerID) {
        long playerID = player.getPlayerID();
        if (getData().getUnionID() == 0) {
            player.warn("kickOutMember not exist union");
            return;
        }
        PbUnion.Logic2CenterKickOutUnionMember.Builder builder = PbUnion.Logic2CenterKickOutUnionMember.newBuilder();

        builder.setPlayerID(playerID);
        builder.setUnionID(getData().getUnionID());
        builder.setTargetPlayerID(targetPlayerID);
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterKickOutUnionMember, builder.build());
    }

    /**
     * 更换盟主
     *
     * @param targetPlayerID
     */
    public void changeLeader(long targetPlayerID) {
        long playerID = player.getPlayerID();
        if (getData().getUnionID() == 0) {
            player.warn("changeLeader not exist union");
            return;
        }
        UnionMember unionMember = playerUnion.getUnionMember(playerID);

        if (unionMember.getPosition() != UnionPositionConst.Leader) {
            player.warn("changeLeader must be leader");
            return;
        }
        UnionMember targetMember = playerUnion.getUnionMember(targetPlayerID);
        if (targetMember == null) {
            player.warn("changeLeader not exist targetPlayerID", getData().getUnionID(), targetPlayerID);
            return;
        }
        PbUnion.Logic2CenterChangeLeader.Builder builder = PbUnion.Logic2CenterChangeLeader.newBuilder();
        builder.setPlayerID(playerID);
        builder.setUnionID(getData().getUnionID());
        builder.setTargetPlayerID(targetPlayerID);
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterChangeLeader, builder.build());
    }

    /**
     * 修改联盟名字
     *
     * @param name
     */
    public void modifyUnionName(String name) {
        long unionID = getData().getUnionID();
        long playerID = player.getPlayerID();
        if (unionID == 0) {
            player.warn("modifyUnionName not exist unionID");
            return;
        }

        //TODO 消耗

        //检测联盟名字合法性
        if (ConfigTool.getUnionConfig().invalid(name)) {
            player.warn("modifyUnionName invalid name", unionID, name);
            return;
        }

        PbUnion.Logic2CenterModifyUnionName.Builder builder = PbUnion.Logic2CenterModifyUnionName.newBuilder();
        builder.setName(name);
        builder.setPlayerID(playerID);
        builder.setUnionID(unionID);
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterModifyUnionName, builder.build());
    }

    /**
     * 修改联盟名字结果
     *
     * @param result
     */
    public void onModifyUnionNameResult(boolean result) {
        if (result == false) {
            //TODO 回滚消耗
        }
        player.info("onModifyUnionNameResult", result);
    }

    /**
     * 修改联盟设置
     *
     * @param settingType
     * @param pbUnionSettingData
     */
    public void modifyUnionSetting(int settingType, PbUnion.PbUnionSettingData pbUnionSettingData) {
        long unionID = getData().getUnionID();
        if (unionID <= 0) {
            player.warn("modifyUnionSetting not exist union");
            return;
        }
        if (settingType == UnionUpdateTypeConst.ModifyNotice) {
            UnionConfig unionConfig = ConfigTool.getUnionConfig();
            UnionTemplate unionTemplate = unionConfig.getUnionTemplate();
            String notice = pbUnionSettingData.getNotice();
            if (notice.length() > unionTemplate.getNoticeMaxCharNum()) {
                player.warn("modifyUnionSetting notice invalid length ", unionID, notice);
                return;
            }
            if (unionConfig.invalid(notice)) {
                player.warn("modifyUnionSetting notice invalid", unionID, notice);
                return;
            }
        }

        PbUnion.Logic2CenterModifyUnionSetting.Builder builder = PbUnion.Logic2CenterModifyUnionSetting.newBuilder();
        builder.setPlayerID(player.getPlayerID());
        builder.setUnionID(unionID);
        builder.setSettingType(settingType);
        builder.setSetting(pbUnionSettingData);
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterModifyUnionSetting, builder.build());
    }

    /**
     * 修改联盟成员职位
     *
     * @param targetPlayerID
     * @param position
     */
    public void modifyUnionMemberPosition(long targetPlayerID, int position) {
        long unionID = getData().getUnionID();
        if (unionID == 0) {
            player.warn("modifyUnionMemberPosition not exist union", unionID);
            return;
        }
        if (position <= 0 || position > UnionPositionConst.count) {
            player.warn("modifyUnionMemberPosition invalid position", position, targetPlayerID);
            return;
        }
        PbUnion.Logic2CenterModifyUnionMemberPosition.Builder builder = PbUnion.Logic2CenterModifyUnionMemberPosition.newBuilder();
        builder.setPlayerID(player.getPlayerID());
        builder.setUnionID(unionID);
        builder.setTargetPlayerID(targetPlayerID);
        builder.setPosition(position);
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterModifyUnionMemberPosition, builder.build());
    }

    /**
     * 修改权限
     *
     * @param position
     * @param permission
     */
    public void changePermission(int position, long permission) {
        long unionID = getData().getUnionID();
        if (unionID == 0) {
            player.warn("changePermission not exist union", unionID);
            return;
        }
        if (position <= 0 || position > UnionPositionConst.count) {
            player.warn("changePermission invalid position", position);
            return;
        }
        PbUnion.Logic2CenterChangePermission.Builder builder = PbUnion.Logic2CenterChangePermission.newBuilder();
        builder.setPlayerID(player.getPlayerID());
        builder.setUnionID(unionID);
        builder.setPosition(position);
        builder.setPermission(permission);
        player.getLogicChildWorld().getUnionSession().sendMsgS2S(PtCode.Logic2CenterChangePermission, builder.build());
    }

    /**
     * 更新联盟数据
     *
     * @param pbUpdateUnionData
     */
    public void updateUnionDataFromCenter(PbUnion.PbUpdateUnionData pbUpdateUnionData) {
        int updateType = pbUpdateUnionData.getUpdateType();
        //是否需要落地
        boolean realChange = getData().isChange();
        switch (updateType) {
            case UnionUpdateTypeConst.Disband -> {
                getData().setUnionID(0);
                realChange = true;
                playerUnion = null;
                player.warn("UnionUpdateTypeConst.Disband");
                onPlayerLiteUnionDataChange();
            }
            case UnionUpdateTypeConst.AddApplyJoinList -> {
                long targetPlayerID = pbUpdateUnionData.getLongValue1();
                int logicServerID = pbUpdateUnionData.getIntValue1();
                playerUnion.addApplyJoinList(targetPlayerID, logicServerID);
                player.warn("UnionUpdateTypeConst.AddApplyJoinList", targetPlayerID, logicServerID);
            }
            case UnionUpdateTypeConst.RemoveApplyJoinList -> {
                long operationID = pbUpdateUnionData.getLongValue1();
                long targetPlayerID = pbUpdateUnionData.getLongValue2();
                playerUnion.removeApplyJoinList(targetPlayerID);
                player.warn("UnionUpdateTypeConst.RemoveApplyJoinList", operationID, targetPlayerID);
            }
            case UnionUpdateTypeConst.AddInviteJoinList -> {
                long targetPlayerID = pbUpdateUnionData.getLongValue1();
                int logicServerID = pbUpdateUnionData.getIntValue1();
                playerUnion.addInviteJoinList(targetPlayerID, logicServerID);
                player.warn("UnionUpdateTypeConst.AddInviteJoinList", targetPlayerID, logicServerID);
            }
            case UnionUpdateTypeConst.RemoveInviteJoinList -> {
                long targetPlayerID = pbUpdateUnionData.getLongValue1();
                playerUnion.removeInviteJoinList(targetPlayerID);
                player.warn("UnionUpdateTypeConst.RemoveInviteJoinList", targetPlayerID);
            }
            case UnionUpdateTypeConst.AddMember -> {
                PbUnion.PbUnionMemberData pbUnionMemberData = pbUpdateUnionData.getMember();
                UnionMember unionMember = UnionMember.create(playerUnion);
                try {
                    unionMember.load(pbUnionMemberData.toByteArray());
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }
                playerUnion.addUnionMember(unionMember.getMemberID(), unionMember);
                player.warn("UnionUpdateTypeConst.AddMember", unionMember);
            }
            case UnionUpdateTypeConst.RemoveMember -> {
                long memberID = pbUpdateUnionData.getLongValue1();
                //是否主动退出
                boolean initiative = pbUpdateUnionData.getBoolValue1();
                playerUnion.removeUnionMember(memberID);
                player.warn("UnionUpdateTypeConst.RemoveMember", memberID);
                //被删除的成员是自己 被踢或者主动退出
                if (memberID == player.getPlayerID()) {
                    realChange = true;
                    getData().setUnionID(0);
                    playerUnion = null;
                    onPlayerLiteUnionDataChange();
                }
            }
            case UnionUpdateTypeConst.ChangeLeader -> {
                long oldMemberID = pbUpdateUnionData.getLongValue1();
                long newMemberID = pbUpdateUnionData.getLongValue2();
                playerUnion.getUnionMember(oldMemberID).setPosition(UnionPositionConst.Member);
                playerUnion.getUnionMember(newMemberID).setPosition(UnionPositionConst.Leader);
                player.warn("UnionUpdateTypeConst.ChangeLeader", oldMemberID, newMemberID);
            }
            case UnionUpdateTypeConst.ModifyUnionName -> {
                String name = pbUpdateUnionData.getStrValue1();
                playerUnion.setUnionName(name);
                player.warn("UnionUpdateTypeConst.ModifyUnionName", name);
                onPlayerLiteUnionDataChange();
            }
            case UnionUpdateTypeConst.ModifyNotice -> {
                String notice = pbUpdateUnionData.getStrValue1();
                playerUnion.getSetting().setNotice(notice);
                player.warn("UnionUpdateTypeConst.ModifyNotice", notice);
            }
            case UnionUpdateTypeConst.ModifyJoinLimitLevel -> {
                int joinLimitLevel = pbUpdateUnionData.getIntValue1();
                playerUnion.getSetting().setJoinLimitLv(joinLimitLevel);
                player.warn("UnionUpdateTypeConst.ModifyJoinLimitLevel", joinLimitLevel);
            }
            case UnionUpdateTypeConst.ModifyJoinNeedApply -> {
                boolean needApply = pbUpdateUnionData.getBoolValue1();
                playerUnion.getSetting().setJoinNeedApply(needApply);
                player.warn("UnionUpdateTypeConst.ModifyJoinNeedApply", needApply);
            }
            case UnionUpdateTypeConst.ModifyMemberPosition -> {
                long memberID = pbUpdateUnionData.getLongValue1();
                int position = pbUpdateUnionData.getIntValue1();
                playerUnion.getUnionMember(memberID).setPosition(position);
                player.warn("UnionUpdateTypeConst.ModifyMemberPosition", memberID, position);
            }
            case UnionUpdateTypeConst.ChangePermission -> {
                int position = pbUpdateUnionData.getIntValue1();
                long permission = pbUpdateUnionData.getLongValue1();
                playerUnion.setPermission(position, permission);
                player.warn("UnionUpdateTypeConst.ChangePermission", position, permission);
            }
            case UnionUpdateTypeConst.UpdateAsset -> {
                int type = pbUpdateUnionData.getIntValue1();
                long value = pbUpdateUnionData.getLongValue1();
                int behaviorSource = (int) pbUpdateUnionData.getLongValue2();
                boolean isAdd = pbUpdateUnionData.getBoolValue1();
                if (isAdd) {
                    playerUnion.addAsset(type, value);
                } else {
                    playerUnion.removeAsset(type, value);
                }
            }
        }
        getData().setChange(realChange);
        PbUnion.S2CUpdateUnion.Builder builder = PbUnion.S2CUpdateUnion.newBuilder();
        builder.setUpdateUnionData(pbUpdateUnionData);
        player.send(PtCode.S2CUpdateUnion, builder.build());
    }

    /**
     * 玩家精简信息联盟数据变化
     */
    public void onPlayerLiteUnionDataChange() {
        if (getData().getUnionID() > 0) {
            Request request = Request.cmd(Command.HMSET).arg(RedisKey.PLAYER_LITE_INFO.value() + player.getPlayerID())
                    .arg(PlayerLiteInfoKeyEnum.PLAYER_UNION_ID.value()).arg(getData().getUnionID())
                    .arg(PlayerLiteInfoKeyEnum.PLAYER_UNION_NAME.value()).arg(playerUnion.getUnionName());
            RedisClientTool.send(player.getChildWorldIndex(), request, (isSuccess, response, errorMsg) -> {
                if (!isSuccess) {
                    player.error("onPlayerLiteUnionDataChange redis add fail", errorMsg);
                }
            });
        } else {
            Request request = Request.cmd(Command.HDEL).arg(RedisKey.PLAYER_LITE_INFO.value() + player.getPlayerID())
                    .arg(PlayerLiteInfoKeyEnum.PLAYER_UNION_ID.value())
                    .arg(PlayerLiteInfoKeyEnum.PLAYER_UNION_NAME.value());
            RedisClientTool.send(player.getChildWorldIndex(), request, (isSuccess, response, errorMsg) -> {
                if (!isSuccess) {
                    player.error("onPlayerLiteUnionDataChange redis del fail", errorMsg);
                }
            });
        }
    }
}
