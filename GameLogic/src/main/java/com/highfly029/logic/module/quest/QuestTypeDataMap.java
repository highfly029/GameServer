package com.highfly029.logic.module.quest;

import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName QuestTypeDataMap
 * @Description 任务类型数据集合
 * @Author liyunpeng
 **/
public class QuestTypeDataMap {
    /**
     * 已完成的任务
     */
    private IntSet completeQuestIDSet = new IntSet();

    /**
     * 已接的任务 questID -> questData
     */
    private IntObjectMap<QuestData> acceptQuestMap = new IntObjectMap<>(QuestData[]::new);

    public IntSet getCompleteQuestIDSet() {
        return completeQuestIDSet;
    }

    public IntObjectMap<QuestData> getAcceptQuestMap() {
        return acceptQuestMap;
    }
}
