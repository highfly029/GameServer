package com.highfly029.logic.module.equipment;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbEquipment;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.logic.module.IClientProtocolRegister;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName EquipProtocol
 * @Description EquipProtocol
 * @Author liyunpeng
 **/
public class EquipProtocol implements IClientProtocolRegister {
    @Override
    public void registerClientProtocol(LogicChildWorld logicChildWorld) throws Exception {
        logicChildWorld.registerClientProtocol(PtCode.C2SPutOnEquipment, this::putOnEquipment);
        logicChildWorld.registerClientProtocol(PtCode.C2SPutOffEquipment, this::putOffEquipment);
    }

    /**
     * 穿装备
     */
    public void putOnEquipment(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbEquipment.C2SPutOnEquipment req = PbEquipment.C2SPutOnEquipment.parseFrom(packet.getData());
        int bagType = req.getBagType();
        int index = req.getIndex();
        int equipmentType = req.getEquipmentType();
        session.getPlayer().getEquipmentModule().putOnEquipment(bagType, index, equipmentType);
    }

    /**
     * 脱装备
     */
    public void putOffEquipment(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbEquipment.C2SPutOffEquipment req = PbEquipment.C2SPutOffEquipment.parseFrom(packet.getData());
        int equipmentType = req.getEquipmentType();
        int slot = req.getSlot();
        int bagType = req.getBagType();
        session.getPlayer().getEquipmentModule().putOffEquipment(equipmentType, slot, bagType);
    }
}
