package com.highfly029.logic.module.auction;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbAuction;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.logic.module.IClientProtocolRegister;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName AuctionProtocol
 * @Description AuctionProtocol
 * @Author liyunpeng
 **/
public class AuctionProtocol implements IClientProtocolRegister {
    @Override
    public void registerClientProtocol(LogicChildWorld logicChildWorld) throws Exception {
        logicChildWorld.registerClientProtocol(PtCode.C2LAuctionSearch, this::auctionSearch);
        logicChildWorld.registerClientProtocol(PtCode.C2LAuctionShelfItem, this::auctionShelfItem);
        logicChildWorld.registerClientProtocol(PtCode.C2LAuctionBuy, this::auctionBuy);
        logicChildWorld.registerClientProtocol(PtCode.C2LAuctionCancelShelfItem, this::auctionCancelShelfItem);
    }

    /**
     * 拍卖行查询
     */
    public void auctionSearch(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbAuction.C2LAuctionSearch req = PbAuction.C2LAuctionSearch.parseFrom(packet.getData());
        int auctionType = req.getAuctionType();
        session.getPlayer().getAuctionModule().auctionSearch(auctionType);
    }

    /**
     * 拍卖行上架物品
     */
    public void auctionShelfItem(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbAuction.C2LAuctionShelfItem req = PbAuction.C2LAuctionShelfItem.parseFrom(packet.getData());
        int index = req.getIndex();
        int itemID = req.getItemID();
        int itemNum = req.getItemNum();
        int price = req.getPrice();
        session.getPlayer().getAuctionModule().auctionShelfItem(index, itemID, itemNum, price);
    }

    /**
     * 拍卖行购买物品
     */
    public void auctionBuy(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbAuction.C2LAuctionBuy req = PbAuction.C2LAuctionBuy.parseFrom(packet.getData());
        session.getPlayer().getAuctionModule().auctionBuy(PbCommonUtils.auctionItemDataPb2Obj(req.getAuctionItemData()));
    }

    /**
     * 拍卖行购买物品
     */
    public void auctionCancelShelfItem(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbAuction.C2LAuctionCancelShelfItem req = PbAuction.C2LAuctionCancelShelfItem.parseFrom(packet.getData());
        session.getPlayer().getAuctionModule().auctionCancelShelfItem(req.getAuctionType(), req.getId());
    }

}
