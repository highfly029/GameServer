package com.highfly029.logic.module.activity.impl;

import com.highfly029.common.protocol.packet.PbActivity;
import com.highfly029.common.template.activity.ActivityTemplate;
import com.highfly029.logic.module.activity.BaseActivity;
import com.highfly029.logic.module.Player;

/**
 * @ClassName Test3
 * @Description Test3
 * @Author liyunpeng
 **/
public class Test3 extends BaseActivity {

    public Test3(Player player, ActivityTemplate activityTemplate) {
        super(player, activityTemplate);
    }

    @Override
    public PbActivity.PbActivityData activityDataToPb() {
        PbActivity.PbActivityData.Builder builder = PbActivity.PbActivityData.newBuilder();
        builder.setId(getId());
        return builder.build();
    }

    @Override
    public void activityDataInitFromPb(PbActivity.PbActivityData pbActivityData) {

    }
}
