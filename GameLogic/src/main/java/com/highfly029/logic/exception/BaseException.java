package com.highfly029.logic.exception;

/**
 * @ClassName BaseException
 * @Description 异常
 * @Author liyunpeng
 **/
public class BaseException extends RuntimeException {

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
