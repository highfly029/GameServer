package com.highfly029.logic.exception;

/**
 * @ClassName StartUpException
 * @Description 启动异常
 * @Author liyunpeng
 **/
public class StartUpException extends BaseException {

    public StartUpException(String message) {
        super(message);
    }

    public StartUpException(String message, Throwable cause) {
        super(message, cause);
    }
}
