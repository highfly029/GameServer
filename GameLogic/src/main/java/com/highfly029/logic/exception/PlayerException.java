package com.highfly029.logic.exception;

/**
 * @ClassName PlayerException
 * @Description 玩家相关异常
 * @Author liyunpeng
 **/
public class PlayerException extends BaseException {

    public PlayerException(String message) {
        super(message);
    }

    public PlayerException(String message, Throwable cause) {
        super(message, cause);
    }
}
