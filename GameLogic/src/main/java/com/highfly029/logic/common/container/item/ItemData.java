package com.highfly029.logic.common.container.item;

import com.highfly029.common.template.item.ItemTemplate;
import com.highfly029.logic.common.container.item.extra.ItemExtraData;
import com.highfly029.logic.common.container.item.extra.ItemExtraDataFactory;

/**
 * @ClassName ItemData
 * @Description 物品数据
 * @Author liyunpeng
 **/
public class ItemData {
    /**
     * 物品产出者id,服务器自己用,可以用来观察非法物品的流向,仅可以流通的物品有此值,在物品流通的日志中打印此数值
     */
    private long createID;
    /**
     * 物品id
     */
    private int itemID;
    /**
     * 物品数量
     */
    private int itemNum;
    /**
     * 是否绑定
     */
    private boolean isBind;
    /**
     * 过期时间戳, 0表示永远不会过期
     */
    private long invalidTime = 0L;
    /**
     * 物品配置
     */
    private ItemTemplate itemTemplate;
    /**
     * 装备物品扩展数据
     */
    private ItemExtraData extraData;

    /**
     * 物品是否有独立的扩展数据
     *
     * @return
     */
    public boolean isHaveExtraData() {
        return extraData != null;
    }

    void init(int itemID, int itemNum) {
        this.itemID = itemID;
        this.itemNum = itemNum;
        extraData = ItemExtraDataFactory.create(itemID);
    }

    public long getCreateID() {
        return createID;
    }

    public void setCreateID(long createID) {
        this.createID = createID;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public int getItemNum() {
        return itemNum;
    }

    public void setItemNum(int itemNum) {
        this.itemNum = itemNum;
    }

    public boolean isBind() {
        return isBind;
    }

    public void setBind(boolean bind) {
        isBind = bind;
    }

    public long getInvalidTime() {
        return invalidTime;
    }

    public void setInvalidTime(long invalidTime) {
        this.invalidTime = invalidTime;
    }

    public ItemTemplate getItemTemplate() {
        return itemTemplate;
    }

    public void setItemTemplate(ItemTemplate itemTemplate) {
        this.itemTemplate = itemTemplate;
    }

    public ItemExtraData getExtraData() {
        return extraData;
    }

    public void setExtraData(ItemExtraData extraData) {
        this.extraData = extraData;
    }


    @Override
    public String toString() {
        return "ItemData{" +
                "createID=" + createID +
                ", itemID=" + itemID +
                ", itemNum=" + itemNum +
                ", isBind=" + isBind +
                ", invalidTime=" + invalidTime +
                '}';
    }
}
