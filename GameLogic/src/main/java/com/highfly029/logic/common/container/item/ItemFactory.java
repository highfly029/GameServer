package com.highfly029.logic.common.container.item;

import com.highfly029.common.template.item.ItemTemplate;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.logic.tool.LogicLoggerTool;

/**
 * @ClassName ItemFactory
 * @Description 物品创建工厂
 * @Author liyunpeng
 **/
public class ItemFactory {
    public static ItemData create(int itemID, int itemNum) {
        ItemData itemData = new ItemData();
        ItemTemplate itemTemplate = ConfigTool.getBagConfig().getItemTemplate(itemID);
        if (itemTemplate == null) {
            LogicLoggerTool.gameLogger.error("invalid itemID=" + itemID, new Exception());
        }
        itemData.setItemTemplate(itemTemplate);
        itemData.init(itemID, itemNum);
        return itemData;
    }
}
