package com.highfly029.logic.common.container.item.extra;

import com.highfly029.common.protocol.packet.PbCommon;

/**
 * @ClassName ItemExtraData
 * @Description 物品扩展数据接口
 * @Author liyunpeng
 **/
public interface ItemExtraData {

    /**
     * 数据初始化
     *
     * @param itemID
     */
    void init(int itemID);

    /**
     * 扩展数据转pb
     *
     * @return
     */
    PbCommon.PbItemExtraData extraDataToPb();

    /**
     * pb初始化扩展数据
     *
     * @param pbItemExtraData
     */
    void extraDataInitFromPb(PbCommon.PbItemExtraData pbItemExtraData);

}
