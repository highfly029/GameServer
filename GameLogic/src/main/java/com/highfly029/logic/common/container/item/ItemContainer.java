package com.highfly029.logic.common.container.item;

import java.util.Arrays;

import com.highfly029.common.template.item.ItemTemplate;
import com.highfly029.common.template.item.ItemTypeConst;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.logic.exception.BaseException;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName ItemContainer
 * @Description 物品通用格子容器
 * @Author liyunpeng
 **/
public abstract class ItemContainer {
    /**
     * 默认初始化大小
     */
    private static final int DEFAULT_INIT_SIZE = 4;
    /**
     * 增加新物品到非法格子
     */
    protected static final int ERROR_LOG_TYPE_INVALID_INDEX = 1;
    /**
     * 增加新物品到一个非空格子
     */
    protected static final int ERROR_LOG_TYPE_PUT_ITEM_TO_NOT_NULL_INDEX = 2;
    /**
     * 增加一个没有模版数据的物品
     */
    protected static final int ERROR_LOG_TYPE_NOT_EXIST_TEMPLATE = 3;
    /**
     * 容器格子数
     */
    private int gridNum;
    /**
     * 物品数组 长度不能超过gridNum
     */
    private ItemData[] itemArray;
    /**
     * 额外的堆叠上限
     */
    private int extendPileMax;

    /**
     * 有过期时间的物品检测组
     */
    private final IntSet invalidTimeCheckSet = new IntSet();
    /**
     * 快捷查找 key:itemID, value:itemNum
     */
    private final IntIntMap itemNumMap = new IntIntMap();
    /**
     * 绑定物品快捷查找 key:itemID, value:itemNum
     */
    private final IntIntMap itemNumMapWithBind = new IntIntMap();
    /**
     * 空格子索引
     */
    private int emptyGridIndex = 0;
    /**
     * 已经使用过的格子数
     */
    private int usedGridNum = 0;
    /**
     * 是否需要排序
     */
    private boolean isNeedSort = true;

    /**
     * 物品数据增加或删除的缓存,防止重复创建
     */
    private final IntIntMap tmpItemNumChangeMap = new IntIntMap();
    private final IntObjectMap<ItemData> tmpNewItemAddMap = new IntObjectMap<>(ItemData[]::new);
    /**
     * 检测多个物品是否可以添加成功的缓存
     */
    private final IntIntMap tmpCheckItemsMap = new IntIntMap();

    public ItemContainer(int gridNum) {
        this.gridNum = gridNum;
        if (gridNum >= DEFAULT_INIT_SIZE) {
            itemArray = new ItemData[DEFAULT_INIT_SIZE];
        } else {
            itemArray = new ItemData[gridNum];
        }
    }

    public ItemContainer(int gridNum, int defaultInitGridNum) {
        if (defaultInitGridNum > gridNum) {
            throw new IllegalArgumentException(String.format("ItemContainer default=%d > gridNum=%d", defaultInitGridNum, gridNum));
        }
        this.gridNum = gridNum;
        int capacity = MathUtils.getNearlyPowerOfTwo(defaultInitGridNum);
        if (capacity > gridNum) {
            capacity = gridNum;
        }
        itemArray = new ItemData[capacity];
    }

    public final int getGridNum() {
        return gridNum;
    }

    public final ItemData[] getItemArray() {
        return itemArray;
    }

    public final void setExtendPileMax(int extendPileMax) {
        this.extendPileMax = extendPileMax;
    }

    /**
     * 物品容器扩容、新的容量至少能放入index位置
     * 保证调用前确保index合法性
     *
     * @param index
     */
    private void reSize(int index) {
        int newCapacity = MathUtils.getNearlyPowerOfTwo(index + 1);
        if (newCapacity > gridNum) {
            newCapacity = gridNum;
        }
        ItemData[] array = new ItemData[newCapacity];
        System.arraycopy(itemArray, 0, array, 0, itemArray.length);
        itemArray = array;
    }

    /**
     * 获取实际的堆叠上限
     *
     * @param itemTemplate
     * @return
     */
    protected int getRealPileMax(ItemTemplate itemTemplate) {
        //如果是武器、强制堆叠数量为1
        if (itemTemplate.getType() == ItemTypeConst.Equipment) {
            return 1;
        } else {
            return itemTemplate.getPileMax() + extendPileMax;
        }
    }


    /**
     * 增加初始默认物品
     *
     * @param itemData
     * @param index
     */
    public void addInitDefaultItem(ItemData itemData, int index) {
        putItemDataToNewArrayIndex(itemData, index);
    }

    /**
     * 重新构造缓存数据
     */
    public void resetCache() {
        emptyGridIndex = findEmptyGridIndex(0);
        int usedNum = 0;
        itemNumMap.clear();
        itemNumMapWithBind.clear();
        if (itemArray != null) {
            ItemData itemData;
            for (int i = 0, len = itemArray.length; i < len; i++) {
                if ((itemData = itemArray[i]) != null) {
                    usedNum++;
                    addItemNumCacheMap(itemData.getItemID(), itemData.getItemNum(), itemData.isBind());
                }
            }
        }
        usedGridNum = usedNum;
        isNeedSort = true;
    }

    /**
     * 在数组的索引位置存放新的物品数据、自动扩容
     *
     * @param itemData
     * @param index
     */
    private void putItemDataToNewArrayIndex(ItemData itemData, int index) {
        if (!isValidIndex(index)) {
            printErrorLog(ERROR_LOG_TYPE_INVALID_INDEX, index, 0, 0);
            return;
        }
        if (index < itemArray.length) {
            if (itemArray[index] != null) {
                printErrorLog(ERROR_LOG_TYPE_PUT_ITEM_TO_NOT_NULL_INDEX, index, 0, 0);
                return;
            }
        } else {
            reSize(index);
        }
        itemArray[index] = itemData;
        usedGridNum++;
    }

    protected void printErrorLog(int type, int index, int itemID, int itemNum) {
        if (type == ERROR_LOG_TYPE_INVALID_INDEX) {
            LogicLoggerTool.systemLogger.error("itemContainer putItemToNewArrayIndex invalid index={}", index);
        } else if (type == ERROR_LOG_TYPE_PUT_ITEM_TO_NOT_NULL_INDEX) {
            LogicLoggerTool.systemLogger.error("itemContainer putItemToNewArrayIndex has not null itemData index=" + index, new BaseException("item"));
        } else if (type == ERROR_LOG_TYPE_NOT_EXIST_TEMPLATE) {
            LogicLoggerTool.systemLogger.error("itemContainer isCanAddItemSuccess not exist itemTemplate itemID={}, itemNum={}", itemID, itemNum);
        }
    }

    /**
     * 判断索引是否合法
     *
     * @param index
     * @return
     */
    private boolean isValidIndex(int index) {
        return index >= 0 && index < gridNum;
    }

    /**
     * 判断格子是否满了
     *
     * @return
     */
    private boolean isGridFull() {
        return usedGridNum >= gridNum;
    }

    /**
     * 判断是否有更多的空余格子
     *
     * @param needGridNum 需要的空格子数
     * @return
     */
    public boolean isHaveMoreFreeGridNum(int needGridNum) {
        return gridNum >= usedGridNum + needGridNum;
    }

    /**
     * 增加物品数量缓存
     *
     * @param itemID
     * @param itemNum
     * @param isBind
     */
    private void addItemNumCacheMap(int itemID, int itemNum, boolean isBind) {
        if (isBind) {
            itemNumMapWithBind.addValue(itemID, itemNum);
        } else {
            itemNumMap.addValue(itemID, itemNum);
        }
    }

    /**
     * 减少物品数量缓存
     *
     * @param itemID
     * @param itemNum
     * @param isBind
     */
    private void removeItemNumCacheMap(int itemID, int itemNum, boolean isBind) {
        if (isBind) {
            itemNumMapWithBind.addValue(itemID, -itemNum);
        } else {
            itemNumMap.addValue(itemID, -itemNum);
        }
    }

    /**
     * 获取物品总数量
     *
     * @param itemID
     * @return
     */
    public int getItemNum(int itemID) {
        return itemNumMap.get(itemID) + itemNumMapWithBind.get(itemID);
    }

    /**
     * 获取物品数量
     *
     * @param itemID
     * @param isBind
     * @return
     */
    private int getItemNum(int itemID, boolean isBind) {
        if (isBind) {
            return itemNumMapWithBind.get(itemID);
        } else {
            return itemNumMap.get(itemID);
        }
    }

    /**
     * 从给定的位置查找空格子索引
     *
     * @param minIndex
     * @return
     */
    private int findEmptyGridIndex(int minIndex) {
        int emptyGridIndex = itemArray.length;
        for (int i = minIndex; i < itemArray.length; i++) {
            if (itemArray[i] == null) {
                emptyGridIndex = i;
                break;
            }
        }
        return emptyGridIndex;
    }

    /**
     * 是否能够成功放入一个物品
     *
     * @param itemID
     * @param itemNum
     * @return
     */
    public boolean isCanAddItemSuccess(int itemID, int itemNum, boolean isBind) {
        ItemTemplate itemTemplate = ConfigTool.getBagConfig().getItemTemplate(itemID);
        return isCanAddItemSuccess(itemID, itemNum, isBind, itemTemplate);
    }

    /**
     * 两个物品逻辑上是否可以叠加、不判断数量
     *
     * @param item
     * @param isBind
     * @return
     */
    private boolean isAllowPile(ItemData item, int itemID, boolean isBind, long invalidTime) {
        return item.getItemID() == itemID && item.isBind() == isBind;
    }

    /**
     * 是否能够成功放入一个物品
     *
     * @param itemID
     * @param itemNum
     * @param isBind
     * @param itemTemplate
     * @return
     */
    private boolean isCanAddItemSuccess(int itemID, int itemNum, boolean isBind, ItemTemplate itemTemplate) {
        if (itemNum <= 0) {
            return false;
        }
        if (itemTemplate == null) {
            printErrorLog(ERROR_LOG_TYPE_NOT_EXIST_TEMPLATE, 0, itemID, itemNum);
            return false;
        }
        //检测物品的背包总上限
        if (itemTemplate.getBagMax() > 0 && getItemNum(itemID) + itemNum > itemTemplate.getBagMax()) {
            return false;
        }
        int pileMax = getRealPileMax(itemTemplate);
        int needGridNum = (int) Math.ceil((double) itemNum / pileMax);
        //优先判断格子数量是否满足添加物品的条件
        if (isHaveMoreFreeGridNum(needGridNum)) {
            return true;
        } else {
            //优化：如果是堆叠为1的物品、格子数不满足直接返回false
            if (pileMax == 1) {
                return false;
            }

            int canAddNum = 0;
            ItemData itemData;
            for (int i = 0; i < itemArray.length; i++) {
                if ((itemData = itemArray[i]) != null) {
                    if (isAllowPile(itemData, itemID, isBind, 0L)) {
                        //可以叠加的数量
                        canAddNum += pileMax - itemData.getItemNum();
                    }
                } else {
                    //空格子可以增加的数量
                    canAddNum += pileMax;
                }
            }
            //数组外空格子可以增加的数量
            canAddNum += (gridNum - itemArray.length) * pileMax;
            return canAddNum >= itemNum;
        }
    }

    public boolean addItem(int itemID, int itemNum, boolean isBind, int behaviorSource) {
        ItemTemplate itemTemplate = ConfigTool.getBagConfig().getItemTemplate(itemID);

        if (!isCanAddItemSuccess(itemID, itemNum, isBind, itemTemplate)) {
            return false;
        }

        tmpItemNumChangeMap.clear();
        tmpNewItemAddMap.clear();
        addItemAbsolutely(itemID, itemNum, isBind, itemTemplate);

        tmpNewItemAddMap.foreachImmutable((index, itemData) -> generateCreateID(itemData));

        onSendMessageAddItems(tmpItemNumChangeMap, tmpNewItemAddMap, behaviorSource);
        onItemAdd(itemID, itemNum, behaviorSource);
        return true;
    }

    /**
     * 直接增加物品实例
     * @param itemData
     */
    private void addItemAbsolutely(ItemData itemData) {
        //物品有独立的扩展数据、需要单独保留
        if (itemData.isHaveExtraData()) {
            int newIndex = emptyGridIndex;
            putItemDataToNewArrayIndex(itemData, newIndex);
            emptyGridIndex = findEmptyGridIndex(newIndex);
            isNeedSort = true;
            addItemNumCacheMap(itemData.getItemID(), itemData.getItemNum(), itemData.isBind());
            tmpNewItemAddMap.put(newIndex, itemData);
        } else {
            ItemTemplate itemTemplate = ConfigTool.getBagConfig().getItemTemplate(itemData.getItemID());
            addItemAbsolutely(itemData.getItemID(), itemData.getItemNum(), itemData.isBind(), itemTemplate);
        }
    }

    /**
     * 增加物品实例
     *
     * @param itemData
     * @param behaviorSource
     * @return
     */
    public boolean addItem(ItemData itemData, int behaviorSource) {
        if (itemData == null || itemData.getItemNum() <= 0) {
            return false;
        }
        if (!isCanAddItemSuccess(itemData.getItemID(), itemData.getItemNum(), itemData.isBind())) {
            return false;
        }
        tmpItemNumChangeMap.clear();
        tmpNewItemAddMap.clear();
        addItemAbsolutely(itemData);
        tmpNewItemAddMap.foreachImmutable((index, data) -> generateCreateID(data));
        onSendMessageAddItems(tmpItemNumChangeMap, tmpNewItemAddMap, behaviorSource);
        onItemAdd(itemData.getItemID(), itemData.getItemNum(), behaviorSource);
        return true;
    }

    /**
     * 指定索引增加物品实例
     *
     * @param itemData
     * @param index
     * @param behaviorSource
     * @return
     */
    public boolean addItemByIndex(ItemData itemData, int index, int behaviorSource) {
        if (itemData == null || itemData.getItemNum() <= 0) {
            return false;
        }
        //位置不合法
        if (!isValidIndex(index)) {
            return false;
        }
        //位置已经有物品了
        if (index < itemArray.length) {
            if (itemArray[index] != null) {
                return false;
            }
        }

        tmpItemNumChangeMap.clear();
        tmpNewItemAddMap.clear();
        putItemDataToNewArrayIndex(itemData, index);
        emptyGridIndex = findEmptyGridIndex(index);
        isNeedSort = true;
        addItemNumCacheMap(itemData.getItemID(), itemData.getItemNum(), itemData.isBind());
        tmpNewItemAddMap.put(index, itemData);
        generateCreateID(itemData);
        onSendMessageAddItems(tmpItemNumChangeMap, tmpNewItemAddMap, behaviorSource);
        onItemAdd(itemData.getItemID(), itemData.getItemNum(), behaviorSource);
        return true;
    }

    /**
     * 增加物品 必定增加成功
     *
     * @param itemID
     * @param itemNum
     * @param isBind
     * @param itemTemplate
     * @return
     */
    private void addItemAbsolutely(int itemID, int itemNum, boolean isBind, ItemTemplate itemTemplate) {
        int tmpItemNum = itemNum;
        long invalidTime = itemTemplate.getValidTime() > 0 ? SystemTimeTool.getMillTime() + itemTemplate.getValidTime() : 0;
        int pileMax = getRealPileMax(itemTemplate);

        //是否叠加上限为1
        boolean isSingleOne = pileMax == 1;
        if (isSingleOne) {
            //直接从emptyGridIndex 开始遍历
            int newEmptyGridIndex = -1;
            for (int i = emptyGridIndex; i < itemArray.length; i++) {
                if (itemArray[i] == null) {
                    if (tmpItemNum == 0) {
                        newEmptyGridIndex = i;
                        break;
                    }
                    ItemData itemData = ItemFactory.create(itemID, 1);
                    itemData.setBind(isBind);
                    itemData.setInvalidTime(invalidTime);
                    putItemDataToNewArrayIndex(itemData, i);
                    tmpNewItemAddMap.put(i, itemData);
                    tmpItemNum--;
                }
            }

            if (tmpItemNum == 0) {
                //重新设置下一个空格子索引
                if (newEmptyGridIndex > -1) {
                    //找到空格子并设置
                    emptyGridIndex = newEmptyGridIndex;
                } else {
                    //没找到空格子，设置为数组长度
                    emptyGridIndex = itemArray.length;
                }
            } else {
                //扩展数据继续增加
                for (int i = itemArray.length; i < gridNum; i++) {
                    ItemData itemData = ItemFactory.create(itemID, 1);
                    itemData.setBind(isBind);
                    itemData.setInvalidTime(invalidTime);
                    putItemDataToNewArrayIndex(itemData, i);
                    tmpNewItemAddMap.put(i, itemData);
                    if (--tmpItemNum == 0) {
                        emptyGridIndex = i + 1;
                        break;
                    }
                }
            }
        } else {
            //从0开始遍历
            ItemData itemData;
            int newEmptyGridIndex = -1;
            for (int i = 0; i < itemArray.length; i++) {
                if ((itemData = itemArray[i]) != null) {
                    if (tmpItemNum == 0) {
                        continue;
                    }
                    //相同物品叠加
                    if (isAllowPile(itemData, itemID, isBind, 0L)) {
                        //堆叠已满、跳过
                        if (pileMax == itemData.getItemNum()) {
                            continue;
                        }
                        int dNum = pileMax - itemData.getItemNum();
                        if (dNum >= tmpItemNum) {
                            itemData.setItemNum(itemData.getItemNum() + tmpItemNum);
                            tmpItemNumChangeMap.put(i, tmpItemNum);
                            tmpItemNum = 0;
                        } else {
                            tmpItemNum -= dNum;
                            itemData.setItemNum(pileMax);
                            tmpItemNumChangeMap.put(i, dNum);
                        }
                    }
                } else {
                    if (tmpItemNum == 0) {
                        newEmptyGridIndex = i;
                        break;
                    }
                    int dNum = tmpItemNum > pileMax ? pileMax : tmpItemNum;
                    tmpItemNum -= dNum;
                    ItemData newItemData = ItemFactory.create(itemID, dNum);
                    newItemData.setBind(isBind);
                    tmpNewItemAddMap.put(i, newItemData);
                    putItemDataToNewArrayIndex(newItemData, i);

                }
            }
            if (tmpItemNum == 0) {
                //重新设置下一个空格子索引
                if (newEmptyGridIndex > -1) {
                    //找到空格子并设置
                    emptyGridIndex = newEmptyGridIndex;
                } else {
                    //没找到空格子，设置为数组长度
                    emptyGridIndex = itemArray.length;
                }
            } else {
                //扩展数据继续增加
                for (int i = itemArray.length; i < gridNum; i++) {
                    int dNum = tmpItemNum > pileMax ? pileMax : tmpItemNum;
                    tmpItemNum -= dNum;
                    ItemData newItemData = ItemFactory.create(itemID, dNum);
                    newItemData.setBind(isBind);
                    putItemDataToNewArrayIndex(newItemData, i);
                    tmpNewItemAddMap.put(i, newItemData);
                    if (tmpItemNum == 0) {
                        emptyGridIndex = i + 1;
                        break;
                    }
                }
            }
        }
        addItemNumCacheMap(itemID, itemNum, isBind);
        isNeedSort = true;
    }

    /**
     * 是否能够成功放入多个物品
     *
     * @param addItemArray
     * @return
     */
    public boolean isCanAddItemsSuccess(int[][] addItemArray, boolean isBind) {
        if (addItemArray == null || addItemArray.length == 0) {
            return false;
        }
        //检测物品的背包总上限
        boolean isBagMaxFlag = false;
        //优先判断格子数量是否满足添加物品的条件
        int needGridNum = 0;

        tmpCheckItemsMap.clear();
        for (int i = 0; i < addItemArray.length; i++) {
            int itemID = addItemArray[i][0];
            int itemNum = addItemArray[i][1];
            //不允许增加数量 <= 0的物品
            if (itemNum <= 0) {
                return false;
            }
            ItemTemplate itemTemplate = ConfigTool.getBagConfig().getItemTemplate(itemID);
            if (itemTemplate == null) {
                return false;
            }
            //不允许有重复的itemID
            if (tmpCheckItemsMap.get(itemID) > 0) {
                return false;
            }
            tmpCheckItemsMap.put(itemID, itemNum);

            if (itemTemplate.getBagMax() > 0 && getItemNum(itemID) + itemNum > itemTemplate.getBagMax()) {
                isBagMaxFlag = true;
                break;
            }
            needGridNum += (int) Math.ceil((double) itemNum / getRealPileMax(itemTemplate));
        }
        if (tmpCheckItemsMap.size() == 0) {
            return false;
        }
        if (isBagMaxFlag == true) {
            return false;
        }

        if (isHaveMoreFreeGridNum(needGridNum)) {
            //空格子足够放入
            return true;
        } else {
            //仅仅空格子不够放入、需要检测叠加
            //先减去可以叠加的物品数量
            ItemData itemData;
            for (int i = 0; i < itemArray.length; i++) {
                if ((itemData = itemArray[i]) != null) {
                    int addNum;
                    if ((addNum = tmpCheckItemsMap.get(itemData.getItemID())) > 0 && isAllowPile(itemData, itemData.getItemID(), isBind, 0L)) {
                        int canAddNum = getRealPileMax(itemData.getItemTemplate()) - itemData.getItemNum();
                        if (canAddNum >= addNum) {
                            tmpCheckItemsMap.remove(itemData.getItemID());
                        } else {
                            tmpCheckItemsMap.addValue(itemData.getItemID(), -canAddNum);
                        }
                    }
                }
            }
            //然后剩余的放入新的格子、看是不是够
            needGridNum = 0;
            int freeValue = tmpCheckItemsMap.getFreeValue();
            long[] table = tmpCheckItemsMap.getTable();
            long entry;
            int key;
            int value;
            for (int i = 0, len = table.length; i < len; i++) {
                entry = table[i];
                if ((key = (int) entry) != freeValue) {
                    value = (int) (entry >>> 32);
                    if (value > 0) {
                        ItemTemplate itemTemplate = ConfigTool.getBagConfig().getItemTemplate(key);
                        needGridNum += (int) Math.ceil((double) value / getRealPileMax(itemTemplate));
                    }
                }
            }
            return isHaveMoreFreeGridNum(needGridNum);
        }
    }

    /**
     * 增加多个物品
     *
     * @param addItemArray
     * @param isBind
     * @param behaviorSource
     * @return
     */
    public boolean addItems(int[][] addItemArray, boolean isBind, int behaviorSource) {
        if (!isCanAddItemsSuccess(addItemArray, isBind)) {
            return false;
        }
        tmpItemNumChangeMap.clear();
        tmpNewItemAddMap.clear();
        for (int i = 0; i < addItemArray.length; i++) {
            int itemID = addItemArray[i][0];
            int itemNum = addItemArray[i][1];
            ItemTemplate itemTemplate = ConfigTool.getBagConfig().getItemTemplate(itemID);
            addItemAbsolutely(itemID, itemNum, isBind, itemTemplate);
        }

        tmpNewItemAddMap.foreachImmutable((index, itemData) -> {
            generateCreateID(itemData);
        });

        onSendMessageAddItems(tmpItemNumChangeMap, tmpNewItemAddMap, behaviorSource);

        for (int i = 0; i < addItemArray.length; i++) {
            int itemID = addItemArray[i][0];
            int itemNum = addItemArray[i][1];
            onItemAdd(itemID, itemNum, behaviorSource);
        }
        return true;
    }

    /**
     * 增加多个物品
     *
     * @param items
     * @param behaviorSource
     * @return
     */
    public boolean addItems(ObjectList<ItemData> items, int behaviorSource) {
        if (!isCanAddItemsSuccess(items)) {
            return false;
        }
        tmpItemNumChangeMap.clear();
        tmpNewItemAddMap.clear();
        for (int i = 0, size = items.size(); i < size; i++) {
            ItemData itemData = items.get(i);
            addItemAbsolutely(itemData);
        }

        tmpNewItemAddMap.foreachImmutable((index, itemData) -> {
            generateCreateID(itemData);
        });

        onSendMessageAddItems(tmpItemNumChangeMap, tmpNewItemAddMap, behaviorSource);

        for (int i = 0, size = items.size(); i < size; i++) {
            ItemData itemData = items.get(i);
            onItemAdd(itemData.getItemID(), itemData.getItemNum(), behaviorSource);
        }
        return true;
    }

    /**
     * 是否能够成功放入多个物品
     *
     * @param items
     * @return
     */
    public boolean isCanAddItemsSuccess(ObjectList<ItemData> items) {
        if (items == null) {
            return false;
        }
        int size = items.size();
        if (size == 0) {
            return false;
        }
        //检测物品的背包总上限
        boolean isBagMaxFlag = false;
        //优先判断格子数量是否满足添加物品的条件
        int needGridNum = 0;

        tmpCheckItemsMap.clear();
        for (int i = 0; i < size; i++) {
            ItemData itemData = items.get(i);
            int itemID = itemData.getItemID();
            int itemNum = itemData.getItemNum();
            //不允许增加数量 <= 0的物品
            if (itemNum <= 0) {
                return false;
            }
            ItemTemplate itemTemplate = ConfigTool.getBagConfig().getItemTemplate(itemID);
            if (itemTemplate == null) {
                return false;
            }
            //把itemID和isBind一起作为唯一标记
            int itemUniqueID = itemID << 1 | (itemData.isBind() ? 1 : 0);
            //不允许有重复的itemID
            if (tmpCheckItemsMap.get(itemUniqueID) > 0) {
                return false;
            }

            tmpCheckItemsMap.put(itemUniqueID, itemNum);

            if (itemTemplate.getBagMax() > 0 && getItemNum(itemID) + itemNum > itemTemplate.getBagMax()) {
                isBagMaxFlag = true;
                break;
            }
            needGridNum += (int) Math.ceil((double) itemNum / getRealPileMax(itemTemplate));
        }
        if (tmpCheckItemsMap.size() == 0) {
            return false;
        }
        if (isBagMaxFlag == true) {
            return false;
        }

        if (isHaveMoreFreeGridNum(needGridNum)) {
            //空格子足够放入
            return true;
        } else {
            //仅仅空格子不够放入、需要检测叠加
            //先减去可以叠加的物品数量
            ItemData itemData;
            for (int i = 0; i < itemArray.length; i++) {
                if ((itemData = itemArray[i]) != null) {
                    int addNum;
                    int isBind = itemData.isBind() ? 1 : 0;
                    if ((addNum = tmpCheckItemsMap.get(itemData.getItemID() << 1 | isBind)) > 0) {
                        int canAddNum = getRealPileMax(itemData.getItemTemplate()) - itemData.getItemNum();
                        if (canAddNum >= addNum) {
                            tmpCheckItemsMap.put(itemData.getItemID(), 0);
                        } else {
                            tmpCheckItemsMap.addValue(itemData.getItemID(), -canAddNum);
                        }
                    }
                }
            }
            //然后剩余的放入新的格子、看是不是够
            needGridNum = 0;
            int freeValue = tmpCheckItemsMap.getFreeValue();
            long[] table = tmpCheckItemsMap.getTable();
            long entry;
            int key;
            int value;
            for (int i = 0, len = table.length; i < len; i++) {
                entry = table[i];
                if ((key = (int) entry) != freeValue) {
                    value = (int) (entry >>> 32);
                    if (value > 0) {
                        ItemTemplate itemTemplate = ConfigTool.getBagConfig().getItemTemplate(key);
                        needGridNum += (int) Math.ceil((double) value / getRealPileMax(itemTemplate));
                    }
                }
            }
            return isHaveMoreFreeGridNum(needGridNum);
        }
    }

    /**
     * 是否能够成功删除一个物品
     *
     * @param itemID
     * @param itemNum
     * @return
     */
    public boolean isCanRemoveItemSuccess(int itemID, int itemNum, boolean isBind) {
        if (itemNum <= 0) {
            return false;
        }
        int totalNum = getItemNum(itemID, isBind);
        return totalNum >= itemNum;
    }

    /**
     * 删除一个物品
     *
     * @param itemID
     * @param itemNum
     * @return
     */
    public boolean removeItem(int itemID, int itemNum, boolean isBind, int behaviorSource) {

        if (!isCanRemoveItemSuccess(itemID, itemNum, isBind)) {
            return false;
        }

        tmpItemNumChangeMap.clear();

        removeItemAbsolutely(itemID, itemNum, isBind);

        onSendMessageRemoveItems(tmpItemNumChangeMap, behaviorSource);

        onItemRemove(itemID, itemNum, behaviorSource);
        return true;
    }

    /**
     * 删除一个物品，必定删除成功
     *
     * @param itemID
     * @param itemNum
     * @param isBind  按照是否绑定来删除
     */
    private void removeItemAbsolutely(int itemID, int itemNum, boolean isBind) {
        //倒序删除
        ItemData itemData;
        for (int i = itemArray.length - 1; i >= 0; i--) {
            if ((itemData = itemArray[i]) != null) {
                if (isAllowPile(itemData, itemID, isBind, 0L)) {
                    int dNum = itemNum > itemData.getItemNum() ? itemData.getItemNum() : itemNum;
                    itemNum -= dNum;
                    itemData.setItemNum(itemData.getItemNum() - dNum);
                    tmpItemNumChangeMap.put(i, dNum);
                    removeItemNumCacheMap(itemID, dNum, itemData.isBind());
                    if (itemData.getItemNum() == 0) {
                        itemArray[i] = null;
                        usedGridNum--;
                        //只有删除的物品在上一个空格子之前，才修改空格子索引
                        if (emptyGridIndex > i) {
                            emptyGridIndex = i;
                        }
                    }
                    if (itemNum == 0) {
                        break;
                    }
                }
            }
        }
        isNeedSort = true;
    }

    /**
     * 是否能够删除多个物品
     *
     * @param removeItemArray
     * @return
     */
    public boolean isCanRemoveItemsSuccess(int[][] removeItemArray, boolean isBind) {
        if (removeItemArray == null) {
            return false;
        }
        tmpCheckItemsMap.clear();
        for (int i = 0; i < removeItemArray.length; i++) {
            int itemID = removeItemArray[i][0];
            int itemNum = removeItemArray[i][1];
            if (itemNum <= 0) {
                return false;
            }
            if (getItemNum(itemID, isBind) < itemNum) {
                return false;
            }
            tmpCheckItemsMap.put(itemID, itemNum);
        }
        //如果有重复的itemID 不允许删除
        return tmpCheckItemsMap.size() == removeItemArray.length;
    }

    /**
     * 删除多个物品
     *
     * @param removeItemArray
     * @param behaviorSource
     * @param isBind
     * @return
     */
    public boolean removeItems(int[][] removeItemArray, boolean isBind, int behaviorSource) {
        if (!isCanRemoveItemsSuccess(removeItemArray, isBind)) {
            return false;
        }
        tmpItemNumChangeMap.clear();

        for (int i = 0; i < removeItemArray.length; i++) {
            int itemID = removeItemArray[i][0];
            int itemNum = removeItemArray[i][1];
            removeItemAbsolutely(itemID, itemNum, isBind);
        }

        onSendMessageRemoveItems(tmpItemNumChangeMap, behaviorSource);

        for (int i = 0; i < removeItemArray.length; i++) {
            int itemID = removeItemArray[i][0];
            int itemNum = removeItemArray[i][1];
            onItemRemove(itemID, itemNum, behaviorSource);
        }
        return true;
    }

    /**
     * 通过索引获取物品
     *
     * @param index
     * @return
     */
    public ItemData getItemByIndex(int index) {
        if (index < itemArray.length) {
            return itemArray[index];
        } else {
            return null;
        }
    }

    /**
     * 直接删除索引所在的物品、不修改itemData的引用内容
     *
     * @param index
     * @param behaviorSource
     * @return
     */
    public boolean removeItemByIndexDirectly(int index, int behaviorSource) {
        if (itemArray[index] == null) {
            return false;
        }
        ItemData itemData = itemArray[index];
        itemArray[index] = null;
        usedGridNum--;
        //只有删除的物品在上一个空格子之前，才修改空格子索引
        if (emptyGridIndex > index) {
            emptyGridIndex = index;
        }
        removeItemNumCacheMap(itemData.getItemID(), itemData.getItemNum(), itemData.isBind());
        isNeedSort = true;

        onSendMessageRemoveItemByIndex(index, itemData.getItemNum(), behaviorSource);
        onItemRemove(itemData.getItemID(), itemData.getItemNum(), behaviorSource);
        return true;
    }

    /**
     * 通过索引删除物品、一定会删除
     *
     * @param index
     * @param num
     * @param behaviorSource
     */
    private void removeItemByIndexAbsolutely(int index, int num, int behaviorSource) {
        ItemData itemData = itemArray[index];
        int restNum = itemData.getItemNum() - num;
        if (restNum == 0) {
            itemArray[index] = null;
            usedGridNum--;
            //只有删除的物品在上一个空格子之前，才修改空格子索引
            if (emptyGridIndex > index) {
                emptyGridIndex = index;
            }
        } else {
            itemData.setItemNum(restNum);
        }
        removeItemNumCacheMap(itemData.getItemID(), num, itemData.isBind());
        isNeedSort = true;

        onSendMessageRemoveItemByIndex(index, num, behaviorSource);
        onItemRemove(itemData.getItemID(), num, behaviorSource);
    }

    /**
     * 通过索引使用物品
     *
     * @param index
     * @param num
     * @param behaviorSource
     * @return
     */
    public boolean useItemByIndex(int index, int num, int behaviorSource) {
        ItemData itemData;
        if ((itemData = getItemByIndex(index)) == null) {
            return false;
        }
        if (itemData.getItemNum() < num) {
            return false;
        }
        if (!itemData.getItemTemplate().isCanUse()) {
            return false;
        }
        if (!isCanUseItem(itemData)) {
            return false;
        }

        removeItemByIndexAbsolutely(index, num, behaviorSource);

        onItemUse(itemData, num, behaviorSource);
        return true;
    }

    /**
     * 是否能使用物品
     *
     * @param itemData
     * @return
     */
    protected boolean isCanUseItem(ItemData itemData) {
        return true;
    }

    /**
     * 通过索引删除物品
     *
     * @param index
     * @param num
     * @param behaviorSource
     * @return
     */
    public boolean removeItemByIndex(int index, int num, int behaviorSource, boolean dropToScene) {
        ItemData itemData;
        if ((itemData = getItemByIndex(index)) == null) {
            return false;
        }
        if (itemData.getItemNum() < num) {
            return false;
        }

        removeItemByIndexAbsolutely(index, num, behaviorSource);

        if (dropToScene) {
            onDropItemToScene(itemData, num);
        }
        return true;
    }

    /**
     * 创建掉落物
     *
     * @param itemData
     * @param dropNum  掉落数量
     */
    protected void onDropItemToScene(ItemData itemData, int dropNum) {

    }

    /**
     * 通过索引删除物品
     *
     * @param index
     * @param num
     * @param behaviorSource
     * @return
     */
    public boolean removeItemByIndex(int index, int num, int behaviorSource) {
        return removeItemByIndex(index, num, behaviorSource, false);
    }

    /**
     * 通过索引交换物品,如果toIndex没有物品则视为移动物品
     *
     * @param fromIndex
     * @param toIndex
     * @return
     */
    public boolean swapItemByIndex(int fromIndex, int toIndex) {
        if (!isValidIndex(fromIndex) || !isValidIndex(toIndex)) {
            return false;
        }
        ItemData fromItemData;
        if ((fromItemData = getItemByIndex(fromIndex)) == null) {
            return false;
        }
        ItemData toItemData = getItemByIndex(toIndex);
        if (toIndex >= itemArray.length) {
            reSize(toIndex);
        }
        itemArray[toIndex] = fromItemData;

        itemArray[fromIndex] = toItemData;

        //空格子发生变化
        if (toItemData == null) {
            //空格子在两个变化的索引位置之前、不需要修改
            int minIndex = Math.min(fromIndex, toIndex);
            if (emptyGridIndex >= minIndex) {
                emptyGridIndex = findEmptyGridIndex(minIndex);
            }
        }
        isNeedSort = true;
        onSendMessageSwapItemByIndex(fromIndex, toIndex);
        return true;
    }

    /**
     * 拆分物品
     *
     * @param index
     * @param num
     */
    public void splitItem(int index, int num) {
        ItemData itemData = getItemByIndex(index);
        if (itemData == null) {
            return;
        }
        if (num <= 0 || num >= itemData.getItemNum()) {
            return;
        }
        if (isGridFull()) {
            return;
        }

        if (itemData.isHaveExtraData()) {
            return;
        }

        ItemData newItemData = ItemFactory.create(itemData.getItemID(), num);
        itemData.setItemNum(itemData.getItemNum() - num);
        int newIndex = emptyGridIndex;
        putItemDataToNewArrayIndex(newItemData, newIndex);
        emptyGridIndex = findEmptyGridIndex(newIndex);
        isNeedSort = true;
        onSendMessageSplitItem(index, num, newIndex, newItemData);
    }

    /**
     * 重新设置格子数量
     *
     * @param newGridNum
     */
    public void resetGridNum(int newGridNum) {
        if (newGridNum <= 0) {
            return;
        }
        if (newGridNum == gridNum) {
            return;
        }
        //是否增加格子
        boolean isAdd = newGridNum > gridNum;

        if (!isAdd) {
            //TODO 减少格子需要检测 不符合则return
            return;
        }

        gridNum = newGridNum;
        onSendMessageResetGridNum(gridNum);
    }

    /**
     * 整理
     */
    public void sort() {
        sort(0, itemArray.length);
    }

    public void sort(int startPos) {
        sort(startPos, itemArray.length);
    }

    /**
     * 整理物品容器
     */
    public void sort(int startPos, int endPos) {
        if (!isNeedSort) {
            return;
        }
        isNeedSort = false;
        int itemArrayLength = itemArray.length;
        if (itemArrayLength == 0) {
            return;
        }

        Arrays.sort(itemArray, startPos, endPos, this::compareItems);

        //剔除itemNum==0和itemData==null的物品
        int beginIndex;
        int endIndex;
        ItemData itemData;
        for (int i = startPos, len = endPos; i < len; i++) {
            if ((itemData = itemArray[i]) != null && itemData.getItemNum() == 0) {
                itemData = itemArray[i] = null;
            }
            if (itemData == null) {
                beginIndex = i;
                endIndex = i + 1;
                boolean isEnd = true;
                for (int j = endIndex; j < len; j++) {
                    if ((itemData = itemArray[j]) != null && itemData.getItemNum() != 0) {
                        endIndex = j;
                        isEnd = false;
                        break;
                    }
                }
                if (isEnd) {
                    break;
                }
                itemArray[beginIndex] = itemArray[endIndex];
                itemArray[endIndex] = null;
            }
        }
        emptyGridIndex = findEmptyGridIndex(0);
        int countUsedGridNum = emptyGridIndex;
        for (int i = countUsedGridNum; i < itemArrayLength; i++) {
            if (itemArray[i] != null) {
                countUsedGridNum++;
            }
        }
        usedGridNum = countUsedGridNum;
        onSendMessageSort(itemArray);
    }

    /**
     * 排序算法
     * 比较优先级
     * 排序索引(从小到大) > 物品id(从小到大) > bind(优先不绑定)
     *
     * @param itemData1
     * @param itemData2
     * @return
     */
    private int compareItems(ItemData itemData1, ItemData itemData2) {
        if (itemData2 == null) {
            return -1;
        }
        if (itemData1 == null) {
            return 1;
        }
        int tmp1, tmp2;
        if ((tmp1 = itemData1.getItemTemplate().getSortIndex()) < (tmp2 = itemData2.getItemTemplate().getSortIndex())) {
            return -1;
        }
        if (tmp1 > tmp2) {
            return 1;
        }
        if ((tmp1 = itemData1.getItemID()) < (tmp2 = itemData2.getItemID())) {
            return -1;
        }
        if (tmp1 > tmp2) {
            return 1;
        }
        if (itemData1.isBind() != itemData2.isBind()) {
            return !itemData1.isBind() ? -1 : 1;
        }
        if (itemData2.getItemNum() == 0) {
            return -1;
        }
        if (itemData1.getItemNum() == 0) {
            return 1;
        }
        int pileMax = getRealPileMax(itemData1.getItemTemplate());

        if (itemData1.getItemNum() == pileMax) {
            return -1;
        }
        if (itemData2.getItemNum() == pileMax) {
            return 1;
        }
        int dNum = Math.min(pileMax - itemData1.getItemNum(), itemData2.getItemNum());
        itemData1.setItemNum(itemData1.getItemNum() + dNum);
        itemData2.setItemNum(itemData2.getItemNum() - dNum);
        return -1;
    }

    /**
     * 生成唯一表示createID的方法
     */
    protected void generateCreateID(ItemData itemData) {

    }

    /**
     * 发送增加物品消息
     *
     * @param addItems       key:索引 value:增加的数量
     * @param newItems       key:索引 value:新的物品
     * @param behaviorSource 行为
     */
    protected void onSendMessageAddItems(IntIntMap addItems, IntObjectMap<ItemData> newItems, int behaviorSource) {
    }

    /**
     * 发送删除物品消息
     *
     * @param removeItems    key:索引 value:删除的数量
     * @param behaviorSource 行为
     */
    protected void onSendMessageRemoveItems(IntIntMap removeItems, int behaviorSource) {
    }

    /**
     * 发送整理消息
     *
     * @param itemArray
     */
    protected void onSendMessageSort(ItemData[] itemArray) {
    }

    /**
     * 发送按照索引删除物品消息
     *
     * @param index          索引位置
     * @param num            删除的数量
     * @param behaviorSource 行为
     */
    protected void onSendMessageRemoveItemByIndex(int index, int num, int behaviorSource) {
    }

    /**
     * 发送按索引交换物品消息
     *
     * @param fromIndex
     * @param toIndex
     */
    protected void onSendMessageSwapItemByIndex(int fromIndex, int toIndex) {
    }

    /**
     * 发送拆分物品信息
     *
     * @param index       源索引
     * @param num         拆分减少的数量
     * @param newIndex    拆分后新增的物品索引
     * @param newItemData 拆分后的新堆叠物品
     */
    protected void onSendMessageSplitItem(int index, int num, int newIndex, ItemData newItemData) {

    }

    /**
     * 发送重置格子数量消息
     *
     * @param gridNum
     */
    protected void onSendMessageResetGridNum(int gridNum) {

    }

    /**
     * 物品删除通知
     *
     * @param itemID
     * @param removeItemNum
     * @param behaviorSource
     */
    protected void onItemRemove(int itemID, int removeItemNum, int behaviorSource) {

    }

    /**
     * 物品增加通知
     *
     * @param itemID
     * @param addItemNum
     * @param behaviorSource
     */
    protected void onItemAdd(int itemID, int addItemNum, int behaviorSource) {

    }

    /**
     * 物品使用通知
     *
     * @param itemData
     * @param useNum
     * @param behaviorSource
     */
    protected void onItemUse(ItemData itemData, int useNum, int behaviorSource) {

    }


    public void print() {
    }


    /**
     * 合并重复的itemID
     */
    public static void merge(ObjectList<ItemData> list) {
        if (list == null) {
            return;
        }
        int size = list.size();
        //只有超过两个才可能重复
        if (size <= 1) {
            return;
        }
        for (int i = size - 1; i > 0; i--) {
            ItemData itemData = list.get(i);
            for (int j = i - 1; j >= 0; j--) {
                ItemData preItemData = list.get(j);
                if (itemData.getItemID() == preItemData.getItemID() && itemData.isBind() == preItemData.isBind()) {
                    preItemData.setItemNum(preItemData.getItemNum() + itemData.getItemNum());
                    list.remove(i);
                    break;
                }
            }
        }
    }

    /**
     * gm命令清空物品容器
     */
    public void clearByGM() {
        resetCache();
        itemArray = new ItemData[gridNum];
        onSendMessageSort(itemArray);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nitemContainer begin.");
        sb.append(" gridNum=").append(gridNum);
        sb.append(" emptyGridIndex=").append(emptyGridIndex);
        sb.append(" usedGridNum=").append(usedGridNum);
        sb.append(" itemNumMap=").append(itemNumMap);
        ItemData itemData;
        for (int i = 0; i < itemArray.length; i++) {
            if ((itemData = itemArray[i]) != null) {
                sb.append("\nindex=").append(i).append(" itemData=").append(itemData);
            }
        }
        sb.append("\nitemContainer end.");
        return sb.toString();
    }
}
