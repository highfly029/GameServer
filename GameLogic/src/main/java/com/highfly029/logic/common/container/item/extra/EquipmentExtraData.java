package com.highfly029.logic.common.container.item.extra;

import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.template.equipment.EquipmentTemplate;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.utils.collection.IntIntMap;

/**
 * @ClassName EquipmentExtraData
 * @Description 装备物品扩展数据
 * @Author liyunpeng
 **/
public class EquipmentExtraData implements ItemExtraData {
    private EquipmentTemplate equipmentTemplate;
    //装备属性(一般是随机出来的)
    private IntIntMap attributes;

    public EquipmentTemplate getEquipmentTemplate() {
        return equipmentTemplate;
    }

    public void setEquipmentTemplate(EquipmentTemplate equipmentTemplate) {
        this.equipmentTemplate = equipmentTemplate;
    }

    public IntIntMap getAttributes() {
        return attributes;
    }

    public void setAttributes(IntIntMap attributes) {
        this.attributes = attributes;
    }

    @Override
    public void init(int itemID) {
        this.equipmentTemplate = ConfigTool.getEquipConfig().getEquipmentTemplate(itemID);

        //TODO 临时增加随机属性
        attributes = new IntIntMap();
        if (itemID < 5) {
            //增加100攻击
            attributes.addValue(65, 100);
        }
    }

    @Override
    public PbCommon.PbItemExtraData extraDataToPb() {
        PbCommon.PbItemExtraData.Builder builder = PbCommon.PbItemExtraData.newBuilder();
        if (attributes != null) {
            int freeValue = attributes.getFreeValue();
            long[] table = attributes.getTable();
            long entry;
            int key;
            int value;
            for (int i = 0, len = table.length; i < len; i++) {
                entry = table[i];
                if ((key = (int) entry) != freeValue) {
                    value = (int) (entry >>> 32);
                    PbCommon.PbAttributeData.Builder attributeBuilder = PbCommon.PbAttributeData.newBuilder();
                    attributeBuilder.setAttributeID(key).setAttributeValue(value);
                    builder.addAttributes(attributeBuilder.build());
                }
            }
        }
        return builder.build();
    }

    @Override
    public void extraDataInitFromPb(PbCommon.PbItemExtraData pbItemExtraData) {
        int len = pbItemExtraData.getAttributesCount();
        if (len > 0) {
            if (attributes == null) {
                attributes = new IntIntMap();
            }
            for (int i = 0; i < len; i++) {
                PbCommon.PbAttributeData pbAttributeData = pbItemExtraData.getAttributes(i);
                attributes.put(pbAttributeData.getAttributeID(), pbAttributeData.getAttributeValue());
            }
        }
    }
}
