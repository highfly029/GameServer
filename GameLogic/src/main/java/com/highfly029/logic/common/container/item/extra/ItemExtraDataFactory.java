package com.highfly029.logic.common.container.item.extra;

import com.highfly029.common.template.item.ItemTemplate;
import com.highfly029.common.template.item.ItemTypeConst;
import com.highfly029.logic.tool.ConfigTool;

/**
 * @ClassName ItemExtraDataFactory
 * @Description ItemExtraDataFactory
 * @Author liyunpeng
 **/
public class ItemExtraDataFactory {
    public static ItemExtraData create(int itemID) {
        ItemExtraData itemExtraData = null;
        ItemTemplate itemTemplate = ConfigTool.getBagConfig().getItemTemplate(itemID);
        switch (itemTemplate.getType()) {
            case ItemTypeConst.Equipment -> itemExtraData = new EquipmentExtraData();
        }
        if (itemExtraData != null) {
            itemExtraData.init(itemID);
        }
        return itemExtraData;
    }
}
