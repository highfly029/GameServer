package com.highfly029.logic.common.container.item;

/**
 * @ClassName EquipmentSlotData
 * @Description 装备槽数据
 * @Author liyunpeng
 **/
public class EquipmentSlotData {
    /**
     * 是否开启槽位
     */
    public boolean isOpen;
    /**
     * 装备物品数据
     */
    public ItemData equipment;

    public static EquipmentSlotData create() {
        EquipmentSlotData equipmentSlotData = new EquipmentSlotData();
        return equipmentSlotData;
    }
}
