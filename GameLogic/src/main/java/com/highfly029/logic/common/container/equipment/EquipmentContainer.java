package com.highfly029.logic.common.container.equipment;

import com.highfly029.logic.common.container.item.EquipmentSlotData;
import com.highfly029.logic.common.container.item.ItemData;
import com.highfly029.logic.common.container.item.extra.EquipmentExtraData;
import com.highfly029.utils.collection.IntIntMap;
import com.highfly029.utils.collection.IntObjectMap;

/**
 * @ClassName EquipmentContainer
 * @Description 装备容器
 * @Author liyunpeng
 **/
public class EquipmentContainer {
    /**
     * 装备数据集合
     */
    private IntObjectMap<EquipmentSlotData> equipmentMap = new IntObjectMap<>(EquipmentSlotData[]::new);
    /**
     * 装备套装数据记录 key=suitID, value=num
     */
    private IntIntMap suitMap = new IntIntMap();

    public IntObjectMap<EquipmentSlotData> getEquipmentMap() {
        return equipmentMap;
    }

    /**
     * 增加初始槽位
     *
     * @param slot
     * @param isOpen
     * @return
     */
    public boolean addInitSlot(int slot, boolean isOpen) {
        if (equipmentMap.contains(slot)) {
            return false;
        }
        EquipmentSlotData equipmentSlotData = EquipmentSlotData.create();
        equipmentSlotData.isOpen = isOpen;
        equipmentMap.put(slot, equipmentSlotData);
        return true;
    }

    /**
     * 增加初始默认装备
     *
     * @param slot
     * @param equipmentSlotData
     */
    public void addInitDefaultFromDB(int slot, EquipmentSlotData equipmentSlotData) {
        if (equipmentMap.contains(slot)) {
            return;
        }
        equipmentMap.put(slot, equipmentSlotData);
    }

    /**
     * 获取装备槽的物品数据
     *
     * @param slot
     * @return
     */
    public ItemData getEquipment(int slot) {
        EquipmentSlotData equipmentSlotData = equipmentMap.get(slot);
        if (equipmentSlotData != null) {
            return equipmentSlotData.equipment;
        } else {
            return null;
        }
    }

    /**
     * 是否可穿上装备
     *
     * @param slot
     * @param equipmentSlotData
     * @param isCanReplace
     * @return
     */
    public boolean checkCanPutOn(int slot, EquipmentSlotData equipmentSlotData, boolean isCanReplace) {
        if (equipmentSlotData == null) {
            return false;
        }
        if (equipmentSlotData.isOpen == false) {
            return false;
        }
        //不可替换装备增加判断
        if (!isCanReplace) {
            return equipmentSlotData.equipment == null;
        }
        return true;
    }

    /**
     * 穿装备
     *
     * @param slot
     * @param itemData
     * @param isCanReplace 是否可替换
     * @return
     */
    public boolean putOnEquipment(int slot, ItemData itemData, boolean isCanReplace) {
        EquipmentSlotData equipmentSlotData = equipmentMap.get(slot);
        if (!checkCanPutOn(slot, equipmentSlotData, isCanReplace)) {
            return false;
        }

        //可替换装备 先删除效果
        if (isCanReplace) {
            if (equipmentSlotData.equipment != null) {
                onEquipmentPutOff(slot, equipmentSlotData.equipment);
                EquipmentExtraData equipmentExtraData = (EquipmentExtraData) equipmentSlotData.equipment.getExtraData();
                int suitID = equipmentExtraData.getEquipmentTemplate().getSuit();
                if (suitID > 0) {
                    int preSuitNum = suitMap.get(suitID);
                    suitMap.addValue(suitID, -1);
                    onRemoveEquipmentSuitNum(suitID, preSuitNum);
                }
            }
        }

        equipmentSlotData.equipment = itemData;

        onEquipmentPutOn(slot, itemData);
        EquipmentExtraData equipmentExtraData = (EquipmentExtraData) itemData.getExtraData();
        int suitID = equipmentExtraData.getEquipmentTemplate().getSuit();
        if (suitID > 0) {
            int newSuitNum = suitMap.addValue(suitID, 1);
            onAddEquipmentSuitNum(suitID, newSuitNum);
        }
        return true;
    }

    /**
     * 是否可脱装备
     *
     * @param slot
     * @return
     */
    public boolean checkCanPutOff(int slot, EquipmentSlotData equipmentSlotData) {
        if (equipmentSlotData == null) {
            return false;
        }
        if (equipmentSlotData.isOpen == false) {
            return false;
        }
        return equipmentSlotData.equipment != null;
    }

    /**
     * 脱装备
     *
     * @param slot
     * @return
     */
    public boolean putOffEquipment(int slot) {
        EquipmentSlotData equipmentSlotData = equipmentMap.get(slot);
        if (!checkCanPutOff(slot, equipmentSlotData)) {
            return false;
        }
        ItemData itemData = equipmentSlotData.equipment;
        equipmentSlotData.equipment = null;

        onEquipmentPutOff(slot, itemData);
        EquipmentExtraData equipmentExtraData = (EquipmentExtraData) itemData.getExtraData();
        int suitID = equipmentExtraData.getEquipmentTemplate().getSuit();
        if (suitID > 0) {
            int preSuitNum = suitMap.get(suitID);
            suitMap.addValue(suitID, -1);
            onRemoveEquipmentSuitNum(suitID, preSuitNum);
        }
        return true;
    }

    /**
     * 穿装备通知
     *
     * @param slot
     * @param itemData
     */
    public void onEquipmentPutOn(int slot, ItemData itemData) {
    }

    /**
     * 脱装备通知
     *
     * @param slot
     * @param itemData
     */
    public void onEquipmentPutOff(int slot, ItemData itemData) {
    }

    /**
     * 增加装备套装数量
     *
     * @param suitID
     * @param newSuitNum
     */
    public void onAddEquipmentSuitNum(int suitID, int newSuitNum) {
    }

    /**
     * 减少装备套装数量
     *
     * @param suitID
     * @param preSuitNum
     */
    public void onRemoveEquipmentSuitNum(int suitID, int preSuitNum) {
    }
}
