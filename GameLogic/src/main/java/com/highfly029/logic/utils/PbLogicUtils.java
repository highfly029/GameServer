package com.highfly029.logic.utils;

import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbMail;
import com.highfly029.common.protocol.packet.PbQuest;
import com.highfly029.logic.common.container.item.EquipmentSlotData;
import com.highfly029.logic.common.container.item.ItemData;
import com.highfly029.logic.common.container.item.ItemFactory;
import com.highfly029.logic.common.container.item.extra.ItemExtraData;
import com.highfly029.logic.common.container.item.extra.ItemExtraDataFactory;
import com.highfly029.logic.module.bag.PlayerItemContainer;
import com.highfly029.logic.module.equipment.PlayerEquipmentContainer;
import com.highfly029.logic.module.goal.GoalData;
import com.highfly029.logic.module.mail.MailData;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.module.quest.QuestData;
import com.highfly029.utils.collection.IntLongMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName PbLogicUtils
 * @Description PbLogicUtils
 * @Author liyunpeng
 **/
public class PbLogicUtils {

    /**
     * 物品数据
     *
     * @param itemData
     * @param index    背包格子索引
     * @return
     */
    public static PbCommon.PbItemData itemObj2Pb(ItemData itemData, int index) {
        PbCommon.PbItemData.Builder builder = PbCommon.PbItemData.newBuilder();
        builder.setItemID(itemData.getItemID());
        builder.setItemNum(itemData.getItemNum());
        builder.setIsBind(itemData.isBind());
        builder.setInvalidTime(itemData.getInvalidTime());
        builder.setCreateID(itemData.getCreateID());
        if (itemData.getExtraData() != null) {
            builder.setExtraData(itemData.getExtraData().extraDataToPb());
        }
        builder.setIndex(index);

        return builder.build();
    }

    /**
     * 物品数据
     *
     * @param pbItemDataInDb
     * @return
     */
    public static ItemData itemPb2Obj(PbCommon.PbItemData pbItemDataInDb) {
        ItemData itemData = ItemFactory.create(pbItemDataInDb.getItemID(), pbItemDataInDb.getItemNum());
        itemData.setBind(pbItemDataInDb.getIsBind());
        itemData.setInvalidTime(pbItemDataInDb.getInvalidTime());
        itemData.setCreateID(pbItemDataInDb.getCreateID());
        if (pbItemDataInDb.hasExtraData()) {
            ItemExtraData itemExtraData = ItemExtraDataFactory.create(pbItemDataInDb.getItemID());
            itemExtraData.extraDataInitFromPb(pbItemDataInDb.getExtraData());
            itemData.setExtraData(itemExtraData);
        }
        return itemData;
    }

    /**
     * 单个背包容器数据
     *
     * @param playerItemContainer
     * @param type                背包容器类型
     * @return
     */
    public static PbCommon.PbItemContainerData playerItemContainerObj2Pb(PlayerItemContainer playerItemContainer, int type) {
        PbCommon.PbItemContainerData.Builder builder = PbCommon.PbItemContainerData.newBuilder();
        builder.setType(type);
        builder.setGridNum(playerItemContainer.getGridNum());
        ItemData[] itemDataArray = playerItemContainer.getItemArray();
        ItemData itemData;
        for (int i = 0, len = itemDataArray.length; i < len; i++) {
            if ((itemData = itemDataArray[i]) != null) {
                PbCommon.PbItemData pbItemData = itemObj2Pb(itemData, i);
                builder.addItemDatas(pbItemData);
            }
        }
        return builder.build();
    }

    public static PlayerItemContainer playerItemContainerPb2Obj(PbCommon.PbItemContainerData pbItemContainerData, Player player) {
        PlayerItemContainer playerItemContainer = new PlayerItemContainer(pbItemContainerData.getGridNum(), pbItemContainerData.getType(), player);
        for (int i = 0, len = pbItemContainerData.getItemDatasCount(); i < len; i++) {
            PbCommon.PbItemData pbItemData = pbItemContainerData.getItemDatas(i);
            ItemData itemData = itemPb2Obj(pbItemData);
            playerItemContainer.addInitDefaultItem(itemData, pbItemData.getIndex());
        }
        return playerItemContainer;
    }

    /**
     * 装备槽
     *
     * @param equipmentSlotData
     * @return
     */
    public static PbCommon.PbEquipmentSlotData equipmentSlotObj2Pb(EquipmentSlotData equipmentSlotData, int slotIndex) {
        PbCommon.PbEquipmentSlotData.Builder builder = PbCommon.PbEquipmentSlotData.newBuilder();
        builder.setIsOpen(equipmentSlotData.isOpen);
        builder.setSlotIndex(slotIndex);
        if (equipmentSlotData.equipment != null) {
            builder.setItemData(itemObj2Pb(equipmentSlotData.equipment, 0));
        }
        return builder.build();
    }

    public static EquipmentSlotData equipmentSlotPb2Obj(PbCommon.PbEquipmentSlotData pbEquipmentSlotData) {
        EquipmentSlotData equipmentSlotData = EquipmentSlotData.create();
        equipmentSlotData.isOpen = pbEquipmentSlotData.getIsOpen();
        if (pbEquipmentSlotData.hasItemData()) {
            equipmentSlotData.equipment = itemPb2Obj(pbEquipmentSlotData.getItemData());
        }
        return equipmentSlotData;
    }

    /**
     * 单个装备容器数据
     *
     * @param playerEquipmentContainer
     * @return
     */
    public static PbCommon.PbEquipmentData playerEquipmentObj2Pb(PlayerEquipmentContainer playerEquipmentContainer) {
        PbCommon.PbEquipmentData.Builder builder = PbCommon.PbEquipmentData.newBuilder();
        builder.setType(playerEquipmentContainer.getEquipmentType());
        int size = playerEquipmentContainer.getEquipmentMap().size();
        if (size > 0) {
            playerEquipmentContainer.getEquipmentMap().foreachImmutable((k, v) -> {
                PbCommon.PbEquipmentSlotData pbEquipmentSlotData = equipmentSlotObj2Pb(v, k);
                builder.addEquipmentSlots(pbEquipmentSlotData);
            });
        }
        return builder.build();
    }

    public static PlayerEquipmentContainer playerEquipmentPb2Obj(PbCommon.PbEquipmentData pbEquipmentData, Player player) {
        PlayerEquipmentContainer playerEquipmentContainer = new PlayerEquipmentContainer(pbEquipmentData.getType(), player);
        for (int i = 0, len = pbEquipmentData.getEquipmentSlotsCount(); i < len; i++) {
            PbCommon.PbEquipmentSlotData pbEquipmentSlotData = pbEquipmentData.getEquipmentSlots(i);
            EquipmentSlotData equipmentSlotData = equipmentSlotPb2Obj(pbEquipmentSlotData);
            playerEquipmentContainer.addInitDefaultFromDB(pbEquipmentSlotData.getSlotIndex(), equipmentSlotData);
        }
        return playerEquipmentContainer;
    }

    //目标
    public static PbCommon.PbGoalData goalObj2Pb(GoalData goalData) {
        PbCommon.PbGoalData.Builder builder = PbCommon.PbGoalData.newBuilder();
        builder.setGoalID(goalData.getGoalID());
        builder.setProgress(goalData.getProgress());
        return builder.build();
    }

    public static GoalData goalPb2Obj(PbCommon.PbGoalData pbGoalData) {
        GoalData goalData = GoalData.createByPb(pbGoalData.getGoalID(), pbGoalData.getProgress());
        return goalData;
    }

    //任务
    public static PbQuest.PbQuestData questObj2Pb(QuestData questData) {
        PbQuest.PbQuestData.Builder pbQuestDataBuilder = PbQuest.PbQuestData.newBuilder();
        pbQuestDataBuilder.setQuestID(questData.getQuestID());
        pbQuestDataBuilder.setInvalidTime(questData.getInvalidTime());
        if (questData.getGoals() != null && questData.getGoals().length > 0) {
            for (int j = 0, jLen = questData.getGoals().length; j < jLen; j++) {
                PbCommon.PbGoalData pbGoalData = goalObj2Pb(questData.getGoals()[j]);
                pbQuestDataBuilder.addGoals(pbGoalData);
            }
        }
        return pbQuestDataBuilder.build();
    }

    //邮件
    public static PbMail.PbMailData mailObj2Pb(MailData mailData) {
        PbMail.PbMailData.Builder mailDataBuilder = PbMail.PbMailData.newBuilder();
        mailDataBuilder.setMailID(mailData.getMailID());
        mailDataBuilder.setType(mailData.getType());
        mailDataBuilder.setTitleID(mailData.getTitleID());
        mailDataBuilder.setContentID(mailData.getContentID());
        if (mailData.getMailExtraArgs() != null) {
            PbMail.PbMailExtraArgs.Builder extraArgsBuilder = PbMail.PbMailExtraArgs.newBuilder();
            ObjectList<String> titleArgs = mailData.getMailExtraArgs().getTitleArgs();
            if (titleArgs != null) {
                for (int i = 0; i < titleArgs.size(); i++) {
                    extraArgsBuilder.addTitleArgs(titleArgs.get(i));
                }
            }
            ObjectList<String> contentArgs = mailData.getMailExtraArgs().getContentArgs();
            if (contentArgs != null) {
                for (int i = 0; i < contentArgs.size(); i++) {
                    extraArgsBuilder.addContentArgs(contentArgs.get(i));
                }
            }
            mailDataBuilder.setExtraArgs(extraArgsBuilder);
        }

        if (mailData.getAttachment() != null) {
            PbMail.PbMailAttachment.Builder attachmentBuilder = PbMail.PbMailAttachment.newBuilder();
            ObjectList<ItemData> itemList = mailData.getAttachment().getItemList();
            if (itemList != null) {
                for (int i = 0; i < itemList.size(); i++) {
                    PbCommon.PbItemData pbItemData = PbLogicUtils.itemObj2Pb(itemList.get(i), 0);
                    attachmentBuilder.addItemList(pbItemData);
                }
            }

            IntLongMap assetMap = mailData.getAttachment().getAssetMap();
            if (assetMap != null) {
                assetMap.foreachImmutable((k, v) -> {
                    PbCommon.PbKeyValue.Builder kvBuilder = PbCommon.PbKeyValue.newBuilder();
                    kvBuilder.setIntKey(k).setLongValue(v);
                    attachmentBuilder.addAssetList(kvBuilder);
                });
            }
            mailDataBuilder.setAttachment(attachmentBuilder);
        }
        mailDataBuilder.setIsRead(mailData.getIsRead());
        mailDataBuilder.setIsReceive(mailData.getIsReceive());
        mailDataBuilder.setBehaviorSource(mailData.getBehaviorSource());
        mailDataBuilder.setCreateTime(mailData.getCreateTime());
        return mailDataBuilder.build();
    }

    public static MailData mailPb2Obj(PbMail.PbMailData pbMailData) {
        MailData mailData = null;
        return mailData;
    }
}
