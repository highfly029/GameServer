package com.highfly029.logic.manager;

import com.highfly029.common.data.activity.ActivityTimeData;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.template.activity.ActivityDriveTypeConst;
import com.highfly029.common.template.activity.ActivityRelativeTimeConst;
import com.highfly029.common.template.activity.ActivityTemplate;
import com.highfly029.common.template.activity.ActivityTimeTypeConst;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.manager.BaseManager;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.logic.global.LogicMysqlGlobalData;
import com.highfly029.logic.module.activity.ActivityConfig;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.world.Global2LogicEvent;
import com.highfly029.logic.world.LogicGlobalWorld;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;

import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName ActivityGlobalManager
 * @Description ActivityGlobalManager
 * @Author liyunpeng
 **/
public class ActivityGlobalManager extends BaseManager {
    private LogicGlobalWorld logicGlobalWorld;
    /**
     * 开启中的逻辑服活动集合
     */
    private final IntSet openedLogicActivitySet = new IntSet();

    public ActivityGlobalManager(LogicGlobalWorld logicGlobalWorld) {
        super(logicGlobalWorld);
        this.logicGlobalWorld = logicGlobalWorld;
    }

    @Override
    public void init() {
        ActivityConfig activityConfig = ConfigTool.getActivityConfig();
        IntObjectMap<ActivityTemplate> activityTemplateMap = activityConfig.getActivityTemplateMap();
        IntObjectMap<ActivityTimeData> logicActivityTimeDataMap = activityConfig.getLogicActivityTemplateDataMap();

        MySQLUtils.sendGlobal(logicGlobalWorld.getConnectionPool(), "SELECT * FROM activity;", null, (isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                IntSet initSet = new IntSet();
                for (Row row : successRows) {
                    int id = row.getInteger("id");
                    long startTime = row.getLong("startTime");
                    long endTime = row.getLong("endTime");
                    ActivityTemplate activityTemplate = activityTemplateMap.get(id);
                    //设置开启中的循环活动的时间
                    if (activityTemplate.getActivityTimeType() == ActivityTimeTypeConst.CycleTime) {
                        ActivityTimeData activityTimeData = logicActivityTimeDataMap.get(id);
                        activityTimeData.setStartTime(startTime);
                        activityTimeData.setEndTime(endTime);
                    }
                    //逻辑服启动时初始化已经在开启中的活动
                    openedLogicActivitySet.add(id);
                    initSet.add(id);
                }
                logicGlobalWorld.postEvent2AllChild(Global2LogicEvent.INIT_ACTIVITY, initSet, null);
            } else {
                LogicLoggerTool.gameLogger.error("ActivityGlobalManager init select fail {}", errorMsg);
            }
        });
    }

    /**
     * 获取中心服的活动
     *
     * @param session
     */
    public void getCenterActivity(Session session) {
        session.sendMsgS2S(PtCode.Logic2CenterGetOpenedActivity, null);
    }

    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {
        ActivityConfig activityConfig = ConfigTool.getActivityConfig();
        IntObjectMap<ActivityTemplate> activityTemplateMap = activityConfig.getActivityTemplateMap();
        IntObjectMap<ActivityTimeData> logicActivityTimeDataMap = activityConfig.getLogicActivityTemplateDataMap();
        long now = SystemTimeTool.getMillTime();
        activityTemplateMap.foreachImmutable((id, activityTemplate) -> {
            if (activityTemplate.getActivityDriveType() == ActivityDriveTypeConst.Logic) {
                ActivityTimeData activityTimeData = logicActivityTimeDataMap.get(id);
                //强制关闭活动
                if (activityTimeData.getForceCloseTime() > 0 && now >= activityTimeData.getForceCloseTime()) {
                    if (openedLogicActivitySet.contains(activityTemplate.getId())) {
                        closeActivity(activityTemplate, activityTimeData, true);
                    }
                    return;
                }

                if (activityTemplate.getActivityTimeType() == ActivityTimeTypeConst.RelativeTime &&
                        activityTimeData.getRelativeArgs()[0] == ActivityRelativeTimeConst.LogicOpenServerTime) {
                    long t1 = LogicMysqlGlobalData.openServerTime + activityTimeData.getRelativeArgs()[1] * 1000L;
                    long t2 = t1 + activityTemplate.getDurationTime() * 1000L;
                    if (now >= t1 && now <= t2) {
                        if (!openedLogicActivitySet.contains(activityTemplate.getId())) {
                            openActivity(activityTemplate, activityTimeData);
                        }
                    }
                    if (now > t2 && openedLogicActivitySet.contains(activityTemplate.getId())) {
                        closeActivity(activityTemplate, activityTimeData, false);
                    }

                } else {
                    if (now >= activityTimeData.getStartTime() && now <= activityTimeData.getEndTime()) {
                        if (!openedLogicActivitySet.contains(activityTemplate.getId())) {
                            openActivity(activityTemplate, activityTimeData);
                        }
                    }
                    if (now > activityTimeData.getEndTime() && openedLogicActivitySet.contains(activityTemplate.getId())) {
                        closeActivity(activityTemplate, activityTimeData, false);
                        //重新计算循环活动的下次时间
                        if (activityTemplate.getActivityTimeType() == ActivityTimeTypeConst.CycleTime) {
                            activityTimeData.refreshCronExpressionTime();
                        }
                    }
                }
            }
        });
    }

    /**
     * 开启新的活动
     *
     * @param activityTemplate
     * @param activityTimeData
     */
    private void openActivity(ActivityTemplate activityTemplate, ActivityTimeData activityTimeData) {
        openedLogicActivitySet.add(activityTemplate.getId());
        LogicLoggerTool.gameLogger.info("开启活动 {}", activityTemplate.getId());
        Tuple tuple = Tuple.of(activityTemplate.getId(), activityTimeData.getStartTime(), activityTimeData.getEndTime());
        MySQLUtils.sendGlobal(logicGlobalWorld.getConnectionPool(), "INSERT INTO activity VALUES(?, ?, ?);", tuple, (isSuccess, successRows, errorMsg) -> {
            if (!isSuccess) {
                LogicLoggerTool.gameLogger.error("openActivity insert fail {} {}", activityTemplate.getId(), errorMsg);
            }
        });

        logicGlobalWorld.postEvent2AllChild(Global2LogicEvent.OPEN_ACTIVITY, activityTemplate.getId(), null);
    }

    /**
     * 关闭活动
     *
     * @param activityTemplate
     * @param activityTimeData
     */
    private void closeActivity(ActivityTemplate activityTemplate, ActivityTimeData activityTimeData, boolean forceClose) {
        openedLogicActivitySet.remove(activityTemplate.getId());
        if (forceClose) {
            LogicLoggerTool.gameLogger.info("强制关闭活动 {}", activityTemplate.getId());
        } else {
            LogicLoggerTool.gameLogger.info("关闭活动 {}", activityTemplate.getId());
        }
        MySQLUtils.sendGlobal(logicGlobalWorld.getConnectionPool(), "DELETE FROM activity WHERE id=" + activityTemplate.getId(), null, (isSuccess, successRows, errorMsg) -> {
            if (!isSuccess) {
                LogicLoggerTool.gameLogger.error("closeActivity delete fail {} {}", activityTemplate.getId(), errorMsg);
            }
        });
        logicGlobalWorld.postEvent2AllChild(Global2LogicEvent.CLOSE_ACTIVITY, activityTemplate.getId(), forceClose);
    }

    @Override
    public void onMinute() {

    }

    @Override
    public void onShutDown() {

    }
}
