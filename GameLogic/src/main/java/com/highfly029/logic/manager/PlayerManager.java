package com.highfly029.logic.manager;

import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLogin;
import com.highfly029.core.manager.BaseManager;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.world.Logic2GlobalEvent;
import com.highfly029.logic.world.LogicChildWorld;
import com.highfly029.utils.collection.LongObjectMap;

/**
 * @ClassName PlayerManager
 * @Description 玩家管理类
 * @Author liyunpeng
 **/
public class PlayerManager extends BaseManager {
    private LogicChildWorld logicChildWorld;

    /**
     * 玩家集合 key playerID
     */
    private final LongObjectMap<Player> players = new LongObjectMap<>(Player[]::new);

    public PlayerManager(LogicChildWorld logicChildWorld) {
        super(logicChildWorld);
        this.logicChildWorld = logicChildWorld;
    }

    /**
     * 创建玩家
     *
     * @return
     */
    public Player create() {
        Player player = new Player();
        return player;
    }

    /**
     * 添加玩家
     *
     * @param player
     */
    public void addPlayer(Player player) {
        logicChildWorld.postEvent2Global(Logic2GlobalEvent.ADD_PLAYER, logicChildWorld.getIndex(), player.getPlayerID());
        players.put(player.getPlayerID(), player);
        LogicLoggerTool.gameLogger.info("PlayerManager addPlayer {}", player.getPlayerID());
    }

    /**
     * 删除玩家
     *
     * @param playerID
     * @return
     */
    public void removePlayer(long playerID) {
        Player player = players.remove(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("player logout is null playerID={}", playerID);
            return;
        }
        player.logout();
        LogicLoggerTool.gameLogger.info("PlayerManager removePlayer {}", playerID);
        logicChildWorld.postEvent2Global(Logic2GlobalEvent.REMOVE_PLAYER, logicChildWorld.getIndex(), playerID);
    }

    /**
     * 踢出玩家
     *
     * @param playerID
     */
    public void kickOutPlayer(long playerID) {
        LogicLoggerTool.gameLogger.info("kickOutPlayer playerID={}", playerID);
        Player player = getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("kickOutPlayer is null playerID={}", playerID);
            return;
        }
        removePlayer(playerID);
        //发送给客户端提出玩家消息，让客户端关闭进程
        PbLogin.S2CKickOut.Builder builder = PbLogin.S2CKickOut.newBuilder();
        builder.setPlayerID(playerID);
        player.send(PtCode.S2CKickOut, builder.build());
        //关闭连接后global的断开连接事件里,PLAYER_OFFLINE已经没有player
        player.getSession().close();

        player.sendMsg2Scene(PtCode.L2SKickOutPlayerEntity, null);
    }

    /**
     * 获取当前线程所有玩家
     *
     * @return
     */
    public LongObjectMap<Player> getAllPlayers() {
        return players;
    }

    /**
     * 获取玩家
     *
     * @param playerID
     * @return
     */
    public Player getPlayer(long playerID) {
        return players.get(playerID);
    }


    @Override
    public void onTick(int escapedMillTime) {
        players.foreachMutable((k, v) -> {
            if (v.isTriggerLogout()) {
                removePlayer(k);
            }
        });

        long freeValue = players.getFreeValue();
        long[] keys = players.getKeys();
        Player[] values = players.getValues();
        long key;
        Player player;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                player = values[i];
                player.onPlayerTick(escapedMillTime);
            }
        }
    }

    @Override
    public void onSecond() {
        long freeValue = players.getFreeValue();
        long[] keys = players.getKeys();
        Player[] values = players.getValues();
        long key;
        Player player;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                player = values[i];
                player.onPlayerSecond();
            }
        }
    }

    @Override
    public void onMinute() {
        long freeValue = players.getFreeValue();
        long[] keys = players.getKeys();
        Player[] values = players.getValues();
        long key;
        Player player;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                player = values[i];
                player.onPlayerMinute();
            }
        }
    }

    @Override
    public void onShutDown() {
        long freeValue = players.getFreeValue();
        long[] keys = players.getKeys();
        Player[] values = players.getValues();
        long key;
        Player player;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                player = values[i];
                player.logout();
            }
        }
    }

    public void onDaily() {
        long freeValue = players.getFreeValue();
        long[] keys = players.getKeys();
        Player[] values = players.getValues();
        long key;
        Player player;
        for (int i = 0, len = keys.length; i < len; i++) {
            if ((key = keys[i]) != freeValue) {
                player = values[i];
                player.onPlayerDaily(false);
            }
        }
    }
}
