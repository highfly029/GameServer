package com.highfly029.logic.manager;

import com.highfly029.core.manager.BaseManager;
import com.highfly029.logic.world.LogicChildWorld;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName ActivityChildManager
 * @Description ActivityChildManager
 * @Author liyunpeng
 **/
public class ActivityChildManager extends BaseManager {
    private final LogicChildWorld logicChildWorld;
    /**
     * 逻辑服和中心服驱动的正在开启中的活动集合
     */
    private final IntSet openedActivitySet = new IntSet();

    public ActivityChildManager(LogicChildWorld logicChildWorld) {
        super(logicChildWorld);
        this.logicChildWorld = logicChildWorld;
    }

    public IntSet getOpenedActivitySet() {
        return openedActivitySet;
    }

    /**
     * 增加初始化活动
     *
     * @param initSet
     */
    public void addInitActivity(IntSet initSet) {
        initSet.foreachImmutable(openedActivitySet::add);
    }

    /**
     * 增加开启的活动
     *
     * @param id
     */
    public void addOpenActivity(int id) {
        openedActivitySet.add(id);
        logicChildWorld.getPlayerManager().getAllPlayers().foreachImmutable((playerID, player) -> {
            player.getActivityModule().onOpenActivity(id);
        });
    }

    /**
     * 删除关闭的活动
     *
     * @param id
     * @param forceClose
     */
    public void removeCloseActivity(int id, boolean forceClose) {
        openedActivitySet.remove(id);
        logicChildWorld.getPlayerManager().getAllPlayers().foreachImmutable((playerID, player) -> {
            player.getActivityModule().onCloseActivity(id, forceClose);
        });
    }

    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {

    }

    @Override
    public void onMinute() {

    }

    @Override
    public void onShutDown() {

    }
}
