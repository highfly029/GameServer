package com.highfly029.logic.manager;

import com.highfly029.core.manager.BaseManager;
import com.highfly029.logic.global.LogicConst;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.world.LogicGlobalWorld;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.LongIntMap;
import com.highfly029.utils.collection.LongSet;

/**
 * @ClassName PlayerGlobalManager
 * @Description 进程全局玩家管理类
 * @Author liyunpeng
 **/
public class PlayerGlobalManager extends BaseManager {
    /**
     * 整个GameScene所有的玩家对应的index
     */
    private final LongIntMap allPlayers = new LongIntMap();

    /**
     * 每个子世界包含的玩家
     */
    private final IntObjectMap<LongSet> childWorldPlayers = new IntObjectMap<>(LongSet[]::new);

    public PlayerGlobalManager(LogicGlobalWorld logicGlobalWorld) {
        super(logicGlobalWorld);
    }

    /**
     * 增加一个player
     *
     * @param index
     * @param playerID
     */
    public void addPlayer(int index, long playerID) {
        allPlayers.put(playerID, index);
        LongSet set = childWorldPlayers.get(index);
        if (set == null) {
            set = new LongSet();
            childWorldPlayers.put(index, set);
        }
        set.add(playerID);
        LogicLoggerTool.gameLogger.info("PlayerGlobalManager addPlayer {}", playerID);
    }

    /**
     * 删除一个player
     *
     * @param index
     * @param playerID
     */
    public void removePlayer(int index, long playerID) {
        allPlayers.remove(playerID);
        LongSet set = childWorldPlayers.get(index);
        if (set != null) {
            set.remove(playerID);
        }
        LogicLoggerTool.gameLogger.info("PlayerGlobalManager removePlayer {}", playerID);
    }

    /**
     * 获取player所在的index
     *
     * @param playerID
     * @return
     */
    public int getPlayerWorldIndex(long playerID) {
        return allPlayers.get(playerID);
    }

    /**
     * 获取所有玩家
     *
     * @return
     */
    public LongIntMap getAllPlayers() {
        return allPlayers;
    }

    /**
     * 获取玩家最少的LogicChildWorldIndex
     *
     * @return
     */
    public int getLeastPlayersWorldIndex() {
        int min = Integer.MAX_VALUE;
        int minIndex = 0;
        for (int i = 0; i < LogicConst.LOGIC_CHILD_WORLD_NUM; i++) {
            int index = i + 1;
            LongSet playerSet = childWorldPlayers.get(index);
            //如果当前场景子世界没人则直接返回
            if (playerSet == null || playerSet.isEmpty()) {
                return index;
            }
            if (playerSet.size() < min) {
                min = playerSet.size();
                minIndex = index;
            }
        }
        return minIndex;
    }


    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {

    }

    @Override
    public void onMinute() {

    }

    @Override
    public void onShutDown() {

    }
}
