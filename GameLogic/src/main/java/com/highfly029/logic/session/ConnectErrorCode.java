package com.highfly029.logic.session;

/**
 * @ClassName ConnectErrorCode
 * @Description 连接错误码
 * @Author liyunpeng
 **/
public enum ConnectErrorCode {
    invalidMd5,

    oldSessionNull,

    playerNull,

    playerIDZero,

    invalidToken,
}
