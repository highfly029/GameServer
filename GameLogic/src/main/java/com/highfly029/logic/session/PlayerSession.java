package com.highfly029.logic.session;

import java.util.function.Consumer;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.core.net.tcp.ChannelAttribute;
import com.highfly029.core.plugins.counter.IntegerCounter;
import com.highfly029.core.protocol.BasePtCode;
import com.highfly029.core.session.Session;
import com.highfly029.logic.module.Player;


/**
 * @ClassName Session
 * @Description session
 * @Author liyunpeng
 **/
public class PlayerSession extends Session {
    /**
     * 发送缓存大小
     */
    private static final int SEND_CACHE_SIZE = 128;
    /**
     * 客户端自增协议序号
     */
    private long sequenceNum;
    /**
     * 服务端生成的channel唯一标示
     */
    private int socketID;

    private Player player;

    /**
     * 客户端接收序号 global set之后需要 child需要立刻可见 需要用volatile
     */
    private volatile long clientReceivedIndex;
    /**
     * 服务端发送序号
     */
    private long serverSendSerialNum;
    /**
     * 发送索引计数器
     */
    private IntegerCounter sendIndexCounter = new IntegerCounter(0, SEND_CACHE_SIZE - 1, true);
    /**
     * 发送缓存 只包含logic线程
     */
    private GeneratedMessageV3[] sendCache = new GeneratedMessageV3[SEND_CACHE_SIZE];

    public long getSequenceNum() {
        return sequenceNum;
    }

    public void setSequenceNum(long sequenceNum) {
        this.sequenceNum = sequenceNum;
    }

    public int getSocketID() {
        return socketID;
    }

    public void setSocketID(int socketID) {
        this.socketID = socketID;
    }

    public Player getPlayer() {
        return player;
    }

    public void copy(PlayerSession other) {
        serverSendSerialNum = other.serverSendSerialNum;
        for (int i = 0; i < SEND_CACHE_SIZE; i++) {
            sendCache[i] = other.sendCache[i];
        }
        sendIndexCounter.setCount(other.sendIndexCounter.getCount());
    }

    public void setClientReceivedIndex(long clientReceivedIndex) {
        this.clientReceivedIndex = clientReceivedIndex;
    }

    public long getClientReceivedIndex() {
        return clientReceivedIndex;
    }

    public void setPlayer(Player player) {
        this.player = player;
        if (player != null) {
            //设置channel 关联playerID
            getChannel().attr(ChannelAttribute.playerID).set(player.getPlayerID());
        }
    }

    @Override
    protected void addSendCache(int protocolCode, GeneratedMessageV3 message) {
        //BasePtCode会有线程安全问题
        if (protocolCode > BasePtCode.MAX) {
            serverSendSerialNum++;
            int index = sendIndexCounter.getNextValue();
            //只缓存logic线程的发送数据、重连时player重新进入entity线程
            sendCache[index] = message;
//            JsonFormat jsonFormat = new JsonFormat();
//            LoggerTool.systemLogger.info("addSendCache serverSendSerialNum={}, index={}, message={}", serverSendSerialNum, index, jsonFormat.printToString(message));
        }
    }

    public long getServerSendSerialNum() {
        return serverSendSerialNum;
    }

    /**
     * 获取发送缓存大小
     *
     * @param clientReceivedIndex
     * @return
     */
    public int getSendCacheDelta(long clientReceivedIndex) {
        int delta = (int) (serverSendSerialNum - clientReceivedIndex);
        if (delta < 0) {
            return -1;
        }
        if (delta > SEND_CACHE_SIZE) {
            return -2;
        }
        return delta;
    }

    /**
     * 处理发送缓存
     *
     * @param delta
     * @param consumer
     */
    public void processSendCache(int delta, Consumer<GeneratedMessageV3> consumer) {
        int curIndex = sendIndexCounter.getCount();
        int begin = curIndex - delta;
//        JsonFormat jsonFormat = new JsonFormat();
        if (begin >= 0) {
            for (int i = begin; i < curIndex; i++) {
                GeneratedMessageV3 message = sendCache[i];
//                LoggerTool.systemLogger.warn("processSendCache1 发送了数据 index={} message={}", i, jsonFormat.printToString(message));
                consumer.accept(message);
            }
        } else {
            begin = SEND_CACHE_SIZE - (delta - curIndex);
            for (int i = begin; i < SEND_CACHE_SIZE; i++) {
                GeneratedMessageV3 message = sendCache[i];
//                LoggerTool.systemLogger.warn("processSendCache2 发送了数据 index={} message={}", i, jsonFormat.printToString(message));
                consumer.accept(message);
            }
            for (int i = 0; i < curIndex; i++) {
                GeneratedMessageV3 message = sendCache[i];
//                LoggerTool.systemLogger.warn("processSendCache3 发送了数据 index={} message={}", i, jsonFormat.printToString(message));
                consumer.accept(message);
            }
        }
    }

    /**
     * 重置Counter
     */
    public void resetSendIndex() {
        sendIndexCounter.reset();
        serverSendSerialNum = 0;
    }

    @Override
    public String toString() {
        return "PlayerSession{" +
                "sequenceNum=" + sequenceNum +
                ", socketID=" + socketID +
                ", player=" + player +
                ", clientReceivedIndex=" + clientReceivedIndex +
                ", serverSendSerialNum=" + serverSendSerialNum +
                ", sendIndexCounter=" + sendIndexCounter.getCount() +
                ", channel=" + getChannel() +
                ", type=" + getType() +
                '}';
    }
}
