package com.highfly029.logic.serverProtocol.broker;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.logic.world.LogicGlobalWorld;

/**
 * @ClassName BrokerGlobalProtocol
 * @Description BrokerGlobalProtocol
 * @Author liyunpeng
 **/
public class BrokerGlobalProtocol implements IRegisterServerGlobalProtocol {
    private LogicGlobalWorld logicGlobalWorld;

    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {
        logicGlobalWorld = (LogicGlobalWorld) globalWorld;

        globalWorld.registerServerGlobalProtocol(PtCode.LoadBalance2LogicMessage, this::loadBalance2Logic);
    }

    /**
     * 别的服发送到逻辑服的消息
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void loadBalance2Logic(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbLoadBalance.LoadBalance2LogicMessage req = PbLoadBalance.LoadBalance2LogicMessage.parseFrom(packet.getData());
        WorldTool.addChildWorldLogicEvent(req.getToIndex(), LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }
}
