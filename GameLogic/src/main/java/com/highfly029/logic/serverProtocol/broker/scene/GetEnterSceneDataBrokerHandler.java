package com.highfly029.logic.serverProtocol.broker.scene;

import com.google.protobuf.ByteString;
import com.highfly029.common.data.base.Vector3;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.common.template.attribute.AttributeConst;
import com.highfly029.common.template.profession.ProfessionTemplate;
import com.highfly029.common.template.scene.SceneTemplate;
import com.highfly029.common.utils.PbCommonUtils;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.serverProtocol.broker.ILogicBrokerHandler;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.world.LogicChildWorld;
import com.highfly029.utils.collection.IntIntMap;

/**
 * @ClassName GetEnterSceneDataBrokerHandler
 * @Description 获取登陆场景数据
 * @Author liyunpeng
 **/
public class GetEnterSceneDataBrokerHandler implements ILogicBrokerHandler {
    @Override
    public int registerPtCode() {
        return PtCode.Scene2LogicGetEnterSceneData;
    }

    @Override
    public void handle(LogicChildWorld logicChildWorld, int from, int fromIndex, int to, int toIndex, long playerID, ByteString byteString) throws Exception {
        PbScene.Scene2LogicGetEnterSceneData req = PbScene.Scene2LogicGetEnterSceneData.parseFrom(byteString);
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("获取登陆场景数据 player={} is null", playerID);
            return;
        }

        int sceneID = req.getSceneID();
        player.getLocation().setSceneID(sceneID);
        player.getLocation().setInstanceID(req.getInstanceID());
        player.info("获取登陆场景数据", sceneID, req.getInstanceID());
        
        PbScene.Logic2SceneGetEnterSceneData.Builder builder = PbScene.Logic2SceneGetEnterSceneData.newBuilder();
        builder.setInstanceID(req.getInstanceID());
        generateTestData(player, sceneID, builder);

        logicChildWorld.sendMsg2Scene(from, fromIndex, playerID, PtCode.Logic2SceneGetEnterSceneData, builder.build());
    }

    /**
     * 生成测试数据
     *
     * @param player
     * @param builder
     */
    private void generateTestData(Player player, int sceneID, PbScene.Logic2SceneGetEnterSceneData.Builder builder) {
        SceneTemplate sceneTemplate = ConfigTool.getSceneConfig().getSceneTemplate(sceneID);
        Vector3 pos = Vector3.createWithFloats(sceneTemplate.getBirthPos());
        Vector3 dir = Vector3.create();
        dir.x = 1.f;
        builder.setCurPos(PbCommonUtils.vector3Obj2Pb(pos));
        builder.setCurDir(PbCommonUtils.vector3Obj2Pb(dir));
        byte professionID = 1;
        ProfessionTemplate professionTemplate = ConfigTool.getProfessionConfig().getProfessionTemplate(professionID);
        builder.setRace(professionTemplate.getBelongRace());
        builder.setModelID(professionTemplate.getDefaultModelID());

        //增加技能
        IntIntMap skillMap = new IntIntMap();
        for (int i = 0; i < 100; i++) {
            int skillID = 1001 + i;
            skillMap.put(skillID, 1);
        }
        builder.addAllActiveSkills(PbCommonUtils.keyValueIntIntMap2List(skillMap));

        //血量最大值
        PbCommon.PbKeyValue.Builder hpMax = PbCommon.PbKeyValue.newBuilder();
        hpMax.setIntKey(AttributeConst.HpMaxBase);
        hpMax.setIntValue(1000);
        builder.addAttributes(hpMax.build());

        //攻击
        PbCommon.PbKeyValue.Builder attack = PbCommon.PbKeyValue.newBuilder();
        attack.setIntKey(AttributeConst.PhysicsAttackBase);
        attack.setIntValue(100);
        builder.addAttributes(attack.build());
    }
}
