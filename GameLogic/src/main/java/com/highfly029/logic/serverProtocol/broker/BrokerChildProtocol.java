package com.highfly029.logic.serverProtocol.broker;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.core.interfaces.IRegisterServerChildProtocol;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.world.ChildWorld;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName BrokerChildProtocol
 * @Description BrokerChildProtocol
 * @Author liyunpeng
 **/
public class BrokerChildProtocol implements IRegisterServerChildProtocol {
    private LogicChildWorld logicChildWorld;

    @Override
    public void registerChildProtocol(ChildWorld childWorld) throws Exception {
        this.logicChildWorld = (LogicChildWorld) childWorld;

        childWorld.registerServerChildProtocol(PtCode.LoadBalance2LogicMessage, this::loadBalance2Logic);
    }

    /**
     * 别的服发送到逻辑服的消息
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void loadBalance2Logic(Session session, int ptCode, GeneratedMessageV3 message) {
        PbLoadBalance.LoadBalance2LogicMessage req = (PbLoadBalance.LoadBalance2LogicMessage) message;
        ILogicBrokerHandler logicBrokerHandler = logicChildWorld.getLogicBrokerHandlers().get(req.getPtCode());
        try {
            logicBrokerHandler.handle(logicChildWorld, req.getFrom(), req.getFromIndex(), req.getToIndex(), req.getToIndex(), req.getPlayerID(), req.getData());
        } catch (Exception e) {
            LoggerTool.systemLogger.error("loadBalance2Logic error, from={}, fromIndex={}, to={}, toIndex={}, ptCode={}, playerID={}",
                    req.getFrom(), req.getFromIndex(), req.getToIndex(), req.getToIndex(), req.getPtCode(), req.getPlayerID());
            LogicLoggerTool.gameLogger.error("loadBalance2Logic", e);
        }

    }


}
