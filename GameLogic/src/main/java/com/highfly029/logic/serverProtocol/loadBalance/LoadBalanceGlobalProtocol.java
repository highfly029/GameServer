package com.highfly029.logic.serverProtocol.loadBalance;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.logic.world.Global2LogicEvent;
import com.highfly029.logic.world.LogicGlobalWorld;

/**
 * @ClassName LoadBalanceGlobalProtocol
 * @Description 负载均衡服务器协议
 * @Author liyunpeng
 **/
public class LoadBalanceGlobalProtocol implements IRegisterServerGlobalProtocol {
    private LogicGlobalWorld logicGlobalWorld;

    @Override
    public void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception {
        logicGlobalWorld = (LogicGlobalWorld) globalWorld;

        globalWorld.registerServerGlobalProtocol(PtCode.L2SGetSceneServerInfo, this::getSceneServerInfo);
        globalWorld.registerServerGlobalProtocol(PtCode.L2SKickOutPlayer, this::kickOutPlayer);
        globalWorld.registerServerGlobalProtocol(PtCode.L2LPushOfflineMsg, this::pushOfflineMsg);
    }

    /**
     * 返回场景服信息
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void getSceneServerInfo(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbScene.L2SGetSceneServerInfo req = PbScene.L2SGetSceneServerInfo.parseFrom(packet.getData());
        WorldTool.addChildWorldLogicEvent(req.getLogicWorldIndex(), LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

    /**
     * 踢出玩家
     *
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void kickOutPlayer(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbLoadBalance.L2SKickOutPlayer req = PbLoadBalance.L2SKickOutPlayer.parseFrom(packet.getData());
        WorldTool.addChildWorldLogicEvent(req.getLogicWorldIndex(), LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, req);
    }

    /**
     * 推送离线消息
     * @param session
     * @param ptCode
     * @param packet
     * @throws InvalidProtocolBufferException
     */
    private void pushOfflineMsg(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException {
        PbLoadBalance.L2LPushOfflineMsg req = PbLoadBalance.L2LPushOfflineMsg.parseFrom(packet.getData());
        int worldIndex = logicGlobalWorld.getPlayerGlobalManager().getPlayerWorldIndex(req.getPlayerID());
        if (worldIndex > 0) {
            logicGlobalWorld.postEvent2Child(worldIndex, Global2LogicEvent.REMOTE_OFFLINE_MSG, req.getPlayerID(), null);
        }
    }
}
