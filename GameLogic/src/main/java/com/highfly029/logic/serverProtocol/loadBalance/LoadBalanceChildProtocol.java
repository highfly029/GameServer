package com.highfly029.logic.serverProtocol.loadBalance;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.common.protocol.packet.PbScene;
import com.highfly029.core.interfaces.IRegisterServerChildProtocol;
import com.highfly029.core.session.Session;
import com.highfly029.core.world.ChildWorld;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.world.LogicChildWorld;

/**
 * @ClassName LoadBalanceChildProtocol
 * @Description 负载均衡服务器协议
 * @Author liyunpeng
 **/
public class LoadBalanceChildProtocol implements IRegisterServerChildProtocol {
    private LogicChildWorld logicChildWorld;

    @Override
    public void registerChildProtocol(ChildWorld childWorld) throws Exception {
        this.logicChildWorld = (LogicChildWorld) childWorld;

        childWorld.registerServerChildProtocol(PtCode.L2SGetSceneServerInfo, this::getSceneServerInfo);
        childWorld.registerServerChildProtocol(PtCode.L2SKickOutPlayer, this::kickOutPlayer);
    }

    /**
     * 返回场景服信息
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void getSceneServerInfo(Session session, int ptCode, GeneratedMessageV3 message) {
        PbScene.L2SGetSceneServerInfo req = (PbScene.L2SGetSceneServerInfo) message;
        long playerID = req.getPlayerID();
        Player player = logicChildWorld.getPlayerManager().getPlayer(playerID);
        player.getSceneModule().onGetSceneServerInfo(req);
    }

    /**
     * 踢出玩家
     *
     * @param session
     * @param ptCode
     * @param message
     */
    private void kickOutPlayer(Session session, int ptCode, GeneratedMessageV3 message) {
        PbLoadBalance.L2SKickOutPlayer req = (PbLoadBalance.L2SKickOutPlayer) message;
        long playerID = req.getPlayerID();
        logicChildWorld.getPlayerManager().kickOutPlayer(playerID);
    }
}
