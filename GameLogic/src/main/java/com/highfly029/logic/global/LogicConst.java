package com.highfly029.logic.global;

import com.highfly029.core.constant.ConfigConst;
import com.highfly029.utils.SnowFlake;

/**
 * @ClassName LogicConst
 * @Description LogicConst
 * @Author liyunpeng
 **/
public class LogicConst {
    /**
     * 数据库连接的名字
     */
    public static final String DB_NAME = "gameDb";

    /**
     * 逻辑服子世界数量
     */
    public static final int LOGIC_CHILD_WORLD_NUM = 4;

    /**
     * playerID生成器
     */
    public static final SnowFlake playerIDSnowFlake = new SnowFlake(ConfigConst.identifier);

}
