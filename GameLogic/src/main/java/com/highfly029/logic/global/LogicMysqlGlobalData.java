package com.highfly029.logic.global;

/**
 * @ClassName LogicMysqlGlobalData
 * @Description 逻辑服数据库全局数据
 * @Author liyunpeng
 **/
public class LogicMysqlGlobalData {
    /**
     * 第一次初始化数据库时间
     */
    public static long initTime;

    /**
     * 开服时间
     */
    public static long openServerTime;
}
