package com.highfly029.logic.world;

import java.io.File;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.common.data.feature.CenterFeature;
import com.highfly029.common.data.feature.LoadBalanceFeature;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.common.redis.RedisKey;
import com.highfly029.common.templateBase.tool.TemplateTool;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.interfaces.IHttpPath;
import com.highfly029.core.interfaces.IServerGlobalProtocolHandler;
import com.highfly029.core.net.NetUtils;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.protocol.BasePtCode;
import com.highfly029.core.session.Session;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.RedisClientTool;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.logic.global.LogicConst;
import com.highfly029.logic.global.LogicMysqlGlobalData;
import com.highfly029.logic.http.LogicHttpPathEnum;
import com.highfly029.logic.manager.ActivityGlobalManager;
import com.highfly029.logic.manager.PlayerGlobalManager;
import com.highfly029.logic.module.mail.MailData;
import com.highfly029.logic.module.mail.MailTool;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.tool.ConfigTool;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.logic.tool.PlayerSessionTool;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.SysUtils;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjObjMap;

import io.netty.channel.Channel;
import io.vertx.mysqlclient.MySQLClient;
import io.vertx.mysqlclient.MySQLPool;
import io.vertx.redis.client.Command;
import io.vertx.redis.client.Request;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName LogicGlobalWorld
 * @Description 逻辑全局世界
 * @Author liyunpeng
 **/
public class LogicGlobalWorld extends GlobalWorld {
    private PlayerGlobalManager playerGlobalManager;
    private ActivityGlobalManager activityGlobalManager;

    /**
     * global数据库连接池
     */
    private MySQLPool connectionPool;

    public LogicGlobalWorld(int childWorldNum) {
        super(true, childWorldNum, 200);
    }

    public MySQLPool getConnectionPool() {
        return connectionPool;
    }

    /**
     * 初始化全局数据
     */
    private void initMysqlGlobalData() {
        MySQLUtils.sendGlobal(connectionPool, "SELECT * FROM global;", null, (isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                for (Row row : successRows) {
                    LogicMysqlGlobalData.initTime = row.getLong("initTime");
                    LogicMysqlGlobalData.openServerTime = row.getLong("openServerTime");
                }
                if (LogicMysqlGlobalData.initTime == 0) {
                    long now = SystemTimeTool.getMillTime();
                    long openServerTime = now + 1000;
                    LogicMysqlGlobalData.initTime = now;
                    LogicMysqlGlobalData.openServerTime = openServerTime;
                    Tuple tuple = Tuple.of(now, openServerTime);
                    MySQLUtils.sendGlobal(connectionPool, "INSERT INTO global VALUES(?,?);", tuple, ((isSuccess1, successRows1, errorMsg1) -> {
                        if (isSuccess1) {
                            LogicLoggerTool.systemLogger.info("initMysqlGlobalData initTime={}, openServerTime={}", LogicMysqlGlobalData.initTime, LogicMysqlGlobalData.openServerTime);
                        } else {
                            LogicLoggerTool.gameLogger.error("initMysqlGlobalData insert fail {}", errorMsg1);
                        }
                    }));
                }
            } else {
                LogicLoggerTool.gameLogger.error("initMysqlGlobalData select fail {}", errorMsg);
            }
        });
    }

    @Override
    protected void onStartUpSuccess() {
        super.onStartUpSuccess();

        getMysqlTool().getPoolMap().foreachImmutable((dbName, connectionPool) -> {
            postEvent2AllChild(Global2LogicEvent.INIT_DB_CONNECTION, dbName, connectionPool);
        });

        connectionPool = mysqlTool.getPoolMap().get(LogicConst.DB_NAME + "_" + ConfigConst.identifier);

        initMysqlGlobalData();

        pushServerInfo2LoadBalance();

        activityGlobalManager.init();
    }

    @Override
    protected void loadTemplateConfig(boolean isHotFix) {
        super.loadTemplateConfig(isHotFix);
        boolean useBinDataFile = ConfigPropUtils.getBoolValue("useBinDataFile");
        TemplateTool.load("csv", useBinDataFile);
        String regionConfigName = ConfigPropUtils.getValue("region");
        String regionConfigPath = ConfigPropUtils.getConfigPath() + "/csv_" + regionConfigName;
        File regionConfigFile = new File(regionConfigPath);
        if (regionConfigFile.exists()) {
            TemplateTool.load("csv_" + regionConfigName, useBinDataFile);
        }
        String currentServerConfig = regionConfigPath + "_" + ConfigConst.identifier;
        File currentServerFile = new File(currentServerConfig);
        if (currentServerFile.exists()) {
            TemplateTool.load("csv_" + regionConfigName + "_" + ConfigConst.identifier, useBinDataFile);
        }
        //加载excel数据
        LoggerTool.systemLogger.info("logic dataTemplates.size={}", TemplateTool.dataTemplates.size());
    }

    @Override
    protected void clearTemplateConfig() {
        super.clearTemplateConfig();
        //清理excel数据
        TemplateTool.clear();
    }

    @Override
    protected void onHandleConfigData(ObjObjMap<Class, BaseGlobalConfig> configMap) {
        ConfigTool.setConfigData(configMap);
    }

    @Override
    protected void onHandleConstConfigData() {
        ConfigTool.setConstConfigData(TemplateTool.constDataMap);
    }

    @Override
    protected void onHandlerConfigOver() {
        ConfigTool.updateHotfixFlag();
    }

    @Override
    protected void onBeforeLoadAll() {
        super.onBeforeLoadAll();
        sessionTool = new PlayerSessionTool(this);
    }

    @Override
    protected void loadAllUpdate() {
        playerGlobalManager = new PlayerGlobalManager(this);
        updateMaps.put(PlayerGlobalManager.class, playerGlobalManager);

        activityGlobalManager = new ActivityGlobalManager(this);
        updateMaps.put(ActivityGlobalManager.class, activityGlobalManager);

    }

    public ActivityGlobalManager getActivityGlobalManager() {
        return activityGlobalManager;
    }

    public PlayerGlobalManager getPlayerGlobalManager() {
        return playerGlobalManager;
    }

    @Override
    protected void registerAllPacketParser() throws Exception {
        registerPacketParser("com.highfly029.parser.pb");
        registerPacketParser("com.highfly029.parser.custom");
    }


    @Override
    protected IHttpPath getHttpPath(String path) {
        LogicHttpPathEnum pathEnum = null;
        try {
            pathEnum = LogicHttpPathEnum.valueOf(path);
        } catch (Exception exception) {
            LoggerTool.httpLogger.error("not exist path", exception);
        }
        return pathEnum;
    }

    @Override
    public void onTcpClientEvent(LogicEvent event) {
        byte action = event.getLogicEventAction();
        if (action == LogicEventAction.CHANNEL_READ) {
            Channel channel = (Channel) event.getData2();
            PbPacket.TcpPacket packet = (PbPacket.TcpPacket) event.getData1();
            tcpClientRead(channel, packet);
        } else {
            super.onTcpClientEvent(event);
            switch (action) {
                case LogicEventAction.CHANNEL_READER_IDLE, LogicEventAction.CHANNEL_WRITER_IDLE, LogicEventAction.CHANNEL_ALL_IDLE -> {
                    Channel channel = (Channel) event.getData1();
                    channel.writeAndFlush(serverHeartReq);
                }
                case LogicEventAction.CHANNEL_ACTIVE -> {
                    TcpClient tcpClient = (TcpClient) event.getData1();
                    Channel channel = (Channel) event.getData2();
                    PbCommon.ConnectTypeReq.Builder req = PbCommon.ConnectTypeReq.newBuilder();
                    req.setType(SessionType.Logic);
                    req.setIdentifier(ConfigConst.identifier);
                    req.setIsReConnect(tcpClient.isReconnect());
                    PbPacket.TcpPacket tcpPacket = createTcpPacket(BasePtCode.CONNECT_TYPE_REQ, req.build().toByteString());
                    channel.writeAndFlush(tcpPacket);
                    LoggerTool.systemLogger.info("logic 发送初始协议 {}", tcpClient);
                    tcpClient.setReconnect(true);
                }
                case LogicEventAction.CHANNEL_INACTIVE -> {
                    TcpClient tcpClient = (TcpClient) event.getData1();
                    Channel channel = (Channel) event.getData2();
                    Session session = sessionTool.getSession(channel);
                    if (session != null) {
                        //remove 会释放session 所以需要提前把type和list取出来投递给子世界
                        int type = session.getType();
                        int identifier = session.getIdentifier();
                        long index = MathUtils.getCompositeIndex(type, identifier);
                        IntList features = session.getFeatures();
                        sessionTool.removeSession(session);
                        postEvent2AllChild(Global2LogicEvent.REMOVE_SERVER_SESSION, index, features);
                        LogicLoggerTool.gameLogger.info("channel inactive disconnect:{}", tcpClient);
                    } else {
                        LogicLoggerTool.gameLogger.error("channel inactive disconnect session is null {}", tcpClient);
                    }
                }
            }
        }
    }

    /**
     * tcp读
     *
     * @param channel
     * @param packet
     */
    private void tcpClientRead(Channel channel, PbPacket.TcpPacket packet) {
        int ptCode = packet.getPtCode();
        int type = 0;
        int identifier = 0;
        try {
            if (ptCode == BasePtCode.HEART_RESP) {
//                LoggerTool.systemLogger.info("收到心跳包 channel={}", channel.id().asShortText());
            } else if (ptCode != BasePtCode.CONNECT_TYPE_RESP) {
                Session session = sessionTool.getSession(channel);
                type = session.getType();
                identifier = session.getIdentifier();
                IServerGlobalProtocolHandler handler = serverGlobalHandlerMaps.get(ptCode);
                handler.onServerProtocolHandler(session, ptCode, packet);
            } else {
                PbCommon.ConnectTypeResp resp = PbCommon.ConnectTypeResp.parseFrom(packet.getData());
                type = resp.getType();
                identifier = resp.getIdentifier();
                LoggerTool.systemLogger.info("logic connect to session type={}, id={}, channel={} success", type, identifier, channel.id().asShortText());

                Session session = sessionTool.getSession(type, identifier);
                if (session != null) {
                    //重复的连接 直接断开
                    LoggerTool.systemLogger.error("logic connect duplicate type={}, id={}, channel={}", type, identifier, channel.id().asShortText());
                    channel.close();
                    return;
                }
                if (type != SessionType.Center && type != SessionType.LoadBalance) {
                    LoggerTool.systemLogger.error("logic connect invalid type={}", type);
                    return;
                }

                session = sessionTool.create();
                session.init(type, identifier, channel);
                for (int feature : resp.getFeaturesList()) {
                    session.addFeature(feature);
                }
                sessionTool.addSession(session);
                postEvent2AllChild(Global2LogicEvent.ADD_SERVER_SESSION, session, null);
                if (type == SessionType.Center) {
                    onCenterConnectSuccess(session);
                }
            }
        } catch (Exception e) {
            LogicLoggerTool.gameLogger.error("tcpClientRead type=" + type + " id=" + identifier + " ptCode=" + ptCode + " channelShort=" + channel.id().asShortText(), e);
        }
    }

    /**
     * 与center连接成功的通知
     *
     * @param session
     */
    private void onCenterConnectSuccess(Session session) {
        if (session.getFeatures().contains(CenterFeature.ACTIVITY)) {
            activityGlobalManager.getCenterActivity(session);
        }
    }

    @Override
    public void onTcpServerEvent(LogicEvent event) {
        byte action = event.getLogicEventAction();
        if (action == LogicEventAction.CHANNEL_READ) {
            Channel channel = (Channel) event.getData2();
            PbPacket.TcpPacketClient packet = (PbPacket.TcpPacketClient) event.getData1();
            tcpServerRead(channel, packet);
        } else {
            switch (action) {
                case LogicEventAction.CHANNEL_ALL_IDLE -> {
                    //服务器检测超时之后应该立刻断开连接、而不是延迟断开连接等待重连
                    Channel channel = (Channel) event.getData1();
                    String idleState = (String) event.getData2();
                    ((PlayerSessionTool) sessionTool).sessionOfflineTimeout(channel, idleState);
                }
                case LogicEventAction.CHANNEL_INACTIVE -> {
                    Channel channel = (Channel) event.getData1();
                    ((PlayerSessionTool) sessionTool).sessionInactive(channel);
                }
                default -> super.onTcpServerEvent(event);
            }
        }
    }

    /**
     * tcp读
     *
     * @param channel
     * @param packet
     */
    private void tcpServerRead(Channel channel, PbPacket.TcpPacketClient packet) {
        int ptCode = packet.getPtCode();
        try {
            PlayerSession session = (PlayerSession) sessionTool.getSession(channel);
            if (ptCode == BasePtCode.HEART_REQ) {
                //heart 如果需要心跳返回附带时间戳等信息，就使用这个
                if (session != null) {
                    session.send(BasePtCode.HEART_RESP);
                } else {
                    channel.close();
                    LogicLoggerTool.gameLogger.error("session is null, protocol={}, ip={}", ptCode, NetUtils.getIP(channel));
                }
            } else if (ptCode != BasePtCode.CONNECT_TYPE_REQ) {
                if (session != null && session.getChannel() != null) {
                    WorldTool.addChildWorldLogicEvent(session.getWorldIndex(), LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_CLIENT_PACKET, ptCode, session, packet);
                } else {
                    channel.close();
                    LogicLoggerTool.gameLogger.error("session is null, protocol={}, ip={}", ptCode, NetUtils.getIP(channel));
                }
            } else {
                if (session != null && session.getChannel() != null) {
                    channel.close();
                    LogicLoggerTool.gameLogger.error("duplicate connect type req and close channel, protocol={}, ip={}", ptCode, NetUtils.getIP(channel));
                    return;
                }
                PlayerSessionTool playerSessionTool = (PlayerSessionTool) sessionTool;
                session = playerSessionTool.createPlayerSession();
                //先生成socketID再使用
                playerSessionTool.generateSocketID(session);
                session.init(SessionType.Client, session.getSocketID(), channel);
                playerSessionTool.handleFirstLogicConnect(session, packet);
            }
        } catch (Exception e) {
            LogicLoggerTool.gameLogger.error("onTcpServerEvent ptCode=" + ptCode + " channelShort=" + channel.id().asShortText(), e);
        }
    }

    /**
     * 向逻辑子世界投递事件
     *
     * @param worldIndex
     * @param event
     * @param obj1
     * @param obj2
     */
    public void postEvent2Child(int worldIndex, Global2LogicEvent event, Object obj1, Object obj2) {
        WorldTool.addChildWorldLogicEvent(worldIndex, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_BASE_EVENT, event, obj1, obj2);
    }

    /**
     * 向所有逻辑子世界投递事件
     *
     * @param event
     * @param obj1
     * @param obj2
     */
    public void postEvent2AllChild(Global2LogicEvent event, Object obj1, Object obj2) {
        for (int i = 0; i < LogicConst.LOGIC_CHILD_WORLD_NUM; i++) {
            int worldIndex = i + 1;
            WorldTool.addChildWorldLogicEvent(worldIndex, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_BASE_EVENT, event, obj1, obj2);
        }
    }

    @Override
    protected void onWorldInnerEvent(LogicEvent logicEvent) {
        super.onWorldInnerEvent(logicEvent);
        byte action = logicEvent.getLogicEventAction();
        switch (action) {
            case LogicEventAction.C2G_BASE_EVENT -> {
                Logic2GlobalEvent event = (Logic2GlobalEvent) logicEvent.getData1();
                onLogic2GlobalEvent(event, logicEvent.getData2(), logicEvent.getData3());
            }
            case LogicEventAction.OFFLINE_MSG -> {
                pushOfflineMsg2Player((long) logicEvent.getData1(), (int) logicEvent.getData2(), (GeneratedMessageV3) logicEvent.getData3());
            }
        }

    }

    private void onLogic2GlobalEvent(Logic2GlobalEvent event, Object data1, Object data2) {
        switch (event) {
            case ADD_PLAYER -> {
                int index = (int) data1;
                long playerID = (long) data2;
                playerGlobalManager.addPlayer(index, playerID);
            }
            case REMOVE_PLAYER -> {
                int index = (int) data1;
                long playerID = (long) data2;
                playerGlobalManager.removePlayer(index, playerID);
            }
            case GM_COMMAND -> {
                String[] stringCmd = (String[]) data1;
                int[] intCmd = (int[]) data2;
                doGlobalGM(stringCmd, intCmd);
            }
        }
    }

    /**
     * 执行全局gm命令
     *
     * @param stringCmd
     * @param intCmd
     */
    private void doGlobalGM(String[] stringCmd, int[] intCmd) {
        switch (stringCmd[0]) {
            case "aaa" -> {
            }
        }
    }

    @Override
    protected void onSecond() {
        super.onSecond();
        //打印协议统计数据
//        StatisticsEnum.MSG_LENGTH.getPlugin().dump();
    }

    @Override
    public void onDaily() {
        super.onDaily();
    }

    /**
     * 推送服务器消息到负载均衡服
     */
    private void pushServerInfo2LoadBalance() {
        PbLoadBalance.S2LPushServerInfo.Builder builder = PbLoadBalance.S2LPushServerInfo.newBuilder();
        String clientHost = ConfigPropUtils.getValue("clientHost");
        int clientPort = ConfigPropUtils.getIntValue("clientPort");

        builder.setType(SessionType.Logic);
        builder.setIdentifier(ConfigConst.identifier);
        builder.setMaxMemory(SysUtils.getMaxMemory());
        builder.setTotalMemory(SysUtils.getTotalMemory());
        builder.setFreeMemory(SysUtils.getFreeMemory());
        builder.setClientHost(clientHost);
        builder.setClientPort(clientPort);
        builder.setPlayerNum(playerGlobalManager.getAllPlayers().size());
        sendMsg2AllFeatureServer(SessionType.LoadBalance, LoadBalanceFeature.SCENE, PtCode.S2LPushServerInfo, builder.build());
        LogicLoggerTool.gameLogger.info("pushLogicServerInfo2LoadBalance id={}", ConfigConst.identifier);
    }

    /**
     * 向另一个玩家推送离线消息
     *
     * @param playerID
     * @param ptCode
     * @param messageV3
     */
    public void pushOfflineMsg2Player(long playerID, int ptCode, GeneratedMessageV3 messageV3) {
        int worldIndex = playerGlobalManager.getPlayerWorldIndex(playerID);
        if (worldIndex > 0) {
            WorldTool.addChildWorldLogicEvent(worldIndex, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.OFFLINE_MSG, playerID, ptCode, messageV3);
        } else {
            //存储到redis然后通知到玩家
            Request request = Request.cmd(Command.RPUSH).arg(RedisKey.PLAYER_OFFLINE_MSG.value() + playerID)
                    .arg(ptCode).arg(messageV3.toByteArray());
            RedisClientTool.send(0, request, (isSuccess, response, errorMsg) -> {
                if (isSuccess) {
                    LogicLoggerTool.gameLogger.info("pushOfflineMsg2Player playerID={},ptCode={},success={}", playerID, ptCode, response);
//                    pushServerInfo2LoadBalance();
                } else {
                    LogicLoggerTool.gameLogger.error("pushOfflineMsg2Player fail playerID={},ptCode={}, msg={}", playerID, ptCode, errorMsg);
                }
            });
            IntObjectMap<Session> map = sessionTool.getSessionMap(SessionType.LoadBalance);
            Session session = RandomUtils.random(map);
            if (session != null) {
                PbLoadBalance.L2LPushOfflineMsg.Builder builder = PbLoadBalance.L2LPushOfflineMsg.newBuilder();
                builder.setPlayerID(playerID);
                session.sendMsgS2S(PtCode.L2LPushOfflineMsg, builder.build());
            } else {
                LogicLoggerTool.gameLogger.error("pushOfflineMsg2Player fail session is null, playerID={},ptCode={}", playerID, ptCode);
            }
        }
    }

    /**
     * 向当前逻辑服另一个玩家推送离线消息
     *
     * @param playerID
     * @param ptCode
     * @param messageV3
     */
    public void pushOfflineMsg2PlayerInCurrentServer(long playerID, int ptCode, GeneratedMessageV3 messageV3) {
        int worldIndex = playerGlobalManager.getPlayerWorldIndex(playerID);
        if (worldIndex > 0) {
            WorldTool.addChildWorldLogicEvent(worldIndex, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.OFFLINE_MSG, playerID, ptCode, messageV3);
        } else {
            //存储到redis等玩家上线后再执行
            Request request = Request.cmd(Command.RPUSH).arg(RedisKey.PLAYER_OFFLINE_MSG.value() + playerID)
                    .arg(ptCode).arg(messageV3.toByteArray());
            RedisClientTool.send(0, request, (isSuccess, response, errorMsg) -> {
                if (isSuccess) {
                    LogicLoggerTool.gameLogger.info("pushOfflineMsg2PlayerInCurrentServer playerID={},ptCode={},success={}", playerID, ptCode, response);
                } else {
                    LogicLoggerTool.gameLogger.error("pushOfflineMsg2PlayerInCurrentServer fail playerID={},ptCode={}, msg={}", playerID, ptCode, errorMsg);
                }
            });
        }
    }

    /**
     * 新增邮件,如果玩家在线则通知玩家,否则直接入库
     *
     * @param playerID
     * @param mailData
     */
    public void addMail(long playerID, MailData mailData) {
        String insertSQLStr = "INSERT INTO mail values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Tuple tuple = MailTool.createMailTuple(playerID, mailData);
        MySQLUtils.sendGlobal(connectionPool, insertSQLStr, tuple, (isSuccess, successRows, errorMsg) -> {
            if (isSuccess) {
                int worldIndex = playerGlobalManager.getPlayerWorldIndex(playerID);
                if (worldIndex > 0) {
                    long mailID = successRows.property(MySQLClient.LAST_INSERTED_ID);
                    mailData.setMailID(mailID);
                    postEvent2Child(worldIndex, Global2LogicEvent.ADD_MAIL, playerID, mailData);
                }
            } else {
                LogicLoggerTool.gameLogger.error("addMail insert fail {}, {}", errorMsg, mailData);
            }
        });
    }
}
