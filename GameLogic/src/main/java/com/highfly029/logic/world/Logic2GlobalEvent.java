package com.highfly029.logic.world;

/**
 * @ClassName Logic2GlobalEvent
 * @Description Logic2GlobalEvent 自定义事件
 * @Author liyunpeng
 **/
public enum Logic2GlobalEvent {
    /**
     * 增加玩家
     */
    ADD_PLAYER,
    /**
     * 删除玩家
     */
    REMOVE_PLAYER,

    /**
     * gm命令
     */
    GM_COMMAND,

    /**
     * 推送离线消息
     */
    PUSH_OFFLINE_MSG,
    ;
}
