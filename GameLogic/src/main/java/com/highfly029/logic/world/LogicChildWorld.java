package com.highfly029.logic.world;

import java.util.ArrayList;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.common.data.feature.CenterFeature;
import com.highfly029.common.data.feature.LoadBalanceFeature;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.common.protocol.packet.PbLoadBalance;
import com.highfly029.common.protocol.packet.PbUnion;
import com.highfly029.common.redis.RedisKey;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.net.NetUtils;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.RedisClientTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.ChildWorld;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.logic.global.LogicConst;
import com.highfly029.logic.manager.ActivityChildManager;
import com.highfly029.logic.manager.PlayerManager;
import com.highfly029.logic.module.IClientProtocolHandler;
import com.highfly029.logic.module.IClientProtocolRegister;
import com.highfly029.logic.module.IOfflineMessageHandler;
import com.highfly029.logic.module.mail.MailData;
import com.highfly029.logic.module.Player;
import com.highfly029.logic.serverProtocol.broker.ILogicBrokerHandler;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.tool.LogicLoggerTool;
import com.highfly029.utils.MathUtils;
import com.highfly029.utils.RandomUtils;
import com.highfly029.utils.ReflectionUtils;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;
import com.highfly029.utils.collection.LongObjectMap;
import com.highfly029.utils.collection.ObjObjMap;
import com.highfly029.utils.collection.ObjectList;

import io.vertx.mysqlclient.MySQLPool;
import io.vertx.redis.client.Command;
import io.vertx.redis.client.Request;


/**
 * @ClassName LogicChildWorld
 * @Description 逻辑子世界
 * @Author liyunpeng
 **/

public class LogicChildWorld extends ChildWorld {

    public ArrayList<Object> testMap = new ArrayList<>();

    private PlayerManager playerManager;

    private ActivityChildManager activityChildManager;

    /**
     * 服务器之间的连接
     * SessionType -> identifier -> session
     */
    private LongObjectMap<Session> serverSessionMap = new LongObjectMap<>(Session[]::new);

    /**
     * 如果只有一个联盟连接
     */
    private static final boolean singleUnion = true;
    private Session unionSession;
    /**
     * 如果只有一个队伍连接
     */
    private static final boolean singleTeam = true;
    private Session teamSession;

    /**
     * 拍卖行连接
     */
    private Session auctionSession;

    /**
     * 如果只有一个负载均衡分配逻辑服务器
     */
    private static final boolean singleLoadBalanceLogic = true;
    private Session loadBalanceLogicSession;

    /**
     * 全部的数据库连接
     */
    private ObjObjMap<String, MySQLPool> connectionPoolMap = new ObjObjMap<>(String[]::new, MySQLPool[]::new);

    /**
     * 客户端消息处理器集合
     */
    private final IntObjectMap<IClientProtocolHandler> clientHandlers = new IntObjectMap<>(IClientProtocolHandler[]::new);

    /**
     * 离线消息集合
     */
    private final IntObjectMap<IOfflineMessageHandler> offlineMessageParsers = new IntObjectMap<>(IOfflineMessageHandler[]::new);

    /**
     * 服务器之间中转消息处理
     */
    private final IntObjectMap<ILogicBrokerHandler> logicBrokerHandlers = new IntObjectMap<>(ILogicBrokerHandler[]::new);

    public LogicChildWorld(int index) {
        super(index, 100);
    }

    /**
     * 注册全部的客户端协议
     */
    private void registerAllClientProtocol() throws Exception {
        String packageName = this.getClass().getPackageName();
        packageName = packageName.substring(0, packageName.lastIndexOf('.'));
        ObjectList<Class> clsModules = ReflectionUtils.getAllClassByInterface(IClientProtocolRegister.class, packageName);
        Class[] values = clsModules.getValues();
        Class cls;
        for (int i = 0, size = clsModules.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                IClientProtocolRegister register = (IClientProtocolRegister) cls.getDeclaredConstructor().newInstance();
                register.registerClientProtocol(this);
            }
        }
    }

    /**
     * 注册离线消息解析器
     */
    private void registerOfflineMessageParser() throws Exception {
        String packageName = this.getClass().getPackageName();
        packageName = packageName.substring(0, packageName.lastIndexOf('.'));
        ObjectList<Class> clsModules = ReflectionUtils.getAllClassByInterface(IOfflineMessageHandler.class, packageName);
        Class[] values = clsModules.getValues();
        Class cls;
        for (int i = 0, size = clsModules.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                IOfflineMessageHandler parser = (IOfflineMessageHandler) cls.getDeclaredConstructor().newInstance();
                int ptCode = parser.registerPtCode();
                offlineMessageParsers.put(ptCode, parser);
            }
        }
    }

    /**
     * 服务器之间中转消息处理
     *
     * @throws Exception
     */
    private void registerLogicBrokerHandler() throws Exception {
        String packageName = this.getClass().getPackageName();
        packageName = packageName.substring(0, packageName.lastIndexOf('.'));
        ObjectList<Class> clsModules = ReflectionUtils.getAllClassByInterface(ILogicBrokerHandler.class, packageName);
        Class[] values = clsModules.getValues();
        Class cls;
        for (int i = 0, size = clsModules.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                ILogicBrokerHandler logicBrokerHandler = (ILogicBrokerHandler) cls.getDeclaredConstructor().newInstance();
                int ptCode = logicBrokerHandler.registerPtCode();
                logicBrokerHandlers.put(ptCode, logicBrokerHandler);
            }
        }
    }

    /**
     * 获取离线消息解析器
     *
     * @param ptCode
     * @return
     */
    public IOfflineMessageHandler getOfflineMessageParser(int ptCode) {
        return offlineMessageParsers.get(ptCode);
    }

    @Override
    protected void onAfterLoadAll() throws Exception {
        super.onAfterLoadAll();
        registerAllClientProtocol();
        registerOfflineMessageParser();
        registerLogicBrokerHandler();
    }

    /**
     * 注册客户端协议
     *
     * @param ptCode
     * @param handler
     * @throws Exception
     */
    public void registerClientProtocol(int ptCode, IClientProtocolHandler handler) throws Exception {
        if (clientHandlers.containsKey(ptCode)) {
            throw new Exception("logic registerProtocol duplicate ptCode:" + ptCode);
        } else {
            clientHandlers.put(ptCode, handler);
        }
    }

    @Override
    protected void loadAllUpdate() {
        playerManager = new PlayerManager(this);
        updateMaps.put(PlayerManager.class, playerManager);

        activityChildManager = new ActivityChildManager(this);
        updateMaps.put(ActivityChildManager.class, activityChildManager);
    }

    public ActivityChildManager getActivityChildManager() {
        return activityChildManager;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    public IntObjectMap<ILogicBrokerHandler> getLogicBrokerHandlers() {
        return logicBrokerHandlers;
    }

    @Override
    protected void onWorldInnerEvent(LogicEvent logicEvent) {
        switch (logicEvent.getLogicEventAction()) {
            case LogicEventAction.G2C_CLIENT_PACKET -> {
                int ptCode = (int) logicEvent.getData1();
                PlayerSession session = (PlayerSession) logicEvent.getData2();
                PbPacket.TcpPacketClient packet = (PbPacket.TcpPacketClient) logicEvent.getData3();
                //check sequence
                long packetSequenceNum = packet.getSequenceNum();
                if (session.getSequenceNum() >= packetSequenceNum) {
                    session.close();
                    LogicLoggerTool.gameLogger.error("ip={}, invalid sequenceNum={} localNum={} and close channel", NetUtils.getIP(session.getChannel()), packetSequenceNum, session.getSequenceNum());
                    return;
                }
                session.setSequenceNum(packetSequenceNum);

                long playerID = session.getPlayer() != null ? session.getPlayer().getPlayerID() : 0;
                IClientProtocolHandler handler = clientHandlers.get(ptCode);
                try {
                    handler.onClientProtocolHandler(session, packet);
                } catch (Exception e) {
                    LogicLoggerTool.gameLogger.error("logicChildWorld Index={} handlerClientError: playerID={}, ptCode={}", getIndex(), playerID, ptCode);
                    LogicLoggerTool.gameLogger.error("onChildWorldEvent", e);
                }
            }
            case LogicEventAction.G2C_BASE_EVENT -> {
                Global2LogicEvent event = (Global2LogicEvent) logicEvent.getData1();
                onGlobal2LogicEvent(event, logicEvent.getData2(), logicEvent.getData3());
            }
            case LogicEventAction.OFFLINE_MSG -> {
                onPushOfflineMsg2PlayerByGlobalWorld((long) logicEvent.getData1(), (int) logicEvent.getData2(), (GeneratedMessageV3) logicEvent.getData3());
            }
            default -> super.onWorldInnerEvent(logicEvent);
        }

    }

    protected void onGlobal2LogicEvent(Global2LogicEvent event, Object data1, Object data2) {
        switch (event) {
            case INIT_DB_CONNECTION -> {
                String dbName = (String) data1;
                MySQLPool connectionPool = (MySQLPool) data2;
                connectionPoolMap.put(dbName, connectionPool);
            }
            case ADD_SERVER_SESSION -> {
                Session session = (Session) data1;
                addServerSession(session);
            }
            case REMOVE_SERVER_SESSION -> {
                long index = (long) data1;
                IntList features = (IntList) data2;
                int type = MathUtils.getCompositeArg1(index);
                int identifier = MathUtils.getCompositeArg2(index);
                removeServerSession(type, identifier, features);
            }
            case PLAYER_OFFLINE -> {
                PlayerSession playerSession = (PlayerSession) data1;
                if (playerSession.getPlayer() == null) {
                    LogicLoggerTool.gameLogger.error("PLAYER_OFFLINE {},{}", playerSession.getSocketID(), playerSession.getChannel().id().asShortText());
                    return;
                }
                playerSession.getPlayer().offline();
            }
            case PLAYER_RECONNECT -> {
                PlayerSession playerSession = (PlayerSession) data1;
                PbCommon.ConnectTypeReq req = (PbCommon.ConnectTypeReq) data2;
                reconnect(playerSession, req);
            }
            case UPDATE_UNION_DATA -> {
                long playerID = (long) data1;
                PbUnion.PbUpdateUnionData pbUpdateUnionData = (PbUnion.PbUpdateUnionData) data2;
                Player player = playerManager.getPlayer(playerID);
                if (player != null) {
                    player.getUnionModule().updateUnionDataFromCenter(pbUpdateUnionData);
                }
            }
            case REMOTE_OFFLINE_MSG -> {
                long playerID = (long) data1;
                Player player = playerManager.getPlayer(playerID);
                if (player != null) {
                    player.handlerRedisOfflineMsg(result -> LogicLoggerTool.gameLogger.info("handle REMOTE_OFFLINE_MSG playerID={} result={}", playerID, result));
                }
            }
            case ADD_MAIL -> {
                long playerID = (long) data1;
                MailData mailData = (MailData) data2;
                Player player = playerManager.getPlayer(playerID);
                if (player != null) {
                    player.getMailModule().onGlobalAddMail(mailData);
                }
            }
            case INIT_ACTIVITY -> {
                IntSet initSet = (IntSet) data1;
                activityChildManager.addInitActivity(initSet);
            }
            case OPEN_ACTIVITY -> {
                int id = (int) data1;
                activityChildManager.addOpenActivity(id);
            }
            case CLOSE_ACTIVITY -> {
                int id = (int) data1;
                boolean forceClose = (boolean) data2;
                activityChildManager.removeCloseActivity(id, forceClose);
            }
        }
    }

    /**
     * 重连
     *
     * @param playerSession
     * @param req
     */
    private void reconnect(PlayerSession playerSession, PbCommon.ConnectTypeReq req) {
        long playerID = req.getPlayerID();
        Player player = playerManager.getPlayer(playerID);
        if (player == null) {
            LogicLoggerTool.gameLogger.error("reconnect fail playerID is null, playerID={}, IP={}", playerID, NetUtils.getIP(playerSession.getChannel()));
            playerSession.close();
            return;
        }

        //TODO 老session令牌 不一样则失败


        long clientReceivedIndex = req.getClientReceivedIndex();
        playerSession.setClientReceivedIndex(clientReceivedIndex);

        player.reconnect(playerSession);
    }

    /**
     * 增加服务器会话
     *
     * @param session
     */
    private void addServerSession(Session session) {
        long index = MathUtils.getCompositeIndex(session.getType(), session.getIdentifier());
        serverSessionMap.put(index, session);

        if (session.getType() == SessionType.Center) {
            //TODO 如果是center断线重连或者center重启后 union数据有可能过期 需要重新pull一下
            //TODO 但是防止同时太多人一起pull数据 需要分散压力或者根据切场景触发
            if (singleUnion && session.getFeatures().contains(CenterFeature.UNION)) {
                unionSession = session;
            }
            if (singleTeam && session.getFeatures().contains(CenterFeature.TEAM)) {
                teamSession = session;
            }
            if (session.getFeatures().contains(CenterFeature.AUCTION)) {
                auctionSession = session;
            }
        } else if (session.getType() == SessionType.LoadBalance) {
            if (singleLoadBalanceLogic && session.getFeatures().contains(LoadBalanceFeature.LOGIC)) {
                loadBalanceLogicSession = session;
            }
        }
    }

    /**
     * 删除服务器会话
     *
     * @param type
     * @param identifier
     * @param features
     */
    private void removeServerSession(int type, int identifier, IntList features) {
        long index = MathUtils.getCompositeIndex(type, identifier);
        serverSessionMap.remove(index);
        if (type == SessionType.Center) {
            if (singleUnion && features.contains(CenterFeature.UNION)) {
                unionSession = null;
            }
            if (singleTeam && features.contains(CenterFeature.TEAM)) {
                teamSession = null;
            }
            if (features.contains(CenterFeature.AUCTION)) {
                auctionSession = null;
            }
        } else if (type == SessionType.LoadBalance) {
            if (singleLoadBalanceLogic && features.contains(LoadBalanceFeature.LOGIC)) {
                loadBalanceLogicSession = null;
            }
        }
    }

    /**
     * 获取队伍连接
     *
     * @return
     */
    public Session getTeamSession() {
        if (singleTeam) {
            return teamSession;
        } else {
            return getRandomSession(SessionType.Center, CenterFeature.TEAM);
        }
    }

    /**
     * 获取联盟连接
     *
     * @return
     */
    public Session getUnionSession() {
        if (singleUnion) {
            return unionSession;
        } else {
            return getRandomSession(SessionType.Center, CenterFeature.UNION);
        }
    }

    /**
     * 获取拍卖行连接
     * @return
     */
    public Session getAuctionSession() {
        return auctionSession;
    }

    /**
     * 获取指定session和功能的所有连接
     *
     * @param sessionType
     * @param feature
     * @return
     */
    private ObjectList<Session> getAllSession(int sessionType, int feature) {
        ObjectList<Session> list = new ObjectList<>(Session[]::new);
        serverSessionMap.foreachImmutable((k, v) -> {
            if (v.getType() == sessionType && v.getFeatures().contains(feature)) {
                list.add(v);
            }
        });
        return list;
    }

    /**
     * 获取指定session和功能的随机连接
     *
     * @param sessionType
     * @param feature
     * @return
     */
    public Session getRandomSession(int sessionType, int feature) {
        ObjectList<Session> list = getAllSession(sessionType, feature);
        return RandomUtils.random(list);
    }

    /**
     * 发送消息到所有负载均衡服务器
     *
     * @param ptCode
     * @param messageV3
     */
    public void sendMsg2AllLoadBalanceLogic(int ptCode, GeneratedMessageV3 messageV3) {
        if (singleLoadBalanceLogic) {
            loadBalanceLogicSession.sendMsgS2S(ptCode, messageV3);
        } else {
            ObjectList<Session> list = getAllSession(SessionType.LoadBalance, LoadBalanceFeature.LOGIC);
            if (list == null || list.isEmpty()) {
                LogicLoggerTool.gameLogger.error("sendMsg2AllLoadBalanceLogic empty", new Exception());
                return;
            }
            for (int i = 0; i < list.size(); i++) {
                list.get(i).sendMsgS2S(ptCode, messageV3);
            }
        }
    }

    /**
     * 发送消息协议到场景服
     *
     * @param ptCode
     * @param messageV3
     */
    public void sendMsg2Scene(int to, int toIndex, long playerID, int ptCode, GeneratedMessageV3 messageV3) {
        PbLoadBalance.Logic2SceneMessage.Builder builder = PbLoadBalance.Logic2SceneMessage.newBuilder();
        builder.setFrom(ConfigConst.identifier);
        builder.setFromIndex(getIndex());
        builder.setTo(to);
        builder.setToIndex(toIndex);
        builder.setPlayerID(playerID);
        builder.setPtCode(ptCode);
        if (messageV3 != null) {
            builder.setData(messageV3.toByteString());
        }
        Session session = getRandomSession(SessionType.LoadBalance, LoadBalanceFeature.BROKER);
        if (session == null) {
            LogicLoggerTool.gameLogger.error("sendMsg2Scene session is null", new Exception());
            return;
        }
        session.sendMsgS2S(PtCode.Logic2SceneMessage, builder.build());
    }

    /**
     * 获取指定数据库连接池
     *
     * @param player
     * @return
     */
    public MySQLPool getConnectionPool(Player player) {
        return getConnectionPool(player.getSourceLogicID());
    }

    /**
     * 获取默认的数据库连接池
     *
     * @return
     */
    public MySQLPool getConnectionPool(int sourceLogicID) {
        MySQLPool mySQLPool = connectionPoolMap.get(LogicConst.DB_NAME + "_" + sourceLogicID);
        if (mySQLPool == null) {
            mySQLPool = connectionPoolMap.get(LogicConst.DB_NAME + "_" + ConfigConst.identifier);
        }
        return mySQLPool;
    }

    /**
     * 向场景global世界投递事件
     *
     * @param event
     * @param obj1
     * @param obj2
     */
    public void postEvent2Global(Logic2GlobalEvent event, Object obj1, Object obj2) {
        WorldTool.addLogicEvent(LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.C2G_BASE_EVENT, event, obj1, obj2);
    }

    @Override
    public void onDaily() {
        playerManager.onDaily();
    }

    /**
     * 向另一个玩家推送离线消息 如果在线则立即执行
     *
     * @param playerID
     * @param ptCode
     * @param messageV3
     */
    public void pushOfflineMsg2Player(long playerID, int ptCode, GeneratedMessageV3 messageV3) {
        Player player = playerManager.getPlayer(playerID);
        if (player != null) {
            player.onPlayerProcessOfflineMsg(ptCode, messageV3.toByteString());
        } else {
            WorldTool.addLogicEvent(LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.OFFLINE_MSG, playerID, ptCode, messageV3);
        }
    }

    /**
     * 收到主世界发送的离线消息
     *
     * @param playerID
     * @param ptCode
     * @param messageV3
     */
    public void onPushOfflineMsg2PlayerByGlobalWorld(long playerID, int ptCode, GeneratedMessageV3 messageV3) {
        Player player = playerManager.getPlayer(playerID);
        if (player != null) {
            player.onPlayerProcessOfflineMsg(ptCode, messageV3.toByteString());
        } else {
            //存储到redis 等玩家上线后处理
            Request request = Request.cmd(Command.RPUSH).arg(RedisKey.PLAYER_OFFLINE_MSG.value() + playerID)
                    .arg(ptCode).arg(messageV3.toByteArray());

            RedisClientTool.send(getIndex(), request, (isSuccess, response, errorMsg) -> {
                if (isSuccess) {
                    LogicLoggerTool.gameLogger.info("onPushOfflineMsg2PlayerByGlobalWorld playerID={},ptCode={},success={}", playerID, ptCode, response);
                } else {
                    LogicLoggerTool.gameLogger.error("onPushOfflineMsg2PlayerByGlobalWorld fail playerID={},ptCode={}, msg={}", playerID, ptCode, errorMsg);
                }
            });
        }
    }
}
