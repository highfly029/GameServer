package com.highfly029.logic.world;

import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.DisruptorBuilder;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.core.world.SleepingWaitExtendStrategy;
import com.highfly029.logic.global.LogicConst;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

/**
 * @ClassName LogicStarter
 * @Description 逻辑服启动器
 * @Author liyunpeng
 **/
public class LogicStarter {
    public static void main(String[] args) {
        int index = 0;
        LogicGlobalWorld logicGlobalWorld = new LogicGlobalWorld(LogicConst.LOGIC_CHILD_WORLD_NUM);
        WaitStrategy waitStrategy = new SleepingWaitExtendStrategy(logicGlobalWorld);
        Disruptor<LogicEvent> disruptor = DisruptorBuilder.builder()
                .setPoolName("LogicGlobal")
                .setProducerType(ProducerType.MULTI)
                .setWorldThread(logicGlobalWorld)
                .setWaitStrategy(waitStrategy)
                .build();

        WorldTool worldTool = WorldTool.getInstance();
        worldTool.setDisruptor(disruptor);
        worldTool.setGlobalWorld(logicGlobalWorld);

        for (int i = 1; i <= LogicConst.LOGIC_CHILD_WORLD_NUM; i++) {
            index++;
            worldTool.addChildWorld(index, createLogicDisruptor(index));
        }
        worldTool.start();
    }

    private static Disruptor<LogicEvent> createLogicDisruptor(int index) {
        LogicChildWorld logicChildWorld = new LogicChildWorld(index);
        WaitStrategy childWait = new SleepingWaitExtendStrategy(logicChildWorld);
        return DisruptorBuilder.builder()
                .setPoolName("Logic")
                .setProducerType(ProducerType.SINGLE)
                .setWorldThread(logicChildWorld)
                .setWaitStrategy(childWait)
                .setIndex(index)
                .build();
    }
}
