package com.highfly029.logic.world;

/**
 * @ClassName Global2Logic
 * @Description Global2Logic 自定义事件
 * @Author liyunpeng
 **/
public enum Global2LogicEvent {
    /**
     * 初始化数据库连接
     */
    INIT_DB_CONNECTION,
    /**
     * 增加服务器会话
     */
    ADD_SERVER_SESSION,
    /**
     * 删除服务器会话
     */
    REMOVE_SERVER_SESSION,
    /**
     * 玩家重连
     */
    PLAYER_RECONNECT,
    /**
     * 玩家离线
     */
    PLAYER_OFFLINE,
    /**
     * 更新玩家联盟数据
     */
    UPDATE_UNION_DATA,
    /**
     * 收到另一个logic服的离线消息
     */
    REMOTE_OFFLINE_MSG,
    /**
     * 新增邮件通知
     */
    ADD_MAIL,
    /**
     * 初始化活动
     */
    INIT_ACTIVITY,
    /**
     * 开启活动
     */
    OPEN_ACTIVITY,
    /**
     * 关闭活动
     */
    CLOSE_ACTIVITY,
    ;
}
