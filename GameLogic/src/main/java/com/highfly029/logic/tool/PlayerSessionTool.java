package com.highfly029.logic.tool;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.common.protocol.PtCode;
import com.highfly029.common.protocol.packet.PbCommon;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.net.NetUtils;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.plugins.counter.IntegerCounter;
import com.highfly029.core.protocol.BasePtCode;
import com.highfly029.core.session.SessionType;
import com.highfly029.core.tool.SessionTool;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.logic.session.PlayerSession;
import com.highfly029.logic.world.Global2LogicEvent;
import com.highfly029.logic.world.LogicGlobalWorld;

import io.netty.channel.Channel;

/**
 * @ClassName PlayerSessionTool
 * @Description PlayerSessionTool
 * @Author liyunpeng
 **/
public class PlayerSessionTool extends SessionTool {
    private LogicGlobalWorld logicGlobalWorld;
    /**
     * socket计数器
     */
    private final IntegerCounter socketCounter = new IntegerCounter();

    public PlayerSessionTool(GlobalWorld globalWorld) {
        super(globalWorld);
        logicGlobalWorld = (LogicGlobalWorld) globalWorld;
    }

    public PlayerSession createPlayerSession() {
        return new PlayerSession();
    }

    /**
     * 生成socketID
     *
     * @param session
     */
    public void generateSocketID(PlayerSession session) {
        session.setSocketID(socketCounter.getNextValue());
    }

    /**
     * session离线超时
     *
     * @param channel
     * @param idleState
     */
    public void sessionOfflineTimeout(Channel channel, String idleState) {
        PlayerSession session = (PlayerSession) getSession(channel);
        long playerID = session != null && session.getPlayer() != null ? session.getPlayer().getPlayerID() : 0;
        LogicLoggerTool.gameLogger.info("idle sessionOfflineTimeout playerID={} ip={} idleState={}", playerID, NetUtils.getIP(channel), idleState);

    }

    /**
     * session断线
     *
     * @param channel
     */
    public void sessionInactive(Channel channel) {
        LogicLoggerTool.gameLogger.info("sessionInactive ip={}, channel={}", NetUtils.getIP(channel), channel.id().asShortText());
        PlayerSession session = (PlayerSession) getSession(channel);
        if (session == null) {
            return;
        }
        //立即删除
        removeSession(session);

        if (session.getType() == SessionType.Client) {
            //通知玩家离线消息
            logicGlobalWorld.postEvent2Child(session.getWorldIndex(), Global2LogicEvent.PLAYER_OFFLINE, session, null);
        }
    }

    /**
     * 处理第一个连接
     *
     * @param session
     * @param packet
     */
    public void handleFirstLogicConnect(PlayerSession session, PbPacket.TcpPacketClient packet) throws InvalidProtocolBufferException {
        PbCommon.ConnectTypeReq req = PbCommon.ConnectTypeReq.parseFrom(packet.getData());
        boolean isReconnect = req.getIsReConnect();
        int oldSocketID = req.getSocketID();
        session.setType(req.getType());
        LogicLoggerTool.gameLogger.info("handleFirstLogicConnect type={}, isReconnect={}, oldSocketID={}, clientReceivedIndex={},worldIndex={}",
                session.getType(), isReconnect, oldSocketID, req.getClientReceivedIndex(), req.getWorldIndex());


        PbCommon.ConnectTypeResp.Builder resp = PbCommon.ConnectTypeResp.newBuilder();
        resp.setType(SessionType.Logic);
        resp.setIsReConnect(isReconnect);
        resp.setSocketID(session.getSocketID());

        //测试时可以关闭校验
        req.getPtCodeMd5();
        if (!req.getPtCodeMd5().equals(PtCode.md5)) {
            session.send(BasePtCode.CONNECT_TYPE_RESP, resp.build());
            LogicLoggerTool.systemLogger.error("handleFirstLogicConnect invalid md5={}, player update client", req.getPtCodeMd5());
            return;
        }
        if (isReconnect) {
            int worldIndex = req.getWorldIndex();
            if (worldIndex <= 0) {
                session.send(BasePtCode.CONNECT_TYPE_RESP, resp.build());
                session.close();
                LogicLoggerTool.systemLogger.error("handleFirstLogicConnect invalid worldIndex={}", worldIndex);
                return;
            }
            //先添加，如果在子世界线程检测不合法close后会触发删除
            addSession(session);
            session.setWorldIndex(worldIndex);
            session.send(BasePtCode.CONNECT_TYPE_RESP, resp.build());

            logicGlobalWorld.postEvent2Child(worldIndex, Global2LogicEvent.PLAYER_RECONNECT, session, req);
        } else {
            //保证在正常登陆时 在其他服已经下线
            LogicGlobalWorld logicGlobalWorld = (LogicGlobalWorld) globalWorld;
            int worldIndex = logicGlobalWorld.getPlayerGlobalManager().getLeastPlayersWorldIndex();

            if (worldIndex <= 0) {
                LogicLoggerTool.gameLogger.error("handleFirstLogicConnect invalid worldIndex={}", worldIndex);
                session.close();
                return;
            }
            session.setWorldIndex(worldIndex);
            resp.setWorldIndex(worldIndex);
            resp.setIdentifier(ConfigConst.identifier);
            session.send(BasePtCode.CONNECT_TYPE_RESP, resp.build());
            addSession(session);
        }
    }
}
