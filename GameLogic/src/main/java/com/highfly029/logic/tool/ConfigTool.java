package com.highfly029.logic.tool;

import java.util.concurrent.atomic.AtomicInteger;

import com.highfly029.common.template.global.GlobalHotfixConst;
import com.highfly029.common.templateBase.BaseConst;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.core.tool.ITool;
import com.highfly029.logic.module.activity.ActivityConfig;
import com.highfly029.logic.module.asset.AssetConfig;
import com.highfly029.logic.module.bag.BagConfig;
import com.highfly029.logic.module.equipment.EquipConfig;
import com.highfly029.logic.module.function.FunctionConfig;
import com.highfly029.logic.module.gm.GMConfig;
import com.highfly029.logic.module.goal.GoalConfig;
import com.highfly029.logic.module.profession.ProfessionConfig;
import com.highfly029.logic.module.quest.QuestConfig;
import com.highfly029.logic.module.role.RoleConfig;
import com.highfly029.logic.module.scene.SceneConfig;
import com.highfly029.logic.module.union.UnionConfig;
import com.highfly029.utils.collection.ObjObjMap;

/**
 * @ClassName ConfigTool
 * @Description ConfigTool
 * @Author liyunpeng
 **/
public class ConfigTool implements ITool {
    /**
     * 热更新标记
     */
    private static AtomicInteger hotfixFlag = new AtomicInteger(-1);

    /**
     * 配置数据集合
     */
    private static ObjObjMap<Class, BaseGlobalConfig>[] configDataMaps = new ObjObjMap[2];

    /**
     * 常量配置数据集合
     */
    private static ObjObjMap<Class, BaseConst>[] constConfigDataMaps = new ObjObjMap[2];

    private static int readIndex() {
        return hotfixFlag.get() % 2;
    }

    private static int writeIndex() {
        return (hotfixFlag.get() + 1) % 2;
    }

    /**
     * 热更新标记
     */
    public static void updateHotfixFlag() {
        hotfixFlag.incrementAndGet();
    }

    /**
     * 设置数据
     *
     * @param map
     */
    public static void setConfigData(ObjObjMap<Class, BaseGlobalConfig> map) {
        configDataMaps[writeIndex()] = map;
    }

    /**
     * 设置常量配置数据
     *
     * @param map
     */
    public static void setConstConfigData(ObjObjMap<Class, BaseConst> map) {
        constConfigDataMaps[writeIndex()] = map;
    }

    /**
     * 获取对应类的常量配置数据
     *
     * @param cls
     * @return
     */
    public static BaseConst getConst(Class cls) {
        return constConfigDataMaps[readIndex()].get(cls);
    }

    /**
     * 获取对应类的配置数据
     *
     * @param cls
     * @return
     */
    public static BaseGlobalConfig getConfig(Class cls) {
        return configDataMaps[readIndex()].get(cls);
    }


    /***************************** 常量配置快捷方式 *****************************/
    public static GlobalHotfixConst getGlobalHotfixConst() {
        return (GlobalHotfixConst) constConfigDataMaps[readIndex()].get(GlobalHotfixConst.class);
    }


    /***************************** 战斗配置快捷方式 *****************************/

//    public static AttackConfig getAttackConfig() {
//        return (AttackConfig) configDataMaps[readIndex()].get(AttackConfig.class);
//    }
//
//    public static AttributeConfig getAttributeConfig() {
//        return (AttributeConfig) configDataMaps[readIndex()].get(AttributeConfig.class);
//    }
//
//    public static AvatarConfig getAvatarConfig() {
//        return (AvatarConfig) configDataMaps[readIndex()].get(AvatarConfig.class);
//    }
//
//    public static BuffConfig getBuffConfig() {
//        return (BuffConfig) configDataMaps[readIndex()].get(BuffConfig.class);
//    }
//
//    public static BulletConfig getBulletConfig() {
//        return (BulletConfig) configDataMaps[readIndex()].get(BulletConfig.class);
//    }
//
//    public static MonsterConfig getMonsterConfig() {
//        return (MonsterConfig) configDataMaps[readIndex()].get(MonsterConfig.class);
//    }
//
//    public static RegionConfig getRegionConfig() {
//        return (RegionConfig) configDataMaps[readIndex()].get(RegionConfig.class);
//    }
//
//    public static SkillConfig getSkillConfig() {
//        return (SkillConfig) configDataMaps[readIndex()].get(SkillConfig.class);
//    }
//
//    public static StateConfig getStateConfig() {
//        return (StateConfig) configDataMaps[readIndex()].get(StateConfig.class);
//    }


    /***************************** 业务配置快捷方式 *****************************/
    public static AssetConfig getAssetConfig() {
        return (AssetConfig) configDataMaps[readIndex()].get(AssetConfig.class);
    }

    public static BagConfig getBagConfig() {
        return (BagConfig) configDataMaps[readIndex()].get(BagConfig.class);
    }

    public static SceneConfig getSceneConfig() {
        return (SceneConfig) configDataMaps[readIndex()].get(SceneConfig.class);
    }

    public static EquipConfig getEquipConfig() {
        return (EquipConfig) configDataMaps[readIndex()].get(EquipConfig.class);
    }

    public static FunctionConfig getFunctionConfig() {
        return (FunctionConfig) configDataMaps[readIndex()].get(FunctionConfig.class);
    }

    public static GMConfig getGMConfig() {
        return (GMConfig) configDataMaps[readIndex()].get(GMConfig.class);
    }

    public static GoalConfig getGoalConfig() {
        return (GoalConfig) configDataMaps[readIndex()].get(GoalConfig.class);
    }

    public static ProfessionConfig getProfessionConfig() {
        return (ProfessionConfig) configDataMaps[readIndex()].get(ProfessionConfig.class);
    }

    public static QuestConfig getQuestConfig() {
        return (QuestConfig) configDataMaps[readIndex()].get(QuestConfig.class);
    }

    public static RoleConfig getRoleConfig() {
        return (RoleConfig) configDataMaps[readIndex()].get(RoleConfig.class);
    }

    public static UnionConfig getUnionConfig() {
        return (UnionConfig) configDataMaps[readIndex()].get(UnionConfig.class);
    }

    public static ActivityConfig getActivityConfig() {
        return (ActivityConfig) configDataMaps[readIndex()].get(ActivityConfig.class);
    }
}
