package com.highfly029.logic.tool;

import com.highfly029.core.tool.LoggerTool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @ClassName LogicLoggerTool
 * @Description 逻辑服日志工具
 * @Author liyunpeng
 **/
public class LogicLoggerTool extends LoggerTool {
    /**
     * game日志
     */
    public static final Logger gameLogger = LoggerFactory.getLogger("game");

    /**
     * 行为日志
     */
    public static final Logger actionLogger = LoggerFactory.getLogger("action");
}
