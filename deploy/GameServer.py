# coding=utf-8
from fabfile import Connection
from invoke import run as local
from fabfile import ThreadingGroup
import logger 

log = logger.log

'''服务器模版抽象类'''
class AbsServer(object):

    def __init__(self, rootPath) -> None:
        self._rootPath = rootPath
        self._dir = self.__class__.__name__
        self._dir = self._dir.removesuffix("Server")
        self._dir = "Game"+self._dir
        self._path = rootPath + "/" + self._dir
        self._name = self._dir.removeprefix("Game").lower() 

    def getName(self):
        return self._name

    def getZipName(self):
        return "{}-1.0.0-final.zip".format(self._dir)

    def getJarName(self):
        return "{}-1.0.0.jar".format(self._dir) 

    '''设置目标地址'''  
    def setTargetHosts(self, hosts):
         self._hosts = hosts

    '''开启防火墙'''
    def firewall(slef):
        pass

    '''执行mvnBase'''
    def compileGameBase(self):
        local("cd {}/mvn && python mvnBase.py > /dev/null".format(self._rootPath))     

    '''执行sql初始化 '''
    def initSql(self):
        log.warning("默认不需要初始化sql")

    '''只编译自己'''
    def compileSelf(self):
       local("cd {} && mvn -DskipTests clean install > /dev/null".format(self._path)) 

    '''编译'''
    def compileAllProject(self):
       log.warning("compileProject")
       self.compileGameBase()
       self.compileSelf()

    '''上传全量zip包'''
    def uploadZip(self):
        log.warning("begin uploadZip")
        group = ThreadingGroup(*self._hosts)
        results = group.put("{}/target/{}".format(self._path, self.getZipName()), "/home")
        for r in results:
            log.warning("host={}, name={} uploadZip upload success".format(r.host, self._name))
        
        results = group.run("cd /home && unzip -o {} -d {}".format(self.getZipName(), self._name), hide=True)
        for r in results:
            log.warning("host={}, name={} uploadZip unzip success".format(r.host, self._name))

    '''上传全部jar包'''
    def uploadAllJar(self):
        log.warning("begin uploadAllJar")
        group = ThreadingGroup(*self._hosts)
        results = group.put("{}/target/{}".format(self._path, self.getZipName()), "/home")
        for r in results:
            log.warning("host={}, name={} uploadZip upload success".format(r.host, self._name))
        
        results = group.run("cd /home && if [ -d tmp ]; then rm -rf tmp; fi && unzip -o {} -d tmp && \cp -rf tmp/lib/* {}/lib/".format(self.getZipName(), self._name), hide=True)
        for r in results:
            log.warning("host={}, name={} uploadAllJar success".format(r.host, self._name))

    def startServer(self):
        log.warning("startServer") 
        group = ThreadingGroup(*self._hosts)
        results = group.run("cd /home/{}/bin && chmod +x start.sh && " \
            "while test `jcmd |grep {}|cut -d ' ' -f1`; do sleep 3s; echo 'wait to stop server'; done" \
                "&& ./start.sh".format(self._name, self._name))
        for r in results:
            log.warning("host={}, name={} startServer success".format(r.host, self._name))

    def stopServer(self):
        log.warning("stopServer")
        group = ThreadingGroup(*self._hosts)
        #如果监测到对应进程则kill
        results = group.run("if test `jcmd |grep {} |cut -d ' ' -f1`;then kill `jcmd |grep {}|cut -d ' ' -f1`; fi".format(self._name, self._name))
        for r in results:
            log.warning("host={}, name={} stopServer success".format(r.host, self._name))     

    '''上传对应项目的逻辑代码jar包'''
    def uploadLogicCodeJar(self):
        group = ThreadingGroup(*self._hosts)
        results = group.put("{}/target/lib/{}".format(self._path, self.getJarName()), "/home/{}/lib".format(self._name))
        for r in results:
            log.warning("host={}, name={} uploadLogicCodeJar upload success".format(r.host, self._name)) 

    '''更新逻辑代码jar包并重启'''
    def updateLogicCodeJar(self):
        self.compileSelf()
        self.uploadLogicCodeJar()
        self.restartServer()

    '''重启服务器'''
    def restartServer(self):
        self.stopServer()
        self.startServer()

    '''重置数据库并重启'''
    def resetSqlAndRestart(self):
        #TODO 线上禁用
        self.stopServer()
        self.initSql()
        self.startServer()
    
    '''全部更新后重启'''
    def updateJarLib(self):
        self.compileAllProject()
        self.uploadAllJar()
        self.restartServer()

    '''全量操作'''
    def allOperation(self):
        log.warning("allOperation")
        self.compileAllProject()
        self.uploadHotfixJar()
        self.uploadZip()
        self.initSql()
        self.firewall()
        #self.restartServer()


    '''上传热更代码工具jar，同时下载arthas'''
    def uploadHotfixJar(self):
        group = ThreadingGroup(*self._hosts)
        results = group.put("{}/GameHotfix/target/GameHotfix-1.0.0.jar".format(self._rootPath), "/home/")
        for r in results:
            log.warning("host={}, name={} uploadHotfixJar upload success".format(r.host, self._name)) 
        
        results = group.run("cd /home && curl -O -s https://arthas.aliyun.com/arthas-boot.jar")
        for r in results:
            log.warning("host={}, name={} download arthas success".format(r.host, self._name)) 

    '''热更逻辑代码'''
    def hotfixCode(self):
        group = ThreadingGroup(*self._hosts)
        #备份删除
        results = group.run("cd /home/{} && [ -d classes ] && tar -zcvf classes-$(date +%Y%m%d-%H%M).tar.gz classes && rm -rf classes; echo 'backup!'".format(self._name))
        local("cd {} && tar -zcvf classes.tar.gz classes".format(self._path))
        #上传
        results = group.put("{}/classes.tar.gz".format(self._path), "/home/{}".format(self._name))
        for r in results:
            log.warning("host={}, name={} hotfixCode upload success".format(r.host, self._name))

        results = group.run("cd /home/{} && tar -zxvf classes.tar.gz".format(self._name))
        
        results = group.run("cd /home && java -jar GameHotfix-1.0.0.jar $(jcmd |grep {}|awk '{{print $1}}') /home/GameHotfix-1.0.0.jar /home/{}/classes".format(self._name.capitalize(), self._name))
        for r in results:
            log.warning("host={}, name={} hotfixCode success".format(r.host, self._name))

    '''热更config'''
    def hotfixConfig(self):
        local("cd {}/src/main/resources && tar -zcvf csv.tar.gz csv".format(self._path)) 
        group = ThreadingGroup(*self._hosts)
        results = group.put("{}/src/main/resources/csv.tar.gz".format(self._path), "/home/{}/conf".format(self._name))
        for r in results:
            log.warning("host={}, name={} hotfixConfig upload success".format(r.host, self._name))

        results = group.run("cd /home/{}/conf/ && tar -zxvf csv.tar.gz && curl -s http://localhost:9999/logic/hotfixConfig && echo '\n'".format(self._name))
        for r in results:
            log.warning("host={}, name={} hotfixConfig curl success".format(r.host, self._name))

    '''热更property'''
    def hotfixProperty(self):
        group = ThreadingGroup(*self._hosts)
        results = group.put("{}/src/main/resources/config.properties".format(self._path), "/home/{}/conf".format(self._name))
        for r in results:
            log.warning("host={}, name={} hotfixProperty upload success".format(r.host, self._name))

        results = group.run("cd /home/{}/conf/ && curl -s http://localhost:9999/{}/hotfixProperty && echo '\n'".format(self._name, self._name))
        for r in results:
            log.warning("host={}, name={} hotfixProperty curl success".format(r.host, self._name))

    '''设置clientHost为内网ip'''
    def setInnerIP(self):
        group = ThreadingGroup(*self._hosts)
        results = group.run('cd /home/{}/conf && {} && {}'.format(self._name, 'INNER_IP=$(ifconfig|grep "inet "|grep -v "127.0"|xargs|awk -F "[ :]" "{print \$2}")','sed -i "s/^clientHost.*/clientHost=${INNER_IP}/" config.properties'))

    '''设置clientHost为公网ip'''
    def setPublicIP(self):
        group = ThreadingGroup(*self._hosts)
        results = group.run('''cd /home/{}/conf && sed -i "s/^clientHost.*$/clientHost=`curl ifconfig.me`/" config.properties'''.format(self._name))

    '''配置'''
    def configuration(self, rowData):
        raise Exception("configuration must be override")

'''场景服务器'''
class SceneServer(AbsServer):
    def firewall(self):
        group = ThreadingGroup(*self._hosts)
        results = group.run("firewall-cmd --zone=public --permanent --add-port={}/tcp && firewall-cmd --reload".format(11001))

    def configuration(self, rowData):
        identifier = int(rowData[0])
        host = rowData[1]
        port = int(rowData[3])
        loadBalanceStr = rowData[4]

        group = ThreadingGroup(*self._hosts)
        results = group.run('''cd /home/{}/conf && sed -i "s/^identifier=.*$/identifier={}/" config.properties'''.format(self._name, identifier))
        results = group.run('''cd /home/{}/conf && sed -i "s/^clientHost=.*$/clientHost={}/" config.properties'''.format(self._name, host))
        results = group.run('''cd /home/{}/conf && sed -i "s/^clientPort=.*$/clientPort={}/" config.properties'''.format(self._name, port))

        results = group.run('''cd /home/{}/conf && sed -i "s/^net.tcp.server.scene.port=.*$/net.tcp.server.scene.port={}/" net.properties'''.format(self._name, port))

        loadBalanceArray = loadBalanceStr.split(",")
        for i in range(len(loadBalanceArray)):
            info = loadBalanceArray[i]
            if len(info) == 0:
                break
            id = i + 1
            infoArray = info.split(":")
            results = group.run('''cd /home/{}/conf && sed -i "s/^net.tcp.client.loadBalance{}.host=.*$/net.tcp.client.loadBalance{}.host={}/" net.properties'''.format(self._name, id, id, infoArray[0]))
            results = group.run('''cd /home/{}/conf && sed -i "s/^net.tcp.client.loadBalance{}.port=.*$/net.tcp.client.loadBalance{}.port={}/" net.properties'''.format(self._name, id, id, infoArray[1]))


'''逻辑服务器'''
class LogicServer(AbsServer):
    def initSql(self):
        group = ThreadingGroup(*self._hosts)
        results = group.run("if [ ! -d /home/sql ]; then mkdir /home/sql; fi")
        results = group.put("{}/sql/logic.sql".format(self._path), "/home/sql")
        for r in results:
            log.warning("host={}, name={} initSql upload success".format(r.host, self._name))

        results = group.run('mysql -uroot -p1234567890 -e "CREATE DATABASE IF NOT EXISTS {};"'.format('my_game_logic_1'))

        results = group.run('mysql -uroot -p1234567890 {} < /home/sql/logic.sql'.format('my_game_logic_1'))

        for r in results:
            log.warning("host={}, name={} initSql init success".format(r.host, self._name))

    def firewall(self):
        group = ThreadingGroup(*self._hosts)
        results = group.run("firewall-cmd --zone=public --permanent --add-port={}/tcp && firewall-cmd --reload".format(10001))

    def configuration(self, rowData):
        identifier = int(rowData[0])
        host = rowData[1]
        port = int(rowData[3])
        accountStr = rowData[4]
        loadBalanceStr = rowData[5]
        centerStr = rowData[6]
        mysqlStr = rowData[7]
        redisStr = rowData[8]

        group = ThreadingGroup(*self._hosts)
        results = group.run('''cd /home/{}/conf && sed -i "s/^identifier=.*$/identifier={}/" config.properties'''.format(self._name, identifier))
        results = group.run('''cd /home/{}/conf && sed -i "s/^clientHost=.*$/clientHost={}/" config.properties'''.format(self._name, host))
        results = group.run('''cd /home/{}/conf && sed -i "s/^clientPort=.*$/clientPort={}/" config.properties'''.format(self._name, port))

        accountArray = accountStr.split(":")
        results = group.run('''cd /home/{}/conf && sed -i "s/^accountHost=.*$/accountHost={}/" config.properties'''.format(self._name, accountArray[0]))
        results = group.run('''cd /home/{}/conf && sed -i "s/^accountPort=.*$/accountPort={}/" config.properties'''.format(self._name, accountArray[1]))

        results = group.run('''cd /home/{}/conf && sed -i "s/^net.tcp.server.logic.port=.*$/net.tcp.server.logic.port={}/" net.properties'''.format(self._name, port))

        loadBalanceArray = loadBalanceStr.split(",")
        for i in range(len(loadBalanceArray)):
            info = loadBalanceArray[i]
            if len(info) == 0:
                break
            id = i + 1
            infoArray = info.split(":")
            results = group.run('''cd /home/{}/conf && sed -i "s/^net.tcp.client.loadBalance{}.host=.*$/net.tcp.client.loadBalance{}.host={}/" net.properties'''.format(self._name, id, id, infoArray[0]))
            results = group.run('''cd /home/{}/conf && sed -i "s/^net.tcp.client.loadBalance{}.port=.*$/net.tcp.client.loadBalance{}.port={}/" net.properties'''.format(self._name, id, id, infoArray[1]))

        centerArray = centerStr.split(",")
        for i in range(len(centerArray)):            
            info = centerArray[i]
            if len(info) == 0:
                break
            id = i + 1
            infoArray = info.split(":")
            results = group.run('''cd /home/{}/conf && sed -i "s/^net.tcp.client.center{}.host=.*$/net.tcp.client.center{}.host={}/" net.properties'''.format(self._name, id, id, infoArray[0]))
            results = group.run('''cd /home/{}/conf && sed -i "s/^net.tcp.client.center{}.port=.*$/net.tcp.client.center{}.port={}/" net.properties'''.format(self._name, id, id, infoArray[1]))

        #mysql和redis 先删除再追加

'''中心服务器'''
class CenterServer(AbsServer):
   pass

'''帐号服务器'''
class AccountServer(AbsServer):

    def initSql(self):
        group = ThreadingGroup(*self._hosts)
        results = group.run("if [ ! -d /home/sql ]; then mkdir /home/sql; fi")
        results = group.put("{}/sql/game_account.sql".format(self._path), "/home/sql")
        results = group.put("{}/sql/game_account_data.sql".format(self._path), "/home/sql")
        for r in results:
            log.warning("host={}, name={} initSql upload success".format(r.host, self._name))

        results = group.run('mysql -uroot -p1234567890 -e "CREATE DATABASE IF NOT EXISTS {};"'.format('game_account'))

        results = group.run('mysql -uroot -p1234567890 {} < /home/sql/game_account.sql'.format('game_account')) 

        results = group.run('mysql -uroot -p1234567890 {} < /home/sql/game_account_data.sql'.format('game_account')) 

        for r in results:
            log.warning("host={}, name={} initSql init success".format(r.host, self._name))


    def configuration(self, rowData):
        port = int(rowData[3])
        mysqlStr = rowData[4]

        group = ThreadingGroup(*self._hosts)
        results = group.run('''cd /home/{}/conf && sed -i "s/^net.http.server.http.port=.*$/net.http.server.http.port={}/" net.properties'''.format(self._name, port))

        mysqlArray = mysqlStr.split(",")
        results = group.run('''cd /home/{}/conf && sed -i "s/^db.mysql.account_db.host=.*$/db.mysql.account_db.host={}/" net.properties'''.format(self._name, mysqlArray[0]))
        results = group.run('''cd /home/{}/conf && sed -i "s/^db.mysql.account_db.port=.*$/db.mysql.account_db.port={}/" net.properties'''.format(self._name, mysqlArray[1]))
        results = group.run('''cd /home/{}/conf && sed -i "s/^db.mysql.account_db.username=.*$/db.mysql.account_db.username={}/" net.properties'''.format(self._name, mysqlArray[2]))
        results = group.run('''cd /home/{}/conf && sed -i "s/^db.mysql.account_db.password=.*$/db.mysql.account_db.password={}/" net.properties'''.format(self._name, mysqlArray[3]))
        results = group.run('''cd /home/{}/conf && sed -i "s/^db.mysql.account_db.database=.*$/db.mysql.account_db.database={}/" net.properties'''.format(self._name, mysqlArray[4]))

'''负载均衡服务器'''
class LoadBalanceServer(AbsServer):
    def configuration(self, rowData):
        identifier = int(rowData[0])
        port = int(rowData[3])

        group = ThreadingGroup(*self._hosts)
        results = group.run('''cd /home/{}/conf && sed -i "s/^identifier=.*$/identifier={}/" config.properties'''.format(self._name, identifier))

        results = group.run('''cd /home/{}/conf && sed -i "s/^net.tcp.server.loadBalance.port=.*$/net.tcp.server.loadBalance.port={}/" net.properties'''.format(self._name, port))




'''SDK服务器'''
class SDKServer(AbsServer):
    def initSql(self):
        group = ThreadingGroup(*self._hosts)
        results = group.run("if [ ! -d /home/sql ]; then mkdir /home/sql; fi")
        results = group.put("{}/sql/game_sdk_data.sql".format(self._path), "/home/sql")
        results = group.put("{}/sql/game_sdk.sql".format(self._path), "/home/sql")
        for r in results:
            log.warning("host={}, name={} initSql upload success".format(r.host, self._name))

        results = group.run('mysql -uroot -p1234567890 -e "CREATE DATABASE IF NOT EXISTS {};"'.format('game_sdk'))

        results = group.run('mysql -uroot -p1234567890 {} < /home/sql/game_sdk.sql'.format('game_sdk')) 

        results = group.run('mysql -uroot -p1234567890 {} < /home/sql/game_sdk_data.sql'.format('game_sdk')) 

        for r in results:
            log.warning("host={}, name={} initSql init success".format(r.host, self._name))

    def configuration(self, rowData):
        port = int(rowData[3])
        mysqlStr = rowData[4]

        group = ThreadingGroup(*self._hosts)
        results = group.run('''cd /home/{}/conf && sed -i "s/^net.http.server.http.port=.*$/net.http.server.http.port={}/" net.properties'''.format(self._name, port))

        mysqlArray = mysqlStr.split(",")
        results = group.run('''cd /home/{}/conf && sed -i "s/^db.mysql.sdk_db.host=.*$/db.mysql.sdk_db.host={}/" net.properties'''.format(self._name, mysqlArray[0]))
        results = group.run('''cd /home/{}/conf && sed -i "s/^db.mysql.sdk_db.port=.*$/db.mysql.sdk_db.port={}/" net.properties'''.format(self._name, mysqlArray[1]))
        results = group.run('''cd /home/{}/conf && sed -i "s/^db.mysql.sdk_db.username=.*$/db.mysql.sdk_db.username={}/" net.properties'''.format(self._name, mysqlArray[2]))
        results = group.run('''cd /home/{}/conf && sed -i "s/^db.mysql.sdk_db.password=.*$/db.mysql.sdk_db.password={}/" net.properties'''.format(self._name, mysqlArray[3]))
        results = group.run('''cd /home/{}/conf && sed -i "s/^db.mysql.sdk_db.database=.*$/db.mysql.sdk_db.database={}/" net.properties'''.format(self._name, mysqlArray[4]))

