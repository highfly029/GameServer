#!/bin/bash

now=`pwd`
cd ~
home=`pwd`

echo ${now}"_"${home}
cd ${now}

[ ! -f ${home}/.ssh/id_rsa.pub ] && ssh-keygen -t ras -P '' &>/dev/null #密钥对不存在则创建
while read line; do
  ip=`echo $line | cut -d " " -f1` #提取文件中的ip
  user_name=`echo $line | cut -d " " -f2` #提取文件中的用户名
  pwd=`echo $line |cut -d " " -f3` #提取文件中的密码
expect <<EOF
  spawn ssh-copy-id -i ${home}/.ssh/id_rsa.pub $user_name@$ip 
  expect {
    "yes/no" { send "yes\n"; exp_continue}
    "password" {send "$pwd\n"}
  }
  expect eof
EOF

done < host_ip.txt #读取文件内容

#pscp.pssh -h host_ip.txt scripts /root
#pssh -h host_ip.txt -i bash /root/scripts.sh
