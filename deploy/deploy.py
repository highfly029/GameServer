# coding=utf-8
from fileinput import filename
import GameServer
import logger
import xlrd 

log = logger.log

'''获取根目录'''
def getRootPath():
    print("选择代码版本")
    print("1.master")
    flag = input("请输入:")
    flag = int(flag)
    if flag == 1:
        return '/Users/highfly029/Documents/project/GameServer'
    else:
        raise Exception("selectVersion invalid flag={}".format(flag))

    #是否需要更新目录

'''获取大区'''
def getRegions():
    print("选择要操作的大区(可多选用空格分割):")
    print("as.aliyun大区")
    print("us.本地1大区")
    flag = input("请输入:")
    log.warning("getRegions=%s", flag)
    flag = flag.strip()
    list = flag.split(" ")
    if len(list) == 0:
        raise Exception("getRegions invalid input")
        
    return list

'''获取服务器'''
def getServers(rootPath):
    print("选择服务器类型(可多选用空格分割):")
    print("1.场景服务器")
    print("2.逻辑服务器")
    print("3.中心服务器")
    print("4.帐号服务器")
    print("5.负载均衡服务器")
    print("6.SDK服务器")
    print("0.全部服务器")
    flag = input("请输入:")
    log.warning("getServers=%s", flag)
    flag = flag.strip()
    list = []
    if flag == '0':
        for i in range(1, 7):
            list.append(str(i))
    else:
        list = flag.split(" ")
        if len(list) == 0:
            raise Exception("getServers invalid input")

    servers = []
    for type in list:
        server = createServer(rootPath, int(type))
        if server == None:
            raise Exception("getServers invalid type=", type)
        servers.append(server)

    return servers

'''选择要部署的目标服主机'''
def selectDeployTarget(regionList, serverList):
    if regionList == None or len(regionList) == 0:
        raise Exception("selectDeployTarget invalid regionList")
    if serverList == None or len(serverList) == 0:
        raise Exception("selectDeployTarget invalid serverList")

    if len(regionList) == 1 and len(serverList) == 1:
        print("输入指定目标服id(0=全部服):")
        flag = input("请输入:")
        if flag.isdecimal():
            return int(flag)
        else:
            return -1
    else:
        print("确认部署到全部目标服主机?(y/n):")
        flag = input("请输入:")
        if flag == 'y':
            return 0
        else:
            return -1

def createServer(rootPath, type):
    if type == 1:
        return GameServer.SceneServer(rootPath)
    elif type == 2:
        return GameServer.LogicServer(rootPath)
    elif type == 3:
        return GameServer.CenterServer(rootPath)
    elif type == 4:
        return GameServer.AccountServer(rootPath)
    elif type == 5:
        return GameServer.LoadBalanceServer(rootPath)
    elif type == 6:
        return GameServer.SDKServer(rootPath)

'''选择操作'''
def selectOperation():

    print("选择操作")
    print("1.全部初始化&更新后重启")
    print("2.更新全部jar包后重启")
    print("3.更新逻辑代码jar包后重启")
    print("4.停服")
    print("5.重启")
    print("6.热更逻辑代码")
    print("7.热更config")
    print("8.热更property")
    print("9.重置数据库并重启(慎用)")
    print("10.配置config")
    flag = input("请输入:")
    log.warning("selectOperation=%s", flag)
    k = int(flag)
    if k == 1:
        return "allOperation"
    elif k == 2:
        return "updateJarLib"
    elif k == 3:
        return "updateLogicCodeJar"
    elif k == 4:
        return "stopServer"
    elif k == 5:
        return "restartServer"
    elif k == 6:
        return "hotfixCode"
    elif k == 7:
        return "hotfixConfig"
    elif k == 8:
        return "hotfixProperty"
    elif k == 9:
        return "resetSqlAndRestart"
    elif k == 10:
        return "configuration"
    else:
        return ""

def main():
    log.warning("deploy begin!")

    try:
        rootPath = getRootPath()
    except Exception as e:
        print("getRootPath", e)
        exit(-1)

    try:
        regionList = getRegions()
    except Exception as e:
        print("getRegions", e)
        exit(-1)

    try:
        serverList = getServers(rootPath)
    except Exception as e:
        print("getServers", e)
        exit(-1)

    try:
        target = selectDeployTarget(regionList, serverList)
        if target < 0:
            print("selectDeployTarget invaid target")
            exit(-1)
    except Exception as e:
        print("selectDeployTarget", e)
        exit(-1)

    try:
        operation = selectOperation()
    except Exception as e:
        print("selectOperation", e)
        exit(-1)

    log.warning("========================================")
    log.warning("rootPath=%s", rootPath)
    log.warning("regionList=%s", regionList)
    log.warning("serverList=%s", serverList)
    log.warning("target=%s", target)
    log.warning("operation=%s", operation)

    for region in regionList:
        fileName = region+"_info.xlsx"
        excelPath = rootPath + "/deploy/" + fileName
        workbook = xlrd.open_workbook(excelPath)
        for server in serverList:
            name = server.getName()
            sheet = workbook.sheet_by_name(name)
            startRowIndex = 1
            endRowIndex = sheet.nrows
            if operation == 'configuration':
                log.info("开始配置 region=%s, server=%s, target=%s", region, name, target)
                if target == 0:
                    for i in range(startRowIndex, endRowIndex):
                        rowData = sheet.row_values(i)
                        hosts = []
                        hosts.append(rowData[1])
                        server.setTargetHosts(tuple(hosts))
                        server.configuration(rowData)
                else:
                    for i in range(startRowIndex, endRowIndex):
                        rowData = sheet.row_values(i)
                        if rowData[0] == target:
                            hosts = []
                            hosts.append(rowData[1])
                            server.setTargetHosts(tuple(hosts))
                            server.configuration(rowData)
            else:
                hosts = []
                if target == 0:
                    for i in range(startRowIndex, endRowIndex):
                        rowData = sheet.row_values(i)
                        hosts.append(rowData[1])
                else:
                    for i in range(startRowIndex, endRowIndex):
                        rowData = sheet.row_values(i)
                        if rowData[0] == target:
                            hosts.append(rowData[1])

                server.setTargetHosts(tuple(hosts))
                getattr(server, operation)()

    log.warning("deploy success!\n\n\n\n")

if __name__ == "__main__":
    main()