# coding=utf-8
from invoke import run as local
from fabfile import ThreadingGroup

#根目录
rootPath = '/Users/highfly029/Documents/project/GameServer/deploy'

'''选择要部署的目标'''
def selectDeployTarget():
    hosts = []
    print("选择要部署的目标(可多选用空格分割):")
    print("1.虚拟1")
    print("2.虚拟2")
    print("3.aliyun")
    flag = input("请输入:")
    flag = flag.strip()
    list = flag.split(" ")
    for s in list:
        k = int(s)
        if k == 1:
            hosts.append("10.211.55.7")
        elif k == 2:
            hosts.append("10.211.55.8")
        elif k == 3:
            hosts.append("123.56.153.66")
    return tuple(hosts)

def main():
    print('main start!')
    try:
        targets = selectDeployTarget()
    except Exception as e:
        print("selectDeployTarget", e)
        exit(-1)
    
    #压缩
    local("cd {} && tar -zcvf boot.tar.gz boot".format(rootPath))

    group = ThreadingGroup(*targets)

    #上传
    results = group.put("{}/boot.tar.gz".format(rootPath), "/tmp/")
    for r in results:
        print("host={} upload success".format(r.host))

    #解压
    results = group.run("cd /tmp && tar -zxvf boot.tar.gz")
    for r in results:
        print("host={} decompression success".format(r.host))

    #上传jdk
    results = group.put("/Users/highfly029/Downloads/bakcup/openjdk-17.0.2_linux-x64_bin.tar.gz", "/tmp/")
    for r in results:
        print("host={} upload jdk success".format(r.host))
    
    #执行boot脚本
    results = group.run("cd /tmp && bash boot/boot.sh")
    for r in results:
        print("host={} boot success".format(r.host)) 

if __name__ == "__main__":
    main()


