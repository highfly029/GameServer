#!/bin/bash

#修改hostname
hostnamectl set-hostname highfly029

#安装wget
yum install -y wget
#安装gcc
yum install -y gcc

#更换yum源
#1)备份
cd /etc/yum.repos.d/
cp CentOS-Base.repo CentOS-Base.repo.bak
#2)下载repo文件
wget http://mirrors.aliyun.com/repo/Centos-7.repo
#3)清理旧包
yum -y clean all
#4)设置为默认源
mv -f Centos-7.repo CentOS-Base.repo
#5)生成yum源缓存
yum -y makecache
#6)更新,是否要更新系统以及所有的包
#yum -y update

#安装vim
yum install -y vim

#安装ifconfig
yum install -y net-tools 

#zip unzip
yum install -y unzip zip

#安装网络工具
#lsof
which lsof
if [ ! $? -eq 0 ]; then
	yum install -y lsof
	echo "install lsof ret:"$?
fi

#安装ntp
yum install -y ntp
systemctl enable ntpd
ntpdate pool.ntp.org
systemctl start ntpd   
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
systemctl status ntpd



#安装redis
redis-server -v
if [ ! $? -eq 0 ]; then
	source /tmp/boot/deploy_redis.sh /tmp/boot 6.2.6
	echo "install redis ret=:"$?
fi

#安装mysql
mysql -V
if [ ! $? -eq 0 ]; then
	source /tmp/boot/deploy_maria.sh /tmp/boot
	echo "install mysql ret=:"$?
fi


#安装java
java -version
if [ ! $? -eq 0 ]; then
	source /tmp/boot/deploy_java.sh /tmp
	echo "install java ret=:"$?
	source ~/.bashrc
	java -version
fi

#重启
echo "now reboot..."
reboot
