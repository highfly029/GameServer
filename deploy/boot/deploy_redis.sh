#!/bin/bash

if [ ! $# -eq 2 ]; then echo "invalid args"$#; exit -1; fi
path=$1
redisVersion=$2
echo "path="$path",redisVersion="$redisVersion
#安装redis
#1)切换路径
cd ${path}
#2)下载安装包
wget http://download.redis.io/releases/redis-${redisVersion}.tar.gz
#3)解压并安装
tar -zxvf redis-${redisVersion}.tar.gz
cd redis-${redisVersion}
make && make install PREFIX=/usr/local/redis

#4)复制配置文件
if [ ! -d /etc/redis ]; then
  mkdir /etc/redis
fi
cp -f ${path}/redis.conf /etc/redis/
#5)创建软连接命令
ln -s /usr/local/redis/bin/redis-cli /usr/bin/redis-cli
ln -s /usr/local/redis/bin/redis-server /usr/bin/redis-server

#6)设置开机启动
cp -f ${path}/redis.service /etc/systemd/system/
systemctl daemon-reload
systemctl start redis.service
systemctl enable redis.service

#6)开启防火墙
firewall-cmd --permanent --zone=public --add-port=6379/tcp
firewall-cmd --reload


