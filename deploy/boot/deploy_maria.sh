#!/bin/bash
if [ ! $# -eq 1 ]; then echo "invalid args"$#; exit -1; fi
path=$1

cp -f ${path}/MariaDB.repo /etc/yum.repos.d

sudo yum install -y MariaDB-server MariaDB-client

#开机启动
systemctl enable mariadb
#启动
systemctl start mariadb

#防火墙开启
firewall-cmd --zone=public --permanent --add-port=3306/tcp && firewall-cmd --reload

#设置密码
mysqladmin password 1234567890

#增加访问权限
mysql <<EOF
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '1234567890' WITH GRANT OPTION;
FLUSH   PRIVILEGES;
EOF

