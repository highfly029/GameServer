#!/bin/bash
if [ ! $# -eq 1 ]; then echo "invalid args"$#; exit -1; fi
path=$1

cd ${path} 

jdkFile=`find openjdk* |grep tar.gz`

if [ ! -f ${jdkFile} ]; then echo "not exit jdkFile="${jdkFile} exit -1; fi

tar -zxvf ${jdkFile}

dir=${jdkFile%%_*}
dir=${dir#open*}

mv ${dir} /usr/local/java

echo "export JAVA_HOME=/usr/local/java" >> ~/.bashrc
echo "export PATH=\$PATH:\$JAVA_HOME/bin" >> ~/.bashrc
echo "export CLASSPATH=.:\$JAVA_HOME/lib/dt.jar:\$JAVA_HOME/lib/tools.jar" >> ~/.bashrc


