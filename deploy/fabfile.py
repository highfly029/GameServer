from fabric import Connection
from fabric import task
from fabric import ThreadingGroup

host_ip = '10.211.55.8'
user_name = 'root'
cmd = 'date'


@task
def upload(c):
	con = Connection(host_ip)
	con.local('tar -zcvf backup.tar.gz backup')
	con.put('backup.tar.gz', '/tmp/')
	con.put('deploy.sh', '/tmp/')

@task
def uploadJDK(c):
	con = Connection(host_ip)
	con.put('/Users/highfly029/Downloads/bakcup/openjdk-17.0.2_linux-x64_bin.tar.gz', '/tmp/')


@task
def test(c, path):
	c.run("cd /tmp && pwd")
	c.run("cd {} && pwd".format(path))
	c.run("jcmd")


@task
def test2(c):
	list = []
	#list.append('10.211.55.7', '10.211.55.8')
	print(list)
	print(tuple(list))
	tup = ('10.211.55.7', '10.211.55.8')
	print('tup=',tup)
	group = ThreadingGroup(*tup)
	group.run("pwd")
	results = group.run("ifconfig")
	for r in results:
		con = results[r]
		#print("{} success ".format(con.host))
	
