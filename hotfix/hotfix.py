56#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import os
import subprocess
import sys

if __name__ == '__main__':
    if (len(sys.argv) != 2):
        print("argv num wrong!")
        sys.exit(-1)
    pid = sys.argv[1]
    cwd = os.getcwd()
    jarPath = os.path.join(cwd, "GameHotfix-1.0.0.jar")
    classPath = os.path.join(cwd, "class")

    cmd = "java -jar GameHotfix-1.0.0.jar " + pid + " " + jarPath + " " + classPath
    print(cmd)
    ret = subprocess.call(cmd, shell=True)

    os.system("pause")
