package com.highfly029.core.plugins.behaviortree;

/**
 * @ClassName BehaviorTreeNodeState
 * @Description 行为树节点状态
 * @Author liyunpeng
 **/
public class BehaviorTreeNodeState {
    public static final byte INVALID = 0;
    public static final byte SUCCESS = 1;
    public static final byte FAIL = 2;
    //在设置运行状态前先设置运行的节点(只有行为节点才有运行中状态)
    public static final byte RUNNING = 3;
}
