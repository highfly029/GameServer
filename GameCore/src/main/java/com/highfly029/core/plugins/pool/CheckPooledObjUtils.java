package com.highfly029.core.plugins.pool;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import com.highfly029.core.interfaces.IPooledObj;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.utils.collection.BaseHash;
import com.highfly029.utils.collection.BaseList;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName CheckPooledObjUtils
 * @Description 检查池化对象的工具
 * @Author liyunpeng
 **/
class CheckPooledObjUtils {

    /**
     * 检测是否完全的清理干净池化对象
     *
     * @param obj
     * @return
     */
    static boolean isReleaseCompletely(IPooledObj obj) throws IllegalAccessException {
        Class clz = obj.getClass();
        ObjectList<Field> fieldList = new ObjectList<>(Field[]::new);
        while (clz != null) {
            Field[] fields = clz.getDeclaredFields();
            fieldList.addArray(fields);
            //父类
            clz = clz.getSuperclass();
        }
        boolean ret = true;
        for (int i = 0, size = fieldList.size(); i < size; i++) {
            Field field = fieldList.get(i);
            if (Modifier.isStatic(field.getModifiers())) {
                continue;
            }
            boolean isDefaultValue = isFieldDefaultValue(obj, field);
            if (!isDefaultValue) {
                ret = false;
                LoggerTool.systemLogger.error("release className={} field not default value name={}", obj.getClass().getName(), field.getName());
                break;
            }
        }
        return ret;
    }

    private static boolean isFieldDefaultValue(IPooledObj IPooledObj, Field field) throws IllegalAccessException {
        String typeName = field.getType().getTypeName();
        field.setAccessible(true);
//        LoggerTool.systemLogger.error("fieldName=" + field.getName() + " isPrimitive=" + field.getType().isPrimitive() + " field typeName=" + typeName);
        switch (typeName) {
            case "boolean" -> {
                boolean objValue = field.getBoolean(IPooledObj);
                return !objValue;
            }
            case "boolean[]" -> {
                boolean[] objValue = (boolean[]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, len = objValue.length; i < len; i++) {
                        if (objValue[i]) {
                            return false;
                        }
                    }
                }
            }
            case "boolean[][]" -> {
                boolean[][] objValue = (boolean[][]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, iLen = objValue.length; i < iLen; i++) {
                        boolean[] iArray = objValue[i];
                        for (int j = 0, jLen = iArray.length; j < jLen; j++) {
                            if (objValue[i][j]) {
                                return false;
                            }
                        }
                    }
                }
            }
            case "byte" -> {
                byte objValue = field.getByte(IPooledObj);
                return objValue == 0;
            }
            case "byte[]" -> {
                byte[] objValue = (byte[]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, len = objValue.length; i < len; i++) {
                        if (objValue[i] != 0) {
                            return false;
                        }
                    }
                }
            }
            case "byte[][]" -> {
                byte[][] objValue = (byte[][]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, iLen = objValue.length; i < iLen; i++) {
                        byte[] iArray = objValue[i];
                        for (int j = 0, jLen = iArray.length; j < jLen; j++) {
                            if (objValue[i][j] != 0) {
                                return false;
                            }
                        }
                    }
                }
            }
            case "short" -> {
                short objValue = field.getShort(IPooledObj);
                return objValue == 0;
            }
            case "short[]" -> {
                short[] objValue = (short[]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, len = objValue.length; i < len; i++) {
                        if (objValue[i] != 0) {
                            return false;
                        }
                    }
                }
            }
            case "short[][]" -> {
                short[][] objValue = (short[][]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, iLen = objValue.length; i < iLen; i++) {
                        short[] iArray = objValue[i];
                        for (int j = 0, jLen = iArray.length; j < jLen; j++) {
                            if (objValue[i][j] != 0) {
                                return false;
                            }
                        }
                    }
                }
            }
            case "int" -> {
                int objValue = field.getInt(IPooledObj);
                return objValue == 0;
            }
            case "int[]" -> {
                int[] objValue = (int[]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, len = objValue.length; i < len; i++) {
                        if (objValue[i] != 0) {
                            return false;
                        }
                    }
                }
            }
            case "int[][]" -> {
                int[][] objValue = (int[][]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, iLen = objValue.length; i < iLen; i++) {
                        int[] iArray = objValue[i];
                        for (int j = 0, jLen = iArray.length; j < jLen; j++) {
                            if (objValue[i][j] != 0) {
                                return false;
                            }
                        }
                    }
                }
            }
            case "long" -> {
                long objValue = field.getLong(IPooledObj);
                return objValue == 0;
            }
            case "long[]" -> {
                long[] objValue = (long[]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, len = objValue.length; i < len; i++) {
                        if (objValue[i] != 0) {
                            return false;
                        }
                    }
                }
            }
            case "long[][]" -> {
                long[][] objValue = (long[][]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, iLen = objValue.length; i < iLen; i++) {
                        long[] iArray = objValue[i];
                        for (int j = 0, jLen = iArray.length; j < jLen; j++) {
                            if (objValue[i][j] != 0) {
                                return false;
                            }
                        }
                    }
                }
            }
            case "float" -> {
                float objValue = field.getFloat(IPooledObj);
                return objValue == 0.f;
            }
            case "float[]" -> {
                float[] objValue = (float[]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, len = objValue.length; i < len; i++) {
                        if (objValue[i] != 0.f) {
                            return false;
                        }
                    }
                }
            }
            case "float[][]" -> {
                float[][] objValue = (float[][]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, iLen = objValue.length; i < iLen; i++) {
                        float[] iArray = objValue[i];
                        for (int j = 0, jLen = iArray.length; j < jLen; j++) {
                            if (objValue[i][j] != 0.f) {
                                return false;
                            }
                        }
                    }
                }
            }
            case "double" -> {
                double objValue = field.getDouble(IPooledObj);
                return objValue == 0.d;
            }
            case "double[]" -> {
                double[] objValue = (double[]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, len = objValue.length; i < len; i++) {
                        if (objValue[i] != 0.d) {
                            return false;
                        }
                    }
                }
            }
            case "double[][]" -> {
                double[][] objValue = (double[][]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, iLen = objValue.length; i < iLen; i++) {
                        double[] iArray = objValue[i];
                        for (int j = 0, jLen = iArray.length; j < jLen; j++) {
                            if (objValue[i][j] != 0.d) {
                                return false;
                            }
                        }
                    }
                }
            }
            case "java.lang.String" -> {
                String objValue = (String) field.get(IPooledObj);
                return objValue == null || objValue.equals("");
            }
            case "java.lang.String[]" -> {
                String[] objValue = (String[]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, len = objValue.length; i < len; i++) {
                        if (objValue[i] != null && !objValue[i].equals("")) {
                            return false;
                        }
                    }
                }
            }
            case "java.lang.String[][]" -> {
                String[][] objValue = (String[][]) field.get(IPooledObj);
                if (objValue != null) {
                    for (int i = 0, iLen = objValue.length; i < iLen; i++) {
                        String[] iArray = objValue[i];
                        for (int j = 0, jLen = iArray.length; j < jLen; j++) {
                            if (objValue[i][j] != null && !objValue[i][j].equals("")) {
                                return false;
                            }
                        }
                    }
                }
            }
            case "char", "char[]", "char[][]" -> {
                LoggerTool.systemLogger.error("cant support char... obj={}", IPooledObj);
                return false;
            }
            default -> {
                Object objValue = field.get(IPooledObj);
                if (objValue == null) {
                    return true;
                } else if (objValue instanceof BaseList) {
                    BaseList baseList = (BaseList) objValue;
                    return baseList.size() == 0;
                } else if (objValue instanceof BaseHash) {
                    BaseHash baseHash = (BaseHash) objValue;
                    return baseHash.size() == 0;
                } else if (objValue instanceof IPooledObj) {
                    return isReleaseCompletely((IPooledObj) objValue);
                } else if (objValue.getClass().isArray()) {
                    IPooledObj[] array = (IPooledObj[]) objValue;
                    boolean isRelease = true;
                    IPooledObj tmpObj;
                    for (int i = 0, len = array.length; i < len; i++) {
                        if ((tmpObj = array[i]) != null) {
                            LoggerTool.systemLogger.error("PooledObj array must be null element. obj={}", IPooledObj);
                            isRelease = false;
                        }
                    }
                    return isRelease;
                } else {
                    LoggerTool.systemLogger.error("cant find typeName={}, obj={}", typeName, IPooledObj);
                    return false;
                }
            }
        }
        return true;
    }
}
