package com.highfly029.core.plugins.rank;

import java.util.Arrays;

import com.highfly029.core.plugins.IPlugin;

/**
 * @ClassName BaseRankPlugin
 * @Description 排行榜插件
 * @Author liyunpeng
 **/
public abstract class BaseRankPlugin implements IPlugin {
    /**
     * 排行数据
     */
    private BaseRankData[] rankArray;
    /**
     * 排行榜大小
     */
    private final int rankMaxNum;

    public BaseRankPlugin(int rankMaxNum) {
        this.rankMaxNum = rankMaxNum;
    }

    public void setRankArray(BaseRankData[] rankArray) {
        this.rankArray = rankArray;
    }

    public BaseRankData[] getRankArray() {
        return rankArray;
    }

    /**
     * 增加排行数据
     *
     * @param rankData
     */
    public void addRankData(BaseRankData rankData) {
        BaseRankData tmpRankData;

        int updateIndex = -1;
        int nullIndex = -1;
        boolean leftShift = false;
        for (int i = 0; i < rankMaxNum; i++) {
            if ((tmpRankData = rankArray[i]) != null) {
                if (tmpRankData.getKey() == rankData.getKey()) {
                    updateIndex = i;
                    int compare = compare(tmpRankData, rankData);
                    if (compare == 0) {
                        //相同key的排行数据没有变化、刷新后直接返回
                        rankArray[updateIndex] = rankData;
                        onRankChange();
                        return;
                    }
                    //需要左移
                    if (compare == 1) {
                        leftShift = true;
                    }
                    //直接跳出
                    break;
                }
            }
        }

        //更新数据
        if (updateIndex > -1) {
            if (leftShift) {
                //数据左移
                if (updateIndex == 0) {
                    //如果已经第一了、没办法左移、直接修改排行数据
                    rankArray[updateIndex] = rankData;
                } else if (compare(rankArray[updateIndex - 1], rankData) != 1) {
                    //如果跟左边的比较、没办法左移、直接修改排行数据
                    rankArray[updateIndex] = rankData;
                } else {
                    int index = 0;
                    for (int i = 0; i < updateIndex; i++) {
                        tmpRankData = rankArray[i];
                        if (compare(tmpRankData, rankData) == 1) {
                            index = i;
                            break;
                        }
                    }
                    int len = updateIndex - index;
                    System.arraycopy(rankArray, index, rankArray, index + 1, len);
                    rankArray[index] = rankData;
                }
            } else {
                //数据右移

                //找到nullIndex的位置
                for (int i = updateIndex; i < rankMaxNum; i++) {
                    if (rankArray[i] == null) {
                        nullIndex = i;
                        break;
                    }
                }
                //排行榜已满时设置null位置
                if (nullIndex == -1) {
                    nullIndex = rankMaxNum;
                }

                if (updateIndex == nullIndex - 1) {
                    //如果已经是最后一个、没办法右移、直接修改排行数据
                    rankArray[updateIndex] = rankData;
                } else if (compare(rankData, rankArray[updateIndex + 1]) != 1) {
                    //如果跟右边的比较、没办法右移、直接修改数据
                    rankArray[updateIndex] = rankData;
                } else {

                    int index = -1;
                    for (int i = updateIndex; i < nullIndex; i++) {
                        tmpRankData = rankArray[i];
                        if (compare(tmpRankData, rankData) == 1) {
                            index = i;
                            break;
                        }
                    }
                    //排行榜已满
                    if (index == -1) {
                        index = nullIndex;
                    }
                    int len = index - updateIndex - 1;
                    System.arraycopy(rankArray, updateIndex + 1, rankArray, updateIndex, len);
                    rankArray[index - 1] = rankData;
                }
            }
            onRankChange();
            return;
        }

        //增加数据
        if ((tmpRankData = rankArray[rankMaxNum - 1]) != null) {
            //排行已满
            //如果新加的排行数据 比较后没法进入排行榜
            if (compare(tmpRankData, rankData) != 1) {
                return;
            }
            //插入新的排行数据
            //倒序提高性能
            int insertIndex = 0;
            for (int i = rankMaxNum - 1; i >= 0; i--) {
                tmpRankData = rankArray[i];
                if (compare(tmpRankData, rankData) != 1) {
                    //找到插入的索引
                    insertIndex = i + 1;
                    break;
                }
            }
            int len = rankMaxNum - insertIndex - 1;
            System.arraycopy(rankArray, insertIndex, rankArray, insertIndex + 1, len);
            rankArray[insertIndex] = rankData;
            onRankChange();
        } else {
            //优化版本 不使用sort只进行一次内存拷贝
            int insertIndex = -1;
            for (int i = 0; i < rankMaxNum; i++) {
                tmpRankData = rankArray[i];
                if (tmpRankData == null) {
                    nullIndex = i;
                    break;
                }
                if (insertIndex > -1) {
                    continue;
                }
                if (compare(tmpRankData, rankData) == 1) {
                    insertIndex = i;
                }
            }
            if (insertIndex == -1 && nullIndex > -1) {
                //不需要插入，直接在最后一个null位置放入数据
                rankArray[nullIndex] = rankData;
            } else {
                //需要插入数据
                int len = nullIndex - insertIndex;
                System.arraycopy(rankArray, insertIndex, rankArray, insertIndex + 1, len);
                rankArray[insertIndex] = rankData;
            }
            onRankChange();
        }
    }

    /**
     * 按主键删除排行
     *
     * @param key
     */
    public void removeRankData(long key) {
        int index = -1;
        BaseRankData rankData;
        for (int i = 0, len = rankArray.length; i < len; i++) {
            if ((rankData = rankArray[i]) != null) {
                if (rankData.getKey() == key) {
                    index = i;
                    break;
                }
            }
        }
        if (index > -1) {
            removeRankIndex(index);
        }
    }

    /**
     * 按排名删除排行
     *
     * @param index
     */
    private void removeRankIndex(int index) {
        if (index < 0 || index >= rankMaxNum) {
            return;
        }
        int len = rankMaxNum - index - 1;
        if (len > 0) {
            System.arraycopy(rankArray, index + 1, rankArray, index, len);
        }
        rankArray[rankMaxNum - 1] = null;
    }

    /**
     * 更新排行榜非排行相关数据
     *
     * @param baseRankData
     */
    public void updateRankData(BaseRankData baseRankData) {
        //TODO
    }

    /**
     * 排行榜变化通知
     */
    protected void onRankChange() {
    }

    /**
     * 清空排行榜
     */
    public void clear() {
        for (int i = 0, len = rankArray.length; i < len; i++) {
            rankArray[i] = null;
        }
        onRankChange();
    }

    /**
     * 对排行榜进行一次全排序
     */
    public void sortRank() {
        Arrays.sort(rankArray, 0, rankArray.length, this::compare);
        onRankChange();
    }

    /**
     * 比较两个排行数据
     *
     * @param rankData1
     * @param rankData2
     * @return 返回1需要交换位置
     */
    protected abstract int compare(BaseRankData rankData1, BaseRankData rankData2);

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < rankMaxNum; i++) {
            BaseRankData rankData = rankArray[i];
            stringBuilder.append("rank:").append(i).append(" ");
            if (rankData == null) {
                stringBuilder.append("data:").append("null");
            } else {
                stringBuilder.append("data:").append(rankData);
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
