package com.highfly029.core.plugins.behaviortree;

import java.io.FileReader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.highfly029.core.plugins.IPlugin;
import com.highfly029.core.plugins.behaviortree.node.BaseCompositeNode;
import com.highfly029.utils.ReflectionUtils;

/**
 * @ClassName BehaviorTree
 * @Description 行为树
 * @Author liyunpeng
 **/
public class BehaviorTree implements IPlugin {
    private static final String ROOT_CLASS = "rootClass";
    public static final String NODE_CLASS = "nodeClass";
    public static final String CHILDREN = "children";
    /**
     * 根节点
     */
    private BaseCompositeNode root;

    public BehaviorTree() {

    }

    /**
     * 使用json文件初始化行为树
     *
     * @param jsonFileAbsPath json绝对路径
     */
    public void initWithJsonFile(String jsonFileAbsPath) {
        try (FileReader fileReader = new FileReader(jsonFileAbsPath)) {
            JsonReader reader = new JsonReader(fileReader);
            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(reader).getAsJsonObject();

            String rootClass = jsonObject.get(ROOT_CLASS).getAsString();
            Class<?> cls = Class.forName(rootClass);
            Object root = ReflectionUtils.getNewInstance(cls);
            this.root = (BaseCompositeNode) root;
            this.root.initWithJson(jsonObject);
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public byte execute(BehaviorTreeInfo behaviorTreeInfo) {
        return root.execute(behaviorTreeInfo);
    }
}
