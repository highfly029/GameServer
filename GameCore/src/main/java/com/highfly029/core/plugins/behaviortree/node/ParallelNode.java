package com.highfly029.core.plugins.behaviortree.node;

import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;

/**
 * @ClassName ParallelNode
 * @Description 并行节点
 * @Author liyunpeng
 **/
public class ParallelNode extends BaseCompositeNode {

    @Override
    public byte execute(BehaviorTreeInfo behaviorTreeInfo) {
        return 0;
    }
}
