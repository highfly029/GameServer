package com.highfly029.core.plugins.section;

import java.util.Arrays;

import com.highfly029.core.plugins.IPlugin;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName Section
 * @Description 分段区间类 min <= key < max 如果max==-1表示无限大
 * @Author liyunpeng
 **/
public final class SectionPlugin<T> implements IPlugin {
    private IntList minList = new IntList();
    private IntList maxList = new IntList();
    private ObjectList<T> valueList = new ObjectList<>();

    private int[] minArray;
    private int[] maxArray;
    private T[] valueArray;
    /**
     * 默认无效为false
     */
    private boolean invalid;

    /**
     * 添加数据
     *
     * @param min
     * @param max
     * @param value
     */
    public void add(int min, int max, T value) {
        if (max != -1 && min >= max) {
            LoggerTool.systemLogger.error("section min>=max", new Exception());
            invalid = true;
            return;
        }
        if (value == null) {
            LoggerTool.systemLogger.error("section value is null", new Exception());
            invalid = true;
            return;
        }
        int len = maxList.size();
        if (len > 0) {
            int lastMax = maxList.get(len - 1);
            if (lastMax != min) {
                LoggerTool.systemLogger.error("section lastMax != min", new Exception());
                invalid = true;
                return;
            }
        }

        minList.add(min);
        maxList.add(max);
        valueList.add(value);
    }

    /**
     * 生成最终数据
     */
    public void generate() {
        if (invalid) {
            LoggerTool.systemLogger.error("section cant generate", new Exception());
            return;
        }
        minArray = minList.toArray();
        maxArray = maxList.toArray();
        valueArray = valueList.toArray();

        minList = null;
        maxList = null;
        valueList = null;
    }

    /**
     * 获取区间值
     *
     * @param key
     * @return null表示不在区间范围内
     */
    public T get(int key) {
        int index = Arrays.binarySearch(minArray, key);
        if (index >= 0) {
            return valueArray[index];
        } else {
            int len = minArray.length;
            if (index == -1) {
                return null;
            } else if (-index == len + 1) {
                int lastMax = maxArray[len - 1];
                if (lastMax == -1 || key < lastMax) {
                    return valueArray[len - 1];
                } else {
                    return null;
                }
            } else {
                index = -index - 2;
                return valueArray[index];
            }
        }
    }
}
