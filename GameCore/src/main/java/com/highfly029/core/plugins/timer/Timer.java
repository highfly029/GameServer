package com.highfly029.core.plugins.timer;

import com.highfly029.core.interfaces.ITimerCallback;
import com.highfly029.core.tool.SystemTimeTool;

/**
 * @ClassName Timer
 * @Description 定时器
 * @Author liyunpeng
 **/
public class Timer {
    private static long idGenerator = 0;
    /**
     * 唯一标识
     */
    private long id;
    /**
     * 延迟时间 单位毫秒
     */
    private int delay;
    /**
     * 重复执行次数 最小是1
     */
    private int repeated;
    /**
     * 间隔时间
     */
    private int interval;

    /**
     * 回调
     */
    private ITimerCallback callback;

    /**
     * 剩余执行次数
     */
    private int remainCnt;
    /**
     * 下次触发时间
     */
    private long triggerTime;

    public static Timer create() {
        Timer timer = new Timer();
        timer.id = ++idGenerator;
        return timer;
    }

    /**
     * 初始化配置
     */
    public void build() {
        this.remainCnt = this.repeated;
        this.triggerTime = SystemTimeTool.getMillTime() + this.delay;
    }

    public Timer buildDelay(int delay) {
        this.delay = delay;
        return this;
    }

    public Timer buildRepeated(int repeated) {
        this.repeated = repeated;
        return this;
    }

    public Timer buildInterval(int interval) {
        this.interval = interval;
        return this;
    }

    public Timer buildCallback(ITimerCallback callback) {
        this.callback = callback;
        return this;
    }

    public final void setTriggerTime() {
        this.triggerTime = SystemTimeTool.getMillTime() + this.interval;
    }

    public final void trigger() {
        this.callback.onTimerCallback(this);
    }

    public final void cancel() {
        this.remainCnt = 0;
    }

    public final int getDelay() {
        return delay;
    }

    public final int getRepeated() {
        return repeated;
    }

    public final int getInterval() {
        return interval;
    }

    public final long getTriggerTime() {
        return triggerTime;
    }

    public final int getRemainCnt() {
        return remainCnt;
    }

    public final void decRemainCnt() {
        remainCnt--;
    }

    @Override
    public String toString() {
        return "Timer{" +
                "id=" + id +
                ", delay=" + delay +
                ", repeated=" + repeated +
                ", interval=" + interval +
                ", remainCnt=" + remainCnt +
                ", triggerTime=" + triggerTime +
                '}';
    }
}
