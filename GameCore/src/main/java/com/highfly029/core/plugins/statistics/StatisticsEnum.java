package com.highfly029.core.plugins.statistics;

public enum StatisticsEnum {
    /**
     * 统计消耗时间
     */
    MSG_TIME_COST("time"),
    /**
     * 统计消息大小
     */
    MSG_LENGTH("length");


    private final String value;

    /**
     * 对应的统计插件
     */
    private final StatisticsPlugin plugin = new StatisticsPlugin();

    StatisticsEnum(String value) {
        this.value = value;
    }

    public StatisticsPlugin getPlugin() {
        return plugin;
    }

}

