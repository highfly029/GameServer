package com.highfly029.core.plugins.behaviortree.node;

import com.google.gson.JsonObject;

/**
 * @ClassName BaseDecoratorNode
 * @Description 装饰节点
 * @Author liyunpeng
 **/
public abstract class BaseDecoratorNode extends BaseNode {
    protected BaseNode child;

    @Override
    public void initWithJson(JsonObject jsonObject) throws Exception {

    }

    /**
     * 装饰节点只有一个子节点
     *
     * @param child
     */
    public void setChild(BaseNode child) {
        this.child = child;
    }
}
