package com.highfly029.core.plugins.behaviortree;

import com.highfly029.core.plugins.behaviortree.node.BaseNode;

/**
 * @ClassName BehaviorTreeInfo
 * @Description 每一个实例存储的行为树信息
 * @Author liyunpeng
 **/
public abstract class BehaviorTreeInfo {
    /**
     * 上一次状态
     */
    public byte preState;
    /**
     * 运行中的节点
     */
    public BaseNode runningNode;
}
