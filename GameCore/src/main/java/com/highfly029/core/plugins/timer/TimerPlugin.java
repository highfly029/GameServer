package com.highfly029.core.plugins.timer;

import java.util.PriorityQueue;

import com.highfly029.core.plugins.IPlugin;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.SystemTimeTool;

/**
 * @ClassName TimerPlugin
 * @Description 定时器插件
 * @Author liyunpeng
 **/
public final class TimerPlugin implements IPlugin {
    private static final int queueSize = 1024;
    private final PriorityQueue<Timer> timerQueue;

    public TimerPlugin() {
        timerQueue = new PriorityQueue<>(queueSize, (Timer timer1, Timer timer2) -> {
            long triggerTime1 = timer1.getTriggerTime();
            long triggerTime2 = timer2.getTriggerTime();
            if (triggerTime1 > triggerTime2) {
                return 1;
            } else if (triggerTime1 == triggerTime2) {
                return 0;
            } else {
                return -1;
            }
        });
    }

    public boolean addTimer(Timer timer) {
        if (timer.getDelay() <= 0) {
            return false;
        }
        if (timer.getRepeated() <= 0) {
            return false;
        }
        if (timer.getInterval() < 0) {
            return false;
        }

        timer.build();
        return timerQueue.offer(timer);
    }

    public void update() {
        Timer tmpTimer;
        long now = SystemTimeTool.getMillTime();
        while (true) {
            tmpTimer = timerQueue.peek();
            if (tmpTimer == null) {
                return;
            }
            if (tmpTimer.getTriggerTime() > now) {
                return;
            }
            timerQueue.poll();
            //被取消
            if (tmpTimer.getRemainCnt() <= 0) {
                continue;
            }
            try {
                tmpTimer.decRemainCnt();
                tmpTimer.trigger();
            } catch (Exception e) {
                LoggerTool.systemLogger.error(e.getMessage(), e);
            }
            if (tmpTimer.getRemainCnt() > 0) {
                tmpTimer.setTriggerTime();
                timerQueue.offer(tmpTimer);
            }
        }
    }
}
