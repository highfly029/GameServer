package com.highfly029.core.plugins.behaviortree.node;

import com.google.gson.JsonObject;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;

/**
 * @ClassName BaseNode
 * @Description 行为树节点
 * @Author liyunpeng
 **/
public abstract class BaseNode {
    public BaseNode() {

    }

    public void initWithJson(JsonObject jsonObject) throws Exception {

    }

    /**
     * 间隔执行
     *
     * @param behaviorTreeInfo 行为树信息
     * @return
     */
    public abstract byte execute(BehaviorTreeInfo behaviorTreeInfo);


}
