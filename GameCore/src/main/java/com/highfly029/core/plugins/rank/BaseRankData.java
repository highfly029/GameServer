package com.highfly029.core.plugins.rank;

/**
 * @ClassName BaseRankData
 * @Description 排行榜单位数据
 * @Author liyunpeng
 **/
public abstract class BaseRankData {

    /**
     * 主键 排行榜唯一标示  playerID & unionID
     */
    private long key;

    //refreshTime roleLevel fightPower


    public long getKey() {
        return key;
    }

    public void setKey(long key) {
        this.key = key;
    }
}
