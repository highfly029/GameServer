package com.highfly029.core.plugins.behaviortree.node;

/**
 * @ClassName BaseConditionNode
 * @Description 条件节点 只返回成功或失败
 * @Author liyunpeng
 **/
public abstract class BaseConditionNode extends BaseNode {
}
