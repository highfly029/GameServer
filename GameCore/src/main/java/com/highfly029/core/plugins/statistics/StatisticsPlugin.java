package com.highfly029.core.plugins.statistics;

import java.util.concurrent.ConcurrentHashMap;

import com.highfly029.core.tool.LoggerTool;
import com.highfly029.utils.collection.IntSet;

/**
 * @ClassName StatisticsPlugin
 * @Description 统计插件
 * @Author liyunpeng
 **/
public class StatisticsPlugin {
    /**
     * 统计集合
     */
    private final ConcurrentHashMap<Integer, StatisticsInfo> messageLength = new ConcurrentHashMap<>();

    private final IntSet ignoreSet = new IntSet();

    /**
     * 记录统计数据
     *
     * @param key
     * @param value
     */
    public void record(int key, int value) {
        if (ignoreSet.contains(key)) {
            return;
        }
        StatisticsInfo info = messageLength.get(key);
        if (info == null) {
            info = new StatisticsInfo(key);
            messageLength.put(key, info);
        }
        info.record(value);
    }

    /**
     * 增加忽略统计的key
     *
     * @param key
     */
    public void addIgnoreStatistics(int key) {
        ignoreSet.add(key);
    }

    /**
     * 打印统计数据
     */
    public void dump() {
        for (StatisticsInfo info : messageLength.values()) {
            LoggerTool.statisticsLogger.info("key={},count={},min={},max={},sum={}", info.getKey(), info.getCount(), info.getMinValue(), info.getMaxValue(), info.getSumValue());
        }
        messageLength.clear();
    }
}
