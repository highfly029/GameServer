package com.highfly029.core.plugins.statistics;

/**
 * @ClassName StatisticsInfo
 * @Description 统计信息
 * @Author liyunpeng
 **/
public class StatisticsInfo {
    /**
     * 主键
     */
    private final int key;
    /**
     * 次数
     */
    private int count;
    /**
     * 最小值
     */
    private int minValue = Integer.MAX_VALUE;
    /**
     * 最大值
     */
    private int maxValue = Integer.MIN_VALUE;
    /**
     * 累计值
     */
    private long sumValue;

    public StatisticsInfo(int key) {
        this.key = key;
    }

    /**
     * 记录值值
     *
     * @param value 统计值
     */
    public void record(int value) {
        count++;
        if (value < minValue) {
            minValue = value;
        }
        if (value > maxValue) {
            maxValue = value;
        }
        sumValue += value;
    }

    public int getKey() {
        return key;
    }

    public int getCount() {
        return count;
    }

    public int getMinValue() {
        return minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public long getSumValue() {
        return sumValue;
    }

    /**
     * 获取平均值
     *
     * @return
     */
    public int getAvgValue() {
        return (int) (sumValue / count);
    }

}
