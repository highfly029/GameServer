package com.highfly029.core.plugins.pool;

import java.util.function.Supplier;

import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.interfaces.IPooledObj;
import com.highfly029.core.plugins.IPlugin;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.collection.creater.ObjectArrayCreateInterface;

/**
 * @ClassName ObjectPool
 * @Description 对象池
 * @Author liyunpeng
 **/
public class ObjectPoolPlugin<T extends IPooledObj> implements IPlugin {
    /**
     * 池化数据存储在数组里
     */
    private final T[] array;

    /**
     * 最后一个空对象位置索引
     */
    private int nullIndex;

    /**
     * 对象构造器
     */
    private final Supplier<T> apply;

    /**
     * @param initCount 初始数量
     * @param maxCount  最大数量 填0就表示不使用池
     * @param create
     */
    public ObjectPoolPlugin(int initCount, int maxCount, ObjectArrayCreateInterface<T> create, Supplier<T> apply) {
        array = create.create(maxCount);
        this.apply = apply;

        if (initCount > 0) {
            if (initCount > maxCount) {
                initCount = maxCount;
            }
            for (int i = 0; i < initCount; i++) {
                array[i] = apply.get();
                nullIndex++;
            }
        }
    }

    /**
     * 获取一个
     *
     * @return
     */
    public T get() {
        //池为空,直接返回新创建的obj
        T t;
        if (nullIndex == 0) {
            t = apply.get();
        } else {
            //如果存在obj,拿出后置空前移再返回。否则直接返回新创建的obj
            T obj = array[nullIndex - 1];
            if (obj != null) {
                array[--nullIndex] = null;
                t = obj;
            } else {
                t = apply.get();
            }
        }
        t.init();
        return t;
    }

    /**
     * 放回一个
     *
     * @param obj
     */
    public void back(T obj) {
        if (obj == null) {
            LoggerTool.systemLogger.error("ObjectPool back null obj is null", new Exception());
            return;
        }
        //池已满、不缓存直接gc回收
        if (nullIndex == array.length) {
            return;
        }
        obj.release();

        if (ConfigPropUtils.getBoolValue(ConfigConst.isCheckPooledObj)) {
            //检测obj是否有没清理干净的
            try {
                if (!CheckPooledObjUtils.isReleaseCompletely(obj)) {
                    LoggerTool.systemLogger.error("ObjectPool back obj release not completely obj={}", obj);
                    return;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            //检测对象池重复添加
            for (int i = 0, len = array.length; i < len; i++) {
                T tmp = array[i];
                if (tmp != null && tmp == obj) {
                    LoggerTool.systemLogger.error("ObjectPool back duplicate obj={}", obj);
                    return;
                }
            }
        }
        array[nullIndex++] = obj;
    }
}
