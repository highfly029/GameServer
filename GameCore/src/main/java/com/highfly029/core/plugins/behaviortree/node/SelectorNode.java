package com.highfly029.core.plugins.behaviortree.node;

import com.highfly029.core.plugins.behaviortree.BehaviorTreeInfo;
import com.highfly029.core.plugins.behaviortree.BehaviorTreeNodeState;

/**
 * @ClassName SelectorNode
 * @Description 选择节点
 * @Author liyunpeng
 **/
public class SelectorNode extends BaseCompositeNode {

    @Override
    public byte execute(BehaviorTreeInfo behaviorTreeInfo) {
        BaseNode node;
        byte result = BehaviorTreeNodeState.FAIL;
        for (int i = 0; i < size; i++) {
            node = nodes[i];
            result = node.execute(behaviorTreeInfo);
            if (result == BehaviorTreeNodeState.SUCCESS) {
                break;
            } else if (result == BehaviorTreeNodeState.RUNNING) {
                break;
            }

        }
        return result;
    }
}
