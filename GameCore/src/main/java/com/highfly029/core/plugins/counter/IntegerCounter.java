package com.highfly029.core.plugins.counter;

import com.highfly029.core.tool.LoggerTool;

/**
 * @ClassName InterCounter
 * @Description 整型计数器
 * @Author liyunpeng
 **/
public class IntegerCounter {
    /**
     * 最小值
     */
    private final int min;
    /**
     * 最大值
     */
    private final int max;
    /**
     * 是否可以环形计数
     */
    private final boolean isRinger;
    /**
     * 计数
     */
    private int count;

    public IntegerCounter() {
        this(1, Integer.MAX_VALUE, false, 1);
    }

    public IntegerCounter(int min, int max, boolean isRinger) {
        this(min, max, isRinger, min);
    }

    public IntegerCounter(int min, int max, boolean isRinger, int defaultValue) {
        this.min = min;
        this.max = max;
        this.isRinger = isRinger;

        this.count = defaultValue;
    }

    public void reset() {
        this.count = this.min;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 获取下一个计数
     *
     * @return min <= ret <= max
     */
    public int getNextValue() {
        int value = count++;
        if (value > max) {
            if (isRinger) {
                value = count = min;
                count++;
            } else {
                LoggerTool.systemLogger.error("getCountValue invalid", new Exception());
            }
        }
        return value;
    }
}
