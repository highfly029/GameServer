package com.highfly029.core.plugins.behaviortree.node;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.highfly029.core.plugins.behaviortree.BehaviorTree;
import com.highfly029.utils.ReflectionUtils;

/**
 * @ClassName BaseCompositeNode
 * @Description 组合节点
 * @Author liyunpeng
 **/
public abstract class BaseCompositeNode extends BaseNode {
    protected BaseNode[] nodes;
    int size = 0;

    @Override
    public void initWithJson(JsonObject jsonObject) throws Exception {
        JsonArray jsonArray = jsonObject.getAsJsonArray(BehaviorTree.CHILDREN);
        if (jsonArray != null) {
            int size = jsonArray.size();
            nodes = new BaseNode[size];
            for (int i = 0; i < size; i++) {
                JsonObject element = jsonArray.get(i).getAsJsonObject();
                String nodeClass = element.get(BehaviorTree.NODE_CLASS).getAsString();
                Class<?> cls = Class.forName(nodeClass);
                BaseNode node = (BaseNode) ReflectionUtils.getNewInstance(cls);
                node.initWithJson(element);
                this.addChild(node);
            }
        }
    }

    /**
     * 组合节点可以添加多个子节点
     *
     * @param child
     */
    public void addChild(BaseNode child) {
        nodes[size++] = child;
    }
}
