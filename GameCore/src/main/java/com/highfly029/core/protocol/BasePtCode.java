package com.highfly029.core.protocol;

/**
 * @ClassName PtCode
 * @Description 网络协议
 * @Author liyunpeng
 **/
public class BasePtCode {
    //建立连接时互相告知对方连接类型
    public static final int CONNECT_TYPE_REQ = 1;
    public static final int CONNECT_TYPE_RESP = 2;
    //心跳协议
    public static final int HEART_REQ = 3;
    public static final int HEART_RESP = 4;

    public static final int MAX = 99;
}
