package com.highfly029.core.config;

import com.highfly029.core.interfaces.IGlobalConfig;
import com.highfly029.core.world.GlobalWorld;

/**
 * @ClassName BaseGlobalConfig
 * @Description 基础全局配置
 * @Author liyunpeng
 **/
public abstract class BaseGlobalConfig implements IGlobalConfig {
    private GlobalWorld globalWorld;

    public void setGlobalWorld(GlobalWorld globalWorld) {
        this.globalWorld = globalWorld;
    }

    @Override
    public void load() {
        dataProcess();
    }

    @Override
    public void reload() {
        dataProcess();
    }

    /**
     * 数据处理
     */
    protected abstract void dataProcess();

    public void init() {

    }
}
