package com.highfly029.core.session;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.utils.collection.IntList;

import io.netty.channel.Channel;

/**
 * @ClassName Session
 * @Description Session
 * @Author liyunpeng
 **/
public class Session {
    /**
     * session来源类型
     */
    private int type;

    private Channel channel;

    /**
     * session唯一表示
     */
    private int identifier;
    /**
     * session分配的子世界索引 服务端session用
     */
    private int worldIndex;
    /**
     * session关联的功能
     */
    private IntList features;

    //session 缓存标记
    private boolean isCache;

    private final PbPacket.TcpPacketCombine.Builder sendAfterSceneTickPacketListCache = PbPacket.TcpPacketCombine.newBuilder();

    public void init(int type, int identifier, Channel channel) {
        this.type = type;
        this.identifier = identifier;
        this.channel = channel;
    }

    /**
     * 增加功能
     *
     * @param feature 功能
     */
    public void addFeature(int feature) {
        if (features == null) {
            features = new IntList();
        }
        features.add(feature);
    }

    public IntList getFeatures() {
        return features;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public int getWorldIndex() {
        return worldIndex;
    }

    public void setWorldIndex(int worldIndex) {
        this.worldIndex = worldIndex;
    }

    //响应客户端请求 只发送协议号，不包含内容
    public void send(int protocolCode) {
        send(protocolCode, null);
    }

    public void send(int protocolCode, GeneratedMessageV3 message) {
        if (ConfigConst.tcpPacketCombine) {
            sendCombine(protocolCode, message);
        } else {
            sendDirectly(protocolCode, message);
        }
    }

    //在scene tick后发送的协议包，未来可做优化 合包和统一flush
    @Deprecated
    private void sendCombine(int protocolCode, GeneratedMessageV3 message) {
        //如果缓存数量太大、分开批次发送
        if (sendAfterSceneTickPacketListCache.getPacketsCount() > 10) {
            flush();
        }

        PbPacket.TcpPacket.Builder tmpPacketBuilder = PbPacket.TcpPacket.newBuilder();
        tmpPacketBuilder.setPtCode(protocolCode);
        if (message != null) {
            tmpPacketBuilder.setData(message.toByteString());
        }
        sendAfterSceneTickPacketListCache.addPackets(tmpPacketBuilder.build());
        isCache = true;
    }

    private void sendDirectly(int protocolCode, GeneratedMessageV3 message) {
        PbPacket.TcpPacket.Builder tmpPacketBuilder = PbPacket.TcpPacket.newBuilder();
        tmpPacketBuilder.setPtCode(protocolCode);
        if (message != null) {
            tmpPacketBuilder.setData(message.toByteString());
        }

        GeneratedMessageV3 build;
        if (ConfigConst.tcpPacketCombine) {
            PbPacket.TcpPacketCombine.Builder tmpPacketListBuilder = PbPacket.TcpPacketCombine.newBuilder();
            tmpPacketListBuilder.clear();
            tmpPacketListBuilder.addPackets(tmpPacketBuilder.build());
            build = tmpPacketListBuilder.build();
        } else {
            build = tmpPacketBuilder.build();
        }
        channel.writeAndFlush(build);
        addSendCache(protocolCode, build);
    }

    //统一刷
    public void flush() {
        if (ConfigConst.tcpPacketCombine && isCache) {
            PbPacket.TcpPacketCombine build = sendAfterSceneTickPacketListCache.build();
            channel.writeAndFlush(build);
            sendAfterSceneTickPacketListCache.clear();
            isCache = false;
        }
    }

    //服务器之间发送消息、如果在服务器之间网络断开后重连前的时间段、消息会发送不出去
    public void sendMsgS2S(int protocolCode, GeneratedMessageV3 message) {
        PbPacket.TcpPacket.Builder builder = PbPacket.TcpPacket.newBuilder();
        builder.setPtCode(protocolCode);
        if (message != null) {
            builder.setData(message.toByteString());
        }

        channel.writeAndFlush(builder.build());
    }

    /**
     * 关闭连接
     */
    public void close() {
        channel.close();
    }

    /**
     * 增加发送缓存
     *
     * @param protocolCode
     * @param message
     */
    protected void addSendCache(int protocolCode, GeneratedMessageV3 message) {

    }
}
