package com.highfly029.core.session;

/**
 * @ClassName SessionType
 * @Description SessionType
 * @Author liyunpeng
 **/
public class SessionType {
    /**
     * 客户端
     */
    public static final int Client = 1;
    /**
     * 场景服务器
     */
    public static final int Scene = 2;
    /**
     * 中心服务器
     */
    public static final int Center = 3;
    /**
     * 负载均衡服务器
     */
    public static final int LoadBalance = 4;
    /**
     * 逻辑服
     */
    public static final int Logic = 5;

}
