package com.highfly029.core.interfaces;

/**
 * @ClassName UpdateInterface
 * @Description 服务器每次更新
 * @Author liyunpeng
 **/
public interface IUpdate {
    /**
     * 每帧事件
     *
     * @param escapedMillTime
     */
    void onTick(int escapedMillTime);

    /**
     * 每秒触发
     */
    void onSecond();

    /**
     * 每分钟触发
     */
    void onMinute();

    /**
     * 停服通知
     */
    void onShutDown();
}
