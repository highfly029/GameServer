package com.highfly029.core.interfaces;

import com.highfly029.core.event.Event;

/**
 * @ClassName EventListener
 * @Description
 * @Author liyunpeng
 **/
public interface IEventListener<T extends Event> {
    void onEvent(T event);
}
