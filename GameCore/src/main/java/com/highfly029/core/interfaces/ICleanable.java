package com.highfly029.core.interfaces;

/**
 * @ClassName Cleanable
 * @Description 清理接口
 * @Author liyunpeng
 **/
public interface ICleanable {
    void clean();
}
