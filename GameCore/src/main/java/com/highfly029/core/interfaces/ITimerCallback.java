package com.highfly029.core.interfaces;

import com.highfly029.core.plugins.timer.Timer;

/**
 * @ClassName TimerCallback
 * @Description
 * @Author liyunpeng
 **/
public interface ITimerCallback {
    void onTimerCallback(Timer timer);
}
