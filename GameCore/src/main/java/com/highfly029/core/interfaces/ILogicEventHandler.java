package com.highfly029.core.interfaces;

import com.highfly029.core.world.LogicEvent;

/**
 * @ClassName LogicEventHandler
 * @Description
 * @Author liyunpeng
 **/

public interface ILogicEventHandler<T extends LogicEvent> {
    void onLogicEvent(T event);
}
