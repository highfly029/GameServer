package com.highfly029.core.interfaces;

/**
 * @ClassName ISendMsgFailCallback
 * @Description 发送消失失败的回调
 * @Author liyunpeng
 **/
public interface ISendMsgFailCallback extends ICallback {
    /**
     * 发送失败回滚接口
     */
    void onSendFailRollback();
}
