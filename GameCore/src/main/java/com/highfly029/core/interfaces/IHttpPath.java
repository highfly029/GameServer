package com.highfly029.core.interfaces;

/**
 * @ClassName IHttpPath
 * @Description http 路径枚举接口
 * @Author liyunpeng
 **/
public interface IHttpPath {
    /**
     * 是否只有一个child处理
     *
     * @return
     */
    boolean isOneChildHandleOnly();

    /**
     * 是否所有child处理
     *
     * @return
     */
    boolean isAllChildHandle();
}
