package com.highfly029.core.interfaces;

import io.vertx.redis.client.Response;

/**
 * @ClassName IRedisCallback
 * @Description IRedisCallback
 * @Author liyunpeng
 **/
public interface IRedisCallback extends ICallback {
    /**
     * @param isSuccess
     * @param response  成功返回的消息
     * @param errorMsg  失败消息
     */
    void onRedisCallback(boolean isSuccess, Response response, String errorMsg);
}
