package com.highfly029.core.interfaces;

import com.highfly029.core.world.WorldThread;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @ClassName IHttpHandler
 * @Description http处理协议
 * @Author liyunpeng
 **/
public interface IHttpHandler {
    /**
     * 是否使global使用
     *
     * @return
     */
    boolean isGlobal();

    /**
     * 初始化
     *
     * @param worldThread
     */
    void init(WorldThread worldThread);

    /**
     * 注册路径
     *
     * @return 路径
     */
    String register();

    /**
     * 处理请求
     *
     * @param channel
     * @param request
     */
    void handler(Channel channel, FullHttpRequest request);
}
