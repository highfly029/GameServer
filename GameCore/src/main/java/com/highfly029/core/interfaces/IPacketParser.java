package com.highfly029.core.interfaces;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

/**
 * @ClassName IPacketTransfer
 * @Description 协议数据转换接口
 * @Author liyunpeng
 **/
public interface IPacketParser {

    /**
     * 获取协议号
     *
     * @return
     */
    int getPtCode();

    /**
     * 获取协议名字
     * @return
     */
    String getName();

    /**
     * 是否解析成16进制的字符串格式
     *
     * @return
     */
    boolean isParserHex();

    /**
     * 把pb解析成可视化的字符串
     *
     * @param byteString
     * @return
     */
    String parser(ByteString byteString) throws InvalidProtocolBufferException;
}
