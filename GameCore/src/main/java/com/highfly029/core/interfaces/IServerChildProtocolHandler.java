package com.highfly029.core.interfaces;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.core.session.Session;

/**
 * @ClassName IServerChildProtocolHandler
 * @Description IServerChildProtocolHandler
 * @Author liyunpeng
 **/
public interface IServerChildProtocolHandler {
    void onServerProtocolHandler(Session session, int ptCode, GeneratedMessageV3 message);
}
