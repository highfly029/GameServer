package com.highfly029.core.interfaces;

import com.highfly029.core.world.ChildWorld;

/**
 * @ClassName IRegisterServerChildProtocol
 * @Description 服务器内部child监听协议注册
 * @Author liyunpeng
 **/
public interface IRegisterServerChildProtocol {
    void registerChildProtocol(ChildWorld childWorld) throws Exception;
}
