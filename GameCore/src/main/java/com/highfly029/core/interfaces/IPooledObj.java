package com.highfly029.core.interfaces;

/**
 * @ClassName PooledObj
 * @Description 池化对象接口
 * @Author liyunpeng
 **/
public interface IPooledObj {

    /**
     * 初始化
     */
    void init();

    /**
     * 释放对象
     */
    void release();
}
