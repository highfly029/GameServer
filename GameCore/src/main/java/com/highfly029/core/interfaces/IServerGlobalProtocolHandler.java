package com.highfly029.core.interfaces;

import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.session.Session;

/**
 * @ClassName IServerGlobalProtocolHandler
 * @Description 服务器内部global协议处理
 * @Author liyunpeng
 **/
public interface IServerGlobalProtocolHandler {
    void onServerProtocolHandler(Session session, int ptCode, PbPacket.TcpPacket packet) throws InvalidProtocolBufferException;
}
