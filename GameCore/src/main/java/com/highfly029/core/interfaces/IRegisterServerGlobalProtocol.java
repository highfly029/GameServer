package com.highfly029.core.interfaces;

import com.highfly029.core.world.GlobalWorld;

/**
 * @ClassName IRegisterServerGlobalProtocol
 * @Description 服务器内部global监听协议注册
 * @Author liyunpeng
 **/
public interface IRegisterServerGlobalProtocol {
    void registerGlobalProtocol(GlobalWorld globalWorld) throws Exception;
}
