package com.highfly029.core.interfaces;


import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;

/**
 * @ClassName MySQLCallback
 * @Description
 * @Author liyunpeng
 **/
public interface IMySQLCallback extends ICallback {
    /**
     * @param isSuccess
     * @param successRows 成功返回的消息
     * @param errorMsg    失败消息
     */
    void onMysqlCallback(boolean isSuccess, RowSet<Row> successRows, String errorMsg);
}
