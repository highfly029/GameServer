package com.highfly029.core.interfaces;

/**
 * @ClassName IHttpClientCallback
 * @Description IHttpClientCallback
 * @Author liyunpeng
 **/
public interface IHttpClientCallback {
    /**
     * httpClient回调
     *
     * @param isSuccess
     * @param errorMsg
     */
    void onHttpClientCallback(boolean isSuccess, String body, String errorMsg);
}
