package com.highfly029.core.interfaces;

import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName IGlobalConfig
 * @Description 全局配置接口
 * @Author liyunpeng
 **/
public interface IGlobalConfig {
    /**
     * 配置数据检查
     */
    void check(boolean isHotfix);

    /**
     * 配置数据加载
     */
    void load();

    /**
     * 配置数据重新加载
     */
    void reload();
}
