package com.highfly029.core.net.udp;

import com.highfly029.core.net.AbstractServer;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.util.concurrent.DefaultThreadFactory;

/**
 * @ClassName UdpServer
 * @Description udp
 * @Author liyunpeng
 **/
public class UdpServer extends AbstractServer {

    public void run() throws InterruptedException {
        boss = new NioEventLoopGroup(bossThreadNum, new DefaultThreadFactory(name + BOSS_THREAD_NAME));
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.channel(NioDatagramChannel.class);
        bootstrap.option(ChannelOption.SO_BROADCAST, true);
        bootstrap.group(boss);
        bootstrap.handler(new ChannelInitializer<NioDatagramChannel>() {
            @Override
            protected void initChannel(NioDatagramChannel ch) throws Exception {
                ch.pipeline().addLast("udpDecoder", new UdpFrameDecoder());
                ch.pipeline().addLast("udpEncoder", new UdpFrameEncoder());
                ch.pipeline().addLast("logic", new UdpServerHandler());
            }
        });
        UdpServer udpServer = this;
        bootstrap.bind(port).addListener((ChannelFutureListener) future -> {
            if (future.isSuccess()) {
                LoggerTool.systemLogger.info("{} udp server bind {} success", name, port);
                WorldTool.addLogicEvent(LogicEventType.UDP_SERVER_EVENT_TYPE, LogicEventAction.PORT_BIND_SUCCESS, udpServer);
            } else {
                LoggerTool.systemLogger.error("{} udp server bind {} fail", name, port);
            }
        });
        LoggerTool.systemLogger.info("{} run...", this.toString());
    }

    @Override
    public String toString() {
        return "UdpServer{" +
                "name='" + name + '\'' +
                ", bossThreadNum=" + bossThreadNum +
                ", port=" + port +
                '}';
    }
}
