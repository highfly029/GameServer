package com.highfly029.core.net.http;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.highfly029.core.tool.LoggerTool;
import com.highfly029.utils.ConfigPropUtils;

/**
 * @ClassName SSLContextFactory
 * @Description 目前实现单向认证
 * @Author liyunpeng
 **/
public class SSLContextFactory {
    private static final String PROTOCOL = "TLS";
    private static SSLContext SERVER_CONTEXT;
    private static SSLContext CLIENT_CONTEXT;

    public static SSLContext getServerContext() {
        if (SERVER_CONTEXT == null) {
            FileInputStream inputStream = null;
            try {
                char[] passArray = ConfigPropUtils.getValue("HTTP_SSL_SERVER_JKS_PWD").toCharArray();
                KeyStore ks = KeyStore.getInstance("JKS");
                //加载keytool 生成的文件
                inputStream = new FileInputStream(ConfigPropUtils.getConfigPath() + ConfigPropUtils.getValue("HTTP_SSL_SERVER_JKS"));
                ks.load(inputStream, passArray);
                KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                kmf.init(ks, passArray);
                SERVER_CONTEXT = SSLContext.getInstance(PROTOCOL);
                SERVER_CONTEXT.init(kmf.getKeyManagers(), null, null);
            } catch (Exception e) {
                LoggerTool.systemLogger.error(e.getMessage(), e);
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        LoggerTool.systemLogger.error(e.getMessage(), e);
                    }

                }
            }
        }
        return SERVER_CONTEXT;
    }

    public static SSLContext getClientSslContext() throws Exception {
        if (CLIENT_CONTEXT == null) {
            try {
                //忽略证书验证
                X509TrustManager tm = new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                };
                CLIENT_CONTEXT = SSLContext.getInstance(PROTOCOL);
                CLIENT_CONTEXT.init(null, new TrustManager[]{tm}, null);
            } catch (Exception e) {
                LoggerTool.systemLogger.error(e.getMessage(), e);
            }
        }
        return CLIENT_CONTEXT;
    }
}
