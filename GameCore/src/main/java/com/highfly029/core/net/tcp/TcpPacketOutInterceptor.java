package com.highfly029.core.net.tcp;

import com.google.protobuf.ByteString;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.plugins.statistics.StatisticsEnum;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.PacketParserTool;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;

/**
 * @ClassName TcpServerOutLogger
 * @Description 协议包数据流出拦截器
 * @Author liyunpeng
 **/
public class TcpPacketOutInterceptor extends ChannelOutboundHandlerAdapter {
    /**
     * 是否是服务器之间的内部通讯
     */
    private final boolean isInnerPacket;
    /**
     * 是否打印日志
     */
    private final boolean isLogger;
    /**
     * 是否打印详细日志
     */
    private final boolean isDumpPacketDetail;
    /**
     * 是否统计数据
     */
    private final boolean isStatistics;

    TcpPacketOutInterceptor(boolean isInnerPacket, boolean isLogger, boolean isDumpPacketDetail, boolean isStatistics) {
        this.isInnerPacket = isInnerPacket;
        this.isLogger = isLogger;
        this.isDumpPacketDetail = isDumpPacketDetail;
        this.isStatistics = isStatistics;
    }


    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (isInnerPacket) {
            PbPacket.TcpPacket packet = (PbPacket.TcpPacket) msg;
            ByteString byteString = packet.getData();
            int ptCode = packet.getPtCode();
            if (isLogger && !PacketParserTool.ignoreDump(ptCode)) {
                IPacketParser packetParser = PacketParserTool.getPacketParser(ptCode);
                LoggerTool.packetLogger.info("tcp-inner-out:{}_{}_{}{}",
                        packetParser.getName(),
                        ctx.channel().attr(ChannelAttribute.playerID).get(),
                        ctx.channel().id().asShortText(),
                        PacketParserTool.getPacketDump(packetParser, byteString, isDumpPacketDetail));
            }
            if (isStatistics) {
                StatisticsEnum.MSG_LENGTH.getPlugin().record(ptCode, byteString.size());
            }

        } else {
            if (ConfigConst.tcpPacketCombine) {
                PbPacket.TcpPacketCombine packets = (PbPacket.TcpPacketCombine) msg;
                for (int i = 0, len = packets.getPacketsCount(); i < len; i++) {
                    PbPacket.TcpPacket packet = packets.getPackets(i);
                    ByteString byteString = packet.getData();
                    int ptCode = packet.getPtCode();
                    if (isLogger && !PacketParserTool.ignoreDump(ptCode)) {
                        IPacketParser packetParser = PacketParserTool.getPacketParser(ptCode);
                        LoggerTool.packetLogger.info("tcp-outer-out:{}_{}_{}{}",
                                packetParser.getName(),
                                ctx.channel().attr(ChannelAttribute.playerID).get(),
                                ctx.channel().id().asShortText(),
                                PacketParserTool.getPacketDump(packetParser, byteString, isDumpPacketDetail));
                    }
                    if (isStatistics) {
                        StatisticsEnum.MSG_LENGTH.getPlugin().record(ptCode, byteString.size());
                    }
                }
            } else {
                PbPacket.TcpPacket packet = (PbPacket.TcpPacket) msg;
                ByteString byteString = packet.getData();
                int ptCode = packet.getPtCode();
                if (isLogger && !PacketParserTool.ignoreDump(ptCode)) {
                    IPacketParser packetParser = PacketParserTool.getPacketParser(ptCode);
                    LoggerTool.packetLogger.info("tcp-outer-out:{}_{}_{}{}",
                            packetParser.getName(),
                            ctx.channel().attr(ChannelAttribute.playerID).get(),
                            ctx.channel().id().asShortText(),
                            PacketParserTool.getPacketDump(packetParser, byteString, isDumpPacketDetail));
                }
                if (isStatistics) {
                    StatisticsEnum.MSG_LENGTH.getPlugin().record(ptCode, byteString.size());
                }
            }
        }
        super.write(ctx, msg, promise);
    }
}
