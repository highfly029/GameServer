package com.highfly029.core.net;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;

/**
 * @ClassName AbstractClient
 * @Description netty客户端
 * @Author liyunpeng
 **/
public abstract class AbstractClient extends AbstractNet {
    private static final int MAX_RETRY = 5;
    protected EventLoopGroup loop;
    //最大连接重试次数 >0 有限次数 ==0无限次数
    protected int maxRetry;
    //当前连接重试次数
    protected int retry;
    protected String host;
    protected Bootstrap bootstrap;
    protected int timeOutMillis;
    protected int workerThreadNum;
    protected Channel channel;


    public void setWorkerThreadNum(int workerThreadNum) {
        this.workerThreadNum = workerThreadNum;
    }

    public Channel getChannel() {
        if (channel != null && channel.isWritable()) {
            return channel;
        }
        return null;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public int getMaxRetry() {
        return this.maxRetry;
    }

    public void setMaxRetry(int maxRetry) {
        this.maxRetry = maxRetry;
        this.retry = this.maxRetry;
    }

    public int getRetry() {
        return retry;
    }

    public void setRetry(int retry) {
        this.retry = retry;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getHost() {
        return host;
    }

    public void setTimeOutMillis(int timeOutMillis) {
        this.timeOutMillis = timeOutMillis;
    }

    public void shutdown() {
        if (null != loop) {
            loop.shutdownGracefully();
        }
    }

}
