package com.highfly029.core.net.udp;

import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

/**
 * @ClassName UdpClientHandler
 * @Description UdpClientHandler
 * @Author liyunpeng
 **/
public class UdpClientHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
//        LoggerTool.systemLogger.info("udp client channelActive");
        WorldTool.addLogicEvent(LogicEventType.UDP_CLIENT_EVENT_TYPE, LogicEventAction.CHANNEL_ACTIVE, ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
//        LoggerTool.systemLogger.info("udp client channelInactive");
        WorldTool.addLogicEvent(LogicEventType.UDP_CLIENT_EVENT_TYPE, LogicEventAction.CHANNEL_INACTIVE, ctx.channel());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        try {
            WorldTool.addLogicEvent(LogicEventType.UDP_CLIENT_EVENT_TYPE, LogicEventAction.CHANNEL_READ, msg, ctx.channel());
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
//        LoggerTool.systemLogger.error(cause.getMessage());
        ctx.close();
    }
}
