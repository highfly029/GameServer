package com.highfly029.core.net;

import java.net.InetSocketAddress;

import io.netty.channel.Channel;

/**
 * @ClassName NetUtils
 * @Description 网络工具类
 * @Author liyunpeng
 **/
public final class NetUtils {
    public static final String favicon = "/favicon.ico";

    public static String getIP(Channel channel) {
        String IP = "null";
        if (channel != null) {
            InetSocketAddress socketAddress = (InetSocketAddress) channel.remoteAddress();
            if (socketAddress != null) {
//                IP = socketAddress.getAddress().getHostAddress();
                IP = socketAddress.getHostString();
            }
        }
        return IP;
    }

    public static int getPort(Channel channel) {
        int port = 0;
        if (channel != null) {
            InetSocketAddress socketAddress = (InetSocketAddress) channel.remoteAddress();
            if (socketAddress != null) {
                port = socketAddress.getPort();
            }
        }
        return port;
    }
}
