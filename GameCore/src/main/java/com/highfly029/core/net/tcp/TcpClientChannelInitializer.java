package com.highfly029.core.net.tcp;

import java.util.concurrent.TimeUnit;

import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.packet.PbPacket;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * @ClassName TcpClientChannelInitializer
 * @Description tcp
 * @Author liyunpeng
 **/
public class TcpClientChannelInitializer extends ChannelInitializer<Channel> {
    private final TcpClient tcpClient;
    private final long readerIdleTime;
    private final long writerIdleTime;

    TcpClientChannelInitializer(TcpClient tcpClient, long readerIdleTime, long writerIdleTime) {
        this.tcpClient = tcpClient;
        this.readerIdleTime = readerIdleTime;
        this.writerIdleTime = writerIdleTime;
    }

    @Override
    protected void initChannel(Channel ch) throws Exception {
        ch.pipeline().addLast("heart", new IdleStateHandler(readerIdleTime, writerIdleTime, 0, TimeUnit.SECONDS));
        ch.pipeline().addLast("customDecoder", new CustomizeFrameDecoder());
        ch.pipeline().addLast("customEncoder", new CustomizeFrameEncoder());
        if (tcpClient.isInnerServer()) {
            ch.pipeline().addLast("protobufDecoder", new ProtobufDecoder(PbPacket.TcpPacket.getDefaultInstance()));
        } else {
            if (ConfigConst.tcpPacketCombine) {
                ch.pipeline().addLast("protobufDecoder", new ProtobufDecoder(PbPacket.TcpPacketCombine.getDefaultInstance()));
            } else {
                ch.pipeline().addLast("protobufDecoder", new ProtobufDecoder(PbPacket.TcpPacket.getDefaultInstance()));
            }
        }

        ch.pipeline().addLast("protobufEncoder", new ProtobufEncoder());
        if (tcpClient.isIntercept()) {
            ch.pipeline().addLast("outLogger", new TcpPacketOutInterceptor(tcpClient.isInnerServer(), tcpClient.isPacketLogger(), tcpClient.isDumpPacketDetail(), tcpClient.isPacketStatistics()));
            ch.pipeline().addLast("inLogger", new TcpPacketInInterceptor(tcpClient.isInnerServer(), tcpClient.isPacketLogger(), tcpClient.isDumpPacketDetail(), tcpClient.isPacketStatistics()));
        }
        ch.pipeline().addLast("logicHandler", new TcpClientHandler(tcpClient));
    }
}
