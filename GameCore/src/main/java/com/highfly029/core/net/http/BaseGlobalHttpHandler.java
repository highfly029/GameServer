package com.highfly029.core.net.http;

import com.highfly029.core.interfaces.IHttpHandler;

/**
 * @ClassName BaseGlobalHttpHandler
 * @Description BaseGlobalHttpHandler
 * @Author liyunpeng
 **/
public abstract class BaseGlobalHttpHandler implements IHttpHandler {
    @Override
    public boolean isGlobal() {
        return true;
    }
}
