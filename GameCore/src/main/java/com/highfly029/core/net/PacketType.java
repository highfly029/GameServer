package com.highfly029.core.net;

/**
 * @ClassName PacketType
 * @Description 包类型
 * @Author liyunpeng
 **/
public class PacketType {
    public static final byte Scene = 1;
    public static final byte Center = 2;
}
