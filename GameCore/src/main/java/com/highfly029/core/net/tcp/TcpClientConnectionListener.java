package com.highfly029.core.net.tcp;

import java.util.concurrent.TimeUnit;

import com.highfly029.core.tool.LoggerTool;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.EventLoopGroup;

/**
 * @ClassName TcpClientConnectionListener
 * @Description tcp
 * @Author liyunpeng
 **/
public class TcpClientConnectionListener implements ChannelFutureListener {
    private final TcpClient tcpClient;

    public TcpClientConnectionListener(TcpClient tcpClient) {
        this.tcpClient = tcpClient;
    }

    @Override
    public void operationComplete(ChannelFuture future) throws Exception {
        if (future.isSuccess()) {
            LoggerTool.systemLogger.info("{} connect success!", tcpClient);
            tcpClient.setRetry(tcpClient.getMaxRetry());
        } else {
            if (tcpClient.getMaxRetry() > 0) {
                //停止重连
                if (tcpClient.getRetry() <= 0) {
                    LoggerTool.systemLogger.error("retry cnt is zero, over {}", tcpClient);
                    return;
                }
                // 第几次重连
                int order = (tcpClient.getMaxRetry() - tcpClient.getRetry()) + 1;
                // 本次重连的间隔
                int delay = 1 << order;
                tcpClient.setRetry(tcpClient.getRetry() - 1);
                LoggerTool.systemLogger.error("connect fail,and begin the " + order + " times ……");
                final EventLoopGroup loop = future.channel().eventLoop();
                loop.schedule(() -> tcpClient.connect(), delay, TimeUnit.SECONDS);
            } else {
                //无限重连
                LoggerTool.systemLogger.error("connect fail,and reconnect");
                final EventLoopGroup loop = future.channel().eventLoop();
                loop.schedule(() -> tcpClient.connect(), 1, TimeUnit.SECONDS);
            }
        }
    }
}
