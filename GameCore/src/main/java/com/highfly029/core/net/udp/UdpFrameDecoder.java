package com.highfly029.core.net.udp;

import java.util.List;

import com.google.protobuf.ByteString;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.utils.CompressUtils;
import com.highfly029.utils.EncryptUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageDecoder;

/**
 * @ClassName UdpFrameDecoder
 * @Description udp
 * @Author liyunpeng
 **/
public class UdpFrameDecoder extends MessageToMessageDecoder<DatagramPacket> {
    private static final short magicNum = 0x4321;

    @Override
    protected void decode(ChannelHandlerContext ctx, DatagramPacket msg, List<Object> out) throws Exception {
        ByteBuf data = msg.content();
        short magic = data.readShortLE();
        if (magic != magicNum) {
//            LoggerTool.systemLogger.error("magic num is error magic={}", magic);
            ctx.close();
            throw new IllegalArgumentException("magic is error");
        }
        byte version = data.readByte();
        byte type = data.readByte();
        byte encrypt = data.readByte();
        byte compress = data.readByte();
        int bodyLength = data.readIntLE();
        if (bodyLength > 1024) {
//            LoggerTool.systemLogger.error("send bodyLength maxsize is 1K,now bodyLength={}", bodyLength);
            ctx.close();
            throw new IllegalArgumentException("encode bodyLength too long");
        }
        byte[] array = new byte[bodyLength];
        data.getBytes(data.readerIndex(), array, 0, bodyLength);
        data.skipBytes(bodyLength);
        if (encrypt == 1) {
            array = EncryptUtils.xor(array);
        }
        if (compress == 1) {
            array = CompressUtils.snappyUncompress(array);
        }
        PbPacket.Udp.Builder builder = PbPacket.Udp.newBuilder();
        builder.setPacket(ByteString.copyFrom(array));
        builder.setUdpHost(msg.sender().getAddress().getHostAddress());
        builder.setUdpPort(msg.sender().getPort());
        out.add(builder.build());
    }
}
