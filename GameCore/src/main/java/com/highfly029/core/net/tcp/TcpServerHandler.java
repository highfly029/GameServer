package com.highfly029.core.net.tcp;

import com.highfly029.core.net.NetUtils;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.ReferenceCountUtil;

/**
 * @ClassName TcpServerHandler
 * @Description tcp
 * @Author liyunpeng
 **/
public class TcpServerHandler extends ChannelInboundHandlerAdapter {
//    public static AtomicLong count = new AtomicLong(0);

    public TcpServerHandler() {
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state() == IdleState.READER_IDLE || event.state() == IdleState.WRITER_IDLE || event.state() == IdleState.ALL_IDLE) {
                LoggerTool.systemLogger.error("tcpServer trigger timeout close IP={}, channel={} state={}", NetUtils.getIP(ctx.channel()), ctx.channel().id().asShortText(), event.state());
                WorldTool.addLogicEvent(LogicEventType.TCP_SERVER_EVENT_TYPE, LogicEventAction.CHANNEL_ALL_IDLE, ctx.channel(), event.state().name());
                ctx.close();
            }
        } else {
            super.userEventTriggered(ctx, evt);
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        try {
            WorldTool.addLogicEvent(LogicEventType.TCP_SERVER_EVENT_TYPE, LogicEventAction.CHANNEL_READ, msg, ctx.channel());
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
//        long cnt = count.incrementAndGet();
//        LoggerTool.systemLogger.info("channelActive all cnt={}---------------id={},IP={}", cnt, ctx.channel().id(), NetUtils.getIP(ctx.channel()));
        WorldTool.addLogicEvent(LogicEventType.TCP_SERVER_EVENT_TYPE, LogicEventAction.CHANNEL_ACTIVE, ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
//        long cnt = count.decrementAndGet();
//        LoggerTool.systemLogger.info("channelInactive all cnt={}  --leave------------- id={},IP={}", cnt, ctx.channel().id(), NetUtils.getIP(ctx.channel()));
        WorldTool.addLogicEvent(LogicEventType.TCP_SERVER_EVENT_TYPE, LogicEventAction.CHANNEL_INACTIVE, ctx.channel());
        ctx.close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        LoggerTool.systemLogger.error("exceptionCaught  leave----------id={},IP={}, cause={}", ctx.channel().id(), NetUtils.getIP(ctx.channel()), cause.getMessage());
        WorldTool.addLogicEvent(LogicEventType.TCP_SERVER_EVENT_TYPE, LogicEventAction.CHANNEL_EXCEPTION, ctx.channel());
        ctx.close();
    }

}
