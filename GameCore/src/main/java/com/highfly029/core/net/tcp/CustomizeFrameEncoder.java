package com.highfly029.core.net.tcp;

import com.highfly029.core.tool.LoggerTool;
import com.highfly029.utils.CompressUtils;
import com.highfly029.utils.EncryptUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class CustomizeFrameEncoder extends MessageToByteEncoder<ByteBuf> {
    /**
     * 魔数
     */
    private static final short MAGIC_NUM = 0x4321;
    /**
     * 消息头字节
     */
    private static final int HEADER_LENGTH = 7;
    /**
     * 是否加密
     */
    private static final byte isEncrypt = 0;
    /**
     * 压缩条件 大于1k
     */
    private static final int COMPRESS_LIMIT = 1024;
    /**
     * 发送单条数据最大长度
     */
    private static final int MAX_LENGTH = 1024 * 1024;

    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf msg, ByteBuf out) throws Exception {
        byte[] array = msg.array();
        byte isCompress = 0;
        if (msg.readableBytes() > COMPRESS_LIMIT) {// >1k 压缩
            isCompress = 1;
            array = CompressUtils.snappyCompress(array);
        }
        if (isEncrypt == 1) { //加密
            array = EncryptUtils.xor(array);
        }
        if (array.length > MAX_LENGTH) {
            LoggerTool.systemLogger.error("send bodyLength is too much than maxsize,now bodyLength={}", array.length);
            ctx.close();
            throw new IllegalArgumentException("encode bodyLength too long");
        }
        out.ensureWritable(HEADER_LENGTH + array.length);
        out.writeShortLE(MAGIC_NUM);
//        out.writeByte(isEncrypt);
        out.writeByte(isCompress);
        out.writeIntLE(array.length);
        out.writeBytes(array);
    }
}
