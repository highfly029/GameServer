package com.highfly029.core.net;

/**
 * @ClassName NetConfigNameConst
 * @Description 网络配置常量
 * @Author liyunpeng
 **/
@SuppressWarnings("ALL")
public interface NetConfigNameConst {
    String NET_PREV = "net";
    String NET_SERVER = "server";
    String NET_CLIENT = "client";
    String NET_TCP = "tcp";
    String NET_UDP = "udp";
    String NET_HTTP = "http";
    String NET_REDIS = "redis";
    String NET_DEFAULT = "default";

    String NET_BACKLOG_NAME = "so_backlog";
    String NET_READBUF_NAME = "so_readbuf";
    String NET_WRITEBUF_NAME = "so_writebuf";
    String NET_BOSS_THREAD_NUM_NAME = "boss_thread_num";
    String NET_WORKER_THREAD_NUM_NAME = "worker_thread_num";
    String NET_EPOLL_MODE_NAME = "epoll_mode";
    String NET_READER_IDLE_TIME_NAME = "reader_idle_time";
    String NET_WRITER_IDLE_TIME_NAME = "writer_idle_time";
    String NET_PORT_NAME = "port";
    String NET_UNIX_DOMAIN_SOCKET_NAME = "unix_domain_socket";
    String NET_HTTP_SSL_NAME = "ssl";
    String NET_HOST_NAME = "host";
    String NET_TIMEOUT_MILLIS = "timeout_millis";
    //client最大重试次数
    String NET_CLIENT_MAX_RETRY = "maxRetry";
    //protobuf 是否是内部服务器
    String INNER_SERVER = "innerServer";
    //client唯一标示
    String NET_CLIENT_IDENTIFIER = "identifier";
    //是否增加拦截器
    String NET_INTERCEPT = "intercept";
    //是否打印packet日志
    String NET_PACKET_LOGGER = "packetLogger";
    //是否打印详细日志
    String NET_PACKET_DUMP_DETAIL = "dumpPacketDetail";
    //是否统计协议包数据
    String NET_PACKET_STATISTICS = "statistics";

}
