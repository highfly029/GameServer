package com.highfly029.core.net.tcp;

import com.highfly029.core.net.AbstractClient;
import com.highfly029.core.tool.LoggerTool;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.concurrent.DefaultThreadFactory;

/**
 * @ClassName TcpClient
 * @Description tcp
 * @Author liyunpeng
 **/
public class TcpClient extends AbstractClient {
    private static final String EVENT_LOOP_NAME = "TcpClient";

    /**
     * 默认不是断线重连
     */
    private boolean isReconnect = false;

    public TcpClient() {

    }

    public boolean isReconnect() {
        return isReconnect;
    }

    public void setReconnect(boolean reconnect) {
        isReconnect = reconnect;
    }

    protected void setRemoteAddress(Bootstrap bootstrap) {
        bootstrap.remoteAddress(host, port);
    }

    protected void setBootstrapChannel(Bootstrap bootstrap) {
        if (isUseEpollModeET()) {
            bootstrap.channel(EpollSocketChannel.class);
        } else {
            bootstrap.channel(NioSocketChannel.class);
        }
    }


    public void connect() {
        if (loop == null) {
            if (isUseEpollModeET()) {
                loop = new EpollEventLoopGroup(workerThreadNum, new DefaultThreadFactory(EVENT_LOOP_NAME));
            } else {
                loop = new NioEventLoopGroup(workerThreadNum, new DefaultThreadFactory(EVENT_LOOP_NAME));
            }
        }
        if (bootstrap == null) {
            bootstrap = new Bootstrap();
            initBootStrap();
        }
        LoggerTool.systemLogger.info("{} connect...", this.toString());
        setRemoteAddress(bootstrap);
        bootstrap.connect().addListener(new TcpClientConnectionListener(this));
    }

    private void initBootStrap() {
        bootstrap.group(loop);
        setBootstrapChannel(bootstrap);
        setOption();
        setHandler();
    }

    protected void setOption() {
        bootstrap.option(ChannelOption.TCP_NODELAY, true);
        bootstrap.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, timeOutMillis);
    }

    protected void setHandler() {
        bootstrap.handler(new TcpClientChannelInitializer(this, readerIdleTime, writerIdleTime));
    }

    @Override
    public String toString() {
        return "TcpClient{" +
                "name='" + name + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", retry=" + retry +
                ", timeOutMillis=" + timeOutMillis +
                ", readerIdleTime=" + readerIdleTime +
                ", writerIdleTime=" + writerIdleTime +
                ", workerThreadNum=" + workerThreadNum +
                ", epollMode='" + epollMode + '\'' +
                '}';
    }
}
