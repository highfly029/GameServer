package com.highfly029.core.net.http;

import com.highfly029.core.net.tcp.TcpServer;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFutureListener;


/**
 * @ClassName HttpServer
 * @Description http
 * @Author liyunpeng
 **/
public class HttpServer extends TcpServer {
    private final boolean isSSL;

    public HttpServer(boolean ssl) {
        this.isSSL = ssl;
    }

    @Override
    public void run() throws Exception {
        setEventLoopGroup();
        ServerBootstrap b = new ServerBootstrap();
        b.group(boss, worker);
        setServerBootstrapChannel(b);
        setOptions(b);
        b.childHandler(new HttpServerChannelInitializer(isSSL, readerIdleTime, writerIdleTime));
        bind(b);
        LoggerTool.systemLogger.info("{} run...", this.toString());
    }

    @Override
    protected void bind(ServerBootstrap serverBootstrap) throws Exception {
        HttpServer httpServer = this;
        serverBootstrap.bind(port).addListener((ChannelFutureListener) future -> {
            if (future.isSuccess()) {
                LoggerTool.systemLogger.info("{} http server bind {} success", name, port);
                WorldTool.addLogicEvent(LogicEventType.HTTP_SERVER_EVENT_TYPE, LogicEventAction.PORT_BIND_SUCCESS, httpServer);
            } else {
                LoggerTool.systemLogger.error("{} http server bind {} fail", name, port);
            }
        });
    }

    @Override
    public String toString() {
        return "HttpServer{" +
                "isSSL=" + isSSL +
                ", name='" + name + '\'' +
                ", bossThreadNum=" + bossThreadNum +
                ", workerThreadNum=" + workerThreadNum +
                ", epollMode='" + epollMode + '\'' +
                ", soBacklogSize=" + soBacklogSize +
                ", soSendBufSize=" + soSendBufSize +
                ", soReceiveBufSize=" + soReceiveBufSize +
                ", readerIdleTime=" + readerIdleTime +
                ", writerIdleTime=" + writerIdleTime +
                ", port=" + port +
                '}';
    }
}
