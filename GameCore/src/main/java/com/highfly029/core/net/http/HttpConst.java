package com.highfly029.core.net.http;

import com.highfly029.utils.ConfigPropUtils;

/**
 * @ClassName HttpConst
 * @Description http
 * @Author liyunpeng
 **/
public class HttpConst {
    /**
     * http路由 如果没有/后缀、自动增加
     */
    public static final String route = ConfigPropUtils.getValue("route").endsWith("/") ? ConfigPropUtils.getValue("route") : ConfigPropUtils.getValue("route") + "/";
    public static final String RESP_FAIL = "fail";
    public static final String RESP_SUCCESS = "success";
    public static final String print = "print";
    public static final String echo = "echo";
    public static final String timeChange = "timeChange";
    public static final String timeReset = "timeReset";
}
