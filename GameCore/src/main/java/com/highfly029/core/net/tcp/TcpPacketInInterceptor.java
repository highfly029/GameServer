package com.highfly029.core.net.tcp;

import com.google.protobuf.ByteString;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.plugins.statistics.StatisticsEnum;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.PacketParserTool;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @ClassName TcpPacketInInterceptor
 * @Description 协议包数据流入拦截器
 * @Author liyunpeng
 **/
public class TcpPacketInInterceptor extends ChannelInboundHandlerAdapter {
    /**
     * 是否是服务器之间的内部通讯
     */
    private final boolean isInnerPacket;
    /**
     * 是否打印日志
     */
    private final boolean isLogger;
    /**
     * 是否打印详细日志
     */
    private final boolean isDumpPacketDetail;
    /**
     * 是否统计数据
     */
    private final boolean isStatistics;

    public TcpPacketInInterceptor(boolean isInnerPacket, boolean isLogger, boolean isDumpPacketDetail, boolean isStatistics) {
        this.isInnerPacket = isInnerPacket;
        this.isLogger = isLogger;
        this.isDumpPacketDetail = isDumpPacketDetail;
        this.isStatistics = isStatistics;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (isInnerPacket) {
            PbPacket.TcpPacket packet = (PbPacket.TcpPacket) msg;
            ByteString byteString = packet.getData();
            int ptCode = packet.getPtCode();
            if (isLogger && !PacketParserTool.ignoreDump(ptCode)) {
                IPacketParser packetParser = PacketParserTool.getPacketParser(ptCode);
                LoggerTool.packetLogger.info("tcp-inner-in:{}_{}_{}{}",
                        packetParser.getName(),
                        ctx.channel().attr(ChannelAttribute.playerID).get(),
                        ctx.channel().id().asShortText(),
                        PacketParserTool.getPacketDump(packetParser, byteString, isDumpPacketDetail));
            }
            if (isStatistics) {
                StatisticsEnum.MSG_LENGTH.getPlugin().record(ptCode, byteString.size());
            }
        } else {
            PbPacket.TcpPacketClient packet = (PbPacket.TcpPacketClient) msg;
            ByteString byteString = packet.getData();
            int ptCode = packet.getPtCode();
            if (isLogger && !PacketParserTool.ignoreDump(ptCode)) {
                IPacketParser packetParser = PacketParserTool.getPacketParser(ptCode);
                LoggerTool.packetLogger.info("tcp-outer-in:{}_{}_{}{}",
                        packetParser.getName(),
                        ctx.channel().attr(ChannelAttribute.playerID).get(),
                        ctx.channel().id().asShortText(),
                        PacketParserTool.getPacketDump(packetParser, byteString, isDumpPacketDetail));
            }
            if (isStatistics) {
                StatisticsEnum.MSG_LENGTH.getPlugin().record(ptCode, byteString.size());
            }
        }
        super.channelRead(ctx, msg);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }
}
