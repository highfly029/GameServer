package com.highfly029.core.net.tcp;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.EventLoopGroup;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;
import io.netty.util.ReferenceCountUtil;

/**
 * @ClassName TcpClientHandler
 * @Description tcp
 * @Author liyunpeng
 **/
public class TcpClientHandler extends ChannelInboundHandlerAdapter {
    private static final AttributeKey<String> CLIENT_NAME = AttributeKey.newInstance("name");
    private static final AtomicLong count = new AtomicLong(0);
    private final TcpClient tcpClient;


    public TcpClientHandler(TcpClient tcpClient) {
        this.tcpClient = tcpClient;
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            byte action;
            if (event.state() == IdleState.WRITER_IDLE) {
                action = LogicEventAction.CHANNEL_WRITER_IDLE;
            } else if (event.state() == IdleState.READER_IDLE) {
                action = LogicEventAction.CHANNEL_READER_IDLE;
            } else {
                action = LogicEventAction.CHANNEL_ALL_IDLE;
            }
            //客户端长时间没有交互、应该主动去发送心跳
            WorldTool.addLogicEvent(LogicEventType.TCP_CLIENT_EVENT_TYPE, action, ctx.channel());

        } else {
            super.userEventTriggered(ctx, evt);
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        Attribute<String> value = ctx.channel().attr(CLIENT_NAME);
        value.set(tcpClient.getName());
        tcpClient.setChannel(ctx.channel());
        long cnt = count.incrementAndGet();
//        LoggerTool.systemLogger.info("channel active send first! cnt={}", cnt);
        WorldTool.addLogicEvent(LogicEventType.TCP_CLIENT_EVENT_TYPE, LogicEventAction.CHANNEL_ACTIVE, tcpClient, ctx.channel());

    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        tcpClient.setChannel(null);
        long cnt = count.decrementAndGet();
//        LoggerTool.systemLogger.info("channel inactive name={} cnt={} isRunning={}", ctx.channel().attr(CLIENT_NAME).get(), cnt, isRunning);


        //TODO 如果在运行状态才会执行重连
        if (true) {
            final EventLoopGroup loop = ctx.channel().eventLoop();
            LoggerTool.systemLogger.error("tcpClient begin reconnect:{}", tcpClient);
            loop.schedule(() -> tcpClient.connect(), 1L, TimeUnit.SECONDS);
        }

        WorldTool.addLogicEvent(LogicEventType.TCP_CLIENT_EVENT_TYPE, LogicEventAction.CHANNEL_INACTIVE, tcpClient, ctx.channel());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        try {
            WorldTool.addLogicEvent(LogicEventType.TCP_CLIENT_EVENT_TYPE, LogicEventAction.CHANNEL_READ, msg, ctx.channel());
        } finally {
            ReferenceCountUtil.release(msg);
        }

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        WorldTool.addLogicEvent(LogicEventType.TCP_CLIENT_EVENT_TYPE, LogicEventAction.CHANNEL_EXCEPTION, ctx.channel());
        ctx.close();
    }
}
