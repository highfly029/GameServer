package com.highfly029.core.net.tcp;

import io.netty.util.AttributeKey;

/**
 * @ClassName ChannelAttribute
 * @Description ChannelAttribute
 * @Author liyunpeng
 **/
public class ChannelAttribute {
    public static final AttributeKey<Long> playerID = AttributeKey.newInstance("uid");
}
