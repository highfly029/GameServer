package com.highfly029.core.net.udp;

import java.net.InetSocketAddress;

import com.highfly029.core.net.AbstractClient;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.util.concurrent.DefaultThreadFactory;

/**
 * @ClassName UdpClient
 * @Description udp
 * @Author liyunpeng
 **/
public class UdpClient extends AbstractClient {
    private static final String EVENT_LOOP_NAME = "UDP_CLIENT";

    public void connect() throws InterruptedException {
        bootstrap = new Bootstrap();
        bootstrap.channel(NioDatagramChannel.class);
        loop = new NioEventLoopGroup(workerThreadNum, new DefaultThreadFactory(EVENT_LOOP_NAME));
        bootstrap.group(loop);
        InetSocketAddress address = new InetSocketAddress(host, port);
        bootstrap.handler(new ChannelInitializer<NioDatagramChannel>() {
            @Override
            protected void initChannel(NioDatagramChannel ch) throws Exception {
                ch.pipeline().addLast("udpDecoder", new UdpFrameDecoder());
                ch.pipeline().addLast("udpEncoder", new UdpFrameEncoder(address));
                ch.pipeline().addLast("logic", new UdpClientHandler());
            }
        });

        UdpClient udpClient = this;
        bootstrap.bind(0).addListener((ChannelFutureListener) future -> {
            if (future.isSuccess()) {
                udpClient.channel = future.channel();
                LoggerTool.systemLogger.info("{} udp client bind {} success", name, port);
                WorldTool.addLogicEvent(LogicEventType.UDP_CLIENT_EVENT_TYPE, LogicEventAction.PORT_BIND_SUCCESS, udpClient);
            } else {
                LoggerTool.systemLogger.error("{} udp client bind {} fail", name, port);
            }
        });
        LoggerTool.systemLogger.info("{} connect...", this.toString());
    }

    @Override
    public String toString() {
        return "UdpClient{" +
                "name='" + name + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                '}';
    }
}
