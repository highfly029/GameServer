package com.highfly029.core.net.tcp;

import java.util.concurrent.TimeUnit;

import com.highfly029.core.net.AbstractServer;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.DefaultThreadFactory;

/**
 * @ClassName TcpServer
 * @Description tcp
 * @Author liyunpeng
 **/
public class TcpServer extends AbstractServer {
    public TcpServer() {
    }

    public void run() throws Exception {
        setEventLoopGroup();
        ServerBootstrap b = new ServerBootstrap();
        b.group(boss, worker);
        setServerBootstrapChannel(b);
        setOptions(b);
        b.childHandler(new ChannelInitializer<>() {
            protected void initChannel(Channel ch) {
                ch.pipeline().addLast("heart", new IdleStateHandler(readerIdleTime, writerIdleTime, 0, TimeUnit.SECONDS));
                ch.pipeline().addLast("customDecoder", new CustomizeFrameDecoder());
                ch.pipeline().addLast("customEncoder", new CustomizeFrameEncoder());
                if (innerServer) {
                    ch.pipeline().addLast("protobufDecoder", new ProtobufDecoder(PbPacket.TcpPacket.getDefaultInstance()));
                } else {
                    ch.pipeline().addLast("protobufDecoder", new ProtobufDecoder(PbPacket.TcpPacketClient.getDefaultInstance()));
                }
                ch.pipeline().addLast("protobufEncoder", new ProtobufEncoder());
                if (isIntercept()) {
                    ch.pipeline().addLast("outLogger", new TcpPacketOutInterceptor(innerServer, packetLogger, dumpPacketDetail, packetStatistics));
                    ch.pipeline().addLast("inLogger", new TcpPacketInInterceptor(innerServer, packetLogger, dumpPacketDetail, packetStatistics));
                }
                ch.pipeline().addLast("logic", new TcpServerHandler());
            }
        });
        bind(b);
        LoggerTool.systemLogger.info("{} run...", this);
    }

    protected void setOptions(ServerBootstrap serverBootstrap) {
        serverBootstrap.option(ChannelOption.SO_BACKLOG, soBacklogSize);
        serverBootstrap.option(ChannelOption.SO_REUSEADDR, true);

        serverBootstrap.childOption(ChannelOption.TCP_NODELAY, true);
        serverBootstrap.childOption(ChannelOption.SO_RCVBUF, soReceiveBufSize);
        serverBootstrap.childOption(ChannelOption.SO_SNDBUF, soSendBufSize);
    }

    protected void setEventLoopGroup() throws Exception {
        if (null != boss || null != worker) {
            throw new Exception("server has running");
        }
        if (isUseEpollModeET()) {
            boss = new EpollEventLoopGroup(bossThreadNum, new DefaultThreadFactory(name + BOSS_THREAD_NAME));
            worker = new EpollEventLoopGroup(workerThreadNum, new DefaultThreadFactory(name + WORKER_THREAD_NAME));
        } else {
            boss = new NioEventLoopGroup(bossThreadNum, new DefaultThreadFactory(name + BOSS_THREAD_NAME));
            worker = new NioEventLoopGroup(workerThreadNum, new DefaultThreadFactory(name + WORKER_THREAD_NAME));
        }
        if (null == boss || null == worker) {
            throw new Exception("eventLoopGroup init error");
        }
    }

    protected void bind(ServerBootstrap serverBootstrap) throws Exception {
        TcpServer tcpServer = this;
        serverBootstrap.bind(port).addListener((ChannelFutureListener) future -> {
            if (future.isSuccess()) {
                LoggerTool.systemLogger.info("{} tcp server bind {} success", name, port);
                WorldTool.addLogicEvent(LogicEventType.TCP_SERVER_EVENT_TYPE, LogicEventAction.PORT_BIND_SUCCESS, tcpServer);
            } else {
                LoggerTool.systemLogger.error("{} tcp server bind {} fail", name, port);
            }
        });
    }

    protected void setServerBootstrapChannel(ServerBootstrap serverBootstrap) {
        if (isUseEpollModeET()) {
            serverBootstrap.channel(EpollServerSocketChannel.class);
        } else {
            serverBootstrap.channel(NioServerSocketChannel.class);
        }
    }

    @Override
    public String toString() {
        return "TcpServer{" +
                "name='" + name + '\'' +
                ", bossThreadNum=" + bossThreadNum +
                ", workerThreadNum=" + workerThreadNum +
                ", epollMode='" + epollMode + '\'' +
                ", soBacklogSize=" + soBacklogSize +
                ", soSendBufSize=" + soSendBufSize +
                ", soReceiveBufSize=" + soReceiveBufSize +
                ", readerIdleTime=" + readerIdleTime +
                ", writerIdleTime=" + writerIdleTime +
                ", port=" + port +
                '}';
    }
}
