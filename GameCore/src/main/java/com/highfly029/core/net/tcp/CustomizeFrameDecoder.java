package com.highfly029.core.net.tcp;

import java.util.List;

import com.highfly029.utils.CompressUtils;
import com.highfly029.utils.EncryptUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public class CustomizeFrameDecoder extends ByteToMessageDecoder {
    /**
     * 单次解析次数
     */
    private static final int MAX_COUNT = 4;//一次解析多个
    /**
     * 魔数
     */
    private static final short MAGIC_NUM = 0x4321;
    /**
     * 消息头字节
     */
    private static final int HEADER_LENGTH = 7;
    /**
     * 发送单条数据最大长度
     */
    private static final int MAX_LENGTH = 1024 * 1024;

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        int count = MAX_COUNT;
        while (count-- > 0) {
            if (in.readableBytes() > HEADER_LENGTH) {
                in.markReaderIndex();
                short magic = in.readShortLE();
                if (magic != MAGIC_NUM) {
//                    LoggerTool.systemLogger.error("magic num is error magic={}", magic);
                    ctx.close();
                    throw new IllegalArgumentException("magic is error");
                }
                byte encrypt = 0;//in.readByte();
                byte compress = in.readByte();
                int bodyLength = in.readIntLE();
                if (bodyLength > MAX_LENGTH) {
//                    LoggerTool.systemLogger.error("received maxSize is 1M ,the bodyLength={}", bodyLength);
                    ctx.close();
                    throw new IllegalArgumentException("decode bodyLength too long");
                }
                if (in.readableBytes() >= bodyLength) {
                    //不改动byte，使用直接内存
                    if (encrypt == 0 && compress == 0) {
                        out.add(in.readRetainedSlice(bodyLength));
                    } else {
                        byte[] array = new byte[bodyLength];
                        in.getBytes(in.readerIndex(), array, 0, bodyLength);
                        in.skipBytes(bodyLength);
                        if (encrypt == 1) {
                            array = EncryptUtils.xor(array);
                        }
                        if (compress == 1) {
                            array = CompressUtils.snappyUncompress(array);
                        }
                        ByteBuf byteBuf = ctx.alloc().heapBuffer(array.length);
                        byteBuf.writeBytes(array);
                        out.add(byteBuf);
                    }
                } else {
                    in.resetReaderIndex();
                    return;
                }
            } else {
                return;
            }
        }
    }
}
