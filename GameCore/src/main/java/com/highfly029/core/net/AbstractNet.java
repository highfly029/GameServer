package com.highfly029.core.net;

import com.highfly029.utils.SysUtils;

/**
 * @ClassName AbstractNet
 * @Description AbstractNet
 * @Author liyunpeng
 **/
public abstract class AbstractNet {
    private static final String ET = "ET";

    protected String name;
    /**
     * 端口
     */
    protected int port;
    /**
     * 是否使用epoll模式
     */
    protected String epollMode;
    /**
     * 读超时时间
     */
    protected int readerIdleTime;
    /**
     * 写超时时间
     */
    protected int writerIdleTime;
    /**
     * 是否是内部服务器
     */
    protected boolean innerServer;
    /**
     * 是否添加拦截器拦截
     */
    protected boolean intercept;
    /**
     * 拦截器中是否打印协议包日志
     */
    protected boolean packetLogger;
    /**
     * 拦截器中是否打印详细日志
     */
    protected boolean dumpPacketDetail;
    /**
     * 拦截器中是否统计协议包数据
     */
    protected boolean packetStatistics;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public void setEpollMode(String epollMode) {
        this.epollMode = epollMode;
    }

    public void setReaderIdleTime(int readerIdleTime) {
        this.readerIdleTime = readerIdleTime;
    }

    public void setWriterIdleTime(int writerIdleTime) {
        this.writerIdleTime = writerIdleTime;
    }

    public void setInnerServer(boolean innerServer) {
        this.innerServer = innerServer;
    }

    public boolean isInnerServer() {
        return innerServer;
    }

    public void setPacketLogger(boolean packetLogger) {
        this.packetLogger = packetLogger;
    }

    public boolean isPacketLogger() {
        return packetLogger;
    }

    public boolean isDumpPacketDetail() {
        return dumpPacketDetail;
    }

    public void setDumpPacketDetail(boolean dumpPacketDetail) {
        this.dumpPacketDetail = dumpPacketDetail;
    }

    public void setPacketStatistics(boolean packetStatistics) {
        this.packetStatistics = packetStatistics;
    }

    public boolean isPacketStatistics() {
        return packetStatistics;
    }

    public boolean isIntercept() {
        return intercept;
    }

    public void setIntercept(boolean intercept) {
        this.intercept = intercept;
    }

    public boolean isUseEpollModeET() {
        return epollMode != null && epollMode.startsWith(ET) && SysUtils.isLinux();
    }

}
