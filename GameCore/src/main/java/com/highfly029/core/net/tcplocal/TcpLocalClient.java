package com.highfly029.core.net.tcplocal;

import java.net.SocketAddress;

import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.tool.LoggerTool;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.epoll.EpollDomainSocketChannel;
import io.netty.channel.unix.DomainSocketAddress;

/**
 * @ClassName TcpLocalClient
 * @Description tcp local
 * @Author liyunpeng
 **/
public class TcpLocalClient extends TcpClient {
    private String unixDomainSocket;

    public TcpLocalClient setUnixDomainSocket(String unixDomainSocket) {
        this.unixDomainSocket = unixDomainSocket;
        return this;
    }

    @Override
    protected void setBootstrapChannel(Bootstrap bootstrap) {
        if (isUseUnixDomainSocket()) {
            bootstrap.channel(EpollDomainSocketChannel.class);
            LoggerTool.systemLogger.info("{} channel use unixDomainSocket", name);
        } else {
            LoggerTool.systemLogger.info("{} channel use unixDomainSocket error", name);
        }
    }

    @Override
    protected void setRemoteAddress(Bootstrap bootstrap) {
        if (isUseUnixDomainSocket()) {
            SocketAddress address = new DomainSocketAddress(unixDomainSocket);
            LoggerTool.systemLogger.info("UnixDomainSocket address ={}", unixDomainSocket);
            bootstrap.remoteAddress(address);
        } else {
            LoggerTool.systemLogger.info("UnixDomainSocket address error");
        }
    }

    public boolean isUseUnixDomainSocket() {
        return isUseEpollModeET() && unixDomainSocket != null && !unixDomainSocket.equals("");
    }

    @Override
    public String toString() {
        return "TcpLocalClient{" +
                "unixDomainSocket='" + unixDomainSocket + '\'' +
                ", name='" + name + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", retry=" + retry +
                ", timeOutMillis=" + timeOutMillis +
                ", readerIdleTime=" + readerIdleTime +
                ", writerIdleTime=" + writerIdleTime +
                ", workerThreadNum=" + workerThreadNum +
                ", epollMode='" + epollMode + '\'' +
                '}';
    }
}
