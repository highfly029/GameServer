package com.highfly029.core.net.tcplocal;

import java.net.SocketAddress;

import com.highfly029.core.net.tcp.TcpServer;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.epoll.EpollServerDomainSocketChannel;
import io.netty.channel.unix.DomainSocketAddress;

/**
 * @ClassName TcpLocalServer
 * @Description tcp local
 * @Author liyunpeng
 **/
public class TcpLocalServer extends TcpServer {
    private String unixDomainSocket;

    public TcpLocalServer setUnixDomainSocket(String unixDomainSocket) {
        this.unixDomainSocket = unixDomainSocket;
        return this;
    }

    @Override
    protected void bind(ServerBootstrap serverBootstrap) throws Exception {
        if (isUseUnixDomainSocket()) {
            TcpLocalServer tcpLocalServer = this;
            SocketAddress socketAddress = new DomainSocketAddress(unixDomainSocket);
            serverBootstrap.bind(socketAddress).addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) {
                    if (future.isSuccess()) {
                        LoggerTool.systemLogger.info("{} Unix DomainSocket bind success {}", name, unixDomainSocket);
                        WorldTool.addLogicEvent(LogicEventType.TCP_SERVER_EVENT_TYPE, LogicEventAction.PORT_BIND_SUCCESS, tcpLocalServer);
                    } else {
                        LoggerTool.systemLogger.info("{} Unix DomainSocket bind fail {}", name, unixDomainSocket);
                    }

                }
            });
        } else {
            LoggerTool.systemLogger.info("{} Unix DomainSocket bind error ", name);
        }
    }

    @Override
    protected void setServerBootstrapChannel(ServerBootstrap serverBootstrap) {
        if (isUseUnixDomainSocket()) {
            serverBootstrap.channel(EpollServerDomainSocketChannel.class);
            LoggerTool.systemLogger.info("{} channel use Unix DomainSocket {}", name, unixDomainSocket);
        } else {
            LoggerTool.systemLogger.info("{} channel use Unix DomainSocket error", name);
        }
    }

    public boolean isUseUnixDomainSocket() {
        return isUseEpollModeET() && unixDomainSocket != null && !unixDomainSocket.equals("");
    }

    @Override
    public String toString() {
        return "TcpLocalServer{" +
                "unixDomainSocket='" + unixDomainSocket + '\'' +
                ", name='" + name + '\'' +
                ", bossThreadNum=" + bossThreadNum +
                ", workerThreadNum=" + workerThreadNum +
                ", epollMode='" + epollMode + '\'' +
                ", soBacklogSize=" + soBacklogSize +
                ", soSendBufSize=" + soSendBufSize +
                ", soReceiveBufSize=" + soReceiveBufSize +
                ", readerIdleTime=" + readerIdleTime +
                ", writerIdleTime=" + writerIdleTime +
                ", port=" + port +
                '}';
    }
}
