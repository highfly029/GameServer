package com.highfly029.core.net.gate;

import java.util.List;

import com.highfly029.core.net.PacketType;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

/**
 * @ClassName GateFrameDecoder
 * @Description 网关解码器
 * @Author liyunpeng
 **/
public class GateFrameDecoder extends ByteToMessageDecoder {
    public static final int MAX_COUNT = 5;//一次解析多个
    public static final short magicNum = 0x4321;

    public GateFrameDecoder() {

    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        int count = MAX_COUNT;
        while (count-- > 0) {
            if (in.readableBytes() > 10) {
                in.markReaderIndex();
                short magic = in.readShortLE();
                if (magic != magicNum) {
//                    LoggerTool.systemLogger.error("magic num is error magic={}", magic);
                    ctx.close();
                    throw new IllegalArgumentException("magic is error");
                }
                byte version = in.readByte();
                byte type = in.readByte();
                byte encrypt = in.readByte();
                byte compress = in.readByte();
                int bodyLength = in.readIntLE();
                if (bodyLength > 1024 * 1024) {
//                    LoggerTool.systemLogger.error("received maxSize is 1M ,the bodyLength={}", bodyLength);
                    ctx.close();
                    throw new IllegalArgumentException("decode bodyLength too long");
                }
                if (in.readableBytes() >= bodyLength) {
                    //解析成pb再转发还是直接转发
                    if (type == PacketType.Scene) {

                    } else if (type == PacketType.Center) {

                    }
                }
            } else {
                return;
            }
        }
    }
}
