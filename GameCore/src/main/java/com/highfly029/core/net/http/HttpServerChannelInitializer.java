package com.highfly029.core.net.http;

import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLEngine;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.http.HttpContentCompressor;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * @ClassName HttpServerChannelInitializer
 * @Description http
 * @Author liyunpeng
 **/
public class HttpServerChannelInitializer extends ChannelInitializer<Channel> {
    private final boolean isSSL;
    private final long readerIdleTime;
    private final long writerIdleTime;

    public HttpServerChannelInitializer(boolean ssl, long readerIdleTime, long writerIdleTime) {
        this.isSSL = ssl;
        this.readerIdleTime = readerIdleTime;
        this.writerIdleTime = writerIdleTime;
    }

    @Override
    protected void initChannel(Channel ch) throws Exception {
        if (isSSL) {
            SSLEngine sslEngine = SSLContextFactory.getServerContext().createSSLEngine();
            sslEngine.setUseClientMode(false);
            ch.pipeline().addLast("sslHandler", new SslHandler(sslEngine));
        }
        ch.pipeline().addLast("heart", new IdleStateHandler(readerIdleTime, writerIdleTime, 0, TimeUnit.SECONDS));
        ch.pipeline().addLast("http-decoder", new HttpRequestDecoder());
        ch.pipeline().addLast("http-deflater", new HttpContentCompressor());
        ch.pipeline().addLast("http-encoder", new HttpResponseEncoder());
        ch.pipeline().addLast("http-aggregator", new HttpObjectAggregator(65536));
        ch.pipeline().addLast("httpServerHandler", new HttpServerHandler());
    }
}