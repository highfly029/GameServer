package com.highfly029.core.net.udp;

import java.net.InetSocketAddress;
import java.util.List;

import com.google.protobuf.MessageLiteOrBuilder;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.utils.CompressUtils;
import com.highfly029.utils.EncryptUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageEncoder;


/**
 * @ClassName UdpFrameEncoder
 * @Description udp
 * @Author liyunpeng
 **/
public class UdpFrameEncoder extends MessageToMessageEncoder<MessageLiteOrBuilder> {
    private static final short magicNum = 0x4321;
    private static final int headerLength = 10;
    private static final byte isEncrypt = 1;
    private InetSocketAddress remoteAddress;

    public UdpFrameEncoder(InetSocketAddress remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public UdpFrameEncoder() {
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, MessageLiteOrBuilder msg, List<Object> out) throws Exception {
        PbPacket.Udp packet = null;
        if (msg instanceof PbPacket.UdpPacket) {
            packet = (PbPacket.Udp) msg;
        } else if (msg instanceof PbPacket.Udp.Builder) {
            packet = ((PbPacket.Udp.Builder) msg).build();
        }
        byte[] array = packet.getPacket().toByteArray();
        byte isCompress = 0;
        if (array.length > 1024) {// >1k 压缩
            isCompress = 1;
            array = CompressUtils.snappyCompress(array);
        }
        if (isEncrypt == 1) { //加密
            array = EncryptUtils.xor(array);
        }
        if (array.length > 1024 * 512) {
//            LoggerTool.systemLogger.error("send bodyLength maxsize is 512K,now bodyLength={}", array.length);
            ctx.close();
            throw new IllegalArgumentException("encode bodyLength too long");
        }
        //TODO分包 500Byte
        ByteBuf buf = ctx.alloc().buffer(headerLength + array.length);
        buf.writeShortLE(magicNum);
        buf.writeByte(0);
        buf.writeByte(0);
        buf.writeByte(isEncrypt);
        buf.writeByte(isCompress);
        buf.writeIntLE(array.length);
        buf.writeBytes(array);
        if (remoteAddress != null) { //client
            out.add(new DatagramPacket(buf, remoteAddress));
        } else { //server
            InetSocketAddress address = new InetSocketAddress(packet.getUdpHost(), packet.getUdpPort());
            out.add(new DatagramPacket(buf, address));
        }

    }
}
