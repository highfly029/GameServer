package com.highfly029.core.net;

/**
 * @ClassName NetType
 * @Description 网络类型
 * @Author liyunpeng
 **/
public class NetType {
    public static final int NET_TCP_SERVER = 1;
    public static final int NET_HTTP_SERVER = 2;
    public static final int NET_TCP_CLIENT = 3;
    public static final int NET_UDP_CLIENT = 6;
    public static final int NET_UDP_SERVER = 7;
}
