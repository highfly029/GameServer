package com.highfly029.core.net.http;

import com.highfly029.core.interfaces.IHttpHandler;

/**
 * @ClassName BaseChildHttpHandler
 * @Description child线程处理
 * @Author liyunpeng
 **/
public abstract class BaseChildHttpHandler implements IHttpHandler {
    @Override
    public boolean isGlobal() {
        return false;
    }
}
