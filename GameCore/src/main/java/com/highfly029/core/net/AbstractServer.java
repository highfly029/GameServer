package com.highfly029.core.net;

import io.netty.channel.EventLoopGroup;

/**
 * @ClassName AbstractServer
 * @Description netty服务端
 * @Author liyunpeng
 **/
public abstract class AbstractServer extends AbstractNet {
    protected static final String BOSS_THREAD_NAME = "Boss";
    protected static final String WORKER_THREAD_NAME = "Worker";

    protected int bossThreadNum;
    protected int workerThreadNum;
    protected EventLoopGroup boss;
    protected EventLoopGroup worker;
    protected int soBacklogSize;
    protected int soSendBufSize;
    protected int soReceiveBufSize;

    public void setBossThreadNum(int bossThreadNum) {
        this.bossThreadNum = bossThreadNum;
    }

    public void setWorkerThreadNum(int workerThreadNum) {
        this.workerThreadNum = workerThreadNum;
    }
    
    public void setSoBacklogSize(int soBacklogSize) {
        this.soBacklogSize = soBacklogSize;
    }

    public void setSoSendBufSize(int soSendBufSize) {
        this.soSendBufSize = soSendBufSize;
    }

    public void setSoReceiveBufSize(int soReceiveBufSize) {
        this.soReceiveBufSize = soReceiveBufSize;
    }

    public void shutdown() {
        if (boss != null) {
            boss.shutdownGracefully();
        }
        if (worker != null) {
            worker.shutdownGracefully();
        }
    }
}
