package com.highfly029.core.db;

import java.util.List;

import com.highfly029.core.interfaces.IMySQLCallback;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;

import io.vertx.mysqlclient.MySQLPool;
import io.vertx.sqlclient.Tuple;

/**
 * @ClassName MySQLUtils
 * @Description MySQLUtils 后续支持批处理 事务
 * @Author liyunpeng
 **/
public class MySQLUtils {

    /**
     * 执行单条sql
     *
     * @param pool     连接池
     * @param sql      sql语句
     * @param tuple    数据
     * @param callback 回调
     */
    public static void sendGlobal(MySQLPool pool, String sql, Tuple tuple, IMySQLCallback callback) {
        send(pool, sql, tuple, 0, callback);
    }


    /**
     * 执行单条sql
     *
     * @param pool     连接池
     * @param sql      sql语句
     * @param tuple    数据
     * @param index    childWorld索引
     * @param callback 回调
     */
    public static void sendChild(MySQLPool pool, String sql, Tuple tuple, int index, IMySQLCallback callback) {
        if (index <= 0) {
            callback.onMysqlCallback(false, null, "MySQLUtils send index invalid");
            return;
        }
        send(pool, sql, tuple, index, callback);
    }

    /**
     * 执行单条sql
     *
     * @param pool     连接池
     * @param sql      sql语句
     * @param tuple    数据
     * @param index    childWorld索引
     * @param callback 回调
     */
    private static void send(MySQLPool pool, String sql, Tuple tuple, int index, IMySQLCallback callback) {
        if (callback == null) {
            LoggerTool.systemLogger.error("MySQLUtils send callback is null", new Exception());
            return;
        }
        if (pool == null) {
            callback.onMysqlCallback(false, null, "MySQLUtils send pool is null");
            return;
        }

        if (tuple == null) {
            pool.preparedQuery(sql).execute().onSuccess(rows -> WorldTool.addLogicEvent(LogicEventType.MYSQL_CLIENT_EVENT_TYPE, LogicEventAction.SUCCESS, index, rows, callback))
                    .onFailure(throwable -> WorldTool.addLogicEvent(LogicEventType.MYSQL_CLIENT_EVENT_TYPE, LogicEventAction.FAIL, index, throwable, callback));
//            pool.preparedQuery(sql).onSuccess(rows -> WorldTool.addLogicEvent(LogicEventType.MYSQL_CLIENT_EVENT_TYPE, LogicEventAction.SUCCESS, index, rows, callback))
//                    .onFailure(throwable -> WorldTool.addLogicEvent(LogicEventType.MYSQL_CLIENT_EVENT_TYPE, LogicEventAction.FAIL, index, throwable, callback));
        } else {
            pool.preparedQuery(sql).execute(tuple).onSuccess(rows -> WorldTool.addLogicEvent(LogicEventType.MYSQL_CLIENT_EVENT_TYPE, LogicEventAction.SUCCESS, index, rows, callback))
                            .onFailure(throwable -> WorldTool.addLogicEvent(LogicEventType.MYSQL_CLIENT_EVENT_TYPE, LogicEventAction.FAIL, index, throwable, callback));
//            pool.preparedQuery(sql, tuple).onSuccess(rows -> WorldTool.addLogicEvent(LogicEventType.MYSQL_CLIENT_EVENT_TYPE, LogicEventAction.SUCCESS, index, rows, callback))
//                    .onFailure(throwable -> WorldTool.addLogicEvent(LogicEventType.MYSQL_CLIENT_EVENT_TYPE, LogicEventAction.FAIL, index, throwable, callback));
        }
    }

    /**
     * 执行sql批处理
     *
     * @param pool
     * @param sql
     * @param batch
     * @param callback
     */
    public static void sendBatch(MySQLPool pool, String sql, List<Tuple> batch, int index, IMySQLCallback callback) {
        if (pool == null) {
            callback.onMysqlCallback(false, null, "MySQLUtils sendBatch pool is null");
            return;
        }
        if (batch == null || batch.size() == 0) {
            callback.onMysqlCallback(false, null, "MySQLUtils sendBatch pool is null");
            return;
        }

        pool.preparedQuery(sql).executeBatch(batch).onSuccess(rows -> WorldTool.addLogicEvent(LogicEventType.MYSQL_CLIENT_EVENT_TYPE, LogicEventAction.SUCCESS, index, rows, callback))
                        .onFailure(throwable -> WorldTool.addLogicEvent(LogicEventType.MYSQL_CLIENT_EVENT_TYPE, LogicEventAction.FAIL, index, throwable, callback));
//        pool.preparedBatch(sql, batch).onSuccess(rows -> WorldTool.addLogicEvent(LogicEventType.MYSQL_CLIENT_EVENT_TYPE, LogicEventAction.SUCCESS, index, rows, callback))
//                .onFailure(throwable -> WorldTool.addLogicEvent(LogicEventType.MYSQL_CLIENT_EVENT_TYPE, LogicEventAction.FAIL, index, throwable, callback));
    }
}
