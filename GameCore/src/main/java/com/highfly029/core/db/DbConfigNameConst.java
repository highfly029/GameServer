package com.highfly029.core.db;

/**
 * @ClassName DbConfigNameConst
 * @Description db常量
 * @Author liyunpeng
 **/
public interface DbConfigNameConst {
    String DB = "db";

    String MYSQL = "mysql";
    String USERNAME = "username";
    String PASSWORD = "password";
    String HOST = "host";
    String PORT = "port";
    String DATABASE = "database";

    String REDIS = "redis";
    String CONNECTION = "connection";
    String POOL_SIZE = "pool_size";
    String TYPE = "type";
}
