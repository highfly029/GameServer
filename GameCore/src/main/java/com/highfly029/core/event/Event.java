package com.highfly029.core.event;

/**
 * @ClassName Event
 * @Description 事件
 * @Author liyunpeng
 **/
public class Event {
    public int type;

    public Event(int type) {
        this.type = type;
    }
}
