package com.highfly029.core.fsm;

/**
 * @ClassName State
 * @Description 状态机
 * @Author liyunpeng
 **/
public abstract class State {
    public abstract void onEnter(Object object);

    public abstract void onExecute(Object object);

    public abstract void onExit(Object object);
}
