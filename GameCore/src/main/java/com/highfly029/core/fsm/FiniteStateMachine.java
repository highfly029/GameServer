package com.highfly029.core.fsm;

/**
 * @ClassName FiniteStateMachine
 * @Description 服务器状态机
 * @Author liyunpeng
 **/
public class FiniteStateMachine<T> {
    private State currentState;
    private final T owner;

    public FiniteStateMachine(T owner) {
        this.owner = owner;
    }

    public boolean changeState(State newState) {
        if (newState == null) {
            return false;
        }
        if (currentState != null) {
            currentState.onExit(owner);
        }
        currentState = newState;
        if (currentState != null) {
            currentState.onEnter(owner);
        }
        return true;
    }


    public boolean execute(int stateInput) {
        if (currentState != null) {
            currentState.onExecute(owner);
            return true;
        } else {
            return false;
        }
    }

    public State getCurrentState() {
        return currentState;
    }
}
