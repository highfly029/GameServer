package com.highfly029.core.fsm.serverfsm;

import com.highfly029.core.fsm.State;
import com.highfly029.core.tool.LoggerTool;

/**
 * @ClassName ClosedServerFSM
 * @Description 服务器状态机
 * @Author liyunpeng
 **/
public class ClosedServerFSM extends State {
    @Override
    public void onEnter(Object object) {
        LoggerTool.systemLogger.info("ClosedServerFSM onEnter");
    }

    @Override
    public void onExecute(Object object) {
        LoggerTool.systemLogger.info("ClosedServerFSM onExecute");
    }

    @Override
    public void onExit(Object object) {
        LoggerTool.systemLogger.info("ClosedServerFSM onExit");
    }
}
