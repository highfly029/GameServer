package com.highfly029.core.fsm.serverfsm;

import com.highfly029.core.fsm.State;
import com.highfly029.core.tool.LoggerTool;

/**
 * @ClassName RunningServerFSM
 * @Description 服务器状态机
 * @Author liyunpeng
 **/
public class RunningServerFSM extends State {
    @Override
    public void onEnter(Object object) {
        LoggerTool.systemLogger.info("RunningServerFSM onEnter");
    }

    @Override
    public void onExecute(Object object) {
        LoggerTool.systemLogger.info("RunningServerFSM onExecute");
    }

    @Override
    public void onExit(Object object) {
        LoggerTool.systemLogger.info("RunningServerFSM onExit");
    }
}
