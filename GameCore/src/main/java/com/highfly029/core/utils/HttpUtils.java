package com.highfly029.core.utils;

import java.util.List;

import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.collection.ObjObjMap;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpConstants;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.QueryStringDecoder;
import io.netty.handler.codec.http.multipart.DefaultHttpDataFactory;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import io.netty.handler.codec.http.multipart.MemoryAttribute;
import io.netty.util.CharsetUtil;

/**
 * @ClassName HttpUtils
 * @Description HttpUtils
 * @Author liyunpeng
 **/
public class HttpUtils {
    public static final String route = ConfigPropUtils.getValue("route");
    public static final String RESP_FAIL = "fail";
    public static final String RESP_SUCCESS = "success";
    public static final String NOT_FIND_HANDLER = "not_find_handler";

    /**
     * 发送http相应
     *
     * @param channel
     * @param result
     */
    public static void sendHttpResponse(Channel channel, String result) {
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.TEXT_PLAIN);
        response.content().writeBytes(result.getBytes(HttpConstants.DEFAULT_CHARSET));
        channel.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

    public static void sendHttpResponse(Channel channel, FullHttpResponse response, String result) {
        response.setStatus(HttpResponseStatus.OK);
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.TEXT_PLAIN);
        response.content().writeBytes(result.getBytes(HttpConstants.DEFAULT_CHARSET));
        channel.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

    /**
     * 生成请求参数
     *
     * @param map
     * @return
     */
    public static String generateParams(ObjObjMap<String, String> map) {
        if (map == null || map.size() == 0) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        map.foreachImmutable((k, v) -> {
            stringBuilder.append(k).append("=").append(v).append("&");
        });
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    public static String getRequestContent(FullHttpRequest request) {
        return request.content().toString(CharsetUtil.UTF_8);
    }

    /**
     * 类似a=1&b=2的数据
     *
     * @param request
     * @return
     */
    public static ObjObjMap<String, String> getRequestParams(HttpRequest request) {
        ObjObjMap<String, String> reqParams = new ObjObjMap<>(String[]::new, String[]::new);
        if (request.method() == HttpMethod.GET) {
            QueryStringDecoder decoder = new QueryStringDecoder(request.uri());
            decoder.parameters().entrySet().forEach(entry -> {
                reqParams.put(entry.getKey(), entry.getValue().get(0));
            });
        } else if (request.method() == HttpMethod.POST) {
            HttpPostRequestDecoder decoder = new HttpPostRequestDecoder(
                    new DefaultHttpDataFactory(false), request);
            List<InterfaceHttpData> postData = decoder.getBodyHttpDatas(); //
            for (InterfaceHttpData data : postData) {
                if (data.getHttpDataType() == InterfaceHttpData.HttpDataType.Attribute) {
                    MemoryAttribute attribute = (MemoryAttribute) data;
                    reqParams.put(attribute.getName(), attribute.getValue());
                }
            }
            decoder.destroy();
        }
        return reqParams;
    }
}
