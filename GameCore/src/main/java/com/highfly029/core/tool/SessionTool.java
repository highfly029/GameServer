package com.highfly029.core.tool;

import com.highfly029.core.session.Session;
import com.highfly029.core.world.GlobalWorld;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjObjMap;

import io.netty.channel.Channel;

/**
 * @ClassName SessionTool
 * @Description SessionTool
 * @Author liyunpeng
 **/
public class SessionTool implements ITool {
    protected GlobalWorld globalWorld;

    // channel -> Session
    private final ObjObjMap<Channel, Session> allSession = new ObjObjMap<>(Channel[]::new, Session[]::new);

    /**
     * session集合 key1:type key2:id -> session
     */
    protected final IntObjectMap<IntObjectMap<Session>> sessionTypeIDMap = new IntObjectMap<>(IntObjectMap[]::new);

    public SessionTool(GlobalWorld globalWorld) {
        this.globalWorld = globalWorld;
    }

    public Session create() {
        return new Session();
    }

    public void addSession(Session session) {
        allSession.put(session.getChannel(), session);

        IntObjectMap<Session> map = sessionTypeIDMap.get(session.getType());
        if (map == null) {
            map = new IntObjectMap<>(Session[]::new);
            sessionTypeIDMap.put(session.getType(), map);
        }
        map.put(session.getIdentifier(), session);
    }

    /**
     * 删除session
     *
     * @param session
     */
    public void removeSession(Session session) {
        allSession.remove(session.getChannel());
        IntObjectMap<Session> map = sessionTypeIDMap.get(session.getType());
        map.remove(session.getIdentifier());
    }

    private void removeSession(int type, int id) {
        IntObjectMap<Session> map = sessionTypeIDMap.get(type);
        Session session = map.remove(id);
        allSession.remove(session.getChannel());
    }

    public Session getSession(int type, int id) {
        IntObjectMap<Session> map = sessionTypeIDMap.get(type);
        if (map != null) {
            return map.get(id);
        } else {
            return null;
        }
    }

    /**
     * 获取指定类型的session集合
     * @param type
     * @return
     */
    public IntObjectMap<Session> getSessionMap(int type) {
        return sessionTypeIDMap.get(type);
    }

    /**
     * 通过channel获取Session
     *
     * @param channel
     * @return
     */
    public Session getSession(Channel channel) {
        return allSession.get(channel);
    }
}
