package com.highfly029.core.tool;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.IntSet;

import io.netty.buffer.ByteBufUtil;

/**
 * @ClassName PacketParserTool
 * @Description 协议包打印工具
 * @Author liyunpeng
 **/
public class PacketParserTool implements ITool {
    /**
     * 注册的协议集合
     */
    private static final IntObjectMap<IPacketParser> map = new IntObjectMap<>();

    /**
     * 忽略打印的协议集合
     */
    private static final IntSet ignoreDumpMessage = new IntSet();

    /**
     * 注册(允许相同的ptCode重复注册)
     *
     * @param ptCode
     * @param packetParser
     */
    public static void registerPacketParser(int ptCode, IPacketParser packetParser) {
        map.put(ptCode, packetParser);
    }

    /**
     * 获取协议包转换类
     *
     * @param ptCode
     * @return
     */
    public static IPacketParser getPacketParser(int ptCode) {
        return map.get(ptCode);
    }

    /**
     * 协议转换成字符串
     *
     * @param packetParser
     * @param byteString
     * @return
     */
    public static String getPacketDump(IPacketParser packetParser, ByteString byteString, boolean isDumpPacketDetail) {
        String dump;
        if (packetParser != null && (!packetParser.isParserHex() || isDumpPacketDetail)) {
            try {
                dump = packetParser.parser(byteString);
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
                LoggerTool.systemLogger.error("getPacketDumpError " + packetParser.getPtCode(), e);
                dump = "null";
            }
        } else {
            dump = ByteBufUtil.hexDump(byteString.toByteArray());
        }
        return dump;
    }

    /**
     * 增加忽略打印的消息
     *
     * @param ptCode
     */
    public static void addIgnoreDump(int ptCode) {
        ignoreDumpMessage.add(ptCode);
    }

    /**
     * 是否是忽略打印消息
     *
     * @param ptCode
     * @return
     */
    public static boolean ignoreDump(int ptCode) {
        return ignoreDumpMessage.contains(ptCode);
    }
}
