package com.highfly029.core.tool;

import com.highfly029.core.db.DbConfigNameConst;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.collection.ObjObjMap;

import io.vertx.mysqlclient.MySQLConnectOptions;
import io.vertx.mysqlclient.MySQLPool;
import io.vertx.sqlclient.PoolOptions;

/**
 * @ClassName MysqlTool
 * @Description mysql工具 进程唯一
 * @Author liyunpeng
 **/
public class MysqlTool implements ITool {

    private final ObjObjMap<String, MySQLPool> poolMap = new ObjObjMap<>(String[]::new, MySQLPool[]::new);


    public MySQLPool createMySQLPool(String name, String configPrev) {
        String host = ConfigPropUtils.getValue(String.format("%s.%s", configPrev, DbConfigNameConst.HOST));
        int port = ConfigPropUtils.getIntValue(String.format("%s.%s", configPrev, DbConfigNameConst.PORT));
        String username = ConfigPropUtils.getValue(String.format("%s.%s", configPrev, DbConfigNameConst.USERNAME));
        String password = ConfigPropUtils.getValue(String.format("%s.%s", configPrev, DbConfigNameConst.PASSWORD));
        String database = ConfigPropUtils.getValue(String.format("%s.%s", configPrev, DbConfigNameConst.DATABASE));

        if (poolMap.contains(name)) {
            return null;
        } else {
            MySQLConnectOptions connectOptions = new MySQLConnectOptions()
                    .setHost(host)
                    .setPort(port)
                    .setDatabase(database)
                    .setUser(username)
                    .setPassword(password)
                    .setCachePreparedStatements(true);

            PoolOptions poolOptions = new PoolOptions()
                    .setMaxSize(4);

            MySQLPool pool = MySQLPool.pool(connectOptions, poolOptions);
            poolMap.put(name, pool);
            return pool;
        }
    }

    /**
     * 获取指定连接池
     *
     * @param name
     * @return
     */
    public MySQLPool getConnection(String name) {
        return poolMap.get(name);
    }

    /**
     * 获取所有连接池
     *
     * @return
     */
    public ObjObjMap<String, MySQLPool> getPoolMap() {
        return poolMap;
    }

    public void shutdown() {
        MySQLPool[] values = poolMap.getValues();
        MySQLPool pool;
        for (int i = 0, len = values.length; i < len; i++) {
            if ((pool = values[i]) != null) {
                pool.close();
            }
        }
    }
}

