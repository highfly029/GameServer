package com.highfly029.core.tool;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName ThreadPoolExecutorTool
 * @Description 线程池执行器工具
 * @Author liyunpeng
 **/
public class ThreadPoolExecutorTool implements ITool {
    /**
     * 线程池执行器
     */
    private static ThreadPoolExecutor threadPoolExecutor;

    public static void init(int corePoolSize, int maximumPoolSize, long keepAliveTime) {
        threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.MINUTES,
                new LinkedBlockingQueue<>(), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());
    }

    /**
     * 执行
     * @param command
     */
    public static void execute(Runnable command) {
        threadPoolExecutor.execute(command);
    }

    public static ThreadPoolExecutor getThreadPoolExecutor() {
        return threadPoolExecutor;
    }
}
