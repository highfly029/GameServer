package com.highfly029.core.tool;

import com.highfly029.core.interfaces.IRedisCallback;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.redis.client.Redis;
import io.vertx.redis.client.RedisClientType;
import io.vertx.redis.client.RedisOptions;
import io.vertx.redis.client.Request;
import io.vertx.redis.client.Response;

/**
 * @ClassName RedisClientTool
 * @Description RedisClientTool
 * @Author liyunpeng
 **/
public class RedisClientTool implements ITool {
    private static Redis redisClient;

    public static Redis init(int type, int poolSize, String[] connections) {
        if (redisClient == null) {
            VertxOptions vertxOptions = new VertxOptions();
            //设置可使用的最大线程数
            vertxOptions.setEventLoopPoolSize(4);
            Vertx vertx = Vertx.vertx(vertxOptions);

            RedisOptions redisOptions = new RedisOptions();
            for (String conn : connections) {
                redisOptions.addConnectionString(conn);
            }
            RedisClientType redisClientType = RedisClientType.values()[type];
            redisOptions.setType(redisClientType);
            //后续可调整池大小
//            redisOptions.setMaxPoolSize(poolSize);
//            redisOptions.setMaxPoolWaiting(poolSize);
            redisClient = Redis.createClient(vertx, redisOptions);
            LoggerTool.systemLogger.info("redis init success");
        } else {
            LoggerTool.systemLogger.error("redis init duplicate", new Exception());
        }
        return redisClient;
    }

    public static void close() {
        if (redisClient != null) {
            LoggerTool.systemLogger.info("RedisClientTool close");
            redisClient.close();
        }
    }

    public static void send(int index, Request request, IRedisCallback callback) {
        Handler<AsyncResult<Response>> handler = result -> {
            if (result.succeeded()) {
                Response response = result.result();
                WorldTool.addLogicEvent(LogicEventType.REDIS_CLIENT_EVENT_TYPE, LogicEventAction.SUCCESS, index, response, callback);
            } else {
                WorldTool.addLogicEvent(LogicEventType.REDIS_CLIENT_EVENT_TYPE, LogicEventAction.FAIL, index, result.cause(), callback);
            }
        };

        redisClient.send(request, handler);
    }
}
