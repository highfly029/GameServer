package com.highfly029.core.tool;

import java.io.File;

import com.highfly029.utils.ConfigPropUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @ClassName LoggerTool
 * @Description 日志工具 进程唯一
 * @Author liyunpeng
 **/
public class LoggerTool implements ITool {
    static {
        //重新设置log4j2.xml的路径
        LoggerContext loggerContext = (LoggerContext) LogManager.getContext(false);
        File file = new File(ConfigPropUtils.getConfigPath() + "/log4j2.xml");
        loggerContext.setConfigLocation(file.toURI());
        loggerContext.reconfigure();
    }

    /**
     * 系统日志
     */
    public static final Logger systemLogger = LoggerFactory.getLogger("system");

    /**
     * http日志
     */
    public static final Logger httpLogger = LoggerFactory.getLogger("http");

    /**
     * 协议包日志
     */
    public static final Logger packetLogger = LoggerFactory.getLogger("packet");

    /**
     * 统计日志
     */
    public static final Logger statisticsLogger = LoggerFactory.getLogger("statistics");
}
