package com.highfly029.core.tool;

import java.util.TimeZone;

import com.highfly029.utils.CronExpression;
import com.highfly029.utils.CronUtils;


/**
 * @ClassName SystemTimeTool
 * @Description 系统时间工具 进程唯一
 * @Author liyunpeng
 **/
public final class SystemTimeTool implements ITool {
    public static final double SECOND_TIME_PER_MILL = 0.001d;
    /**
     * 偏移的时间
     */
    private static volatile long offsetMillTime;
    /**
     * 缓存的毫秒数
     */
    private static volatile long cacheMillTime;
    /**
     * 缓存的秒数
     */
    private static volatile long cacheSecondTime;

    /**
     * 下一次跨天时间
     */
    private static volatile long nextDailyTime;

    private static final TimeZone timeZone = TimeZone.getDefault();// TimeZone.getTimeZone("GMT+8:00");
    private static final CronExpression nextDailyCron = CronUtils.createCronExpression("0 0 0 * * ?", timeZone);

    static {
        invalidate(System.currentTimeMillis());
    }

    /**
     * 使缓存失效
     * @return 是否跨天
     */
    public static boolean invalidate(long now) {
        cacheMillTime = now + offsetMillTime;
        cacheSecondTime = cacheMillTime / 1000L;
        if (cacheMillTime >= nextDailyTime) {
            nextDailyTime = CronUtils.getNextCronTime(nextDailyCron, cacheMillTime);
            return true;
        }
        return false;
    }

    /**
     * 设置偏移量
     *
     * @param offsetMillTime
     */
    public static void setOffsetMillTime(long offsetMillTime) {
        SystemTimeTool.offsetMillTime = offsetMillTime;
        invalidate(System.currentTimeMillis());
    }

    /**
     * 获取毫秒时间
     *
     * @return
     */
    public static long getMillTime() {
        return cacheMillTime;
    }

    /**
     * 获取秒时间
     *
     * @return
     */
    public static long getSecondTime() {
        return cacheSecondTime;
    }

    public static long getNextDailyTime() {
        return nextDailyTime;
    }
}
