package com.highfly029.core.tool;

import com.highfly029.core.interfaces.IHttpClientCallback;
import com.highfly029.core.world.LogicEventAction;
import com.highfly029.core.world.LogicEventType;
import com.highfly029.utils.collection.ObjObjMap;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.RequestOptions;

/**
 * @ClassName HttpClient
 * @Description 异步HttpClient 进程唯一
 * @Author liyunpeng
 **/
public class HttpClientTool implements ITool {
    private static HttpClient httpClient;

    public static HttpClient init() {
        if (httpClient == null) {
            VertxOptions vertxOptions = new VertxOptions();
            //设置可使用的最大线程数
            vertxOptions.setEventLoopPoolSize(4);
            Vertx vertx = Vertx.vertx(vertxOptions);
            HttpClientOptions httpClientOptions = new HttpClientOptions();
            httpClient = vertx.createHttpClient(httpClientOptions);
            httpClientOptions.setConnectTimeout(60000);
            httpClientOptions.setIdleTimeout(10);
            httpClientOptions.setMaxWaitQueueSize(10);

            LoggerTool.httpLogger.info("init httpClient success!");
        } else {
            LoggerTool.httpLogger.error("init httpClient duplicate!", new Exception());
        }
        return httpClient;
    }

    public static void close() {
        if (httpClient != null) {
            LoggerTool.httpLogger.info("HttpClientTool close!!!");
            httpClient.close();
            httpClient = null;
        }
    }

    private static void send(int index, HttpMethod method, int port, String host, String requestURI, String chunk, ObjObjMap<String, String> headers, IHttpClientCallback callback) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.setMethod(method);
        requestOptions.setHost(host);
        requestOptions.setPort(port);
        requestOptions.setURI(requestURI);
        if (headers != null) {
            headers.foreachImmutable(requestOptions::putHeader);
        }
        httpClient.request(requestOptions).onComplete((req) -> {
            if (req.succeeded()) {
                Future<HttpClientResponse> future;
                if (chunk != null) {
                    future = req.result().send(chunk);
                } else {
                    future = req.result().send();
                }
                future.onComplete((resp) -> {
                    if (resp.succeeded()) {
                        resp.result().bodyHandler(handler -> WorldTool.addLogicEvent(LogicEventType.HTTP_CLIENT_EVENT_TYPE, LogicEventAction.SUCCESS, index, handler.toString(), callback));
                    } else {
                        WorldTool.addLogicEvent(LogicEventType.HTTP_CLIENT_EVENT_TYPE, LogicEventAction.FAIL, index, resp.cause().getMessage(), callback);
                    }
                });
            } else {
                WorldTool.addLogicEvent(LogicEventType.HTTP_CLIENT_EVENT_TYPE, LogicEventAction.FAIL, index, req.cause().getMessage(), callback);
            }
        });
    }

    /**
     * get请求
     *
     * @param index
     * @param host
     * @param requestURI
     * @param callback
     */
    public static void get(int index, String host, String requestURI, IHttpClientCallback callback) {
        send(index, HttpMethod.GET, 80, host, requestURI, null, null, callback);
    }

    public static void get(int index, int port, String host, String requestURI, IHttpClientCallback callback) {
        send(index, HttpMethod.GET, port, host, requestURI, null, null, callback);
    }

    /**
     * post请求
     *
     * @param index
     * @param port
     * @param host
     * @param requestURI
     * @param chunk
     * @param headers
     * @param callback
     */
    public static void post(int index, int port, String host, String requestURI, String chunk, ObjObjMap<String, String> headers, IHttpClientCallback callback) {
        send(index, HttpMethod.POST, 80, host, requestURI, chunk, headers, callback);
    }
}
