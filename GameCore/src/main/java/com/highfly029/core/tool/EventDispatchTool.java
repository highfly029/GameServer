package com.highfly029.core.tool;

import com.highfly029.core.event.Event;
import com.highfly029.core.interfaces.IEventListener;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName EventDispatchTool
 * @Description 事件分发工具 线程单例
 * @Author liyunpeng
 **/
public final class EventDispatchTool<E extends Event> implements ITool {
    private final IntObjectMap<ObjectList<IEventListener>> eventListenerMap = new IntObjectMap<>(ObjectList[]::new);

    public EventDispatchTool() {
    }

    public void addEventListener(int eventType, IEventListener IEventListener) {
        ObjectList<IEventListener> listenerList = this.eventListenerMap.get(eventType);
        if (listenerList == null) {
            listenerList = new ObjectList<>(IEventListener[]::new);
            this.eventListenerMap.put(eventType, listenerList);
        }
        listenerList.add(IEventListener);
    }

    public void removeEventListener(int eventType, IEventListener IEventListener) {
        ObjectList<IEventListener> listenerList = this.eventListenerMap.get(eventType);
        if (listenerList == null) {
            throw new IllegalStateException("remove unknown listener " + " eventType=" + eventType);
        }
        listenerList.remove(IEventListener);
    }

    public void postEvent(E event) {
        ObjectList<IEventListener> listenerList = this.eventListenerMap.get(event.type);
        if (listenerList != null) {
            IEventListener[] values = listenerList.getValues();
            IEventListener listener;
            int size = listenerList.size();
            for (int i = 0; i < size; i++) {
                if ((listener = values[i]) != null) {
                    listener.onEvent(event);
                }
            }
        }
    }
}
