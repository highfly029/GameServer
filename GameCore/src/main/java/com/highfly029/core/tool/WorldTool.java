package com.highfly029.core.tool;

import com.highfly029.core.world.GlobalWorld;
import com.highfly029.core.world.LogicEvent;
import com.highfly029.utils.collection.IntObjectMap;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;

/**
 * @ClassName WorldTool
 * @Description 世界工具类 进程唯一
 * @Author liyunpeng
 **/
public class WorldTool implements ITool {

    private static final WorldTool instance = new WorldTool();
    /**
     * 全局disruptor
     */
    private Disruptor<LogicEvent> disruptor;

    /**
     * 主世界
     */
    private GlobalWorld globalWorld;

    /**
     * 子世界disruptor
     */
    private Disruptor<LogicEvent>[] childDisruptorArray;

    private IntObjectMap<Disruptor> childMap;

    private WorldTool() {

    }

    public static WorldTool getInstance() {
        return instance;
    }

    public void setDisruptor(Disruptor disruptor) {
        this.disruptor = disruptor;
    }

    public void setGlobalWorld(GlobalWorld globalWorld) {
        this.globalWorld = globalWorld;
    }

    public GlobalWorld getGlobalWorld() {
        return globalWorld;
    }

    /**
     * 增加子世界
     */
    public void addChildWorld(int index, Disruptor disruptor) {
        if (childMap == null) {
            childMap = new IntObjectMap<>(Disruptor[]::new);
        }
        childMap.put(index, disruptor);
    }

    private void beforeStart() {
        if (childMap != null && !childMap.isEmpty()) {
            var ref = new Object() {
                private int maxValue = 0;
            };
            childMap.foreachImmutable((k, v) -> {
                if (k > ref.maxValue) {
                    ref.maxValue = k;
                }
            });
            childDisruptorArray = new Disruptor[ref.maxValue + 1];
            for (int i = 0; i < ref.maxValue + 1; i++) {
                childDisruptorArray[i] = childMap.get(i);
            }
        } else {
            childDisruptorArray = new Disruptor[0];
        }
    }

    /**
     * 启动
     */
    public void start() {
        beforeStart();
        disruptor.start();
        if (childDisruptorArray != null) {
            for (int i = 0, len = childDisruptorArray.length; i < len; i++) {
                Disruptor disruptor;
                if ((disruptor = childDisruptorArray[i]) != null) {
                    disruptor.start();
                }
            }
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            disruptor.shutdown();
            if (childDisruptorArray != null) {
                for (int i = 0, len = childDisruptorArray.length; i < len; i++) {
                    Disruptor disruptor;
                    if ((disruptor = childDisruptorArray[i]) != null) {
                        disruptor.shutdown();
                    }
                }
            }

//            HttpClientTool.close();

//            RedisClientTool.close();

            try {
                LoggerTool.systemLogger.info("wait for save log before shutdown");
                //server关闭前 日志需要刷新到磁盘 玩家需要保存到服务器
                Thread.sleep(1000 * 10);
                LoggerTool.systemLogger.info("shutdown over!");
            } catch (Exception e) {
                LoggerTool.systemLogger.error("shutdown sleep error", e);
            } finally {
                LoggerTool.systemLogger.info("WorldTool finally shutdown success!");
            }
        }));
    }

    /**
     * 增加主世界逻辑事件
     */
    public static void addLogicEvent(byte eventType, byte eventAction, Object data1) {
        RingBuffer<LogicEvent> ringBuffer = instance.disruptor.getRingBuffer();
        long sequence = ringBuffer.next();
        try {
            LogicEvent event = ringBuffer.get(sequence);
            event.setLogicEventType(eventType);
            event.setLogicEventAction(eventAction);
            event.setData1(data1);
        } finally {
            ringBuffer.publish(sequence);
        }
    }

    public static void addLogicEvent(byte eventType, byte eventAction, Object data1, Object data2) {
        RingBuffer<LogicEvent> ringBuffer = instance.disruptor.getRingBuffer();
        long sequence = ringBuffer.next();
        try {
            LogicEvent event = ringBuffer.get(sequence);
            event.setLogicEventType(eventType);
            event.setLogicEventAction(eventAction);
            event.setData1(data1);
            event.setData2(data2);
        } finally {
            ringBuffer.publish(sequence);
        }
    }

    public static void addLogicEvent(byte eventType, byte eventAction, Object data1, Object data2, Object data3) {
        RingBuffer<LogicEvent> ringBuffer = instance.disruptor.getRingBuffer();
        long sequence = ringBuffer.next();
        try {
            LogicEvent event = ringBuffer.get(sequence);
            event.setLogicEventType(eventType);
            event.setLogicEventAction(eventAction);
            event.setData1(data1);
            event.setData2(data2);
            event.setData3(data3);
        } finally {
            ringBuffer.publish(sequence);
        }
    }

    /**
     * 增加子世界逻辑事件
     */
    public static void addChildWorldLogicEvent(int worldIndex, byte eventType, byte eventAction, Object data1, Object data2, Object date3) {
        RingBuffer<LogicEvent> ringBuffer = instance.childDisruptorArray[worldIndex].getRingBuffer();
        long sequence = ringBuffer.next();
        try {
            LogicEvent event = ringBuffer.get(sequence);
            event.setLogicEventType(eventType);
            event.setLogicEventAction(eventAction);

            event.setData1(data1);
            event.setData2(data2);
            event.setData3(date3);
        } finally {
            ringBuffer.publish(sequence);
        }
    }

    public static void addChildWorldLogicEvent(int worldIndex, byte eventType, byte eventAction, Object data1, Object data2) {
        RingBuffer<LogicEvent> ringBuffer = instance.childDisruptorArray[worldIndex].getRingBuffer();
        long sequence = ringBuffer.next();
        try {
            LogicEvent event = ringBuffer.get(sequence);
            event.setLogicEventType(eventType);
            event.setLogicEventAction(eventAction);
            event.setData1(data1);
            event.setData2(data2);
        } finally {
            ringBuffer.publish(sequence);
        }
    }

    /**
     * 所有子世界都增加逻辑事件
     *
     * @param eventType
     * @param eventAction
     * @param data1
     * @param data2
     * @param date3
     */
    public static void addAllChildWorldLogicEvent(byte eventType, byte eventAction, Object data1, Object data2, Object date3) {
        for (Disruptor<LogicEvent> disruptor : instance.childDisruptorArray) {
            if (disruptor != null) {
                RingBuffer<LogicEvent> ringBuffer = disruptor.getRingBuffer();
                long sequence = ringBuffer.next();
                try {
                    LogicEvent event = ringBuffer.get(sequence);
                    event.setLogicEventType(eventType);
                    event.setLogicEventAction(eventAction);

                    event.setData1(data1);
                    event.setData2(data2);
                    event.setData3(date3);
                } finally {
                    ringBuffer.publish(sequence);
                }
            }
        }
    }


}
