package com.highfly029.core.tool;

import com.highfly029.core.net.AbstractClient;
import com.highfly029.core.net.AbstractServer;
import com.highfly029.core.net.NetConfigNameConst;
import com.highfly029.core.net.NetType;
import com.highfly029.core.net.http.HttpServer;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.net.tcp.TcpServer;
import com.highfly029.core.net.tcplocal.TcpLocalClient;
import com.highfly029.core.net.tcplocal.TcpLocalServer;
import com.highfly029.core.net.udp.UdpClient;
import com.highfly029.core.net.udp.UdpServer;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.collection.ObjObjMap;

/**
 * @ClassName NetTool
 * @Description 网络工具 进程唯一
 * @Author liyunpeng
 **/
public final class NetTool implements ITool {
    public ObjObjMap<String, AbstractClient> clientMaps = new ObjObjMap<>(String[]::new, AbstractClient[]::new);
    private final ObjObjMap<String, AbstractServer> serverMaps = new ObjObjMap<>(String[]::new, AbstractServer[]::new);

    /**
     * 获取网络配置 如果没找到则读取默认配置
     *
     * @param key
     * @param name
     * @return
     */
    private static String getNetConfigValue(String key, String name) {
        String value = ConfigPropUtils.getValue(key);
        if (value == null || value.equals("")) {
            value = ConfigPropUtils.getValue(String.format("%s.%s.%s", NetConfigNameConst.NET_PREV, NetConfigNameConst.NET_DEFAULT, name));
        }
        return value;
    }

    public AbstractServer createNetServer(int type, String name, String configPrev) {
        int bossThreadNum = Integer.valueOf(getNetConfigValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_BOSS_THREAD_NUM_NAME),
                NetConfigNameConst.NET_BOSS_THREAD_NUM_NAME));

        int workerThreadNum = Integer.valueOf(getNetConfigValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_WORKER_THREAD_NUM_NAME),
                NetConfigNameConst.NET_WORKER_THREAD_NUM_NAME));

        String epollMode = getNetConfigValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_EPOLL_MODE_NAME),
                NetConfigNameConst.NET_EPOLL_MODE_NAME);

        int backlog = Integer.valueOf(getNetConfigValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_BACKLOG_NAME),
                NetConfigNameConst.NET_BACKLOG_NAME));

        int sendBuf = Integer.valueOf(getNetConfigValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_WRITEBUF_NAME),
                NetConfigNameConst.NET_WRITEBUF_NAME));

        int receivedBuf = Integer.valueOf(getNetConfigValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_READBUF_NAME),
                NetConfigNameConst.NET_READBUF_NAME));

        int readerIdleTime = Integer.valueOf(getNetConfigValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_READER_IDLE_TIME_NAME),
                NetConfigNameConst.NET_READER_IDLE_TIME_NAME));

        int writerIdleTime = Integer.valueOf(getNetConfigValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_WRITER_IDLE_TIME_NAME),
                NetConfigNameConst.NET_WRITER_IDLE_TIME_NAME));

        int port = ConfigPropUtils.getIntValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_PORT_NAME));
        boolean innerServer = ConfigPropUtils.getBoolValue(String.format("%s.%s", configPrev, NetConfigNameConst.INNER_SERVER));
        boolean intercept = ConfigPropUtils.getBoolValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_INTERCEPT));
        boolean packetLogger = ConfigPropUtils.getBoolValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_PACKET_LOGGER));
        boolean dumpPacketDetail = ConfigPropUtils.getBoolValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_PACKET_DUMP_DETAIL));
        boolean packetStatistics = ConfigPropUtils.getBoolValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_PACKET_STATISTICS));

        String unixDomainSocket = ConfigPropUtils.getValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_UNIX_DOMAIN_SOCKET_NAME));
        boolean ssl = ConfigPropUtils.getBoolValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_HTTP_SSL_NAME));

        AbstractServer server;
        switch (type) {
            case NetType.NET_TCP_SERVER -> {
                if (unixDomainSocket == null || unixDomainSocket.equals("")) {
                    server = new TcpServer();
                } else {
                    server = new TcpLocalServer();
                    ((TcpLocalServer) server).setUnixDomainSocket(unixDomainSocket);
                }
            }
            case NetType.NET_UDP_SERVER -> {
                server = new UdpServer();
            }
            case NetType.NET_HTTP_SERVER -> {
                server = new HttpServer(ssl);
            }
            default -> {
                return null;
            }
        }

        if (serverMaps.containsKey(name)) {
            return null;
        } else {
            serverMaps.put(name, server);
        }

        server.setName(name);
        server.setBossThreadNum(bossThreadNum);
        server.setWorkerThreadNum(workerThreadNum);
        server.setEpollMode(epollMode);
        server.setSoBacklogSize(backlog);
        server.setSoSendBufSize(sendBuf);
        server.setSoReceiveBufSize(receivedBuf);
        server.setReaderIdleTime(readerIdleTime);
        server.setWriterIdleTime(writerIdleTime);
        server.setPort(port);
        server.setIntercept(intercept);
        server.setInnerServer(innerServer);
        server.setPacketLogger(packetLogger);
        server.setDumpPacketDetail(dumpPacketDetail);
        server.setPacketStatistics(packetStatistics);
        return server;
    }

    public AbstractClient createNetClient(int type, String name, String configPrev) {
        int workerThreadNum = Integer.valueOf(getNetConfigValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_WORKER_THREAD_NUM_NAME),
                NetConfigNameConst.NET_WORKER_THREAD_NUM_NAME));

        int timeoutMillis = Integer.valueOf(getNetConfigValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_TIMEOUT_MILLIS),
                NetConfigNameConst.NET_TIMEOUT_MILLIS));

        int readerIdleTime = ConfigPropUtils.getIntValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_READER_IDLE_TIME_NAME));
        int writerIdleTime = ConfigPropUtils.getIntValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_WRITER_IDLE_TIME_NAME));

        String epollMode = ConfigPropUtils.getValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_EPOLL_MODE_NAME));

        int port = ConfigPropUtils.getIntValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_PORT_NAME));

        String unixDomainSocket = ConfigPropUtils.getValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_UNIX_DOMAIN_SOCKET_NAME));

        String host = ConfigPropUtils.getValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_HOST_NAME));

        int maxRetry = ConfigPropUtils.getIntValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_CLIENT_MAX_RETRY));

        boolean innerServer = ConfigPropUtils.getBoolValue(String.format("%s.%s", configPrev, NetConfigNameConst.INNER_SERVER));
        boolean intercept = ConfigPropUtils.getBoolValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_INTERCEPT));
        boolean packetLogger = ConfigPropUtils.getBoolValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_PACKET_LOGGER));
        boolean dumpPacketDetail = ConfigPropUtils.getBoolValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_PACKET_DUMP_DETAIL));
        boolean packetStatistics = ConfigPropUtils.getBoolValue(String.format("%s.%s", configPrev, NetConfigNameConst.NET_PACKET_STATISTICS));

        AbstractClient client;
        switch (type) {
            case NetType.NET_TCP_CLIENT -> {
                if (unixDomainSocket == null || unixDomainSocket.equals("")) {
                    client = new TcpClient();
                } else {
                    client = new TcpLocalClient();
                    ((TcpLocalClient) client).setUnixDomainSocket(unixDomainSocket);
                }
            }
            case NetType.NET_UDP_CLIENT -> {
                client = new UdpClient();
            }
            default -> {
                return null;
            }
        }
        if (clientMaps.containsKey(name)) {
            return null;
        } else {
            clientMaps.put(name, client);
        }
        client.setName(name);
        client.setWorkerThreadNum(workerThreadNum);
        client.setEpollMode(epollMode);
        client.setReaderIdleTime(readerIdleTime);
        client.setWriterIdleTime(writerIdleTime);
        client.setTimeOutMillis(timeoutMillis);
        client.setHost(host);
        client.setPort(port);
        client.setMaxRetry(maxRetry);
        client.setInnerServer(innerServer);
        client.setIntercept(intercept);
        client.setPacketLogger(packetLogger);
        client.setDumpPacketDetail(dumpPacketDetail);
        client.setPacketStatistics(packetStatistics);
        return client;
    }

    /**
     * 获取服务端连接
     *
     * @param name
     * @return
     */
    public AbstractServer getServer(String name) {
        return serverMaps.get(name);
    }

    /**
     * 获取客户端连接
     *
     * @param name
     * @return
     */
    public AbstractClient getClient(String name) {
        return clientMaps.get(name);
    }


    public void shutdown() {
        AbstractServer[] serverValues = serverMaps.getValues();
        AbstractServer server;
        for (int i = 0, len = serverValues.length; i < len; i++) {
            if ((server = serverValues[i]) != null) {
                server.shutdown();
            }
        }

        AbstractClient[] clientValues = clientMaps.getValues();
        AbstractClient client;
        for (int i = 0, len = clientValues.length; i < len; i++) {
            if ((client = clientValues[i]) != null) {
                client.shutdown();
            }
        }

    }
}
