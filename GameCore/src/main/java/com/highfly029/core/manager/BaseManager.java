package com.highfly029.core.manager;

import com.highfly029.core.interfaces.IUpdate;
import com.highfly029.core.world.WorldThread;

/**
 * @ClassName IManager
 * @Description IManager
 * @Author liyunpeng
 **/
public abstract class BaseManager implements IUpdate {
    private final WorldThread world;

    public BaseManager(WorldThread world) {
        this.world = world;
    }

    /**
     * 初始化
     */
    public void init() {

    }
}
