package com.highfly029.core.manager;

import com.highfly029.core.world.WorldThread;

/**
 * @ClassName SessionGlobalManager
 * @Description Session全局管理器
 * @Author liyunpeng
 **/
public class SessionGlobalManager extends BaseManager {

    public SessionGlobalManager(WorldThread world) {
        super(world);
    }

    @Override
    public void onTick(int escapedMillTime) {

    }

    @Override
    public void onSecond() {

    }

    @Override
    public void onMinute() {

    }

    @Override
    public void onShutDown() {

    }
}
