package com.highfly029.core.world;

import com.highfly029.core.interfaces.IHttpHandler;
import com.highfly029.core.interfaces.ILogicEventHandler;
import com.highfly029.core.interfaces.IUpdate;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.utils.ReflectionUtils;
import com.highfly029.utils.SysUtils;
import com.highfly029.utils.collection.ObjObjMap;
import com.highfly029.utils.collection.ObjectList;

/**
 * @ClassName WorldThread
 * @Description 世界线程
 * @Author liyunpeng
 **/
public abstract class WorldThread {
    /**
     * 是否是全局线程
     */
    protected boolean isGlobal;
    /**
     * 上一次tick时间
     */
    protected long lastTickTime = System.currentTimeMillis();
    /**
     * 累计的秒时间
     */
    private int secondTick;
    /**
     * 累计的分钟次数
     */
    private int minuteTick;
    /**
     * tick间隔 单位毫秒
     */
    private static final int TICK_INTERVAL = 30;

    /**
     * 处理多少次事件会触发一次循环 防止一直在处理事件没有tick
     */
    private static final short EVENT_TIMES_TRIGGER_LOOP = 10;
    /**
     * 记录触发的事件次数、当超过一定值时触发一次循环
     */
    private short curEventTimes = 0;
    /**
     * 事件消耗时间最大值，超过则打印warning日志
     */
    private static final long EVENT_COST_TIME_MAX = 30;
    /**
     * tick消耗时间最大值，超过则打印warning日志
     */
    private static final long TICK_COST_TIME_MAX = 500;

    /**
     * 是否启动成功 可以开始tick
     */
    private boolean isStartSuccess = false;

    /**
     * disruptor逻辑事件handler
     */
    protected final ILogicEventHandler[] eventHandlers = new ILogicEventHandler[LogicEventType.maxValue + 1];
    /**
     * 所有时间更新类
     */
    protected ObjObjMap<Class, IUpdate> updateMaps = new ObjObjMap<>(Class[]::new, IUpdate[]::new);
    /**
     * http协议处理map
     */
    protected ObjObjMap<String, IHttpHandler> httpHandlerMaps = new ObjObjMap<>(String[]::new, IHttpHandler[]::new);

    /**
     * 逻辑循环
     */
    public final void onLogicLoop() {
        try {
            tick();
            curEventTimes = 0;
        } catch (Exception e) {
            LoggerTool.systemLogger.error("onLogicLoop", e);
        }
    }

    private final void tick() {
        final long start = System.currentTimeMillis();
        final int escapedMillTime = checkTick(start);
        if (0 == escapedMillTime) {
            return;
        }
        tick(escapedMillTime);
        final long cost = System.currentTimeMillis() - start;
        if (cost > TICK_COST_TIME_MAX) {
            LoggerTool.systemLogger.warn("tick cost too much time {}ms", cost);
        }
    }

    /**
     * 检测是否需要tick
     *
     * @return escapedMillTime
     */
    protected int checkTick(long now) {
        final int delta;
        if ((delta = (int) (now - lastTickTime)) >= TICK_INTERVAL) {
            lastTickTime = now;
            return delta;
        }
        return 0;
    }

    private final void tick(int escapedMillTime) {
        if (isStartSuccess) {
            onTick(escapedMillTime);
            if ((secondTick += escapedMillTime) > 1000) {
                secondTick = 0;
                onSecond();
                if ((++minuteTick) >= 60) {
                    minuteTick = 0;
                    onMinute();
                }
            }
        }
    }


    protected void onTick(int escapedMillTime) {
        final IUpdate[] values = updateMaps.getValues();
        IUpdate updateInterface;
        for (int i = 0, len = values.length; i < len; i++) {
            if ((updateInterface = values[i]) != null) {
                updateInterface.onTick(escapedMillTime);
            }
        }
    }

    protected void onSecond() {
        final IUpdate[] values = updateMaps.getValues();
        IUpdate IUpdate;
        for (int i = 0, len = values.length; i < len; i++) {
            if ((IUpdate = values[i]) != null) {
                IUpdate.onSecond();
            }
        }
    }

    protected void onMinute() {
        final IUpdate[] values = updateMaps.getValues();
        IUpdate IUpdate;
        for (int i = 0, len = values.length; i < len; i++) {
            if ((IUpdate = values[i]) != null) {
                IUpdate.onMinute();
            }
        }
    }

    /**
     * 初始化
     */
    protected void init() {

    }

    /**
     * 启动通知
     */
    final void onStartUp() {
        try {
            //先不执行cpu绑定
//            affinityLock.acquireLock(AffinityStrategies.DIFFERENT_SOCKET, AffinityStrategies.DIFFERENT_CORE);
//            affinityLock.bind();

//            affinityLock.acquireCore();
//            affinityLock.bind(true);
            init();
            onBeforeLoadAll();
            loadEventHandler();
            loadAllUpdate();
            loadAllHttpHandler();
            onAfterLoadAll();

            onBeforeStartUp();
            startUp();

        } catch (Exception exception) {
            LoggerTool.systemLogger.error("onStartUp exception", exception);
            SysUtils.exit();
        }
    }

    /**
     * 逻辑事件通知
     *
     * @param event
     */
    public final void onLogicEvent(LogicEvent event) {
        try {
            final long start = System.currentTimeMillis();
            eventHandlers[event.getLogicEventType()].onLogicEvent(event);
            final long cost = System.currentTimeMillis() - start;
            if (cost > EVENT_COST_TIME_MAX) {
                LoggerTool.systemLogger.warn("onLogicEvent cost too much time {}ms, event={}", cost, event);
            }
        } catch (Exception e) {
            LoggerTool.systemLogger.error("onLogicEvent exception", e);
//        } catch (Throwable throwable) {
//            LoggerTool.systemLogger.error("onLogicEvent throwable", throwable);
        } finally {
            event.clean();
            ++curEventTimes;
            if (curEventTimes == EVENT_TIMES_TRIGGER_LOOP) {
                curEventTimes = 0;
                onLogicLoop();
            }
        }
    }

    /**
     * 关闭通知
     */
    public void onShutDown() {
        try {
            if (isStartSuccess) {
                beforeOnShutDown();

            }
        } catch (Exception exception) {
            LoggerTool.systemLogger.error("onShutDown exception", exception);
        }
//        } catch (Throwable throwable) {
//            LoggerTool.systemLogger.error("onShutDown throwable", throwable);
//        }
        LoggerTool.systemLogger.info("onShutDown success");
    }

    /**
     * 关服前
     */
    protected void beforeOnShutDown() {
        final IUpdate[] values = updateMaps.getValues();
        IUpdate IUpdate;
        for (int i = 0, len = values.length; i < len; i++) {
            if ((IUpdate = values[i]) != null) {
                IUpdate.onShutDown();
            }
        }
    }


    /**
     * 异常通知
     */
    public final void onException(Throwable ex, String reason) {
        LoggerTool.systemLogger.error(reason, ex);
    }

    /**
     * 加载前通知
     */
    protected void onBeforeLoadAll() {

    }

    /**
     * 加载后通知
     *
     * @throws Exception
     */
    protected void onAfterLoadAll() throws Exception {

    }

    /**
     * 加载事件处理器
     */
    protected abstract void loadEventHandler();

    /**
     * 加载所有update实现类
     */
    protected void loadAllUpdate() {

    }

    /**
     * 加载所有http处理类
     *
     * @throws Exception
     */
    protected void loadAllHttpHandler() throws Exception {
        ObjectList<Class> clsList = ReflectionUtils.getAllClassByInterface(IHttpHandler.class, getWorldPackageName());
        Class[] values = clsList.getValues();
        Class cls;
        for (int i = 0, size = clsList.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                IHttpHandler httpHandler = (IHttpHandler) cls.getDeclaredConstructor().newInstance();
                if (isGlobal == httpHandler.isGlobal()) {
                    httpHandler.init(this);
                    String path = httpHandler.register();
                    httpHandlerMaps.put(path, httpHandler);
                }
            }
        }
    }

    /**
     * 启动前通知
     */
    protected void onBeforeStartUp() {

    }

    /**
     * 启动通知
     */
    protected void startUp() throws Exception {

    }

    /**
     * 启动成功
     */
    protected void onStartUpSuccess() {
        isStartSuccess = true;
    }


    /**
     * 获取包名
     *
     * @return
     */
    protected String getWorldPackageName() {
        String packageName = this.getClass().getPackageName();
        packageName = packageName.substring(0, packageName.lastIndexOf('.'));
        return packageName;
    }

}
