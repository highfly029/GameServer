package com.highfly029.core.world;

import com.highfly029.core.interfaces.ICleanable;

/**
 * @ClassName LogicEvent
 * @Description 全局逻辑队列事件
 * @Author liyunpeng
 **/
public class LogicEvent implements ICleanable {
    /**
     * 逻辑事件类型
     */
    private byte logicEventType;

    /**
     * 逻辑事件行为
     */
    private byte logicEventAction;

    /**
     * 传输参数1
     */
    private Object data1;

    /**
     * 传输参数2
     */
    private Object data2;
    /**
     * 传输参数3
     */
    private Object data3;


    public byte getLogicEventType() {
        return logicEventType;
    }

    public void setLogicEventType(byte logicEventType) {
        this.logicEventType = logicEventType;
    }

    public byte getLogicEventAction() {
        return logicEventAction;
    }

    public void setLogicEventAction(byte logicEventAction) {
        this.logicEventAction = logicEventAction;
    }

    public Object getData1() {
        return data1;
    }

    public void setData1(Object data1) {
        this.data1 = data1;
    }

    public Object getData2() {
        return data2;
    }

    public void setData2(Object data2) {
        this.data2 = data2;
    }

    public Object getData3() {
        return data3;
    }

    public void setData3(Object data3) {
        this.data3 = data3;
    }

    @Override
    public void clean() {
        logicEventType = 0;
        logicEventAction = 0;

        data1 = null;
        data2 = null;
        data3 = null;
    }

    @Override
    public String toString() {
        return "LogicEvent{" +
                "logicEventType=" + logicEventType +
                ", logicEventAction=" + logicEventAction +
                ", data1=" + data1 +
                ", data2=" + data2 +
                ", data3=" + data3 +
                '}';
    }
}
