package com.highfly029.core.world;

import java.util.concurrent.ThreadFactory;

import com.highfly029.utils.MathUtils;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

/**
 * @ClassName DisruptorBuilder
 * @Description 构造器
 * @Author liyunpeng
 **/
public class DisruptorBuilder {
    /**
     * 默认环形队列大小
     * 合理设置数组大小 既要防止消息队列堵塞 也要避免大对象(超过4M)影响G1 CMS等 对ZGC影响相对较小
     */
    private static final int DEFAULT_RING_BUFFER_SIZE = 1024 * 512;
    /**
     * 世界线程
     */
    private WorldThread worldThread;
    /**
     * 环形队列大小
     */
    private int ringBufferSize;
    /**
     * 等待策略
     */
    private WaitStrategy waitStrategy;
    /**
     * 生产类型
     */
    private ProducerType producerType;
    /**
     * 线程名字
     */
    private String poolName;
    /**
     * 线程索引
     */
    private int index;

    private DisruptorBuilder() {

    }

    public static DisruptorBuilder builder() {
        return new DisruptorBuilder();
    }

    public DisruptorBuilder setWorldThread(WorldThread worldThread) {
        this.worldThread = worldThread;
        return this;
    }

    public DisruptorBuilder setRingBufferSize(int ringBufferSize) {
        this.ringBufferSize = ringBufferSize;
        return this;
    }

    public DisruptorBuilder setWaitStrategy(WaitStrategy waitStrategy) {
        this.waitStrategy = waitStrategy;
        return this;
    }


    public DisruptorBuilder setProducerType(ProducerType producerType) {
        this.producerType = producerType;
        return this;
    }

    public DisruptorBuilder setPoolName(String poolName) {
        this.poolName = poolName;
        return this;
    }

    public DisruptorBuilder setIndex(int index) {
        this.index = index;
        return this;
    }

    public Disruptor<LogicEvent> build() {
        if (ringBufferSize <= 0) {
            ringBufferSize = DEFAULT_RING_BUFFER_SIZE;
        }
        if (!MathUtils.isPowerOfTwo(ringBufferSize)) {
            throw new IllegalArgumentException("ringBufferSize");
        }
        if (worldThread == null) {
            throw new IllegalArgumentException("worldThread");
        }
        if (waitStrategy == null) {
            throw new IllegalArgumentException("waitStrategy");
        }
        if (producerType == null) {
            throw new IllegalArgumentException("producerType");
        }
        if (poolName == null) {
            throw new IllegalArgumentException("poolName");
        }
        LogicEventFactory eventFactory = new LogicEventFactory();
        ThreadFactory threadFactory = new WorldThreadFactory(poolName, index);
        Disruptor<LogicEvent> disruptor = new Disruptor<>(eventFactory, ringBufferSize, threadFactory, producerType, waitStrategy);
        LogicEventHandler logicEventHandler = new LogicEventHandler(worldThread);
        disruptor.handleEventsWith(logicEventHandler);
        disruptor.setDefaultExceptionHandler(logicEventHandler);

        return disruptor;
    }
}
