package com.highfly029.core.world;

/**
 * @ClassName LogicEventType
 * @Description 逻辑队列事件类型
 * @Author liyunpeng
 **/
public final class LogicEventType {
    /**
     * 作为服务端接收逻辑事件类型
     */
    public static final byte TCP_SERVER_EVENT_TYPE = 1;
    /**
     * 作为客户端接收逻辑事件类型
     */
    public static final byte TCP_CLIENT_EVENT_TYPE = 2;
    /**
     * 短连接事件
     */
    public static final byte HTTP_SERVER_EVENT_TYPE = 3;
    public static final byte HTTP_CLIENT_EVENT_TYPE = 4;
    /**
     * mysql客户端事件
     */
    public static final byte MYSQL_CLIENT_EVENT_TYPE = 5;
    /**
     * reids客户端事件
     */
    public static final byte REDIS_CLIENT_EVENT_TYPE = 6;
    /**
     * 作为UDP服务端接收逻辑事件类型
     */
    public static final byte UDP_SERVER_EVENT_TYPE = 7;
    /**
     * 作为UDP客户端接收逻辑事件类型
     */
    public static final byte UDP_CLIENT_EVENT_TYPE = 8;
    /**
     * 世界内部事件传输
     */
    public static final byte WORLD_INNER_EVENT_TYPE = 11;

    /**
     * 事件类型最大值
     */
    public static final int maxValue = 11;
}
