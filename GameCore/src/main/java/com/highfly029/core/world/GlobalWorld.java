package com.highfly029.core.world;

import java.lang.reflect.InvocationTargetException;
import java.net.URI;

import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.core.config.BaseGlobalConfig;
import com.highfly029.core.constant.ConfigConst;
import com.highfly029.core.db.DbConfigNameConst;
import com.highfly029.core.db.MySQLUtils;
import com.highfly029.core.interfaces.IGlobalConfig;
import com.highfly029.core.interfaces.IHttpClientCallback;
import com.highfly029.core.interfaces.IHttpHandler;
import com.highfly029.core.interfaces.IHttpPath;
import com.highfly029.core.interfaces.IMySQLCallback;
import com.highfly029.core.interfaces.IPacketParser;
import com.highfly029.core.interfaces.IRedisCallback;
import com.highfly029.core.interfaces.IRegisterServerGlobalProtocol;
import com.highfly029.core.interfaces.IServerGlobalProtocolHandler;
import com.highfly029.core.interfaces.ITimerCallback;
import com.highfly029.core.net.AbstractClient;
import com.highfly029.core.net.AbstractServer;
import com.highfly029.core.net.NetConfigNameConst;
import com.highfly029.core.net.NetType;
import com.highfly029.core.net.NetUtils;
import com.highfly029.core.net.http.HttpConst;
import com.highfly029.core.net.http.HttpServer;
import com.highfly029.core.net.tcp.TcpClient;
import com.highfly029.core.net.tcp.TcpServer;
import com.highfly029.core.net.udp.UdpClient;
import com.highfly029.core.net.udp.UdpServer;
import com.highfly029.core.packet.PbPacket;
import com.highfly029.core.plugins.statistics.StatisticsEnum;
import com.highfly029.core.plugins.timer.Timer;
import com.highfly029.core.plugins.timer.TimerPlugin;
import com.highfly029.core.protocol.BasePtCode;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.HttpClientTool;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.tool.MysqlTool;
import com.highfly029.core.tool.NetTool;
import com.highfly029.core.tool.PacketParserTool;
import com.highfly029.core.tool.RedisClientTool;
import com.highfly029.core.tool.SessionTool;
import com.highfly029.core.tool.SystemTimeTool;
import com.highfly029.core.tool.ThreadPoolExecutorTool;
import com.highfly029.core.tool.WorldTool;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.utils.ConfigPropUtils;
import com.highfly029.utils.ReflectionUtils;
import com.highfly029.utils.SysUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjObjMap;
import com.highfly029.utils.collection.ObjectList;
import com.highfly029.utils.collection.ObjectSet;

import io.netty.channel.Channel;
import io.netty.handler.codec.DecoderResult;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.ReferenceCountUtil;
import io.vertx.mysqlclient.MySQLPool;
import io.vertx.redis.client.Command;
import io.vertx.redis.client.Redis;
import io.vertx.redis.client.Request;
import io.vertx.redis.client.Response;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;

/**
 * @ClassName GlobalWorld
 * @Description 进程唯一的主世界
 * @Author liyunpeng
 **/
public abstract class GlobalWorld extends WorldThread {
    /**
     * 子世界数量
     */
    protected int childWorldNum;
    /**
     * http自增索引
     */
    private int httpIncreaseIndex;
    /**
     * tick间隔
     */
    private final int globalTickInterval;
    /**
     * 主世界上次tick
     */
    private long lastGlobalTick = System.currentTimeMillis();
    /**
     * 定时器
     */
    protected TimerPlugin timerPlugin = new TimerPlugin();
    /**
     * mysql工具
     */
    protected MysqlTool mysqlTool = new MysqlTool();
    /**
     * 网络工具
     */
    protected NetTool netTool = new NetTool();
    /**
     * session工具
     */
    protected SessionTool sessionTool;

    /**
     * 用来检测nets是否都执行成功
     */
    private final ObjectSet<Object> nets = new ObjectSet<>(Object[]::new);

    //服务器之间的心跳请求
    protected PbPacket.TcpPacket serverHeartReq;
    //服务器之间的心跳相应
    protected PbPacket.TcpPacket serverHeartResp;
    /**
     * 服务器之间global协议处理
     */
    protected IntObjectMap<IServerGlobalProtocolHandler> serverGlobalHandlerMaps = new IntObjectMap<>(IServerGlobalProtocolHandler[]::new);

    public GlobalWorld(boolean isGlobal, int childWorldNum, int globalTickInterval) {
        this.isGlobal = isGlobal;
        this.childWorldNum = childWorldNum;
        this.globalTickInterval = globalTickInterval;
    }

    public MysqlTool getMysqlTool() {
        return mysqlTool;
    }

    public TimerPlugin getTimerPlugin() {
        return timerPlugin;
    }

    public SessionTool getSessionTool() {
        return sessionTool;
    }

    @Override
    protected int checkTick(long now) {
        if (super.checkTick(now) > 0) {
            //globalTick间隔大于child 必须保证系统时间在childTick间隔内更新
            boolean isNewDay = SystemTimeTool.invalidate(now);
            if (isNewDay) {
                changeDay();
            }
        }
        final int delta;
        if ((delta = (int) (now - lastGlobalTick)) >= globalTickInterval) {
            lastGlobalTick = now;
            //触发tick前更新定时器
            timerPlugin.update();
            return delta;
        }
        return 0;
    }

    /**
     * 在线跨天
     */
    private void changeDay() {
        onDaily();
        WorldTool.addAllChildWorldLogicEvent(LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.DAILY, null, null, null);
    }

    /**
     * 在线跨天通知
     */
    public void onDaily() {

    }

    @Override
    protected void init() {
        super.init();
        sessionTool = new SessionTool(this);
        int corePoolSize = ConfigPropUtils.getIntValue("corePoolSize");
        int maximumPoolSize = ConfigPropUtils.getIntValue("maximumPoolSize");
        int keepAliveTime = ConfigPropUtils.getIntValue("keepAliveTime");
        if (maximumPoolSize > 0) {
            ThreadPoolExecutorTool.init(corePoolSize, maximumPoolSize, keepAliveTime);
        }
    }

    @Override
    protected void onAfterLoadAll() throws Exception {
        registerAllGlobalServerProtocol();
    }

    /**
     * 注册全部的服务端协议
     */
    private void registerAllGlobalServerProtocol() throws Exception {
        ObjectList<Class> clsModules = ReflectionUtils.getAllClassByInterface(IRegisterServerGlobalProtocol.class, getWorldPackageName());
        Class[] values = clsModules.getValues();
        Class cls;
        for (int i = 0, size = clsModules.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                IRegisterServerGlobalProtocol register = (IRegisterServerGlobalProtocol) cls.getDeclaredConstructor().newInstance();
                register.registerGlobalProtocol(this);
            }
        }
    }

    /**
     * 注册服务器global协议
     *
     * @param ptCode
     * @param handler
     * @throws Exception
     */
    public void registerServerGlobalProtocol(int ptCode, IServerGlobalProtocolHandler handler) throws Exception {
        if (serverGlobalHandlerMaps.containsKey(ptCode)) {
            throw new Exception("registerServerGlobalProtocol duplicate ptCode:" + ptCode);
        } else {
            serverGlobalHandlerMaps.put(ptCode, handler);
        }
    }

    /**
     * 获取配置集合
     *
     * @return
     * @throws Exception
     */
    public ObjObjMap<Class, BaseGlobalConfig> getConfigMap() throws Exception {
        ObjectList<Class> clzList = ReflectionUtils.getAllClassByInterface(IGlobalConfig.class, getWorldPackageName());
        ObjObjMap<Class, BaseGlobalConfig> map = new ObjObjMap<>(clzList.size(), Class[]::new, BaseGlobalConfig[]::new);
        Class[] values = clzList.getValues();
        Class cls;
        for (int i = 0, size = clzList.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                BaseGlobalConfig globalConfig = (BaseGlobalConfig) cls.getDeclaredConstructor().newInstance();
                globalConfig.setGlobalWorld(this);
                map.put(cls, globalConfig);
            }
        }
        return map;
    }

    /**
     * 网络模块启动，包括tcp udp http https redis mysql
     *
     * @throws Exception
     */
    private void netStartUp() throws Exception {
        //netty server
        for (String keyName : ConfigPropUtils.getPropKeySet()) {
            String[] keyNameArray = keyName.split("\\.");
            if (keyNameArray.length == 4 && keyNameArray[0].equals(NetConfigNameConst.NET_PREV)) {
                if (keyNameArray[1].equals(NetConfigNameConst.NET_TCP)) {
                    if (keyNameArray[2].equals(NetConfigNameConst.NET_CLIENT)) {
                        String name = keyNameArray[3];
                        if (ConfigPropUtils.getBoolValue(keyName)) {
                            AbstractClient client = netTool.createNetClient(NetType.NET_TCP_CLIENT, name, keyName);
                            if (client != null) {
                                ((TcpClient) client).connect();
                                nets.add(client);
                            }
                        }
                    } else if (keyNameArray[2].equals(NetConfigNameConst.NET_SERVER)) {
                        String name = keyNameArray[3];
                        if (ConfigPropUtils.getBoolValue(keyName)) {
                            AbstractServer server = netTool.createNetServer(NetType.NET_TCP_SERVER, name, keyName);
                            if (server != null) {
                                ((TcpServer) server).run();
                                nets.add(server);
                            }
                        }
                    }
                } else if (keyNameArray[1].equals(NetConfigNameConst.NET_UDP)) {
                    if (keyNameArray[2].equals(NetConfigNameConst.NET_CLIENT)) {
                        String name = keyNameArray[3];
                        if (ConfigPropUtils.getBoolValue(keyName)) {
                            AbstractClient client = netTool.createNetClient(NetType.NET_UDP_CLIENT, name, keyName);
                            if (client != null) {
                                ((UdpClient) client).connect();
                                nets.add(client);
                            }
                        }
                    } else if (keyNameArray[2].equals(NetConfigNameConst.NET_SERVER)) {
                        String name = keyNameArray[3];
                        if (ConfigPropUtils.getBoolValue(keyName)) {
                            AbstractServer server = netTool.createNetServer(NetType.NET_UDP_SERVER, name, keyName);
                            if (server != null) {
                                ((UdpServer) server).run();
                                nets.add(server);
                            }
                        }
                    }
                } else if (keyNameArray[1].equals(NetConfigNameConst.NET_HTTP)) {
                    if (keyNameArray[2].equals(NetConfigNameConst.NET_SERVER)) {
                        //http server
                        String name = keyNameArray[3];
                        if (ConfigPropUtils.getBoolValue(keyName)) {
                            AbstractServer server = netTool.createNetServer(NetType.NET_HTTP_SERVER, name, keyName);
                            if (server != null) {
                                ((HttpServer) server).run();
                                nets.add(server);
                            }
                        }
                    } else if (keyNameArray[2].equals(NetConfigNameConst.NET_CLIENT)) {
                        //http client
                        if (ConfigPropUtils.getBoolValue(keyName)) {
                            HttpClientTool.init();
                        }
                    }
                }
            }


            if (keyNameArray.length == 3 && keyNameArray[0].equals(DbConfigNameConst.DB)) {
                //mysql client
                if (keyNameArray[1].equals(DbConfigNameConst.MYSQL)) {
                    String name = keyNameArray[2];
                    if (ConfigPropUtils.getBoolValue(keyName)) {
                        MySQLPool mySQLPool = mysqlTool.createMySQLPool(name, keyName);
                        if (mySQLPool == null) {
                            throw new Exception("create MySQLPool error name=" + name + " config=" + keyName);
                        }
                        nets.add(mySQLPool);

                        MySQLUtils.sendGlobal(mySQLPool, "select now()", null, (isSuccess, rows, errorMsg) -> {
                            if (isSuccess) {
                                nets.remove(mySQLPool);
                                LoggerTool.systemLogger.info("mysql:{} connect success", keyName);
                            } else {
                                LoggerTool.systemLogger.error("mysql:{} connect fail errorMsg={}", keyName, errorMsg);
                            }
                        });
                    }
                } else if (keyNameArray[1].equals(DbConfigNameConst.REDIS)) {
                    //redis client
                    String name = keyNameArray[2];
                    if (ConfigPropUtils.getBoolValue(keyName)) {
                        int type = ConfigPropUtils.getIntValue(String.format("%s.%s", keyName, DbConfigNameConst.TYPE));
                        int poolSize = ConfigPropUtils.getIntValue(String.format("%s.%s", keyName, DbConfigNameConst.POOL_SIZE));
                        ObjectList<String> list = new ObjectList<>(String[]::new);
                        int index = 1;
                        do {
                            String connection = ConfigPropUtils.getValue(String.format("%s.%s%s", keyName, DbConfigNameConst.CONNECTION, index));
                            if (connection == null || connection.equals("")) {
                                break;
                            }
                            list.add(connection);
                            index++;
                        } while (index > 0);
                        Redis redis = RedisClientTool.init(type, poolSize, list.toArray());
                        nets.add(redis);
                        Request request = Request.cmd(Command.ECHO).arg("ok");
                        RedisClientTool.send(0, request, (isSuccess, response, errorMsg) -> {
                            if (isSuccess) {
                                nets.remove(redis);
                                LoggerTool.systemLogger.info("redis:{} connect success {}", keyName, response);
                            } else {
                                LoggerTool.systemLogger.error("redis:{} connect fail errorMsg={}", keyName, errorMsg);
                            }
                        });
                    }
                }
            }
        }
    }


    @Override
    protected void onBeforeStartUp() {
        super.onBeforeStartUp();
        try {
            //在网络连接启动之前注册协议打印日志
            registerAllPacketParser();
            //在网络连接启动之前增加忽略的协议处理
            addIgnoreDump();
            addIgnoreStatistics();

            //初始化服务器之间的心跳协议包
            initServerHeartPacket();
        } catch (Exception exception) {
            LoggerTool.systemLogger.error("registerPacketParser", exception);
        }
    }

    @Override
    protected void startUp() throws Exception {
        super.startUp();

        netStartUp();
        if (ConfigPropUtils.getBoolValue(ConfigConst.isStartUpQuickly)) {
            startUpSuccess();
        } else {
            ITimerCallback callback = (Timer timer) -> {
                if (nets.size() == 0) {
                    timer.cancel();
                    startUpSuccess();
                } else {
                    if (timer.getRemainCnt() == 0) {
                        LoggerTool.systemLogger.info("start world fail then exit! nets={}", nets);
                        SysUtils.exit();
                    } else {
                        LoggerTool.systemLogger.info("wait for nets={}", nets);
                    }
                }
            };
            Timer timer = Timer.create().buildDelay(1000).buildRepeated(100).buildInterval(1000).buildCallback(callback);
            timerPlugin.addTimer(timer);
        }
    }

    @Override
    protected void beforeOnShutDown() {
        super.beforeOnShutDown();
        netTool.shutdown();
        mysqlTool.shutdown();
        RedisClientTool.close();
        HttpClientTool.close();
    }

    private void startUpSuccess() {
        try {
            handleAllConfigs(false);

            //先执行global的onStartUpSuccess再执行child的onStartUpSuccess
            onStartUpSuccess();
            WorldTool.addAllChildWorldLogicEvent(LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_START_SUCCESS, null, null, null);
        } catch (Exception e) {
            LoggerTool.systemLogger.error(e.getMessage(), e);
            SysUtils.exit();
        }


        LoggerTool.systemLogger.info("\n" +
                "       __                    __                            __     \n" +
                "      /\\ \\__                /\\ \\__                        /\\ \\    \n" +
                "  ____\\ \\ ,_\\    __     _ __\\ \\ ,_\\      __  __  _____    \\ \\ \\   \n" +
                " /',__\\\\ \\ \\/  /'__`\\  /\\`'__\\ \\ \\/     /\\ \\/\\ \\/\\ '__`\\   \\ \\ \\  \n" +
                "/\\__, `\\\\ \\ \\_/\\ \\L\\.\\_\\ \\ \\/ \\ \\ \\_    \\ \\ \\_\\ \\ \\ \\L\\ \\   \\ \\_\\ \n" +
                "\\/\\____/ \\ \\__\\ \\__/.\\_\\\\ \\_\\  \\ \\__\\    \\ \\____/\\ \\ ,__/    \\/\\_\\\n" +
                " \\/___/   \\/__/\\/__/\\/_/ \\/_/   \\/__/     \\/___/  \\ \\ \\/      \\/_/\n" +
                "                                                   \\ \\_\\          \n" +
                "                                                    \\/_/          \n");
        System.gc();
    }

    /**
     * 加载模版配置数据
     */
    protected void loadTemplateConfig(boolean isHotFix) {

    }

    /**
     * 清理模版配置
     */
    protected void clearTemplateConfig() {

    }

    /**
     * 处理全局配置
     *
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    public ObjObjMap<Class, BaseGlobalConfig> handleAllConfigs(boolean isHotFix) throws Exception {
        ObjObjMap<Class, BaseGlobalConfig> configMap = getConfigMap();

        loadTemplateConfig(isHotFix);
        BaseGlobalConfig[] values = configMap.getValues();
        BaseGlobalConfig globalConfig;

        if (ConfigPropUtils.getBoolValue(ConfigConst.isCheckExcelData)) {
            for (int i = 0, len = values.length; i < len; i++) {
                if ((globalConfig = values[i]) != null) {
                    globalConfig.check(isHotFix);
                }
            }
        }

        if (isHotFix) {
            for (int i = 0, len = values.length; i < len; i++) {
                if ((globalConfig = values[i]) != null) {
                    globalConfig.reload();
                }
            }
        } else {
            for (int i = 0, len = values.length; i < len; i++) {
                if ((globalConfig = values[i]) != null) {
                    globalConfig.load();
                }
            }
        }

        for (int i = 0, len = values.length; i < len; i++) {
            if ((globalConfig = values[i]) != null) {
                globalConfig.init();
            }
        }
        onHandleConfigData(configMap);
        onHandleConstConfigData();
        onHandlerConfigOver();

        clearTemplateConfig();
        return configMap;
    }

    /**
     * 处理配置数据
     *
     * @param configMap
     */
    protected void onHandleConfigData(ObjObjMap<Class, BaseGlobalConfig> configMap) {

    }

    /**
     * 处理常量配置数据 (在清理TemplateTool之前)
     */
    protected void onHandleConstConfigData() {

    }

    /**
     * 处理配置结束
     */
    protected void onHandlerConfigOver() {

    }

    /**
     * 初始化服务器之间的心跳协议包
     */
    private void initServerHeartPacket() {
        PbPacket.TcpPacket.Builder builderReq = PbPacket.TcpPacket.newBuilder();
        builderReq.setPtCode(BasePtCode.HEART_REQ);
        serverHeartReq = builderReq.build();

        PbPacket.TcpPacket.Builder builderResp = PbPacket.TcpPacket.newBuilder();
        builderResp.setPtCode(BasePtCode.HEART_RESP);
        serverHeartResp = builderResp.build();
    }

    /**
     * 注册当前包下的协议解析器
     */
    protected void registerPacketParser(String packetName) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        ObjectList<Class> clzList = ReflectionUtils.getAllClassByInterface(IPacketParser.class, packetName);
        for (int i = 0, len = clzList.size(); i < len; i++) {
            Class cls = clzList.get(i);
            IPacketParser packetParser = (IPacketParser) cls.getDeclaredConstructor().newInstance();
            PacketParserTool.registerPacketParser(packetParser.getPtCode(), packetParser);
        }
    }

    /**
     * 统一注册协议解析器
     */
    protected void registerAllPacketParser() throws Exception {

    }

    /**
     * 增加忽略打印的协议
     */
    protected void addIgnoreDump() {
        PacketParserTool.addIgnoreDump(BasePtCode.CONNECT_TYPE_REQ);
        PacketParserTool.addIgnoreDump(BasePtCode.CONNECT_TYPE_RESP);
        PacketParserTool.addIgnoreDump(BasePtCode.HEART_REQ);
        PacketParserTool.addIgnoreDump(BasePtCode.HEART_RESP);
    }

    /**
     * 增加忽略统计的协议
     */
    protected void addIgnoreStatistics() {
        StatisticsEnum.MSG_LENGTH.getPlugin().addIgnoreStatistics(BasePtCode.CONNECT_TYPE_REQ);
        StatisticsEnum.MSG_LENGTH.getPlugin().addIgnoreStatistics(BasePtCode.CONNECT_TYPE_RESP);
        StatisticsEnum.MSG_LENGTH.getPlugin().addIgnoreStatistics(BasePtCode.HEART_REQ);
        StatisticsEnum.MSG_LENGTH.getPlugin().addIgnoreStatistics(BasePtCode.HEART_RESP);
    }

    /**
     * 快捷创建TcpPacket
     *
     * @param protocolCode
     * @param byteString
     * @return
     */
    public PbPacket.TcpPacket createTcpPacket(int protocolCode, ByteString byteString) {
        PbPacket.TcpPacket.Builder packet = PbPacket.TcpPacket.newBuilder();
        packet.setPtCode(protocolCode);
        if (byteString != null) {
            packet.setData(byteString);
        }
        return packet.build();
    }

    /**
     * 注册事件处理器
     */
    protected void loadEventHandler() {
        eventHandlers[LogicEventType.TCP_SERVER_EVENT_TYPE] = this::onTcpServerEvent;
        eventHandlers[LogicEventType.TCP_CLIENT_EVENT_TYPE] = this::onTcpClientEvent;
        eventHandlers[LogicEventType.HTTP_SERVER_EVENT_TYPE] = this::onHttpServerEvent;
        eventHandlers[LogicEventType.HTTP_CLIENT_EVENT_TYPE] = this::onHttpClientEvent;
        eventHandlers[LogicEventType.MYSQL_CLIENT_EVENT_TYPE] = this::onMySQLClientEvent;
        eventHandlers[LogicEventType.REDIS_CLIENT_EVENT_TYPE] = this::onRedisClientEvent;
        eventHandlers[LogicEventType.UDP_SERVER_EVENT_TYPE] = this::onUdpServerEvent;
        eventHandlers[LogicEventType.UDP_CLIENT_EVENT_TYPE] = this::onUdpClientEvent;
        eventHandlers[LogicEventType.WORLD_INNER_EVENT_TYPE] = this::onWorldInnerEvent;
    }

    /**
     * 接收世界内部事件
     *
     * @param logicEvent
     */
    protected void onWorldInnerEvent(LogicEvent logicEvent) {

    }

    public void onTcpServerEvent(LogicEvent event) {
        if (event.getLogicEventAction() == LogicEventAction.PORT_BIND_SUCCESS) {
            TcpServer tcpServer = (TcpServer) event.getData1();
            nets.remove(tcpServer);
        }
    }

    public void onTcpClientEvent(LogicEvent event) {
        if (event.getLogicEventAction() == LogicEventAction.CHANNEL_ACTIVE) {
            TcpClient tcpClient = (TcpClient) event.getData1();
            nets.remove(tcpClient);
        }
    }

    public void onUdpServerEvent(LogicEvent event) {
        if (event.getLogicEventAction() == LogicEventAction.PORT_BIND_SUCCESS) {
            UdpServer udpServer = (UdpServer) event.getData1();
            nets.remove(udpServer);
        }
    }

    public void onUdpClientEvent(LogicEvent event) {
        if (event.getLogicEventAction() == LogicEventAction.PORT_BIND_SUCCESS) {
            UdpClient udpClient = (UdpClient) event.getData1();
            nets.remove(udpClient);
        }
    }

    public void onHttpServerEvent(LogicEvent event) {
        switch (event.getLogicEventAction()) {
            case LogicEventAction.PORT_BIND_SUCCESS -> {
                HttpServer httpServer = (HttpServer) event.getData1();
                nets.remove(httpServer);
            }
            case LogicEventAction.CHANNEL_READ -> {
                Channel channel = (Channel) event.getData2();
                try {
                    boolean flag = false;
                    HttpResponseStatus httpResponseStatus = null;
                    do {
                        if (!(event.getData1() instanceof FullHttpRequest)) {
                            break;
                        }
                        FullHttpRequest request = (FullHttpRequest) event.getData1();
                        if (request.decoderResult().equals(DecoderResult.SUCCESS)) {
                            break;
                        }
                        URI uri = new URI(request.uri());

                        String path = uri.getPath();
                        if (path == null) {
                            break;
                        }

                        if (NetUtils.favicon.equals(path)) {
                            httpResponseStatus = HttpResponseStatus.FORBIDDEN;
                            break;
                        }

                        if (!path.startsWith(HttpConst.route)) {
                            httpResponseStatus = HttpResponseStatus.NOT_FOUND;
                            break;
                        }

                        if (path.length() <= HttpConst.route.length()) {
                            httpResponseStatus = HttpResponseStatus.BAD_REQUEST;
                            break;
                        }

                        LoggerTool.httpLogger.info("HTTP ip={},port={},uri={},method={},params={},content={}",
                                NetUtils.getIP(channel), NetUtils.getPort(channel), uri, request.method().name(), HttpUtils.getRequestParams(request), HttpUtils.getRequestContent(request));

                        onHttpServerResponse(channel, request, path.substring(HttpConst.route.length()));
                        flag = true;
                    } while (false);
                    if (!flag) {
                        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR);
                        if (httpResponseStatus != null) {
                            response.setStatus(httpResponseStatus);
                        }
                        HttpUtils.sendHttpResponse(channel, response, HttpConst.RESP_FAIL);
                    }
                } catch (Exception e) {
                    LoggerTool.httpLogger.error(e.getMessage(), e);
                    channel.close();
                } finally {
                    ReferenceCountUtil.release(event.getData1());
                }
            }
        }
    }

    protected IHttpPath getHttpPath(String path) {
        return null;
    }

    private void onHttpServerResponse(Channel channel, FullHttpRequest request, String path) {
        final IHttpPath pathEnum = getHttpPath(path);
        if (pathEnum == null) {
            HttpUtils.sendHttpResponse(channel, HttpConst.RESP_FAIL);
            return;
        }

        //CPU分支预测 频率高的优先
        if (pathEnum.isOneChildHandleOnly()) {
            int childIndex = httpIncreaseIndex++ % childWorldNum + 1;
            WorldTool.addChildWorldLogicEvent(childIndex, LogicEventType.HTTP_SERVER_EVENT_TYPE, LogicEventAction.G2C_HTTP_MSG, channel, request, path);
        } else if (pathEnum.isAllChildHandle()) {
            WorldTool.addAllChildWorldLogicEvent(LogicEventType.HTTP_SERVER_EVENT_TYPE, LogicEventAction.G2C_HTTP_MSG, channel, request, path);
        } else {
            //global处理
            IHttpHandler httpHandler = httpHandlerMaps.get(path);
            if (httpHandler == null) {
                HttpUtils.sendHttpResponse(channel, HttpUtils.NOT_FIND_HANDLER);
            } else {
                httpHandler.handler(channel, request);
            }
        }
    }

    private void onHttpClientEvent(LogicEvent logicEvent) {
        final int index = (int) logicEvent.getData1();
        final IHttpClientCallback callback = (IHttpClientCallback) logicEvent.getData3();
        final byte action = logicEvent.getLogicEventAction();
        if (action == LogicEventAction.SUCCESS) {
            if (index > 0) {
                WorldTool.addChildWorldLogicEvent(index, logicEvent.getLogicEventType(), logicEvent.getLogicEventAction(), logicEvent.getData2(), callback);
            } else {
                String body = (String) logicEvent.getData2();
                callback.onHttpClientCallback(true, body, null);
            }
        } else if (action == LogicEventAction.FAIL) {
            if (index > 0) {
                WorldTool.addChildWorldLogicEvent(index, logicEvent.getLogicEventType(), logicEvent.getLogicEventAction(), logicEvent.getData2(), callback);
            } else {
                String errorMsg = (String) logicEvent.getData2();
                callback.onHttpClientCallback(false, null, errorMsg);
            }
        }
    }

    private void onMySQLClientEvent(LogicEvent logicEvent) {
        final int index = (int) logicEvent.getData1();
        final IMySQLCallback callback = (IMySQLCallback) logicEvent.getData3();
        final byte action = logicEvent.getLogicEventAction();
        if (action == LogicEventAction.SUCCESS) {
            if (index > 0) {
                WorldTool.addChildWorldLogicEvent(index, logicEvent.getLogicEventType(), logicEvent.getLogicEventAction(), logicEvent.getData2(), callback);
            } else {
                RowSet<Row> rows = (RowSet<Row>) logicEvent.getData2();
                callback.onMysqlCallback(true, rows, null);
            }
        } else if (action == LogicEventAction.FAIL) {
            if (index > 0) {
                WorldTool.addChildWorldLogicEvent(index, logicEvent.getLogicEventType(), logicEvent.getLogicEventAction(), logicEvent.getData2(), callback);
            } else {
                Throwable throwable = (Throwable) logicEvent.getData2();
                callback.onMysqlCallback(false, null, throwable.getMessage());
            }
        }
    }

    public void onRedisClientEvent(LogicEvent logicEvent) {
        final int index = (int) logicEvent.getData1();
        final IRedisCallback callback = (IRedisCallback) logicEvent.getData3();
        final byte action = logicEvent.getLogicEventAction();
        if (action == LogicEventAction.SUCCESS) {
            if (index > 0) {
                WorldTool.addChildWorldLogicEvent(index, logicEvent.getLogicEventType(), logicEvent.getLogicEventAction(), logicEvent.getData2(), callback);
            } else {
                Response response = (Response) logicEvent.getData2();
                callback.onRedisCallback(true, response, null);
            }
        } else if (action == LogicEventAction.FAIL) {
            if (index > 0) {
                WorldTool.addChildWorldLogicEvent(index, logicEvent.getLogicEventType(), logicEvent.getLogicEventAction(), logicEvent.getData2(), callback);
            } else {
                Throwable throwable = (Throwable) logicEvent.getData2();
                callback.onRedisCallback(false, null, throwable.getMessage());
            }
        }
    }

    /**
     * 增加服务器消息到子世界
     *
     * @param worldIndex
     * @param ptCode
     * @param session
     * @param message
     */
    public void addServerMessage2ChildWorld(int worldIndex, int ptCode, Session session, GeneratedMessageV3 message) {
        WorldTool.addChildWorldLogicEvent(worldIndex, LogicEventType.WORLD_INNER_EVENT_TYPE, LogicEventAction.G2C_SERVER_PACKET, ptCode, session, message);
    }

    /**
     * 发送消息到所有包含此功能的服务器
     *
     * @param sessionType
     * @param loadBalanceFeature
     * @param ptCode
     * @param messageV3
     */
    protected void sendMsg2AllFeatureServer(int sessionType, int loadBalanceFeature, int ptCode, GeneratedMessageV3 messageV3) {
        IntObjectMap<Session> map = sessionTool.getSessionMap(sessionType);
        map.foreachImmutable((k, v) -> {
            if (v.getFeatures().contains(loadBalanceFeature)) {
                v.sendMsgS2S(ptCode, messageV3);
            }
        });
    }
}
