package com.highfly029.core.world;

import com.highfly029.utils.SysUtils;
import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.ExceptionHandler;
import com.lmax.disruptor.LifecycleAware;

/**
 * @ClassName LogicEventHandler
 * @Description 逻辑队列事件处理
 * @Author liyunpeng
 **/
public class LogicEventHandler implements EventHandler<LogicEvent>, LifecycleAware, ExceptionHandler<LogicEvent> {
    private final WorldThread worldThread;

    public LogicEventHandler(WorldThread worldThread) {
        this.worldThread = worldThread;
    }

    @Override
    public void onEvent(LogicEvent event, long sequence, boolean endOfBatch) throws Exception {
        worldThread.onLogicEvent(event);
    }

    @Override
    public void handleEventException(Throwable ex, long sequence, LogicEvent event) {
        worldThread.onException(ex, event.toString());
    }

    @Override
    public void handleOnStartException(Throwable ex) {
        worldThread.onException(ex, "handleOnStartException");
        SysUtils.exit();
    }

    @Override
    public void handleOnShutdownException(Throwable ex) {
        worldThread.onException(ex, "handleOnShutdownException");
    }

    @Override
    public void onStart() {
        worldThread.onStartUp();
    }

    @Override
    public void onShutdown() {
        worldThread.onShutDown();
    }
}
