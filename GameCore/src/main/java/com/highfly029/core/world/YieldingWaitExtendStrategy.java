package com.highfly029.core.world;

import com.lmax.disruptor.AlertException;
import com.lmax.disruptor.Sequence;
import com.lmax.disruptor.SequenceBarrier;
import com.lmax.disruptor.WaitStrategy;

/**
 * @ClassName YieldingWaitExtendStrategy
 * @Description 等待策略
 * @Author liyunpeng
 **/
public class YieldingWaitExtendStrategy implements WaitStrategy {
    private static final int SPIN_TRIES = 100;
    private final WorldThread worldThread;

    public YieldingWaitExtendStrategy(WorldThread worldThread) {
        this.worldThread = worldThread;
    }

    @Override
    public long waitFor(
            final long sequence, Sequence cursor, final Sequence dependentSequence, final SequenceBarrier barrier)
            throws AlertException, InterruptedException {
        long availableSequence;
        int counter = SPIN_TRIES;

        while ((availableSequence = dependentSequence.get()) < sequence) {
            counter = applyWaitMethod(barrier, counter);
        }

        return availableSequence;
    }

    @Override
    public void signalAllWhenBlocking() {
    }

    private int applyWaitMethod(final SequenceBarrier barrier, int counter)
            throws AlertException {
        barrier.checkAlert();

        if (0 == counter) {
            worldThread.onLogicLoop();
            Thread.yield();
        } else {
            --counter;
        }

        return counter;
    }
}
