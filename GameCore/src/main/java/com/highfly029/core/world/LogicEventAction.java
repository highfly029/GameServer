package com.highfly029.core.world;

/**
 * @ClassName LogicEventAction
 * @Description 逻辑事件类型的行为
 * @Author liyunpeng
 **/
public class LogicEventAction {
    public static final byte CHANNEL_READ = 1;
    public static final byte CHANNEL_ACTIVE = 2;
    public static final byte CHANNEL_INACTIVE = 3;
    public static final byte CHANNEL_WRITER_IDLE = 4;
    public static final byte CHANNEL_READER_IDLE = 5;
    public static final byte CHANNEL_ALL_IDLE = 6;
    public static final byte CHANNEL_EXCEPTION = 7;
    public static final byte CHANNEL_CONNECT_FAIL = 8;
    public static final byte CHANNEL_WRITE_FAIL = 9;
    public static final byte PORT_BIND_SUCCESS = 10;

    /**
     * mysql http redis 成功失败标识
     */
    public static final byte SUCCESS = 20;
    public static final byte FAIL = 21;

    /**
     * globalWorld传递给childWorld 客户端包
     */
    public static final byte G2C_CLIENT_PACKET = 30;
    /**
     * globalWorld传递给childWorld http数据
     */
    public static final byte G2C_HTTP_MSG = 31;
    /**
     * globalWorld传递给childWorld 服务器包
     */
    public static final byte G2C_SERVER_PACKET = 32;
    /**
     * globalWorld传递给childWorld 基础事件
     */
    public static final byte G2C_BASE_EVENT = 33;
    /**
     * childWorld传递给globalWorld 基础事件
     */
    public static final byte C2G_BASE_EVENT = 34;
    /**
     * globalWorld传递给childWorld启动成功
     */
    public static final byte G2C_START_SUCCESS = 35;
    /**
     * childWorld传递给childWorld 基础事件
     */
    public static final byte C2C_BASE_EVENT = 36;
    /**
     * 离线消息
     */
    public static final byte OFFLINE_MSG = 37;
    /**
     * 在线跨天
     */
    public static final byte DAILY = 38;


}
