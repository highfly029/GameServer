package com.highfly029.core.world;

import com.lmax.disruptor.EventFactory;

/**
 * @ClassName LogicEventFactory
 * @Description 逻辑队列事件工厂
 * @Author liyunpeng
 **/
public class LogicEventFactory implements EventFactory<LogicEvent> {
    @Override
    public LogicEvent newInstance() {
        return new LogicEvent();
    }
}
