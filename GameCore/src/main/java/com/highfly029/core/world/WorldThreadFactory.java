package com.highfly029.core.world;

import java.util.concurrent.ThreadFactory;

import io.netty.util.concurrent.FastThreadLocalThread;

/**
 * @ClassName WorldThreadFactory
 * @Description WorldThreadFactory
 * @Author liyunpeng
 **/
public class WorldThreadFactory implements ThreadFactory {
    private final String poolName;
    private final boolean daemon = false;
    private final int priority = Thread.NORM_PRIORITY;
    private final int index;

    protected final ThreadGroup threadGroup = System.getSecurityManager() == null ?
            Thread.currentThread().getThreadGroup() : System.getSecurityManager().getThreadGroup();

    public WorldThreadFactory(String poolName, int index) {
        this.poolName = poolName;
        this.index = index;
    }

    @Override
    public Thread newThread(Runnable r) {
        String threadName = index > 0 ? poolName + "-" + index : poolName;
        Thread t = new FastThreadLocalThread(threadGroup, r, threadName);
        try {
            if (t.isDaemon() != daemon) {
                t.setDaemon(daemon);
            }

            if (t.getPriority() != priority) {
                t.setPriority(priority);
            }
        } catch (Exception ignored) {
            // Doesn't matter even if failed to set.
        }
        return t;
    }
}
