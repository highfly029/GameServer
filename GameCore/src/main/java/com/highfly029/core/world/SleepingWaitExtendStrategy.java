package com.highfly029.core.world;

import java.util.concurrent.locks.LockSupport;

import com.lmax.disruptor.AlertException;
import com.lmax.disruptor.Sequence;
import com.lmax.disruptor.SequenceBarrier;
import com.lmax.disruptor.WaitStrategy;

/**
 * @ClassName SleepingWaitExtendStrategy
 * @Description 等待策略
 * @Author liyunpeng
 **/
public class SleepingWaitExtendStrategy implements WaitStrategy {
    private static final int DEFAULT_RETRIES = 200;
    /**
     * 1ms睡眠
     */
    private static final long DEFAULT_SLEEP = 1000000;
    private final WorldThread worldThread;
    private final int retries;
    private final long sleepTimeNs;


    public SleepingWaitExtendStrategy(WorldThread worldThread) {
        this.worldThread = worldThread;
        this.retries = DEFAULT_RETRIES;
        this.sleepTimeNs = DEFAULT_SLEEP;
    }

    @Override
    public long waitFor(
            final long sequence, Sequence cursor, final Sequence dependentSequence, final SequenceBarrier barrier)
            throws AlertException {
        long availableSequence;
        int counter = retries;

        while ((availableSequence = dependentSequence.get()) < sequence) {
            counter = applyWaitMethod(barrier, counter);
        }

        return availableSequence;
    }

    @Override
    public void signalAllWhenBlocking() {
    }

    private int applyWaitMethod(final SequenceBarrier barrier, int counter)
            throws AlertException {
        barrier.checkAlert();

        //会让出更多的cpu资源
        worldThread.onLogicLoop();
        LockSupport.parkNanos(sleepTimeNs);

//        if (counter > 100) {
//            --counter;
//        } else if (counter > 0) {
//            --counter;
////            logicEventConsumer.onLogicLoop();
//            Thread.yield();
//        } else {
//            logicEventConsumer.onLogicLoop();
//            LockSupport.parkNanos(sleepTimeNs);
//        }

        return counter;
    }
}
