package com.highfly029.core.world;

import com.google.protobuf.GeneratedMessageV3;
import com.highfly029.core.interfaces.IHttpClientCallback;
import com.highfly029.core.interfaces.IHttpHandler;
import com.highfly029.core.interfaces.IMySQLCallback;
import com.highfly029.core.interfaces.IRedisCallback;
import com.highfly029.core.interfaces.IRegisterServerChildProtocol;
import com.highfly029.core.interfaces.IServerChildProtocolHandler;
import com.highfly029.core.session.Session;
import com.highfly029.core.tool.LoggerTool;
import com.highfly029.core.utils.HttpUtils;
import com.highfly029.utils.ReflectionUtils;
import com.highfly029.utils.collection.IntObjectMap;
import com.highfly029.utils.collection.ObjectList;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.FullHttpRequest;
import io.vertx.redis.client.Response;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;

/**
 * @ClassName ChildWorld
 * @Description 主世界下的子世界
 * @Author liyunpeng
 **/
public abstract class ChildWorld extends WorldThread {
    /**
     * 子世界序号
     */
    private final int index;

    /**
     * tick间隔
     */
    private final int childTickInterval;
    /**
     * 子世界上次tick
     */
    private long lastChildTick = System.currentTimeMillis();

    /**
     * 服务器之间child协议处理
     */
    protected IntObjectMap<IServerChildProtocolHandler> serverChildHandlerMaps = new IntObjectMap<>(IServerChildProtocolHandler[]::new);

    public ChildWorld(int index, int childTickInterval) {
        this.index = index;
        if (index <= 0) {
            throw new IllegalArgumentException("ChildWorld index must be positive");
        }
        this.childTickInterval = childTickInterval;
    }

    @Override
    protected int checkTick(long now) {
        final int delta;
        if ((delta = (int) (now - lastTickTime)) >= childTickInterval) {
            lastTickTime = now;
            return delta;
        }
        return 0;
    }

    public int getIndex() {
        return index;
    }

    @Override
    protected void loadEventHandler() {
        eventHandlers[LogicEventType.MYSQL_CLIENT_EVENT_TYPE] = this::onMySQLClientEvent;
        eventHandlers[LogicEventType.HTTP_CLIENT_EVENT_TYPE] = this::onHttpClientEvent;
        eventHandlers[LogicEventType.REDIS_CLIENT_EVENT_TYPE] = this::onRedisClientEvent;
        eventHandlers[LogicEventType.HTTP_SERVER_EVENT_TYPE] = this::onHttpServerEvent;
        eventHandlers[LogicEventType.WORLD_INNER_EVENT_TYPE] = this::onWorldInnerEvent;
    }

    @Override
    protected void onBeforeStartUp() {
        super.onBeforeStartUp();
        LoggerTool.systemLogger.info("childWorld index={} beforeStartUp", index);
    }

    /**
     * 在线跨天通知
     */
    public void onDaily() {

    }

    /**
     * 接收世界内部事件
     *
     * @param logicEvent
     */
    protected void onWorldInnerEvent(LogicEvent logicEvent) {
        switch (logicEvent.getLogicEventAction()) {
            case LogicEventAction.G2C_SERVER_PACKET -> {
                int ptCode = (int) logicEvent.getData1();
                Session session = (Session) logicEvent.getData2();
                GeneratedMessageV3 message = (GeneratedMessageV3) logicEvent.getData3();
                IServerChildProtocolHandler handler = serverChildHandlerMaps.get(ptCode);
                try {
                    handler.onServerProtocolHandler(session, ptCode, message);
                } catch (Exception e) {
                    LoggerTool.systemLogger.error("G2C_SERVER_PACKET childIndex=" + getIndex() + " ptCode=" + ptCode, e);
                }
            }
            case LogicEventAction.G2C_START_SUCCESS -> onStartUpSuccess();
            case LogicEventAction.DAILY -> onDaily();
        }
    }

    private void onHttpServerEvent(LogicEvent logicEvent) {
        if (logicEvent.getLogicEventAction() == LogicEventAction.G2C_HTTP_MSG) {
            Channel channel = (Channel) logicEvent.getData1();
            FullHttpRequest request = (FullHttpRequest) logicEvent.getData2();
            String path = (String) logicEvent.getData3();
            IHttpHandler httpHandler = httpHandlerMaps.get(path);
            if (httpHandler == null) {
                HttpUtils.sendHttpResponse(channel, HttpUtils.NOT_FIND_HANDLER);
            } else {
                httpHandler.handler(channel, request);
            }
        }
    }

    private void onMySQLClientEvent(LogicEvent logicEvent) {
        final IMySQLCallback callback = (IMySQLCallback) logicEvent.getData2();
        final byte action = logicEvent.getLogicEventAction();
        if (action == LogicEventAction.SUCCESS) {
            RowSet<Row> rows = (RowSet<Row>) logicEvent.getData1();
            callback.onMysqlCallback(true, rows, null);
        } else if (action == LogicEventAction.FAIL) {
            Throwable throwable = (Throwable) logicEvent.getData1();
            callback.onMysqlCallback(false, null, throwable.getMessage());
        }
    }

    private void onRedisClientEvent(LogicEvent logicEvent) {
        final IRedisCallback callback = (IRedisCallback) logicEvent.getData2();
        final byte action = logicEvent.getLogicEventAction();
        if (action == LogicEventAction.SUCCESS) {
            Response response = (Response) logicEvent.getData1();
            callback.onRedisCallback(true, response, null);
        } else if (action == LogicEventAction.FAIL) {
            Throwable throwable = (Throwable) logicEvent.getData1();
            callback.onRedisCallback(false, null, throwable.getMessage());
        }
    }

    private void onHttpClientEvent(LogicEvent logicEvent) {
        final IHttpClientCallback callback = (IHttpClientCallback) logicEvent.getData2();
        final byte action = logicEvent.getLogicEventAction();
        if (action == LogicEventAction.SUCCESS) {
            String body = (String) logicEvent.getData1();
            callback.onHttpClientCallback(true, body, null);
        } else if (action == LogicEventAction.FAIL) {
            String errorMsg = (String) logicEvent.getData1();
            callback.onHttpClientCallback(false, null, errorMsg);
        }
    }

    @Override
    protected void onAfterLoadAll() throws Exception {
        registerAllServerChildProtocol();
    }


    /**
     * 注册全部的服务端child协议
     */
    private void registerAllServerChildProtocol() throws Exception {
        ObjectList<Class> clsModules = ReflectionUtils.getAllClassByInterface(IRegisterServerChildProtocol.class, getWorldPackageName());
        Class[] values = clsModules.getValues();
        Class cls;
        for (int i = 0, size = clsModules.size(); i < size; i++) {
            if ((cls = values[i]) != null) {
                IRegisterServerChildProtocol register = (IRegisterServerChildProtocol) cls.getDeclaredConstructor().newInstance();
                register.registerChildProtocol(this);
            }
        }
    }

    /**
     * 注册服务器child协议
     *
     * @param ptCode
     * @param handler
     * @throws Exception
     */
    public void registerServerChildProtocol(int ptCode, IServerChildProtocolHandler handler) throws Exception {
        if (serverChildHandlerMaps.containsKey(ptCode)) {
            throw new Exception("registerServerChildProtocol duplicate ptCode:" + ptCode);
        } else {
            serverChildHandlerMaps.put(ptCode, handler);
        }
    }
}
