package com.highfly029.core.constant;

import com.highfly029.utils.ConfigPropUtils;

/**
 * @ClassName ConfigConst
 * @Description 配置常量
 * @Author liyunpeng
 **/
public interface ConfigConst {
    /**
     * 是否检测表格数据
     */
    String isCheckExcelData = "isCheckExcelData";

    /**
     * 是否检测池化对象是否清理干净
     */
    String isCheckPooledObj = "isCheckPooledObj";

    /**
     * 服务器唯一标识
     */
    int identifier = ConfigPropUtils.getIntValue("identifier");

    /**
     * 是否开启gm
     */
    String isOpenGM = "isOpenGM";
    /**
     * 是否快速启动 不等待连接结果
     */
    String isStartUpQuickly = "isStartUpQuickly";

    /**
     * tcp发送给客户端的包是否合并
     */
    boolean tcpPacketCombine = ConfigPropUtils.getBoolValue("tcpPacketCombine");
}
