package com.highfly029.core.plugins.counter;

import java.util.Arrays;

import com.highfly029.utils.collection.ObjectList;

import org.junit.Test;

public class IntegerCounterTest {

    @Test
    public void test1() {
        IntegerCounter integerCounter = new IntegerCounter();
        for (int i = 0; i < 10; i++) {
            System.out.println(integerCounter.getNextValue());
        }

        for (int i = 0; i < 10; i++) {
            System.out.println(integerCounter.getNextValue());
        }

        integerCounter.reset();

        for (int i = 0; i < 10; i++) {
            System.out.println(integerCounter.getNextValue());
        }
    }

    @Test
    public void test2() {
        IntegerCounter integerCounter = new IntegerCounter(0, 63, true);
        for (int i = 0; i < 200; i++) {
            System.out.println(integerCounter.getNextValue());
        }
//        integerCounter.reset();
//        for (int i = 0; i < 10; i++) {
//            System.out.println(integerCounter.getNextValue());
//        }
    }

    @Test
    public void reconnect() {

        int size = 20;
        IntegerCounter integerCounter = new IntegerCounter(0, size - 1, true);
        String[] cache = new String[size];
        long sendIndex = 0;
        int curIndex = 0;
        for (int i = 0; i < 65; i++) {
            curIndex = integerCounter.getNextValue();
            sendIndex = i;
            cache[curIndex] = "cache" + i;
        }
        System.out.println("======end======");
        System.out.println("curIndex=" + curIndex + " sendIndex=" + sendIndex + " ");
        System.out.println(Arrays.toString(cache));

        long clientIndex = 54;
        int delta = (int) (sendIndex - clientIndex);
        if (delta > size) {
            System.err.println("error");
        }

        for (long i = clientIndex; i < sendIndex; i++) {
            int index = (int) (i & (size - 1));
//            System.out.println(index);
        }

        ObjectList<String> list = new ObjectList<>(String[]::new);
        while (delta > 0) {
            String data = cache[curIndex--];
            if (curIndex < 0) {
                curIndex = size - 1;
            }
            list.add(data);
            delta--;
        }
        System.out.println(list.size());
        System.out.println(list);

    }
}