package com.highfly029.core.plugins.pool;

import com.highfly029.core.interfaces.IPooledObj;

/**
 * @ClassName B
 * @Description B
 * @Author liyunpeng
 **/
public class Extend implements IPooledObj {
    public float extend1;
    protected float extend2;
    private float extend3;

    @Override
    public void init() {

    }

    @Override
    public void release() {
        extend1 = 0.f;
    }
}
