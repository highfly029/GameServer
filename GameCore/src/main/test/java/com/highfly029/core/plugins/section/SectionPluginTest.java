package com.highfly029.core.plugins.section;

import org.junit.Test;

public class SectionPluginTest {

    @Test
    public void test1() {

        SectionPlugin<Integer> sectionPlugin = new SectionPlugin<>();
        sectionPlugin.add(1, 11, 1);
        sectionPlugin.add(11, 21, 11);
        sectionPlugin.add(21, -1, 21);

        sectionPlugin.generate();

        System.out.println(sectionPlugin.get(0));
        System.out.println(sectionPlugin.get(1));
        System.out.println(sectionPlugin.get(10));
        System.out.println(sectionPlugin.get(11));
        System.out.println(sectionPlugin.get(31));
        System.out.println(sectionPlugin.get(1200));
    }

    @Test
    public void test2() {
        SectionPlugin<Integer> sectionPlugin = new SectionPlugin<>();
        sectionPlugin.add(1, 11, 1);
        sectionPlugin.add(11, 21, 11);
        sectionPlugin.add(21, 1, 21);

        sectionPlugin.generate();

        System.out.println(sectionPlugin.get(0));
    }
}