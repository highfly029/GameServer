package com.highfly029.core.plugins.pool;

import com.highfly029.utils.collection.IntList;
import com.highfly029.utils.collection.ObjectList;

import org.junit.Test;

public class ObjectPoolPluginTest {
    @Test
    public void test() {
        ObjectPoolPlugin<Child> pool = new ObjectPoolPlugin<>(4, 10, Child[]::new, Child::new);

        ObjectList<Child> list = new ObjectList<>();
        for (int i = 0; i < 50; i++) {
            Child child = pool.get();
            child.child1 = i + 1;
            list.add(child);
        }

        System.out.println("=======dont use======");
        for (int i = 0, size = list.size(); i < size; i++) {
            Child child = list.get(i);
            pool.back(child);
        }
        System.out.println("=== over ===");
    }

    @Test
    public void testCheck() throws IllegalAccessException {
        Child child = new Child();
        Extend extend = new Extend();
        child.extend = extend;
        child.child1 = 0;

        child.byte0 = 0;
        child.byte1 = new byte[5];
        child.byte1[0] = 0;

        child.byte2 = new byte[4][5];
        child.byte2[3][4] = 0;

        child.string0 = "";
        child.string1 = new String[5];
        child.string1[3] = "";

        child.string2 = new String[5][5];
        child.string2[4][2] = "";

        extend.extend1 = 0.f;
        extend.extend2 = 0.f;

//        child.noPooledObj = new NoPooledObj();

        child.intList = new IntList();
//        child.intList.add(4);

        child.array = new Extend[2];

        child.array[0] = new Extend();

        boolean check = CheckPooledObjUtils.isReleaseCompletely(child);
        System.out.println("check======" + check);
    }
}