package com.highfly029.core.plugins.rank;

/**
 * @ClassName TestRankTool
 * @Description TestRankTool
 * @Author liyunpeng
 **/
class TestRankPlugin extends BaseRankPlugin {

    public TestRankPlugin(int rankMaxNum) {
        super(rankMaxNum);
    }

    @Override
    protected int compare(BaseRankData rankData1, BaseRankData rankData2) {
        TestRankData data1 = (TestRankData) rankData1;
        TestRankData data2 = (TestRankData) rankData2;
        if (data2 == null) {
            return -1;
        }
        if (data1 == null) {
            return 1;
        }
        //等级小的的在前
        return Long.compare(data1.level, data2.level);
    }
}

