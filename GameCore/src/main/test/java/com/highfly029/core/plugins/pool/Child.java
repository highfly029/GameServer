package com.highfly029.core.plugins.pool;

import com.highfly029.core.interfaces.IPooledObj;
import com.highfly029.utils.collection.IntList;

/**
 * @ClassName A
 * @Description A
 * @Author liyunpeng
 **/
public class Child extends Base implements IPooledObj {
    public int child1;
    protected int child2;
    private int child3;

    public byte byte0;
    public byte[] byte1;
    public byte[][] byte2;

    public short short0;
    public short[] short1;
    public short[][] short2;

    public int int0;
    public int[] int1;
    public int[][] int2;

    public long long0;
    public long[] long1;
    public long[][] long2;


    public String string0;
    public String[] string1;
    public String[][] string2;

    public Extend extend;

    public Extend extend2;

    public IntList intList;

    public NoPooledObj noPooledObj;

    public Extend[] array;

    @Override
    public void init() {

    }

    @Override
    public void release() {
        child1 = 0;
        if (extend != null) {
            extend.release();
        }
    }
}
