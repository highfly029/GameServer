package com.highfly029.core.plugins.rank;

/**
 * @ClassName TestRankData
 * @Description TestRankData
 * @Author liyunpeng
 **/
class TestRankData extends BaseRankData {
    /**
     * 等级
     */
    public long level;

    @Override
    public String toString() {
        return "TestRankData{" +
                "key=" + key +
                ",level=" + level +
                '}';
    }
}
