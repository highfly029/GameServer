package com.highfly029.core.plugins.rank;

import com.highfly029.utils.RandomUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BaseRankPluginTest {

    public TestRankPlugin rankTool;

    @Before
    public void setUp() throws Exception {

    }


    @Test
    public void testAddRank() {
        int maxNum = 5;
        TestRankData rankData;
        BaseRankData[] array;
        int len;

        //1.1 未满榜 开头增加
        System.out.println("=====未满榜 开头增加======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        rankData = new TestRankData();
        rankData.key = 999;
        rankData.level = 1;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);


        //1.2 未满榜 中间增加
        System.out.println("=====未满榜 中间增加======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        len = (int) Math.ceil(maxNum / 2.f);
        for (int i = 0; i < len; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 999;
        rankData.level = 1;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);


        //1.3 未满榜 结尾增加
        System.out.println("=====未满榜 结尾增加======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        len = (int) Math.ceil(maxNum / 2.f);
        for (int i = 0; i < len; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 999;
        rankData.level = 10;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);


        //2.1 已满榜 开头增加
        System.out.println("=====已满榜 开头增加======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        for (int i = 0; i < maxNum; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 999;
        rankData.level = -1;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);


        //2.2 已满榜 中间增加
        System.out.println("=====已满榜 中间增加======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        for (int i = 0; i < maxNum; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 999;
        rankData.level = 2;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);


        //2.3 已满榜 结尾增加
        System.out.println("=====已满榜 结尾增加======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        for (int i = 0; i < maxNum; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 999;
        rankData.level = 99;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);
    }

    @Test
    public void testUpdateRank() {
        int maxNum = 5;
        TestRankData rankData;
        BaseRankData[] array;
        int len;

        //1.1 未满榜 开头更新
        System.out.println("=====未满榜 开头更新======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        for (int i = 0; i < 1; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 0;
        rankData.level = 999;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);


        //1.2 未满榜 中间更新左移
        System.out.println("=====未满榜 中间更新左移======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        len = (int) Math.ceil(maxNum / 2.f);
        for (int i = 0; i < len; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 1;
        rankData.level = -1;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);

        //1.3 未满榜 中间更新右移
        System.out.println("=====未满榜 中间更新右移======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        len = (int) Math.ceil(maxNum / 2.f);
        for (int i = 0; i < len; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 1;
        rankData.level = 999;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);


        //1.4 未满榜 结尾更新
        System.out.println("=====未满榜 结尾更新======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        len = (int) Math.ceil(maxNum / 2.f);
        for (int i = 0; i < len; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 2;
        rankData.level = 999;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);


        //2.1 已满榜 开头更新
        System.out.println("=====已满榜 开头更新======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        for (int i = 0; i < maxNum; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 0;
        rankData.level = -1;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);


        //2.2 已满榜 中间更新左移
        System.out.println("=====已满榜 中间更新左移======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        for (int i = 0; i < maxNum; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 2;
        rankData.level = -1;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);


        //2.3 已满榜 中间更新右移
        System.out.println("=====已满榜 中间更新右移======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        for (int i = 0; i < maxNum; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 2;
        rankData.level = 999;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);


        //2.4 已满榜 结尾更新
        System.out.println("=====已满榜 结尾更新======");
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        for (int i = 0; i < maxNum; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        rankData = new TestRankData();
        rankData.key = 4;
        rankData.level = 999;
        rankTool.addRankData(rankData);
        System.out.println(rankTool);
    }

    @Test
    public void randomRank() {
        int maxNum = 20;
        TestRankData rankData;
        BaseRankData[] array;
        int len;
        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);

        //10 100 10000
        int randomNum = 1000;
        for (int i = 0; i < randomNum; i++) {
            TestRankData tmp = new TestRankData();
            int key = RandomUtils.randomInt(10000, 99999);
            int level = RandomUtils.randomInt(1, 999);
            tmp.key = key;
            tmp.level = level;
            rankTool.addRankData(tmp);
        }
        System.out.println(rankTool);
    }

    @Test
    public void removeRank() {
        int maxNum = 5;
        TestRankData rankData;
        BaseRankData[] array;
        int len;

        rankTool = new TestRankPlugin(maxNum);
        array = new BaseRankData[maxNum];
        rankTool.setRankArray(array);
        for (int i = 0; i < 5; i++) {
            TestRankData tmp = new TestRankData();
            tmp.key = i;
            tmp.level = i;
            rankTool.addRankData(tmp);
        }
        System.out.println(rankTool);
        rankTool.removeRankData(4);
        System.out.println(rankTool);
    }


    @After
    public void tearDown() throws Exception {
    }
}